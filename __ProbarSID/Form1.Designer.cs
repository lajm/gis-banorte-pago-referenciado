﻿namespace __ProbarSID
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_open = new System.Windows.Forms.Button();
            this.c_dll_version = new System.Windows.Forms.Button();
            this.c_Reset = new System.Windows.Forms.Button();
            this.c_Respuesta = new System.Windows.Forms.TextBox();
            this.c_bag_replace = new System.Windows.Forms.Button();
            this.c_cashIn = new System.Windows.Forms.Button();
            this.c_ups = new System.Windows.Forms.Button();
            this.c_shutdown = new System.Windows.Forms.Button();
            this.c_gettotal = new System.Windows.Forms.Button();
            this.c_identify = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.c_barcode = new System.Windows.Forms.Button();
            this.c_bolsa_test = new System.Windows.Forms.Button();
            this.c_bolsa = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_open
            // 
            this.c_open.Location = new System.Drawing.Point(29, 12);
            this.c_open.Name = "c_open";
            this.c_open.Size = new System.Drawing.Size(171, 39);
            this.c_open.TabIndex = 0;
            this.c_open.Text = "Sid_Test";
            this.c_open.UseVisualStyleBackColor = true;
            this.c_open.Click += new System.EventHandler(this.c_open_Click);
            // 
            // c_dll_version
            // 
            this.c_dll_version.Location = new System.Drawing.Point(29, 57);
            this.c_dll_version.Name = "c_dll_version";
            this.c_dll_version.Size = new System.Drawing.Size(171, 40);
            this.c_dll_version.TabIndex = 1;
            this.c_dll_version.Text = "Sid_Dll_Version";
            this.c_dll_version.UseVisualStyleBackColor = true;
            this.c_dll_version.Click += new System.EventHandler(this.c_dll_version_Click);
            // 
            // c_Reset
            // 
            this.c_Reset.Location = new System.Drawing.Point(29, 149);
            this.c_Reset.Name = "c_Reset";
            this.c_Reset.Size = new System.Drawing.Size(171, 39);
            this.c_Reset.TabIndex = 2;
            this.c_Reset.Text = "Sid_Reset";
            this.c_Reset.UseVisualStyleBackColor = true;
            this.c_Reset.Click += new System.EventHandler(this.c_Reset_Click);
            // 
            // c_Respuesta
            // 
            this.c_Respuesta.Location = new System.Drawing.Point(217, 75);
            this.c_Respuesta.Multiline = true;
            this.c_Respuesta.Name = "c_Respuesta";
            this.c_Respuesta.Size = new System.Drawing.Size(555, 275);
            this.c_Respuesta.TabIndex = 3;
            // 
            // c_bag_replace
            // 
            this.c_bag_replace.Location = new System.Drawing.Point(577, 12);
            this.c_bag_replace.Name = "c_bag_replace";
            this.c_bag_replace.Size = new System.Drawing.Size(171, 39);
            this.c_bag_replace.TabIndex = 4;
            this.c_bag_replace.Text = "Sid_Replace_Bag";
            this.c_bag_replace.UseVisualStyleBackColor = true;
            this.c_bag_replace.Click += new System.EventHandler(this.c_bag_open_Click);
            // 
            // c_cashIn
            // 
            this.c_cashIn.Location = new System.Drawing.Point(400, 12);
            this.c_cashIn.Name = "c_cashIn";
            this.c_cashIn.Size = new System.Drawing.Size(171, 39);
            this.c_cashIn.TabIndex = 5;
            this.c_cashIn.Text = "Sid_CashIn";
            this.c_cashIn.UseVisualStyleBackColor = true;
            this.c_cashIn.Click += new System.EventHandler(this.c_cashIn_Click);
            // 
            // c_ups
            // 
            this.c_ups.Location = new System.Drawing.Point(29, 235);
            this.c_ups.Name = "c_ups";
            this.c_ups.Size = new System.Drawing.Size(171, 39);
            this.c_ups.TabIndex = 6;
            this.c_ups.Text = "Sid_UPS";
            this.c_ups.UseVisualStyleBackColor = true;
            this.c_ups.Click += new System.EventHandler(this.c_ups_Click);
            // 
            // c_shutdown
            // 
            this.c_shutdown.Location = new System.Drawing.Point(29, 280);
            this.c_shutdown.Name = "c_shutdown";
            this.c_shutdown.Size = new System.Drawing.Size(171, 39);
            this.c_shutdown.TabIndex = 7;
            this.c_shutdown.Text = "Sid_Shutdown";
            this.c_shutdown.UseVisualStyleBackColor = true;
            this.c_shutdown.Click += new System.EventHandler(this.c_shutdown_Click);
            // 
            // c_gettotal
            // 
            this.c_gettotal.Location = new System.Drawing.Point(223, 12);
            this.c_gettotal.Name = "c_gettotal";
            this.c_gettotal.Size = new System.Drawing.Size(171, 39);
            this.c_gettotal.TabIndex = 8;
            this.c_gettotal.Text = "Sid_Totalbanknotes";
            this.c_gettotal.UseVisualStyleBackColor = true;
            this.c_gettotal.Click += new System.EventHandler(this.c_gettotal_Click);
            // 
            // c_identify
            // 
            this.c_identify.Location = new System.Drawing.Point(29, 103);
            this.c_identify.Name = "c_identify";
            this.c_identify.Size = new System.Drawing.Size(171, 40);
            this.c_identify.TabIndex = 9;
            this.c_identify.Text = "Sid_Identify";
            this.c_identify.UseVisualStyleBackColor = true;
            this.c_identify.Click += new System.EventHandler(this.c_identify_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(66, 411);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Force Open";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // c_barcode
            // 
            this.c_barcode.Location = new System.Drawing.Point(30, 194);
            this.c_barcode.Name = "c_barcode";
            this.c_barcode.Size = new System.Drawing.Size(170, 35);
            this.c_barcode.TabIndex = 11;
            this.c_barcode.Text = "Barcode";
            this.c_barcode.UseVisualStyleBackColor = true;
            this.c_barcode.Click += new System.EventHandler(this.c_barcode_Click);
            // 
            // c_bolsa_test
            // 
            this.c_bolsa_test.Location = new System.Drawing.Point(217, 356);
            this.c_bolsa_test.Name = "c_bolsa_test";
            this.c_bolsa_test.Size = new System.Drawing.Size(170, 35);
            this.c_bolsa_test.TabIndex = 12;
            this.c_bolsa_test.Text = "BOLSA TEST";
            this.c_bolsa_test.UseVisualStyleBackColor = true;
            this.c_bolsa_test.Click += new System.EventHandler(this.c_bolsa_test_Click);
            // 
            // c_bolsa
            // 
            this.c_bolsa.Location = new System.Drawing.Point(447, 364);
            this.c_bolsa.Name = "c_bolsa";
            this.c_bolsa.Size = new System.Drawing.Size(94, 20);
            this.c_bolsa.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(406, 367);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "#bolsa";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_bolsa);
            this.Controls.Add(this.c_bolsa_test);
            this.Controls.Add(this.c_barcode);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.c_identify);
            this.Controls.Add(this.c_gettotal);
            this.Controls.Add(this.c_shutdown);
            this.Controls.Add(this.c_ups);
            this.Controls.Add(this.c_cashIn);
            this.Controls.Add(this.c_bag_replace);
            this.Controls.Add(this.c_Respuesta);
            this.Controls.Add(this.c_Reset);
            this.Controls.Add(this.c_dll_version);
            this.Controls.Add(this.c_open);
            this.Name = "Form1";
            this.Text = "Probar FUNCINES DEL SID";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_open;
        private System.Windows.Forms.Button c_dll_version;
        private System.Windows.Forms.Button c_Reset;
        private System.Windows.Forms.TextBox c_Respuesta;
        private System.Windows.Forms.Button c_bag_replace;
        private System.Windows.Forms.Button c_cashIn;
        private System.Windows.Forms.Button c_ups;
        private System.Windows.Forms.Button c_shutdown;
        private System.Windows.Forms.Button c_gettotal;
        private System.Windows.Forms.Button c_identify;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button c_barcode;
        private System.Windows.Forms.Button c_bolsa_test;
        private System.Windows.Forms.TextBox c_bolsa;
        private System.Windows.Forms.Label label1;
    }
}

