﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;


namespace __ProbarSID
{
    public partial class Form1 : Form
    {
        int l_Reply = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void c_open_Click(object sender, EventArgs e)
        {
                
            
            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

           
           
            
            l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
            
        }

        private void c_dll_version_Click(object sender, EventArgs e)
        {
            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

            StringBuilder  l_cadena = new StringBuilder (64);

            l_Reply = SidLib.SID_GetVersion(l_cadena, 64);
            if(l_Reply== SidLib.SID_OKAY )
                       c_Respuesta.Text += "\r\n" + l_cadena.ToString() ;

            l_Reply = SidLib.SID_Initialize();
            c_Respuesta.Text += "\r\n"+l_Reply.ToString();

            l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
        }

        private void c_Reset_Click(object sender, EventArgs e)
        {
           

            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

             l_Reply = SidLib.SID_Reset((char)SidLib.RESET_ERROR); 
             if (l_Reply == SidLib.SID_OKAY)
                 c_Respuesta.Text += "\r\n" + "Reset Exitoso";
             else
                 c_Respuesta.Text += "\r\n" + "Error: "+ l_Reply.ToString();

             l_Reply = SidLib.SID_Close();
             c_Respuesta.Text += "\r\n" + l_Reply.ToString();
        }

        private void c_bag_open_Click(object sender, EventArgs e)
        {
            Byte [] key = new Byte[1];
            Byte [] sensor = new Byte [64];
            int l_porcentaje=0;


          
            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

            c_Respuesta.Text="Introduzca la clave";
            l_Reply = SidLib.SID_CheckCodeLockCorrect(10);
              //  c_Respuesta.Text = l_Reply.ToString();
                if (l_Reply == SidLib.SID_OKAY)
                {
                    c_Respuesta.Text="Sellando  Bolsa";
                      l_Reply = SidLib.SID_JoinBag(0);       
                    c_Respuesta.Text = l_Reply.ToString();

                    if (l_Reply != SidLib.SID_OKAY)
                        return;

                    do
                    {
                       // System.Threading.Thread.Sleep(5000);
                        l_Reply = SidLib.SID_JoinBagResult(ref l_porcentaje);
                        c_Respuesta.Text = l_Reply.ToString();

                    } while (l_Reply == SidLib.SID_PERIF_BUSY);



                    l_Reply = SidLib.SID_EnableToOpenDoor();
                    c_Respuesta.Text = l_Reply.ToString();

                    MessageBox.Show("abra la Boveda");
                    l_Reply = SidLib.SID_WaitDoorOpen();
                    c_Respuesta.Text = l_Reply.ToString();
                    if (l_Reply == SidLib.SID_OKAY)
                    {

                        l_Reply = SidLib.SID_WaitRemoveBag(10); /* Wait the remove of the bag*/
                        c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                        l_Reply = SidLib.SID_WaitReplaceBag(10); /* Wait the replacement of the bag*/
                        c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                        /*Alternately it is possible to wait for all the phases 
                            do{ /*Wait remove the bag 
                         l_Reply = SidLib.SID_UnitStatus( key,  sensor,  null,null,null);
                         * c_Respuesta.Text += "\r\n" + l_Reply.ToString();
                            } while( sensor[7] ==   0x02 );
                
                            do{ /*Wait a new bag insertion 
                         l_Reply = SidLib.SID_UnitStatus ( key,  sensor,  null,null,null);
                         * c_Respuesta.Text += "\r\n" + l_Reply.ToString();
                            }while( sensor[7] != 0x02);
                            */
                        l_Reply = SidLib.SID_WaitDoorClosed(5); /*Wait closing the door */
                        c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                        if (l_Reply == SidLib.SID_OKAY) ///check corect instlacion
                        {
                            l_Reply = SidLib.SID_UnitStatus( key,  sensor, null, null, null);
                            c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                            if (sensor[7] != 0)
                            {
                                MessageBox.Show("Revise la colocacion de la bolsa");

                                MessageBox.Show("Introduzca la clave");
                                l_Reply = SidLib.SID_CheckCodeLockCorrect(3);
                                c_Respuesta.Text = l_Reply.ToString();
                                if (l_Reply == SidLib.SID_OKAY)
                                {
                                    MessageBox.Show("abra la Boveda");
                                    l_Reply = SidLib.SID_WaitDoorOpen();
                                    c_Respuesta.Text = l_Reply.ToString();
                                    if (l_Reply == SidLib.SID_OKAY)
                                    {
                                        l_Reply = SidLib.SID_WaitReplaceBag(10); /* Wait the replacement of the bag*/
                                        c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                                        l_Reply = SidLib.SID_WaitDoorClosed(5); /*Wait closing the door */
                                        c_Respuesta.Text += "\r\n" + l_Reply.ToString();

                                    }
                                }

                            }
                        }

                    }
                }



                else
                {
                    c_Respuesta.Text += "Codigo incorecto o se agoto el tiempo de espera";
                }

                l_Reply = SidLib.SID_Close(); /*Disconnect from unit */
                c_Respuesta.Text += "\r\n" + l_Reply.ToString();
            
                    
        }

        private void c_cashIn_Click(object sender, EventArgs e)
        {
            Byte [] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            ushort[] l_totalNotes= new ushort [10];



            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();
          //  l_Reply = SidLib.SID_Initialize();
          //  c_Respuesta.Text += "\r\n" + l_Reply.ToString();

            l_Reply = SidLib.SID_UnitStatus( key, sensor, null, null, null);
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();

            if ( sensor[7] != 148)
            {
              /*  l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);
                foreach (ushort billetes in l_totalNotes)
                    c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;*/

               l_Reply = SidLib.SID_CashIn(0,1,1,false,0 ,0 );/*Capture the notes */

               short[] result = new short[1];
             while( SidLib.SID_WaitValidationResult(result) == SidLib.SID_PERIF_BUSY )

                  {
                      if (result[0] != 0)
                      {
                          c_Respuesta.Text += "\r\n" + result[0].ToString();
                          // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");


                          ; /* Wait the end of the previous command*/
                          SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                          //   result[0] = 0;
                      }
                    }
            }

          /*  l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);
            foreach (ushort billetes in l_totalNotes)
                c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;*/

                l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
        }

        private void c_ups_Click(object sender, EventArgs e)
        {
            bool Encendido=false;
            bool reserv1, reserv2;

            reserv1 = reserv2 = false;
            l_Reply=  SidLib.SID_UPSStatus(ref Encendido,ref reserv1,ref reserv2 );

            if (l_Reply == SidLib.SID_OKAY && Encendido)
            {
                c_Respuesta.Text += " UPs Encendido"; 
            }else

                c_Respuesta.Text += " UPs No Encontrado"; 

        }

        private void c_shutdown_Click(object sender, EventArgs e)
        {
            l_Reply = SidLib.SID_UPSShutDown(2);

            if (l_Reply == SidLib.SID_OKAY )
            {
                c_Respuesta.Text += " Reiniciando";
            }
            else

                c_Respuesta.Text += " UPs No Encontrado"; 
        }

        private void c_gettotal_Click(object sender, EventArgs e)
        {
          ushort [] l_totalNotes= new  ushort [10]  ;

            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

           l_Reply= SidLib.SID_GetCashTotalBag( l_totalNotes );
            foreach (ushort billetes in l_totalNotes )
               c_Respuesta.Text += "\r\n l_totalNotes[]= "+  billetes ;

  

            l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
        }

        private void c_identify_Click(object sender, EventArgs e)
        {
            DataTable l_datos=null;
            SidLib.Dll_Configuration l_configuration = new SidLib.Dll_Configuration();
            


            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

            l_Reply= SidLib.GetDLLVersion (ref l_configuration ,ref l_datos);
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();

            foreach (DataRow  linea in l_datos.Rows  )
                c_Respuesta.Text += "\r\n Datos[]= " + linea[1];
            if (l_configuration.Door_Sensor)
                c_Respuesta.Text += "\r\n Door Sensor Present ";
            if (l_configuration.Lock_Sensor )
                c_Respuesta.Text += "\r\n lock Sensor Present";
            if (l_configuration.Uv_Sensor )
                c_Respuesta.Text += "\r\n Uv SENSOr Present ";
            if (l_configuration.Magnetic_18Channel )
                c_Respuesta.Text += "\r\n MICR Present ";



            l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
            

        }

        private void button1_Click(object sender, EventArgs e)
        {

            l_Reply = SidLib.SID_Open(false);
            c_Respuesta.Text = l_Reply.ToString();

            MessageBox.Show("abra la puerta ahora");
            l_Reply = SidLib.SID_ForceOpenDoor();
            if (l_Reply == SidLib.SID_OKAY)
                MessageBox.Show("abra la puerta ahora");

            l_Reply = SidLib.SID_Close();
            c_Respuesta.Text += "\r\n" + l_Reply.ToString();
            


        }

        private void c_barcode_Click(object sender, EventArgs e)
        {

            StringBuilder l_barcode = new StringBuilder(128);
            short l_len =0;


          l_Reply =  SidLib.SID_Open(false);
          if (l_Reply == SidLib.SID_OKAY)
          {
              l_Reply = SidLib.SID_ReadBarcode(l_barcode, ref l_len, 30);
              if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED || l_Reply == SidLib .SID_DATATRUNC )
                  c_Respuesta.Text = l_barcode.ToString();
              else
                  c_Respuesta.Text = "No se pudo leer el codigo";

          }
                    SidLib.SID_Close();

                
        }

        private void c_bolsa_test_Click(object sender, EventArgs e)
        {
       
            StringBuilder l_barcode = new StringBuilder(128);
            string l_barcodeOK = "";
            short l_length = 128;
            int l_trys = 0;
            do
            {
                l_length = 128;
                if (l_trys>0)
                    c_Respuesta.Text += "\r\n" + "Intento " + l_trys;
                l_Reply = SidLib.SID_Open(false);

                l_Reply = SidLib.SID_ReadBarcode(l_barcode, ref l_length, 10);
              
                c_Respuesta.Text += "\r\n" +"Funcion Barcode: " + l_Reply.ToString();

                if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED || l_Reply == SidLib.SID_DATATRUNC)
                {
                    try
                    {
                        string l_barcodelimpio = l_barcode.ToString().Substring(0, l_barcode.ToString().Length - 5).Replace(",", "");
                        l_barcodeOK = l_barcodelimpio;
                        c_Respuesta.Text += "\r\n"+l_barcodelimpio;
                        
                    }
                    catch (Exception l_error)
                    {
                        c_Respuesta.Text += "\r\n"+"ERROR de LECTURA " + l_error.Message;
                        if (String.IsNullOrEmpty(l_barcode.ToString()))
                        {
                            string l_barcode2 = l_barcode.ToString().Replace(",", "");
                            c_Respuesta.Text += "\r\n"+ "\r\n" + l_barcode2;
                            
                        }
                        else
                        {
                           
                            c_Respuesta.Text += "\r\n"+ "NO Capturado";
                            
                        }

                    }

                }
                l_trys++;
                SidLib.SID_Close();
            } while (!ComprobacionEnvaseNuevo(l_barcodeOK) && l_trys < 3);

            if (l_trys > 3)
                c_Respuesta.Text += "\r\n" + "Introducir a Mano el Numero";
        }

        private bool ComprobacionEnvaseNuevo(string numeroSerieBOLSA)
        {
            if (numeroSerieBOLSA == "00000")
            {
                c_Respuesta.Text += "\r\n" + "Duplicado 000000";
                return false;
            }
            else if (numeroSerieBOLSA == c_bolsa.Text)
            {
                c_Respuesta.Text += "\r\n" + "Duplicado: "+ numeroSerieBOLSA;
                return false;
            }
            else
            return true;
        }
    }
}
