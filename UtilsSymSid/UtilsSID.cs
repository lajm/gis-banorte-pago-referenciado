﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UtilsSymSid
{
    public class UtilsSid
    {

         static string l_User ="gsiftp@symetry.online";
        static string l_Pass = "Gsi0089";
        static String l_Uri = "ftp://symetry.online/" ;

        public static string NombreBitacoraSid(DateTime p_DateTime)
        {
            string l_Nombre = ".\\Bitacoras\\Bitacora_" + p_DateTime.ToString( "MM_dd_yyyy" ) + ".txt";

            return l_Nombre;
        }

        static String  l_NombreFinal="";
        public static int MandarBitacoraAnterior( int p_NumeroDiasAntes, int p_NumeroEstacion, out string p_Error )
        {
            int l_R = 0;
            p_Error = "";
            String l_NombreComprimido = null;
            try
            {
                DateTime l_Fecha = DateTime.Now - new TimeSpan(p_NumeroDiasAntes, 0, 0, 0);
                
                String l_NombreBitacora = NombreBitacoraSid(l_Fecha);
                l_R = ComprimirArchivoGZ(l_NombreBitacora, out p_Error);
                if (l_R < 0)
                    return l_R;

                
                l_NombreComprimido = Path.GetFileName(Path.ChangeExtension(l_NombreBitacora, ".gz"));
                l_NombreFinal = p_NumeroEstacion.ToString("0000") + "_" + Path.GetFileName(l_NombreComprimido);
                File.Delete(l_NombreFinal);
                File.Move(l_NombreComprimido, l_NombreFinal );

                l_R = MandarArchivoFtp(l_Uri+ CrearDirectorio() +l_NombreFinal ,l_User ,l_Pass  , l_NombreFinal, out p_Error);

                
                if (l_R < 0)
                    return l_R;
            }
            catch (Exception E )
            {
                p_Error = "MandarBitacoraAnterior EXCEPTION - " + E.Message;
                l_R = -1;
            }
            finally
            {
                if (!String.IsNullOrEmpty(l_NombreFinal))
                {
                    try
                    {
                        Directory.CreateDirectory( ".\\ResguardoBitacoras" );
                        File.Move(l_NombreFinal , ".\\ResguardoBitacoras\\"+ l_NombreFinal );
                    }
                    catch (Exception E)
                    {
                        p_Error = "Error al Mover Bitacora a Resguardo: " + E.Message;
                        l_R = -1;
                    }
                    
                }
            }
            return l_R;
        }

        public static int
            MandarArchivoFtp(string p_Uri, string p_Usuario, string p_Contrasena, String p_Archivo, out string p_Error)
        {
            int l_R = 0;
            p_Error = "";

            try
            {
               

                using (WebClient client = new WebClient())
                {
                    
                    client.Credentials = new NetworkCredential(p_Usuario, p_Contrasena);
                    client.UploadFile(p_Uri  , p_Archivo);
                }
            }
            catch (Exception E)
            {
                p_Error = "EXCEPTION MandarArchivoFtp - " + E.Message;
            }
            return 0;
        }


        public static int ComprimirArchivoGZ( String p_Path, out string p_Error )
        {
            int l_R = 0;
            p_Error = "";

            try
            {
                FileInfo l_FileInfo = new FileInfo(p_Path);

                // Get the stream of the source file.
                using (FileStream inFile = l_FileInfo.OpenRead())
                {
                    // Prevent compressing hidden and 
                    // already compressed files.
                    if ((File.GetAttributes(l_FileInfo.FullName)
                        & FileAttributes.Hidden)
                        != FileAttributes.Hidden & l_FileInfo.Extension != ".gz")
                    {
                        // Create the compressed file.
                        using (FileStream outFile =
                                    File.Create(Path.GetFileNameWithoutExtension(l_FileInfo.FullName) + ".gz"))
                        {
                            using (GZipStream Compress =
                                new GZipStream(outFile,
                                CompressionMode.Compress))
                            {
                                // Copy the source file into 
                                // the compression stream.
                                inFile.CopyTo(Compress);

                                return 0;
                            }
                        }
                    }
                }
            }
            catch (Exception E)
            {
                l_R = -1;
                p_Error = "ComprimirArchivo EXCEPTION - " + E.Message;
            }

            return l_R;
        }

        public static String CrearDirectorio ()
        {
            String l_Mes_año = DateTime.Now.AddDays( -1 ).ToString( "MMMM" , CultureInfo.CreateSpecificCulture( "es-ES" ) );
            string l_directorio = DateTime.Now.AddDays( -1 ).Year + "_" + l_Mes_año.ToUpper( );
            try
            {
               
            

                WebRequest l_peticion = WebRequest.Create( l_Uri + l_directorio );
                l_peticion.Method = WebRequestMethods.Ftp.MakeDirectory;
                l_peticion.Credentials = new NetworkCredential( l_User , l_Pass );
                using ( var resp = (FtpWebResponse) l_peticion.GetResponse( ) )
                {
                   Console.WriteLine( resp.StatusCode.ToString( ));
                }

                return l_directorio + "/";
            }catch(Exception exx)
            {
                return l_directorio + "/";
            }

        }
    }
}
