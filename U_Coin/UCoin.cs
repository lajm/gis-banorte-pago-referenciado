﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using U_Coin;
using static System.Net.Mime.MediaTypeNames;

namespace SymetryDevices
{
    public class UCoin
    {
        public static decimal[] _DenominacionesMonedas
                        = new decimal[] { (decimal) 0.10, (decimal) 0.20
                                            , (decimal) 0.50, 1, 2, 5, 10, 20 };
        public static int 
            ObtenerTotales( byte p_Puerto, string p_Usuario, out decimal p_Total, out Dictionary<decimal, int> p_Detalle )
        {

            int l_Resultado = 0;
            try
            {

                p_Detalle = new Dictionary<decimal, int>();

                for (int i = 0; i < _DenominacionesMonedas.Length; i++)
                {
                    p_Detalle.Add(_DenominacionesMonedas[i], 0);
                }

                //bool stop;
                byte[] rcDescription = new byte[1024];
                UInt16[] l_DetalleDeposito = new UInt16[U_Dep.MAX_DEPOSIT_DOCTYPE];
                //Int32 l_TotalRechazadas = 0;
                byte[] l_Puerto = new byte[3];

                U_Dep l_aceptador = new U_Dep();
                int result;

                p_Total = (decimal)0.0;

                Array.Clear(l_DetalleDeposito, 0, l_DetalleDeposito.Length);

                result = l_aceptador.DLLU_DEP_ConnectAutoCOM(l_Puerto);


                if (result != U_Dep.U_DEP_OK)
                {

                    //MessageBox.Show( "Error Connect" );
                    return result;
                }
                //result = U_Dep.U_DEP_LogOut();

                ////LOGIN WITHOUT AUTENTICATION
                //result = U_Dep.U_DEP_LogIn( p_Usuario );

                //if ( result != U_Dep.U_DEP_OK )
                //{
                //   // MessageBox.Show( "Login" );
                //    return result;
                //}

                result = l_aceptador.DLLU_DEP_SetMode(U_Dep.U_DEP_MODE_ENABLE, 0x30); // notused reserved: 30H
                                                                                      //RESET PARTIAL
                if (result != U_Dep.U_DEP_OK)
                {
                    // MessageBox.Show( "Set Mode" );
                    return result;
                }

                //result = U_Dep.U_DEP_ResetPartialCounter();
                ////DEPOSIT
                //if ( result != U_Dep.U_DEP_OK )
                //{
                //    //MessageBox.Show( "U_DEP_SetMode - ERROR - " + result.ToString() );
                //    return result;
                //}

                byte[] l_Currency = System.Text.ASCIIEncoding.ASCII.GetBytes("MXN");

                result = l_aceptador.DLLU_DEP_ReadCashStatus( l_DetalleDeposito, l_Currency );

                //String l_Mensaje = l_DetalleDeposito[ 0 ] + " - " + l_DetalleDeposito[ 1 ] + " - " + l_DetalleDeposito[ 2 ] + " - " +
                //    l_DetalleDeposito[ 3 ] + " - " + l_DetalleDeposito[ 4 ] + " - " + l_DetalleDeposito[ 5 ] + " - " + l_DetalleDeposito[ 6 ] + " - ";

                //MessageBox.Show( l_Mensaje );

                int l_50c = l_DetalleDeposito[5];
                int l_1p = l_DetalleDeposito[6];
                int l_2p = l_DetalleDeposito[7];
                int l_5p = l_DetalleDeposito[8];
                int l_10p = l_DetalleDeposito[9];
                int l_20p = l_DetalleDeposito[10];
                Log("Paso 14");


                p_Detalle[ (decimal) 0.50 ] = l_50c;
                p_Detalle[ (decimal) 1 ] = l_1p;
                p_Detalle[ (decimal) 2 ] = l_2p;
                p_Detalle[ (decimal) 5 ] = l_5p;
                p_Detalle[ (decimal) 10 ] = l_10p;
                p_Detalle[ (decimal) 20 ] = l_20p;

                foreach ( decimal l_D in _DenominacionesMonedas )
                    p_Total += p_Detalle[l_D] * l_D;


                //SET MODE: DISABLE
                if ( result == U_Dep.U_DEP_OK )
                    result = l_aceptador.DLLU_DEP_SetMode( U_Dep.U_DEP_MODE_DISABLE, 0x30 );

                //if ( result != U_Dep.U_DEP_OK )
                //    MessageBox.Show( "Error U_DEP_SetMode U_DEP_MODE_DISABLE - " + result.ToString(), "ERROR" );
                //LOGIN WITHOUT AUTENTICATION
                result = l_aceptador.DLLU_DEP_LogOut();
                if ( result != U_Dep.U_DEP_OK )
                {
                    //MessageBox.Show( "U_DEP_Logout - ERROR - " + result.ToString() );
                    return result;
                }

                result = l_aceptador.DLLU_DEP_Disconnect();
                if ( result != U_Dep.U_DEP_OK )
                {
                    //MessageBox.Show( "U_DEP_Disconnect - ERROR - " + result.ToString() );
                    return result;
                }
            }
            catch ( Exception E )
            {
                Log( E.Message );
                p_Total = 0;
                p_Detalle = null;
            }

            return l_Resultado;
        }

        public static int
            Depositar( int p_Port, string p_Usuario, out decimal p_Total, out Dictionary<decimal, int> p_Detalle, int p_Timeout )
        {

            int l_Resultado = 0;

            try
            {
               
                Log("Paso 1");

                p_Detalle = new Dictionary<decimal, int>();

                for (int i = 0; i < _DenominacionesMonedas.Length; i++)
                {
                    p_Detalle.Add(_DenominacionesMonedas[i], 0);
                }

                Log("Paso 2");


                byte[] rcDescription = new byte[1024];
                UInt16[] l_DetalleDeposito = new UInt16[U_Dep.MAX_DEPOSIT_DOCTYPE];
                UInt16[] l_TotalRechazadas = new UInt16[1] ;  //DCS 22Sep2016 short l_TotalRechazadas = 0;
                //byte l_Puerto = (byte)p_Port;
                U_Dep l_aceptador = new U_Dep();

                int result = -1;

                p_Total = (decimal)0.0;
                Log("Paso 3");

                
                //U_Dep.U_DEP_Disconnect();
                byte []  l_server = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid");

                byte[] l_Puerto = new byte [3];

               result = l_aceptador.DLLU_DEP_ConnectAutoCOM(l_Puerto);

            // result = l_aceptador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER,(byte)p_Port , null, 0);

                Log("Paso 4");

                if (result != U_Dep.U_DEP_OK)
                {
                    return result;
                }
                Log("Paso 5");

                // result = U_Dep.U_DEP_ResetSoftware();
                //result = U_Dep.U_DEP_ResetCoin_CashStatus();
                Log("Paso 6");
                result = l_aceptador.DLLU_DEP_LogOut();

                Log("Paso 7");
                byte[] l_usuario = System.Text.Encoding.UTF8.GetBytes(p_Usuario);
                //LOGIN WITHOUT AUTENTICATION
                result = l_aceptador.DLLU_DEP_LogIn(l_usuario);
                //SET MODE: ENABLE Device is enabled to accept document, validate

                Log("Paso 8");
                if (result != U_Dep.U_DEP_OK)
                {
                    // MessageBox.Show( "U_DEP_LogIn - ERROR - " + result.ToString() );
                    return result;
                }

                result = l_aceptador.DLLU_DEP_SetMode(U_Dep.U_DEP_MODE_ENABLE, 0x30); // notused reserved: 30H
                Log("Paso 9");
                //RESET PARTIAL
                if (result != U_Dep.U_DEP_OK)
                {
                    //MessageBox.Show( "U_DEP_SetMode - ERROR - " + result.ToString() );
                    return result;
                }

                result = l_aceptador.DLLU_DEP_ResetPartialCounter();
                Log("Paso 10");
                //DEPOSIT
                if (result != U_Dep.U_DEP_OK)
                {
                   Log( "U_DEP_Reset Parcial - ERROR - " + result.ToString() );

                  int l_result =  l_aceptador.DLLU_DEP_Disconnect( );
                    Log( "Paso 20" );
                    if ( l_result != U_Dep.U_DEP_OK )
                    {
                        Log( "U_DEP_Disconnect - ERROR - " + result.ToString( ) );
                        return l_result;
                    }

                    

                    return result;
                }

                

                result = l_aceptador.DLLU_DEP_Deposit_Coin_Start(null
                                , l_DetalleDeposito, l_TotalRechazadas
                                ,(uint) (p_Timeout * 1000));

                Log("Paso 11");

                //String l_DetalleStr = l_DetalleDeposito[ 0 ] + " " + l_DetalleDeposito[ 1 ] + " "
                //    + l_DetalleDeposito[ 2 ] + " " + l_DetalleDeposito[ 3 ] + " "
                //    + l_DetalleDeposito[ 4 ] + " " + l_DetalleDeposito[ 5 ] + " "
                //    + l_DetalleDeposito[ 6 ] + " " + l_DetalleDeposito[ 7 ];

                String l_DetalleStr = l_DetalleDeposito[0].ToString("X8") + " " + l_DetalleDeposito[1].ToString("X8") + " "
                    + l_DetalleDeposito[2].ToString("X8") + " " + l_DetalleDeposito[3].ToString("X8") + " "
                    + l_DetalleDeposito[4].ToString("X8") + " " + l_DetalleDeposito[5].ToString("X8") + " ";
                Log("Paso 12 " + l_TotalRechazadas);

                //File.AppendAllText( "depositos.txt", DateTime.Now.ToShortTimeString() + " - " +  l_DetalleStr + Environment.NewLine );
                Log("Paso 13");

                int l_50c = l_DetalleDeposito[5] ;
                int l_1p = l_DetalleDeposito[6];
                int l_2p = l_DetalleDeposito[7];
                int l_5p = l_DetalleDeposito[8];
                int l_10p = l_DetalleDeposito[9];
                int l_20p = l_DetalleDeposito[10];
                Log("Paso 14");

                p_Detalle[(decimal)0.50] = l_50c;
                p_Detalle[(decimal)1] = l_1p;
                p_Detalle[(decimal)2] = l_2p;
                p_Detalle[(decimal)5] = l_5p;
                p_Detalle[(decimal)10] = l_10p;
                p_Detalle[(decimal)20] = l_20p;
                Log("Paso 15");

                foreach (decimal l_D in _DenominacionesMonedas)
                    p_Total += p_Detalle[l_D] * l_D;
                Log("Paso 16");

                //If Reject FULL, it's possible to empty reject box and continue with deposit
                if (result == U_Dep.U_DEP_REJECT_FULL)
                {
                   Log("Salida de rechazo llena" + "ERROR");
                }
                else if ( result == U_Dep.U_DEP_BAGFULL_COUNT_WRN )
                {
                    Log( "UCOIN LLENO Código de error:" + result.ToString( ) + "ERROR" );

                    int l_result=  l_aceptador.DLLU_DEP_Disconnect( );
                    Log( "Paso 20" );
                    if ( l_result != U_Dep.U_DEP_OK )
                    {
                        Log( "U_DEP_Disconnect - ERROR - " + result.ToString( ) );
                        return l_result;
                    }

                    return result;
                }
                else if (result != U_Dep.U_DEP_OK)
                {
                    Log( "Código de error:" + result.ToString() + "ERROR");

                    int l_result =  l_aceptador.DLLU_DEP_Disconnect( );
                    Log( "Paso 20" );
                    if ( l_result != U_Dep.U_DEP_OK )
                    {
                        Log( "U_DEP_Disconnect - ERROR - " + result.ToString( ) );
                        return l_result;
                    }

                    return result;
                }
                Log("Paso 17");


                //SET MODE: DISABLE
                if (result == U_Dep.U_DEP_OK)
                    result = l_aceptador.DLLU_DEP_SetMode(U_Dep.U_DEP_MODE_DISABLE, 0x30);
                Log("Paso 18");

                if (result != U_Dep.U_DEP_OK)
                    Log("Error U_DEP_SetMode U_DEP_MODE_DISABLE - " + result.ToString() + " ERROR");
                //LOGIN WITHOUT AUTENTICATION
                result = l_aceptador.DLLU_DEP_LogOut();
                Log("Paso 19");
                if (result != U_Dep.U_DEP_OK)
                {
                    Log("U_DEP_Logout - ERROR - " + result.ToString());
                    return result;
                }

                result = l_aceptador.DLLU_DEP_Disconnect();
                Log("Paso 20");
                if (result != U_Dep.U_DEP_OK)
                {
                    Log("U_DEP_Disconnect - ERROR - " + result.ToString());
                    return result;
                }

            }
            catch (Exception exp)
            {
                p_Total = 0;
                p_Detalle = null;

                Log("Exepcion encontrada Ucoin_depositar() "+ exp.Message);
            }
            

                return l_Resultado;
           
        }

        #region - DCS 22Sep2016 - Realiza el retiro de monedas
        public static int cRetiro( int p_Port )
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte) p_Port;
            int result;
            U_Dep l_Depositador = new U_Dep();

            //DCS 26Sep2016 Se comentarea para validar si funciona U_Dep.U_DEP_Disconnect();

            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid");

            result = l_Depositador.DLLU_DEP_Connect( U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0 );

            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }


            #region - DCS 22Sep2016 - Realiza la apertura de la bobeda y el reset del estatus dinero

            result = l_Depositador.DLLU_DEP_OpenSafe();
            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_ResetCashStatus();

            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }
            #endregion

            //result = U_Dep.U_DEP_ResetSoftware(); //DCS 26Sep2016 Es para que no se tenga problemas con los demas depositos

            result = l_Depositador.DLLU_DEP_Disconnect();
            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }

            return l_Resultado;
        }
        #endregion        

        #region - DCS 23Sep2016 - Realiza el Reset Software
        public static int cRealizaResetSoftware( int p_Port )
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte) p_Port;
            int result;
            StringBuilder l_version = new StringBuilder(31);

            U_Dep l_Depositador = new U_Dep();

            //DCS 26Sep2016 Se comentarea para validar si funciona U_Dep.U_DEP_Disconnect();

            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid");

            result = l_Depositador.DLLU_DEP_Connect( U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto,l_Usuario, 0 );

            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }

           
            result = l_Depositador.DLLU_DEP_ResetSoftware();

            if ( result != U_Dep.U_DEP_OK ) //regresa código de error
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_Disconnect();
            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }
            return l_Resultado;
        }
        #endregion



        #region - DCS 23Sep2016 - Realiza el Reset Hardware
        public static int cRealizaResetHardware( int p_Port )
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte) p_Port;
            int result;
            StringBuilder l_version = new StringBuilder(31);
 
  
                      U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect( U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0 );
            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }
result = l_Depositador.DLLU_DEP_ResetHardware();

            if ( result != U_Dep.U_DEP_OK ) //regresa código de error
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_Disconnect();
            if ( result != U_Dep.U_DEP_OK )
            {
                return result;
            }
            return l_Resultado;
        }
        #endregion

        #region - Misael 07Feb2017 - Realiza Apertura de Bovedad
        public static int cOpenSafe(int p_Port)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;

            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

        result = l_Depositador.DLLU_DEP_OpenSafe();
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

           
            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            return l_Resultado;
        }
        #endregion

        #region - Misael 07Feb2017 - Realiza Test Luz ON
        public static int cTestLigthON(int p_Port)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;


            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_Light(U_Dep.U_DEP_LIGHT_INPUT, U_Dep.U_DEP_LIGHT_ON);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }


            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            return l_Resultado;
        }
        #endregion

        #region - Misael 07Feb2017 - Realiza Test Luz OFF
        public static int cTestLigthOFF(int p_Port)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;

            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }


            result = l_Depositador.DLLU_DEP_Light(U_Dep.U_DEP_LIGHT_INPUT, U_Dep.U_DEP_LIGHT_OFF);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }


            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            return l_Resultado;
        }
        #endregion

        #region - Misael 07Feb2017 - Obtiene FW validator version
        public static int cgetValidator(int p_Port, out String o_id_validacion)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;
            StringBuilder l_version = new StringBuilder(31);
        
            o_id_validacion = "";
            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            //result = l_Depositador.DLL_U_DEP_GetID_Validation(  l_version);
            //if (result != U_Dep.U_DEP_OK)
            //{
               
            //    return result;
            //}

            o_id_validacion = l_version.ToString();

            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {
                
                return result;
            }

      
            return l_Resultado;
        }
        #endregion

        #region - Misael 07Feb2017 - Obtiene FW validator version
        public static int cgetMaster(int p_Port, out String o_id_Master)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;
            StringBuilder l_version = new StringBuilder(31);

            o_id_Master= "";

            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }


            if (result != U_Dep.U_DEP_OK)
            {

                return result;
            }

            byte[] l_Version = new byte[256];

            result = l_Depositador.DLLU_DEP_GetID_Master(l_Version);
            if (result != U_Dep.U_DEP_OK)
            {

                return result;
            }

            o_id_Master = l_version.ToString();

            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {

                return result;
            }


            return l_Resultado;
        }
        #endregion

        #region - Misael 16Feb2017 - Status UCOIN
        public static int cgetStatus(int p_Port)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            byte[] l_photo = new byte[4];
            byte[] l_photo_validator = new byte[4];
            StringBuilder l_version = new StringBuilder(31);
            int[] l_status = new int[1];
            int result;

            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            byte[] l_Version = new byte[256];

            result = l_Depositador.DLLU_DEP_GetID_Master( l_Version );
            if ( result != U_Dep.U_DEP_OK )
            {

                return result;
            }


            result = l_Depositador.DLLU_DEP_GetStatus(l_photo, l_status, l_photo_validator);
            if (result != U_Dep.U_DEP_OK)
            {

                return result;
            }
            l_Resultado = l_status[0];


            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {

                return result;
            }


            return l_Resultado;
        }
        #endregion

        #region - MCG 16-02-2017 - Realiza el Reset Software
        public static int cIncializarUCOIN(int p_Port)
        {
            int l_Resultado = 0;

            byte l_Puerto = (byte)p_Port;
            int result;
            byte[] l_version = new byte[1024];



            //DCS 26Sep2016 Se comentarea para validar si funciona U_Dep.U_DEP_Disconnect();

            U_Dep l_Depositador = new U_Dep();
            byte[] l_Usuario = System.Text.ASCIIEncoding.ASCII.GetBytes("Sid ");

            result = l_Depositador.DLLU_DEP_Connect(U_Dep.U_DEP_CONNECTMODE_SER, l_Puerto, l_Usuario, 0);
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_GetID_Master(l_version);

            if (result != U_Dep.U_DEP_OK) //regresa código de error
            {
                return result;
            }


            result = l_Depositador.DLLU_DEP_ResetHardware();

            if (result != U_Dep.U_DEP_OK) //regresa código de error
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_ResetSoftware();

            if (result != U_Dep.U_DEP_OK) //regresa código de error
            {
                return result;
            }

            result = l_Depositador.DLLU_DEP_Disconnect();
            if (result != U_Dep.U_DEP_OK)
            {
                return result;
            }
            return l_Resultado;
        }
        #endregion

        public static void Log( String p_Text )
        {
            File.AppendAllText( "log.txt", DateTime.Now.ToShortTimeString() + " - " +  p_Text + Environment.NewLine );
        }
    }
}
