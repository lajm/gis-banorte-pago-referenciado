#ifndef _U_ONE_RC_H_
#define _U_ONE_RC_H_

//
// doc results
//
#ifndef DOCTYPE_1
#define DOCTYPE_CHECK              	        0x20
#define DOCTYPE_1               	        0x21
#define DOCTYPE_2               	        0x22
#define DOCTYPE_5               	        0x23
#define DOCTYPE_10              	        0x24
#define DOCTYPE_20              	        0x25
#define DOCTYPE_50              	        0x26
#define DOCTYPE_100             	        0x27
#define DOCTYPE_200             	        0x28
#define DOCTYPE_500             	        0x29
#define DOCTYPE_1000            	        0x2A
#define DOCTYPE_2000				        0x2B
#define DOCTYPE_5000				        0x2C
#define DOCTYPE_10000					    0x2D
#endif
#define DOC_NO_DATA							0x95

// 0x5x: errors during visible (gray) recognition
#define VIS_BAD_CLASSIFICATION						0x50  // neither near nor far from a template
#define VIS_REJECTED								0x51  // unknown
#define VIS_COLOR_ERR								0x52
#define VIS_DOUBLE  								0x53
#define VIS_DARK									0x54
#define VIS_BAD_BARCODE								0x55

// 0x6x: errors during IR recognition
#define IR_REJECTED									0x60
#define UV_REJECTED									0x6A
#define SPECTRUM_REJECTED							0x6B
// 0x7x: magnetic check
#define MG_CHECK_FAILED								0x70
#define MG_DIRTY									0x71
// 0x8x: pre-fitness errors
#define DOC_BAD_DIMENSIONS							0x80  // before classification
#define DOC_BAD_SHAPE								0x81

//0x9x
#define PARAM_ERROR									0x90

//0xAx
#define REJECT_FORCE								0xA0
#define ACQ_DOC_TOO_LONG							0xA1
#define DOC_ON_PATH_TOO_EARLY						0xA2
#define SKEW_ERR									0xA3
#define INVALID_DOC									0xA4
#define SURPLUS_AUMOUNT_IN_BAG						0xA5


//
// retcodes
//
// 0x00
#define U_DEP_OK							0x00
#define U_DEP_BAG_NOT_EMPTY_WRN				0x02
#define U_DEP_LOGIN_ALREADY_PRESENT_WRN		0x05
#define U_DEP_SAFE_NOT_OPEN_WRN				0x06
#define U_DEP_CANT_FEED						0x07
#define U_DEP_DISABLED_WRN					0x08
#define U_DEP_INPUT_EMPTY					0x09
#define U_DEP_SN_BAG_NOT_PRESENT_WRN		0x0A
#define U_DEP_AMOUNT_BAG_NEAREST_FULL_WRN	0x0B
#define U_DEP_BAG_NOT_PRESENT_WRN			0x0C
#define U_DEP_BAGFULL_DIM_WRN				0x0D
#define U_DEP_BAGFULL_AMOUNT_WRN			0x0E
#define U_DEP_BAGFULL_COUNT_WRN				0x0F

// 0x10
#define U_DEP_JAM_FOTO_TRASP_A_ERR			0x10
#define U_DEP_JAM_FOTO_TRASP_B_ERR			0x11
#define U_DEP_JAM_FOTO_TRASP_C_ERR			0x12
#define U_DEP_JAM_FOTO_TRASP_D_ERR			0x13
#define U_DEP_VALIDATION_TIMEOUT			0x14
#define U_DEP_JAM_PATH_ERROR_ERR			0x15
#define U_DEP_JAM_DOC_PATH_ERR				0x16
#define U_DEP_MOTOR_TRANSPORT_BLOCKED_ERR	0x17
#define U_DEP_MOTOR_PATH_BLOCKED_ERR		0x18
#define U_DEP_VALIDATION_TIME_TOO_LONG		0x19
#define U_DEP_INPUT_NOT_EMPTY				0x1A
#define U_DEP_TEMPLATE_NOT_PRESENT			0x1B
#define U_DEP_CURRRENCY_NOT_PRESENT			0x1C
#define U_DEP_JAM_USCITA					0x1D
#define U_DEP_SAFE_OPEN_ERR					0x1E
#define U_DEP_POWERFAIL_ERR					0x1F

// 0x20
#define U_DEP_DOC_NOT_PRESENT_ON_EXTERNAL_SAFE_SENSOR	0x20
#define U_DEP_JAM_ON_EXTERNAL_SAFE_SENSOR				0x21
#define U_DEP_DOC_NOT_PRESENT_ON_INTERNAL_SAFE_SENSOR	0x22
#define U_DEP_JAM_ON_INTERNAL_SAFE_SENSOR				0x23
#define U_DEP_UNEXPECTED_DOC_ON_PATH					0x24
#define U_DEP_JAM_ON_DOC_IN_HOPPER_SENSOR				0x25
#define U_DEP_DOC_NOT_PRESENT_ON_DOC_IN_HOPPER_SENSOR	0x26
#define U_DEP_COVER_OPEN								0x2A
#define U_DEP_PATH_OPEN									0x2B
#define U_DEP_REJECT_PATH_OPEN							0x2C
#define U_DEP_SAFE_PATH_OPEN							0x2D
#define U_DEP_HOPPER_NOT_CLOSED_ERR						0x2E
#define U_DEP_HOPPER_NOT_OPEN_ERR						0x2F

// 0x30
#define U_DEP_PWM_TARATURA_ERR				0x30
#define U_DEP_COMP_TARTURA_SOTT_ERR			0x31
#define U_DEP_COMP_TARTURA_DIFF_ERR			0x32
#define U_DEP_COMP_TARTURA_MOLT_ERR			0x33


// 0x90
#define U_DEP_COMMAND_ERR						0x90
#define U_DEP_PARAMETER_ERR						0x91
#define U_DEP_COMMAND_NOT_EXE_ERR				0x93
#define U_DEP_PERIPHERAL_NOT_CONNECTED			0x94
#define U_DEP_UNKNOWN_USER						0x95
#define U_DEP_WRONG_PASSWORD					0x96
#define U_DEP_RC_NO_USER						0x97
#define U_DEP_BUSY								0x98
#define U_DEP_REJECT_NOT_EMPTY					0x99
#define U_DEP_REJECT_FULL						0x9A
#define U_DEP_BAG_TO_REMOVE_ON_SAFE				0x9B
#define U_DEP_LIFT_IN_WRONG_POSITION			0x9C


// 0xA0
#define U_DEP_EXT_FLASH_ERASE_ERR				0xA0
#define U_DEP_EXT_FLASH_PROGRAM_ERR				0xA1
#define U_DEP_EXT_FLASH_ON_ERROR_ERR			0xA2
#define U_DEP_EEPROM_WRITE_ERR					0xA4
#define U_DEP_EEPROM_READ_ERR					0xA5
#define U_DEP_FLASH_WRITE_ERR					0xA6
//0xB0
#define U_DEP_SOLDER_MICRO_BAG_ANOMALY_ERR		0xB0
#define U_DEP_SOLDER_BAG_OPEN_ERR				0xB1
#define U_DEP_SOLDER_BAG_CLOSE_ERR				0xB2
#define U_DEP_SOLDER_TIMER_ERR					0xB3
#define U_DEP_SOLDER_PWM_LIMIT_UP_ERR			0xB4
#define U_DEP_SOLDER_PWM_LIMIT_DOWN_ERR			0xB5
#define U_DEP_SOLDER_V_UP_ERR					0xB6
#define U_DEP_SOLDER_V_DOWN_ERR					0xB7
#define U_DEP_SOLDER_CURR_LIMIT_ERR				0xB8
#define U_DEP_SOLDER_ON_ERR						0xB9
#define U_DEP_MICRO_LIFT_ANOMALY_ERR			0xBA
#define U_DEP_RESET_LIFT_ERR					0xBB
#define U_DEP_LIFT_MOVEMENT_ERR					0xBC
// 0xC0
#define U_DEP_DP988_ANSWER_ERR					0xC0
#define U_DEP_SAFE_ANSWER_ERR					0xC1
#define U_DEP_SAFE_SHUTTER_OPEN_ERR				0xCC
#define U_DEP_SAFE_COMBINATION_ERR				0xCD
#define U_DEP_SAFE_LOCK_OPEN_ERR				0xCE
#define U_DEP_SAFE_BOLTOPEN_ERR					0xCF


// 0xD0
// 0xE0
// 0xF0
#define U_DEP_SYSTEM_ERR						0xFF

#define U_DEP_FILEOPEN_ERR					-10
#define U_DEP_DOWNLOAD_ERR					-11
#define U_DEP_BAD_FILE_FORMAT_ERR			-12
#define U_DEP_DLL_COINS_DISABLED			-13

#define U_DEP_EVENT_CREATE_ERROR			-30
#define U_DEP_THREAD_CREATE_ERROR			-31

#define U_DEP_DLL_BAD_DEPOSIT_PARAM			-41
#define U_DEP_DLL_BAD_DISPENCE_PARAM		-42
#define U_DEP_DLL_BAD_PARAM					-43



#define U_DEP_LOG_DISABLED					-50

#define U_DEP_COM_OPEN_ERR					-1010
#define U_DEP_COM_CLOSE_ERR					-1011
#define U_DEP_COM_WRITE_ERR					-1012
#define U_DEP_COM_READ_ERR					-1013
#define U_DEP_COM_NOT_OPEN_ERR				-1014
#define U_DEP_COM_ON_ERR					-1015

#define U_DEP_NET_OPEN_ERR					-1040
#define U_DEP_NET_CLOSE_ERR					-1041
#define U_DEP_NET_WRITE_ERR					-1042
#define U_DEP_NET_READ_ERR					-1043
#define U_DEP_NET_READ_NODATA				-1044


#endif//_U_ONE_RC_H_

