#ifndef _U_DEP_H_
#define _U_DEP_H_

// To align structure dimension
#pragma warning( disable : 4121 )  // C4121 : alignment of a member was sensitive to packing
#pragma pack(4)

#ifndef INOUT
#define INOUT
#endif

// Connection mode
#define U_DEP_CONNECTMODE_SER			0
#define U_DEP_CONNECTMODE_NET			1
#define U_DEP_REMOTE_PORT				1982 // greater than 1024

// Welding module presence
#define WELDING_MODULE_NOT_PRESENT		'0'
#define WELDING_MODULE_PRESENT			'1'

// Feeder module presence
#define FEEDER_MODULE_NOT_PRESENT		'0'
#define FEEDER_MODULE_PRESENT			'1'

// Set mode
#define U_DEP_MODE_DISABLE				'0' // disable banknote feeding
#define U_DEP_MODE_ENABLE				'1' // enable banknote feeding
#define U_DEP_MODE_ALTERNATE			'2'	//alternate
#define U_DEP_MODE_COUNTING				'3'	//counting without validation
#define U_DEP_MODE_ONE_BY_ONE			'4'	//one by one with validation
#define U_DEP_MODE_ONLY_VALIDATION		'5'	//no doc acquisition
#define U_DEP_MODE_DATABASE_REJ			'6'	//validation, no deposit: database of all docs. All doc will be rejected
#define U_DEP_MODE_DATABASE_REJECT		'7'	//validation + deposit: database of rejected docs
#define U_DEP_MODE_DATABASE_DEP			'8'	//validation + deposit: database of all docs. All doc will be putted on hopper


#define FW_LOG_MAXLEN					(16*1024*1024) // 16MB

// save bmp
#define SAVE_GRAY						0
#define SAVE_RGB						1

//Deposito
#define MAX_DEPOSIT_DOCTYPE				21

//U_DEP_CalibCompensation
#define CALIBRATION_BLACK				'B'
#define CALIBRATION_WHITE_STEP_1		'w'
#define CALIBRATION_WHITE				'W'
#define CALIBRATION_SIDE_FRONT			'F'
#define CALIBRATION_SIDE_REAR			'R'
#define CALIBRATION_SIDE_ALL			'A'


//U_DEP_SealBag
#define SEAL_ENABLED					'0'
#define SEAL_DISABLED					'1'

//U_DEP_VerifySafeDoor
#define SAFE_CLOSED						'0'
#define SAFE_OPEN						'1'

//U_DEP_VerifyBagPresent
#define BAG_NOT_PRESENT					'0'
#define BAG_PRESENT						'1'

//U_DEP_DW_Firmware
#define MODULE_VALIDATOR				'D'
#define MODULE_MASTER					'M'

//U_DEP_SetupBagNumber
#define SETUP_SET						'S'
#define SETUP_GET						'G'

//U_DEP_SetupMaxBagAmount
#define MAX_BAG_AMOUNT_DISABLED			'0'
#define MAX_BAG_AMOUNT_ENABLED			'1'

//U_DEP_SetupConsole
#define U_DEP_CONSOLE_DISABLED			'0'
#define U_DEP_CONSOLE_ENABLED			'1'

//U_DEP_User_Read
#define U_DEP_SERVICE					'T'
#define U_DEP_ADMIN						'A'
#define U_DEP_CIT						'C'
#define U_DEP_SUPERUSER					'S'
#define U_DEP_USER						'U'
#define U_DEP_USER_NOT_PRESENT			'0'
#define U_DEP_USER_PRESENT				'1'
#define U_DEP_USER_ERASED				'2'

//U_DEP_SetupLanguage
#define LANGUAGE_ITA					'0'
#define LANGUAGE_ENG					'1'
#define LANGUAGE_FRA					'2'
#define LANGUAGE_DEU					'3'

//U_DEP_View_LogIn
#define USER_NOT_PRESENT				'0'
#define USER_PRESENT					'1'

//U_DEP_VerifyNewEvent
#define EVENT_NONE						0
#define EVENT_FEED						'X'
#define EVENT_RESET_HW					'R'
#define EVENT_START_WELDING				'W'
#define EVENT_END_WELDING				'w'
#define EVENT_OPEN_SAFE					'O'
#define EVENT_CLOSE_SAFE				'C'
#define EVENT_LOGIN						'L'
#define EVENT_LOGOUT					'l'
#define EVENT_TAKE_OFF_BAG				'S'
#define EVENT_MODE						'E'

//U_DEP_Light
#define U_DEP_LIGHT_INPUT				'I'
#define U_DEP_LIGHT_REJECT				'R'
#define U_DEP_LIGHT_OFF					'0'
#define U_DEP_LIGHT_ON					'1'


//U_DEP_Erase_StringToPrint
//U_DEP_Get_StringToPrint
//U_DEP_Set_StringToPrint
#define U_DEP_STRING_TO_PRINT_1			'1'
#define U_DEP_STRING_TO_PRINT_2			'2'
#define U_DEP_STRING_TO_PRINT_3			'3'
#define U_DEP_STRING_TO_PRINT_4			'4'
#define U_DEP_STRING_TO_PRINT_5			'5'

//U_DEP_VerifyNewDoc
#define U_DEP_NO_NEW_DOC				'0'
#define U_DEP_NEW_DOC					'1'

//U_DEP_CardReader_ReadData
#define U_DEP_CARD_READER_TRACK1		'1'
#define U_DEP_CARD_READER_TRACK2		'2'
#define U_DEP_CARD_READER_TRACK3		'3'


#define DATA_LEN						500
typedef struct tag_dati_u_bundle
{
	//Trasp
	BYTE Trasp_A[DATA_LEN];
	int  Trasp_A_Len;
	BYTE Trasp_B[DATA_LEN];
	int  Trasp_B_Len;
	BYTE Trasp_C[DATA_LEN];
	int  Trasp_C_Len;
	BYTE Trasp_D[DATA_LEN];
	int  Trasp_D_Len;
	//Rifl
	BYTE Rifl_UP_A[DATA_LEN];
	int  Rifl_UP_A_Len;
	BYTE Rifl_UP_B[DATA_LEN];
	int  Rifl_UP_B_Len;
	BYTE Rifl_UP_C[DATA_LEN];
	int  Rifl_UP_C_Len;
	BYTE Rifl_UP_D[DATA_LEN];
	int  Rifl_UP_D_Len;
	BYTE Rifl_DOWN_E[DATA_LEN];
	int  Rifl_DOWN_E_Len;
	BYTE Rifl_DOWN_F[DATA_LEN];
	int  Rifl_DOWN_F_Len;
	BYTE Rifl_DOWN_G[DATA_LEN];
	int  Rifl_DOWN_G_Len;
	BYTE Rifl_DOWN_H[DATA_LEN];
	int  Rifl_DOWN_H_Len;
	//Color
	BYTE Color_UP_Red[DATA_LEN];
	int  Color_UP_Red_Len;
	BYTE Color_UP_Blu[DATA_LEN];
	int  Color_UP_Blu_Len;
	BYTE Color_UP_Green[DATA_LEN];
	int  Color_UP_Green_Len;
	BYTE Color_DOWN_Red[DATA_LEN];
	int  Color_DOWN_Red_Len;
	BYTE Color_DOWN_Blu[DATA_LEN];
	int  Color_DOWN_Blu_Len;
	BYTE Color_DOWN_Green[DATA_LEN];
	int  Color_DOWN_Green_Len;
	//Spectrum
	BYTE Spectrum_UP_Red[DATA_LEN];
	int  Spectrum_UP_Red_Len;
	BYTE Spectrum_UP_Blu[DATA_LEN];
	int  Spectrum_UP_Blu_Len;
	BYTE Spectrum_DOWN_Red[DATA_LEN];
	int  Spectrum_DOWN_Red_Len;
	BYTE Spectrum_DOWN_Blu[DATA_LEN];
	int  Spectrum_DOWN_Blu_Len;
	//Magnetic
	BYTE Magnetic_A[DATA_LEN];
	int  Magnetic_A_Len;
	BYTE Magnetic_B[DATA_LEN];
	int  Magnetic_B_Len;
	BYTE Magnetic_C[DATA_LEN];
	int  Magnetic_C_Len;
	BYTE Magnetic_D[DATA_LEN];
	int  Magnetic_D_Len;
	//UV
	BYTE UV[DATA_LEN];
	int  UV_Len;

}dati_u_bundle;


#define CURRENCY_LEN		4

typedef void (__stdcall PERCENTCALLBACK)(IN DWORD pPercent);
typedef PERCENTCALLBACK *LPPERCENTCALLBACK;

typedef int(__stdcall *U_DEP_CALLBACK)(void);

#ifdef __cplusplus
extern "C" {
#endif

int __stdcall U_DEP_Connect(IN BYTE mode, IN BYTE comport, IN char *servername, IN DWORD serverport);
int __stdcall U_DEP_ConnectAutoCOM(OUT LPBYTE p_comport);
int __stdcall U_DEP_Disconnect(void);
int __stdcall U_DEP_GetID(OUT char ID[30]);
int __stdcall U_DEP_GetID_Master(OUT char ID[30]);
int __stdcall U_DEP_GetID_Validation(OUT char ID_VAL[30]);
int __stdcall U_DEP_DLL_GetRelease(OUT char release[34]);
int __stdcall U_DEP_GetStatus(OUT BYTE photo[4], OUT int *rc_val, OUT BYTE photo_val[4]);
int __stdcall U_DEP_ResetSoftware(void);
int __stdcall U_DEP_ResetHardware(void);
int __stdcall U_DEP_LogIn(IN BYTE user[10+1]);
int __stdcall U_DEP_LogIn_And_Password(IN BYTE user[10 + 1], IN BYTE password[5 + 1]);
int __stdcall U_DEP_LogOut(void);
int __stdcall U_DEP_View_LogIn(OUT BYTE *present, OUT BYTE *type, OUT BYTE user[10 + 1]);
int __stdcall U_DEP_ReadCashStatus(OUT WORD numDoc[MAX_DEPOSIT_DOCTYPE], IN BYTE currency[3 + 1]);
int __stdcall U_DEP_ResetCashStatus(void);
int __stdcall U_DEP_SealBag(IN BYTE disable);
int __stdcall U_DEP_OpenSafe(void);
int __stdcall U_DEP_VerifySafeDoor(OUT BYTE *p_safeState);
int __stdcall U_DEP_VerifyBagPresent(OUT BYTE *p_BagPresent);
int __stdcall U_DEP_SetupBagNumber(IN BYTE getset, char bagNumber[7 + 1]);
int __stdcall U_DEP_SetupMaxBagAmount(IN BYTE getset, INOUT BYTE *enable, INOUT DWORD *value_max, INOUT DWORD *value_warning);
int __stdcall U_ONE_SetupSafeopenDelay(IN BYTE getset, int *p_minute);
int __stdcall U_DEP_Setup_AltAtReject(IN BYTE getset, LPBYTE p_mode);
int __stdcall U_DEP_Setup_AutoStart(IN BYTE getset, LPBYTE p_mode);
int __stdcall U_DEP_Deposit_Start(U_DEP_CALLBACK cbk, WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD *Dep_rejected);
int __stdcall U_DEP_Deposit_Start_And_Exit(void);
int __stdcall U_DEP_Deposit_Coin_Start(U_DEP_CALLBACK cbk, WORD Coins_Total[MAX_DEPOSIT_DOCTYPE], WORD *Coins_rejected, DWORD msec_timeOut);
int __stdcall U_DEP_GetDepositCounter(WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD *Dep_rejected);
int __stdcall U_DEP_GetDepositCounter_SingleCurrency(IN BYTE currency[3 + 1], WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD *Dep_rejected);
void Thread_Deposito(void);
int __stdcall U_DEP_ResetPartialCounter(void);
int __stdcall U_DEP_GetThreesholdFoto(IN BYTE sensor, OUT BYTE *threeshold);
int __stdcall U_DEP_SetThreesholdFoto(IN BYTE sensor, IN BYTE threeshold);
int __stdcall U_DEP_SetMode(IN BYTE mode, IN BYTE notused);
int __stdcall U_DEP_GetMode(OUT BYTE *mode, OUT BYTE *prm);
int __stdcall U_DEP_GetRCDescription(IN int rc, OUT BYTE *rcDescription);
int __stdcall U_DEP_GetResultDescription(IN int result, OUT BYTE *docResultDescription);
int __stdcall U_DEP_GetCurrency(OUT BYTE Currency[4]);
int __stdcall U_DEP_SetCurrency(IN BYTE Currency[4]);
int __stdcall U_DEP_GetTotalDoc(OUT DWORD *Total);
int __stdcall U_DEP_GetSolderModulePresence(OUT BYTE *presence);
int __stdcall U_DEP_Log_Read(OUT char *pLogData, OUT DWORD *pLogDataLen, IN char *filePath);
int __stdcall U_DEP_Log_ReadSingle(IN WORD ID_Log, OUT WORD *ID_Log_Last, OUT char *pLogData, OUT DWORD *pLogDataLen);
int __stdcall U_DEP_Log_Read_Last(IN DWORD nLog, OUT char *pLogData, OUT DWORD *pLogDataLen, IN char *filePath);
int __stdcall U_DEP_Log_Read_Range(IN DWORD nLogFrom, IN DWORD nLogTo, OUT char *pLogData, OUT DWORD *pLogDataLen, IN char *filePath);
int __stdcall U_DEP_BarcodeReader_Enable(void);
int __stdcall U_DEP_BarcodeReader_Disable(void);
int __stdcall U_DEP_BarcodeReader_ResetData(void);
int __stdcall U_DEP_BarcodeReader_ReadData(OUT BYTE *data, OUT BYTE *num_data);
int __stdcall U_DEP_CardReader_Enable(void);
int __stdcall U_DEP_CardReader_Disable(void);
int __stdcall U_DEP_CardReader_ResetData(void);
int __stdcall U_DEP_CardReader_ReadData(IN BYTE TrackNumber, OUT BYTE *data, OUT BYTE *num_data);
int __stdcall U_DEP_SetupConsole(IN BYTE getset, INOUT BYTE *data);
int __stdcall U_DEP_SetupCalibTouchScreen(void);
int __stdcall U_DEP_User_Add(IN BYTE *user, IN BYTE type, IN char *password);
int __stdcall U_DEP_User_Read(IN WORD number, OUT BYTE *presence, OUT BYTE *user, OUT BYTE *type, OUT char *password);
int __stdcall U_DEP_User_Erase(IN BYTE *user);
int __stdcall U_DEP_User_ChangePassword(IN BYTE *user, IN char *password_old, IN char *password_new);
int __stdcall U_DEP_SetupLogOutTimeOut(IN BYTE getset, INOUT WORD *time);
int __stdcall U_DEP_SetupLanguage(IN BYTE getset, INOUT BYTE *language);
int __stdcall U_DEP_DW_bmpToPrint(IN BYTE type, IN char *filepath, IN LPPERCENTCALLBACK pcallback);
int __stdcall U_DEP_Erase_bmpToPrint(void);
int __stdcall U_DEP_VerifyNewEvent(OUT BYTE *event, OUT BYTE *prm, OUT BYTE user[10 + 1], OUT BYTE bag_id[7 + 1]);
int __stdcall U_DEP_SetupCurrencyConversion(IN BYTE getset, IN BYTE nCurrency, INOUT WORD *Conversion);
int __stdcall U_DEP_GetID_Operation(OUT DWORD *id);
int __stdcall U_DEP_ChangeBag(void);
int __stdcall U_DEP_Light(IN BYTE light, IN BYTE action);
int __stdcall U_DEP_ReadCoinCashStatus(OUT WORD numCoin[MAX_DEPOSIT_DOCTYPE], IN BYTE currency[3 + 1]);
int __stdcall U_DEP_GetDepositCoinCounter(WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD *Dep_rejected);
int __stdcall U_DEP_Deposit_Notes_Coins_Start(U_DEP_CALLBACK cbk, WORD Notes_Total[MAX_DEPOSIT_DOCTYPE], WORD *Notes_rejected, WORD Coins_Total[MAX_DEPOSIT_DOCTYPE], WORD *Coins_rejected);
int __stdcall U_DEP_ResetCoin_CashStatus(void);
int __stdcall U_DEP_Get_StringToPrint(IN BYTE stringNum, OUT BYTE *prm, OUT BYTE *string);
int __stdcall U_DEP_Set_StringToPrint(IN BYTE stringNum, OUT BYTE prm, IN BYTE *string);
int __stdcall U_DEP_Erase_StringToPrint(IN BYTE stringNum);
int __stdcall U_DEP_SetupOpenSafeTime(IN BYTE getset, INOUT WORD *time);
int __stdcall U_DEP_DW_LogoDisplay(IN BYTE type, IN char *filepath, IN LPPERCENTCALLBACK pcallback);
int __stdcall U_DEP_Erase_LogoDisplay(void);
int __stdcall U_DEP_Get_LogoDisplay(BYTE *LogoName);
int __stdcall U_DEP_Get_bmpToPrint(OUT BYTE *presence);
int __stdcall U_DEP_VerifyNewDoc(OUT BYTE *presence, OUT BYTE *newdoc);
int __stdcall U_DEP_SetupTicketNumber(IN BYTE getset, INOUT BYTE *DepositTicket, INOUT BYTE *SolderTicket, INOUT BYTE *nu);
int __stdcall U_DEP_SetupLanServerParameter(IN BYTE getset, INOUT BYTE *ip, INOUT int *port);
#ifdef __cplusplus
}
#endif

#endif//_U_DEP_H_

