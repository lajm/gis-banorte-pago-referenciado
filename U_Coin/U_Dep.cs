﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace U_Coin
{
    public class U_Dep
    {
        public const int U_DEP_CONNECTMODE_SER = 0;
        public const int U_DEP_CONNECTMODE_NET = 1;
        public const int U_DEP_REMOTE_PORT = 1982;// greater than 1024

        //        // Welding module presence
        public const int WELDING_MODULE_NOT_PRESENT = '0';
        public const int WELDING_MODULE_PRESENT = '1';

        //        // Feeder module presence
        public const int FEEDER_MODULE_NOT_PRESENT = '0';
        public const int FEEDER_MODULE_PRESENT			='1';

        //        // Set mode
        public const byte U_DEP_MODE_DISABLE = (byte) '0'; // disable banknote feeding
        public const byte U_DEP_MODE_ENABLE = (byte) '1'; // enable banknote feeding
        public const byte U_DEP_MODE_ALTERNATE = (byte) '2';	//alternate
        public const byte U_DEP_MODE_COUNTING = (byte) '3';	//counting without validation:
        public const byte U_DEP_MODE_ONE_BY_ONE = (byte) '4';	//one by one with validation
        public const byte U_DEP_MODE_ONLY_VALIDATION = (byte) '5';	//no doc acquisition
        public const byte U_DEP_MODE_DATABASE_REJ = (byte) '6';//validation, no deposit: database of all docs. All doc will be rejected
        public const byte U_DEP_MODE_DATABASE_REJECT = (byte) '7';	//validation + deposit: database of rejected docs
        public const byte U_DEP_MODE_DATABASE_DEP = (byte) '8';	//validation + deposit: database of all docs. All doc will be putted on hopper

        //#define FW_LOG_MAXLEN					(16*1024*1024) // 16MB

        //        // save bmp
        public const int SAVE_GRAY = 0;
        public const int SAVE_RGB = 1;

        //        //Deposito
        public const int MAX_DEPOSIT_DOCTYPE = 21;

        //        //U_DEP_CalibCompensation
        public char CALIBRATION_BLACK = 'B';
        public char CALIBRATION_WHITE_STEP_1 = 'w';
        public char CALIBRATION_WHITE = 'W';
        public char CALIBRATION_SIDE_FRONT = 'F';
        public char CALIBRATION_SIDE_REAR = 'R';
        public char CALIBRATION_SIDE_ALL = 'A';

        //U_DEP_Light
        public const byte U_DEP_LIGHT_INPUT = (byte)'I';
        public const byte U_DEP_LIGHT_REJECT = (byte)'R';
        public const byte U_DEP_LIGHT_OFF = (byte)'0';
        public const byte U_DEP_LIGHT_ON = (byte)'1';


        //        //U_DEP_SealBag
        public char SEAL_ENABLED = '0';
        public char SEAL_DISABLED = '1';

        //        //U_DEP_VerifySafeDoor
        public char SAFE_CLOSED = '0';
        public char SAFE_OPEN = '1';

        //        //U_DEP_VerifyBagPresent
        public char BAG_NOT_PRESENT = '0';
        public char BAG_PRESENT = '1';

        //        //U_DEP_DW_Firmware
        public char MODULE_VALIDATOR = 'D';
        public char MODULE_MASTER = 'M';

        //        //U_DEP_SetupBagNumber
        public char SETUP_SET = 'S';
        public char SETUP_GET = 'G';

        //        //U_DEP_SetupMaxBagAmount
        public char MAX_BAG_AMOUNT_DISABLED = '0';
        public char MAX_BAG_AMOUNT_ENABLED = '1';

        //        //U_DEP_SetupConsole
        public char U_DEP_CONSOLE_DISABLED = '0';
        public char U_DEP_CONSOLE_ENABLED = '1';
        public const int DOCTYPE_CHECK = 0x20;
        public const int DOCTYPE_1 = 0x21;
        public const int DOCTYPE_2 = 0x22;
        public const int DOCTYPE_5 = 0x23;
        public const int DOCTYPE_10 = 0x24;
        public const int DOCTYPE_20 = 0x25;
        public const int DOCTYPE_50 = 0x26;
        public const int DOCTYPE_100 = 0x27;
        public const int DOCTYPE_200 = 0x28;
        public const int DOCTYPE_500 = 0x29;
        public const int DOCTYPE_1000 = 0x2A;
        public const int DOCTYPE_2000 = 0x2B;
        public const int DOCTYPE_5000 = 0x2C;
        public const int DOCTYPE_10000 = 0x2D;
        public const int DOC_NO_DATA = 0x95;

        //        // 0x5x: errors during visible (gray) recognition
        public const int VIS_BAD_CLASSIFICATION = 0x50;// neither near nor far from a template
        public const int VIS_REJECTED = 0x51;
        public const int VIS_COLOR_ERR = 0x52;
        public const int VIS_DOUBLE = 0x53;
        public const int VIS_DARK									=0x54;
        public const int VIS_BAD_BARCODE = 0x55;

        //        // 0x6x: errors during IR recognition
        public const int IR_REJECTED = 0x60;
        public const int UV_REJECTED = 0x6A;
        public const int SPECTRUM_REJECTED = 0x6B;
        //        // 0x7x: magnetic check
        public const int MG_CHECK_FAILED = 0x70;
        public const int MG_DIRTY = 0x71;
        //        // 0x8x: pre-fitness errors
        public const int DOC_BAD_DIMENSIONS							= 0x80  ;// before classification
        public const int DOC_BAD_SHAPE = 0x81;

        //        //0x9x
        //#define PARAM_ERROR									0x90

        //        //0xAx
        public const int REJECT_FORCE = 0xA0;
        public const int ACQ_DOC_TOO_LONG = 0xA1;
        public const int DOC_ON_PATH_TOO_EARLY = 0xA2;
        public const int SKEW_ERR = 0xA3;
        public const int INVALID_DOC = 0xA4;
        public const int SURPLUS_AUMOUNT_IN_BAG = 0xA5;

        //        //
        //        // retcodes
        //        //
        //        // 0x00
        public const int U_DEP_OK = 0x00;
        public const int U_DEP_BAG_NOT_EMPTY_WRN = 0x02;
        public const int U_DEP_LOGIN_ALREADY_PRESENT_WRN = 0x05;
        public const int U_DEP_SAFE_NOT_OPEN_WRN = 0x06;
        public const int U_DEP_CANT_FEED = 0x07;
        public const int U_DEP_DISABLED_WRN = 0x08;
        public const int U_DEP_INPUT_EMPTY = 0x09;
        public const int U_DEP_SN_BAG_NOT_PRESENT_WRN = 0x0A;
        public const int U_DEP_AMOUNT_BAG_NEAREST_FULL_WRN = 0x0B;
        public const int U_DEP_BAG_NOT_PRESENT_WRN = 0x0C;
        public const int U_DEP_BAGFULL_DIM_WRN = 0x0D;
        public const int U_DEP_BAGFULL_AMOUNT_WRN = 0x0E;
        public const int U_DEP_BAGFULL_COUNT_WRN = 0x0F;

        //        // 0x10
        public const int U_DEP_JAM_FOTO_TRASP_A_ERR = 0x10;
        public const int U_DEP_JAM_FOTO_TRASP_B_ERR = 0x11;
        public const int U_DEP_JAM_FOTO_TRASP_C_ERR = 0x12;
        public const int U_DEP_JAM_FOTO_TRASP_D_ERR = 0x13;
        public const int U_DEP_VALIDATION_TIMEOUT = 0x14;
        public const int U_DEP_JAM_PATH_ERROR_ERR = 0x15;
        public const int U_DEP_JAM_DOC_PATH_ERR = 0x16;
        public const int U_DEP_MOTOR_TRANSPORT_BLOCKED_ERR = 0x17;
        public const int U_DEP_MOTOR_PATH_BLOCKED_ERR = 0x18;
        public const int U_DEP_VALIDATION_TIME_TOO_LONG = 0x19;
        public const int U_DEP_INPUT_NOT_EMPTY = 0x1A;
        public const int U_DEP_TEMPLATE_NOT_PRESENT = 0x1B;
        public const int U_DEP_CURRRENCY_NOT_PRESENT = 0x1C;
        public const int U_DEP_JAM_USCITA = 0x1D;
        public const int U_DEP_SAFE_OPEN_ERR = 0x1E;
        public const int U_DEP_POWERFAIL_ERR = 0x1F;

        //        // 0x20
        public const int U_DEP_DOC_NOT_PRESENT_ON_EXTERNAL_SAFE_SENSOR	= 0x20;
        public const int U_DEP_JAM_ON_EXTERNAL_SAFE_SENSOR = 0x21;
        public const int U_DEP_DOC_NOT_PRESENT_ON_INTERNAL_SAFE_SENSOR = 0x22;
        public const int U_DEP_JAM_ON_INTERNAL_SAFE_SENSOR = 0x23;
        public const int U_DEP_UNEXPECTED_DOC_ON_PATH = 0x24;
        public const int U_DEP_JAM_ON_DOC_IN_HOPPER_SENSOR = 0x25;
        public const int U_DEP_DOC_NOT_PRESENT_ON_DOC_IN_HOPPER_SENSOR = 0x26;
        public const int U_DEP_COVER_OPEN = 0x2A;
        public const int U_DEP_PATH_OPEN = 0x2B;
        public const int U_DEP_REJECT_PATH_OPEN = 0x2C;
        public const int U_DEP_SAFE_PATH_OPEN = 0x2D;
        public const int U_DEP_HOPPER_NOT_CLOSED_ERR = 0x2E;
        public const int U_DEP_HOPPER_NOT_OPEN_ERR = 0x2F;

        //        // 0x30
        public const int U_DEP_PWM_TARATURA_ERR				= 0x30;
        public const int U_DEP_COMP_TARTURA_SOTT_ERR = 0x31;
        public const int U_DEP_COMP_TARTURA_DIFF_ERR			= 0x32;
        public const int U_DEP_COMP_TARTURA_MOLT_ERR = 0x33;


        //        // 0x90
        public const int U_DEP_COMMAND_ERR = 0x90;
        public const int U_DEP_PARAMETER_ERR = 0x91;
        public const int U_DEP_COMMAND_NOT_EXE_ERR = 0x93;
        public const int U_DEP_PERIPHERAL_NOT_CONNECTED = 0x94;
        public const int U_DEP_UNKNOWN_USER = 0x95;
        public const int U_DEP_WRONG_PASSWORD = 0x96;
        public const int U_DEP_RC_NO_USER = 0x97;
        public const int U_DEP_BUSY = 0x98;
        public const int U_DEP_REJECT_NOT_EMPTY = 0x99;
        public const int U_DEP_REJECT_FULL = 0x9A;
        public const int U_DEP_BAG_TO_REMOVE_ON_SAFE = 0x9B;
        public const int U_DEP_LIFT_IN_WRONG_POSITION = 0x9C;


        //        // 0xA0
        public const int U_DEP_EXT_FLASH_ERASE_ERR = 0xA0;
        public const int U_DEP_EXT_FLASH_PROGRAM_ERR = 0xA1;
        public const int U_DEP_EXT_FLASH_ON_ERROR_ERR = 0xA2;
        public const int U_DEP_EEPROM_WRITE_ERR = 0xA4;
        public const int U_DEP_EEPROM_READ_ERR = 0xA5;
        public const int U_DEP_FLASH_WRITE_ERR = 0xA6;
        //        //0xB0
        public const int U_DEP_SOLDER_MICRO_BAG_ANOMALY_ERR = 0xB0;
        public const int U_DEP_SOLDER_BAG_OPEN_ERR = 0xB1;
        public const int U_DEP_SOLDER_BAG_CLOSE_ERR = 0xB2;
        public const int U_DEP_SOLDER_TIMER_ERR = 0xB3;
        public const int U_DEP_SOLDER_PWM_LIMIT_UP_ERR = 0xB4;
        public const int U_DEP_SOLDER_PWM_LIMIT_DOWN_ERR = 0xB5;
        public const int U_DEP_SOLDER_V_UP_ERR = 0xB6;
        public const int U_DEP_SOLDER_V_DOWN_ERR = 0xB7;
        public const int U_DEP_SOLDER_CURR_LIMIT_ERR = 0xB8;
        public const int U_DEP_SOLDER_ON_ERR = 0xB9;
        public const int U_DEP_MICRO_LIFT_ANOMALY_ERR = 0xBA;
        public const int U_DEP_RESET_LIFT_ERR = 0xBB;
        public const int U_DEP_LIFT_MOVEMENT_ERR = 0xBC;
        //        // 0xC0
        public const int U_DEP_DP988_ANSWER_ERR = 0xC0;
        public const int U_DEP_SAFE_ANSWER_ERR = 0xC1;
        public const int U_DEP_SAFE_SHUTTER_OPEN_ERR = 0xCC;
        public const int U_DEP_SAFE_COMBINATION_ERR = 0xCD;
        public const int U_DEP_SAFE_LOCK_OPEN_ERR = 0xCE;
        public const int U_DEP_SAFE_BOLTOPEN_ERR = 0xCF;


        //        // 0xD0
        //        // 0xE0
        //        // 0xF0
        public const int U_DEP_SYSTEM_ERR = 0xFF;

        public const int U_DEP_FILEOPEN_ERR = -10;
        public const int U_DEP_DOWNLOAD_ERR = -11;
        public const int U_DEP_BAD_FILE_FORMAT_ERR = -12;
        public const int U_DEP_DLL_COINS_DISABLED = -13;

        public const int U_DEP_EVENT_CREATE_ERROR = - 30;
        public const int U_DEP_THREAD_CREATE_ERROR = -31;

        public const int U_DEP_DLL_BAD_DEPOSIT_PARAM = - 41;
        public const int U_DEP_DLL_BAD_DISPENCE_PARAM = -42;
        public const int U_DEP_DLL_BAD_PARAM = -43;



        public const int U_DEP_LOG_DISABLED = - 50;

        public const int U_DEP_COM_OPEN_ERR = - 1010;
        public const int U_DEP_COM_CLOSE_ERR = -1011;
        public const int U_DEP_COM_WRITE_ERR = -1012;
        public const int U_DEP_COM_READ_ERR = -1013;
        public const int U_DEP_COM_NOT_OPEN_ERR = -1014;
        public const int U_DEP_COM_ON_ERR = -1015;

        public const int U_DEP_NET_OPEN_ERR = -1040;
        public const int U_DEP_NET_CLOSE_ERR = -1041;
        public const int U_DEP_NET_WRITE_ERR = -1042;
        public const int U_DEP_NET_READ_ERR = -1043;
        public const int U_DEP_NET_READ_NODATA = -1044;

        public delegate int CallBack();

        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Deposit_Coin_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected, UInt32 timeout);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Deposit_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetStatus([MarshalAs(UnmanagedType.LPArray)]byte[] photo, [MarshalAs(UnmanagedType.LPArray)]int[] rc, [MarshalAs(UnmanagedType.LPArray)]byte[] photo_val);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Connect(Byte mode, Byte comport, [MarshalAs(UnmanagedType.LPArray)]byte[] servername, int serverport);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ResetSoftware();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ResetHardware();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ReadCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ConnectAutoCOM([MarshalAs(UnmanagedType.LPArray)]byte[] comPort);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Disconnect();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetID([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetID_Master([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetID_Validation([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Setup_SerialNumber(char g, [MarshalAs(UnmanagedType.LPArray)]byte[] snPres, [MarshalAs(UnmanagedType.LPArray)]byte[] sn, [MarshalAs(UnmanagedType.LPArray)]byte[] dt);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetMaxDocInHopper([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetMaxDocInBag([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] numDocW);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetSealTime([MarshalAs(UnmanagedType.LPArray)]int[] amp, [MarshalAs(UnmanagedType.LPArray)]int[] time);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_SetMode(byte mode, byte notused);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_DiaGetSensor([MarshalAs(UnmanagedType.LPArray)]byte[] sensor, [MarshalAs(UnmanagedType.LPArray)]byte[] sensor_val);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_OpenSafe();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ResetCashStatus();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]char[] currency);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_SealBag(byte disable);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetTotalDoc([MarshalAs(UnmanagedType.LPArray)]int[] total);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetCurrencyList([MarshalAs(UnmanagedType.LPArray)]byte[] p_list, [MarshalAs(UnmanagedType.LPArray)]byte[] param);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_SetupBagNumber(byte g, [MarshalAs(UnmanagedType.LPArray)]byte[] barcode);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetRCDescription(int rc, [MarshalAs(UnmanagedType.LPArray)]byte[] p_desc);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_VerifyBagPresent([MarshalAs(UnmanagedType.LPArray)]byte[] status);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_VerifySafeDoor([MarshalAs(UnmanagedType.LPArray)]byte[] status);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_HopperMovement(byte movement);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ResetPartialCounter();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetDepositCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetCurrencyList(byte pos, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_LogIn([MarshalAs(UnmanagedType.LPArray)]byte[] user);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_LogOut();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ChangeBag();
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_DiaTubo([MarshalAs(UnmanagedType.LPArray)]byte[] tx, int txlen, [MarshalAs(UnmanagedType.LPArray)]byte[] rx, [MarshalAs(UnmanagedType.LPArray)]int[] rxlen, [MarshalAs(UnmanagedType.LPArray)]int[] msec_TimeOut);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Light(byte light, byte action);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_Deposit_Notes_Coins_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_rejected, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_rejected);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ReadCoinCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_GetDepositCoinCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej);
        [DllImport(@"U_DEP.dll")]
        private static extern int U_DEP_ResetCoin_CashStatus();




        public unsafe int DLLU_DEP_Deposit_Coin_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected, UInt32 timeout)
        {
            return U_DEP_Deposit_Coin_Start(cbk, Dep_Total, Dep_rejected, timeout);
        }
        public unsafe int DLLU_DEP_Deposit_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected)
        {
            return U_DEP_Deposit_Start(cbk, Dep_Total, Dep_rejected);
        }
        public int DLLU_DEP_GetStatus([MarshalAs(UnmanagedType.LPArray)]byte[] photo, [MarshalAs(UnmanagedType.LPArray)]int[] rc, [MarshalAs(UnmanagedType.LPArray)]byte[] photo_val)
        {
            return U_DEP_GetStatus(photo, rc, photo_val);
        }
        public unsafe int DLLU_DEP_Connect(Byte mode, Byte comport, [MarshalAs(UnmanagedType.LPArray)]byte[] servername, int serverport)
        {
            return U_DEP_Connect(mode, comport, servername, serverport);
        }
        public int DLLU_DEP_ResetSoftware()
        {
            return U_DEP_ResetSoftware();
        }
        public int DLLU_DEP_ResetHardware()
        {
            return U_DEP_ResetHardware();
        }
        public int DLLU_DEP_ReadCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
        {
            return U_DEP_ReadCashStatus(numDoc, curr);
        }
        public int DLLU_DEP_ConnectAutoCOM([MarshalAs(UnmanagedType.LPArray)]byte[] comPort)
        {
            return U_DEP_ConnectAutoCOM(comPort);
        }
        public int DLLU_DEP_Disconnect()
        {
            return U_DEP_Disconnect();
        }
        public int DLLU_DEP_GetID([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
        {
            return U_DEP_GetID(ID);
        }
        public int DLLU_DEP_GetID_Master([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
        {
            return U_DEP_GetID_Master(ID);
        }
        public int DLLU_DEP_GetID_Validation([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
        {
            return U_DEP_GetID_Validation(ID);
        }
        public int DLLU_DEP_SetupSernum(char g, [MarshalAs(UnmanagedType.LPArray)]byte[] sn, [MarshalAs(UnmanagedType.LPArray)]byte[] dt)
        {
            byte[] snPres = new byte[10];
            return U_DEP_Setup_SerialNumber(g, snPres, sn, dt);
        }
        public int DLLU_DEP_GetMaxDocInHopper([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc)
        {
            return U_DEP_GetMaxDocInHopper(numDoc);
        }
        public int DLLU_DEP_GetMaxDocInBag([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] numDocW)
        {
            return U_DEP_GetMaxDocInBag(numDoc, numDocW);
        }
        public int DLLU_DEP_GetSealTime([MarshalAs(UnmanagedType.LPArray)]int[] amp, [MarshalAs(UnmanagedType.LPArray)]int[] time)
        {
            return U_DEP_GetSealTime(amp, time);
        }
        public int DLLU_DEP_SetMode(byte mode, byte notused)
        {
            return U_DEP_SetMode(mode, notused);
        }
        public int DLLU_DEP_DiaGetSensor([MarshalAs(UnmanagedType.LPArray)]byte[] sensor, [MarshalAs(UnmanagedType.LPArray)]byte[] sensor_val)
        {
            return U_DEP_DiaGetSensor(sensor, sensor_val);
        }
        public int DLLU_DEP_OpenSafe()
        {
            return U_DEP_OpenSafe();
        }
        public int DLLU_DEP_ResetCashStatus()
        {
            return U_DEP_ResetCashStatus();
        }
        public int DLLU_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]char[] currency)
        {
            return U_DEP_SetCurrency(currency);
        }
        public int DLLU_DEP_SealBag()
        {
            return U_DEP_SealBag(Convert.ToByte('0'));
        }
        public int DLLU_DEP_GetTotDoc([MarshalAs(UnmanagedType.LPArray)]int[] total)
        {
            return U_DEP_GetTotalDoc(total);
        }
        public int DLLU_DEP_GetCurrencyList([MarshalAs(UnmanagedType.LPArray)]byte[] p_list, [MarshalAs(UnmanagedType.LPArray)]byte[] param)
        {
            return U_DEP_GetCurrencyList(p_list, param);
        }
        public int DLLU_DEP_SetupBagBarcode(byte g, [MarshalAs(UnmanagedType.LPArray)]byte[] barcode)
        {
            return U_DEP_SetupBagNumber(g, barcode);
        }
        public int DLLU_DEP_GetRCDescription(int rc, [MarshalAs(UnmanagedType.LPArray)]byte[] p_desc)
        {
            return U_DEP_GetRCDescription(rc, p_desc);
        }
        public int DLLU_DEP_VerifyBagPresent([MarshalAs(UnmanagedType.LPArray)]byte[] status)
        {
            return U_DEP_VerifyBagPresent(status);
        }
        public int DLLU_DEP_VerifySafeDoor([MarshalAs(UnmanagedType.LPArray)]byte[] status)
        {
            return U_DEP_VerifySafeDoor(status);
        }
        public int DLLU_DEP_GetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr)
        {
            return U_DEP_GetCurrency(curr);
        }
        public int DLLU_DEP_GetCurrencyList(byte pos, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
        {
            return U_DEP_GetCurrencyList(pos, curr);
        }
        public int DLLU_DEP_OpenHopper()
        {
            return U_DEP_HopperMovement(Convert.ToByte('0'));
        }
        public int DLLU_DEP_ResetPartialCounter()
        {
            return U_DEP_ResetPartialCounter();
        }
        public int DLLU_DEP_GetDepositCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej)
        {
            return U_DEP_GetDepositCounter(numDoc, rej);
        }
        public int DLLU_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr)
        {
            return U_DEP_SetCurrency(curr);
        }
        public int DLLU_DEP_LogIn([MarshalAs(UnmanagedType.LPArray)]byte[] user)
        {
            return U_DEP_LogIn(user);
        }
        public int DLLU_DEP_LogOut()
        {
            return U_DEP_LogOut();
        }
        public int DLLU_DEP_ChangeBag()
        {
            return U_DEP_ChangeBag();
        }
        public int DLLU_DEP_DiaTubo([MarshalAs(UnmanagedType.LPArray)]byte[] tx, int txlen, [MarshalAs(UnmanagedType.LPArray)]byte[] rx, [MarshalAs(UnmanagedType.LPArray)]int[] rxlen, [MarshalAs(UnmanagedType.LPArray)]int[] msec_TimeOut)
        {
            return U_DEP_DiaTubo(tx, txlen, rx, rxlen, msec_TimeOut);
        }
        public int DLLU_DEP_Light(byte light, byte action)
        {
            return U_DEP_Light(light, action);
        }
        public int DLLU_DEP_Deposit_Notes_Coins_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_rejected, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_rejected)
        {
            return U_DEP_Deposit_Notes_Coins_Start(cbk, Notes_Total, Notes_rejected, Coins_Total, Coins_rejected);
        }
        public int DLLU_DEP_ReadCoinCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
        {
            return U_DEP_ReadCoinCashStatus(numCoin, curr);
        }
        public int DLLU_DEP_GetDepositCoinCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej)
        {
            return U_DEP_GetDepositCoinCounter(numCoin, rej);
        }
        public int DLLU_DEP_ResetCoin_CashStatus()
        {
            return U_DEP_ResetCoin_CashStatus();
        }


        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //    int U_DEP_Connect( byte mode, byte comport, string servername, Int64 serverport );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.Cdecl)]// .StdCall )]
        //public static extern
        //int U_DEP_ConnectAutoCOM( out Int32 p_comport);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_Disconnect();
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_GetID([MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder ID);
        //[DllImport( "U_DEP.dll",CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        //public static extern
        //        int U_DEP_GetID_Master([MarshalAs(UnmanagedType.LPStr)] StringBuilder ID);
        //[DllImport( "U_DEP.dll", EntryPoint = "U_DEP_GetID_Validation", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //    int U_DEP_GetID_Validation([MarshalAs(UnmanagedType.LPStr)] StringBuilder ID_VAL );

        //[DllImport("U_DEP.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        //public static extern int U_DEP_GetStatus(byte[] p_Photo, out int p_Estatus, byte[] p_PhotoVal);

        //[ DllImport( "U_DEP.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_DLL_GetRelease( out StringBuilder p_Release);
        ////        int __stdcall U_DEP_GetStatus( OUT BYTE photo[ 4 ], OUT int* rc_val, OUT BYTE photo_val[ 4 ]);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_ResetSoftware();
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_ResetHardware();
        //[DllImport( "U_DEP.dll",CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_LogIn( [MarshalAs( UnmanagedType.LPStr )] string user );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_LogIn_And_Password( string user, string password);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_LogOut();
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_View_LogIn( out byte present, out byte type, out StringBuilder user);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //       int U_DEP_ReadCashStatus( Int32[] numDoc, string currency);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_ResetCashStatus( );
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_SealBag( byte disable );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_OpenSafe( );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_VerifySafeDoor( out byte p_safeState);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_VerifyBagPresent( out byte p_BagPresent);
        ////        int __stdcall U_DEP_SetupBagNumber( IN BYTE getset, char bagNumber[7 + 1]);
        ////        int __stdcall U_DEP_SetupMaxBagAmount( IN BYTE getset, INOUT BYTE *enable, INOUT DWORD *value_max, INOUT DWORD *value_warning);
        ////        int __stdcall U_ONE_SetupSafeopenDelay( IN BYTE getset, int* p_minute );
        ////        int __stdcall U_DEP_Setup_AltAtReject( IN BYTE getset, LPBYTE p_mode );
        ////        int __stdcall U_DEP_Setup_AutoStart( IN BYTE getset, LPBYTE p_mode );
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_Deposit_Start( IntPtr cbk, ref Int32[] Dep_Total, ref Int32 Dep_rejected );
        ////int U_DEP_Deposit_Start( U_DEP_CALLBACK cbk, ref Int32[] Dep_Total, ref [] Dep_rejected );
        ////        int __stdcall U_DEP_Deposit_Start_And_Exit( void);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_Deposit_Coin_Start(IntPtr cbk, Int32[] Coins_Total
        //                , out Int32 Coins_rejected, Int32 msec_timeOut );
        ////        int __stdcall U_DEP_GetDepositCounter( WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD* Dep_rejected );
        ////int __stdcall U_DEP_GetDepositCounter_SingleCurrency( IN BYTE currency[ 3 + 1 ], WORD Dep_Total[MAX_DEPOSIT_DOCTYPE], WORD* Dep_rejected );
        ////        void Thread_Deposito( void);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_ResetPartialCounter();
        ////        int __stdcall U_DEP_GetThreesholdFoto( IN BYTE sensor, OUT BYTE *threeshold);
        ////        int __stdcall U_DEP_SetThreesholdFoto( IN BYTE sensor, IN BYTE threeshold);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_SetMode( byte mode, byte notused );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_GetMode( out byte mode, out byte prm );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_GetRCDescription( int rc, [MarshalAs( UnmanagedType.LPStr )] out StringBuilder rcDescription );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern 
        //    int U_DEP_GetResultDescription( int result, out string Description);
        ////        int __stdcall U_DEP_GetCurrency( OUT BYTE Currency[ 4 ]);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //    int U_DEP_SetCurrency( [MarshalAs( UnmanagedType.LPStr )]string Currency);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_GetTotalDoc( out Int64 Total);
        ////        int __stdcall U_DEP_GetSolderModulePresence( OUT BYTE *presence);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_Log_ReadSingle( Int32 ID_Log, out Int32 ID_Log_Last, out StringBuilder pLogData, out Int64 pLogDataLen);
        ////        int __stdcall U_DEP_Log_Read_Last( IN DWORD nLog, OUT char* pLogData, OUT DWORD *pLogDataLen, IN char* filePath );
        ////        int __stdcall U_DEP_Log_Read_Range( IN DWORD nLogFrom, IN DWORD nLogTo, OUT char* pLogData, OUT DWORD *pLogDataLen, IN char* filePath );
        ////        int __stdcall U_DEP_BarcodeReader_Enable( void);
        ////        int __stdcall U_DEP_BarcodeReader_Disable( void);
        ////        int __stdcall U_DEP_BarcodeReader_ResetData( void);
        ////        int __stdcall U_DEP_BarcodeReader_ReadData( OUT BYTE *data, OUT BYTE *num_data);
        ////        int __stdcall U_DEP_CardReader_Enable( void);
        ////        int __stdcall U_DEP_CardReader_Disable( void);
        ////        int __stdcall U_DEP_CardReader_ResetData( void);
        ////        int __stdcall U_DEP_CardReader_ReadData( IN BYTE TrackNumber, OUT BYTE *data, OUT BYTE *num_data);
        ////        int __stdcall U_DEP_SetupConsole( IN BYTE getset, INOUT BYTE *data);
        ////        int __stdcall U_DEP_SetupCalibTouchScreen( void);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_User_Add( string user, byte type, string password );
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall ) ]
        //public static extern
        //int U_DEP_User_Read( Int32 number, out Int32 presence, out StringBuilder user, out byte type, out string password );
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall ) ]
        //public static extern
        //int U_DEP_User_Erase( string user);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall ) ]
        //public static extern
        //int U_DEP_User_ChangePassword( string user, string password_old, string password_new );
        ////        int __stdcall U_DEP_SetupLogOutTimeOut( IN BYTE getset, INOUT WORD *time);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_SetupLanguage( byte getset, out byte language);
        ////        int __stdcall U_DEP_DW_bmpToPrint( IN BYTE type, IN char* filepath, IN LPPERCENTCALLBACK pcallback);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_Erase_bmpToPrint( );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_VerifyNewEvent( out byte p_event, out byte prm, out StringBuilder user , out StringBuilder bag_id );
        ////int __stdcall U_DEP_SetupCurrencyConversion( IN BYTE getset, IN BYTE nCurrency, INOUT WORD *Conversion);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_GetID_Operation( out Int64 id);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_ChangeBag( );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //        int U_DEP_Light( byte light, byte action);
        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_ReadCoinCashStatus( Int32 [] numCoin, out StringBuilder currency);

        //[ DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_GetDepositCoinCounter( Int32[] Dep_Total,  out Int32[] Dep_rejected );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern        //        
        //    int U_DEP_Deposit_Notes_Coins_Start( Int32 cbk, out Int32[] Notes_Total, out Int32 Notes_rejected, Int32[] Coins_Total
        //                , out Int32 Coins_rejected );
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_ResetCoin_CashStatus();
        ////        int __stdcall U_DEP_Get_StringToPrint( IN BYTE stringNum, OUT BYTE *prm, OUT BYTE *string);
        ////        int __stdcall U_DEP_Set_StringToPrint( IN BYTE stringNum, OUT BYTE prm, IN BYTE *string);
        ////        int __stdcall U_DEP_Erase_StringToPrint( IN BYTE stringNum);
        ////        int __stdcall U_DEP_SetupOpenSafeTime( IN BYTE getset, INOUT WORD *time);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_DW_LogoDisplay( byte type, String filepath, IntPtr pcallback);
        ////        int __stdcall U_DEP_Erase_LogoDisplay( void);
        ////        int __stdcall U_DEP_Get_LogoDisplay( BYTE* LogoName );
        ////        int __stdcall U_DEP_Get_bmpToPrint( OUT BYTE *presence);
        ////        int __stdcall U_DEP_VerifyNewDoc( OUT BYTE *presence, OUT BYTE *newdoc);
        //[DllImport( "U_DEP.dll", CallingConvention = CallingConvention.StdCall )]
        //public static extern
        //int U_DEP_SetupTicketNumber( byte getset, ref byte DepositTicket, ref byte SolderTicket, ref byte nu);
        ////        int __stdcall U_DEP_SetupLanServerParameter( IN BYTE getset, INOUT BYTE *ip, INOUT int* port );


    }
}
