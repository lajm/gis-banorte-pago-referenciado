﻿namespace TestCodeVarios
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.c_ligth = new System.Windows.Forms.Button();
            this.c_ligth_off = new System.Windows.Forms.Button();
            this.c_safe_Open = new System.Windows.Forms.Button();
            this.c_Reset_Cash = new System.Windows.Forms.Button();
            this.c_CashTotal = new System.Windows.Forms.Button();
            this.c_resetSW = new System.Windows.Forms.Button();
            this.c_resethw = new System.Windows.Forms.Button();
            this.c_validator = new System.Windows.Forms.Button();
            this.c_Depositar = new System.Windows.Forms.Button();
            this.c_Status = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TEST TICKET";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 40);
            this.button1.TabIndex = 1;
            this.button1.Text = "Ticket";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(684, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "TEST MONEDAS";
            // 
            // c_ligth
            // 
            this.c_ligth.Location = new System.Drawing.Point(698, 50);
            this.c_ligth.Name = "c_ligth";
            this.c_ligth.Size = new System.Drawing.Size(113, 40);
            this.c_ligth.TabIndex = 3;
            this.c_ligth.Text = "Ligth ON";
            this.c_ligth.UseVisualStyleBackColor = true;
            this.c_ligth.Click += new System.EventHandler(this.c_ligth_Click);
            // 
            // c_ligth_off
            // 
            this.c_ligth_off.Location = new System.Drawing.Point(698, 96);
            this.c_ligth_off.Name = "c_ligth_off";
            this.c_ligth_off.Size = new System.Drawing.Size(113, 40);
            this.c_ligth_off.TabIndex = 4;
            this.c_ligth_off.Text = "Ligth OFF";
            this.c_ligth_off.UseVisualStyleBackColor = true;
            this.c_ligth_off.Click += new System.EventHandler(this.c_ligth_off_Click);
            // 
            // c_safe_Open
            // 
            this.c_safe_Open.Location = new System.Drawing.Point(698, 142);
            this.c_safe_Open.Name = "c_safe_Open";
            this.c_safe_Open.Size = new System.Drawing.Size(113, 40);
            this.c_safe_Open.TabIndex = 5;
            this.c_safe_Open.Text = "Abrir Boveda";
            this.c_safe_Open.UseVisualStyleBackColor = true;
            this.c_safe_Open.Click += new System.EventHandler(this.c_safe_Open_Click);
            // 
            // c_Reset_Cash
            // 
            this.c_Reset_Cash.Location = new System.Drawing.Point(698, 188);
            this.c_Reset_Cash.Name = "c_Reset_Cash";
            this.c_Reset_Cash.Size = new System.Drawing.Size(113, 40);
            this.c_Reset_Cash.TabIndex = 6;
            this.c_Reset_Cash.Text = "Empty Cassete";
            this.c_Reset_Cash.UseVisualStyleBackColor = true;
            this.c_Reset_Cash.Click += new System.EventHandler(this.c_Reset_Cash_Click);
            // 
            // c_CashTotal
            // 
            this.c_CashTotal.Location = new System.Drawing.Point(698, 467);
            this.c_CashTotal.Name = "c_CashTotal";
            this.c_CashTotal.Size = new System.Drawing.Size(113, 40);
            this.c_CashTotal.TabIndex = 7;
            this.c_CashTotal.Text = "Consulta";
            this.c_CashTotal.UseVisualStyleBackColor = true;
            this.c_CashTotal.Click += new System.EventHandler(this.c_CashTotal_Click);
            // 
            // c_resetSW
            // 
            this.c_resetSW.Location = new System.Drawing.Point(698, 280);
            this.c_resetSW.Name = "c_resetSW";
            this.c_resetSW.Size = new System.Drawing.Size(113, 40);
            this.c_resetSW.TabIndex = 8;
            this.c_resetSW.Text = "Reset SW";
            this.c_resetSW.UseVisualStyleBackColor = true;
            this.c_resetSW.Click += new System.EventHandler(this.c_resetSW_Click);
            // 
            // c_resethw
            // 
            this.c_resethw.Location = new System.Drawing.Point(698, 234);
            this.c_resethw.Name = "c_resethw";
            this.c_resethw.Size = new System.Drawing.Size(113, 40);
            this.c_resethw.TabIndex = 9;
            this.c_resethw.Text = "Reste HW";
            this.c_resethw.UseVisualStyleBackColor = true;
            this.c_resethw.Click += new System.EventHandler(this.c_resethw_Click);
            // 
            // c_validator
            // 
            this.c_validator.Location = new System.Drawing.Point(698, 421);
            this.c_validator.Name = "c_validator";
            this.c_validator.Size = new System.Drawing.Size(113, 40);
            this.c_validator.TabIndex = 10;
            this.c_validator.Text = "Id Validator";
            this.c_validator.UseVisualStyleBackColor = true;
            this.c_validator.Click += new System.EventHandler(this.c_validator_Click);
            // 
            // c_Depositar
            // 
            this.c_Depositar.Location = new System.Drawing.Point(698, 326);
            this.c_Depositar.Name = "c_Depositar";
            this.c_Depositar.Size = new System.Drawing.Size(113, 40);
            this.c_Depositar.TabIndex = 11;
            this.c_Depositar.Text = "Depositar";
            this.c_Depositar.UseVisualStyleBackColor = true;
            this.c_Depositar.Click += new System.EventHandler(this.c_Depositar_Click);
            // 
            // c_Status
            // 
            this.c_Status.Location = new System.Drawing.Point(698, 375);
            this.c_Status.Name = "c_Status";
            this.c_Status.Size = new System.Drawing.Size(113, 40);
            this.c_Status.TabIndex = 12;
            this.c_Status.Text = "STATUS";
            this.c_Status.UseVisualStyleBackColor = true;
            this.c_Status.Click += new System.EventHandler(this.c_Status_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 343);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 55);
            this.button2.TabIndex = 13;
            this.button2.Text = "Test coin";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TestCodeVarios.Properties.Resources._287;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(844, 519);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.c_Status);
            this.Controls.Add(this.c_Depositar);
            this.Controls.Add(this.c_validator);
            this.Controls.Add(this.c_resethw);
            this.Controls.Add(this.c_resetSW);
            this.Controls.Add(this.c_CashTotal);
            this.Controls.Add(this.c_Reset_Cash);
            this.Controls.Add(this.c_safe_Open);
            this.Controls.Add(this.c_ligth_off);
            this.Controls.Add(this.c_ligth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Ticket Verificador";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button c_ligth;
        private System.Windows.Forms.Button c_ligth_off;
        private System.Windows.Forms.Button c_safe_Open;
        private System.Windows.Forms.Button c_Reset_Cash;
        private System.Windows.Forms.Button c_CashTotal;
        private System.Windows.Forms.Button c_resetSW;
        private System.Windows.Forms.Button c_resethw;
        private System.Windows.Forms.Button c_validator;
        private System.Windows.Forms.Button c_Depositar;
        private System.Windows.Forms.Button c_Status;
        private System.Windows.Forms.Button button2;
    }
}

