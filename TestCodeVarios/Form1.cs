﻿using Azteca_SID;
using Newtonsoft.Json;
using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using SymetryDevices;


namespace TestCodeVarios
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SidApi.SidLib.SID_Open(false);

           int l_reply =  SidLib.SID_Reset((char)0x34);

            MessageBox.Show("reset bag Reply : " + l_reply );


           // Ticketcomprobacionstatus();
        }





        private void Ticketcomprobacionstatus()
        {

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            String l_Error = "";


            bool Cerradura = false;
            bool PuertaBovedad = false;
            bool Comunicaciones = false;
            bool Bolsapuesta = false;
            bool l_Tapa_abierta = false;

            String l_ip = "";

            EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Revisando Equipo", 1);

            if (true)
            {
                try
                {

                    SensarSIDcs.UnitStatus l_Status = SensarSIDcs.GetStatus();

                                
                        EscribirBitacora("Estatus del Equipo", "UnitStatus  KEY : ", key[0].ToString("X2") + " : " + l_Error, 1);
                        SidLib.SID_Close();
                    if (SensarSIDcs.IsBagReadyForDeposit())
                        Bolsapuesta = true;

                    if (l_Status.m_Interlock_door_strongbox_closed)
                        PuertaBovedad = true;

                    if (l_Status.m_Code_lock_correct)
                        Cerradura = true;

                    if (l_Status.m_Interlock_cover_head_open)



                    EscribirBitacora("Estatus del Equipo", "UnitStatus: ", l_Status.m_SenseKey.ToString() + " : " + l_Error, 1);
                    EscribirBitacora("Estatus del Equipo", "UnitStatus: ", JsonConvert.SerializeObject(l_Status), 1);

                    l_ip= GetLocalIPAddress();
                }
                catch (Exception exception)
                {
                    SidLib.SID_Close();
                    EscribirBitacora("Estatus del Equipo", "UnitStatus: ", " Excepcion encontrada" + " : " + exception, 1);
                }
            }

            if (!l_ip.Contains("127.0.0.1") && !String.IsNullOrEmpty(l_ip))
                Comunicaciones = true;



            ModulePrint imprimir = new ModulePrint();
           // imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);
            try
            {
                String[] datos = "Puebla|Puebla|Central de Abastos".Split('|');

                imprimir.AddHeaderLine("Estado:         " + datos[0]);
                imprimir.AddHeaderLine("Ciudad:         " + datos[1]);
                imprimir.AddHeaderLine("Ubicación:     " + datos[2]);

            }
            catch (Exception exx)
            {
                imprimir.AddHeaderLine("Estado:         " + "México");
                imprimir.AddHeaderLine("Ciudad:         " + "México");
                imprimir.AddHeaderLine("Ubicación:     " + "Suc. Banorte X");
                EscribirBitacora("Ticket ", "Escribir Cabecera", "Puebla|Puebla|Central de Abastos", 3);
            }

            imprimir.AddHeaderLine("Verificación de Estatus Cajero");

            imprimir.AddHeaderLine("========================================");



            imprimir.AddHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToShortTimeString());
            imprimir.AddHeaderLine("");

            imprimir.AddHeaderLine("BOLSA COLOCADA: " + (Bolsapuesta ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
            imprimir.AddHeaderLine("");
            imprimir.AddHeaderLine("DIAL COLOCADO: " + (Cerradura ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
            imprimir.AddHeaderLine("");
            imprimir.AddHeaderLine("PUERTA CERRADA: " + (PuertaBovedad ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
            imprimir.AddHeaderLine("");
            imprimir.AddHeaderLine("COMUNICACIÓN: " + (Comunicaciones ? "CORRECTA " : "Fuera de Linea, Por favor revise"));
            imprimir.AddHeaderLine("");

            if (Bolsapuesta && Cerradura && PuertaBovedad && Comunicaciones)
            {
                imprimir.AddFooterLine("Comprobación Realizada: ");
                imprimir.AddFooterLine("----RECEPTOR OPERABLE----");
                EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Conclusion: OPERABLE", 1);
            }
            else
            {
                imprimir.AddFooterLine("Comprobación Realizada: ");
                imprimir.AddFooterLine("----RECEPTOR NO  OPERABLE----");
                imprimir.AddFooterLine("Revise segun las Indicaciones");
                EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Conclusion: NO OPERABLE", 1);
                if (!Bolsapuesta)
                    EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Bolsa mal Puesta", 1);
                if (!PuertaBovedad)
                    EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Puerta Abierta", 1);
                if (!Cerradura)
                    EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "DIAL ABIERTO", 1);
                if (!Comunicaciones)
                    EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "No hay IP o conexión de RED", 1);

            }

            try
            {

               // imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

            }
            catch (Exception exx)
            {

            }



            imprimir.PrintTicket("CUSTOM TL80", DateTime.Now.ToShortTimeString());



            imprimir.Dispose();

        }

        public static void
           EscribirBitacora(String p_Modulo, String p_Funcion, String p_Mensaje, int nivel_detalle)
        {
            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.ToString("MM_dd") + ".txt";
            try
            {
                if (nivel_detalle <= 3)
                {
                    using (StreamWriter l_Archivo = File.AppendText(l_log))
                    {
                        l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                    }


                }

            }
            catch (Exception exx)
            {
                System.Threading.Thread.Sleep(120);
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                {
                    l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                }
            }
        }

             public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("IP Local NO Encontrada");
        }

        String l_error;
        int l_nuevoError;

        private void c_ligth_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cTestLigthON(3);
            ErroresUcoin.Error_Ucoin (l_reply,out l_nuevoError,out  l_error);
            MessageBox.Show(l_error);


        }

        private void c_ligth_off_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cTestLigthOFF(3);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_error);
        }

        private void c_safe_Open_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cOpenSafe(3);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_error);

        }

        private void c_Reset_Cash_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cRetiro(3);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_error);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void c_CashTotal_Click(object sender, EventArgs e)
        {
            decimal l_total = 0;
            Dictionary<decimal, int> p_tabla = new Dictionary<decimal, int>();

            int l_reply = UCoin.ObtenerTotales(3, "Admin",out l_total,out p_tabla);


            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_total + " " + l_error);
        }

        private void c_resethw_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cRealizaResetHardware(4);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_error);
        }

        private void c_resetSW_Click(object sender, EventArgs e)
        {
            int l_reply = UCoin.cRealizaResetSoftware(4);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_error);
        }

        private void c_validator_Click(object sender, EventArgs e)
        {
            String l_id;
            int l_reply = UCoin.cgetValidator(3,out l_id);
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_id + " " +l_error);
        }

        private void c_Depositar_Click(object sender, EventArgs e)
        {
            decimal l_total = 0;
            Dictionary<decimal, int> p_tabla = new Dictionary<decimal, int>();

            String l_master;

            int l_status = UCoin.cgetMaster(4, out l_master);
            int l_reply = UCoin.Depositar(4, "Admin", out l_total,out p_tabla , 3);

                
            ErroresUcoin.Error_Ucoin(l_reply, out l_nuevoError, out l_error);
            MessageBox.Show(l_total + " " + l_error);
        }

        private void c_Status_Click(object sender, EventArgs e)
        {
            int l_status;
            String l_master;

            l_status = UCoin.cgetMaster(4,out l_master);

            l_status = UCoin.cgetStatus(4);

            ErroresUcoin.Error_Ucoin(l_status, out l_nuevoError, out l_error);
            MessageBox.Show(l_status + " " + l_error);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Device.OpenDevice(false);
            byte[] b = new byte[100];
            Device.GetMasterID(b);
            Device.Logout();
            byte[] user = System.Text.Encoding.ASCII.GetBytes("ale\0");
            Device.Login(user);


        }
    }
}
