﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace TestCodeVarios
{
  class ClassCommand
  {
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Deposit_Coin_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected, UInt32 timeout);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Deposit_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetStatus([MarshalAs(UnmanagedType.LPArray)]byte[] photo, [MarshalAs(UnmanagedType.LPArray)]int[] rc, [MarshalAs(UnmanagedType.LPArray)]byte[] photo_val);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Connect(Byte mode, Byte comport, [MarshalAs(UnmanagedType.LPArray)]byte[] servername, int serverport);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ResetSoftware();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ResetHardware();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ReadCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ConnectAutoCOM([MarshalAs(UnmanagedType.LPArray)]byte[] comPort);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Disconnect();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetID([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetID_Master([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetID_Validation([MarshalAs(UnmanagedType.LPArray)]byte[] ID);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Setup_SerialNumber(char g, [MarshalAs(UnmanagedType.LPArray)]byte[] snPres, [MarshalAs(UnmanagedType.LPArray)]byte[] sn, [MarshalAs(UnmanagedType.LPArray)]byte[] dt);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetMaxDocInHopper([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetMaxDocInBag([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] numDocW);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetSealTime([MarshalAs(UnmanagedType.LPArray)]int[] amp, [MarshalAs(UnmanagedType.LPArray)]int[] time);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_SetMode(byte mode, byte notused);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_DiaGetSensor([MarshalAs(UnmanagedType.LPArray)]byte[] sensor, [MarshalAs(UnmanagedType.LPArray)]byte[] sensor_val);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_OpenSafe();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ResetCashStatus();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]char[] currency);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_SealBag(byte disable);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetTotalDoc([MarshalAs(UnmanagedType.LPArray)]int[] total);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetCurrencyList([MarshalAs(UnmanagedType.LPArray)]byte[] p_list, [MarshalAs(UnmanagedType.LPArray)]byte[] param);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_SetupBagNumber(byte g, [MarshalAs(UnmanagedType.LPArray)]byte[] barcode);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetRCDescription(int rc, [MarshalAs(UnmanagedType.LPArray)]byte[] p_desc);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_VerifyBagPresent([MarshalAs(UnmanagedType.LPArray)]byte[] status);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_VerifySafeDoor([MarshalAs(UnmanagedType.LPArray)]byte[] status);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_HopperMovement(byte movement);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ResetPartialCounter();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetDepositCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej);    
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetCurrencyList(byte pos, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_LogIn([MarshalAs(UnmanagedType.LPArray)]byte[] user);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_LogOut();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ChangeBag();
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_DiaTubo([MarshalAs(UnmanagedType.LPArray)]byte[] tx, int txlen,[MarshalAs(UnmanagedType.LPArray)]byte[] rx,[MarshalAs(UnmanagedType.LPArray)]int[] rxlen,[MarshalAs(UnmanagedType.LPArray)]int[] msec_TimeOut);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Light(byte light, byte action);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_Deposit_Notes_Coins_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_rejected, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_rejected);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ReadCoinCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]byte[] curr);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_GetDepositCoinCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej);
    [DllImport(@"U_DEP.dll")]
    private static extern int U_DEP_ResetCoin_CashStatus();




    public unsafe int DLLU_DEP_Deposit_Coin_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected, UInt32 timeout)
    {
      return U_DEP_Deposit_Coin_Start(cbk, Dep_Total, Dep_rejected,timeout);
    }
    public unsafe int DLLU_DEP_Deposit_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Dep_rejected)
    {
      return U_DEP_Deposit_Start(cbk, Dep_Total, Dep_rejected);
    }
    public int DLLU_DEP_GetStatus([MarshalAs(UnmanagedType.LPArray)]byte[] photo, [MarshalAs(UnmanagedType.LPArray)]int[] rc, [MarshalAs(UnmanagedType.LPArray)]byte[] photo_val)
    {
      return U_DEP_GetStatus(photo, rc, photo_val);
    }
    public unsafe int DLLU_DEP_Connect(Byte mode, Byte comport, [MarshalAs(UnmanagedType.LPArray)]byte[] servername, int serverport)
    {
      return U_DEP_Connect(mode, comport, servername, serverport);
    }
    public int DLLU_DEP_ResetSoftware()
    {
      return U_DEP_ResetSoftware();
    }
    public int DLLU_DEP_ResetHardware()
    {
      return U_DEP_ResetHardware();
    }
    public int DLLU_DEP_ReadCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
    {
      return U_DEP_ReadCashStatus(numDoc,curr);
    }
    public int DLLU_DEP_ConnectAutoCOM([MarshalAs(UnmanagedType.LPArray)]byte[] comPort)
    {
      return U_DEP_ConnectAutoCOM(comPort);
    }
    public int DLLU_DEP_Disconnect()
    {
      return U_DEP_Disconnect();
    }
    public int DLLU_DEP_GetID([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
    {
      return U_DEP_GetID(ID);
    }
    public int DLLU_DEP_GetID_Master([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
    {
      return U_DEP_GetID_Master(ID);
    }
    public int DLLU_DEP_GetID_Validation([MarshalAs(UnmanagedType.LPArray)]byte[] ID)
    {
      return U_DEP_GetID_Validation(ID);
    }
    public int DLLU_DEP_SetupSernum(char g, [MarshalAs(UnmanagedType.LPArray)]byte[] sn, [MarshalAs(UnmanagedType.LPArray)]byte[] dt)
    {
      byte[] snPres=new byte[10];
      return U_DEP_Setup_SerialNumber(g, snPres, sn, dt);
    }
    public int DLLU_DEP_GetMaxDocInHopper([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc)
    {
      return U_DEP_GetMaxDocInHopper(numDoc);
    }
    public int DLLU_DEP_GetMaxDocInBag([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] numDocW)
    {
      return U_DEP_GetMaxDocInBag(numDoc, numDocW);
    }
    public int DLLU_DEP_GetSealTime([MarshalAs(UnmanagedType.LPArray)]int[] amp, [MarshalAs(UnmanagedType.LPArray)]int[] time)
    {
      return U_DEP_GetSealTime(amp, time);
    }
    public int DLLU_DEP_SetMode(byte mode, byte notused)
    {
      return U_DEP_SetMode(mode, notused);
    }
    public int DLLU_DEP_DiaGetSensor([MarshalAs(UnmanagedType.LPArray)]byte[] sensor, [MarshalAs(UnmanagedType.LPArray)]byte[] sensor_val)
    {
      return U_DEP_DiaGetSensor(sensor, sensor_val);
    }
    public int DLLU_DEP_OpenSafe()
    {
      return U_DEP_OpenSafe();
    }
    public int DLLU_DEP_ResetCashStatus()
    {
      return U_DEP_ResetCashStatus();
    }
    public int DLLU_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]char[] currency)
    {
      return U_DEP_SetCurrency(currency);
    }
    public int DLLU_DEP_SealBag()
    {
      return U_DEP_SealBag(Convert.ToByte('0'));
    }
    public int DLLU_DEP_GetTotDoc([MarshalAs(UnmanagedType.LPArray)]int[] total)
    {
      return U_DEP_GetTotalDoc(total);
    }
    public int DLLU_DEP_GetCurrencyList([MarshalAs(UnmanagedType.LPArray)]byte[] p_list, [MarshalAs(UnmanagedType.LPArray)]byte[] param)
    {
      return U_DEP_GetCurrencyList(p_list, param);
    }
    public int DLLU_DEP_SetupBagBarcode(byte g, [MarshalAs(UnmanagedType.LPArray)]byte[] barcode)
    {
      return U_DEP_SetupBagNumber(g, barcode);
    }
    public int DLLU_DEP_GetRCDescription(int rc, [MarshalAs(UnmanagedType.LPArray)]byte[] p_desc)
    {
      return U_DEP_GetRCDescription(rc, p_desc);
    }
    public int DLLU_DEP_VerifyBagPresent([MarshalAs(UnmanagedType.LPArray)]byte[] status)
    {
      return U_DEP_VerifyBagPresent(status);
    }
    public int DLLU_DEP_VerifySafeDoor([MarshalAs(UnmanagedType.LPArray)]byte[] status)
    {
      return U_DEP_VerifySafeDoor(status);
    }
    public int DLLU_DEP_GetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr)
    {
      return U_DEP_GetCurrency(curr);
    }
    public int DLLU_DEP_GetCurrencyList(byte pos, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
    {
      return U_DEP_GetCurrencyList(pos, curr);
    }
    public int DLLU_DEP_OpenHopper()
    {
      return U_DEP_HopperMovement(Convert.ToByte('0'));
    }
    public int DLLU_DEP_ResetPartialCounter()
    {
      return U_DEP_ResetPartialCounter();
    }
    public int DLLU_DEP_GetDepositCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numDoc, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej)
    {
      return U_DEP_GetDepositCounter(numDoc, rej);
    }
    public int DLLU_DEP_SetCurrency([MarshalAs(UnmanagedType.LPArray)]byte[] curr)
    {
      return U_DEP_SetCurrency(curr);
    }
    public int DLLU_DEP_LogIn([MarshalAs(UnmanagedType.LPArray)]byte[] user)
    {
      return U_DEP_LogIn(user);
    }
    public int DLLU_DEP_LogOut()
    {
      return U_DEP_LogOut();
    }
    public int DLLU_DEP_ChangeBag()
    {
      return U_DEP_ChangeBag();
    }
    public int DLLU_DEP_DiaTubo([MarshalAs(UnmanagedType.LPArray)]byte[] tx, int txlen, [MarshalAs(UnmanagedType.LPArray)]byte[] rx, [MarshalAs(UnmanagedType.LPArray)]int[] rxlen, [MarshalAs(UnmanagedType.LPArray)]int[] msec_TimeOut)
    {
      return U_DEP_DiaTubo(tx, txlen, rx, rxlen, msec_TimeOut);
    }
    public int DLLU_DEP_Light(byte light, byte action)
    {
      return U_DEP_Light(light, action);
    }
    public int DLLU_DEP_Deposit_Notes_Coins_Start(CallBack cbk, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Notes_rejected, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_Total, [MarshalAs(UnmanagedType.LPArray)]UInt16[] Coins_rejected)
    {
      return U_DEP_Deposit_Notes_Coins_Start(cbk, Notes_Total, Notes_rejected, Coins_Total, Coins_rejected);
    }
    public int DLLU_DEP_ReadCoinCashStatus([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]byte[] curr)
    {
      return U_DEP_ReadCoinCashStatus(numCoin, curr);
    }
    public int DLLU_DEP_GetDepositCoinCounter([MarshalAs(UnmanagedType.LPArray)]UInt16[] numCoin, [MarshalAs(UnmanagedType.LPArray)]UInt16[] rej)
    {
      return U_DEP_GetDepositCoinCounter(numCoin, rej);
    }
    public int DLLU_DEP_ResetCoin_CashStatus()
    {
      return U_DEP_ResetCoin_CashStatus();
    }

  }
}
