﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public delegate int CallBack();

namespace TestCodeVarios
{
  class Device
  {
    private static ClassCommand cCmd = new ClassCommand();

    public static int StartDepositCoin(CallBack cbk, UInt16[] Deposited, UInt16[] Rejected)
    {
      int ret = 0;
      ret = cCmd.DLLU_DEP_Deposit_Coin_Start(cbk, Deposited, Rejected, 10000);
      return ret;
    }

    public static int OpenDevice(bool KillThread)
    {
      byte[] comPort;
      comPort = new byte[3];
      int ret = 0;
      ret = cCmd.DLLU_DEP_ConnectAutoCOM(comPort);
      return ret;
    }
    public static int CloseDevice()
    {
      int ret = 0;
      ret = cCmd.DLLU_DEP_Disconnect();
      return ret;
    }
    public static int GetMasterID(byte[] id)
    {
      return cCmd.DLLU_DEP_GetID_Master(id);
    }
    public static int Login(byte[] user)
    {
      return cCmd.DLLU_DEP_LogIn(user);
    }
    public static int Logout()
    {
      return cCmd.DLLU_DEP_LogOut();
    }
    public static int ResetPartial()
    {
      return cCmd.DLLU_DEP_ResetPartialCounter();
    }










  }

  //public class EnumDepositCallback
  //{
  //  private static Form1 _mainForm;

  //  public static int deposit_callback()
  //  {
  //    _mainForm.RefreshDeposit();
  //    return 0;  // just a fake value
  //  }

  //  public static void setForm(Form1 f)
  //  {
  //    _mainForm = f;
  //  }
  //}
}
