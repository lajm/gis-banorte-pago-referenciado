﻿namespace Azteca_SID
{
    partial class FormaDatosDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_fecha = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_hora = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.c_serial = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.c_CR = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.c_numSecuencia = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.c_Referencia = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.c_Folio = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.c_divisa = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.c_importe = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.c_importeLetra = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.c_claveRastreo = new System.Windows.Forms.TextBox();
            this.c_mensaje = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_fecha
            // 
            this.c_fecha.BackColor = System.Drawing.Color.White;
            this.c_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_fecha.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_fecha.Location = new System.Drawing.Point(418, 70);
            this.c_fecha.Name = "c_fecha";
            this.c_fecha.Size = new System.Drawing.Size(337, 29);
            this.c_fecha.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(51, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "FECHA:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(51, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "HORA: ";
            // 
            // c_hora
            // 
            this.c_hora.BackColor = System.Drawing.Color.White;
            this.c_hora.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_hora.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_hora.Location = new System.Drawing.Point(418, 106);
            this.c_hora.Name = "c_hora";
            this.c_hora.Size = new System.Drawing.Size(337, 29);
            this.c_hora.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label3.Location = new System.Drawing.Point(51, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "ID DISPOSITIVO: ";
            // 
            // c_serial
            // 
            this.c_serial.BackColor = System.Drawing.Color.White;
            this.c_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_serial.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_serial.Location = new System.Drawing.Point(418, 140);
            this.c_serial.Name = "c_serial";
            this.c_serial.Size = new System.Drawing.Size(337, 29);
            this.c_serial.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(51, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(264, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "CENTRO RESPONSABLE: ";
            // 
            // c_CR
            // 
            this.c_CR.BackColor = System.Drawing.Color.White;
            this.c_CR.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_CR.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_CR.Location = new System.Drawing.Point(418, 174);
            this.c_CR.Name = "c_CR";
            this.c_CR.Size = new System.Drawing.Size(337, 29);
            this.c_CR.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label5.Location = new System.Drawing.Point(51, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "CUENTA: ";
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.Color.White;
            this.c_cuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_cuenta.Location = new System.Drawing.Point(418, 208);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(337, 29);
            this.c_cuenta.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label6.Location = new System.Drawing.Point(51, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(229, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "NUMERO SECUENCIA:";
            // 
            // c_numSecuencia
            // 
            this.c_numSecuencia.BackColor = System.Drawing.Color.White;
            this.c_numSecuencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numSecuencia.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_numSecuencia.Location = new System.Drawing.Point(418, 242);
            this.c_numSecuencia.Name = "c_numSecuencia";
            this.c_numSecuencia.Size = new System.Drawing.Size(337, 29);
            this.c_numSecuencia.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label7.Location = new System.Drawing.Point(51, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(249, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "NUMERO REFERENCIA: ";
            // 
            // c_Referencia
            // 
            this.c_Referencia.BackColor = System.Drawing.Color.White;
            this.c_Referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Referencia.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_Referencia.Location = new System.Drawing.Point(418, 276);
            this.c_Referencia.Name = "c_Referencia";
            this.c_Referencia.Size = new System.Drawing.Size(337, 29);
            this.c_Referencia.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label8.Location = new System.Drawing.Point(51, 313);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 24);
            this.label8.TabIndex = 16;
            this.label8.Text = "NUMERO MOVIMIENTO:";
            // 
            // c_Folio
            // 
            this.c_Folio.BackColor = System.Drawing.Color.White;
            this.c_Folio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Folio.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_Folio.Location = new System.Drawing.Point(418, 310);
            this.c_Folio.Name = "c_Folio";
            this.c_Folio.Size = new System.Drawing.Size(337, 29);
            this.c_Folio.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label9.Location = new System.Drawing.Point(51, 347);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 24);
            this.label9.TabIndex = 18;
            this.label9.Text = "DIVISA:";
            // 
            // c_divisa
            // 
            this.c_divisa.BackColor = System.Drawing.Color.White;
            this.c_divisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_divisa.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_divisa.Location = new System.Drawing.Point(418, 344);
            this.c_divisa.Name = "c_divisa";
            this.c_divisa.Size = new System.Drawing.Size(337, 29);
            this.c_divisa.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label10.Location = new System.Drawing.Point(51, 381);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 24);
            this.label10.TabIndex = 20;
            this.label10.Text = "IMPORTE:";
            // 
            // c_importe
            // 
            this.c_importe.BackColor = System.Drawing.Color.White;
            this.c_importe.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_importe.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_importe.Location = new System.Drawing.Point(418, 378);
            this.c_importe.Name = "c_importe";
            this.c_importe.Size = new System.Drawing.Size(337, 29);
            this.c_importe.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label11.Location = new System.Drawing.Point(51, 415);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(221, 24);
            this.label11.TabIndex = 22;
            this.label11.Text = "IMPORTE EN LETRA: ";
            // 
            // c_importeLetra
            // 
            this.c_importeLetra.BackColor = System.Drawing.Color.White;
            this.c_importeLetra.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_importeLetra.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_importeLetra.Location = new System.Drawing.Point(418, 412);
            this.c_importeLetra.Name = "c_importeLetra";
            this.c_importeLetra.Size = new System.Drawing.Size(337, 29);
            this.c_importeLetra.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label12.Location = new System.Drawing.Point(51, 449);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(193, 24);
            this.label12.TabIndex = 24;
            this.label12.Text = "CLAVE RASTREO: ";
            // 
            // c_claveRastreo
            // 
            this.c_claveRastreo.BackColor = System.Drawing.Color.White;
            this.c_claveRastreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_claveRastreo.ForeColor = System.Drawing.SystemColors.InfoText;
            this.c_claveRastreo.Location = new System.Drawing.Point(264, 446);
            this.c_claveRastreo.Name = "c_claveRastreo";
            this.c_claveRastreo.Size = new System.Drawing.Size(491, 29);
            this.c_claveRastreo.TabIndex = 23;
            // 
            // c_mensaje
            // 
            this.c_mensaje.AutoSize = true;
            this.c_mensaje.BackColor = System.Drawing.Color.Transparent;
            this.c_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mensaje.ForeColor = System.Drawing.Color.Red;
            this.c_mensaje.Location = new System.Drawing.Point(110, 483);
            this.c_mensaje.Name = "c_mensaje";
            this.c_mensaje.Size = new System.Drawing.Size(596, 16);
            this.c_mensaje.TabIndex = 25;
            this.c_mensaje.Text = "mensaje deposito gsi ####################@@@@@@@@@@@@@##########";
            // 
            // FormaDatosDeposito
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_mensaje);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.c_claveRastreo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.c_importeLetra);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.c_importe);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.c_divisa);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.c_Folio);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.c_Referencia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_numSecuencia);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_cuenta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.c_CR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_serial);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_hora);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_fecha);
            this.Name = "FormaDatosDeposito";
            this.Load += new System.EventHandler(this.FormaDatosDeposito_Load);
            this.Controls.SetChildIndex(this.c_fecha, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_hora, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.c_serial, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.c_CR, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_cuenta, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.c_numSecuencia, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.c_Referencia, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.c_Folio, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.c_divisa, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.c_importe, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.c_importeLetra, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.c_claveRastreo, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.c_mensaje, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_fecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_hora;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_serial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_CR;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_numSecuencia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox c_Referencia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox c_Folio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox c_divisa;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox c_importe;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox c_importeLetra;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox c_claveRastreo;
        private System.Windows.Forms.Label c_mensaje;
    }
}
