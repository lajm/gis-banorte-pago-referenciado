﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaUSUARIOgsi : Azteca_SID.FormBase
    {
        private int v;

        public FormaUSUARIOgsi( )
        {
            InitializeComponent( );
        }

        public FormaUSUARIOgsi(bool p_cierreA ):base (p_cierreA)
        {
            InitializeComponent( );
        }

        private void c_volver_Click( object sender , EventArgs e )
        {
            Close( );
        }

        private void c_aceptar_Click( object sender , EventArgs e )
        {
            this.DialogResult = DialogResult.OK;
        }

        private void FormaUSUARIOgsi_Load( object sender , EventArgs e )
        {
            string CuentaTmp = Globales.IdUsuario;
            if ( !Properties.Settings.Default.CAJA_EMPRESARIAL )
                try
                {

                    CuentaTmp = CuentaTmp.Replace( System.Environment.NewLine , "" );

                    CuentaTmp = "********" + CuentaTmp.Substring( CuentaTmp.Length - 4 , 4 );

                }
                catch ( Exception E )
                {
                    //throw;
                    c_cuenta.Text = Globales.IdUsuario;
                }
            c_cuenta.Text = CuentaTmp;

            e_nombre.Text  = Globales.NombreUsuario;

            if (Globales.Limite_MontoGSI > 0 )
            {
                e_restriccion.Text += Globales.Limite_MontoGSI.ToString( "C" );
                e_restriccion.Visible = true;
                e_mensajeRestriccion.Visible = true;
                e_instrucciones. Visible = false;
               // i_alerta.Visible = true;
            }
        }
    }
}
