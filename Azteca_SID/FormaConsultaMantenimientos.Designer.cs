﻿namespace Azteca_SID
{
    partial class FormaConsultaMantenimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.c_numoperaciones = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_fechabox = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_ConsultaOperaciones = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // c_numoperaciones
            // 
            this.c_numoperaciones.BackColor = System.Drawing.Color.Gainsboro;
            this.c_numoperaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_numoperaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numoperaciones.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_numoperaciones.Location = new System.Drawing.Point(258, 480);
            this.c_numoperaciones.Name = "c_numoperaciones";
            this.c_numoperaciones.Size = new System.Drawing.Size(81, 19);
            this.c_numoperaciones.TabIndex = 54;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(36, 478);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 20);
            this.label2.TabIndex = 53;
            this.label2.Text = "Numero de Operaciones : ";
            // 
            // c_fechabox
            // 
            this.c_fechabox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_fechabox.Location = new System.Drawing.Point(12, 64);
            this.c_fechabox.MinDate = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            this.c_fechabox.Name = "c_fechabox";
            this.c_fechabox.Size = new System.Drawing.Size(285, 26);
            this.c_fechabox.TabIndex = 52;
            this.c_fechabox.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(302, 62);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 29);
            this.label1.TabIndex = 51;
            this.label1.Text = "Mantenimientos";
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(593, 507);
            this.c_Cerrar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(184, 40);
            this.c_Cerrar.TabIndex = 50;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = true;
            this.c_Cerrar.Visible = false;
            // 
            // c_ConsultaOperaciones
            // 
            this.c_ConsultaOperaciones.AllowUserToAddRows = false;
            this.c_ConsultaOperaciones.AllowUserToDeleteRows = false;
            this.c_ConsultaOperaciones.AllowUserToOrderColumns = true;
            this.c_ConsultaOperaciones.AllowUserToResizeColumns = false;
            this.c_ConsultaOperaciones.AllowUserToResizeRows = false;
            this.c_ConsultaOperaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.c_ConsultaOperaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.c_ConsultaOperaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.c_ConsultaOperaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_ConsultaOperaciones.Location = new System.Drawing.Point(6, 93);
            this.c_ConsultaOperaciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_ConsultaOperaciones.Name = "c_ConsultaOperaciones";
            this.c_ConsultaOperaciones.ReadOnly = true;
            this.c_ConsultaOperaciones.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.c_ConsultaOperaciones.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.c_ConsultaOperaciones.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ConsultaOperaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.c_ConsultaOperaciones.Size = new System.Drawing.Size(792, 380);
            this.c_ConsultaOperaciones.TabIndex = 49;
            // 
            // FormaConsultaMantenimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_numoperaciones);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_fechabox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Cerrar);
            this.Controls.Add(this.c_ConsultaOperaciones);
            this.Name = "FormaConsultaMantenimientos";
            this.Text = "FormaConsultaMantenimientos";
            this.Load += new System.EventHandler(this.FormaConsultaMantenimientos_Load);
            this.Controls.SetChildIndex(this.c_ConsultaOperaciones, 0);
            this.Controls.SetChildIndex(this.c_Cerrar, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_fechabox, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.c_numoperaciones, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_numoperaciones;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker c_fechabox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.DataGridView c_ConsultaOperaciones;
    }
}