﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaVerificarcs : Azteca_SID.FormBase
    {
        [DllImport( "pi6100k.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern long depositacheque([MarshalAs(UnmanagedType.LPStr)] string parCuenta);

        public FormaVerificarcs(bool p_timer ):base(p_timer)
        {
            InitializeComponent();
            c_Referencia.MaxLength = Convert.ToInt32(UtilsComunicacion.Desencriptar(Globales.refUno).Substring(0, 2));
        }


        public FormaVerificarcs()
        {
            InitializeComponent();
            c_Referencia.MaxLength = Convert.ToInt32(UtilsComunicacion.Desencriptar(Globales.refUno).Substring(0, 2));
        }

        String l_Identificacion = "";
        private void FormaVerificarcs_Load(object sender, EventArgs e)
        {
            c_leyenda.Text = "Verifique si los datos a los que se realizará el Depósito son correctos";


            string CuentaTmp = Globales.IdUsuario;
            if (!Properties.Settings.Default.CAJA_EMPRESARIAL)
                try
            {

                CuentaTmp = CuentaTmp.Replace(System.Environment.NewLine, "");

                CuentaTmp = "********" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);

            }
            catch (Exception E)
            {
                //throw;
                c_cuenta.Text = Globales.IdUsuario;
            }
            c_cuenta.Text = CuentaTmp;

            if (Properties.Settings.Default.CAJA_EMPRESARIAL && !String.IsNullOrEmpty( Globales.ConvenioCIEUsuario) )
            {
                c_Referencia.Text = Globales.ConvenioCIEUsuario;
                c_group_Numeros.Visible = false;
                gb_alfa.Visible = false;
            }

            if (!String.IsNullOrEmpty(Globales.ReferenciaGSI))
            {
                c_Referencia.Text = Globales.ReferenciaGSI;
                c_group_Numeros.Visible = true;
            }

            if (Properties.Settings.Default.Deposito_Remoto)
            {
                c_mensajeCuenta.Visible = false;
                c_group_Numeros.Visible = false;
                gb_alfa.Visible = false;
                g_referencia .Visible = false;
                c_Cheque.Visible = true;
            }

            gb_alfa.Visible = false;
            

        }

        private void c_volver_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_continuar_Click(object sender, EventArgs e)
        {
            if (c_Referencia.Text.Length == Convert.ToInt32(UtilsComunicacion.Desencriptar(Globales.refUno).Substring(0,2)))
            {
                if (UtilsComunicacion.Desencriptar(Globales.refUno).Substring(2, 1) == "N")
                {
                    long temp = 0;
                    if (!long.TryParse(c_Referencia.Text, out temp))
                    {
                        using (FormaError f_Error = new FormaError(true, "warning"))
                        {
                            f_Error.c_MensajeError.Text = "La referencia no es valida";
                            f_Error.ShowDialog();
                        }
                        return;
                    }                    
                }
            }
            else
            {
                using (FormaError f_Error = new FormaError(true, "warning"))
                {
                    f_Error.c_MensajeError.Text = "La longitud de la referencia es invalida";
                    f_Error.ShowDialog();
                }
                return;
            }

            try
            {
                c_continuar.Enabled = false;
             

                if (c_Referencia.Text == "Ingrese su Referencia")
                {
                    c_Referencia.Text = "";

                }
                else {

                    // Regex l_patron = new Regex( ( "^[0-9]*[1-9][0-9]*$" ) );
                    Regex l_patron = new Regex(("^+[0]{1,20}$"));

                    if (l_patron.IsMatch(c_Referencia.Text))
                        c_Referencia.Text = "";

                }


                Globales.EscribirBitacora("Verificar Cuenta", "Continuar", "Referencia: " + c_Referencia.Text, 3);
                Globales.ReferenciaGSI = c_Referencia.Text;


                FormaPrincipal.s_Forma.Gsi_Cronometro(false);
              
               using (Formatrancision l_trans = new Formatrancision ())

                {
                    l_trans.ShowDialog( );

                }

                Close( );

            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaVerificar", "c_continuar_Click", E.Message, 3);
               
            }
            finally
            {

            //   FormaPrincipal.s_Forma.Gsi_Cronometro(true);

            }

            c_continuar.Enabled = true;
        }


        private void Oprimir_tecla( object sender , EventArgs e )
        {
            Cursor.Show( );
            Cursor.Show( );
            Cursor.Current = Cursors.WaitCursor;




            if ( c_Referencia.Text == "Ingrese su Referencia" || c_Referencia .Text == Globales.ReferenciaGSI )
                c_Referencia.Text = "";

            if ( l_Identificacion.Length < Convert.ToInt32(UtilsComunicacion.Desencriptar(Globales.refUno).Substring(0, 2)) )
            {

                if ( sender.Equals( c_Boton0 ) )
                {
                    l_Identificacion += "0";
                    c_Referencia.Text += "0";
                }
                else if ( sender.Equals( c_Boton1 ) )
                {
                    l_Identificacion += "1";
                    c_Referencia.Text += "1";
                }
                else if ( sender.Equals( c_Boton2 ) )
                {
                    l_Identificacion += "2";
                    c_Referencia.Text += "2";
                }
                else if ( sender.Equals( c_Boton3 ) )
                {
                    l_Identificacion += "3";
                    c_Referencia.Text += "3";
                }
                else if ( sender.Equals( c_Boton4 ) )
                {
                    l_Identificacion += "4";
                    c_Referencia.Text += "4";
                }
                else if ( sender.Equals( c_Boton5 ) )
                {
                    l_Identificacion += "5";
                    c_Referencia.Text += "5";
                }
                else if ( sender.Equals( c_Boton6 ) )
                {
                    l_Identificacion += "6";
                    c_Referencia.Text += "6";
                }
                else if ( sender.Equals( c_Boton7 ) )
                {
                    l_Identificacion += "7";
                    c_Referencia.Text += "7";
                }
                else if ( sender.Equals( c_Boton8 ) )
                {
                    l_Identificacion += "8";
                    c_Referencia.Text += "8";
                }
                else if ( sender.Equals( c_Boton9 ) )
                {
                    l_Identificacion += "9";
                    c_Referencia.Text += "9";
                }

                // LETRAS ALFABETICAS
                else if ( sender.Equals( c_A) )
                {
                    l_Identificacion += "A";
                    c_Referencia.Text += "A";
                }
                else if ( sender.Equals( c_B ) )
                {
                    l_Identificacion += "B";
                    c_Referencia.Text += "B";
                }
                else if ( sender.Equals( c_C ) )
                {
                    l_Identificacion += "C";
                    c_Referencia.Text += "C";
                }
                else if ( sender.Equals( c_D ) )
                {
                    l_Identificacion += "D";
                    c_Referencia.Text += "D";
                }
                else if ( sender.Equals( c_E ) )
                {
                    l_Identificacion += "E";
                    c_Referencia.Text += "E";
                }
                else if ( sender.Equals( c_F ) )
                {
                    l_Identificacion += "F";
                    c_Referencia.Text += "F";
                }
                else if ( sender.Equals( c_G ) )
                {
                    l_Identificacion += "G";
                    c_Referencia.Text += "G";
                }
                else if ( sender.Equals( c_H ) )
                {
                    l_Identificacion += "H";
                    c_Referencia.Text += "H";
                }
                else if ( sender.Equals( c_I ) )
                {
                    l_Identificacion += "I";
                    c_Referencia.Text += "I";
                }
                else if ( sender.Equals( c_J ) )
                {
                    l_Identificacion += "J";
                    c_Referencia.Text += "J";
                }
                else if ( sender.Equals( c_K ) )
                {
                    l_Identificacion += "K";
                    c_Referencia.Text += "K";
                }
                else if ( sender.Equals( c_L ) )
                {
                    l_Identificacion += "L";
                    c_Referencia.Text += "L";
                }
                else if ( sender.Equals( c_M ) )
                {
                    l_Identificacion += "M";
                    c_Referencia.Text += "M";
                }
                else if ( sender.Equals( c_N ) )
                {
                    l_Identificacion += "N";
                    c_Referencia.Text += "N";
                }
                else if ( sender.Equals( c_O ) )
                {
                    l_Identificacion += "O";
                    c_Referencia.Text += "O";
                }
                else if ( sender.Equals( c_P ) )
                {
                    l_Identificacion += "P";
                    c_Referencia.Text += "P";
                }
                else if ( sender.Equals( c_Q ) )
                {
                    l_Identificacion += "Q";
                    c_Referencia.Text += "Q";
                }
                else if ( sender.Equals( c_R ) )
                {
                    l_Identificacion += "R";
                    c_Referencia.Text += "R";
                }
                else if ( sender.Equals( c_S ) )
                {
                    l_Identificacion += "S";
                    c_Referencia.Text += "S";
                }
                else if ( sender.Equals( c_T ) )
                {
                    l_Identificacion += "T";
                    c_Referencia.Text += "T";
                }
                else if ( sender.Equals( c_U ) )
                {
                    l_Identificacion += "U";
                    c_Referencia.Text += "U";
                }
                else if ( sender.Equals( c_V ) )
                {
                    l_Identificacion += "V";
                    c_Referencia.Text += "V";
                }
                else if ( sender.Equals( c_W ) )
                {
                    l_Identificacion += "W";
                    c_Referencia.Text += "W";
                }
                else if ( sender.Equals( c_X ) )
                {
                    l_Identificacion += "X";
                    c_Referencia.Text += "X";
                }
                else if ( sender.Equals( c_Y ) )
                {
                    l_Identificacion += "Y";
                    c_Referencia.Text += "Y";
                }
                else if ( sender.Equals( c_Z ) )
                {
                    l_Identificacion += "Z";
                    c_Referencia.Text += "Z";
                }

            }

            if ( sender.Equals( c_BackSpace ) )
            {
                if ( l_Identificacion.Length != 0 )
                {
                    l_Identificacion = l_Identificacion.Substring( 0 , l_Identificacion.Length - 1 );
                    c_Referencia.Text = c_Referencia.Text.Substring( 0 , l_Identificacion.Length );
                }
            }

            Cursor.Hide( );
        }


        private void c_cheques_Click(object sender, EventArgs e)
        {
            c_Cheque.Enabled = false;

            int l_result = (int)depositacheque(Globales.IdUsuario);

            Globales.EscribirBitacora( "FormaVerificar" , "Deposito de Cheques" ,"Resultado ="  + l_result , 3 );

            if (l_result > 20)
            {
                int contador = 0;
                String[] l_info = Leer_ticket("\\Tickets\\F_" + DateTime.Now.ToString("yyyyMMdd") + "\\t_" + l_result + ".txt");
                if (l_info != null)
                {
                    ModulePrint imprimir = new ModulePrint();
                    imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);

                    if (Properties.Settings.Default.WebGSI)
                    {
                        imprimir.AddHeaderLine("              DEPOSITO DE CHEQUE          ");
                        imprimir.AddHeaderLine("No. de Comprobante:   " + l_result);
                        imprimir.AddHeaderLine("Caja de Deposito:         " + Properties.Settings.Default.NumSerialEquipo);
                        string CuentaTmp = Globales.IdUsuario;
                        try
                        {

                            CuentaTmp = CuentaTmp.Replace(System.Environment.NewLine, "");

                            CuentaTmp = "********" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);

                        }
                        catch (Exception E)
                        {
                            //throw;
                            imprimir.AddHeaderLine("No. Cuenta:            00000000");
                        }

                        imprimir.AddHeaderLine("No. de Sesion:              " + CuentaTmp);
                        imprimir.AddHeaderLine("No. de Operacion:        " + l_result);
                        imprimir.AddHeaderLine("DIVISA:                       " + "MXP");
                        imprimir.AddHeaderLine(" ");

                        foreach (string line in l_info)
                        {
                            imprimir.AddHeaderLine(line);

                        }

                        try
                        {

                            imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

                        }
                        catch (Exception exx)
                        {

                        }

                        while (contador < Properties.Settings.Default.NumeroTicketsDeposito)
                        {
                            imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                            imprimir.AddFooterLine("COMPROBANTE COPIA para Usuario: " + Globales.IdUsuario);
                            contador++;
                        }

                        imprimir.Dispose();




                        Application.DoEvents();

                    }
                }
            }else
                using (FormaError f_Error = new FormaError(true, "warning"))
                {
                    f_Error.c_MensajeError.Text = "No se Encuentra Scanner de Cheques";
                    f_Error.ShowDialog();
                }

            c_Cheque.Enabled = false;
        }

        private String[] Leer_ticket(string path)
        {
            if (File.Exists(Application.StartupPath + path))
            {
                String[] lineas = File.ReadAllLines(Application.StartupPath + path);

                if (lineas.Length > 0)
                {
                    return lineas;
                }
                else
                    return null;
            }
            else
                return null;
        }

        private void c_leyenda_Click(object sender, EventArgs e)
        {

        }

        private void c_group_Numeros_Enter( object sender , EventArgs e )
        {

        }

        private void c_Mostar_Click( object sender , EventArgs e )
        {
            if ( gb_alfa.Visible )
                    gb_alfa.Visible = false;

            else
                gb_alfa.Visible = true;
        }
    }
}
