﻿using System;

namespace Azteca_SID
{
    partial class FormaVerificarcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_volver = new System.Windows.Forms.PictureBox();
            this.c_continuar = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_leyenda = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.c_Boton0 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Referencia = new System.Windows.Forms.TextBox();
            this.c_mensajeCuenta = new System.Windows.Forms.Label();
            this.c_Boton2 = new System.Windows.Forms.PictureBox();
            this.c_Boton3 = new System.Windows.Forms.PictureBox();
            this.c_Boton4 = new System.Windows.Forms.PictureBox();
            this.c_Boton5 = new System.Windows.Forms.PictureBox();
            this.c_Boton6 = new System.Windows.Forms.PictureBox();
            this.c_Boton7 = new System.Windows.Forms.PictureBox();
            this.c_Boton8 = new System.Windows.Forms.PictureBox();
            this.c_Boton9 = new System.Windows.Forms.PictureBox();
            this.c_BackSpace = new System.Windows.Forms.PictureBox();
            this.c_Boton1 = new System.Windows.Forms.PictureBox();
            this.c_group_Numeros = new System.Windows.Forms.GroupBox();
            this.c_Mostar = new System.Windows.Forms.PictureBox();
            this.c_Cheque = new System.Windows.Forms.Button();
            this.g_referencia = new System.Windows.Forms.GroupBox();
            this.gb_alfa = new System.Windows.Forms.GroupBox();
            this.c_V = new System.Windows.Forms.PictureBox();
            this.c_Z = new System.Windows.Forms.PictureBox();
            this.c_X = new System.Windows.Forms.PictureBox();
            this.c_Y = new System.Windows.Forms.PictureBox();
            this.c_W = new System.Windows.Forms.PictureBox();
            this.c_U = new System.Windows.Forms.PictureBox();
            this.c_T = new System.Windows.Forms.PictureBox();
            this.c_S = new System.Windows.Forms.PictureBox();
            this.c_R = new System.Windows.Forms.PictureBox();
            this.c_Q = new System.Windows.Forms.PictureBox();
            this.c_P = new System.Windows.Forms.PictureBox();
            this.c_O = new System.Windows.Forms.PictureBox();
            this.c_N = new System.Windows.Forms.PictureBox();
            this.c_L = new System.Windows.Forms.PictureBox();
            this.c_J = new System.Windows.Forms.PictureBox();
            this.c_K = new System.Windows.Forms.PictureBox();
            this.c_I = new System.Windows.Forms.PictureBox();
            this.c_M = new System.Windows.Forms.PictureBox();
            this.c_H = new System.Windows.Forms.PictureBox();
            this.c_G = new System.Windows.Forms.PictureBox();
            this.c_F = new System.Windows.Forms.PictureBox();
            this.c_E = new System.Windows.Forms.PictureBox();
            this.c_D = new System.Windows.Forms.PictureBox();
            this.c_C = new System.Windows.Forms.PictureBox();
            this.c_B = new System.Windows.Forms.PictureBox();
            this.c_A = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_continuar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BackSpace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton1)).BeginInit();
            this.c_group_Numeros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Mostar)).BeginInit();
            this.g_referencia.SuspendLayout();
            this.gb_alfa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_V)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Z)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_W)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_U)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_T)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Q)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_P)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_O)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_J)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_I)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_M)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_H)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_G)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_E)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_D)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_C)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // c_volver
            // 
            this.c_volver.Image = global::Azteca_SID.Properties.Resources.btn_banorte_volver;
            this.c_volver.Location = new System.Drawing.Point(0, 519);
            this.c_volver.Name = "c_volver";
            this.c_volver.Size = new System.Drawing.Size(127, 48);
            this.c_volver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_volver.TabIndex = 4;
            this.c_volver.TabStop = false;
            this.c_volver.Click += new System.EventHandler(this.c_volver_Click);
            // 
            // c_continuar
            // 
            this.c_continuar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_continuar;
            this.c_continuar.Location = new System.Drawing.Point(670, 519);
            this.c_continuar.Name = "c_continuar";
            this.c_continuar.Size = new System.Drawing.Size(127, 48);
            this.c_continuar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_continuar.TabIndex = 5;
            this.c_continuar.TabStop = false;
            this.c_continuar.Click += new System.EventHandler(this.c_continuar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(319, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "VERIFIQUE";
            // 
            // c_leyenda
            // 
            this.c_leyenda.BackColor = System.Drawing.Color.Transparent;
            this.c_leyenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_leyenda.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.c_leyenda.Location = new System.Drawing.Point(194, 97);
            this.c_leyenda.Name = "c_leyenda";
            this.c_leyenda.Size = new System.Drawing.Size(305, 50);
            this.c_leyenda.TabIndex = 7;
            this.c_leyenda.Text = "Verifique";
            this.c_leyenda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.c_leyenda.Click += new System.EventHandler(this.c_leyenda_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.pesos;
            this.pictureBox3.Location = new System.Drawing.Point(355, 148);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(144, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_cuenta.Enabled = false;
            this.c_cuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.Location = new System.Drawing.Point(47, 147);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(307, 41);
            this.c_cuenta.TabIndex = 9;
            this.c_cuenta.Text = "******XXXX";
            // 
            // c_Boton0
            // 
            this.c_Boton0.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton0.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_0;
            this.c_Boton0.Location = new System.Drawing.Point(104, 203);
            this.c_Boton0.Name = "c_Boton0";
            this.c_Boton0.Size = new System.Drawing.Size(69, 58);
            this.c_Boton0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton0.TabIndex = 50;
            this.c_Boton0.TabStop = false;
            this.c_Boton0.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(4, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(542, 29);
            this.label2.TabIndex = 49;
            this.label2.Text = "REFERENCIA  NUMERICA / ALFANUMERICA";
            // 
            // c_Referencia
            // 
            this.c_Referencia.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_Referencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c_Referencia.Enabled = false;
            this.c_Referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Referencia.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.c_Referencia.Location = new System.Drawing.Point(8, 45);
            this.c_Referencia.Margin = new System.Windows.Forms.Padding(5);
            this.c_Referencia.MaxLength = 10;
            this.c_Referencia.Name = "c_Referencia";
            this.c_Referencia.Size = new System.Drawing.Size(460, 34);
            this.c_Referencia.TabIndex = 48;
            this.c_Referencia.Text = "Ingrese su Referencia";
            this.c_Referencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_mensajeCuenta
            // 
            this.c_mensajeCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_mensajeCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mensajeCuenta.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.c_mensajeCuenta.Location = new System.Drawing.Point(67, 220);
            this.c_mensajeCuenta.Name = "c_mensajeCuenta";
            this.c_mensajeCuenta.Size = new System.Drawing.Size(401, 57);
            this.c_mensajeCuenta.TabIndex = 47;
            this.c_mensajeCuenta.Text = "Ingrese una Referencia.                         DEPOSITO REFERENCIADO.";
            this.c_mensajeCuenta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_Boton2
            // 
            this.c_Boton2.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton2.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_2;
            this.c_Boton2.Location = new System.Drawing.Point(105, 12);
            this.c_Boton2.Name = "c_Boton2";
            this.c_Boton2.Size = new System.Drawing.Size(69, 58);
            this.c_Boton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton2.TabIndex = 46;
            this.c_Boton2.TabStop = false;
            this.c_Boton2.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton3
            // 
            this.c_Boton3.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton3.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_3;
            this.c_Boton3.Location = new System.Drawing.Point(185, 12);
            this.c_Boton3.Name = "c_Boton3";
            this.c_Boton3.Size = new System.Drawing.Size(69, 58);
            this.c_Boton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton3.TabIndex = 45;
            this.c_Boton3.TabStop = false;
            this.c_Boton3.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton4
            // 
            this.c_Boton4.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton4.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_4;
            this.c_Boton4.Location = new System.Drawing.Point(25, 75);
            this.c_Boton4.Name = "c_Boton4";
            this.c_Boton4.Size = new System.Drawing.Size(69, 58);
            this.c_Boton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton4.TabIndex = 44;
            this.c_Boton4.TabStop = false;
            this.c_Boton4.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton5
            // 
            this.c_Boton5.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton5.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_5;
            this.c_Boton5.Location = new System.Drawing.Point(105, 75);
            this.c_Boton5.Name = "c_Boton5";
            this.c_Boton5.Size = new System.Drawing.Size(69, 58);
            this.c_Boton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton5.TabIndex = 43;
            this.c_Boton5.TabStop = false;
            this.c_Boton5.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton6
            // 
            this.c_Boton6.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton6.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_6;
            this.c_Boton6.Location = new System.Drawing.Point(185, 75);
            this.c_Boton6.Name = "c_Boton6";
            this.c_Boton6.Size = new System.Drawing.Size(69, 58);
            this.c_Boton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton6.TabIndex = 42;
            this.c_Boton6.TabStop = false;
            this.c_Boton6.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton7
            // 
            this.c_Boton7.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton7.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_7;
            this.c_Boton7.Location = new System.Drawing.Point(25, 139);
            this.c_Boton7.Name = "c_Boton7";
            this.c_Boton7.Size = new System.Drawing.Size(69, 58);
            this.c_Boton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton7.TabIndex = 41;
            this.c_Boton7.TabStop = false;
            this.c_Boton7.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton8
            // 
            this.c_Boton8.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton8.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_8;
            this.c_Boton8.Location = new System.Drawing.Point(105, 138);
            this.c_Boton8.Name = "c_Boton8";
            this.c_Boton8.Size = new System.Drawing.Size(69, 58);
            this.c_Boton8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton8.TabIndex = 40;
            this.c_Boton8.TabStop = false;
            this.c_Boton8.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton9
            // 
            this.c_Boton9.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton9.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_9;
            this.c_Boton9.Location = new System.Drawing.Point(185, 138);
            this.c_Boton9.Name = "c_Boton9";
            this.c_Boton9.Size = new System.Drawing.Size(69, 58);
            this.c_Boton9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton9.TabIndex = 39;
            this.c_Boton9.TabStop = false;
            this.c_Boton9.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_BackSpace
            // 
            this.c_BackSpace.BackColor = System.Drawing.Color.Transparent;
            this.c_BackSpace.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_borrar;
            this.c_BackSpace.Location = new System.Drawing.Point(23, 203);
            this.c_BackSpace.Name = "c_BackSpace";
            this.c_BackSpace.Size = new System.Drawing.Size(69, 58);
            this.c_BackSpace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_BackSpace.TabIndex = 38;
            this.c_BackSpace.TabStop = false;
            this.c_BackSpace.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton1
            // 
            this.c_Boton1.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton1.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_1;
            this.c_Boton1.Location = new System.Drawing.Point(25, 11);
            this.c_Boton1.Name = "c_Boton1";
            this.c_Boton1.Size = new System.Drawing.Size(69, 58);
            this.c_Boton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Boton1.TabIndex = 37;
            this.c_Boton1.TabStop = false;
            this.c_Boton1.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_group_Numeros
            // 
            this.c_group_Numeros.Controls.Add(this.c_Mostar);
            this.c_group_Numeros.Controls.Add(this.c_Boton0);
            this.c_group_Numeros.Controls.Add(this.c_Boton2);
            this.c_group_Numeros.Controls.Add(this.c_Boton3);
            this.c_group_Numeros.Controls.Add(this.c_Boton4);
            this.c_group_Numeros.Controls.Add(this.c_Boton5);
            this.c_group_Numeros.Controls.Add(this.c_Boton6);
            this.c_group_Numeros.Controls.Add(this.c_Boton7);
            this.c_group_Numeros.Controls.Add(this.c_Boton8);
            this.c_group_Numeros.Controls.Add(this.c_Boton9);
            this.c_group_Numeros.Controls.Add(this.c_BackSpace);
            this.c_group_Numeros.Controls.Add(this.c_Boton1);
            this.c_group_Numeros.Location = new System.Drawing.Point(508, 86);
            this.c_group_Numeros.Name = "c_group_Numeros";
            this.c_group_Numeros.Size = new System.Drawing.Size(280, 273);
            this.c_group_Numeros.TabIndex = 51;
            this.c_group_Numeros.TabStop = false;
            this.c_group_Numeros.Enter += new System.EventHandler(this.c_group_Numeros_Enter);
            // 
            // c_Mostar
            // 
            this.c_Mostar.BackColor = System.Drawing.Color.White;
            this.c_Mostar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c_Mostar.Image = global::Azteca_SID.Properties.Resources.insert_text_2;
            this.c_Mostar.Location = new System.Drawing.Point(184, 203);
            this.c_Mostar.Name = "c_Mostar";
            this.c_Mostar.Size = new System.Drawing.Size(69, 58);
            this.c_Mostar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Mostar.TabIndex = 51;
            this.c_Mostar.TabStop = false;
            this.c_Mostar.Click += new System.EventHandler(this.c_Mostar_Click);
            // 
            // c_Cheque
            // 
            this.c_Cheque.BackColor = System.Drawing.Color.Transparent;
            this.c_Cheque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_Cheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cheque.Image = global::Azteca_SID.Properties.Resources.bills;
            this.c_Cheque.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.c_Cheque.Location = new System.Drawing.Point(302, 242);
            this.c_Cheque.Name = "c_Cheque";
            this.c_Cheque.Size = new System.Drawing.Size(197, 80);
            this.c_Cheque.TabIndex = 52;
            this.c_Cheque.Text = "Procesar CHEQUE";
            this.c_Cheque.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_Cheque.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.c_Cheque.UseVisualStyleBackColor = false;
            this.c_Cheque.Visible = false;
            this.c_Cheque.Click += new System.EventHandler(this.c_cheques_Click);
            // 
            // g_referencia
            // 
            this.g_referencia.Controls.Add(this.label2);
            this.g_referencia.Controls.Add(this.c_Referencia);
            this.g_referencia.Location = new System.Drawing.Point(16, 276);
            this.g_referencia.Name = "g_referencia";
            this.g_referencia.Size = new System.Drawing.Size(483, 92);
            this.g_referencia.TabIndex = 53;
            this.g_referencia.TabStop = false;
            // 
            // gb_alfa
            // 
            this.gb_alfa.Controls.Add(this.c_V);
            this.gb_alfa.Controls.Add(this.c_Z);
            this.gb_alfa.Controls.Add(this.c_X);
            this.gb_alfa.Controls.Add(this.c_Y);
            this.gb_alfa.Controls.Add(this.c_W);
            this.gb_alfa.Controls.Add(this.c_U);
            this.gb_alfa.Controls.Add(this.c_T);
            this.gb_alfa.Controls.Add(this.c_S);
            this.gb_alfa.Controls.Add(this.c_R);
            this.gb_alfa.Controls.Add(this.c_Q);
            this.gb_alfa.Controls.Add(this.c_P);
            this.gb_alfa.Controls.Add(this.c_O);
            this.gb_alfa.Controls.Add(this.c_N);
            this.gb_alfa.Controls.Add(this.c_L);
            this.gb_alfa.Controls.Add(this.c_J);
            this.gb_alfa.Controls.Add(this.c_K);
            this.gb_alfa.Controls.Add(this.c_I);
            this.gb_alfa.Controls.Add(this.c_M);
            this.gb_alfa.Controls.Add(this.c_H);
            this.gb_alfa.Controls.Add(this.c_G);
            this.gb_alfa.Controls.Add(this.c_F);
            this.gb_alfa.Controls.Add(this.c_E);
            this.gb_alfa.Controls.Add(this.c_D);
            this.gb_alfa.Controls.Add(this.c_C);
            this.gb_alfa.Controls.Add(this.c_B);
            this.gb_alfa.Controls.Add(this.c_A);
            this.gb_alfa.Location = new System.Drawing.Point(12, 360);
            this.gb_alfa.Name = "gb_alfa";
            this.gb_alfa.Size = new System.Drawing.Size(776, 141);
            this.gb_alfa.TabIndex = 54;
            this.gb_alfa.TabStop = false;
            // 
            // c_V
            // 
            this.c_V.BackColor = System.Drawing.Color.White;
            this.c_V.Image = global::Azteca_SID.Properties.Resources.bb_teclado_V;
            this.c_V.Location = new System.Drawing.Point(476, 84);
            this.c_V.Name = "c_V";
            this.c_V.Size = new System.Drawing.Size(55, 50);
            this.c_V.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_V.TabIndex = 71;
            this.c_V.TabStop = false;
            this.c_V.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Z
            // 
            this.c_Z.BackColor = System.Drawing.Color.White;
            this.c_Z.Image = global::Azteca_SID.Properties.Resources.bb_teclado_Z;
            this.c_Z.Location = new System.Drawing.Point(711, 84);
            this.c_Z.Name = "c_Z";
            this.c_Z.Size = new System.Drawing.Size(55, 50);
            this.c_Z.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Z.TabIndex = 70;
            this.c_Z.TabStop = false;
            this.c_Z.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_X
            // 
            this.c_X.BackColor = System.Drawing.Color.White;
            this.c_X.Image = global::Azteca_SID.Properties.Resources.bb_teclado_X;
            this.c_X.Location = new System.Drawing.Point(595, 84);
            this.c_X.Name = "c_X";
            this.c_X.Size = new System.Drawing.Size(55, 50);
            this.c_X.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_X.TabIndex = 69;
            this.c_X.TabStop = false;
            this.c_X.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Y
            // 
            this.c_Y.BackColor = System.Drawing.Color.White;
            this.c_Y.Image = global::Azteca_SID.Properties.Resources.bb_teclado_Y;
            this.c_Y.Location = new System.Drawing.Point(652, 84);
            this.c_Y.Name = "c_Y";
            this.c_Y.Size = new System.Drawing.Size(55, 50);
            this.c_Y.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Y.TabIndex = 68;
            this.c_Y.TabStop = false;
            this.c_Y.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_W
            // 
            this.c_W.BackColor = System.Drawing.Color.White;
            this.c_W.Image = global::Azteca_SID.Properties.Resources.bb_teclado_W;
            this.c_W.Location = new System.Drawing.Point(534, 84);
            this.c_W.Name = "c_W";
            this.c_W.Size = new System.Drawing.Size(55, 50);
            this.c_W.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_W.TabIndex = 67;
            this.c_W.TabStop = false;
            this.c_W.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_U
            // 
            this.c_U.BackColor = System.Drawing.Color.White;
            this.c_U.Image = global::Azteca_SID.Properties.Resources.bb_teclado_U;
            this.c_U.Location = new System.Drawing.Point(417, 84);
            this.c_U.Name = "c_U";
            this.c_U.Size = new System.Drawing.Size(55, 50);
            this.c_U.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_U.TabIndex = 65;
            this.c_U.TabStop = false;
            this.c_U.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_T
            // 
            this.c_T.BackColor = System.Drawing.Color.White;
            this.c_T.Image = global::Azteca_SID.Properties.Resources.bb_teclado_T;
            this.c_T.Location = new System.Drawing.Point(358, 84);
            this.c_T.Name = "c_T";
            this.c_T.Size = new System.Drawing.Size(55, 50);
            this.c_T.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_T.TabIndex = 64;
            this.c_T.TabStop = false;
            this.c_T.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_S
            // 
            this.c_S.BackColor = System.Drawing.Color.White;
            this.c_S.Image = global::Azteca_SID.Properties.Resources.bb_teclado_S;
            this.c_S.Location = new System.Drawing.Point(299, 84);
            this.c_S.Name = "c_S";
            this.c_S.Size = new System.Drawing.Size(55, 50);
            this.c_S.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_S.TabIndex = 63;
            this.c_S.TabStop = false;
            this.c_S.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_R
            // 
            this.c_R.BackColor = System.Drawing.Color.White;
            this.c_R.Image = global::Azteca_SID.Properties.Resources.bb_teclado_R;
            this.c_R.Location = new System.Drawing.Point(240, 84);
            this.c_R.Name = "c_R";
            this.c_R.Size = new System.Drawing.Size(55, 50);
            this.c_R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_R.TabIndex = 62;
            this.c_R.TabStop = false;
            this.c_R.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Q
            // 
            this.c_Q.BackColor = System.Drawing.Color.White;
            this.c_Q.Image = global::Azteca_SID.Properties.Resources.bb_teclado_Q;
            this.c_Q.Location = new System.Drawing.Point(181, 84);
            this.c_Q.Name = "c_Q";
            this.c_Q.Size = new System.Drawing.Size(55, 50);
            this.c_Q.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Q.TabIndex = 61;
            this.c_Q.TabStop = false;
            this.c_Q.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_P
            // 
            this.c_P.BackColor = System.Drawing.Color.White;
            this.c_P.Image = global::Azteca_SID.Properties.Resources.bb_teclado_P;
            this.c_P.Location = new System.Drawing.Point(122, 84);
            this.c_P.Name = "c_P";
            this.c_P.Size = new System.Drawing.Size(55, 50);
            this.c_P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_P.TabIndex = 60;
            this.c_P.TabStop = false;
            this.c_P.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_O
            // 
            this.c_O.BackColor = System.Drawing.Color.White;
            this.c_O.Image = global::Azteca_SID.Properties.Resources.bb_teclado_O;
            this.c_O.Location = new System.Drawing.Point(63, 84);
            this.c_O.Name = "c_O";
            this.c_O.Size = new System.Drawing.Size(55, 50);
            this.c_O.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_O.TabIndex = 59;
            this.c_O.TabStop = false;
            this.c_O.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_N
            // 
            this.c_N.BackColor = System.Drawing.Color.White;
            this.c_N.Image = global::Azteca_SID.Properties.Resources.bb_teclado_N;
            this.c_N.Location = new System.Drawing.Point(4, 84);
            this.c_N.Name = "c_N";
            this.c_N.Size = new System.Drawing.Size(55, 50);
            this.c_N.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_N.TabIndex = 58;
            this.c_N.TabStop = false;
            this.c_N.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_L
            // 
            this.c_L.BackColor = System.Drawing.Color.White;
            this.c_L.Image = global::Azteca_SID.Properties.Resources.bb_teclado_L;
            this.c_L.Location = new System.Drawing.Point(652, 17);
            this.c_L.Name = "c_L";
            this.c_L.Size = new System.Drawing.Size(55, 50);
            this.c_L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_L.TabIndex = 57;
            this.c_L.TabStop = false;
            this.c_L.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_J
            // 
            this.c_J.BackColor = System.Drawing.Color.White;
            this.c_J.Image = global::Azteca_SID.Properties.Resources.bb_teclado_J;
            this.c_J.Location = new System.Drawing.Point(534, 17);
            this.c_J.Name = "c_J";
            this.c_J.Size = new System.Drawing.Size(55, 50);
            this.c_J.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_J.TabIndex = 56;
            this.c_J.TabStop = false;
            this.c_J.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_K
            // 
            this.c_K.BackColor = System.Drawing.Color.White;
            this.c_K.Image = global::Azteca_SID.Properties.Resources.bb_teclado_K;
            this.c_K.Location = new System.Drawing.Point(593, 17);
            this.c_K.Name = "c_K";
            this.c_K.Size = new System.Drawing.Size(55, 50);
            this.c_K.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_K.TabIndex = 55;
            this.c_K.TabStop = false;
            this.c_K.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_I
            // 
            this.c_I.BackColor = System.Drawing.Color.White;
            this.c_I.Image = global::Azteca_SID.Properties.Resources.bb_teclado_I;
            this.c_I.Location = new System.Drawing.Point(475, 17);
            this.c_I.Name = "c_I";
            this.c_I.Size = new System.Drawing.Size(55, 50);
            this.c_I.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_I.TabIndex = 54;
            this.c_I.TabStop = false;
            this.c_I.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_M
            // 
            this.c_M.BackColor = System.Drawing.Color.White;
            this.c_M.Image = global::Azteca_SID.Properties.Resources.bb_teclado_M;
            this.c_M.Location = new System.Drawing.Point(711, 17);
            this.c_M.Name = "c_M";
            this.c_M.Size = new System.Drawing.Size(55, 50);
            this.c_M.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_M.TabIndex = 53;
            this.c_M.TabStop = false;
            this.c_M.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_H
            // 
            this.c_H.BackColor = System.Drawing.Color.White;
            this.c_H.Image = global::Azteca_SID.Properties.Resources.bb_teclado_H;
            this.c_H.Location = new System.Drawing.Point(416, 17);
            this.c_H.Name = "c_H";
            this.c_H.Size = new System.Drawing.Size(55, 50);
            this.c_H.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_H.TabIndex = 52;
            this.c_H.TabStop = false;
            this.c_H.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_G
            // 
            this.c_G.BackColor = System.Drawing.Color.White;
            this.c_G.Image = global::Azteca_SID.Properties.Resources.bb_teclado_G;
            this.c_G.Location = new System.Drawing.Point(357, 17);
            this.c_G.Name = "c_G";
            this.c_G.Size = new System.Drawing.Size(55, 50);
            this.c_G.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_G.TabIndex = 50;
            this.c_G.TabStop = false;
            this.c_G.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_F
            // 
            this.c_F.BackColor = System.Drawing.Color.White;
            this.c_F.Image = global::Azteca_SID.Properties.Resources.bb_teclado_F;
            this.c_F.Location = new System.Drawing.Point(298, 17);
            this.c_F.Name = "c_F";
            this.c_F.Size = new System.Drawing.Size(55, 50);
            this.c_F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_F.TabIndex = 48;
            this.c_F.TabStop = false;
            this.c_F.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_E
            // 
            this.c_E.BackColor = System.Drawing.Color.White;
            this.c_E.Image = global::Azteca_SID.Properties.Resources.bb_teclado_E;
            this.c_E.Location = new System.Drawing.Point(239, 17);
            this.c_E.Name = "c_E";
            this.c_E.Size = new System.Drawing.Size(55, 50);
            this.c_E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_E.TabIndex = 46;
            this.c_E.TabStop = false;
            this.c_E.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_D
            // 
            this.c_D.BackColor = System.Drawing.Color.White;
            this.c_D.Image = global::Azteca_SID.Properties.Resources.bb_teclado_D;
            this.c_D.Location = new System.Drawing.Point(180, 17);
            this.c_D.Name = "c_D";
            this.c_D.Size = new System.Drawing.Size(55, 50);
            this.c_D.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_D.TabIndex = 44;
            this.c_D.TabStop = false;
            this.c_D.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_C
            // 
            this.c_C.BackColor = System.Drawing.Color.White;
            this.c_C.Image = global::Azteca_SID.Properties.Resources.bb_teclado_C;
            this.c_C.Location = new System.Drawing.Point(121, 17);
            this.c_C.Name = "c_C";
            this.c_C.Size = new System.Drawing.Size(55, 50);
            this.c_C.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_C.TabIndex = 42;
            this.c_C.TabStop = false;
            this.c_C.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_B
            // 
            this.c_B.BackColor = System.Drawing.Color.White;
            this.c_B.Image = global::Azteca_SID.Properties.Resources.bb_teclado_B;
            this.c_B.Location = new System.Drawing.Point(62, 17);
            this.c_B.Name = "c_B";
            this.c_B.Size = new System.Drawing.Size(55, 50);
            this.c_B.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_B.TabIndex = 40;
            this.c_B.TabStop = false;
            this.c_B.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_A
            // 
            this.c_A.BackColor = System.Drawing.Color.White;
            this.c_A.Image = global::Azteca_SID.Properties.Resources.bb_teclado_A;
            this.c_A.Location = new System.Drawing.Point(3, 17);
            this.c_A.Name = "c_A";
            this.c_A.Size = new System.Drawing.Size(55, 50);
            this.c_A.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_A.TabIndex = 38;
            this.c_A.TabStop = false;
            this.c_A.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Azteca_SID.Properties.Resources.Imagen1;
            this.pictureBox4.Location = new System.Drawing.Point(13, 56);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(175, 76);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 55;
            this.pictureBox4.TabStop = false;
            // 
            // FormaVerificarcs
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.gb_alfa);
            this.Controls.Add(this.g_referencia);
            this.Controls.Add(this.c_group_Numeros);
            this.Controls.Add(this.c_mensajeCuenta);
            this.Controls.Add(this.c_cuenta);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.c_leyenda);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_continuar);
            this.Controls.Add(this.c_volver);
            this.Controls.Add(this.c_Cheque);
            this.Name = "FormaVerificarcs";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.FormaVerificarcs_Load);
            this.Controls.SetChildIndex(this.c_Cheque, 0);
            this.Controls.SetChildIndex(this.c_volver, 0);
            this.Controls.SetChildIndex(this.c_continuar, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_leyenda, 0);
            this.Controls.SetChildIndex(this.pictureBox3, 0);
            this.Controls.SetChildIndex(this.c_cuenta, 0);
            this.Controls.SetChildIndex(this.c_mensajeCuenta, 0);
            this.Controls.SetChildIndex(this.c_group_Numeros, 0);
            this.Controls.SetChildIndex(this.g_referencia, 0);
            this.Controls.SetChildIndex(this.gb_alfa, 0);
            this.Controls.SetChildIndex(this.pictureBox4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_continuar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BackSpace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton1)).EndInit();
            this.c_group_Numeros.ResumeLayout(false);
            this.c_group_Numeros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Mostar)).EndInit();
            this.g_referencia.ResumeLayout(false);
            this.g_referencia.PerformLayout();
            this.gb_alfa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c_V)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Z)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_W)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_U)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_T)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Q)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_P)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_O)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_J)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_I)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_M)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_H)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_G)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_E)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_D)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_C)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox c_volver;
        private System.Windows.Forms.PictureBox c_continuar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label c_leyenda;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.PictureBox c_Boton0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Referencia;
        private System.Windows.Forms.Label c_mensajeCuenta;
        private System.Windows.Forms.PictureBox c_Boton2;
        private System.Windows.Forms.PictureBox c_Boton3;
        private System.Windows.Forms.PictureBox c_Boton4;
        private System.Windows.Forms.PictureBox c_Boton5;
        private System.Windows.Forms.PictureBox c_Boton6;
        private System.Windows.Forms.PictureBox c_Boton7;
        private System.Windows.Forms.PictureBox c_Boton8;
        private System.Windows.Forms.PictureBox c_Boton9;
        private System.Windows.Forms.PictureBox c_BackSpace;
        private System.Windows.Forms.PictureBox c_Boton1;
        private System.Windows.Forms.GroupBox c_group_Numeros;
        private System.Windows.Forms.Button c_Cheque;
        private System.Windows.Forms.GroupBox g_referencia;
        private System.Windows.Forms.GroupBox gb_alfa;
        private System.Windows.Forms.PictureBox c_Z;
        private System.Windows.Forms.PictureBox c_X;
        private System.Windows.Forms.PictureBox c_Y;
        private System.Windows.Forms.PictureBox c_W;
        private System.Windows.Forms.PictureBox c_U;
        private System.Windows.Forms.PictureBox c_T;
        private System.Windows.Forms.PictureBox c_S;
        private System.Windows.Forms.PictureBox c_R;
        private System.Windows.Forms.PictureBox c_Q;
        private System.Windows.Forms.PictureBox c_P;
        private System.Windows.Forms.PictureBox c_O;
        private System.Windows.Forms.PictureBox c_N;
        private System.Windows.Forms.PictureBox c_L;
        private System.Windows.Forms.PictureBox c_J;
        private System.Windows.Forms.PictureBox c_K;
        private System.Windows.Forms.PictureBox c_I;
        private System.Windows.Forms.PictureBox c_M;
        private System.Windows.Forms.PictureBox c_H;
        private System.Windows.Forms.PictureBox c_G;
        private System.Windows.Forms.PictureBox c_F;
        private System.Windows.Forms.PictureBox c_E;
        private System.Windows.Forms.PictureBox c_D;
        private System.Windows.Forms.PictureBox c_C;
        private System.Windows.Forms.PictureBox c_B;
        private System.Windows.Forms.PictureBox c_A;
        private System.Windows.Forms.PictureBox c_V;
        private System.Windows.Forms.PictureBox c_Mostar;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}
