﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaConsultaOperaciones : FormBase
    {

        DataTable t_Operacoiones;
        DataTable t_Depositos_Pendientes;
        DataTable t_Retiros_Pendientes;
        public FormaConsultaOperaciones()
        {
            InitializeComponent();
        }

        private void c_numoperaciones_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormaConsultaOperaciones_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            // Operaciones

            CargarDatos();


        }

        private void CargarDatos()
        {
            t_Operacoiones = BDConsulta.ObtenerConsultaOperaciones(c_fechabox.Value);
            c_ConsultaOperaciones.DataSource = t_Operacoiones;
            c_ConsultaOperaciones.Columns[0].HeaderText = "Folio";
            c_ConsultaOperaciones.Columns[1].HeaderText = "Clave";
            c_ConsultaOperaciones.Columns[2].HeaderText = "Operacion";
            c_ConsultaOperaciones.Columns[3].HeaderText = "Monto";
            c_ConsultaOperaciones.Columns[4].HeaderText = "Moneda";
            c_ConsultaOperaciones.Columns[5].HeaderText = "Fecha/Hora";
            c_ConsultaOperaciones.Columns[6].HeaderText = "Clave";

            if (c_ConsultaOperaciones.RowCount == 0)
            {
                c_ConsultaOperaciones.DataSource = null;
                c_numoperaciones.Text = "0";
                
            }
            else
                c_numoperaciones.Text = c_ConsultaOperaciones.RowCount.ToString();

            // Depositos Pendientes
            t_Depositos_Pendientes = BDConsulta.ObtenerDepositosGSIPendientes();
            dg_depositos_pendientes.DataSource = t_Depositos_Pendientes;


            if (dg_depositos_pendientes.RowCount == 0)
            {
                dg_depositos_pendientes.DataSource = null;
                c_num_depositos_pendientes.Text = "0";
                c_reenviar_Deposito.Enabled = false;
            }
            else
            {
                c_num_depositos_pendientes.Text = dg_depositos_pendientes.RowCount.ToString();
                c_reenviar_Deposito.Enabled = true;
            }

            // Retiros Pendientes
            t_Retiros_Pendientes = BDConsulta.ObtenerRetirosGSIPendientes();
            dg_retiros_pendientes.DataSource = t_Retiros_Pendientes;

            Cursor.Hide();

            if (dg_retiros_pendientes.RowCount == 0)
            {
                dg_retiros_pendientes.DataSource = null;
                c_num_retiros_pendientes.Text = "0";
                c_Enviar_Retiro.Enabled = false;
            }
            else
            {
                c_num_retiros_pendientes.Text = dg_retiros_pendientes.RowCount.ToString();
                c_Enviar_Retiro.Enabled = true;
            }
        }

        private void c_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_fechabox_ValueChanged(object sender, EventArgs e)
        {
            t_Operacoiones = BDConsulta.ObtenerConsultaOperaciones(c_fechabox.Value);

            c_ConsultaOperaciones.DataSource = t_Operacoiones;
            c_ConsultaOperaciones.Columns[0].HeaderText = "Folio";
           // c_ConsultaOperaciones.Columns[1].HeaderText = "Nombre";
            //   c_ConsultaOperaciones.Columns[0].MinimumWidth = 150  ;
            c_ConsultaOperaciones.Columns[1].HeaderText = "Clave";
            c_ConsultaOperaciones.Columns[2].HeaderText = "Operación";
            c_ConsultaOperaciones.Columns[3].HeaderText = "Monto";
            c_ConsultaOperaciones.Columns[4].HeaderText = "Moneda";
            c_ConsultaOperaciones.Columns[5].HeaderText = "Fecha/Hora";
            c_ConsultaOperaciones.Columns[6].HeaderText = "Clave";

            if (c_ConsultaOperaciones.RowCount == 0)
            {
                c_ConsultaOperaciones.DataSource = null;
                c_numoperaciones.Text = "0";
            }
            else
                c_numoperaciones.Text = c_ConsultaOperaciones.RowCount.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string FolidID;
                string UserID;
                string Mondea;
                string Monto;
                string FechaHora;
                string Operacion;
                DateTime l_FechaHora;
                DataTable l_Detalle;
                string Fecha="";
                string Clave;
                String Hora="";
                int IdMoneda = 0;
                string referencia = string.Empty;

                int l_numbilletes = 0;

                FolidID = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[0].Value.ToString();
                UserID = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[1].Value.ToString();
                Operacion = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[2].Value.ToString();
                Mondea = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[4].Value.ToString();
                Monto = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[3].Value.ToString();
                FechaHora = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[5].Value.ToString();
                Clave = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[6].Value.ToString();
                referencia = c_ConsultaOperaciones.Rows[c_ConsultaOperaciones.SelectedRows[0].Index].Cells[7].Value.ToString();

                if ( DateTime.TryParse( FechaHora , out l_FechaHora ) )
                {
                    Fecha = l_FechaHora.ToShortDateString( );
                    Hora = l_FechaHora.ToString("HH:mm:ss");
                }

                if (Mondea == "Pesos MXP")
                {
                    IdMoneda = 1;
                }


                if (Operacion.Trim() == "RETIRO")
                   l_Detalle = BDRetiro.ObtenerDetalleRetiro(Int32.Parse(FolidID));
                else
               l_Detalle = BDDeposito.ObtenerDetalleDeposito(Int32.Parse(FolidID));


                if (l_Detalle != null)
                {
                    foreach (DataRow fila in l_Detalle.Rows)
                    {
                        l_numbilletes += (int)fila["Cantidad"];
                    }
                }

                string CuentaTmp = UserID;
                try
                {

                    CuentaTmp = CuentaTmp.Replace( System.Environment.NewLine , "" );

                    CuentaTmp = "********" + CuentaTmp.Substring( CuentaTmp.Length - 4 , 4 );

                }
                catch ( Exception E )
                {
                    //throw;
                    
                }
               //UserID = CuentaTmp;



                ReImprimir(FolidID,UserID, IdMoneda, Monto, l_numbilletes, l_Detalle, Fecha, Hora, Clave, referencia);
            } catch (Exception exx)
            { }

        }

        private void ReImprimir(string FolidID,string p_IdUsuario, int p_IdMoneda, string p_TotalTransaccion
                                , int l_numbilletes, DataTable l_Detalle, string Fecha, string Hora, string Clave,string referencia)
        {
            int contador = 0;


            ModulePrint imprimir = new ModulePrint();
            imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);

            imprimir.AddHeaderLine("REIMPRESION DE TICKET ");
            imprimir.AddHeaderLine("         BANCO MERCANTIL DEL NORTE, SA       ");

            try
            {
                String[] datos = Properties.Settings.Default.EdoCdUbi.Split( '|' );

                imprimir.AddHeaderLine( "Estado:         " + datos[0] );
                imprimir.AddHeaderLine( "Ciudad:         " + datos[1] );
                imprimir.AddHeaderLine( "Sucursal:      " + datos[2] );
                imprimir.AddHeaderLine("Fecha      " + Convert.ToDateTime(Fecha).ToString("yyyy-MM-dd"));
                imprimir.AddHeaderLine("Hora:      " + Convert.ToDateTime(Hora).ToString("HH:mm:ss"));
                imprimir.AddHeaderLine("Id Cajero:      " + Properties.Settings.Default.GsiCajero);
                imprimir.AddHeaderLine(Clave==string.Empty? "CR:      " : "CR:      " + Clave.Substring(6,4));
                imprimir.AddHeaderLine("#Usuario:      " + p_IdUsuario);
                imprimir.AddHeaderLine("#Secuencia:      " + FolidID.PadLeft(20,'0'));
                imprimir.AddHeaderLine("#Referencia:      " + referencia);
                imprimir.AddHeaderLine("Movimiento:        " + "Depósito Referenciado");
                imprimir.AddHeaderLine("Nombre de la empresa:           " + Globales.nombreEmisora);
                imprimir.AddHeaderLine("Divisa:         " + "MXP");
                imprimir.AddHeaderLine("Importe:         " + p_TotalTransaccion);
                imprimir.AddHeaderLine("Importe Letra:         " + enletras(p_TotalTransaccion.Replace("$","").Replace(",","")) + " PESOS 00/100 M.N.");

                imprimir.AddHeaderLine("Clave de Rastreo: " + Clave);
            }
            catch ( Exception exx )
            {
                imprimir.AddHeaderLine( "Estado:         " + "México" );
                imprimir.AddHeaderLine( "Ciudad:         " + "México" );
                imprimir.AddHeaderLine( "Sucursal:     " + "Suc. Banorte X" );
                Globales.EscribirBitacora( "Ticket " , "Escribir Cabezera" , Properties.Settings.Default.EdoCdUbi , 3 );
            }

            imprimir.AddHeaderLine(" ");
            //if (p_IdMoneda == 1)
            //    imprimir.AddHeaderLine("Moneda: Pesos");
            //else
            //    imprimir.AddHeaderLine("Moneda: Dólares");

            //imprimir.AddHeaderLine("Nombre: " + p_IdUsuario);
            //imprimir.AddHeaderLine(Globales.NombreUsuario.ToUpper());


            //imprimir.AddHeaderLine("Usuario: " + p_IdUsuario);
            //imprimir.AddHeaderLine("Cliente: " + Properties.Settings.Default.Cliente);
            // imprimir.AddHeaderLine("Cuenta: " + Globales.CuentaBanco);
            //imprimir.AddHeaderLine("Cuenta: " + p_IdUsuario);



            //imprimir.AddSubHeaderLine("Fecha: " + Fecha + " Hora: " + Hora);
            imprimir.AddHeaderLine("");
            foreach (DataRow l_Detalle1 in l_Detalle.Rows)
            {
                imprimir.AddItem(l_Detalle1[1].ToString(), Double.Parse(l_Detalle1[2].ToString()).ToString("$#,###,##0.00"),
                    Double.Parse(l_Detalle1[2].ToString()).ToString());
            }


            imprimir.AddFooterLine2(l_numbilletes.ToString(), "Total Depositado: " + p_TotalTransaccion);
            //imprimir.AddFooterLine("PIEZAS TOTALES:           " + l_numbilletes.ToString());

            //imprimir.AddFooterLine("Total Depósito: " + p_TotalTransaccion); //.ToString("$#,###,###,##0.00"));
            imprimir.AddFooterLine("");
            //imprimir.AddFooterLine("EMISIÓN: " + Properties.Settings.Default.Cliente);
            //imprimir.AddFooterLine("SERIAL EQUIPO: " + Properties.Settings.Default.NumSerialEquipo);

            //imprimir.AddFooterLine("Clave de Rastreo: "+ Clave);


           imprimir.AddFooterLine("REIMPRESION TIEMPO: " + DateTime.Now.ToString("dd/M/yyyy") + " " +DateTime.Now.ToShortTimeString());

            imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

            while (contador < Properties.Settings.Default.NumeroTicketsDeposito)
            {
                imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                imprimir.AddFooterLine("COMPROBANTE COPIA para Usuario: " + p_IdUsuario);
                contador++;
            }

            imprimir.Dispose();
        }

        public string enletras(string num)


        {


            string res, dec = "";


            Int64 entero;


            int decimales;


            double nro;


            try


            {


                nro = Convert.ToDouble(num);


            }


            catch


            {


                return "";


            }


            entero = Convert.ToInt64(Math.Truncate(nro));


            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));


            if (decimales > 0)


            {


                dec = " CON " +decimales.ToString() + "/ 100";


            }


            res = toText(Convert.ToDouble(entero)) + dec;


            return res;


        }


        private string toText(double value)


        {


            string Num2Text = "";


            value = Math.Truncate(value);


            if (value == 0) Num2Text = "CERO";


           else if (value == 1) Num2Text = "UNO";


           else if (value == 2) Num2Text = "DOS";


           else if (value == 3) Num2Text = "TRES";


           else if (value == 4) Num2Text = "CUATRO";


           else if (value == 5) Num2Text = "CINCO";


           else if (value == 6) Num2Text = "SEIS";


           else if (value == 7) Num2Text = "SIETE";


           else if (value == 8) Num2Text = "OCHO";


           else if (value == 9) Num2Text = "NUEVE";


           else if (value == 10) Num2Text = "DIEZ";


           else if (value == 11) Num2Text = "ONCE";


           else if (value == 12) Num2Text = "DOCE";


           else if (value == 13) Num2Text = "TRECE";


           else if (value == 14) Num2Text = "CATORCE";


           else if (value == 15) Num2Text = "QUINCE";


           else if (value < 20) Num2Text = "DIECI" +toText(value - 10);


           else if (value == 20) Num2Text = "VEINTE";


           else if (value < 30) Num2Text = "VEINTI" +toText(value - 20);


           else if (value == 30) Num2Text = "TREINTA";


           else if (value == 40) Num2Text = "CUARENTA";


           else if (value == 50) Num2Text = "CINCUENTA";


           else if (value == 60) Num2Text = "SESENTA";


           else if (value == 70) Num2Text = "SETENTA";


           else if (value == 80) Num2Text = "OCHENTA";


           else if (value == 90) Num2Text = "NOVENTA";


           else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " +toText(value % 10);


           else if (value == 100) Num2Text = "CIEN";


           else if (value < 200) Num2Text = "CIENTO " +toText(value - 100);


           else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";


           else if (value == 500) Num2Text = "QUINIENTOS";


           else if (value == 700) Num2Text = "SETECIENTOS";


           else if (value == 900) Num2Text = "NOVECIENTOS";


           else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " +toText(value % 100);


           else if (value == 1000) Num2Text = "MIL";


           else if (value < 2000) Num2Text = "MIL " +toText(value % 1000);


           else if (value < 1000000)


            {


                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";


                if ((value % 1000) > 0) Num2Text = Num2Text + " " +toText(value % 1000);


            }


            else if (value == 1000000) Num2Text = "UN MILLON";


           else if (value < 2000000) Num2Text = "UN MILLON " +toText(value % 1000000);


           else if (value < 1000000000000)


            {


                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";


                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " +toText(value - Math.Truncate(value / 1000000) * 1000000);


            }


            else if (value == 1000000000000) Num2Text = "UN BILLON";


           else if (value < 2000000000000) Num2Text = "UN BILLON " +toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);


           else


           {


                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";


                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " +toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);


            }


            return Num2Text;


        }

        private void c_reenviar_Deposito_Click(object sender, EventArgs e)
        {
            c_reenviar_Deposito.Enabled = false;

            if (dg_depositos_pendientes.SelectedRows.Count == 1)
            {
                string l_FolidID;
                string l_Cuenta;
                string l_GSICajero;
                string l_Divisa;
                string l_Monto;
                string l_fecha;
                string l_hora;
                string l_referencia;
                string l_envase;


                DataTable l_Detalle;
                Globales.GsiDeposito = null;

                l_FolidID = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[0].Value.ToString();
                l_Cuenta = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[1].Value.ToString();
                l_GSICajero = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[2].Value.ToString();
                l_Divisa = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[3].Value.ToString();
                l_Monto = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[4].Value.ToString();
                l_fecha = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[5].Value.ToString();
                l_hora = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[6].Value.ToString();
                l_referencia = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[7].Value.ToString();
                l_envase = dg_depositos_pendientes.Rows[dg_depositos_pendientes.SelectedRows[0].Index].Cells[8].Value.ToString();

                l_Detalle = BDDeposito.ObtenerDetalleDeposito(Int32.Parse(l_FolidID));


                //Globales.GsiDeposito = UtilsComunicacion.DepositoGsi(l_Cuenta, l_GSICajero, l_Divisa, l_fecha, l_hora, Int32.Parse(l_FolidID).ToString("D20")
                //                                  , l_referencia, l_envase, l_Detalle,null);
                Globales.GsiDeposito= UtilsComunicacion.PagoReferenciadoGSI(l_Monto, Globales.claveEmisora, "GSI901", "MXP" , l_fecha, l_hora
                                                               , Int32.Parse(l_FolidID).ToString("D20"), l_referencia, l_envase, l_Detalle);

                if (Globales.GsiDeposito != null)
                {
                    BDDeposito.UpdateDepositoGSI(Int32.Parse(l_FolidID));

                    if(Convert.ToBoolean( UtilsComunicacion.Desencriptar(Globales.GsiDeposito.exito))==true)
                    {
                        if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo)))
                            using (FormaError v_exito = new FormaError(true, "exito"))
                            {
                                v_exito.c_MensajeError.Text = "Deposito Reenviado y Actualizado";
                                v_exito.ShowDialog();
                            }
                    }
                    else
                    {
                        if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.mensaje)))
                            using (FormaError v_exito = new FormaError(true, "exito"))
                            {
                                v_exito.c_MensajeError.Text = "Deposito Reenviado y Actualizado";
                                v_exito.ShowDialog();
                            }
                    }

                    
                }else
                using (FormaError v_exito = new FormaError(true, "Error"))
                {
                    v_exito.c_MensajeError.Text = "No se pudo Reenviar el deposito intentelo mas tarde";
                    v_exito.ShowDialog();
                }

                CargarDatos();
            }
            c_reenviar_Deposito.Enabled = true;
        }

        private void c_Enviar_Retiro_Click(object sender, EventArgs e)
        {
            c_Enviar_Retiro.Enabled = false;

            if (dg_retiros_pendientes.SelectedRows.Count == 1)
            {

                string l_FolidID;
                string l_GSICajero;
                string l_Divisa;
                string l_fecha;
                string l_envase;


                DataTable l_Detalle;
                Globales.GsiRecoleccion = null;

                l_FolidID = dg_retiros_pendientes.Rows[dg_retiros_pendientes.SelectedRows[0].Index].Cells["IdRetiro"].Value.ToString();
                l_GSICajero = dg_retiros_pendientes.Rows[dg_retiros_pendientes.SelectedRows[0].Index].Cells["GsiCajero"].Value.ToString();
                l_Divisa = dg_retiros_pendientes.Rows[dg_retiros_pendientes.SelectedRows[0].Index].Cells["Divisa"].Value.ToString();
                l_fecha = dg_retiros_pendientes.Rows[dg_retiros_pendientes.SelectedRows[0].Index].Cells["Fecha"].Value.ToString();
                l_envase = dg_retiros_pendientes.Rows[dg_retiros_pendientes.SelectedRows[0].Index].Cells["Envase"].Value.ToString();

                l_Detalle = BDRetiro.ObtenerDetalleRetiro(Int32.Parse(l_FolidID));


                Globales.GsiRecoleccion = UtilsComunicacion.RecoleccionGsi(l_GSICajero, l_Divisa, l_fecha, Int32.Parse(l_FolidID).ToString("D20")
                                                 , l_envase, l_Detalle);

                if (Globales.GsiRecoleccion != null)
                {
                    BDRetiro.UpdateRetiroGSI(Int32.Parse(l_FolidID));
                    using (FormaError v_exito = new FormaError(true, "exito"))
                    {
                        v_exito.c_MensajeError.Text = "Retiro Reenviado";
                        v_exito.ShowDialog();
                    }
                }else
                using (FormaError v_exito = new FormaError(true, "Error"))
                {
                    v_exito.c_MensajeError.Text = "No se pudo Reenviar el Retiro intentelo mas tarde";
                    v_exito.ShowDialog();
                }



                CargarDatos();
            }

            c_Enviar_Retiro.Enabled = true;

        }
    }
}
