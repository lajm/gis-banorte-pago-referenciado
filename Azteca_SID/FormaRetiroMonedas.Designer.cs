﻿namespace Azteca_SID
{
    partial class FormaRetiroMonedas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_totalMonedas_b2 = new System.Windows.Forms.Label();
            this.c_bolsa2_10c = new System.Windows.Forms.Label();
            this.c_bolsa2_20c = new System.Windows.Forms.Label();
            this.c_bolsa2_50c = new System.Windows.Forms.Label();
            this.c_bolsa2_1 = new System.Windows.Forms.Label();
            this.c_bolsa2_2 = new System.Windows.Forms.Label();
            this.c_bolsa2_5 = new System.Windows.Forms.Label();
            this.c_bolsa2_10 = new System.Windows.Forms.Label();
            this.c_bolsa2_20 = new System.Windows.Forms.Label();
            this.c_bolsa1_10c = new System.Windows.Forms.Label();
            this.c_bolsa1_20c = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.c_totalbolsa2 = new System.Windows.Forms.Label();
            this.c_totalMonedas_b1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.c_imprimirMonedas = new System.Windows.Forms.Button();
            this.c_cerrarM = new System.Windows.Forms.Button();
            this.c_totalbolsa1 = new System.Windows.Forms.Label();
            this.c_bolsa1_50c = new System.Windows.Forms.Label();
            this.c_bolsa1_1 = new System.Windows.Forms.Label();
            this.c_bolsa1_2 = new System.Windows.Forms.Label();
            this.c_bolsa1_5 = new System.Windows.Forms.Label();
            this.c_bolsa1_10 = new System.Windows.Forms.Label();
            this.c_bolsa1_20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_totalMonedas_b2
            // 
            this.c_totalMonedas_b2.BackColor = System.Drawing.Color.Transparent;
            this.c_totalMonedas_b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalMonedas_b2.Location = new System.Drawing.Point(593, 414);
            this.c_totalMonedas_b2.Name = "c_totalMonedas_b2";
            this.c_totalMonedas_b2.Size = new System.Drawing.Size(117, 24);
            this.c_totalMonedas_b2.TabIndex = 233;
            this.c_totalMonedas_b2.Text = "0";
            this.c_totalMonedas_b2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_10c
            // 
            this.c_bolsa2_10c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_10c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_10c.Location = new System.Drawing.Point(573, 353);
            this.c_bolsa2_10c.Name = "c_bolsa2_10c";
            this.c_bolsa2_10c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_10c.TabIndex = 232;
            this.c_bolsa2_10c.Text = "0";
            this.c_bolsa2_10c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_10c.Visible = false;
            // 
            // c_bolsa2_20c
            // 
            this.c_bolsa2_20c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_20c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_20c.Location = new System.Drawing.Point(572, 323);
            this.c_bolsa2_20c.Name = "c_bolsa2_20c";
            this.c_bolsa2_20c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_20c.TabIndex = 231;
            this.c_bolsa2_20c.Text = "0";
            this.c_bolsa2_20c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_20c.Visible = false;
            // 
            // c_bolsa2_50c
            // 
            this.c_bolsa2_50c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_50c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_50c.Location = new System.Drawing.Point(573, 293);
            this.c_bolsa2_50c.Name = "c_bolsa2_50c";
            this.c_bolsa2_50c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_50c.TabIndex = 230;
            this.c_bolsa2_50c.Text = "0";
            this.c_bolsa2_50c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_1
            // 
            this.c_bolsa2_1.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_1.Location = new System.Drawing.Point(573, 263);
            this.c_bolsa2_1.Name = "c_bolsa2_1";
            this.c_bolsa2_1.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_1.TabIndex = 229;
            this.c_bolsa2_1.Text = "0";
            this.c_bolsa2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_2
            // 
            this.c_bolsa2_2.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_2.Location = new System.Drawing.Point(573, 233);
            this.c_bolsa2_2.Name = "c_bolsa2_2";
            this.c_bolsa2_2.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_2.TabIndex = 228;
            this.c_bolsa2_2.Text = "0";
            this.c_bolsa2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_5
            // 
            this.c_bolsa2_5.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_5.Location = new System.Drawing.Point(573, 203);
            this.c_bolsa2_5.Name = "c_bolsa2_5";
            this.c_bolsa2_5.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_5.TabIndex = 227;
            this.c_bolsa2_5.Text = "0";
            this.c_bolsa2_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_10
            // 
            this.c_bolsa2_10.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_10.Location = new System.Drawing.Point(573, 173);
            this.c_bolsa2_10.Name = "c_bolsa2_10";
            this.c_bolsa2_10.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_10.TabIndex = 226;
            this.c_bolsa2_10.Text = "0";
            this.c_bolsa2_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_20
            // 
            this.c_bolsa2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_20.Location = new System.Drawing.Point(573, 143);
            this.c_bolsa2_20.Name = "c_bolsa2_20";
            this.c_bolsa2_20.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_20.TabIndex = 225;
            this.c_bolsa2_20.Text = "0";
            this.c_bolsa2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_20.Visible = false;
            // 
            // c_bolsa1_10c
            // 
            this.c_bolsa1_10c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_10c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_10c.Location = new System.Drawing.Point(329, 353);
            this.c_bolsa1_10c.Name = "c_bolsa1_10c";
            this.c_bolsa1_10c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_10c.TabIndex = 224;
            this.c_bolsa1_10c.Text = "0";
            this.c_bolsa1_10c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_10c.Visible = false;
            // 
            // c_bolsa1_20c
            // 
            this.c_bolsa1_20c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_20c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_20c.Location = new System.Drawing.Point(328, 323);
            this.c_bolsa1_20c.Name = "c_bolsa1_20c";
            this.c_bolsa1_20c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_20c.TabIndex = 223;
            this.c_bolsa1_20c.Text = "0";
            this.c_bolsa1_20c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_20c.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(151, 346);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 24);
            this.label41.TabIndex = 222;
            this.label41.Text = "$0.10";
            this.label41.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(150, 317);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 24);
            this.label40.TabIndex = 221;
            this.label40.Text = "$0.20";
            this.label40.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(536, 116);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(190, 24);
            this.label32.TabIndex = 220;
            this.label32.Text = "Contenido en Bolsa 2";
            // 
            // c_totalbolsa2
            // 
            this.c_totalbolsa2.BackColor = System.Drawing.Color.Transparent;
            this.c_totalbolsa2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbolsa2.Location = new System.Drawing.Point(572, 381);
            this.c_totalbolsa2.Name = "c_totalbolsa2";
            this.c_totalbolsa2.Size = new System.Drawing.Size(117, 24);
            this.c_totalbolsa2.TabIndex = 219;
            this.c_totalbolsa2.Text = "$0.00";
            this.c_totalbolsa2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_totalMonedas_b1
            // 
            this.c_totalMonedas_b1.BackColor = System.Drawing.Color.Transparent;
            this.c_totalMonedas_b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalMonedas_b1.Location = new System.Drawing.Point(367, 414);
            this.c_totalMonedas_b1.Name = "c_totalMonedas_b1";
            this.c_totalMonedas_b1.Size = new System.Drawing.Size(117, 24);
            this.c_totalMonedas_b1.TabIndex = 218;
            this.c_totalMonedas_b1.Text = "0";
            this.c_totalMonedas_b1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(66, 418);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(308, 20);
            this.label14.TabIndex = 217;
            this.label14.Text = "NUMERO DE MONEDAS EN TOTAL: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(163, 381);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 24);
            this.label15.TabIndex = 216;
            this.label15.Text = "Totales:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(274, 116);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(190, 24);
            this.label16.TabIndex = 215;
            this.label16.Text = "Contenido en Bolsa 1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(150, 288);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 24);
            this.label17.TabIndex = 214;
            this.label17.Text = "$0.50";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(150, 259);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 24);
            this.label18.TabIndex = 213;
            this.label18.Text = "$1.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(150, 230);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 24);
            this.label19.TabIndex = 212;
            this.label19.Text = "$2.00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(150, 201);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 24);
            this.label20.TabIndex = 211;
            this.label20.Text = "$5.00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(140, 172);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 24);
            this.label21.TabIndex = 210;
            this.label21.Text = "$10.00";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(140, 143);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 24);
            this.label22.TabIndex = 209;
            this.label22.Text = "$20.00";
            this.label22.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(72, 116);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(133, 24);
            this.label23.TabIndex = 208;
            this.label23.Text = "Denominación";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label24.Location = new System.Drawing.Point(170, 63);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(436, 29);
            this.label24.TabIndex = 207;
            this.label24.Text = "RETIRO DE EFECTIVO EN MONEDAS";
            // 
            // c_imprimirMonedas
            // 
            this.c_imprimirMonedas.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_imprimirMonedas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_imprimirMonedas.Location = new System.Drawing.Point(166, 450);
            this.c_imprimirMonedas.Name = "c_imprimirMonedas";
            this.c_imprimirMonedas.Size = new System.Drawing.Size(143, 47);
            this.c_imprimirMonedas.TabIndex = 206;
            this.c_imprimirMonedas.TabStop = false;
            this.c_imprimirMonedas.Text = "Aceptar";
            this.c_imprimirMonedas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_imprimirMonedas.UseVisualStyleBackColor = true;
            this.c_imprimirMonedas.Click += new System.EventHandler(this.c_imprimirMonedas_Click);
            // 
            // c_cerrarM
            // 
            this.c_cerrarM.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cerrarM.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_cerrarM.Location = new System.Drawing.Point(463, 450);
            this.c_cerrarM.Name = "c_cerrarM";
            this.c_cerrarM.Size = new System.Drawing.Size(148, 47);
            this.c_cerrarM.TabIndex = 205;
            this.c_cerrarM.TabStop = false;
            this.c_cerrarM.Text = "Cerrar";
            this.c_cerrarM.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_cerrarM.UseVisualStyleBackColor = true;
            this.c_cerrarM.Click += new System.EventHandler(this.c_cerrarM_Click_1);
            // 
            // c_totalbolsa1
            // 
            this.c_totalbolsa1.BackColor = System.Drawing.Color.Transparent;
            this.c_totalbolsa1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbolsa1.Location = new System.Drawing.Point(321, 381);
            this.c_totalbolsa1.Name = "c_totalbolsa1";
            this.c_totalbolsa1.Size = new System.Drawing.Size(117, 24);
            this.c_totalbolsa1.TabIndex = 204;
            this.c_totalbolsa1.Text = "$0.00";
            this.c_totalbolsa1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_50c
            // 
            this.c_bolsa1_50c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_50c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_50c.Location = new System.Drawing.Point(329, 293);
            this.c_bolsa1_50c.Name = "c_bolsa1_50c";
            this.c_bolsa1_50c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_50c.TabIndex = 203;
            this.c_bolsa1_50c.Text = "0";
            this.c_bolsa1_50c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_1
            // 
            this.c_bolsa1_1.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_1.Location = new System.Drawing.Point(329, 263);
            this.c_bolsa1_1.Name = "c_bolsa1_1";
            this.c_bolsa1_1.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_1.TabIndex = 202;
            this.c_bolsa1_1.Text = "0";
            this.c_bolsa1_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_2
            // 
            this.c_bolsa1_2.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_2.Location = new System.Drawing.Point(329, 233);
            this.c_bolsa1_2.Name = "c_bolsa1_2";
            this.c_bolsa1_2.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_2.TabIndex = 201;
            this.c_bolsa1_2.Text = "0";
            this.c_bolsa1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_5
            // 
            this.c_bolsa1_5.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_5.Location = new System.Drawing.Point(329, 203);
            this.c_bolsa1_5.Name = "c_bolsa1_5";
            this.c_bolsa1_5.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_5.TabIndex = 200;
            this.c_bolsa1_5.Text = "0";
            this.c_bolsa1_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_10
            // 
            this.c_bolsa1_10.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_10.Location = new System.Drawing.Point(329, 173);
            this.c_bolsa1_10.Name = "c_bolsa1_10";
            this.c_bolsa1_10.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_10.TabIndex = 199;
            this.c_bolsa1_10.Text = "0";
            this.c_bolsa1_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_20
            // 
            this.c_bolsa1_20.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_20.Location = new System.Drawing.Point(329, 143);
            this.c_bolsa1_20.Name = "c_bolsa1_20";
            this.c_bolsa1_20.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_20.TabIndex = 198;
            this.c_bolsa1_20.Text = "0";
            this.c_bolsa1_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_20.Visible = false;
            // 
            // FormaRetiroMonedas
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_totalMonedas_b2);
            this.Controls.Add(this.c_bolsa2_10c);
            this.Controls.Add(this.c_bolsa2_20c);
            this.Controls.Add(this.c_bolsa2_50c);
            this.Controls.Add(this.c_bolsa2_1);
            this.Controls.Add(this.c_bolsa2_2);
            this.Controls.Add(this.c_bolsa2_5);
            this.Controls.Add(this.c_bolsa2_10);
            this.Controls.Add(this.c_bolsa2_20);
            this.Controls.Add(this.c_bolsa1_10c);
            this.Controls.Add(this.c_bolsa1_20c);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.c_totalbolsa2);
            this.Controls.Add(this.c_totalMonedas_b1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.c_imprimirMonedas);
            this.Controls.Add(this.c_cerrarM);
            this.Controls.Add(this.c_totalbolsa1);
            this.Controls.Add(this.c_bolsa1_50c);
            this.Controls.Add(this.c_bolsa1_1);
            this.Controls.Add(this.c_bolsa1_2);
            this.Controls.Add(this.c_bolsa1_5);
            this.Controls.Add(this.c_bolsa1_10);
            this.Controls.Add(this.c_bolsa1_20);
            this.Name = "FormaRetiroMonedas";
            this.Load += new System.EventHandler(this.FormaRetiroMonedas_Load);
            this.Controls.SetChildIndex(this.c_bolsa1_20, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_10, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_5, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_2, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_1, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_50c, 0);
            this.Controls.SetChildIndex(this.c_totalbolsa1, 0);
            this.Controls.SetChildIndex(this.c_cerrarM, 0);
            this.Controls.SetChildIndex(this.c_imprimirMonedas, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.label21, 0);
            this.Controls.SetChildIndex(this.label20, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.c_totalMonedas_b1, 0);
            this.Controls.SetChildIndex(this.c_totalbolsa2, 0);
            this.Controls.SetChildIndex(this.label32, 0);
            this.Controls.SetChildIndex(this.label40, 0);
            this.Controls.SetChildIndex(this.label41, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_20c, 0);
            this.Controls.SetChildIndex(this.c_bolsa1_10c, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_20, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_10, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_5, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_2, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_1, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_50c, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_20c, 0);
            this.Controls.SetChildIndex(this.c_bolsa2_10c, 0);
            this.Controls.SetChildIndex(this.c_totalMonedas_b2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label c_totalMonedas_b2;
        private System.Windows.Forms.Label c_bolsa2_10c;
        private System.Windows.Forms.Label c_bolsa2_20c;
        private System.Windows.Forms.Label c_bolsa2_50c;
        private System.Windows.Forms.Label c_bolsa2_1;
        private System.Windows.Forms.Label c_bolsa2_2;
        private System.Windows.Forms.Label c_bolsa2_5;
        private System.Windows.Forms.Label c_bolsa2_10;
        private System.Windows.Forms.Label c_bolsa2_20;
        private System.Windows.Forms.Label c_bolsa1_10c;
        private System.Windows.Forms.Label c_bolsa1_20c;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label c_totalbolsa2;
        private System.Windows.Forms.Label c_totalMonedas_b1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button c_imprimirMonedas;
        private System.Windows.Forms.Button c_cerrarM;
        private System.Windows.Forms.Label c_totalbolsa1;
        private System.Windows.Forms.Label c_bolsa1_50c;
        private System.Windows.Forms.Label c_bolsa1_1;
        private System.Windows.Forms.Label c_bolsa1_2;
        private System.Windows.Forms.Label c_bolsa1_5;
        private System.Windows.Forms.Label c_bolsa1_10;
        private System.Windows.Forms.Label c_bolsa1_20;
    }
}
