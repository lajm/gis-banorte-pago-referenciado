﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    class BDMantenimiento
    {

        public static bool
            InsertarEvento( DateTime p_FechaHora, String p_Nombre_Tecnico, String p_Vendor, String p_tipo_Mantenimeimnto)
        {
            String l_Query
                = "INSERT INTO              Mantenimiento "
                + "                         (FechaHoraInicio, Tecnico, Multivendor, TipoMantenimiento, Concluido) "
                + "VALUES                   (@FechaHoraInicio, @Tecnico, @Vendor, @TipoMantenimiento,0) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);

                l_Comando.Parameters.AddWithValue("@FechaHoraInicio", p_FechaHora);
                l_Comando.Parameters.AddWithValue("@Tecnico", p_Nombre_Tecnico);
                l_Comando.Parameters.AddWithValue("@Vendor", p_Vendor);
                l_Comando.Parameters.AddWithValue("@TipoMantenimiento",p_tipo_Mantenimeimnto);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Insertar Evento de Mantenimiento", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
        ActualizarMantenimiento(int p_folio_Registro, DateTime p_Fecha , String p_Comentarios)
        {
            String l_Query
                = "UPDATE                   Mantenimiento "
                + "SET                      FechaHoraTermino = @Fecha, "
                + "                         Comentarios = @Comentarios, "
                + "                         Concluido = 1 "
                + "WHERE                    IdRegistro = @IdRegistro";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Fecha", p_Fecha);
                l_Comando.Parameters.AddWithValue("@Comentarios",p_Comentarios);
                l_Comando.Parameters.AddWithValue("@IdRegistro", p_folio_Registro);


                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Actualizar  Mantenimiento", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static DataTable
         ObtenerConsulta_mantenimientos()
        {
            String l_Query
              = "SELECT          * "
              + "FROM            Mantenimiento ";
           

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Mantenimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
        ObtenerConsulta_mantenimientos_abiertos()
        {
            String l_Query
              = "SELECT          * "
              + "FROM            Mantenimiento "
              + "WHERE           Concluido = 0 ";


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Mantenimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }
    }
}
