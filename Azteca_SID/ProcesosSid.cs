﻿using SidApi;
using SymetryDevices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    class ProcesosSid
    {
        private static int m_TimeoutTotalesBolsa = 20;

        public static int TimeoutTotalesBolsa
        {
            get { return m_TimeoutTotalesBolsa; }

            set { m_TimeoutTotalesBolsa = value; }
        }

        public static int
            Estatus()
        {
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_Reply = ErroresSid.Codigos.OKAY;
            String l_Error;

            try
            {
                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                if ( l_Reply < 0 )
                    SidLib.SID_Reset( (char) SidLib.RESET_ERROR );
                l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                SidLib.Error_Sid( l_Reply , out l_Error );
                EnviarErrorSid( l_Reply , "" );
                Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) + " " + l_Error , 1 );
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid.Estatus", "EXCEPTION", E.Message, 1);
                l_Error = "ProcesosSid.Estatus()  EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return l_Reply;
        }

        public static bool
            AlimentadorVacio()
        {
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_Reply = ErroresSid.Codigos.OKAY;

            try
            {
                //l_Reply
                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", l_Reply.ToString() + " - " + sensor[0].ToString("X2"), 1);
                return (sensor[0] & 0x80) != 0 ? false : true;

              
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid.Estatus", "EXCEPTION", E.Message, 1);
                String l_Error = "ProcesosSid.Estatus()  EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return false;
        }



        public static int
            Estatus(out Byte[] p_Key, out Byte[] p_Sensor)
        {
            p_Key = new Byte[1];
            p_Sensor = new Byte[64];
            int l_Reply = ErroresSid.Codigos.OKAY;
            String l_Error;

            try
            {
                l_Reply = SidLib.SID_UnitStatus(p_Key, p_Sensor, null, null, null);
                SidLib.Error_Sid( l_Reply , out l_Error );
                EnviarErrorSid( l_Reply , "" );
                Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) + " " + l_Error , 1 );

            }
            catch (Exception E)
            {
                 l_Error = "ProcesosSid.Estatus()";
                Globales.EscribirBitacora(l_Error, "EXCEPTION", E.Message, 1);
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error + "  EXCEPTION " + E.Message);
            }

            return l_Reply;
        }

        public static int
            ManejoAtoramientoManual(int p_MaxIntentos, string p_Error)
        {
            int l_R = 0;
            int l_Intentos = 0;
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];

            try
            {
                do
                {
                    Globales.EscribirBitacora("EnableDeposit", "Recuperacion intento: " + l_Intentos, p_Error, 1);
                    using (FormaError l_FormaError = new FormaError(false, "atasco"))
                    {
                        l_FormaError.c_MensajeError.Text = p_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                        if (l_Intentos == 2)
                            l_FormaError.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                        l_FormaError.TopMost = true;
                        l_FormaError.ShowDialog();
                    }
                    l_R = SidLib.SID_Reset((char)SidLib.RESET_FREE_PATH);
                    l_R = SidLib.SID_Open(false);
                    //l_R = SidLib.SID_Reset( (char) SidLib.RESET_ERROR );
                    l_R = Estatus();
                    Globales.EscribirBitacora("Deposito de Efectivo", "ManejoAtoramientoManual UnitStatus", "R: + " + l_R.ToString(), 1);
                    if (ErroresSid.EsAtoramiento(l_R) == false )
                        break;
                    l_Intentos++;
                } while (l_Intentos < p_MaxIntentos);
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid.ManejoAtoramientoManual", "EXCEPTION", E.Message, 1);
                String l_Error = "ProcesosSid.ManejoAtoramientoManual() EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return l_R;
        }


        public static int SensadoYManejoAtoramientosManual(int p_MaxIntentos)
        {
            int l_Reply = ErroresSid.Codigos.OKAY;
            String l_Error;

            try
            {
                // -- ESTATUS SID
                l_Reply = Estatus();
                //SidLib.Error_Sid( l_Reply , out l_Error );
                //EnviarErrorSid( l_Reply , "" );
                //Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) + " " + l_Error , 1 );

                if (l_Reply == ErroresSid.Codigos.OKAY)
                    return l_Reply;

                bool? l_Atoramiento = ErroresSid.EsAtoramiento(l_Reply);
                if (l_Atoramiento == true )
                {
                    Globales.CambioEstatusReceptor(Globales.EstatusReceptor.No_Operable);
                    EnviarErrorSid(l_Reply, "ProcesoSid.SensadoYManejoAtoramientosManual()");
                    l_Error = "ProcesosSid.SensadoYManejoAtoramientosManual()";
                    Globales.EscribirBitacora("EnableDeposit", "Atasco de Billetes::", l_Error, 1);
                    SidLib.Error_Sid(l_Reply, out l_Error);
                    l_Reply = ProcesosSid.ManejoAtoramientoManual(p_MaxIntentos, l_Error);

                    if (l_Reply >= ErroresSid.Codigos.OKAY)
                    {
                        Globales.CambioEstatusReceptor(Globales.EstatusReceptor.Operable);
                    }
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid", "SensadoYManejoAtoramientosManual", E.Message, 1);
                l_Error = "ProcesosSid.SensadoYManejoAtoramientosManual() EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return l_Reply;
        }


        public static int SensadoYManejoAtoramientosAutomatizado(int p_MaxReintentos)
        {
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_Reply = ErroresSid.Codigos.OKAY;
            String l_Error;

            Globales.EscribirBitacora("SensadoYManejoAtoramientosAutomatizado << ");

            try
            {
                // -- ESTATUS SID
                l_Reply = Estatus();
                //SidLib.Error_Sid( l_Reply , out l_Error );
                //EnviarErrorSid( l_Reply , "" );
                //Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) + " " + l_Error , 1 );

                bool? l_Atoramiento = ErroresSid.EsAtoramiento(l_Reply);
                if (l_Reply == ErroresSid.Codigos.JAM_ENTER_BAG_OUT)
                {
                    return SensadoYManejoAtoramientosManual(p_MaxReintentos);
                }
                // -- MANEJO DE ATASCAMIENTO EN LA ENTRADA DEL FEEDER
                else if (l_Reply == ErroresSid.Codigos.JAM_IN_FEEDER_IN)
                {
                    using (FormaError f_Error = new FormaError(false, "atasco"))
                    {
                        l_Error = "ProcesosSid.SensadoYManejoAtoramientosAutomatizado()";
                        Globales.EscribirBitacora("Deposito", "Atasco en el Deposito", l_Error, 1);
                        EnviarErrorSid(l_Reply, l_Error);
                        SidLib.Error_Sid(l_Reply, out l_Error);
                        f_Error.c_MensajeError.Text = l_Error + " - Ocurrio un atasco de billetes en la entrada del alimentador por favor acomode y revise la entrada de billetes";
                        f_Error.TopMost = true;
                        f_Error.ShowDialog();
                    }
                    l_Reply = SidLib.ResetError();
                    l_Reply = Estatus();
                    Globales.EscribirBitacora("LEON ESTATUS ATASCA :" + l_Reply);
                    if (!(bool)ErroresSid.EsAtoramiento(l_Reply))
                        l_Atoramiento = false;
                }
                if (l_Atoramiento == true)
                {
                    l_Reply = SidLib.SID_Reset((char)SidLib.RESET_FREE_PATH);
                    l_Reply = SidLib.ResetError();
                    l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);

                    if ((bool)ErroresSid.EsAtoramiento(l_Reply))
                    {
                        Globales.EscribirBitacora("ATORAMIENTO Globales.CambioEstatusReceptor() <<");
                        Globales.CambioEstatusReceptor(Globales.EstatusReceptor.No_Operable);

                        Globales.EscribirBitacora("ATORAMIENTO Globales.CambioEstatusReceptor() >>");
                        int l_Intentos = 0;
                        do
                        {
                            l_Reply = SidLib.SID_Reset((char)SidLib.RESET_FREE_PATH);
                            l_Reply = SidLib.ResetError();
                            l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                            Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus:::", l_Reply.ToString(), 1);
                            l_Intentos++;
                        } while ((l_Reply < 0) &&
                                  (l_Intentos < p_MaxReintentos-1));
                        if (l_Reply < 0)
                        {
                            using (FormaError f_Error = new FormaError(false, "atasco"))
                            {
                                SidLib.Error_Sid(l_Reply, out l_Error);
                                Globales.EscribirBitacora("Deposito", "Atasco en el Deposito", l_Error, 1);
                                EnviarErrorSid(l_Reply, "");
                                f_Error.TopMost = true;
                                f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                               // f_Error.ShowDialog();
                            }
                            l_Reply = SensadoYManejoAtoramientosManual(p_MaxReintentos);
                        }
                    }
                    if (l_Reply >= ErroresSid.Codigos.OKAY)
                    {
                        EnviarErrorSid( l_Reply , "" );
                    }
                }else
                {
                  //  Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid", "SensadoYManejoAtoramientosAutomatizado", E.Message, 1);
                l_Error = "ProcesosSid.SensadoYManejoAtoramientosAutomatizado()  EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }
            finally
            {
                Globales.EscribirBitacora("SensadoYManejoAtoramientosAutomatizado >>  R : " + l_Reply);
            }
            return l_Reply;
        }

        internal static int EnviarTransaccionTruncaRetiro( string l_Folio_Envase_pendiente )
        {
            try
            {
                String l_aux = Globales.NumeroSerieBOLSA;
                if ( Globales.NumeroSerieBOLSA != l_Folio_Envase_pendiente )
                {
                    Globales.NumeroSerieBOLSA = l_Folio_Envase_pendiente;
                }


                using ( FormaRetiroEfectivo f_Retiro = new FormaRetiroEfectivo( true ) )
                {
                    f_Retiro.ShowDialog( );

                }

                Globales.NumeroSerieBOLSA = l_aux;
                return 0;
            }
            catch ( Exception exc )
            {
                Globales.EscribirBitacora( "Proceso_SID" , "EnviarTransaccionTruncaRetiro()" , "Excepcion en Retiro: " + l_Folio_Envase_pendiente
                     + Environment.NewLine + "Retiro con Folio: " + l_Folio_Envase_pendiente , 3 );
                return -1;
            }

        }
        public static int SensadoYManejoManualdePuerta(int p_MaxIntentos)
        {
            int l_Reply = ErroresSid.Codigos.OKAY;
            String l_Error;

            try
            {
                // -- ESTATUS SID
                l_Reply = Estatus();
                SidLib.Error_Sid( l_Reply , out l_Error );
                EnviarErrorSid( l_Reply , "" );
                Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) + " " + l_Error , 1 );
                bool? l_SensoresPuertasOk = ErroresSid.EsSensoresPuertas(l_Reply);

                if (l_SensoresPuertasOk == true)
                {

                    Globales.CambioEstatusReceptor(Globales.EstatusReceptor.No_Operable);
                    l_Error = "ProcesosSid.SensadoYManejoManualdePuerta()";
                    EnviarErrorSid(l_Reply, l_Error);
                    Globales.EscribirBitacora("EnableDeposit", "Problema DIP_SWITCH::", l_Error, 1);
                    l_Reply = ProcesosSid.ManejoSensoresPuertasManual(3, l_Error);
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid", "SensadoYManejoAtoramientosManual", E.Message, 1);
                l_Error = "ProcesosSid.SensadoYManejoManualdePuerta()  EXCEPTION : " + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return l_Reply;
        }


        public static int ManejoSensoresPuertasManual(int p_MaxIntentos, string p_Error)
        {
            int l_R = 0;
            int l_Intentos = 0;
            String l_Error;
            //Byte[] key = new Byte[1];
            //Byte[] sensor = new Byte[64];

            try
            {
                do
                {
                    using (FormaError l_FormaError = new FormaError(false, "switchAbierto"))
                    {
                        Globales.EscribirBitacora("EnableDeposit", "Areas Cerradas completamente: " + l_Intentos, p_Error, 1);
                        l_FormaError.c_MensajeError.Text = p_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                        if (l_Intentos == 2)
                            l_FormaError.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                        l_FormaError.TopMost = true;
                        l_FormaError.ShowDialog();
                        l_R = SidLib.ResetError();
                    }
                    l_R = Estatus();
                    SidLib.Error_Sid( l_R , out l_Error );
                    EnviarErrorSid( l_R , "" );
                    Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_R.ToString( ) + " " + l_Error , 1 );
                    if (ErroresSid.EsSensoresPuertas(l_R) == false)
                        break;
                    l_Intentos++;
                } while (l_Intentos < p_MaxIntentos);
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid.ManejoAtoramientoManual", "EXCEPTION", E.Message, 1);
                l_Error = "ProcesosSid.ManejoSensoresPuertasManual()" + E.Message;
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
            }

            return l_R;
        }


        public static bool? SensadoYProcesamientoHayBilletes()
        {
            bool? l_R = null;
            try
            {
                if (AlimentadorVacio())
                {
                    String l_Error = "ProcesosSid.SensadoYProcesamientoHayBilletes()";
                    EnviarErrorSid(1, l_Error);
                    Globales.EscribirBitacora("SensadoYProcesamientoNoHayBilletes", "Feeder vacio", l_Error, 1);
                    using (FormaError f_Error = new FormaError(true, "alimentarBilletes"))
                    {
                        f_Error.c_MensajeError.Text = l_Error + "Coloque los billetes en el validador y presione Depositar";
                        f_Error.ShowDialog();
                    }
                    l_R = true;
                }
                else
                    l_R = true;
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid.SensadoYProcesamientoHayBilletes", "EXCEPTION", E.Message, 1);
                String l_Error = "ProcesosSidSensadoYProcesamientoHayBilletes()";
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
                l_R = null;
            }

            return l_R;
        }


        public static bool? EquipoListoDeposito()
        {
            bool? l_R = null;
            Byte[] l_Key = new Byte[1];
            Byte[] l_Sensor = new Byte[64];

            try
            {
                int l_Reply = Estatus(out l_Key, out l_Sensor);
                if (l_Sensor[7] == 148)
                    l_R = true;
                else
                    l_R = false;

                // TODO: Validar otras variables
            }
            catch (Exception E)
            {
                String l_Error = "ProcesosSid.EquipoListoDeposito()";
                Globales.EscribirBitacora(l_Error, "EXCEPTION", E.Message, 1);
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
                l_R = null;
            }
            return l_R;
        }


        public static bool? SensadoBolsaLista()
        {
            bool? l_R = true;
            int l_Reply;
            String l_Error;

            try
            {
                SensarSIDcs.UnitStatus l_Estatus = SensarSIDcs.GetStatusOpened();
                if (!l_Estatus.m_Sensor_bag_present == true)
                {
                    l_Reply = ErroresSid.Codigos.BAG_NOT_PRESENT;
                    l_Error = "Bolsa no presente";
                    EnviarErrorSid(l_Reply, l_Error);
                    Globales.EscribirBitacora("ProcesosSid", "SensadoBolsaLista()", l_Error, 1);
                    return false;
                }

                //if (!l_Estatus.m_Dip_Switch_elevator_position_DOWN == true)
                //{
                //    l_Reply = ErroresSid.Codigos.BAG_NOT_PRESENT;
                //    l_Error = "Dip_Switch_elevator_position_DOWN";
                //    EnviarErrorSid(l_Reply, l_Error);
                //    Globales.EscribirBitacora("ProcesosSid", "SensadoBolsaLista()", l_Error, 1);
                //    return false;
                //}

                if (!l_Estatus.m_Sensor_bag_ready_for_deposit == true)
                {
                    l_Reply = ErroresSid.Codigos.BAG_NOT_PRESENT;
                    l_Error = "Bolsa no lista para depositar";
                    EnviarErrorSid(l_Reply, "Sensor_bag_ready_for_deposit");
                    Globales.EscribirBitacora("ProcesosSid", "SensadoBolsaLista()", l_Error, 1);
                    return false;
                }
            }
            catch (Exception E)
            {
                l_Error = "ProcesosSid.SensadoBolsaLista() EXCEPTION : " + E.Message;
                Globales.EscribirBitacora("ProcesosSid", "SensadoBolsaLista()", E.Message, 1);
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
                l_R = null;
            }

            return l_R;
        }


        public static bool? SensadoEquipoListoDeposito()
        {
            bool? l_R = null;
            Byte[] l_Key = new Byte[1];
            Byte[] l_Sensor = new Byte[64];
            String l_Error = "";


            try
            {
               // SidLib.ResetError();

                SensarSIDcs.UnitStatus l_Status = SensarSIDcs.GetStatus();

                String l_Errores = "";

                bool? l_ErrorBolsa = SensadoBolsaLista( );
                bool l_dialError = false;
                bool l_tapaError = false;
                bool ? l_ErrorPuerta =false ;
                l_R = true;
                if (l_ErrorBolsa != true)
                {
                    l_ErrorBolsa = true;
                    l_Errores += l_Error + "Bolsa no lista, ";
                    l_R = false;
                }

                if (l_Status.m_Code_lock_correct)
                {
                    l_Error = "Code lock";
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                        UtilsComunicacion.AlertaGSI(ErroresSid.Codigos.LOCK_ERROR, l_Error);
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                    l_dialError = true;
                }

                if (l_Status.m_Interlock_cover_head_open)
                {
                    l_Error = "Puerta Interlock Abierta";
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                        UtilsComunicacion.AlertaGSI(ErroresSid.Codigos.INTERLOCK_OPEN, l_Error);
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                    l_tapaError  = true;
                }

                if (!l_Status.m_Interlock_door_strongbox_closed)
                {
                    l_Error = "Puerta de boveda abierta";
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                        UtilsComunicacion.AlertaGSI(ErroresSid.Codigos.INTERLOCK_DOOR_OPEN, l_Error);
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                    l_ErrorPuerta = true;
                }

                if (l_Status.m_Sensor_door_open)
                {
                    l_Error = "Puerta de Boveda Abierta";
                   if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable)
                    UtilsComunicacion.AlertaGSI(ErroresSid.Codigos.SENSOR_DOOR_OPEN, l_Error);
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                    l_ErrorPuerta = true;
                }

                if ( l_R == false )
                {
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    Globales.EscribirBitacora( "Deposito" , "Equipo no listo para depositar" , l_Error , 1 );

                    String l_aux = Globales.IdUsuario;
                    String l_ref_aux = Globales.ReferenciaGSI;
                    Double l_monto_aux = Globales.Limite_MontoGSI;
                    String l_nombre_aux = Globales.NombreUsuario;
                    using ( FormaFueraServicio l_Forma = new FormaFueraServicio( l_dialError , (bool) l_ErrorPuerta , (bool) l_ErrorBolsa , l_tapaError , false , true , false , false ) )
                    {
                        l_Forma.ShowDialog( );

                    }
                    //22-Junio Correcciion IDCuenta Not Suplied
                    if ( l_aux != Globales.IdUsuario || Globales.CuentaBanco == null )
                    {
                       

                        if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        {
                            Globales.AutenticarUsuario( l_aux );
                        }
                        else
                            Globales.AutenticarUsuarioGsi( l_aux, l_nombre_aux , l_monto_aux );

                     
                            Globales.ReferenciaGSI = l_ref_aux;


                        }
                    //using (FormaError f_Error = new FormaError(false, "malFucionamiento"))
                    //{
                       
                    //    //EnviarErrorSid(ErroresSid.Codigos.OPERATION_ERROR, out l_Error);
                        
                    //    //if (l_Sensor[7] == 150 && Properties.Settings.Default.Enviar_AlertasGSI)
                    //    //    UtilsComunicacion.AlertaGSI(-226, "Bolsa No Presente ");
                    //    f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde. Causa : " + l_Errores;
                    //    f_Error.ShowDialog();
                    //}
                }
             

            }
            catch (Exception E)
            {
                l_Error = "ProcesoSid.SensadoEquipoListoDeposito() EXCEPTION : " + E.Message;
                Globales.EscribirBitacora("ProcesosSid.ManejoAtoramientoManual", "EXCEPTION", E.Message, 1);
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
                l_R = null;
            }

            return l_R;
        }


        public static ContadorEfectivo TotalBolsaFW(out int p_Reply, out string p_Error)
        {
            ushort[] l_totalNotes = new ushort[64];
            ContadorEfectivo l_Contador = null;
            int l_Reply = 0;

            try
            {
                // -- ESPERAR A QUE SE LIBERE EL EQUIPO
                for (int i = 0; i < m_TimeoutTotalesBolsa *2; i++)
                {
                    if (i==0)
                    System.Threading.Thread.Sleep(250);

                    SidLib.SID_Open( false );

                    l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);

                    if (l_Reply == ErroresSid.Codigos.OKAY)
                    {
                        Globales.EscribirBitacora("SID_GetCashTotalBag == OKAY  ");
                        break;
                    }
                    else if (l_Reply == ErroresSid.Codigos.COMMAND_IN_EXECUTION_YET)
                    {
                        SidLib.Error_Sid(l_Reply, out p_Error);
                        Globales.EscribirBitacora("ProcesosSid", "TotalBolsaFW - Equipo trabajando todavia", l_Reply.ToString(), 1);
                        System.Threading.Thread.Sleep( 500 );
                    }
                    Globales.EscribirBitacora("ProcesosSid", "TotalBolsaFW - Error : ", l_Reply.ToString(), 1);


                    
                    //else
                    //{
                    //    Globales.EscribirBitacora("Contenido bolsa", "SID_GetCashTotalBag", l_Reply.ToString(), 1);
                    //    // TODO: Validar si hay que hacer break
                    //    break;
                    //}
                }

                if (l_Reply != ErroresSid.Codigos.OKAY)
                {
                    SidLib.Error_Sid(l_Reply, out p_Error);
                    Globales.EscribirBitacora("ProcesosSid", "TotalBolsa", " No se pudo obtener el total en Bolsa: " + l_Reply, 1);
                    p_Reply = l_Reply;
                    return null;
                }

                l_Contador = ContadorEfectivo.DeTotalesCtsMxn(l_totalNotes, out p_Error);

                if (l_Contador == null)
                {
                    Globales.EscribirBitacora("ProcesosSid", "ProcesosSid.TotalBolsa.DeTotalesCts", p_Error, 1);
                    p_Reply = ErroresSid.Codigos.SOFTWARE;
                    return null;
                }
            }
            catch (Exception E)
            {
                p_Reply = ErroresSid.Codigos.SOFTWARE;
                p_Error = " Exception : " + E.Message;
                Globales.EscribirBitacora("ProcesosSid", "TotalBolsaFW()", p_Error, 1);
            }

            p_Reply = l_Reply;
            return l_Contador;
        }


        public static void EnviarErrorSid(int l_Reply, String p_ErrorInfo)
        {
            String l_Error = "";
            try
            {
                SidLib.Error_Sid(l_Reply, out l_Error);

                if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
                {
                    if ( l_Reply != SidLib.SID_JAM_IN_FEEDER_IN )
                        if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply < 0 && ( Globales.Estatus != Globales.EstatusReceptor.No_Operable ) )
                        {
                            UtilsComunicacion.AlertaGSI( l_Reply , l_Error + p_ErrorInfo );
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                        }


                    if ( ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 )
                        && ( Globales.Estatus == Globales.EstatusReceptor.No_Operable ) )
                    {
                        UtilsComunicacion.AlertaGSI( l_Reply , l_Error + p_ErrorInfo );
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                    }
                }

            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid", "EnviarErrorSid", E.Message, 1);
            }
        }


        public static ContadorEfectivo ObtenerTotalBilletesFW()
        {
            //Globales.EscribirBitacora( "ObtenerTotalBolsa() << " );
            ContadorEfectivo l_Contador = null;
            String l_Error;
            try
            {
                int l_Reply;
                l_Contador = ProcesosSid.TotalBolsaFW(out l_Reply, out l_Error);
                if (l_Reply != ErroresSid.Codigos.OKAY)
                {
                    Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "ObtenerTotalBolsa()", 1);
                }
                else
                    Globales.EscribirBitacora("FormaDeposito", "ObtenerTotalBilletesFW", "ObtenerTotalBolsa()", 1);
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("Deposito", "ObtenerTotalBolsa() Exception ", E.Message, 1);
            }
            //Globales.EscribirBitacora( "ObtenerTotalBolsa() >> " );
            return l_Contador;
        }


        public static int EnviarTransaccionTrunca(DepositoEnCurso p_Deposito)
        {
            String l_Error = "";
            ContadorEfectivo l_BilletesFinalFW = null;
            ContadorEfectivo l_MonedasFinalFW = null;
            ContadorEfectivo l_Deposito = null;
            int l_Reply;

            try
            {
                //if (!Properties.Settings.Default.No_Debug_Pc)
                //    return ErroresSid.Codigos.NOT_AVAILABLE_IN_DEBUG_MODE;

                l_Reply = SidLib.SID_Open(false);
                Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca", "Open R:" + l_Reply, 3);

                l_BilletesFinalFW = ObtenerTotalBilletesFW();

                l_Reply = SidLib.SID_Close();

                if (l_BilletesFinalFW == null)
                {
                    l_Error = "Mandando transaccion recuperada  Sesion: " + p_Deposito.m_IdSesion + " no se puede obtener los totales del SID Transaccion no recuperada" + l_Error;
                    EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, l_Error);
                    return ErroresSid.Codigos.SOFTWARE;
                }

                // - CALCULAR DEPOSITO REAL
                l_Deposito = l_BilletesFinalFW.DiferenciaCts(p_Deposito.m_ConteoInicialBilletes);
                Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca", "DiferenciaCts" + l_Deposito.ToString(), 3);

                if (Properties.Settings.Default.Port_YUGO != 0)
                {
                    decimal l_MontoMonedas;
                    Dictionary<decimal, int> l_TotalesMonedas;
                    UCoin.ObtenerTotales((byte)Properties.Settings.Default.Port_YUGO, "", out l_MontoMonedas, out l_TotalesMonedas);
                    l_MonedasFinalFW = ContadorEfectivo.DeTotalesUCoin(l_TotalesMonedas, out l_Error);

                    Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca - Inventario Anterior Monedas >>>>> ", l_MonedasFinalFW.ToString(),3);
                    if ( l_MonedasFinalFW == null )
                    {
                        Globales.EscribirBitacora( "ProcesosSid" , "EnviarTransaccionTrunca()" , l_Error , 3 );
                        l_Error = "Mandando transaccion recuperada  Sesion: " + p_Deposito.m_IdSesion + " no se puede obtener los totales del UCoin enviando sin monedas" + l_Error;
                        EnviarErrorSid( ErroresSid.Codigos.SOFTWARE , l_Error );
                    }
                    else
                    {
                        ContadorEfectivo l_DepositoMonedas = l_MonedasFinalFW.DiferenciaUCoin( p_Deposito.m_ConteoInicialMonedas );

                        l_Deposito.Suma( l_DepositoMonedas );
                    }
                }

                Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca", "Totales Piezas: " + l_Deposito.TotalPiezas().ToString()
                                + " , TotalMonto: " + l_Deposito.TotalMonto(),  3);

                if (l_Deposito.TotalPiezas() > 0)
                {
                    Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca() - Detalle Deposito " + Environment.NewLine
                                    , l_Deposito.ToString(), 3);
                    if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                        Globales.AutenticarUsuario( p_Deposito.m_IdUsuario );
                    else
                        Globales.AutenticarUsuarioGsi(p_Deposito.m_Cuenta, p_Deposito.m_Nombre , 0 );

                    // -- GENERAR TABLA DETALLE DEPOSITOGlobales.NombreCliente
                    DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
                    for (int i = 0; i < l_Deposito.Desglose.Length; i++)
                    {
                        if (l_Deposito.Desglose[i].m_Cantidad > 0)
                        {
                            DataRow l_Nueva = l_Detalle.NewRow();
                            l_Nueva[0] = l_Deposito.Desglose[i].m_IdDenominacion;
                            l_Nueva[1] = l_Deposito.Desglose[i].m_Cantidad;
                            l_Nueva[2] = l_Deposito.Desglose[i].m_Valor;
                            l_Detalle.Rows.Add(l_Nueva);
                        }
                    }
                    Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca() "
                                    , "DataTable Creada", 3);

                    Globales.ReferenciaGSI = p_Deposito.m_Referencia;

                    Globales.EscribirBitacora( "ProcesosSid" , "EnviarTransaccionTrunca() "
                                   , " "+ Globales.IdUsuario+ " " + Globales.NombreCliente + " " + p_Deposito.m_Cuenta , 3 );
                    Globales.FechaHoraSesion = p_Deposito.m_FechaInicio;
                    if (!UtilsComunicacion.Enviar_Transaccion(Globales.NombreCliente, 1, 1, 1, l_Deposito.TotalMonto(),
                        l_Detalle, 0, true, Globales.IdUsuario))
                    {
                        Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca() - Detalle Deposito " + Environment.NewLine
                                        , l_Deposito.ToString(), 3);
                        return -1;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ProcesosSid", "EnviarTransaccionTrunca()", E.Message, 3);
                EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, "ProcesosSid.EnviarTransaccionTrunca() - " + E.Message);
                return ErroresSid.Codigos.SOFTWARE;
            }
            return 0;
        }
    }
}
