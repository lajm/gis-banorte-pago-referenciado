﻿namespace Azteca_SID
{
    partial class FormaError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaError));
            this.c_emblema = new System.Windows.Forms.PictureBox();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_Imagen = new System.Windows.Forms.PictureBox();
            this.c_MensajeError = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.c_cierreAutomatico = new System.Windows.Forms.Timer(this.components);
            this.c_activarVentana = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.c_emblema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Imagen)).BeginInit();
            this.SuspendLayout();
            // 
            // c_emblema
            // 
            this.c_emblema.BackColor = System.Drawing.Color.Transparent;
            this.c_emblema.Location = new System.Drawing.Point(12, 60);
            this.c_emblema.Name = "c_emblema";
            this.c_emblema.Size = new System.Drawing.Size(55, 50);
            this.c_emblema.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_emblema.TabIndex = 32;
            this.c_emblema.TabStop = false;
            this.c_emblema.Visible = false;
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonCancelar.BackColor = System.Drawing.Color.Transparent;
            this.c_BotonCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_BotonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_BotonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ForeColor = System.Drawing.Color.DimGray;
            this.c_BotonCancelar.Location = new System.Drawing.Point(229, 254);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(122, 39);
            this.c_BotonCancelar.TabIndex = 1;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Cancelar";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonCancelar.UseVisualStyleBackColor = false;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonAceptar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.c_BotonAceptar.BackColor = System.Drawing.Color.Transparent;
            this.c_BotonAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_BotonAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.c_BotonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ForeColor = System.Drawing.Color.Gray;
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.c_BotonAceptar.ImageKey = "paloma";
            this.c_BotonAceptar.Location = new System.Drawing.Point(50, 254);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(122, 39);
            this.c_BotonAceptar.TabIndex = 0;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonAceptar.UseVisualStyleBackColor = false;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_Imagen
            // 
            this.c_Imagen.BackColor = System.Drawing.Color.Transparent;
            this.c_Imagen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Imagen.Location = new System.Drawing.Point(340, 62);
            this.c_Imagen.Name = "c_Imagen";
            this.c_Imagen.Size = new System.Drawing.Size(48, 48);
            this.c_Imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_Imagen.TabIndex = 31;
            this.c_Imagen.TabStop = false;
            // 
            // c_MensajeError
            // 
            this.c_MensajeError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.c_MensajeError.BackColor = System.Drawing.Color.Transparent;
            this.c_MensajeError.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_MensajeError.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_MensajeError.Location = new System.Drawing.Point(14, 62);
            this.c_MensajeError.Name = "c_MensajeError";
            this.c_MensajeError.Size = new System.Drawing.Size(374, 173);
            this.c_MensajeError.TabIndex = 2;
            this.c_MensajeError.Text = "label1";
            this.c_MensajeError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "correcto");
            this.imageList1.Images.SetKeyName(1, "importante");
            this.imageList1.Images.SetKeyName(2, "precaucion");
            this.imageList1.Images.SetKeyName(3, "informacion");
            this.imageList1.Images.SetKeyName(4, "preguntaroja");
            this.imageList1.Images.SetKeyName(5, "preguntaAmarilla");
            this.imageList1.Images.SetKeyName(6, "incorrecto");
            this.imageList1.Images.SetKeyName(7, "reloj");
            this.imageList1.Images.SetKeyName(8, "tiempo");
            this.imageList1.Images.SetKeyName(9, "paloma");
            this.imageList1.Images.SetKeyName(10, "error");
            this.imageList1.Images.SetKeyName(11, "informacion2");
            this.imageList1.Images.SetKeyName(12, "pregunta");
            this.imageList1.Images.SetKeyName(13, "warning");
            this.imageList1.Images.SetKeyName(14, "precaucion2");
            this.imageList1.Images.SetKeyName(15, "esperar");
            this.imageList1.Images.SetKeyName(16, "mensaje");
            this.imageList1.Images.SetKeyName(17, "iniciando");
            this.imageList1.Images.SetKeyName(18, "password");
            this.imageList1.Images.SetKeyName(19, "texto");
            this.imageList1.Images.SetKeyName(20, "noUsuario");
            // 
            // c_cierreAutomatico
            // 
            this.c_cierreAutomatico.Interval = 10000;
            this.c_cierreAutomatico.Tick += new System.EventHandler(this.c_cierreAutomatico_Tick);
            // 
            // c_activarVentana
            // 
            this.c_activarVentana.Interval = 3000;
            this.c_activarVentana.Tick += new System.EventHandler(this.c_activarVentana_Tick);
            // 
            // FormaError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(400, 300);
            this.Controls.Add(this.c_emblema);
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.Controls.Add(this.c_Imagen);
            this.Controls.Add(this.c_MensajeError);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormaError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaError";
            this.Load += new System.EventHandler(this.FormaError_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.c_emblema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Imagen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox c_emblema;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        public System.Windows.Forms.PictureBox c_Imagen;
        public System.Windows.Forms.Label c_MensajeError;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Timer c_cierreAutomatico;
        private System.Windows.Forms.Timer c_activarVentana;
    }
}