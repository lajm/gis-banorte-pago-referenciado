﻿namespace Azteca_SID
{
    partial class FormaConsultaOperaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.c_numoperaciones = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_fechabox = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_ConsultaOperaciones = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.c_Enviar_Retiro = new System.Windows.Forms.Button();
            this.c_reenviar_Deposito = new System.Windows.Forms.Button();
            this.dg_retiros_pendientes = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_num_retiros_pendientes = new System.Windows.Forms.TextBox();
            this.dg_depositos_pendientes = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.c_num_depositos_pendientes = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_retiros_pendientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_depositos_pendientes)).BeginInit();
            this.SuspendLayout();
            // 
            // c_numoperaciones
            // 
            this.c_numoperaciones.BackColor = System.Drawing.Color.WhiteSmoke;
            this.c_numoperaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_numoperaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numoperaciones.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_numoperaciones.Location = new System.Drawing.Point(239, 383);
            this.c_numoperaciones.Name = "c_numoperaciones";
            this.c_numoperaciones.Size = new System.Drawing.Size(83, 19);
            this.c_numoperaciones.TabIndex = 48;
            this.c_numoperaciones.TextChanged += new System.EventHandler(this.c_numoperaciones_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(17, 381);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 20);
            this.label2.TabIndex = 47;
            this.label2.Text = "Numero de Operaciones : ";
            // 
            // c_fechabox
            // 
            this.c_fechabox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_fechabox.Location = new System.Drawing.Point(6, 17);
            this.c_fechabox.MinDate = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            this.c_fechabox.Name = "c_fechabox";
            this.c_fechabox.Size = new System.Drawing.Size(353, 26);
            this.c_fechabox.TabIndex = 46;
            this.c_fechabox.ValueChanged += new System.EventHandler(this.c_fechabox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(363, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(387, 29);
            this.label1.TabIndex = 45;
            this.label1.Text = "CONSULTA DE OPERACIONES";
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(559, 373);
            this.c_Cerrar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(103, 40);
            this.c_Cerrar.TabIndex = 44;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = true;
            this.c_Cerrar.Visible = false;
            this.c_Cerrar.Click += new System.EventHandler(this.c_Cerrar_Click);
            // 
            // c_ConsultaOperaciones
            // 
            this.c_ConsultaOperaciones.AllowUserToAddRows = false;
            this.c_ConsultaOperaciones.AllowUserToDeleteRows = false;
            this.c_ConsultaOperaciones.AllowUserToOrderColumns = true;
            this.c_ConsultaOperaciones.AllowUserToResizeColumns = false;
            this.c_ConsultaOperaciones.AllowUserToResizeRows = false;
            this.c_ConsultaOperaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.c_ConsultaOperaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.c_ConsultaOperaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.c_ConsultaOperaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_ConsultaOperaciones.Location = new System.Drawing.Point(0, 46);
            this.c_ConsultaOperaciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_ConsultaOperaciones.MultiSelect = false;
            this.c_ConsultaOperaciones.Name = "c_ConsultaOperaciones";
            this.c_ConsultaOperaciones.ReadOnly = true;
            this.c_ConsultaOperaciones.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.c_ConsultaOperaciones.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.c_ConsultaOperaciones.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ConsultaOperaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.c_ConsultaOperaciones.Size = new System.Drawing.Size(773, 325);
            this.c_ConsultaOperaciones.TabIndex = 43;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(31, 414);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 36);
            this.button1.TabIndex = 49;
            this.button1.Text = "Reimpresion Ticket";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 60);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(788, 485);
            this.tabControl1.TabIndex = 50;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.c_fechabox);
            this.tabPage1.Controls.Add(this.c_ConsultaOperaciones);
            this.tabPage1.Controls.Add(this.c_Cerrar);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.c_numoperaciones);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(780, 452);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Consulta de Operaciones";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.c_Enviar_Retiro);
            this.tabPage2.Controls.Add(this.c_reenviar_Deposito);
            this.tabPage2.Controls.Add(this.dg_retiros_pendientes);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.c_num_retiros_pendientes);
            this.tabPage2.Controls.Add(this.dg_depositos_pendientes);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.c_num_depositos_pendientes);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(780, 452);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consulta de Operaciones Pendientes";
            // 
            // c_Enviar_Retiro
            // 
            this.c_Enviar_Retiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Enviar_Retiro.Location = new System.Drawing.Point(334, 366);
            this.c_Enviar_Retiro.Name = "c_Enviar_Retiro";
            this.c_Enviar_Retiro.Size = new System.Drawing.Size(179, 36);
            this.c_Enviar_Retiro.TabIndex = 59;
            this.c_Enviar_Retiro.Text = "RE Enviar Retiro";
            this.c_Enviar_Retiro.UseVisualStyleBackColor = true;
            this.c_Enviar_Retiro.Click += new System.EventHandler(this.c_Enviar_Retiro_Click);
            // 
            // c_reenviar_Deposito
            // 
            this.c_reenviar_Deposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_reenviar_Deposito.Location = new System.Drawing.Point(325, 196);
            this.c_reenviar_Deposito.Name = "c_reenviar_Deposito";
            this.c_reenviar_Deposito.Size = new System.Drawing.Size(179, 36);
            this.c_reenviar_Deposito.TabIndex = 58;
            this.c_reenviar_Deposito.Text = "RE Enviar Depósito";
            this.c_reenviar_Deposito.UseVisualStyleBackColor = true;
            this.c_reenviar_Deposito.Click += new System.EventHandler(this.c_reenviar_Deposito_Click);
            // 
            // dg_retiros_pendientes
            // 
            this.dg_retiros_pendientes.AllowUserToAddRows = false;
            this.dg_retiros_pendientes.AllowUserToDeleteRows = false;
            this.dg_retiros_pendientes.AllowUserToOrderColumns = true;
            this.dg_retiros_pendientes.AllowUserToResizeColumns = false;
            this.dg_retiros_pendientes.AllowUserToResizeRows = false;
            this.dg_retiros_pendientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_retiros_pendientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_retiros_pendientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_retiros_pendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_retiros_pendientes.Location = new System.Drawing.Point(9, 254);
            this.dg_retiros_pendientes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dg_retiros_pendientes.MultiSelect = false;
            this.dg_retiros_pendientes.Name = "dg_retiros_pendientes";
            this.dg_retiros_pendientes.ReadOnly = true;
            this.dg_retiros_pendientes.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dg_retiros_pendientes.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dg_retiros_pendientes.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dg_retiros_pendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_retiros_pendientes.Size = new System.Drawing.Size(757, 104);
            this.dg_retiros_pendientes.TabIndex = 54;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label5.Location = new System.Drawing.Point(530, 220);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(236, 29);
            this.label5.TabIndex = 55;
            this.label5.Text = "Retiros Pendientes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label6.Location = new System.Drawing.Point(11, 373);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(217, 20);
            this.label6.TabIndex = 56;
            this.label6.Text = "Numero de Operaciones : ";
            // 
            // c_num_retiros_pendientes
            // 
            this.c_num_retiros_pendientes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.c_num_retiros_pendientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_num_retiros_pendientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_num_retiros_pendientes.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_num_retiros_pendientes.Location = new System.Drawing.Point(233, 375);
            this.c_num_retiros_pendientes.Name = "c_num_retiros_pendientes";
            this.c_num_retiros_pendientes.Size = new System.Drawing.Size(83, 19);
            this.c_num_retiros_pendientes.TabIndex = 57;
            // 
            // dg_depositos_pendientes
            // 
            this.dg_depositos_pendientes.AllowUserToAddRows = false;
            this.dg_depositos_pendientes.AllowUserToDeleteRows = false;
            this.dg_depositos_pendientes.AllowUserToOrderColumns = true;
            this.dg_depositos_pendientes.AllowUserToResizeColumns = false;
            this.dg_depositos_pendientes.AllowUserToResizeRows = false;
            this.dg_depositos_pendientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_depositos_pendientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_depositos_pendientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dg_depositos_pendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_depositos_pendientes.Location = new System.Drawing.Point(12, 37);
            this.dg_depositos_pendientes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dg_depositos_pendientes.MultiSelect = false;
            this.dg_depositos_pendientes.Name = "dg_depositos_pendientes";
            this.dg_depositos_pendientes.ReadOnly = true;
            this.dg_depositos_pendientes.RowHeadersVisible = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dg_depositos_pendientes.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dg_depositos_pendientes.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dg_depositos_pendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_depositos_pendientes.Size = new System.Drawing.Size(757, 154);
            this.dg_depositos_pendientes.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label3.Location = new System.Drawing.Point(499, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(270, 29);
            this.label3.TabIndex = 50;
            this.label3.Text = "Depósitos Pendientes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(14, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(217, 20);
            this.label4.TabIndex = 52;
            this.label4.Text = "Numero de Operaciones : ";
            // 
            // c_num_depositos_pendientes
            // 
            this.c_num_depositos_pendientes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.c_num_depositos_pendientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_num_depositos_pendientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_num_depositos_pendientes.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_num_depositos_pendientes.Location = new System.Drawing.Point(236, 205);
            this.c_num_depositos_pendientes.Name = "c_num_depositos_pendientes";
            this.c_num_depositos_pendientes.Size = new System.Drawing.Size(83, 19);
            this.c_num_depositos_pendientes.TabIndex = 53;
            // 
            // FormaConsultaOperaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormaConsultaOperaciones";
            this.Text = "FormaConsultaOperaciones";
            this.Load += new System.EventHandler(this.FormaConsultaOperaciones_Load);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_retiros_pendientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_depositos_pendientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox c_numoperaciones;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker c_fechabox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.DataGridView c_ConsultaOperaciones;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dg_retiros_pendientes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_num_retiros_pendientes;
        private System.Windows.Forms.DataGridView dg_depositos_pendientes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_num_depositos_pendientes;
        private System.Windows.Forms.Button c_Enviar_Retiro;
        private System.Windows.Forms.Button c_reenviar_Deposito;
    }
}