﻿using Irds;
using SymetryDevices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsSymSid;

namespace Azteca_SID
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        
        static void Main()
        {

            
            if (FirstInstance() && checkVigencia())
            {
                Conexion.PonerCadenaConexion(Properties.Settings.Default.CadenaConexion);
               
                


                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormaPrincipal());

                

                //Globales.UsuarioGSI= "hflores";
                //Globales.ContraseñaGsi= "!CE#_[]*!";
                //Globales.LlaveGsi= "HG58YZ3CR9";


                //String Aux = UtilsComunicacion.Desencriptar( l_config.mensaje );



            }
            else
            {

                if (!checkVigencia( ) )
                {
                 
                        using ( FormaError f_Error = new FormaError( true , "warning" ) )
                    {
                        f_Error.c_MensajeError.Text = "Por favor comuniquese a Symetry S.A de C.V  Codigo: S1967";
                        f_Error.ShowDialog( );

                    }
                }
                    else
                {
                    using ( FormaError f_Error = new FormaError( true , "warning" ) )
                    {
                        f_Error.c_MensajeError.Text = "YA HAY OTRA INSTANCIA CORRIENDO";
                        f_Error.ShowDialog( );
                       
                    }
                                   }


            }
        }

    
        private static bool FirstInstance()
        {
           
                bool created;
      
                string name = Assembly.GetEntryAssembly().FullName;

            Process currentProcess = Process.GetCurrentProcess();

        //    Process[] localAll = Process.GetProcesses();
            // Get all instances of Notepad running on the local computer.
            // This will return an empty array if notepad isn't running.
            Process[] localByName = Process.GetProcessesByName("GSI_SID");
            if (localByName.Length <= 1)
                created = true;
            else
                created = false;

            return created;
           
        }

         private static bool  checkVigencia()
        {
            DateTime l_hoy = DateTime.Now;
            DateTime l_vigencia = new DateTime(2019,9, 30);




            return (l_hoy < l_vigencia);
        }

      

    }
}
