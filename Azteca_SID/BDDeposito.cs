﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;

namespace Azteca_SID
{
    public class BDDeposito
    {
        public static DataTable
            ObtenerDeposito( int p_IdDeposito )
        {
            String l_Query
                = "SELECT               IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado "
                + "FROM                 Deposito "
                + "WHERE                IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                DataTable l_Datos = new DataTable( "Deposito" );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }

        public static DataTable
            ObtenerDetalleDeposito( int p_IdDeposito )
        {
            String l_Query
                = "SELECT           a.IdDenominacion, a.Cantidad, b.Codigo "
                + "FROM             Denominacion b, DetalleDeposito a "
                + "WHERE            b.IdDenominacion = a.IdDenominacion "
                + "AND              IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                DataTable l_Datos = new DataTable( "DetalleDeposito" );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }

        public static DataTable
            BuscarDepositosNoEnviados( )
        {
            String l_Query
                = "SELECT               ProcessId, IdDeposito, IdCuenta, IdMoneda, TotalDeposito, IdUsuario, Equipo "
                + "FROM                 Deposito "
                + "WHERE                Enviado = 0 "
                + "ORDER BY             IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                DataTable l_Datos = new DataTable( "Depositos" );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }

        public static bool
            ConfirmarDeposito( int p_IdDeposito )
        {
            String l_Query
                = "UPDATE               Deposito "
                + "SET                  Enviado = 1 "
                + "WHERE                IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                    return false;
                else
                    return true;
            }
        }

        public static bool
            ActualizarProcessId( int p_IdDeposito , Int64 p_IdProceso )
        {
            String l_Query
                = "UPDATE           Deposito "
                + "SET              ProcessId = @ProcessId "
                + "WHERE            IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@ProcessId" , p_IdProceso );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                    return false;
                else
                    return true;
            }
        }

        public static bool
            InsertarDepositoManual( String p_IdCuenta , int p_IdCajon , int p_IdMoneda , Double p_TotalDeposito
           , DataTable p_DatosDetalle ,DateTime fecha , String p_IdUsuario , out int p_IdDeposito )
        {
            p_IdDeposito = FolioDeposito( );
            string l_Equipo = "Equipo1";
            String l_Query
                = "INSERT INTO          Deposito "
                + "                     (IdDeposito, IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado, Retirado,Equipo,Manual) "
                + "VALUES               (@IdDeposito, @IdUsuario, @IdCuenta, @IdCajon, @IdMoneda, @FechaHora, @TotalDeposito, 0, 0,@Equipo,1) ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction( );
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion , l_Transaccion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                l_Comando.Parameters.AddWithValue( "@IdUsuario" , p_IdUsuario );
                l_Comando.Parameters.AddWithValue( "@IdCuenta" , p_IdCuenta );
                l_Comando.Parameters.AddWithValue( "@IdCajon" , p_IdCajon );
                l_Comando.Parameters.AddWithValue( "@IdMoneda" , p_IdMoneda );
                l_Comando.Parameters.AddWithValue( "@FechaHora" , fecha );
                l_Comando.Parameters.AddWithValue( "@TotalDeposito" , p_TotalDeposito );
                l_Comando.Parameters.AddWithValue( "@Equipo" , l_Equipo );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                if ( !InsertarDetalleDeposito( p_DatosDetalle , p_IdDeposito , l_Conexion , l_Transaccion ) )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                else
                {
                    l_Transaccion.Commit( );
                    InsertarCiclo( p_IdDeposito );

                    //BDIRDS.InsertarTransaccion(Globales.Num_Estacion, UtilsIRDS.TipoTransaccion.Deposito, p_IdDeposito, FolioCiclo(), DateTime.Now.AddMinutes(-5), DateTime.Now, 0
                    //           , p_TotalDeposito, p_IdDeposito, 0, "Transaccion exitosa", p_DatosDetalle, Globales.Num_ContenedorManual);

                    //BDIRDS.ActualizarCicloIrdsManual(FolioCiclo(), BDConsulta.TotalDepositadoCicloManual(FolioCiclo()));

                    return true;
                }
            }
        }

        internal static bool InsetarDepositoGsi( string p_CuentaBanco , string p_GsiCajero , string p_Divisa , string p_Fecha , string p_Hora
                                              , string p_Folio , Double p_Montototal , string p_ReferenciaGSI , string p_NumeroSerieBOLSA , bool p_Exitoso )
        {

            String l_Query
                = "INSERT INTO          DepositoGSI "
                + "                     (IdDeposito, CuentaBanco, GsiCajero, Monto, Divisa, Fecha, Hora, Referencia, Envase, Pendiente) "
                + "VALUES             (@Folio, @IdCuenta, @GsiCajero, @Monto,@Divisa, @Fecha, @Hora, @Referencia, @Envase, @Pendiente) ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {

                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@IdCuenta" , p_CuentaBanco );
                l_Comando.Parameters.AddWithValue( "@GsiCajero" , p_GsiCajero );
                l_Comando.Parameters.AddWithValue( "@Monto" , p_Montototal );
                l_Comando.Parameters.AddWithValue( "@Divisa" , p_Divisa );
                l_Comando.Parameters.AddWithValue( "@Fecha" , p_Fecha );
                l_Comando.Parameters.AddWithValue( "@Hora" , p_Hora );
                l_Comando.Parameters.AddWithValue( "@Folio" , int.Parse( p_Folio ) );
                l_Comando.Parameters.AddWithValue( "@Referencia" , p_ReferenciaGSI );
                l_Comando.Parameters.AddWithValue( "@Envase" , Globales.NumeroSerieBOLSA );
                l_Comando.Parameters.AddWithValue( "@Pendiente" , p_Exitoso );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                {
                    return false;
                }
                else
                {
                    return true;

                }
            }
        }

        public static bool
            InsertarDeposito( String p_IdCuenta , int p_IdCajon , int p_IdMoneda , Double p_TotalDeposito
                , DataTable p_DatosDetalle , DateTime fecha, String p_IdUsuario , out int p_IdDeposito )
        {
            string l_Equipo = "Equipo1";
            p_IdDeposito = FolioDeposito( );

            String l_Query
                = "INSERT INTO          Deposito "
                + "                     (IdDeposito, IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado, Retirado,Equipo,Bolsa) "
                + "VALUES               (@IdDeposito, @IdUsuario, @IdCuenta, @IdCajon, @IdMoneda, @FechaHora, @TotalDeposito, 0, 0,@Equipo,@Bolsa) ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction( );
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion , l_Transaccion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                l_Comando.Parameters.AddWithValue( "@IdUsuario" , p_IdUsuario );
                l_Comando.Parameters.AddWithValue( "@IdCuenta" , p_IdCuenta );
                l_Comando.Parameters.AddWithValue( "@IdCajon" , p_IdCajon );
                l_Comando.Parameters.AddWithValue( "@IdMoneda" , p_IdMoneda );
                l_Comando.Parameters.AddWithValue( "@FechaHora" , fecha );
                l_Comando.Parameters.AddWithValue( "@TotalDeposito" , p_TotalDeposito );
                l_Comando.Parameters.AddWithValue( "@Equipo" , l_Equipo );
                l_Comando.Parameters.AddWithValue( "@Bolsa" , Globales.NumeroSerieBOLSA );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                if ( !InsertarDetalleDeposito( p_DatosDetalle , p_IdDeposito , l_Conexion , l_Transaccion ) )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                else
                {
                    l_Transaccion.Commit( );

                    //Globales.EscribirBitacoraAzteca("Deposito","Escribir Deposito Azteca","Empieza Registro de Transacción",3);

                    //RegistrarMovAzteca(p_IdDeposito, DateTime.Now, (decimal)p_TotalDeposito, p_IdMoneda, Globales.IdUsuario, Globales.WorkStation);
                    //Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Termino el Registro de Transacción", 3);

                    InsertarCiclo( p_IdDeposito );



                    //MensajeDeposito1 l_Irds = new MensajeDeposito1(
                    //Properties.Settings.Default.Estacion     // id estacion
                    //, p_IdDeposito   // folio transacción
                    //, FolioCiclo()    // ciclo operacional
                    //, DateTime.Now.AddMinutes(-3) // fecha hora inicio
                    //, DateTime.Now // fecha hora fin
                    //, Properties.Settings.Default.IdUsuario // string p_IdOperadorLocal
                    //, Properties.Settings.Default.NombreUsuario   // String nombre completo del operador local
                    //, "SuperUser"               // Usuario global del sistema que realiza la operación o que creo el operador local
                    //, Moneda.MonedaPesos // Moneda p_IdMoneda
                    //, 1 // IdCuenta  ID Global cuenta del sistema
                    //, Properties.Settings.Default.ReferenciaUsuario //string p_Referencia
                    //, l_saldoActual // decimal p_SaldoAnterior
                    //, (decimal)p_TotalDeposito // decimal p_MontoTransaccion
                    //, 0
                    //, 0         // int m_TotalIncidentes
                    //);

                    //foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
                    //{

                    //    l_Irds.AgregarDenominacion(1, Int32.Parse (l_Detalle[2].ToString()), (Int32)l_Detalle[1]); // 10 de 100$ en contenedor 1
                    //}

                    //ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(1);
                    //l_Cola.AgregarMensaje(l_Irds);


                    //try
                    //{
                    //    ColaMensajesIrdsHttp l_Cola_envios = new ColaMensajesIrdsHttp(2);

                    //    l_Cola_envios.MandarMensajesPendientes(Properties.Settings.Default.Estacion);
                    //   Globales.EscribirBitacora("Mensajes Pendientes ", "Lanzados ", "Sin Problemas", 1);

                    //}
                    //catch (Exception exxx)
                    //{
                    //   Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", exxx.Message, 1);
                    //}

                    return true;


                }
            }
        }

        internal static DataTable ChekBolsa( )
        {
            String l_Query = " SELECT distinct bolsa "
            + "FROM                Deposito "
            + "WHERE             Retirado = 0 ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                DataTable l_Datos = new DataTable( "BolsasPendientes" );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }


        }

        internal static void UpdateDepositoGSI( int p_folio )
        {
            String l_Query
               = "UPDATE               DepositoGSI "
               + "SET                  Pendiente = 0 "
                + "WHERE               IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_folio );
                l_Comando.ExecuteNonQuery( );
            }
        }

        public static bool
          Insertar_Deposito( String p_IdCuenta , int p_IdCajon , int p_IdMoneda , Double p_TotalDeposito
              , DataTable p_DatosDetalle , String p_IdUsuario, DateTime fecha , String p_Equipo , out int p_IdDeposito )
        {
            p_IdDeposito = FolioDeposito( );
            String l_Query
                = "INSERT INTO          Deposito "
                + "                     (IdDeposito, IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado, Retirado, Equipo) "
                + "VALUES               (@IdDeposito, @IdUsuario, @IdCuenta, @IdCajon, @IdMoneda, @FechaHora, @TotalDeposito, 0, 0,@Equipo) ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction( );
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion , l_Transaccion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                l_Comando.Parameters.AddWithValue( "@IdUsuario" , p_IdUsuario );
                l_Comando.Parameters.AddWithValue( "@IdCuenta" , p_IdCuenta );
                l_Comando.Parameters.AddWithValue( "@IdCajon" , p_IdCajon );
                l_Comando.Parameters.AddWithValue( "@IdMoneda" , p_IdMoneda );
                l_Comando.Parameters.AddWithValue( "@FechaHora" , fecha);
                l_Comando.Parameters.AddWithValue( "@TotalDeposito" , p_TotalDeposito );
                l_Comando.Parameters.AddWithValue( "@Equipo" , p_Equipo );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                if ( !InsertarDetalleDeposito( p_DatosDetalle , p_IdDeposito , l_Conexion , l_Transaccion ) )
                {
                    l_Transaccion.Rollback( );
                    return false;
                }
                else
                {
                    l_Transaccion.Commit( );


                    return true;
                }
            }
        }

        internal static bool UpdateDeposito( int p_IdDeposito , string p_detalle_deposito )
        {
            String l_Query
               = "UPDATE           Deposito "
               + "SET              Detalle_Deposito = @Detalle_Deposito "
               + "WHERE            IdDeposito = @IdDeposito ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@Detalle_Deposito" , p_detalle_deposito );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                    return false;
                else
                    return true;
            }
        }

        private static bool
            InsertarDetalleDeposito( DataTable p_DatosDetalle , int p_IdDeposito , SqlConnection p_Conexion
                , SqlTransaction p_Transaccion )
        {
            String l_Query
                = "INSERT INTO              DetalleDeposito "
                + "                         (IdDeposito, IdDenominacion, Cantidad) "
                + "VALUES                   (@IdDeposito, @IdDenominacion, @Cantidad) ";

            foreach ( DataRow l_Detalle in p_DatosDetalle.Rows )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , p_Conexion , p_Transaccion );
                l_Comando.Parameters.AddWithValue( "@IdDeposito" , p_IdDeposito );
                l_Comando.Parameters.AddWithValue( "@IdDenominacion" , (int) l_Detalle[0] );
                l_Comando.Parameters.AddWithValue( "@Cantidad" , (Int32) l_Detalle[1] );
                try
                {
                    if ( l_Comando.ExecuteNonQuery( ) != 1 )
                        return false;
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Insertar Detalle Deposito" , "Error Consulta : " , ex.Message , 1 );
                    return false;
                }
            }

            return true;
        }

        private static int
            FolioDeposito( )
        {
            String l_Query
                = "SELECT MAX           (IdDeposito) "
                + "FROM                 Deposito ";
            int l_Folio = 0;

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Folio = (int) l_Lector[0];
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Folio Deposito" , "Error Consulta : " , ex.Message , 1 );
                }
            }
            l_Folio++;
            return l_Folio;
        }

        public static int ObtenerTotalBilletes( )
        {
            int l_Folio = 0;
            String l_Query
              = "SELECT           SUM(a.Cantidad) as Totalbilletes "
              + "FROM             Deposito b "
              + "INNER JOIN       DetalleDeposito a "
              + "ON               b.IdDeposito = a.IdDeposito "
              + "WHERE            b.Retirado = 0 "
              + "AND              a.IdDenominacion < 20";





            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {


                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );

                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Folio = (int) l_Lector[0];
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Obtener Total Billetes" , "Error Consulta : " , "No hay Existencias" , 1 );
                    l_Folio = 0;
                }

                return l_Folio;
            }
        }

        public static int ObtenerTotalMonedas( )
        {
            int l_Folio = 0;
            String l_Query
              = "SELECT           SUM(a.Cantidad) as TotalMonedas "
              + "FROM             Deposito b "
              + "INNER JOIN       DetalleDeposito a "
              + "ON               b.IdDeposito = a.IdDeposito "
              + "WHERE            b.Retirado = 0 "
              + "AND              a.IdDenominacion > 29";





            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {


                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );

                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Folio = (int) l_Lector[0];
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Obtener Total Monedas" , "Error Consulta : " , "No hay Existencias" , 1 );
                    l_Folio = 0;
                }

                return l_Folio;
            }
        }

        public static Double ObtenerMontoActual( )
        {
            Double l_Monto = 0;
            String l_Query
              = "SELECT            SUM(TotalDeposito) AS MontoActual "
              + "FROM             Deposito "
              + "WHERE            Retirado = 0 ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {


                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );

                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Monto = Double.Parse( l_Lector[0].ToString( ) );
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Obtener Monto Actual" , "Error Consulta : " , ex.Message , 1 );
                    l_Monto = 0;
                }

                return l_Monto;
            }
        }

        public static int
         FolioCiclo( )
        {
            String l_Query
                = "SELECT MAX           (IdCicloOperacion) "
                + "FROM                 CicloOperacion ";
            int l_Folio = 0;

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Folio = (int) l_Lector[0];
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Folo CicloOperacion" , "Error Consulta : " , ex.Message , 1 );
                }
            }

            return l_Folio;
        }




        public static bool
      VerificarUnicidad( DateTime p_fecha_hora )
        {
            String l_Query
                = "  SELECT FechaHora "
                  + " FROM Deposito "
                  + " WHERE DATEPART( yy , @fecha ) = DATEPART( yy , FechaHora ) "
                  + " AND DATEPART( MM , @fecha ) = DATEPART( MM , FechaHora ) "
                  + " AND DATEPART( DD , @fecha ) = DATEPART( DD , FechaHora ) "
                  + " AND DATEPART( HH , @fecha ) = DATEPART( HH , FechaHora ) "
                  + " AND DATEPART( mi , @fecha ) = DATEPART( mi , FechaHora ) "
                  + " AND DATEPART( ss , @fecha ) = DATEPART( ss , FechaHora ) ";
          

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                    l_Comando.Parameters.AddWithValue( "@fecha" , p_fecha_hora );
                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        return false;
                    else
                        return true;
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Verificar Unicidad Deposito" , "Error Consulta : " , ex.Message , 1 );
                    return true;
                }
            }
        }

           

        public static void
          InsertarCiclo(int p_folioDeposito)
        {
            int p_IdCiclo = FolioCiclo();
            String l_Query
                = "Insert INTO   CicloOperacion "
                + " (IdCicloOperacion, IdDeposito, IdRetiro) "
                + "VALUES (@IdCicloOperacion,@IdDeposito,0)";
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    l_Comando.Parameters.AddWithValue("@IdCicloOperacion", p_IdCiclo);
                    l_Comando.Parameters.AddWithValue("@IdDeposito", p_folioDeposito);
                    l_Comando.ExecuteNonQuery();
                }catch(Exception ex)
                {
                   Globales.EscribirBitacora("Folo CicloOperacion", "Error al Escribir el Ciclo de Operación : ", ex.Message, 1);
                }

                }

        }

        //public static void RegistrarMovAzteca(int p_IdDeposito, DateTime p_fecha, decimal p_Monto, int p_divisa, string p_usuario, string p_workstation)
        //{
        //    string l_respuesta = "";
           

        //    try {

        //        Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Insertando Registro en Tabla", 3);

        //        Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Datos Registro: " + p_IdDeposito + ";" +  p_fecha.ToString() + ";" + p_Monto + ";" + p_divisa + ";" + p_usuario + ";" + p_workstation , 3);

        //        if (BDRegistroAzteca.InsertarRegistro(p_IdDeposito, p_fecha, p_Monto, p_divisa, p_usuario, p_workstation))
        //        {
        //            RespuestaAzteca[] l_resp = new RespuestaAzteca[1];

        //            try
        //            {
        //                String l_Uri = String.Format("http://{0}/BazCajaFront/Servicios/Aceptador/RegistraDeposito.svc", Properties.Settings.Default.ServidorSucursalAzteca);

        //                Globales.EscribirBitacoraAzteca("Deposito", "ServicioWSdl informador", "Creando cliente: " + l_Uri, 3);
        //                redAzteca.IService1 l_informador = new redAzteca.Service1Client("BasicHttpBinding_IService1",l_Uri);
                        

        //                Globales.EscribirBitacoraAzteca("Deposito", "ServicioWSdl informador", "Consumiendo servicio", 3);
        //                l_respuesta = l_informador.RegistraDepositoAceptador(p_IdDeposito, p_fecha.ToString("dd/MM/yy"), p_Monto, 1, p_usuario, p_workstation);
                       

        //                l_resp = (RespuestaAzteca []) JsonConvert.DeserializeObject<RespuestaAzteca []>(l_respuesta);
        //                Globales.EscribirBitacoraAzteca("Deposito", "ServicioWSdl informador", "Respuesta: " + l_respuesta, 3);
        //                //.Replace ("[","").Replace("]","")
                        
                        
        //            }
        //            catch (Exception exrz)
        //            {
        //                Globales.EscribirBitacora("Deposito", "RegistroAzteca", exrz.Message, 1);
        //                Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Error en el consumo  wsdl de Azteca: "+ exrz.Message, 3);
        //            }
        //            finally
        //            {
        //                Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Actualizando  Registro en Tabla", 3);
        //                if (l_resp[0].Cod == 0 && l_resp[0].DesCod != null)
        //                    BDRegistroAzteca.ActualizarRegistro(p_IdDeposito, int.Parse(l_resp[0].DesCod));

        //                else if ( l_resp[0].DesCod != null)
        //                    BDRegistroAzteca.ActualizarRegistro(p_IdDeposito, l_resp[0].DesCod);

        //            }
        //        }
        //        else
        //        {
        //            Globales.EscribirBitacora("Deposito", "RegistroAzteca", "Error al Escribir en la Tabla RegistroAzteca", 1);
        //            Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Error al Escribir en la Tabla RegistroAzteca", 3);
        //        }
        //    }catch (Exception excp)
        //    {
        //        Globales.EscribirBitacora("Deposito", "RegistroAzteca", "Exepcion al Escribir en la Tabla RegistroAzteca: " + excp.Message, 1);
        //        Globales.EscribirBitacoraAzteca("Deposito", "Escribir Deposito Azteca", "Exepcion al Escribir en la Tabla RegistroAzteca: " + excp.Message, 3);
        //    }

        //}

    }
}
