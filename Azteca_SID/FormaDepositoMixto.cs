﻿using SidApi;
using SymetryDevices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaDepositoMixto:  Form
    {

        bool l_Noscanimages = false;
        String l_directorioimages = "C:\\DetalleDepositos";
        String l_Path = "";
        int l_numerobilleteF = 1;
        int l_numerobilleteR = 1;

        int l_Reply = -1;
        String l_Error;
        int Intentos_Globales = 0;
        DateTime l_FechaHoraInicio = DateTime.Now;
      


        ushort[] l_TotalBag = new ushort[10];

        ushort[] l_bag_inicio;
        ushort[] l_bag_final;
        ushort[] l_bag_deposito;

        ushort Procesar_NUM_Documentos = 0;

        int l_topeDeposito = 0;
        int l_topefijo = Properties.Settings.Default.CapacidadBolsa;

        uint l_TotalAceptados = 0;
        uint l_TotalRechazados = 0;
        int l_Total20 = 0;
        int l_Total50 = 0;
        int l_Total100 = 0;
        int l_Total200 = 0;
        int l_Total500 = 0;
        int l_Total1000 = 0;

        int l_Total10 = 0;
        int l_Total5 = 0;
        int l_Total2 = 0;
        int l_Total1 = 0;
        int l_Total50c = 0;

        byte drawer_id;
        bool Ramdom = false;
        Double l_TotalEfectivo=0;
        Double l_GranTotal=0;
        DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
        DataTable l_DetalleM = BDDeposito.ObtenerDetalleDeposito(0);

        public FormaDepositoMixto()
        {
            InitializeComponent();
        }

        private void EscribirResultados()
        {
            //Cursor.Show();
            //Cursor.Current = Cursors.WaitCursor;
            Double l_Efectivo20;
            Double l_Efectivo50;
            Double l_Efectivo100;
            Double l_Efectivo200;
            Double l_Efectivo500;
            Double l_Efectivo1000;

            c_BilletesAceptados.Text = l_TotalAceptados.ToString();
            if (l_TotalRechazados > 1)
                c_BilletesRechazados.Text = "Si";
            c_Cantidad100.Text = l_Total100.ToString();
            c_Cantidad1000.Text = l_Total1000.ToString();
            c_Cantidad20.Text = l_Total20.ToString();
            c_Cantidad200.Text = l_Total200.ToString();
            c_Cantidad50.Text = l_Total50.ToString();
            c_Cantidad500.Text = l_Total500.ToString();
            l_Efectivo100 = l_Total100 * 100;
            l_Efectivo1000 = l_Total1000 * 1000;
            l_Efectivo20 = l_Total20 * 20;
            l_Efectivo200 = l_Total200 * 200;
            l_Efectivo50 = l_Total50 * 50;
            l_Efectivo500 = l_Total500 * 500;
            c_Suma100.Text = l_Efectivo100.ToString("$#,###,##0.00");
            c_Suma1000.Text = l_Efectivo1000.ToString("$#,###,##0.00");
            c_Suma20.Text = l_Efectivo20.ToString("$#,###,##0.00");
            c_Suma200.Text = l_Efectivo200.ToString("$#,###,##0.00");
            c_Suma50.Text = l_Efectivo50.ToString("$#,###,##0.00");
            c_Suma500.Text = l_Efectivo500.ToString("$#,###,##0.00");
            l_TotalEfectivo = l_Efectivo500 + l_Efectivo50 + l_Efectivo200
                + l_Efectivo20 + l_Efectivo1000 + l_Efectivo100;
            c_TotalBilletes.Text = l_TotalEfectivo.ToString("$#,###,##0.00");

            c_TotalDeposito.Text = (l_TotalEfectivo + (Double)l_Total_Monedas).ToString("$#,###,##0.00");


            l_topeDeposito = l_topefijo - (int)l_TotalAceptados;

            if (l_topeDeposito < 1)
            {
                c_tope.Text = "DEJE DE ALIMENTAR EL EQUIPO POR FAVOR";
                c_tope.BackColor = Color.Yellow;


            }
            else
            {
                if (l_topeDeposito < 300)
                {
                    Procesar_NUM_Documentos = (ushort)l_topeDeposito;
                    c_tope.Visible = true;
                }

                c_tope.Text = "Maximo Permitido:  " + l_topeDeposito + " Billetes";
            }


            // Ligereza
            c_TotalDeposito.Update();
            c_BilletesAceptados.Update();
            c_BilletesRechazados.Update();
            c_Cantidad100.Update();
            c_Cantidad1000.Update();
            c_Cantidad20.Update();
            c_Cantidad200.Update();
            c_Cantidad50.Update();
            c_Cantidad500.Update();
            c_Suma100.Update();
            c_Suma1000.Update();
            c_Suma20.Update();
            c_Suma200.Update();
            c_Suma50.Update();
            c_Suma500.Update();
            c_TotalDeposito.Update();
            c_tope.Update();
            c_TotalBilletes.Invalidate();
            c_TotalBilletes.Update();
            c_TotalDeposito.Invalidate();
            c_TotalDeposito.Update();
            //Ligereza
          
            //  Cursor.Hide();
        }
        private void c_Depositar_Click(object sender, EventArgs e)
        {
            c_BilletesRechazados.Text = "No";
            l_TotalRechazados = 0;

            Int32 Frente, Back, IRFrente, IRBack, MgFrente, Mgback;
            int Reserved = 0;
            int intentos = 0;




            Cursor.Current = Cursors.WaitCursor;

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            ushort[] l_totalNotes = new ushort[10];

           
            c_Depositar.Enabled = false;
           
            //pictureBox3.Enabled = false;
            c_BotonAceptar.Enabled = false;
            //c_Monedas.Enabled = false;
            c_integrado.Enabled = false;
            Forzardibujado();

            if (l_topeDeposito < 50)
            {
                c_BotonAceptar.Enabled = true;
                Refresh();
                return;
            }

            if (Properties.Settings.Default.No_Debug_Pc)
            {
                // char[] l_username = { '1', '0', '0', '2', '9', '1' };
                char[] l_username = Globales.IdUsuario.ToCharArray();

                try
                {
                    try
                    {


                        try
                        {

                            l_Reply = SidLib.SID_Open(l_Noscanimages);
                            Globales.EscribirBitacora("Deposito de Efectivo", "OPEN", l_Reply.ToString(), 1);
                            if (l_Reply != 0)
                                l_Reply = SidLib.ResetError();

                            l_Reply = SidApi.SidLib.SID_Login(l_username);

                            Globales.EscribirBitacora("SELECT DEPOSITO", "SID_LOGIN", "login SIDserial: " + Properties.Settings.Default.NumSerialEquipo, 3);

                            // l_Reply = SidLib.SID_ConfigDoubleLeafingAndDocLength(SidLib.SID_DOUBLE_LEAFING_WARNING, 50,0, 0);

                            l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                            Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", l_Reply.ToString(), 1);


                            if ( -215 <= l_Reply && l_Reply <= -199 )
                            {
                                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                                Error_Sid( l_Reply , out l_Error );
                                Globales.EscribirBitacora( "EnableDeposit" , "Atasco de Billetes::" , l_Error , 1 );

                                do
                                {
                                    Globales.EscribirBitacora( "EnableDeposit" , "Recuperacion intento: " + intentos , l_Error , 1 );
                                    using ( FormaError f_Error = new FormaError( false , "atasco" ) )
                                    {
                                        f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                                        if ( intentos == 2 )
                                            f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                        f_Error.ShowDialog( );




                                        // c_BotonCancelar_Click(this, null);
                                    }
                                    try
                                    {
                                        l_Reply = SidLib.ResetPath( );

                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        l_Reply = SidLib.SID_Open( l_Noscanimages );
                                    }
                                    l_Reply = SidLib.ResetError( );
                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                    Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) , 1 );
                                    intentos++;
                                    Intentos_Globales++;
                                } while ( l_Reply != 0 && intentos < 3 );
                                SidLib.SID_Close( );
                                c_BotonAceptar.Enabled = true;
                                if ( Intentos_Globales >= 3 )
                                    return;

                            }

                            else if ( -246 <= l_Reply && l_Reply <= -239 )
                            {
                                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                                Error_Sid( l_Reply , out l_Error );
                                Globales.EscribirBitacora( "EnableDeposit" , "Problema DIP_SWITCH::" , l_Error , 1 );
                                intentos = 0;
                                do
                                {
                                    Globales.EscribirBitacora( "EnableDeposit" , "Areas Cerradas completamente: " + intentos , l_Error , 1 );
                                    using ( FormaError f_Error = new FormaError( false , "switchAbierto" ) )
                                    {
                                        f_Error.c_MensajeError.Text = l_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                                        if ( intentos == 2 )
                                            f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                        f_Error.ShowDialog( );
                                        l_Reply = SidLib.ResetError( );


                                        // c_BotonCancelar_Click(this, null);
                                    }
                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                    Globales.EscribirBitacora( "Deposito de Efectivo" , "UnitStatus" , l_Reply.ToString( ) , 1 );
                                    intentos++;
                                    Intentos_Globales++;
                                } while ( l_Reply != 0 && intentos < 3 );

                                SidLib.SID_Close( );
                                c_BotonAceptar.Enabled = true;
                                if ( Intentos_Globales >= 3 )
                                    return;

                            }

                            else if ( l_Reply == 1 )
                            {
                                Error_Sid( l_Reply , out l_Error );
                                Globales.EscribirBitacora( "EnableDepositWhileFeeding" , "Feeder vacio" , l_Error , 1 );

                                using ( FormaError f_Error = new FormaError( true , "alimentarBilletes" ) )
                                {
                                    f_Error.c_MensajeError.Text = "Coloque los billetes en el validador y presione Depositar";
                                    f_Error.ShowDialog( );
                                    SidLib.ResetError( );
                                    SidLib.SID_Close( );
                                    c_BotonAceptar.Enabled = true;

                                  //  pictureBox3.Enabled = true;
                                    c_Depositar.Enabled = true;
                                    return;
                                    // c_BotonCancelar_Click(this, null);
                                }
                            }
                            else
                            {

                                if (sensor[7] == 148)
                                {
                                    Globales.EscribirBitacora("CashIn", "sensor[7]", sensor[7].ToString(), 1);
                                    //  l_Reply = SidLib.SID_GetCashTotalBag(l_TotalBag);
                                    // foreach (ushort billetes in l_totalNotes)
                                    // c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;

                                    //l_Reply = SidLib.SID_ConfigDoubleLeafingAndDocLength(SidLib.DOUBLE_LEAFING_WARNING, (short)Globales.Doubleleafing, 0, 0);
                                    l_Reply = SidLib.SID_ConfigDoubleLeafing(SidLib.DOUBLE_LEAFING_WARNING
                                        , (short)Properties.Settings.Default.ValorDoubleLeafing, (short)Properties.Settings.Default.ValorPolimero, 0);
                                    Error_Sid(l_Reply, out l_Error);
                                    Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: ALGODON " + Properties.Settings.Default.ValorDoubleLeafing, l_Error, 1);
                                    Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: POLYMERO " + Properties.Settings.Default.ValorPolimero, l_Error, 1);

                                    l_Reply = SidLib.SID_CashIn(Procesar_NUM_Documentos, 1, 1, l_Noscanimages, 0, 0);/*Capture the notes */

                                    if (l_Reply == 1)
                                    {

                                        Error_Sid(l_Reply, out l_Error);
                                        Globales.EscribirBitacora("EnableDeposit", "Feeder vacio", l_Error, 1);

                                        using (FormaError f_Error = new FormaError(true, "alimentarBilletes"))
                                        {
                                            f_Error.c_MensajeError.Text = "Ponga billetes en el alimentador y presione Depositar";
                                            f_Error.ShowDialog();
                                            Refresh();
                                            SidLib.ResetError();
                                            SidLib.SID_Close();
                                            c_Depositar.Enabled = true;
                                            c_BotonAceptar.Enabled = true;
                                        
                                            //pictureBox3.Enabled = true;

                                            c_Depositar.Text = "ALGO MAS";
                                            return;
                                            // c_BotonCancelar_Click(this, null);
                                        }
                                    }
                                    else if (l_Reply == SidApi.SidLib.SID_OKAY)
                                    {
                                        short[] result = new short[1];
                                        l_Reply = SidLib.SID_WaitValidationResult(result);
                                        System.Threading.Thread.Sleep(60);

                                        while (l_Reply == SidLib.SID_PERIF_BUSY || l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING || l_Reply == SidLib.SID_UNIT_DOC_TOO_LONG)
                                        {

                                            if (l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING || l_Reply == SidLib.SID_UNIT_DOC_TOO_LONG)
                                            {
                                                l_TotalRechazados++;
                                                SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                                                if (l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING)
                                                {
                                                    l_TotalRechazados++;
                                                    //  l_detalle_reconocimiento = SidApi.SidLib.SID_RecognizeErrorDetail();

                                                    //  SidApi.Error_Sid(l_detalle_reconocimiento, out l_Error);
                                                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "Billete alimentado DOBLE", 2);
                                                }
                                                else
                                                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "Billete muy largo o Doble Desfazado", 2);
                                            }
                                            else if (result[0] == 0xe0 || result[0] == 0xe2)
                                            {
                                                l_TotalRechazados++;
                                              int   l_detalle_reconocimiento = SidApi.SidLib.SID_RecognizeErrorDetail();
                                                SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                                                SidLib.Error_Sid(l_detalle_reconocimiento, out l_Error);
                                                Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", l_Error, 2);
                                            }
                                            else
                                                // c_Respuesta.Text += "\r\n" + result[0].ToString("X2");
                                                // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");
                                                if (result[0] != 0 && l_Reply != SidLib.SID_DOUBLE_LEAFING_WARNING)
                                            {

                                                switch (result[0].ToString())
                                                {
                                                    case "16":
                                                        l_TotalAceptados++;
                                                        l_Total20++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "20 Pesos", 3);
                                                        break;
                                                    case "17":
                                                        l_TotalAceptados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "50 Pesos", 3);
                                                        l_Total50++;
                                                        break;
                                                    case "18":
                                                        l_TotalAceptados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "100 Pesos", 3);
                                                        l_Total100++;
                                                        break;
                                                    case "19":
                                                        l_TotalAceptados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "200 Pesos", 3);
                                                        l_Total200++;
                                                        break;
                                                    case "20":
                                                        l_TotalAceptados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "500 Pesos", 3);
                                                        l_Total500++;
                                                        break;
                                                    case "21":
                                                        l_TotalAceptados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "1000 Pesos", 3);
                                                        l_Total1000++;
                                                        break;
                                                    case "224":
                                                        l_TotalRechazados++;
                                                        break;
                                                    case "226":
                                                        l_TotalRechazados++;
                                                        break;
                                                    case "227":
                                                        l_TotalRechazados++;
                                                        break;
                                                    case "228":
                                                        l_TotalRechazados++;
                                                        break;
                                                    case "255":
                                                        l_TotalRechazados++;
                                                        break;
                                                    default:
                                                        l_TotalRechazados++;
                                                        Globales.EscribirBitacora("Deposito", "Billete NO RECONOCIDO: ", "Denominacion desconocida", 3);
                                                        break;

                                                }
                                                /* Wait the end of the previous command*/
                                                System.Threading.Thread.Sleep(30);


                                                EscribirResultados();
                                               // this.Refresh();

                                                if (l_Noscanimages)
                                                {
                                                    Frente = Back = IRFrente = IRBack = Mgback = MgFrente = 0;

                                                    Reserved = SidLib.SID_ReadImage((int)SidLib.NO_CLEAR_BLACK, ref Frente, ref Back, ref IRFrente, ref IRBack, ref MgFrente, ref Mgback, ref Reserved);
                                                    Globales.EscribirBitacora("Deposito", "SID_READIMAGE: ", Reserved.ToString(), 3);

                                                    Reserved = SidLib.SID_SaveDIB(Frente, CrearPathDestinoFrente("Frente.bmp"));
                                                    Globales.EscribirBitacora("Deposito", "SID_DIB FRENTE: ", Reserved.ToString(), 3);
                                                    Reserved = SidLib.SID_SaveDIB(Back, CrearPathDestinoReverso("Reverso.bmp"));
                                                    Globales.EscribirBitacora("Deposito", "SID_DIB BACK: ", Reserved.ToString(), 3);

                                                    Reserved = SidLib.SID_SaveDIB(IRFrente, CrearPathDestinoFrente("FrenteIR.bmp"));
                                                    Globales.EscribirBitacora("Deposito", "SID_DIB IRFRENTE: ", Reserved.ToString(), 3);
                                                    Reserved = SidLib.SID_SaveDIB(IRBack, CrearPathDestinoReverso("ReversoIR.bmp"));
                                                    Globales.EscribirBitacora("Deposito", "SID_DIB IRBACK: ", Reserved.ToString(), 3);

                                                    if (Frente != 0)
                                                        SidLib.SID_FreeImage(Frente);
                                                    if (Back != 0)
                                                        SidLib.SID_FreeImage(Back);
                                                    if (IRFrente != 0)
                                                        SidLib.SID_FreeImage(IRFrente);
                                                    if (IRBack != 0)
                                                        SidLib.SID_FreeImage(IRBack);
                                                    if (MgFrente != 0)
                                                        SidLib.SID_FreeImage(MgFrente);
                                                    if (Mgback != 0)
                                                        SidLib.SID_FreeImage(Mgback);


                                                }
                                                //Globales.EscribirBitacora("Deposito", "Detalle Billete", "Reply: " + l_Reply.ToString()+ " --Result"+  result[0].ToString("X2"),1);

                                                SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/

                                            }




                                            l_Reply = SidLib.SID_WaitValidationResult(result);

                                        }

                                        SidLib.SID_ClearDenominationNote();

                                        if (-211 < l_Reply && l_Reply <= -201)
                                        {
                                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                                            intentos = 0;
                                            using (FormaError f_Error = new FormaError(false, "atasco"))
                                            {
                                                Error_Sid(l_Reply, out l_Error);
                                                Globales.EscribirBitacora("Deposito", "Atasco en el Deposito", l_Error, 1);

                                                f_Error.c_MensajeError.Text = "Ocurrio un atasco de billetes. Causa : " + l_Error;
                                                f_Error.ShowDialog();
                                                c_Depositar.Enabled = false;
                                                //pictureBox3.Enabled = false;
                                                //Close();
                                                // return;
                                            }
                                            do
                                            {
                                                try
                                                {
                                                    l_Reply = SidLib.ResetPath();
                                                }
                                                catch { }
                                                finally
                                                {
                                                    l_Reply = SidLib.SID_Open(l_Noscanimages);
                                                }
                                                l_Reply = SidLib.ResetError();

                                                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                                Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", l_Reply.ToString(), 1);
                                                intentos++;
                                            } while (l_Reply != 0 && intentos < 3);

                                            SidLib.SID_Close();
                                            c_BotonAceptar_Click(null, null);
                                        }

                                        if (l_Reply == -13)
                                        {
                                            Error_Sid(l_Reply, out l_Error);
                                            Globales.EscribirBitacora("Deposito", "ERROR en el Deposito", l_Error, 1);

                                            SidLib.ResetPath();
                                            SidLib.ResetError();
                                            SidLib.SID_Close();

                                            Globales.EscribirBitacora("Deposito de Efectivo", "Error en Deposito", l_Reply.ToString(), 1);
                                            c_BotonAceptar_Click(null, null);

                                        }

                                        if (l_Reply == -360)
                                        {
                                            Globales.CambioEstatusReceptor(Globales.EstatusReceptor.No_Operable);
                                            Error_Sid(l_Reply, out l_Error);
                                            Globales.EscribirBitacora("Deposito", "ERROR en el RECEPTOR, Recomendacion apagar unos minutos", l_Error, 1);


                                            SidLib.ResetError();
                                            SidLib.SID_Close();

                                            Globales.EscribirBitacora("Deposito de Efectivo ", "Error durante Deposito: ", l_Reply.ToString(), 1);
                                            c_BotonAceptar_Click(null, null);

                                        }

                                        SidLib.ResetError();

                                    }


                                }
                                else
                                    using (FormaError f_Error = new FormaError(false,"malFucionamiento"))
                                    {
                                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                                        Error_Sid(l_Reply, out l_Error);
                                        Globales.EscribirBitacora("Deposito", "Problema en el Deposito", l_Error, 1);
                                        if ( sensor[7] == 150 && Properties.Settings.Default.Enviar_AlertasGSI )
                                            UtilsComunicacion.AlertaGSI( -222 , "Bolsa No Presente " );
                                        f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde. Causa : " + l_Error + "Sensor STatus en ALERTA Revisar LOCK,Swhicth u otros: "+ sensor[7];
                                        f_Error.ShowDialog();
                                        c_Depositar.Enabled = true;
                                        c_BotonAceptar.Enabled = true;
                                      
                                        //pictureBox3.Enabled = true;
                                        //Close();
                                        return;
                                    }


                            }


                            l_Reply = SidApi.SidLib.SID_Logout();
                            Globales.EscribirBitacora("SELECT DEPOSITO", "SID_LOgout", "logout: " + l_Reply, 3);
                            // SidLib.ResetError();
                            SidLib.SID_Close();

                        }
                        catch (Exception ex)
                        {
                            Globales.EscribirBitacora("Deposito", "LOOP VALIDACION", "Execpcion  = " + ex.Message, 1);
                        }



                        Globales.EscribirBitacora("Deposito", "AutoDOCHANDLE", "Estatus = " + l_Reply.ToString(), 1);
                     
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception ex)
                {
                }

            }
            else
            {
                //test();
          //Llenar_Aleatorio();
          //Llenar_Aleatorio_Monedas();
          //     Llenar_Aleatorio_extra( );
                
                EscribirResultados();
            }
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();
            if (l_TotalRechazados > 0)
            {
                using (FormaError f_Error = new FormaError(true,"retireBilletes"))
                {
                    f_Error.c_MensajeError.Text = "Por favor retire los billetes rechazados";
                    f_Error.ShowDialog();
                    l_TotalRechazados = 0;
                }
            }

            c_Depositar.Enabled = true;
         //   pictureBox3.Enabled = true;
         //   c_Monedas.Enabled = true;
            c_integrado.Enabled = true;

            //  System.Threading.Thread.Sleep(5000);
            c_BotonAceptar.Text = "Terminar";
            c_BotonAceptar.Enabled = true;
          

            Globales.EscribirBitacora("Deposito", "Total aceptados parcial", l_TotalAceptados.ToString(), 1);
            c_Depositar.Text = "ALGO MAS";

        }

       bool Candado = false;
       

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {

            c_Depositar.Enabled = false;
            c_BotonAceptar.Enabled = false;



            System.Threading.Thread.Sleep( 100 );  //Esperar que SID termine ejecucion de Cashin
           


            if ( Intentos_Globales < 3 )
            {
                //reviso Trayectoria

                // realizo un Freepath Si atoramiento doy ticket

                // Verificar Rechazo

                // Realizo dar ticket

                if ( Properties.Settings.Default.No_Debug_Pc )
                    try
                    {
                        int intentos = Intentos_Globales;
                        Byte[] key = new Byte[1];
                        Byte[] sensor = new Byte[64];

                        //VERIFICAR TRAYECTORIA DE BILLETES
                        l_Reply = SidLib.SID_Open( l_Noscanimages );
                        Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "OPEN" , l_Reply.ToString( ) , 1 );
                        if ( l_Reply != 0 )
                            l_Reply = SidLib.ResetError( );


                        l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                        Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "UnitStatus" , l_Reply.ToString( ) , 1 );

                        //atascos    -10
                        if ( -215 <= l_Reply && l_Reply <= -201 )
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                            Error_Sid( l_Reply , out l_Error );
                            Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "Atasco de Billetes::" , l_Error , 1 );

                            do
                            {
                                Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "Recuperacion intento: " + intentos , l_Error , 1 );
                                using ( FormaError f_Error = new FormaError( false , "atasco" ) )
                                {
                                    f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                                    if ( intentos == 2 )
                                        f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                    f_Error.ShowDialog( );
                                }
                                try
                                {
                                    l_Reply = SidLib.ResetPath( );
                                }
                                catch
                                {
                                }
                                finally
                                {
                                    l_Reply = SidLib.SID_Open( l_Noscanimages );
                                }
                                l_Reply = SidLib.ResetError( );
                                l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "UnitStatus" , l_Reply.ToString( ) , 1 );
                                intentos++;
                                Intentos_Globales++;
                            } while ( l_Reply != 0 && intentos < 3 );
                            SidLib.SID_Close( );

                            if (Intentos_Globales < 3)
                            {

                                c_Depositar.Enabled = true;
                                c_BotonAceptar.Enabled = true;

                                return;
                            }

                        }

                        if ( -246 <= l_Reply && l_Reply <= -239 )
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                            Error_Sid( l_Reply , out l_Error );
                            Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "Problema DIP_SWITCH::" , l_Error , 1 );
                            intentos = Intentos_Globales++;

                            do
                            {
                                Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "Areas Cerradas completamente: " + intentos , l_Error , 1 );
                                using ( FormaError f_Error = new FormaError( false , "switchAbierto" ) )
                                {
                                    f_Error.c_MensajeError.Text = l_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                                    if ( intentos == 2 )
                                        f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                    f_Error.ShowDialog( );
                                    l_Reply = SidLib.ResetError( );


                                    // c_BotonCancelar_Click(this, null);
                                }
                                l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "UnitStatus" , l_Reply.ToString( ) , 1 );
                                intentos++;
                                Intentos_Globales++;
                            } while ( l_Reply != 0 && intentos < 3 );
                            SidLib.SID_Close( );
                            if (Intentos_Globales < 3)
                            {

                                c_Depositar.Enabled = true;
                                c_BotonAceptar.Enabled = true;

                                return;
                            }
                        }
                    }
                    catch ( Exception exx )
                    {
                        Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES" , "excepcion Encontrada" , exx.Message , 1 );

                    }


            }
            //  Evita Doble envio de transaccion ENero 2017



            if (Candado)
            {
                Globales.EscribirBitacora("Deposito", "Se ha detectado envio Duplicado", "Motivo Candado Aplicado", 1);
                UtilsComunicacion.AlertaGSI(90, "SID_BLOQUEO_DOUBLE_EQUIPO");
                return;
            }
            if (!Candado)
            {
               
                Candado = true;
                Globales.EscribirBitacora("Deposito", "Aplicando Candado de 1 Envio", "Candado Activado", 1);


            }


            if (Globales.FechaInicioTransaccionEnviada == l_FechaHoraInicio)
            {
                Globales.EscribirBitacora("Deposito", "Se ha detectado envio Duplicado", "Motivo Fecha hora Duplicada", 1);
                UtilsComunicacion.AlertaGSI(90,"SID_BLOQUEO_DOUBLE_EQUIPO");
                this.DialogResult = DialogResult.Abort;
                return;
            }



            //  Evita Doble envio de transaccion ENero 2017

            //continuar

            c_Depositar.Enabled = false;
            c_BotonAceptar.Enabled = false;
           
        //    pictureBox3.Enabled = false;
              //Reloj
            //c_accion.Image = Properties.Resources.appointment_soon;
            //c_accion.SizeMode = PictureBoxSizeMode.Zoom;
            c_enviando.Visible = true;
            Forzardibujado();

       

            int[] l_sobrante = new int[6];
            int[] l_faltante = new int[6];

            if (l_TotalEfectivo > 0 || l_Total_Monedas > 0)
            {
                if (l_TotalEfectivo > 0)
                {
                    Globales.EscribirBitacora("Deposito", "Contabilidad en Pantalla", "BIlletes Validados\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad Actual: " + l_Total1000 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad Actual: " + l_Total500 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad Actual: " + l_Total200 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad Actual: " + l_Total100 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad Actual: " + l_Total50 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad Actual: " + l_Total20 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", " Cantidad total Actual: " + l_TotalEfectivo + "\n\r", 1);

                    Globales.EscribirBitacora("Deposito", "Contabilidad en Pantalla", "Monedas Validadas\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 10 Cantidad Actual: " + l_Total10 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 5 Cantidad Actual: " + l_Total5 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 2 Cantidad Actual: " + l_Total2 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 1 Cantidad Actual: " + l_Total1 + "\n\r", 1);
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 50c  Cantidad Actual: " + l_Total50c + "\n\r", 1);
                   
                    Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", " Cantidad total Actual Moneda: " + l_Total_Monedas + "\n\r", 1);

                    ///////////////////Correccion ante Atascos 
                    if (Properties.Settings.Default.No_Debug_Pc)
                    {
                        Globales.EscribirBitacora("Deposito", "Contabilidad EN BOLSA", "BIlletes ACEPTADOS y CONTADOS\n\r", 1);
                        l_bag_deposito = total_deposito_en_bolsa(l_bag_inicio, Contenido_en_Bolsa());

                        Escribir_ContenidoBolsa(l_bag_deposito);

                        if (l_bag_deposito == null)
                        {
                            l_bag_deposito = new ushort[6];

                            Double l_TotalEfectivoFaltante = 0;

                            l_faltante[5] = l_Total1000 - l_bag_deposito[5];
                            l_faltante[4] = l_Total500 - l_bag_deposito[4];
                            l_faltante[3] = l_Total200 - l_bag_deposito[3];
                            l_faltante[2] = l_Total100 - l_bag_deposito[2];
                            l_faltante[1] = l_Total50 - l_bag_deposito[1];
                            l_faltante[0] = l_Total20 - l_bag_deposito[0];

                            l_TotalEfectivoFaltante = l_faltante[5] * 1000 + l_faltante[4] * 500 + l_faltante[3] * 200 + l_faltante[2] * 100 + l_faltante[1] * 50 + l_faltante[0] * 20;

                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[5] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[4] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[3] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[2] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[1] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[0] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Cantidad total PRESUNTAMENTE en bolsa: " + l_TotalEfectivoFaltante + "\n\r", 1);
                            // EscribirResultados();
                            // Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA",1);
                            Escribir_Log_faltante(l_faltante);
                            Globales.EscribirBitacora("Registro Deposito", "Registro sin CONFRONTAR", "FIN Registro", 1);

                            l_faltante[5] = 0;
                            l_faltante[4] = 0;
                            l_faltante[3] = 0;
                            l_faltante[2] = 0;
                            l_faltante[1] = 0;
                            l_faltante[0] = 0;
                            l_TotalEfectivoFaltante = 0;
                        }
                        else
                        {

                            Double l_TotalEfectivoFaltante = 0;
                            ///compara una a una las entradas
                            if (l_Total1000 >= l_bag_deposito[5])
                            {
                                l_sobrante[5] = l_Total1000 - l_bag_deposito[5];
                                l_Total1000 = l_bag_deposito[5];

                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[5] * 1000;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[5]);

                            }
                            else
                            {
                                l_faltante[5] = l_bag_deposito[5] - l_Total1000;
                                l_TotalEfectivoFaltante += l_faltante[5] * 1000;


                            }

                            if (l_Total500 >= l_bag_deposito[4])
                            {
                                l_sobrante[4] = l_Total500 - l_bag_deposito[4];
                                l_Total500 = l_bag_deposito[4];
                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[4] * 500;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[4]);
                            }
                            else
                            {
                                l_faltante[4] = l_bag_deposito[4] - l_Total500;
                                l_TotalEfectivoFaltante += l_faltante[4] * 500;

                            }

                            if (l_Total200 >= l_bag_deposito[3])
                            {
                                l_sobrante[3] = l_Total200 - l_bag_deposito[3];
                                l_Total200 = l_bag_deposito[3];
                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[3] * 200;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[3]);
                            }
                            else
                            {
                                l_faltante[3] = l_bag_deposito[3] - l_Total200;
                                l_TotalEfectivoFaltante += l_faltante[3] * 200;

                            }

                            if (l_Total100 >= l_bag_deposito[2])
                            {
                                l_sobrante[2] = l_Total100 - l_bag_deposito[2];
                                l_Total100 = l_bag_deposito[2];

                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[2] * 100;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[2]);
                            }
                            else
                            {

                                l_faltante[2] = l_bag_deposito[2] - l_Total100;
                                l_TotalEfectivoFaltante += l_faltante[2] * 100;

                            }

                            if (l_Total50 >= l_bag_deposito[1])
                            {
                                l_sobrante[1] = l_Total50 - l_bag_deposito[1];
                                l_Total50 = l_bag_deposito[1];
                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[1] * 50;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[1]);
                            }
                            else
                            {
                                l_faltante[1] = l_bag_deposito[1] - l_Total50;
                                l_TotalEfectivoFaltante += l_faltante[1] * 50;


                            }

                            if (l_Total20 >= l_bag_deposito[0])
                            {
                                l_sobrante[0] = l_Total20 - l_bag_deposito[0];
                                l_Total20 = l_bag_deposito[0];
                                l_TotalEfectivo = l_TotalEfectivo - l_sobrante[0] * 20;
                                l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[0]);
                            }
                            else
                            {
                                l_faltante[0] = l_bag_deposito[0] - l_Total20;
                                l_TotalEfectivoFaltante += l_faltante[0] * 20;

                            }


                            if (obtener_cantidad_total(l_sobrante) > 0)
                            {
                                Globales.EscribirBitacora("Deposito", "DIFERENCIA DEPOSITO VS ACEPTADOS", "Empieza AJUSTE", 1);

                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad Actual: " + l_Total1000 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad Actual: " + l_Total500 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad Actual: " + l_Total200 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad Actual: " + l_Total100 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad Actual: " + l_Total50 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad Actual: " + l_Total20 + "\n\r", 1);
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Cantidad total Actual: " + l_TotalEfectivo + "\n\r", 1);
                                EscribirResultados();
                                Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA", 1);
                                Escribir_Log_sobrante(l_sobrante);
                                Globales.EscribirBitacora("Deposito", "AJUSTE REALIZADO CON SOBRANTE", "FIN de AJUSTE", 1);

                            }

                            if (obtener_cantidad_total(l_faltante) > 0)
                            {
                                Globales.EscribirBitacora("Deposito", "DIFERENCIA DEPOSITO VS ACEPTADOS", "Empieza Registro", 1);

                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad FALTANTE en Conteo: " + l_faltante[5] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad FALTANTE en Conteo: " + l_faltante[4] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad FALTANTE en Conteo: " + l_faltante[3] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad FALTANTE en Conte: " + l_faltante[2] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad FALTANTE en Conteo: " + l_faltante[1] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad FALTANTE en Conteo: " + l_faltante[0] + "\n\r", 1);
                                Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Cantidad total FALTANTE en Conteo: " + l_TotalEfectivoFaltante + "\n\r", 1);
                                // EscribirResultados();
                                // Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA",1);
                                Escribir_Log_faltante(l_faltante);
                                Globales.EscribirBitacora("Registro Deposito", "Registro de FALTANTE en CONTEO", "FIN Registro", 1);

                                l_TotalEfectivo = l_TotalEfectivo + l_TotalEfectivoFaltante;
                            }


                            if (obtener_cantidad_total(l_sobrante) == 0 && obtener_cantidad_total(l_faltante) == 0)
                                Globales.EscribirBitacora("Deposito", "Contabilidad", "Deposito Congruente, sin ajustes", 1);
                        }
                    }
                    //////////////FIN CORRECCION
                }//BIlletes 

               


                DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
                if (l_Total1000 > 0 || l_faltante[5] >0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "1000");
                    l_Nueva[1] = l_Total1000 + l_faltante[5];
                    l_Nueva[2] = "1000";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total500 > 0 || l_faltante[4] > 0 )
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "500");
                    l_Nueva[1] = l_Total500 + l_faltante[4];
                    l_Nueva[2] = "500";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total200 > 0 || l_faltante[3] > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "200");
                    l_Nueva[1] = l_Total200 + l_faltante[3];
                    l_Nueva[2] = "200";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total100 > 0 || l_faltante[2] > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "100");
                    l_Nueva[1] = l_Total100 + l_faltante[2];
                    l_Nueva[2] = "100";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total50 > 0 || l_faltante[1] > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "50");
                    l_Nueva[1] = l_Total50 + l_faltante[1];
                    l_Nueva[2] = "50";
                    l_Detalle.Rows.Add(l_Nueva);
                }

                if (l_Total20 > 0 || l_faltante[0] > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "20");
                    l_Nueva[1] = l_Total20 + l_faltante[0];
                    l_Nueva[2] = "20";

                    l_Detalle.Rows.Add(l_Nueva);
                }

                //MONEDAS

                if (l_Total10 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "10.0");
                    l_Nueva[1] = l_Total10;
                    l_Nueva[2] = "10";

                    l_Detalle.Rows.Add(l_Nueva);
                }

                if (l_Total5 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "5.00");
                    l_Nueva[1] = l_Total5;
                    l_Nueva[2] = "5";

                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total2 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "2.00");
                    l_Nueva[1] = l_Total2;
                    l_Nueva[2] = "2";

                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total1 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "1.00");
                    l_Nueva[1] = l_Total1;
                    l_Nueva[2] = "1";

                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total50c > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.50");
                    l_Nueva[1] = l_Total50c;
                    l_Nueva[2] = "0.5";

                    l_Detalle.Rows.Add(l_Nueva);
                }

                // 0.20 -- 0.10

                Cursor.Current = Cursors.Default;

                //Validar -fchade ultima transaccion y Importetotal Receptor

                if (!UtilsComunicacion.Enviar_Transaccion(Globales.NombreCliente, 1, 1, 1, l_TotalEfectivo + l_Total_Monedas,
                    l_Detalle, 0, true, Globales.IdUsuario))
                {
                    //actualizacion de Importe y Fecha Enviado
                    Globales.FechaInicioTransaccionEnviada = l_FechaHoraInicio;

                    using (FormaError f_Error = new FormaError(false,"warning"))
                    {
                        f_Error.c_MensajeError.Text = "Hay un Problema al Registar la Transaccion Avise Al Administrador";
                        f_Error.ShowDialog();
                    

                        Close();
                        return;
                    }
                }
                else
                {
                    // Santander.Escribir_Transaccion(Santander.CrearTransaccion(Santander.CrearDesglose(l_Detalle), 1, Globales.NoCuenta, Globales.IdUsuario, l_TotalEfectivo));
                    //actualizacion de Importe y Fecha Enviado
                    Globales.FechaInicioTransaccionEnviada = l_FechaHoraInicio;
                    this.DialogResult = DialogResult.OK;
                   
                    //using (FormFINdeposito l_fin = new FormFINdeposito(true))
                    //{

                    //    l_fin.ShowDialog();

                       

                    //}
                   // c_accion.Image = Properties.Resources.appointment_new_2;

                    GC.Collect();
                    //   GC.WaitForPendingFinalizers();
                 
                    return;


                    //using (FormaError f_Error = new FormaError(true, "exitoso"))
                    //{
                    //    Error_Sid(l_Reply, out l_Error);
                    //    Globales.EscribirBitacora("Deposito", "Terminar Deposito", l_Error, 1);
                    //    f_Error.c_MensajeError.Text = "Deposito Exitoso";
                    //    f_Error.ShowDialog();
                    //    Dispose();
                    //    GC.Collect();
                    //    GC.WaitForPendingFinalizers();

                    //    Close();
                    //    return;
                    //}


                }
            }
            else
            {
                using (FormaError f_Error = new FormaError(true,"sinDinero"))
                {


                    Globales.EscribirBitacora("Deposito", "SIN Deposito", l_Error, 1);
                    f_Error.c_MensajeError.Text = "No ha depositado nada";
                    f_Error.ShowDialog();
                    Dispose();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    Close();
                    return;
                }

            }
           
        }

        private void FormaDeposito_Load(object sender, EventArgs e)
        {
            GC.Collect();

            if (Properties.Settings.Default.SalvarImagenes)
            {
                l_Noscanimages = true;
                Creardirectoriodestino();

            }

            c_empleado.Text = Globales.NombreUsuario.ToUpper();
            c_EtiquetaNumeroCuenta.Text = Globales.NombreCliente;

           

            string CuentaTmp = Globales.IdUsuario;
            try
            {

                CuentaTmp = CuentaTmp.Replace(System.Environment.NewLine, "");

                CuentaTmp = "********" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);

            }
            catch (Exception E)
            {
                //throw;
                c_cuenta.Text = Globales.IdUsuario;
            }

            if ( !String.IsNullOrEmpty( Globales.ReferenciaGSI ) )
            {
                l_ref.Visible = true;
                l_ref.Text += " " + Globales.ReferenciaGSI;
            }
            c_cuenta.Text = CuentaTmp;
            Cursor.Hide();



            if (Properties.Settings.Default.No_Debug_Pc)
            {
                Globales.EscribirBitacora("Deposito", "DETALLE BOLSA ANTES DEL DEPOSITO", "CONTABILIDAD", 1);
                l_bag_inicio = Contenido_en_Bolsa();

                l_topefijo = l_topeDeposito = Properties.Settings.Default.CapacidadBolsa - obtener_cantidad_total(l_bag_inicio);

                c_tope.Text = "Maximo Permitido: " + l_topeDeposito + " Billetes";

                if (l_topeDeposito < 300)
                {
                    Procesar_NUM_Documentos = (ushort)l_topeDeposito;
                    c_tope.Visible = true;
                }

                if (l_bag_inicio == null)
                    using (FormaError f_Error = new FormaError(true, "noDisponible"))
                    {
                        Error_Sid(l_Reply, out l_Error);
                        Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "Funcion 'Contenido_en_Bolsa'", 1);
                        Registrar.Alerta("Problema al obtener Billetes de Bolsa, Sugerir reinicio de Sistema");
                        f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde, Reinicie el Equipo si persiste este Error ";
                        f_Error.ShowDialog();
                        c_Depositar.Enabled = false;
                   //     pictureBox3.Enabled = false;
                        Close();
                    }

                }else
                l_topeDeposito = Properties.Settings.Default.CapacidadBolsa;
        }


        #region Conteo en Bolsa

        private void Llenar_Aleatorio()
        {
            Random rd = new Random();
            int l_ValorR;
            for (int i = 0; i < 50; i++) //50
            {
                l_ValorR = rd.Next(0, 1001); //1001
                if (l_ValorR <= 20)
                {
                    l_TotalAceptados++;
                    l_Total20++;
                }
                else if (l_ValorR <= 50)
                {
                    l_TotalAceptados++;
                    l_Total50++;
                }
                else if (l_ValorR <= 100)
                {
                    l_TotalAceptados++;
                    l_Total100++;
                }
                else if (l_ValorR <= 200)
                {
                    l_TotalAceptados++;
                    l_Total200++;
                }
                else if (l_ValorR <= 500)
                {
                    l_Total500++;
                    l_TotalAceptados++;
                }
                else if (l_ValorR <= 1000)
                {
                    l_TotalAceptados++;
                    l_Total1000++;
                }
                else
                    l_TotalRechazados++;
            }
        }

        private void Llenar_Aleatorio_extra( )
        {

            //l_Total20 = 0;
            //l_Total50c = 64;
            //l_Total1 = 568;
            //l_Total2 = 171;
            //l_Total5 = 357;
            //l_Total10 = 345;

            



        }

        private ushort[] total_deposito_en_bolsa(ushort[] l_inicio, ushort[] l_fin)
        {
            ushort[] l_resta = new ushort[6];

            if (l_inicio != null && l_fin != null)
                for (int i = 0; i < 6; i++)
                {
                    l_resta[i] = (ushort)(l_fin[i] - l_inicio[i]);
                }
            else if (l_fin == null)
            {
                l_resta = null;
                Registrar.Alerta("Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario:  " + Globales.NombreUsuario + "Hora evento: " + DateTime.Now);
                Globales.EscribirBitacora("Comparar Bolsas", "total_deposito_en_bolsa", "Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario:  " + Globales.NombreUsuario + "Hora evento: " + DateTime.Now, 3);
            }


            return l_resta;

        }

        private ushort[] Contenido_en_Bolsa()
        {
            int contador = 1;
            int l_Reply;
            if (Properties.Settings.Default.No_Debug_Pc)
            {
                ushort[] l_totalNotes = new ushort[64];

                l_Reply = SidLib.SID_Open(false);
                Error_Sid(l_Reply, out l_Error);
                Globales.EscribirBitacora("Contenido bolsa", "sid_open", l_Reply.ToString(), 1);

                do
                {

                    if (contador %10 ==0)
                    {
                        l_Reply = SidLib.SID_Open(false);
                        Error_Sid(l_Reply, out l_Error);
                        Globales.EscribirBitacora("Contenido bolsa", "sid_open", l_Reply.ToString(), 1);
                    }
                    contador++;

                    if (contador == 100)
                        l_Reply = SidLib.SID_CURRENT_ERROR;

                } while (l_Reply == SidLib.SID_COMMAND_IN_EXECUTION_YET);


                if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_ALREADY_OPEN)
                {
                    contador = 0;
                    do
                    {
                        l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);

                        if (contador == 100)
                            break;

                    } while (l_Reply != SidLib.SID_OKAY);

                    // l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);
                    Error_Sid(l_Reply, out l_Error);
                    Globales.EscribirBitacora("Contenido en bolsa", "Global total notes obtenido", l_Error, 1);

                    if (l_Reply == SidLib.SID_OKAY)
                    {
                        l_Reply = SidLib.SID_Close();
                        Escribir_ContenidoBolsa(l_totalNotes);
                        return l_totalNotes;
                    }
                    else
                    {
                        Globales.EscribirBitacora("GetTotalCash", "Sid_open", " No se pudo obtener el total en Bolsa: " + l_Error, 1);
                        l_Reply = SidLib.SID_Close();
                        return null;
                    }
                }
                else
                {
                    Globales.EscribirBitacora("GetTotalCash", "Sid_open", " No se pudo obtener el total en Bolsa: " + l_Error, 1);
                    return null;
                }
            }
            else
                return null;

        }


        private void Escribir_Log_sobrante(int[] l_datos)
        {

            for (int i = 0; i < l_datos.Length; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Macheo de billetes", "Diferencia de Billetes Cantidad :  " + l_datos[i] + "  Denominacion 0..5 : " + i.ToString(), 1);

                }
            }


        }

        private void Escribir_Log_faltante(int[] l_datos)
        {

            for (int i = 0; i < l_datos.Length; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Macheo de billetes", "Diferencia de Billetes Cantidad :  " + l_datos[i] + "  Denominacion 0..5 : " + i.ToString(), 1);

                }
            }


        }

        private void Escribir_ContenidoBolsa(int[] l_datos)
        {

            for (int i = 0; i < 6; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Contenido bolsa", "  Denominacion: " + i.ToString() + " Cantidad Actual: " + l_datos[i], 1);

                }
            }


        }

        private void Escribir_ContenidoBolsa(ushort[] l_datos)
        {

            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    if (l_datos[i] != 0)
                    {
                        Globales.EscribirBitacora("Deposito", " Registro SID ", "  Denominacion: " + i.ToString() + " Cantidad Esperada: " + l_datos[i], 1);

                    }
                }
            else
            {
                for (int i = 0; i < 6; i++)
                {

                    Globales.EscribirBitacora("Deposito", " Registro SID ", "  Denominacion: " + i.ToString() + " Cantidad Esperada: " + "NO DETERMINADO", 1);


                }

            }


        }

        private int obtener_cantidad_total(ushort[] l_datos)
        {
            int l_suma_billetes = 0;
            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    l_suma_billetes += l_datos[i];
                }

            Globales.EscribirBitacora("Deposito", "Contenido bolsa", " Cantidad total en bolsa # de billetes: " + l_suma_billetes, 1);
            return l_suma_billetes;
        }

        private int obtener_cantidad_total(int[] l_datos)
        {
            int l_suma_billetes = 0;
            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    l_suma_billetes += l_datos[i];
                }

            // Globales.EscribirBitacora("Deposito", "Contenido bolsa", " Cantidad total en bolsa # de billetes: " + l_suma_billetes, 1);
            return l_suma_billetes;
        }

        public void Forzardibujado()
        {
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();

        }

        #endregion

        #region SALVAR IMAGENES
        private void Creardirectoriodestino()
        {
            try
            {
                String l_carpetaDia = DateTime.Today.ToString("d_dddd");
                String l_caprteaMes = DateTime.Today.ToString("MMMM");
                String l_horaTransaccion = DateTime.Now.ToString("HH_mm_ss");

                l_Path = l_directorioimages + "\\" + l_caprteaMes + "\\" + l_carpetaDia + "\\" + Globales.IdUsuario + "\\" + l_horaTransaccion;
                Directory.CreateDirectory(l_Path);
                Globales.EscribirBitacora("Deposito con Imagenes ", "Creardirectoriodestino", l_Path, 1);
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Deposito con Imagenes ", ex.Message, l_Path, 1);
            }

        }

        private string CrearPathDestinoFrente(string l_sufijo)
        {

            String l_fin = "\\" + l_numerobilleteF.ToString("00000") + l_sufijo;
            l_numerobilleteF++;
            return l_Path + l_fin;
        }

        private string CrearPathDestinoReverso(string l_sufijo)
        {

            String l_fin = "\\" + l_numerobilleteR.ToString("00000") + l_sufijo;
            l_numerobilleteR++;
            return l_Path + l_fin;
        }
        #endregion


        public void Error_Sid( int l_Reply, out String p_Error )
        {
            String l_Error = "";

            SidLib.Error_Sid( l_Reply, out l_Error );
            p_Error = l_Error;

            if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply < 0 )
                UtilsComunicacion.AlertaGSI( l_Reply, l_Error );


            if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
            {
                UtilsComunicacion.AlertaGSI( l_Reply, l_Error );
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
            }


        }

        Dictionary<decimal, int> l_DetalleMonedas;
        bool MonedasFin = false;
        Decimal l_Parcial_Monedas = 0;
        Double l_Total_Monedas;
        private void bw_monedas_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Properties.Settings.Default.No_Debug_Pc)
            {
                string l_errorM;
                int l_nuevoError;
                try
                {
                    l_DetalleM = BDDeposito.ObtenerDetalleDeposito(0);
                    Globales.EscribirBitacora("FormaDepositoMixto", "bw_monedas_DoWork <<", "", 3);
                    int result = UCoin.Depositar(Properties.Settings.Default.Port_YUGO, Globales.IdUsuario, out l_Parcial_Monedas, out l_DetalleMonedas, 3);
                    Globales.EscribirBitacora("FormaDepositoMixto", "bw_monedas_DoWork >>", "", 3);


                    if (result != 0)
                    {
                        ErroresUcoin.Error_Ucoin(result, out l_nuevoError, out l_errorM);
                        if (Properties.Settings.Default.Enviar_AlertasGSI)
                            UtilsComunicacion.AlertaGSI(l_nuevoError, l_errorM);
                    }
                }
                catch (Exception exx)
                {
                    Globales.EscribirBitacora("Deposito Monedas", "Problema de Comunicacion UCOIN", exx.Message, 1);
                }

               
            }
            else
            {
              //  Llenar_Aleatorio_Monedas();
                EscribirResultadosM();
            }

        }

        private void Llenar_Aleatorio_Monedas()
        {
            Ramdom = true;

            Random rd = new Random();
            int l_ValorR;
            for (int i = 0; i < 50; i++) //50
            {
                l_ValorR = rd.Next(0, 1001); //1001
                if (l_ValorR <= 20)
                {
                  
                    
                }
                else if (l_ValorR <= 50)
                {
                  
                    l_Total50c++;
                }
                else if (l_ValorR <= 100)
                {
                  
                    l_Total1++;
                }
                else if (l_ValorR <= 200)
                {
                  
                    l_Total2++;
                }
                else if (l_ValorR <= 500)
                {
                    l_Total5++;
                    
                }
                else if (l_ValorR <= 1000)
                {
                    
                    l_Total10++;
                }
                
            }
        }

        private void bw_monedas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (l_DetalleMonedas != null)
            {

                Globales.EscribirBitacora("Deposito MONEDAS", "Parcial Monedas: ", l_Parcial_Monedas.ToString("###,##0.00"), 3);
                foreach (KeyValuePair<decimal, int> Par in l_DetalleMonedas)
                {
                    // -- GENERAR FILAS DE LA TABLA Monedas


                    // DataRow l_Nueva = l_Detalle.NewRow();
                    DataRow l_NuevaM = l_DetalleM.NewRow();
                    l_NuevaM[0] = BDDenominaciones.ObtenerIdDenominacion(0, Par.Key.ToString("###,##0.00").Substring(0, 4));
                    l_NuevaM[1] = Par.Value;
                    l_NuevaM[2] = Par.Key.ToString("###,##0.00");

                    l_DetalleM.Rows.Add(l_NuevaM);
                    //   l_Detalle.Rows.Add(l_Nueva);

                }
            }

            //comentar para Productivo 2 lineas
            //Globales.EscribirBitacora("Deposito Monedas", "Simulacion de  Comunicacion UCOIN", "4 segundos", 1);
            //System.Threading.Thread.Sleep(4000);

            MonedasFin = true;
            EscribirResultadosM();
            //pictureBox3.Enabled = true;
            c_BotonAceptar.Enabled = true;
          //  c_Monedas.Enabled = true;
            c_integrado.Enabled = true;
            Refresh();
        }

        private void EscribirResultadosM()
        {
            //Double l_Efectivo20;
            Double l_Efectivo10;
            Double l_Efectivo5;
            Double l_Efectivo2;
            Double l_Efectivo1;
            Double l_Efectivo50c;
            //Double l_Efectivo20c;
            //Double l_Efectivo10c;
          


            if (MonedasFin)
            {
                //Cambiar esta forma de los DATOs
                if (!Ramdom && l_DetalleM != null)                {
                    l_Total50c += (int)l_DetalleM.Rows[2][1];
                    l_Total1 += (int)l_DetalleM.Rows[3][1];
                    l_Total2 += (int)l_DetalleM.Rows[4][1];
                    l_Total5 += (int)l_DetalleM.Rows[5][1];
                    l_Total10 += (int)l_DetalleM.Rows[6][1];
                }

                c_Cantidad50c.Text = l_Total50c.ToString();
                c_Cantidad1.Text = l_Total1.ToString();
                c_Cantidad2.Text = l_Total2.ToString();
                c_Cantidad5.Text = l_Total5.ToString();
                c_Cantidad10.Text = l_Total10.ToString();

                //l_Efectivo10c = (int)l_DetalleM.Rows[0][1] * 0.1;
                // l_Efectivo20c = (int)l_DetalleM.Rows[1][1] * 0.2;
                l_Efectivo50c = l_Total50c * 0.5;
                l_Efectivo1 = l_Total1 * 1;
                l_Efectivo2 = l_Total2 * 2;
                l_Efectivo5 = l_Total5 * 5;
                l_Efectivo10 = l_Total10 * 10;
                //l_Efectivo20 = (int)l_DetalleM.Rows[7][1] * 20;


                c_Suma50c.Text = l_Efectivo50c.ToString("$#,###,##0.00");
                c_Suma1.Text = l_Efectivo1.ToString("$#,###,##0.00");
                c_Suma2.Text = l_Efectivo2.ToString("$#,###,##0.00");
                c_Suma5.Text = l_Efectivo5.ToString("$#,###,##0.00");
                c_Suma10.Text = l_Efectivo10.ToString("$#,###,##0.00");


                l_Total_Monedas = l_Efectivo50c + l_Efectivo1 + l_Efectivo2 +
                                    l_Efectivo5 + l_Efectivo10;
              
                c_MonedasAceptadas.Text = (l_Total50c + l_Total1 + l_Total2 + l_Total5 + l_Total10).ToString();
                c_montomonedas.Text = l_Total_Monedas.ToString("$#,###,##0.00");


              
                try
                {

                    c_TotalDeposito.Text = (l_TotalEfectivo + l_Total_Monedas).ToString("$#,###,##0.00");
                }
                catch (Exception)
                {
                    c_TotalDeposito.Text = (l_TotalEfectivo + l_Total_Monedas).ToString("$#,###,##0.00");
                }
              
                c_Cantidad10.Update();
                c_Cantidad5.Update();
                c_Cantidad2.Update();
                c_Cantidad1.Update();
                c_Cantidad50c.Update();

                c_Suma10.Update();
                c_Suma5.Update();
                c_Suma2.Update();
                c_Suma1.Update();
                c_Suma50c.Update();

                c_TotalDeposito.Update();

                Ramdom = false;

            }
        }

        private void c_Monedas_Click(object sender, EventArgs e)
        {
            //pictureBox3.Enabled = false;
            c_BotonAceptar.Enabled = false;
            c_Depositar.Enabled = false;
            //c_Monedas.Enabled = false;
            c_integrado.Enabled = false;
            Refresh();
            bw_monedas.RunWorkerAsync();
        }

        private void c_integrado_Click(object sender, EventArgs e)
        {
            try
            {
                bw_monedas.RunWorkerAsync();
            } catch (Exception exx)
            { }

            c_Depositar_Click(null, null);
        }

        
    }
}
