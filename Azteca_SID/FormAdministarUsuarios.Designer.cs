﻿namespace Azteca_SID
{
    partial class FormAdministarUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("AGREGAR", "Nuevo");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("MODIFICAR", "Editar");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("ELIMINAR", "Eliminar");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Cambiar CONTRASEÑA", "Contraseña");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("SALIR", "Salida");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdministarUsuarios));
            this.c_visor = new System.Windows.Forms.ListView();
            this.c_imagenes = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // c_visor
            // 
            this.c_visor.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.c_visor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            listViewItem1.ToolTipText = "Agregar un nuevo Usuario";
            listViewItem2.ToolTipText = "Ver Informacion de Usuario";
            listViewItem3.ToolTipText = "Eliminar Un Usuario Del Sistema";
            listViewItem4.ToolTipText = "Para el Administrador del Equipo";
            listViewItem5.ToolTipText = "Regresar ala Pantalla Anterior";
            this.c_visor.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.c_visor.LargeImageList = this.c_imagenes;
            this.c_visor.Location = new System.Drawing.Point(98, 104);
            this.c_visor.Name = "c_visor";
            this.c_visor.Size = new System.Drawing.Size(602, 357);
            this.c_visor.TabIndex = 1;
            this.c_visor.UseCompatibleStateImageBehavior = false;
            this.c_visor.ItemActivate += new System.EventHandler(this.c_visor_ItemActivate);
            this.c_visor.SelectedIndexChanged += new System.EventHandler(this.c_visor_SelectedIndexChanged);
            // 
            // c_imagenes
            // 
            this.c_imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("c_imagenes.ImageStream")));
            this.c_imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.c_imagenes.Images.SetKeyName(0, "Eliminar");
            this.c_imagenes.Images.SetKeyName(1, "Nuevo");
            this.c_imagenes.Images.SetKeyName(2, "Editar");
            this.c_imagenes.Images.SetKeyName(3, "Salida");
            this.c_imagenes.Images.SetKeyName(4, "Contraseña");
            // 
            // FormAdministarUsuarios
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_visor);
            this.Name = "FormAdministarUsuarios";
            this.Controls.SetChildIndex(this.c_visor, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView c_visor;
        private System.Windows.Forms.ImageList c_imagenes;
    }
}
