﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaConsultaMantenimientos : FormBase
    {
        DataTable t_Operacoiones;
        public FormaConsultaMantenimientos()
        {
            InitializeComponent();
        }

        private void FormaConsultaMantenimientos_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;


            //foreach (String l_correo in Globales.MailDestino.Split(';'))
            //    c_mails.Items.Add(l_correo);
            //c_mails.SelectedIndex = c_mails.Items.Count - 1;


            t_Operacoiones = BDMantenimiento.ObtenerConsulta_mantenimientos ();
            c_ConsultaOperaciones.DataSource = t_Operacoiones;
            c_ConsultaOperaciones.Columns[0].HeaderText = "Folio";
            //  c_ConsultaOperaciones.Columns[1].HeaderText = "Nombre";
            //   c_ConsultaOperaciones.Columns[0].MinimumWidth = 150  ;
            c_ConsultaOperaciones.Columns[1].HeaderText = "Inicio";
            c_ConsultaOperaciones.Columns[2].HeaderText = "Tecnico";
            c_ConsultaOperaciones.Columns[3].HeaderText = "Multivendor";
            c_ConsultaOperaciones.Columns[4].HeaderText = "TipoMantenimiento";
            c_ConsultaOperaciones.Columns[5].HeaderText = "Termino";
          
            c_ConsultaOperaciones.Columns[6].HeaderText = "Comentario";
            c_ConsultaOperaciones.Columns[7].HeaderText = "Concluido";

           // c_ConsultaOperaciones.Columns[5].MinimumWidth = 200;

            Cursor.Hide();

            if (c_ConsultaOperaciones.RowCount == 0)
            {
                c_ConsultaOperaciones.DataSource = null;
                c_numoperaciones.Text = "0";
            }
            else
                c_numoperaciones.Text = c_ConsultaOperaciones.RowCount.ToString();

        }
    }
}
