﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaPerfil : Azteca_SID.FormBase
    {
        public FormaPerfil( bool p_cierreAutomatico):base (p_cierreAutomatico)
        {
            InitializeComponent( );
        }

        public FormaPerfil( )
        {
            InitializeComponent( );
        }

        private void c_administrador_Click( object sender , EventArgs e )
        {
            using ( FormaIniciarSesion f_Inicio = new FormaIniciarSesion( ) )
            {

                f_Inicio.l_IdUsuario = "Supervisor_ZONA";
                f_Inicio.ShowDialog( );
            }
        }

        private void Sucursal_Click( object sender , EventArgs e )
        {
            using ( FormaIniciarSesion f_Inicio = new FormaIniciarSesion( ) )
            {

                f_Inicio.l_IdUsuario = "SUCURSAL";
                f_Inicio.ShowDialog( );
            }
        }

        private void c_Tecnico_Click( object sender , EventArgs e )
        {
            using ( FormaIniciarSesion f_Inicio = new FormaIniciarSesion( ) )
            {

                f_Inicio.l_IdUsuario = "MANTENIMIENTO";
                f_Inicio.ShowDialog( );
            }
        }
    }
}
