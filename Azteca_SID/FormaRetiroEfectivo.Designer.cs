﻿namespace Azteca_SID
{
    partial class FormaRetiroEfectivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_totalenDB = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.c_totalbilletesbolsa = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.c_20 = new System.Windows.Forms.Label();
            this.c_50 = new System.Windows.Forms.Label();
            this.c_100 = new System.Windows.Forms.Label();
            this.c_200 = new System.Windows.Forms.Label();
            this.c_500 = new System.Windows.Forms.Label();
            this.c_1000 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Cajon2_20 = new System.Windows.Forms.Label();
            this.c_Cajon2_50 = new System.Windows.Forms.Label();
            this.c_Cajon2_100 = new System.Windows.Forms.Label();
            this.c_Cajon2_200 = new System.Windows.Forms.Label();
            this.c_Cajon2_500 = new System.Windows.Forms.Label();
            this.c_Cajon2_1000 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_TotalRetiroEfectivo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // c_totalenDB
            // 
            this.c_totalenDB.Enabled = false;
            this.c_totalenDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalenDB.ForeColor = System.Drawing.SystemColors.Info;
            this.c_totalenDB.Location = new System.Drawing.Point(214, 418);
            this.c_totalenDB.Name = "c_totalenDB";
            this.c_totalenDB.ReadOnly = true;
            this.c_totalenDB.Size = new System.Drawing.Size(128, 30);
            this.c_totalenDB.TabIndex = 107;
            this.c_totalenDB.TabStop = false;
            this.c_totalenDB.Text = "0";
            this.c_totalenDB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(7, 423);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 25);
            this.label12.TabIndex = 106;
            this.label12.Text = "Total Billetes :";
            // 
            // c_totalbilletesbolsa
            // 
            this.c_totalbilletesbolsa.Enabled = false;
            this.c_totalbilletesbolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbilletesbolsa.ForeColor = System.Drawing.SystemColors.Info;
            this.c_totalbilletesbolsa.Location = new System.Drawing.Point(638, 418);
            this.c_totalbilletesbolsa.Name = "c_totalbilletesbolsa";
            this.c_totalbilletesbolsa.ReadOnly = true;
            this.c_totalbilletesbolsa.Size = new System.Drawing.Size(128, 30);
            this.c_totalbilletesbolsa.TabIndex = 105;
            this.c_totalbilletesbolsa.TabStop = false;
            this.c_totalbilletesbolsa.Text = "0";
            this.c_totalbilletesbolsa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.c_totalbilletesbolsa.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(431, 423);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 25);
            this.label11.TabIndex = 104;
            this.label11.Text = "Total Billetes en Bolsa:";
            this.label11.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(410, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(218, 24);
            this.label17.TabIndex = 103;
            this.label17.Text = "Contenido Real en Bolsa";
            // 
            // c_20
            // 
            this.c_20.BackColor = System.Drawing.Color.Transparent;
            this.c_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_20.Location = new System.Drawing.Point(482, 344);
            this.c_20.Name = "c_20";
            this.c_20.Size = new System.Drawing.Size(98, 24);
            this.c_20.TabIndex = 102;
            this.c_20.Text = "0";
            this.c_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_50
            // 
            this.c_50.BackColor = System.Drawing.Color.Transparent;
            this.c_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_50.Location = new System.Drawing.Point(482, 301);
            this.c_50.Name = "c_50";
            this.c_50.Size = new System.Drawing.Size(98, 24);
            this.c_50.TabIndex = 101;
            this.c_50.Text = "0";
            this.c_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_100
            // 
            this.c_100.BackColor = System.Drawing.Color.Transparent;
            this.c_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_100.Location = new System.Drawing.Point(482, 258);
            this.c_100.Name = "c_100";
            this.c_100.Size = new System.Drawing.Size(98, 24);
            this.c_100.TabIndex = 100;
            this.c_100.Text = "0";
            this.c_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_200
            // 
            this.c_200.BackColor = System.Drawing.Color.Transparent;
            this.c_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_200.Location = new System.Drawing.Point(482, 215);
            this.c_200.Name = "c_200";
            this.c_200.Size = new System.Drawing.Size(98, 24);
            this.c_200.TabIndex = 99;
            this.c_200.Text = "0";
            this.c_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_500
            // 
            this.c_500.BackColor = System.Drawing.Color.Transparent;
            this.c_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_500.Location = new System.Drawing.Point(482, 172);
            this.c_500.Name = "c_500";
            this.c_500.Size = new System.Drawing.Size(98, 24);
            this.c_500.TabIndex = 98;
            this.c_500.Text = "0";
            this.c_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_1000
            // 
            this.c_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_1000.Location = new System.Drawing.Point(482, 129);
            this.c_1000.Name = "c_1000";
            this.c_1000.Size = new System.Drawing.Size(98, 24);
            this.c_1000.TabIndex = 97;
            this.c_1000.Text = "0";
            this.c_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(331, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 24);
            this.label10.TabIndex = 96;
            this.label10.Text = "Cantidad Esperada";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(235, 344);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 95;
            this.label8.Text = "$20.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(235, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 24);
            this.label7.TabIndex = 94;
            this.label7.Text = "$50.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(225, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 93;
            this.label6.Text = "$100.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(225, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 92;
            this.label5.Text = "$200.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(225, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 24);
            this.label4.TabIndex = 91;
            this.label4.Text = "$500.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(210, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 90;
            this.label3.Text = "$1,000.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(171, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 24);
            this.label2.TabIndex = 89;
            this.label2.Text = "Denominación";
            // 
            // c_Cajon2_20
            // 
            this.c_Cajon2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_20.Location = new System.Drawing.Point(390, 344);
            this.c_Cajon2_20.Name = "c_Cajon2_20";
            this.c_Cajon2_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_20.TabIndex = 88;
            this.c_Cajon2_20.Text = "0";
            this.c_Cajon2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_50
            // 
            this.c_Cajon2_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_50.Location = new System.Drawing.Point(390, 301);
            this.c_Cajon2_50.Name = "c_Cajon2_50";
            this.c_Cajon2_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_50.TabIndex = 87;
            this.c_Cajon2_50.Text = "0";
            this.c_Cajon2_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_100
            // 
            this.c_Cajon2_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_100.Location = new System.Drawing.Point(390, 258);
            this.c_Cajon2_100.Name = "c_Cajon2_100";
            this.c_Cajon2_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_100.TabIndex = 86;
            this.c_Cajon2_100.Text = "0";
            this.c_Cajon2_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_200
            // 
            this.c_Cajon2_200.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_200.Location = new System.Drawing.Point(390, 215);
            this.c_Cajon2_200.Name = "c_Cajon2_200";
            this.c_Cajon2_200.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_200.TabIndex = 85;
            this.c_Cajon2_200.Text = "0";
            this.c_Cajon2_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_500
            // 
            this.c_Cajon2_500.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_500.Location = new System.Drawing.Point(390, 172);
            this.c_Cajon2_500.Name = "c_Cajon2_500";
            this.c_Cajon2_500.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_500.TabIndex = 84;
            this.c_Cajon2_500.Text = "0";
            this.c_Cajon2_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_1000
            // 
            this.c_Cajon2_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_1000.Location = new System.Drawing.Point(390, 129);
            this.c_Cajon2_1000.Name = "c_Cajon2_1000";
            this.c_Cajon2_1000.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_1000.TabIndex = 83;
            this.c_Cajon2_1000.Text = "0";
            this.c_Cajon2_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(187, 378);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 25);
            this.label9.TabIndex = 82;
            this.label9.Text = "Total Retiro:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(261, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 29);
            this.label1.TabIndex = 81;
            this.label1.Text = "RETIRO DE EFECTIVO";
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonCancelar.Location = new System.Drawing.Point(441, 454);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(205, 48);
            this.c_BotonCancelar.TabIndex = 80;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Cancelar";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(154, 454);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(208, 48);
            this.c_BotonAceptar.TabIndex = 79;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_TotalRetiroEfectivo
            // 
            this.c_TotalRetiroEfectivo.Enabled = false;
            this.c_TotalRetiroEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalRetiroEfectivo.ForeColor = System.Drawing.SystemColors.Info;
            this.c_TotalRetiroEfectivo.Location = new System.Drawing.Point(335, 378);
            this.c_TotalRetiroEfectivo.Name = "c_TotalRetiroEfectivo";
            this.c_TotalRetiroEfectivo.ReadOnly = true;
            this.c_TotalRetiroEfectivo.Size = new System.Drawing.Size(195, 30);
            this.c_TotalRetiroEfectivo.TabIndex = 78;
            this.c_TotalRetiroEfectivo.TabStop = false;
            this.c_TotalRetiroEfectivo.Text = "$ 0.00";
            this.c_TotalRetiroEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FormaRetiroEfectivo
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_totalenDB);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.c_totalbilletesbolsa);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.c_20);
            this.Controls.Add(this.c_50);
            this.Controls.Add(this.c_100);
            this.Controls.Add(this.c_200);
            this.Controls.Add(this.c_500);
            this.Controls.Add(this.c_1000);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Cajon2_20);
            this.Controls.Add(this.c_Cajon2_50);
            this.Controls.Add(this.c_Cajon2_100);
            this.Controls.Add(this.c_Cajon2_200);
            this.Controls.Add(this.c_Cajon2_500);
            this.Controls.Add(this.c_Cajon2_1000);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.Controls.Add(this.c_TotalRetiroEfectivo);
            this.Name = "FormaRetiroEfectivo";
            this.Load += new System.EventHandler(this.FormaRetiroEfectivo_Load);
            this.Controls.SetChildIndex(this.c_TotalRetiroEfectivo, 0);
            this.Controls.SetChildIndex(this.c_BotonAceptar, 0);
            this.Controls.SetChildIndex(this.c_BotonCancelar, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_1000, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_500, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_200, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_100, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_50, 0);
            this.Controls.SetChildIndex(this.c_Cajon2_20, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.c_1000, 0);
            this.Controls.SetChildIndex(this.c_500, 0);
            this.Controls.SetChildIndex(this.c_200, 0);
            this.Controls.SetChildIndex(this.c_100, 0);
            this.Controls.SetChildIndex(this.c_50, 0);
            this.Controls.SetChildIndex(this.c_20, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.c_totalbilletesbolsa, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.c_totalenDB, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_totalenDB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox c_totalbilletesbolsa;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label c_20;
        private System.Windows.Forms.Label c_50;
        private System.Windows.Forms.Label c_100;
        private System.Windows.Forms.Label c_200;
        private System.Windows.Forms.Label c_500;
        private System.Windows.Forms.Label c_1000;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label c_Cajon2_20;
        private System.Windows.Forms.Label c_Cajon2_50;
        private System.Windows.Forms.Label c_Cajon2_100;
        private System.Windows.Forms.Label c_Cajon2_200;
        private System.Windows.Forms.Label c_Cajon2_500;
        private System.Windows.Forms.Label c_Cajon2_1000;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_TotalRetiroEfectivo;
    }
}
