﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    class BDGsi
    {


        public static bool
        Insertar_Mensaje( String p_Fecha ,String p_Idcajero , String p_Codigo , String p_Descripcion ,String p_Ticket, out int p_folio )
        {
            p_folio=FolioMensaje ( );

            String l_Query
                = "INSERT INTO          ColaAlertasGSI "
                + "                     (IdMensaje, Fecha, IdCajero, ClaveFalla, DescFalla, Ticket, Enviado) "
                + "VALUES               (@IdMensaje, @Fecha, @IdCajero, @ClaveFalla, @DescFalla, @Ticket,  0) ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
               
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion);
                l_Comando.Parameters.AddWithValue( "@IdMensaje" , p_folio);
                l_Comando.Parameters.AddWithValue( "@Fecha" , p_Fecha);
                l_Comando.Parameters.AddWithValue( "@IdCajero" ,p_Idcajero);
                l_Comando.Parameters.AddWithValue( "@ClaveFalla" , p_Codigo);
                l_Comando.Parameters.AddWithValue( "@DescFalla" , p_Descripcion  );
                l_Comando.Parameters.AddWithValue( "@Ticket" , p_Ticket );
       
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                {
                  return false;
                } else
                {
                  
                  return true;
                }
            }
        }




        public static DataTable
       ObtenerMensajesGSIPendientes( )
        {
            String l_Query
                = "SELECT               * "
                + "FROM                 ColaAlertasGSI "
                + "WHERE                Enviado = 0 ";


            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );

                DataTable l_Datos = new DataTable( "Mensajes Pendientes" );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }

     




        public  static bool UpdateMensaje( int p_IdMensaje, bool p_exitoso)
        {
            String l_Query
               = "UPDATE           ColaAlertasGSI "
               + "SET              Enviado = @Enviado "
               + "WHERE            IdMensaje = @IdMensaje ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                l_Comando.Parameters.AddWithValue( "@Enviado" , p_exitoso );
                l_Comando.Parameters.AddWithValue( "@IdMensaje" , p_IdMensaje );
                if ( l_Comando.ExecuteNonQuery( ) != 1 )
                    return false;
                else
                    return true;
            }
        }


        private static int
          FolioMensaje( )
        {
            String l_Query
                = "SELECT MAX           (IdMensaje) "
                + "FROM                 ColaAlertasGSI ";
            int l_Folio = 0;

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                    SqlDataReader l_Lector = l_Comando.ExecuteReader( );
                    if ( l_Lector.Read( ) )
                        l_Folio = (int) l_Lector[0];
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Folio Alerta Menssaje" , "Error Consulta : " , ex.Message , 1 );
                }
            }
            l_Folio++;
            return l_Folio;
        }
    }
}
