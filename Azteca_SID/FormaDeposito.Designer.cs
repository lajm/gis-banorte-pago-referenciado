﻿namespace Azteca_SID
{
    partial class FormaDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //  //  base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.c_tope = new System.Windows.Forms.Label();
            this.c_empleado = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Depositar = new System.Windows.Forms.Button();
            this.c_EtiquetaNumeroCuenta = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_TotalDeposito = new System.Windows.Forms.TextBox();
            this.c_BilletesRechazados = new System.Windows.Forms.TextBox();
            this.c_BilletesAceptados = new System.Windows.Forms.TextBox();
            this.c_Suma20 = new System.Windows.Forms.TextBox();
            this.c_Suma50 = new System.Windows.Forms.TextBox();
            this.c_Suma100 = new System.Windows.Forms.TextBox();
            this.c_Suma200 = new System.Windows.Forms.TextBox();
            this.c_Suma500 = new System.Windows.Forms.TextBox();
            this.c_Suma1000 = new System.Windows.Forms.TextBox();
            this.c_Cantidad20 = new System.Windows.Forms.TextBox();
            this.c_Cantidad50 = new System.Windows.Forms.TextBox();
            this.c_Cantidad100 = new System.Windows.Forms.TextBox();
            this.c_Cantidad200 = new System.Windows.Forms.TextBox();
            this.c_Cantidad500 = new System.Windows.Forms.TextBox();
            this.c_Cantidad1000 = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.c_accion = new System.Windows.Forms.PictureBox();
            this.c_enviando = new System.Windows.Forms.Label();
            this.l_ref = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.t_inactividad = new System.Windows.Forms.Timer(this.components);
            this.e_restriccion = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_accion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_tope
            // 
            this.c_tope.AutoSize = true;
            this.c_tope.BackColor = System.Drawing.Color.Transparent;
            this.c_tope.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_tope.ForeColor = System.Drawing.Color.Red;
            this.c_tope.Location = new System.Drawing.Point(513, 61);
            this.c_tope.Name = "c_tope";
            this.c_tope.Size = new System.Drawing.Size(206, 25);
            this.c_tope.TabIndex = 93;
            this.c_tope.Text = "Maximo Permitido:";
            this.c_tope.Visible = false;
            // 
            // c_empleado
            // 
            this.c_empleado.AutoSize = true;
            this.c_empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_empleado.ForeColor = System.Drawing.SystemColors.Highlight;
            this.c_empleado.Location = new System.Drawing.Point(43, 86);
            this.c_empleado.Name = "c_empleado";
            this.c_empleado.Size = new System.Drawing.Size(89, 20);
            this.c_empleado.TabIndex = 92;
            this.c_empleado.Text = "Empleado";
            this.c_empleado.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(172, 20);
            this.label14.TabIndex = 91;
            this.label14.Text = "Nombre de CUENTA";
            this.label14.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point(584, 371);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(132, 21);
            this.label13.TabIndex = 90;
            this.label13.Text = "Total a Depositar";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(625, 196);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 21);
            this.label12.TabIndex = 89;
            this.label12.Text = "Suma";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Location = new System.Drawing.Point(487, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 21);
            this.label11.TabIndex = 88;
            this.label11.Text = "Cantidad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(389, 335);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 87;
            this.label8.Text = "$20.00";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Black;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(389, 312);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "$50.00";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(383, 289);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "$100.00";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(383, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 84;
            this.label5.Text = "$200.00";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(383, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 83;
            this.label4.Text = "$500.00";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Black;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(374, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 82;
            this.label9.Text = "$1,000.00";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label10.Location = new System.Drawing.Point(28, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(318, 60);
            this.label10.TabIndex = 81;
            this.label10.Text = "Coloque el efectivo en la bandeja de conteo";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(371, 404);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 16);
            this.label3.TabIndex = 80;
            this.label3.Text = "Billetes Rechazados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(374, 362);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 16);
            this.label2.TabIndex = 79;
            this.label2.Text = "Billetes Aceptados";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Gotham Bold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(42, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 28);
            this.label1.TabIndex = 78;
            this.label1.Text = "INGRESE EL EFECTIVO";
            // 
            // c_Depositar
            // 
            this.c_Depositar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.c_Depositar.BackColor = System.Drawing.Color.Transparent;
            this.c_Depositar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Depositar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c_Depositar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Depositar.ForeColor = System.Drawing.Color.Transparent;
            this.c_Depositar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_depositar;
            this.c_Depositar.Location = new System.Drawing.Point(335, 525);
            this.c_Depositar.Name = "c_Depositar";
            this.c_Depositar.Size = new System.Drawing.Size(130, 48);
            this.c_Depositar.TabIndex = 77;
            this.c_Depositar.TabStop = false;
            this.c_Depositar.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_Depositar.UseVisualStyleBackColor = true;
            this.c_Depositar.EnabledChanged += new System.EventHandler(this.c_Depositar_EnabledChanged);
            this.c_Depositar.Click += new System.EventHandler(this.c_Depositar_Click);
            // 
            // c_EtiquetaNumeroCuenta
            // 
            this.c_EtiquetaNumeroCuenta.AutoSize = true;
            this.c_EtiquetaNumeroCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaNumeroCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaNumeroCuenta.Location = new System.Drawing.Point(12, 106);
            this.c_EtiquetaNumeroCuenta.Name = "c_EtiquetaNumeroCuenta";
            this.c_EtiquetaNumeroCuenta.Size = new System.Drawing.Size(135, 20);
            this.c_EtiquetaNumeroCuenta.TabIndex = 76;
            this.c_EtiquetaNumeroCuenta.Text = "00000000000001";
            this.c_EtiquetaNumeroCuenta.Visible = false;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.c_BotonAceptar.BackColor = System.Drawing.Color.Transparent;
            this.c_BotonAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BotonAceptar.Enabled = false;
            this.c_BotonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ForeColor = System.Drawing.Color.Transparent;
            this.c_BotonAceptar.Image = global::Azteca_SID.Properties.Resources.Terminar_Transaccion;
            this.c_BotonAceptar.Location = new System.Drawing.Point(518, 525);
            this.c_BotonAceptar.Margin = new System.Windows.Forms.Padding(0);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(256, 48);
            this.c_BotonAceptar.TabIndex = 75;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_TotalDeposito
            // 
            this.c_TotalDeposito.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_TotalDeposito.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_TotalDeposito.Font = new System.Drawing.Font("Roboto Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalDeposito.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_TotalDeposito.Location = new System.Drawing.Point(579, 404);
            this.c_TotalDeposito.Name = "c_TotalDeposito";
            this.c_TotalDeposito.ReadOnly = true;
            this.c_TotalDeposito.Size = new System.Drawing.Size(157, 26);
            this.c_TotalDeposito.TabIndex = 74;
            this.c_TotalDeposito.TabStop = false;
            this.c_TotalDeposito.Text = "$0.00";
            this.c_TotalDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_BilletesRechazados
            // 
            this.c_BilletesRechazados.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_BilletesRechazados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesRechazados.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesRechazados.Location = new System.Drawing.Point(377, 418);
            this.c_BilletesRechazados.Name = "c_BilletesRechazados";
            this.c_BilletesRechazados.ReadOnly = true;
            this.c_BilletesRechazados.Size = new System.Drawing.Size(86, 20);
            this.c_BilletesRechazados.TabIndex = 73;
            this.c_BilletesRechazados.TabStop = false;
            this.c_BilletesRechazados.Text = "No";
            // 
            // c_BilletesAceptados
            // 
            this.c_BilletesAceptados.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_BilletesAceptados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesAceptados.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesAceptados.Location = new System.Drawing.Point(377, 381);
            this.c_BilletesAceptados.Name = "c_BilletesAceptados";
            this.c_BilletesAceptados.ReadOnly = true;
            this.c_BilletesAceptados.Size = new System.Drawing.Size(79, 20);
            this.c_BilletesAceptados.TabIndex = 72;
            this.c_BilletesAceptados.TabStop = false;
            this.c_BilletesAceptados.Text = "0";
            // 
            // c_Suma20
            // 
            this.c_Suma20.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma20.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma20.Location = new System.Drawing.Point(566, 335);
            this.c_Suma20.Name = "c_Suma20";
            this.c_Suma20.ReadOnly = true;
            this.c_Suma20.Size = new System.Drawing.Size(170, 20);
            this.c_Suma20.TabIndex = 71;
            this.c_Suma20.TabStop = false;
            this.c_Suma20.Text = "$0.00";
            this.c_Suma20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma50
            // 
            this.c_Suma50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma50.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma50.Location = new System.Drawing.Point(566, 312);
            this.c_Suma50.Name = "c_Suma50";
            this.c_Suma50.ReadOnly = true;
            this.c_Suma50.Size = new System.Drawing.Size(170, 20);
            this.c_Suma50.TabIndex = 70;
            this.c_Suma50.TabStop = false;
            this.c_Suma50.Text = "$0.00";
            this.c_Suma50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma100
            // 
            this.c_Suma100.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma100.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma100.Location = new System.Drawing.Point(566, 289);
            this.c_Suma100.Name = "c_Suma100";
            this.c_Suma100.ReadOnly = true;
            this.c_Suma100.Size = new System.Drawing.Size(170, 20);
            this.c_Suma100.TabIndex = 69;
            this.c_Suma100.TabStop = false;
            this.c_Suma100.Text = "$0.00";
            this.c_Suma100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma200
            // 
            this.c_Suma200.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma200.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma200.Location = new System.Drawing.Point(566, 266);
            this.c_Suma200.Name = "c_Suma200";
            this.c_Suma200.ReadOnly = true;
            this.c_Suma200.Size = new System.Drawing.Size(170, 20);
            this.c_Suma200.TabIndex = 68;
            this.c_Suma200.TabStop = false;
            this.c_Suma200.Text = "$0.00";
            this.c_Suma200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma500
            // 
            this.c_Suma500.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma500.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma500.Location = new System.Drawing.Point(566, 243);
            this.c_Suma500.Name = "c_Suma500";
            this.c_Suma500.ReadOnly = true;
            this.c_Suma500.Size = new System.Drawing.Size(170, 20);
            this.c_Suma500.TabIndex = 67;
            this.c_Suma500.TabStop = false;
            this.c_Suma500.Text = "$0.00";
            this.c_Suma500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma1000
            // 
            this.c_Suma1000.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma1000.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma1000.Location = new System.Drawing.Point(566, 220);
            this.c_Suma1000.Name = "c_Suma1000";
            this.c_Suma1000.ReadOnly = true;
            this.c_Suma1000.Size = new System.Drawing.Size(170, 20);
            this.c_Suma1000.TabIndex = 66;
            this.c_Suma1000.TabStop = false;
            this.c_Suma1000.Text = "$0.00";
            this.c_Suma1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad20
            // 
            this.c_Cantidad20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad20.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad20.Location = new System.Drawing.Point(489, 335);
            this.c_Cantidad20.Name = "c_Cantidad20";
            this.c_Cantidad20.ReadOnly = true;
            this.c_Cantidad20.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad20.TabIndex = 65;
            this.c_Cantidad20.TabStop = false;
            this.c_Cantidad20.Text = "0";
            this.c_Cantidad20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad50
            // 
            this.c_Cantidad50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad50.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad50.Location = new System.Drawing.Point(489, 312);
            this.c_Cantidad50.Name = "c_Cantidad50";
            this.c_Cantidad50.ReadOnly = true;
            this.c_Cantidad50.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad50.TabIndex = 64;
            this.c_Cantidad50.TabStop = false;
            this.c_Cantidad50.Text = "0";
            this.c_Cantidad50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad100
            // 
            this.c_Cantidad100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad100.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad100.Location = new System.Drawing.Point(489, 289);
            this.c_Cantidad100.Name = "c_Cantidad100";
            this.c_Cantidad100.ReadOnly = true;
            this.c_Cantidad100.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad100.TabIndex = 63;
            this.c_Cantidad100.TabStop = false;
            this.c_Cantidad100.Text = "0";
            this.c_Cantidad100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad200
            // 
            this.c_Cantidad200.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad200.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad200.Location = new System.Drawing.Point(489, 266);
            this.c_Cantidad200.Name = "c_Cantidad200";
            this.c_Cantidad200.ReadOnly = true;
            this.c_Cantidad200.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad200.TabIndex = 62;
            this.c_Cantidad200.TabStop = false;
            this.c_Cantidad200.Text = "0";
            this.c_Cantidad200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad500
            // 
            this.c_Cantidad500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad500.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad500.Location = new System.Drawing.Point(489, 243);
            this.c_Cantidad500.Name = "c_Cantidad500";
            this.c_Cantidad500.ReadOnly = true;
            this.c_Cantidad500.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad500.TabIndex = 61;
            this.c_Cantidad500.TabStop = false;
            this.c_Cantidad500.Text = "0";
            this.c_Cantidad500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad1000
            // 
            this.c_Cantidad1000.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad1000.Font = new System.Drawing.Font("Roboto", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad1000.Location = new System.Drawing.Point(489, 220);
            this.c_Cantidad1000.Name = "c_Cantidad1000";
            this.c_Cantidad1000.ReadOnly = true;
            this.c_Cantidad1000.Size = new System.Drawing.Size(71, 20);
            this.c_Cantidad1000.TabIndex = 60;
            this.c_Cantidad1000.TabStop = false;
            this.c_Cantidad1000.Text = "0";
            this.c_Cantidad1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.btn_banorte_depositar;
            this.pictureBox3.Location = new System.Drawing.Point(56, 525);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(128, 48);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 94;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.c_Depositar_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Azteca_SID.Properties.Resources.pesos;
            this.pictureBox4.Location = new System.Drawing.Point(628, 134);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(113, 40);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 95;
            this.pictureBox4.TabStop = false;
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_cuenta.Enabled = false;
            this.c_cuenta.Font = new System.Drawing.Font("Roboto", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.Location = new System.Drawing.Point(371, 135);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(258, 39);
            this.c_cuenta.TabIndex = 96;
            this.c_cuenta.Text = "******XXXX";
            // 
            // c_accion
            // 
            this.c_accion.BackColor = System.Drawing.Color.Transparent;
            this.c_accion.Image = global::Azteca_SID.Properties.Resources.bills;
            this.c_accion.Location = new System.Drawing.Point(89, 239);
            this.c_accion.Name = "c_accion";
            this.c_accion.Size = new System.Drawing.Size(192, 148);
            this.c_accion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_accion.TabIndex = 97;
            this.c_accion.TabStop = false;
            // 
            // c_enviando
            // 
            this.c_enviando.BackColor = System.Drawing.Color.Transparent;
            this.c_enviando.Font = new System.Drawing.Font("Gotham Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_enviando.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_enviando.Location = new System.Drawing.Point(0, 397);
            this.c_enviando.Name = "c_enviando";
            this.c_enviando.Size = new System.Drawing.Size(346, 61);
            this.c_enviando.TabIndex = 99;
            this.c_enviando.Text = "Un momento por favor, estamos procesando su Depósito";
            this.c_enviando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.c_enviando.Visible = false;
            // 
            // l_ref
            // 
            this.l_ref.AutoSize = true;
            this.l_ref.BackColor = System.Drawing.Color.Transparent;
            this.l_ref.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_ref.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.l_ref.Location = new System.Drawing.Point(368, 176);
            this.l_ref.Name = "l_ref";
            this.l_ref.Size = new System.Drawing.Size(95, 21);
            this.l_ref.TabIndex = 153;
            this.l_ref.Text = "Referencia: ";
            this.l_ref.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label15.Location = new System.Drawing.Point(4, 459);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(370, 46);
            this.label15.TabIndex = 154;
            this.label15.Text = "Para Terminar su Depósito  de click en TERMINAR TRANSACCIÓN y no olvide tomar su " +
    "Comprobante";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label16.Location = new System.Drawing.Point(415, 86);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(326, 46);
            this.label16.TabIndex = 155;
            this.label16.Text = "Verifique si los datos a los que se realizará el Depósito seran los correctos";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // t_inactividad
            // 
            this.t_inactividad.Interval = 60000;
            this.t_inactividad.Tick += new System.EventHandler(this.t_inactividad_Tick);
            // 
            // e_restriccion
            // 
            this.e_restriccion.AutoSize = true;
            this.e_restriccion.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_restriccion.ForeColor = System.Drawing.Color.Red;
            this.e_restriccion.Location = new System.Drawing.Point(574, 459);
            this.e_restriccion.Name = "e_restriccion";
            this.e_restriccion.Size = new System.Drawing.Size(76, 25);
            this.e_restriccion.TabIndex = 157;
            this.e_restriccion.Text = "Límite: ";
            this.e_restriccion.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Azteca_SID.Properties.Resources.LOGOGSI;
            this.pictureBox1.Location = new System.Drawing.Point(21, 537);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 158;
            this.pictureBox1.TabStop = false;
            // 
            // FormaDeposito
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte_dep;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.e_restriccion);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.l_ref);
            this.Controls.Add(this.c_enviando);
            this.Controls.Add(this.c_accion);
            this.Controls.Add(this.c_cuenta);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.c_BotonAceptar);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.c_tope);
            this.Controls.Add(this.c_empleado);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Depositar);
            this.Controls.Add(this.c_EtiquetaNumeroCuenta);
            this.Controls.Add(this.c_TotalDeposito);
            this.Controls.Add(this.c_BilletesRechazados);
            this.Controls.Add(this.c_BilletesAceptados);
            this.Controls.Add(this.c_Suma20);
            this.Controls.Add(this.c_Suma50);
            this.Controls.Add(this.c_Suma100);
            this.Controls.Add(this.c_Suma200);
            this.Controls.Add(this.c_Suma500);
            this.Controls.Add(this.c_Suma1000);
            this.Controls.Add(this.c_Cantidad20);
            this.Controls.Add(this.c_Cantidad50);
            this.Controls.Add(this.c_Cantidad100);
            this.Controls.Add(this.c_Cantidad200);
            this.Controls.Add(this.c_Cantidad500);
            this.Controls.Add(this.c_Cantidad1000);
            this.Controls.Add(this.label16);
            this.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormaDeposito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormaDeposito_FormClosing);
            this.Load += new System.EventHandler(this.FormaDeposito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_accion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label c_tope;
        private System.Windows.Forms.Label c_empleado;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Depositar;
        private System.Windows.Forms.Label c_EtiquetaNumeroCuenta;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_TotalDeposito;
        private System.Windows.Forms.TextBox c_BilletesRechazados;
        private System.Windows.Forms.TextBox c_BilletesAceptados;
        private System.Windows.Forms.TextBox c_Suma20;
        private System.Windows.Forms.TextBox c_Suma50;
        private System.Windows.Forms.TextBox c_Suma100;
        private System.Windows.Forms.TextBox c_Suma200;
        private System.Windows.Forms.TextBox c_Suma500;
        private System.Windows.Forms.TextBox c_Suma1000;
        private System.Windows.Forms.TextBox c_Cantidad20;
        private System.Windows.Forms.TextBox c_Cantidad50;
        private System.Windows.Forms.TextBox c_Cantidad100;
        private System.Windows.Forms.TextBox c_Cantidad200;
        private System.Windows.Forms.TextBox c_Cantidad500;
        private System.Windows.Forms.TextBox c_Cantidad1000;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.PictureBox c_accion;
        private System.Windows.Forms.Label c_enviando;
        private System.Windows.Forms.Label l_ref;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Timer t_inactividad;
        private System.Windows.Forms.Label e_restriccion;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
