﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaConfirmarUsuario : FormBase
    {
        public FormaConfirmarUsuario()
        {
            InitializeComponent();
            c_Empleado.Text = Globales.IdUsuario;
            c_nombreUsuario.Text = Globales.NombreUsuario.ToUpper();
            c_nombreUsuario.Location = new System.Drawing.Point(400 - c_nombreUsuario.Size.Width / 2, 233);

            Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO", 1);
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            c_BotonAceptar.Enabled = false;

            GC.Collect();

            if (Properties.Settings.Default.No_Debug_Pc)
            {

                int l_Reintentos = 0;
                try
                {
                    do
                    {
                        if (l_Reintentos >= 1) //mayor que cero para cambiar BIN
                            break;
                        l_Reply = SidLib.ResetError();
                        l_Reintentos++;
                    } while (l_Reply != 0);
                }
                catch (Exception ex)
                {
                }
            }
            FormaPrincipal.s_Forma.Gsi_Cronometro( false );

            if ( Properties.Settings.Default.Port_YUGO > 0 )
            {
                DialogResult l_R;
                using ( FormaDepositoMixto2 f_DepositoMixto = new FormaDepositoMixto2( ) )
                {

                    l_R  = f_DepositoMixto.ShowDialog( );


                }
                if (l_R == DialogResult.OK)
                {
                    using (FormFINdeposito l_F = new FormFINdeposito(true))
                    {
                        l_F.ShowDialog();
                    }
                }




            }
            else
            {
                DialogResult l_R;
                using ( FormaDeposito f_Deposito = new FormaDeposito( ) )
                {

                    l_R  = f_Deposito.ShowDialog( );


                }
                if (l_R == DialogResult.OK)
                {
                    using (FormFINdeposito l_F = new FormFINdeposito(true))
                    {
                        l_F.ShowDialog();
                    }
                }

            }
          //  FormaPrincipal.s_Forma.Gsi_Cronometro( true );
            Close();
        }
    }
}
