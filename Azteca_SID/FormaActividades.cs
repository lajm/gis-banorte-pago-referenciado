﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaActividades : Azteca_SID.FormBase
    {
        public FormaActividades()
        {
            InitializeComponent();
        }

        enum Perfil
        { Operador = 1,
            Etv,
            Supervisor,
            Mantenimiento,
            Sucursal
        };

        Globales.EstatusReceptor l_auxiliar;
        private void FormaActividades_Load(object sender, EventArgs e)
        {

            if (Globales.IdTipoUsuario == (int)Perfil.Mantenimiento)
            {
                 l_auxiliar = Globales.Estatus;

                //Globales.CambioEstatusReceptor(Globales.EstatusReceptor.Mantenimiento);
                //if (Properties.Settings.Default.Enviar_AlertasGSI)
                //    UtilsComunicacion.KeepaliveGSI((int)Globales.Estatus);


                OPERACIONES.TabPages.Remove( Admin );
                OPERACIONES.TabPages.Remove( Etv );

                //Sistema
                c_OpcionCreaLayout.Visible = false;
                c_numeroBolsa.Visible = false;

                e_Pefil.Visible = true;

            }

           else if (Globales.IdTipoUsuario == (int ) Perfil.Supervisor)
            {
                OPERACIONES.TabPages.Remove(Etv);

                            
            }else if ( Globales.IdTipoUsuario == (int) Perfil.Sucursal )
            {
                OPERACIONES.TabPages.Remove( Etv );
                OPERACIONES.TabPages.Remove( Mantenimiento );

                //Administracion
                c_OpcionConsultaEfectivo.Visible = false;
                c_administrarUsuarios.Visible = false;
                c_contabilidad.Visible = false;

                //Sistema
                c_OpcionCreaLayout.Visible = false;
                c_numeroBolsa.Visible = false;
                c_CerrarSistema.Visible = false;

                e_Pefil.Text = "MODO SUCURSAL";
                e_Pefil.Visible = true;
            }
            else
            {
                OPERACIONES.TabPages.Remove(Admin);
                OPERACIONES.TabPages.Remove(Sistema);
                OPERACIONES.TabPages.Remove(Mantenimiento);

            }
        }


        private void c_OpcionConsultaEfectivo_Click(object sender, EventArgs e)
        {
            using (FormaConsultaEfectivo f_ConsultaPesos = new FormaConsultaEfectivo())
            {
                f_ConsultaPesos.ShowDialog();
            }
        }

        private void c_OpcionConsultaOperacion_Click(object sender, EventArgs e)
        {
            using (FormaConsultaOperaciones f_Operaciones = new FormaConsultaOperaciones())
            {
                f_Operaciones.ShowDialog();
                //  Close();
            }
        }

        private void c_contabilidad_Click(object sender, EventArgs e)
        {
            using (FormaError f_Mensaje = new FormaError(true,true,false,"pregunta"))
            {
                f_Mensaje.c_Imagen.Visible = false;
                f_Mensaje.c_MensajeError.Text =
                            " SE GENERARA UNA ALERTA PARA EL RESET DE CONTABILIDAD ¿Desea Continuar?";
                DialogResult l_respuesta = f_Mensaje.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                {
                    using (FormaError f_MensajeOK = new FormaError(true, true, false, "pregunta"))
                    {
                        f_MensajeOK.c_Imagen.Visible = false;
                        f_MensajeOK.c_MensajeError.Text =
                                    " CONFIRME EL RESET DE CONTABILIDAD ¡No podra deshacer esta Operación!....";
                        DialogResult l_respuestaOK = f_MensajeOK.ShowDialog();

                        if (l_respuestaOK == DialogResult.OK)
                        {
                            DialogResult l_resultado;

                            using ( FormaAutenticar v_autenticar = new FormaAutenticar("Contabilidad") )
                            {
                                l_resultado = v_autenticar.ShowDialog( );
                            }

                            if ( l_resultado == DialogResult.OK )
                            {

                                BDRetiro.ActualizarRetiros( );
                                using ( FormaError f_OK = new FormaError( true , "exito" ) )
                                {
                                    f_OK.c_Imagen.Visible = true;
                                    f_OK.c_MensajeError.Text = "Contabilidad en 0s";
                                    f_OK.ShowDialog( );
                                }
                            }else
                                Globales.EscribirBitacora( "OPCION ADMINISTRADOR" , "PASSWORD DE CONTABILIDAD Erroneo" , l_respuestaOK.ToString( ) , 1 );
                        }
                        Globales.EscribirBitacora("OPCION ADMINISTRADOR", "Confirmación RESET CONTABILIDAD", l_respuestaOK.ToString(), 1);
                    }
                }

                Globales.EscribirBitacora("OPCION ADMINISTRADOR", "RESET CONTABILIDAD", l_respuesta.ToString(), 1);

            }
        }

        private void c_Reset_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.No_Debug_Pc)
            {
                c_Reset.Enabled = false;


                Error_Sid( SidApi.SidLib.ResetPath( ) , out l_Error );

                Error_Sid( SidApi.SidLib.ResetPath( ) , out l_Error );

                c_Reset.Enabled = true;
            }
        }

        private void c_numeroBolsa_Click(object sender, EventArgs e)
        {
            using (FormaCambioNumeroBolsa f_cambiobolsa = new FormaCambioNumeroBolsa())
            {
                f_cambiobolsa.TopMost = true;
                f_cambiobolsa.ShowDialog();

            }
        }

        private void c_OpcionCreaLayout_Click(object sender, EventArgs e)
        {
            //if (Registrar. MandarBitacoraActual())
            //{
            //    using (FormaError f_Error = new FormaError(true,"exito"))
            //    {
            //        f_Error.c_MensajeError.Text = "Bitacora Mandada con Exito";
            //        f_Error.ShowDialog();

            //    }

            //    Globales.EscribirBitacora("Enviar Bitacora Actual", "MandarBitacoraActual", "Bitacora Mandada con Exito", 1);
            //}
            //else
            //{
            //    using (FormaError f_Error = new FormaError(true, "problemas"))
            //    {
            //        f_Error.c_MensajeError.Text = "Error Enviando Bitacora...";
            //        f_Error.ShowDialog();

            //    }
            //    Globales.EscribirBitacora("Enviar Bitacora Actual", "MandarBitacoraActual", "Erro en la creación, o en el envio de Bitacora", 1);
            //}

            //if (Properties.Settings.Default.Port_YUGO >0 )
            using (FormaConsolaYugo v_consola = new FormaConsolaYugo())
            {
                v_consola.ShowDialog();
            }
        }

        private void c_CerrarSistema_Click(object sender, EventArgs e)
        {

            BDSesion.CerrarSesion(Globales.Numero_Sesion, out l_Error);


            if (Properties.Settings.Default.Enviar_AlertasGSI)
                UtilsComunicacion.AlertaGSI( 115 , " Cierre de Aplicativo desde Consola" );

            if (Globales.Estatus != Globales.EstatusReceptor.No_Operable)
            Globales.CambioEstatusReceptor(Globales.EstatusReceptor.Apagando);

            Registrar.Alerta("Apagando Equipo desde SISTEMA");
            Application.Exit();
        }

        private void c_OpcionRetiroETV_Click(object sender, EventArgs e)
        {
            FormaPrincipal.s_Forma.Gsi_Cronometro( false );
            using (FormaRetiroEfectivo f_Retiro = new FormaRetiroEfectivo())
            {
                f_Retiro.ShowDialog();
                Close();
            }
            FormaPrincipal.s_Forma.Gsi_Cronometro( true );
        }

        private void c_AperturaForzada_Click(object sender, EventArgs e)
        {
            FormaPrincipal.s_Forma.Gsi_Cronometro( false );
            int l_Reply = 0;
            String l_Error;


            using ( FormaError f_Mensaje = new FormaError( true , true , false , "warning" ) )
            {
                f_Mensaje.c_Imagen.Visible = false;
                f_Mensaje.c_MensajeError.Text =
                            " SE GENERARA UNA ALERTA DE APERTURA DE BOVEDAD ¿Desea Continuar? ";



                DialogResult l_respuesta = f_Mensaje.ShowDialog( );

                if ( l_respuesta == DialogResult.OK )
                {
                    DialogResult l_resultado;
                    int l_aux = Globales.IdTipoUsuario;
                    using ( FormaAutenticar v_autenticar = new FormaAutenticar( "Seguridad" ) )
                    {
                        l_resultado = v_autenticar.ShowDialog( );
                    }

                    if ( l_resultado == DialogResult.OK )
                    {


                        Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , " Intento de APertura Forzada codigo Error: 200 " , Globales.IdUsuario , 1 );

                        if ( Globales.Estatus == Globales.EstatusReceptor.Operable )
                            UtilsComunicacion.AlertaGSI( 200 , "SID_Intento_AperTura_Forzada" );

                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );

                        Registrar.Alerta( "INTENTO DE APERTURA FORZADA DE BOVEDAD POR ETV: " + DateTime.Now.ToLongDateString( ) + DateTime.Now.ToLongTimeString( ) );
                        if ( Properties.Settings.Default.No_Debug_Pc )
                        {
                            Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , " INICIAR" , Globales.IdUsuario , 1 );
                            l_Reply = SidApi.SidLib.SID_Open( true );
                            if ( l_Reply == SidApi.SidLib.SID_OKAY || l_Reply == SidApi.SidLib.SID_ALREADY_OPEN )
                            {


                                MostarMensaje( "Presione Aceptar, Introduzca el Codigo y Gire, tiene 30 segundos" , true , "alerta" );
                                Forzardibujado( );
                                l_Reply = SidApi.SidLib.SID_CheckCodeLockCorrect( 30 );


                                if ( l_Reply == SidApi.SidLib.SID_OKAY )
                                {
                                    Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , " CODIGO CORRECTO " , Globales.IdUsuario , 1 );
                                    l_Reply = SidApi.SidLib.SID_ForceOpenDoor( );
                                    if ( l_Reply == SidApi.SidLib.SID_OKAY )
                                    {
                                        int count = 0;
                                        do
                                        {
                                            Application.DoEvents( );
                                            System.Threading.Thread.Sleep( 500 );
                                            if ( SensarSIDcs.IsDoorOpen( ) )
                                            {



                                                if ( !Globales.Alarmado )
                                                {

                                                    UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );
                                                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );

                                                    UtilsComunicacion.AlertaGSI( 201 , "SID_AperTura_Forzada_Exitosa" );
                                                  
                                                    
                                                }


                                                Registrar.Alerta( "APERTURA FORZADA DE BOVEDAD POR ETV REALIZADA CON EXITO, El Equipo esta abierto : " + DateTime.Now.ToLongDateString( ) + DateTime.Now.ToLongTimeString( ) );

                                                Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "EXITOSA" , Globales.IdUsuario , 1 );

                                                MostarMensaje( "....Exito al abrir la puerta..." , true , "exito" );
                                                break;
                                            }
                                            else
                                                Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "Esperando Apertura de Boveda" , Globales.IdUsuario , 1 );

                                            count++;
                                        } while ( count < 30 );

                                        if ( SensarSIDcs.IsLockOpen( ) )
                                        {
                                            if ( !Globales.Alarmado )
                                            {
                                                UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );
                                                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                                            }
                                            Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "Combinación de DIAL Abierto " , " ------" , 1 );



                                        }

                                    }
                                    else
                                    {

                                        // Error_Sid( l_Reply , out l_Error );
                                        Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "NO REALIZADA:: " , "Error SID_ForceOPEN()" , 1 );

                                        MostarMensaje( "Intentelo mas tarde posible razón: " + "SID_ForceOPEN( )" , true , "error" );


                                    }
                                }
                                else
                                {


                                    if ( !Globales.Alarmado )
                                    {
                                        UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );
                                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                                    }



                                    Error_Sid( l_Reply , out l_Error );
                                    Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "Combinación de DIAL Invalido o Tiempo Agotado: " , l_Error , 1 );
                                    using ( FormaError f_Mensaje2 = new FormaError( true , "Error" ) )
                                    {
                                        f_Mensaje2.c_Imagen.Visible = false;
                                        f_Mensaje2.c_MensajeError.Text =
                                          " Combinación de DIAL Invalido o Tiempo Agotado.. Reintente: Error = " + l_Error;
                                        f_Mensaje2.ShowDialog( );
                                    }

                                }

                                Application.DoEvents( );

                                if ( !Globales.Alarmado )
                                    using ( FormaFueraServicio l_check = new FormaFueraServicio( ) )
                                    {

                                        l_check.ShowDialog( );
                                    }

                            }
                            else
                            {

                                Error_Sid( l_Reply , out l_Error );
                                Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "NO REALIZADA:: " , l_Error , 1 );
                                using ( FormaError f_Mensaje2 = new FormaError( true , "fuera de linea" ) )
                                {
                                    f_Mensaje2.c_Imagen.Visible = false;
                                    f_Mensaje2.c_MensajeError.Text =
                                      " NO POSIBLE POR EL MOMENTO " + l_Error;
                                }
                            }
                            l_Reply = SidApi.SidLib.SID_Close( );
                        }
                    }else
                        Globales.EscribirBitacora( "APERTURA DE BOVEDA FORZADA" , "Pasword de Seguridad:  " ,"ERRONEO" , 1 );
                }
            }

            FormaPrincipal.s_Forma.Gsi_Cronometro( true );
        }

        private void c_reset_retiro_Click(object sender, EventArgs e)
        {
            
            if (Properties.Settings.Default.No_Debug_Pc)
            {
                c_Reset.Enabled = false;


                Error_Sid( SidApi.SidLib.ResetPath( ) ,out l_Error );

                Error_Sid( SidApi.SidLib.ResetPath( ) , out l_Error );

                c_Reset.Enabled = true;
            }
        }

        private void c_BotonSalir_Click(object sender, EventArgs e)
        {
            //if (Globales.IdTipoUsuario == 4)
            //{

            //    Globales.CambioEstatusReceptor(l_auxiliar);

            //    if (Properties.Settings.Default.Enviar_AlertasGSI)
            //        UtilsComunicacion.KeepaliveGSI((int)Globales.Estatus);
            //}
            Close();
        }

        private void c_administrarUsuarios_Click(object sender, EventArgs e)
        {
            using (FormAdministarUsuarios f_administrar = new FormAdministarUsuarios())
            {

                f_administrar.ShowDialog();

            }
        }

         public void Error_Sid( int l_Reply , out String p_Error )
        {
            String l_Error = "";

            SidLib.Error_Sid( l_Reply , out l_Error );
            p_Error = l_Error;

            if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply != 0 && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
            {
                UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
            }

            if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable)
            {
                UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable);
            }
        }

        private void c_registro_Click(object sender, EventArgs e)
        {
            //Lanzar registro de Mantenimiento
            using (FormaAltaMantenimiento l_alta = new FormaAltaMantenimiento())
            {
                l_alta.ShowDialog();
            }



            
        }

        private void c_ConsultaMante_Click(object sender, EventArgs e)
        {
            //Mostar Tabla de Mantenimientos
            using (FormaConsultaMantenimientos l_mantenimientos = new FormaConsultaMantenimientos())
            {
                l_mantenimientos.ShowDialog();
            }
        }

        private void c_registroSalida_Click(object sender, EventArgs e)
        {
            //Lanzar Termino de Mantenimiento consulta parcial
            using (FormaCierreMantenimiento l_cerrar = new FormaCierreMantenimiento())
            {
                l_cerrar.ShowDialog();
            }

        }

        private void c_ticket_Click(object sender, EventArgs e)
        {
            // Comprobacion de cajero funcional

            using (FormaFueraServicio l_check = new  FormaFueraServicio(false,false))
            {
                l_check.ShowDialog();

            }

        }

        private void FormaActividades_FormClosing( object sender , FormClosingEventArgs e )
        {
            Globales.EscribirBitacora( "FormaActividades()" , "Cierre de Actividades: " ,"Logout ETV/ADMINISTRADOR/SUCURSAL" , 3 );
        }
    }
}
