﻿namespace Azteca_SID
{
    partial class FormaConfirmarUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_nombreUsuario = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Empleado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_EtiquetaMoneda = new System.Windows.Forms.Label();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c_nombreUsuario
            // 
            this.c_nombreUsuario.AutoSize = true;
            this.c_nombreUsuario.BackColor = System.Drawing.Color.Transparent;
            this.c_nombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nombreUsuario.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.c_nombreUsuario.Location = new System.Drawing.Point(36, 233);
            this.c_nombreUsuario.Name = "c_nombreUsuario";
            this.c_nombreUsuario.Size = new System.Drawing.Size(722, 42);
            this.c_nombreUsuario.TabIndex = 67;
            this.c_nombreUsuario.Text = "################################";
            this.c_nombreUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(275, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(235, 24);
            this.label6.TabIndex = 66;
            this.label6.Text = "NOMBRE DE LA CUENTA";
            // 
            // c_Empleado
            // 
            this.c_Empleado.AutoSize = true;
            this.c_Empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_Empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Empleado.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.c_Empleado.Location = new System.Drawing.Point(322, 160);
            this.c_Empleado.Name = "c_Empleado";
            this.c_Empleado.Size = new System.Drawing.Size(150, 42);
            this.c_Empleado.TabIndex = 65;
            this.c_Empleado.Text = "######";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(280, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 24);
            this.label4.TabIndex = 64;
            this.label4.Text = "NUMERO DE CUENTA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DeepPink;
            this.label3.Location = new System.Drawing.Point(239, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(323, 29);
            this.label3.TabIndex = 63;
            this.label3.Text = "DEPÓSITO DE EFECTIVO";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(9, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(776, 102);
            this.label2.TabIndex = 62;
            this.label2.Text = "Verifique su numero de CUENTA  para que  los depósito sean correctos y oprima con" +
    "tinuar";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(182, 310);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 61;
            this.label1.Text = "Moneda";
            // 
            // c_EtiquetaMoneda
            // 
            this.c_EtiquetaMoneda.AutoSize = true;
            this.c_EtiquetaMoneda.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaMoneda.Location = new System.Drawing.Point(333, 310);
            this.c_EtiquetaMoneda.Name = "c_EtiquetaMoneda";
            this.c_EtiquetaMoneda.Size = new System.Drawing.Size(128, 42);
            this.c_EtiquetaMoneda.TabIndex = 60;
            this.c_EtiquetaMoneda.Text = "Pesos";
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonCancelar.Location = new System.Drawing.Point(419, 439);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(257, 53);
            this.c_BotonCancelar.TabIndex = 59;
            this.c_BotonCancelar.Text = "CANCELAR";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(126, 439);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(257, 53);
            this.c_BotonAceptar.TabIndex = 58;
            this.c_BotonAceptar.Text = "CONTINUAR";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // FormaConfirmarUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_nombreUsuario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Empleado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_EtiquetaMoneda);
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.Controls.Add(this.label2);
            this.Name = "FormaConfirmarUsuario";
            this.Text = "FormaConfirmarUsuario";
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.c_BotonAceptar, 0);
            this.Controls.SetChildIndex(this.c_BotonCancelar, 0);
            this.Controls.SetChildIndex(this.c_EtiquetaMoneda, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_Empleado, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.c_nombreUsuario, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label c_nombreUsuario;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label c_Empleado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label c_EtiquetaMoneda;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
    }
}