﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaCambioNumeroBolsa : Azteca_SID.FormBase
    {
        public FormaCambioNumeroBolsa()
        {
            InitializeComponent();
        }

        private void c_Aceptar_Click(object sender, EventArgs e)
        {
            c_NumeroBolsa.Text = c_NumeroBolsa.Text.Trim().Replace(" ", "");


            if (c_NumeroBolsa.Text == "000000" || c_NumeroBolsa.Text == "0000000")
            {
                using (FormaError f_Error = new FormaError(true, "noNumeros"))
                {
                    f_Error.c_MensajeError.Text = "Este Numero No es valido, Escriba nuevamente el # de Bolsa";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.TopMost = true;
                    f_Error.ShowDialog();
                }
                return;
            }

                if (c_NumeroBolsa.Text.Length < 9 && c_NumeroBolsa.Text.Length > 5)
            {
                if (EsNumero(c_NumeroBolsa.Text))
                {
                    using (FormaError f_Error = new FormaError(true, true, false, "pregunta"))
                    {
                        f_Error.c_MensajeError.Text = "Esta seguro de SOBRESCRIBIR EL NÚMERO De BOlSA, Se registrara este Evento";
                        f_Error.TopMost = true;
                        f_Error.c_Imagen.Visible = true;
                        DialogResult l_respuesta = f_Error.ShowDialog();

                        if (l_respuesta == DialogResult.OK)
                        {

                                if (BDSide.BuscarENVASE(c_NumeroBolsa.Text))
                                {
                                    EscribirNumeroDeBOLSA(c_NumeroBolsa.Text);
                                    Globales.NumeroSerieBOLSA = c_NumeroBolsa.Text;
                                    Globales.EscribirBitacora("Cambio # de BOLSA", "Se ha cambiado el número de Bolsa", c_NumeroBolsa.Text, 33);
                                    using (FormaError f_aviso = new FormaError(true, "exito"))
                                    {
                                        f_aviso.c_MensajeError.Text = "Se ha cambiado el numero de Bolsa";
                                        f_aviso.c_Imagen.Visible = false;
                                   f_aviso.TopMost = true;
                                    f_aviso.ShowDialog();
                                    }
                                    this.DialogResult = DialogResult.OK;
                                }
                                else
                                    using (FormaError l_Error = new FormaError(true, "warning"))
                                    {
                                        Globales.EscribirBitacora("Cambio # de BOLSA", "Ya existe el Numero de Bolsa", c_NumeroBolsa.Text, 33);
                                        l_Error.c_MensajeError.Text = "Aparentemente El Folio de BOLSA ya se USO, Introduzca un folio Distinto... ";
                                        l_Error.TopMost = true;
                                        l_Error.ShowDialog();
                                    }
                            

                        }

                    }

                    Close();

                }
                else
                {
                    using (FormaError f_Error = new FormaError(true, "noNumeros"))
                    {
                        f_Error.c_MensajeError.Text = "Se esperan solo números, Escriba nuevamente el # de Bolsa";
                        f_Error.c_Imagen.Visible = false;
                        f_Error.TopMost = true;
                        f_Error.ShowDialog();
                    }
                }
            }else
                using (FormaError f_Error = new FormaError(true, "noNumeros"))
                {
                    f_Error.c_MensajeError.Text = "Use Todos los Numeros de la BOLSA, Escriba nuevamente el # de Bolsa";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.TopMost = true;
                    f_Error.ShowDialog();
                }

        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_NumeroBolsa_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_NumeroBolsa.Text = l_teclado.Captura;



            }
        }

        private bool EsNumero(string cadena)
        {
            //Sencillamente, si se logra hacer la conversión, entonces es número
            try
            {
                decimal resp = Convert.ToDecimal(cadena);
                return true;
            }
            catch //caso contrario, es falso.
            {
                return false;
            }

        }
        public void
        EscribirNumeroDeBOLSA(String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("NumeroBOLSA.txt"))
            {
                l_Archivo.WriteLine(p_Mensaje);
            }
        }

        private void FormaCambioNumeroBolsa_Load(object sender, EventArgs e)
        {
            Globales.LeerBolsaPuesta();
            c_actualnumBolsa.Text = Globales.NumeroSerieBOLSA;
        }

        private void c_actualnumBolsa_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
