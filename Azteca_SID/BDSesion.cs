﻿using Irds;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Azteca_SID
{
    class BDSesion
    {
        public const int ESTATUS_CERRADA = 0;
        public const int ESTATUS_ABIERTA = 1;
        public const int ESTATUS_TERMINACION_ANOMALA = 3;


        /// <summary>
        /// Abre una nueva sesión
        /// </summary>
        /// <param name="p_Error">
        /// Si hubo error, descripción del error
        /// </param>
        /// <returns>
        /// >0 Folio de la sesión
        /// <= 0 No se pudo generar el registro de sesión
        /// </returns>
        public static int
            AbrirNueva(out DateTime p_FechaHoraInicio, out string p_Error)
        {
            int l_R = 0;
            p_Error = "";
            p_FechaHoraInicio = DateTime.Now;

            String l_Query
                = "INSERT INTO Sesion "
                + " ( FechaHoraInicio, IdEstatusSesion) "
                + "VALUES "
                + " ( @FechaHoraInicio, @IdEstatusSesion) "
                + "; SELECT SCOPE_IDENTITY()";

            try
            {
                using (SqlConnection l_Conexion = Conexion.NuevaConexion())
                {
                    using (SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion))
                    {
                        l_Comando.Parameters.AddWithValue("@FechaHoraInicio", p_FechaHoraInicio );
                        l_Comando.Parameters.AddWithValue("@IdEstatusSesion", ESTATUS_ABIERTA);
                        l_R = Convert.ToInt32(l_Comando.ExecuteScalar());
                    }
                }
            }
            catch (Exception E)
            {
                p_Error = "BDSesion.AbrirNueva() - " + E.Message;
                l_R = -1;
            }

            return l_R;
        }

        /// <summary>
        /// Cierra la sesión
        /// </summary>
        /// <param name="p_FolioSesion"></param>
        /// <param name="p_Error"></param>
        /// <returns>
        /// >0 Se cerró la sesión, <=0 No se pudo cerrar la sesion
        /// </returns>
        public static int
            CerrarSesion(long p_FolioSesion, out string p_Error)
        {
            int l_R = 0;
            p_Error = "";

            string l_Query
                = "UPDATE  Sesion "
                + "SET FechaHoraFin = @FechaHoraFin "
                + "     , IdEstatusSesion = @IdEstatusSesion "
                + "WHERE FolioSesion = @FolioSesion ";
            try
            {
                using (SqlConnection l_Conexion = Conexion.NuevaConexion())
                {
                    using (SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion))
                    {
                        l_Comando.Parameters.AddWithValue("@FechaHoraFin", DateTime.Now);
                        l_Comando.Parameters.AddWithValue("@IdEstatusSesion", ESTATUS_CERRADA);
                        l_Comando.Parameters.AddWithValue("@FolioSesion",p_FolioSesion);
                        l_R = l_Comando.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception E)
            {
                p_Error = "BDSesion.AbrirNueva() - " + E.Message;
                l_R = -1;
            }
            return l_R;
        }

        /// <summary>
        /// Cierra sesiones previas que no se hayan cerrado correctamente
        /// </summary>
        /// <param name="p_Error"></param>
        /// <returns>>0 Habia sesiones abiertas, que se cerraron abruptamente</returns>
        public static int
            CerrarSesionesAnteriores(out string p_Error)
        {
            int l_R = 0;
            p_Error = "";

            string l_Query
                = "UPDATE  Sesion "
                + "SET FechaHoraFin = @FechaHoraFin "
                + ", IdEstatusSesion = @IdEstatusSesion "
                + "WHERE IdEstatusSesion =1 ";
               


            try
            {
                using (SqlConnection l_Conexion = Conexion.NuevaConexion())
                {
                    using (SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion))
                    {
                        l_Comando.Parameters.AddWithValue("@FechaHoraFin", DateTime.Now);
                        l_Comando.Parameters.AddWithValue("@IdEstatusSesion", ESTATUS_TERMINACION_ANOMALA);
                        l_R = l_Comando.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception E)
            {
                p_Error = "BDSesion.AbrirNueva() - " + E.Message;
                l_R = -1;
            }
            return l_R;
        }

        /// <summary>
        /// Cierra sesiones previas que no se hayan cerrado correctamente
        /// </summary>
        /// <param name="p_Error"></param>
        /// <returns>>0 Habia sesiones abiertas, que se cerraron abruptamente</returns>
        public static int
            ObtenerUltimaSesionAbierta(out string p_Error)
        {
            int l_R = 0;
            p_Error = "";

            string l_Query
                = "SELECT TOP 1 FolioSesion "
                + "FROM Sesion "
                + "WHERE IdEstatusSesion = 1 "
                + "ORDER BY FolioSesion DESC ";
            try
            {
                DataTable l_X = UtilsBD.GenerarTabla(l_Query);
                if (l_X.Rows.Count > 0)
                {
                    l_R = Convert.ToInt32(l_X.Rows[0][0]);
                }
            }
            catch (Exception E)
            {
                p_Error = "BDSesion.SesionesAbiertas() - " + E.Message;
                l_R = -1;
            }
            return l_R;
        }
    }
}
