﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormModificarUsuario : Azteca_SID.FormBase
    {

        DataTable t_Usuarios;
        DataTable t_perfil;
        String l_usuario;

        public FormModificarUsuario()
        {
            InitializeComponent();
        }


        private void CargarDatos()
        {

            t_perfil = BDPerfiles.TraerPerfiles();

            c_perfiles.DataSource = t_perfil;
            c_perfiles.DisplayMember = "Descripcion";
            c_perfiles.ValueMember = "IdPerfil";

            t_Usuarios = BDUsuarios.Traerusuarios();
            c_Usuarios.DataSource = t_Usuarios;
            c_Usuarios.DisplayMember = "IdUsuario";
            c_Usuarios.ValueMember = "IdUsuario";
            
        }

        private void FormModificarUsuario_Load(object sender, EventArgs e)
        {

            if (Properties.Settings.Default.CAJA_EMPRESARIAL)
            {
                label9.Visible = false;
                label8.Visible = false;
                label1.Visible = false;
               
            }
            else
            {
                label10.Visible = false;
                label11.Visible = false;
                label12.Visible = false;
                label13.Visible = false;
            }


            CargarDatos();
        }

        private void c_Usuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            c_perfiles.SelectedIndex = (int)t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"] - 1;
            c_Nombre.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Nombre"].ToString();
            c_referencia.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Referencia"].ToString();
            c_convenio.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["ConvenioDEM"].ToString();
           c_reFija. Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["ConvenioCIE"].ToString();
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Modificar_Click(object sender, EventArgs e)
        {
            if (c_Contraseña.Text.Length < 1)
            {
                if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                {
                   

                    if (c_reFija.Text.Length > 20)
                    {
                        using (FormaError f_Error = new FormaError(false, "error"))
                        {
                            f_Error.c_MensajeError.Text = "Referencia FIJA debe ser a lo mas 20 posiciones";
                            f_Error.ShowDialog();
                            return;
                        }

                    }
                    if (c_referencia.Text.Length != 10)

                    {
                        using (FormaError f_Error = new FormaError(false, "error"))
                        {
                            f_Error.c_MensajeError.Text = "La Cuenta Bancaria debe tener exactamente 10 posiciones";
                            f_Error.ShowDialog();
                            return;
                        }

                    }
                }

                if (!BDUsuarios.ActualizarUsuario(t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdUsuario"].ToString()
                                                , (int)t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"], c_Nombre.Text.ToUpper(), c_referencia.Text, c_convenio.Text,c_reFija.Text))
                {
                    using (FormaError f_Error = new FormaError(false,"error"))
                    {
                        f_Error.c_MensajeError.Text = "No se Pudo Actualizar este usuario";
                        f_Error.ShowDialog();
                    }

                }
                else
                {
                    using (FormaError f_Error = new FormaError(false, "exito"))
                    {
                        f_Error.c_MensajeError.Text = "Datos ACTUALIZADOs";
                        f_Error.ShowDialog();
                    }
                }
            }
            else
            {
                if (c_Contraseña.Text == c_confirmacionContraseña.Text && c_Contraseña.Text.Length >= 5)
                {
                    if (!BDUsuarios.ActualizarContraseña(t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdUsuario"].ToString()
                                                        , (int)t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"], c_Contraseña.Text))
                    {
                        using (FormaError f_Error = new FormaError(false, "error"))
                        {
                            f_Error.c_MensajeError.Text = "No se Pudo actualizar la contraseña";
                            f_Error.ShowDialog();
                        }
                    }
                    else
                    {
                        using (FormaError f_Error = new FormaError(false,"exito"))
                        {
                            f_Error.c_MensajeError.Text = "Contraseña ACTUALIZADA";
                            f_Error.ShowDialog();
                        }
                    }

                }
                else
                {
                    using (FormaError f_Error = new FormaError(false,"error"))
                    {
                        f_Error.c_MensajeError.Text = "La contraseña debe contener minimo 6 caracteres";
                        f_Error.ShowDialog();
                    }
                }
            }



            c_confirmacionContraseña.Text = "";
            c_Contraseña.Text = "";

            CargarDatos();
        }

        private String EntradaTexto()
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    return l_teclado.Captura;
                else
                    return "";

            }
        }

        private void c_Nombre_Click(object sender, EventArgs e)
        {
            c_Nombre.Text = EntradaTexto();
        }

        private void c_referencia_Click(object sender, EventArgs e)
        {
            c_referencia.Text = EntradaTexto();
        }

        private void c_convenio_Click(object sender, EventArgs e)
        {
            c_convenio.Text = EntradaTexto();
        }

        private void c_Contraseña_Click(object sender, EventArgs e)
        {
            c_Contraseña.Text = EntradaTexto();

        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            c_confirmacionContraseña.Text = EntradaTexto();
        }

        private void c_reFija_Click(object sender, EventArgs e)
        {
            c_reFija.Text = EntradaTexto();
        }
    }
}
