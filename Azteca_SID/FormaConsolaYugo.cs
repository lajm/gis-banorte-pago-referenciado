﻿using SymetryDevices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaConsolaYugo : FormBase
    {
        public FormaConsolaYugo()
        {
            InitializeComponent();
        }
        int Puerto;
        int l_nuevoError;
        DataTable t_Monedas;
        private void FormaConsolaYugo_Load(object sender, EventArgs e)
        {
            Puerto = Properties.Settings.Default.Port_YUGO;

          

        }

        private DataTable CrearTabla()
        {
            t_Monedas = new DataTable("Consulta de Monedas");
            t_Monedas.Columns.Add("Denominacion", typeof(String));
            t_Monedas.Columns.Add("Piezas", typeof(int));
            t_Monedas.Columns.Add("Monto", typeof(string));


            return t_Monedas;
        }

        private void c_conectar_Click(object sender, EventArgs e)
        {
            c_conectar.Enabled = false;

            string l_master="";

            c_status.Text = "..Espere....";

            l_Reply = UCoin.cgetMaster(Puerto,out l_master);

            l_Reply = UCoin.cRealizaResetSoftware(Puerto);



            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }

                l_Reply = UCoin.cRealizaResetHardware(Puerto);



                ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

                if (l_Reply != 0)
                {
                    if (Properties.Settings.Default.Enviar_AlertasGSI)
                        UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                    using (FormaError v_Error = new FormaError(true, "warning"))
                    {
                        v_Error.c_MensajeError.Text = l_Error;
                        v_Error.ShowDialog();
                    }


                }

            }
            else
            {
                g_ucoin.Visible = true;
                c_status.Text = "Equipo Conectado y listo";
                
                c_Consulta.PerformClick();
            }
            c_conectar.Enabled = true;
        }

        private void c_resetSW_Click(object sender, EventArgs e)
        {
            c_resetSW .Enabled = false;
            l_Reply = UCoin.cRealizaResetSoftware(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                c_status.Text = "Reset Exitoso, espere al final de movimiento del mecanismo";
            }
           c_resetSW .Enabled = true;
        }

        private void c_resetHW_Click(object sender, EventArgs e)
        {
            c_resetHW.Enabled = false;
            l_Reply = UCoin.cRealizaResetHardware(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                c_status.Text = "Reset Exitoso";
            }
            c_resetHW.Enabled = true;
        }

        private void c_ligthON_Click(object sender, EventArgs e)
        {
            c_ligthON.Enabled = false;
            l_Reply = UCoin.cTestLigthON(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true,"warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                c_status.Text = "LED Prendido";
            }
            c_ligthON.Enabled = true;
        }

        private void c_ligthOFF_Click(object sender, EventArgs e)
        {
            c_ligthOFF.Enabled = false;
            l_Reply = UCoin.cTestLigthOFF(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                c_status.Text = "LED Apagado";
            }
            c_ligthOFF.Enabled = true;
        }

        private void c_AbrirBovedad_Click(object sender, EventArgs e)
        {
            //Preguntar status Pendiente

            c_AbrirBovedad.Enabled = false;
            c_status.Text = "Por favor Abra cuando escuche el Click";
            l_Reply = UCoin.cOpenSafe(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                c_status.Text = "Apertura exitosa";
            }
            c_AbrirBovedad .Enabled = true;
        }

        private void c_vaciarCassete_Click(object sender, EventArgs e)
        {
            c_vaciarCassete.Enabled = false;
            using (FormaError v_Error = new FormaError(true, "warning"))
            {
                v_Error.c_MensajeError.Text = "A continuación Por favor Abra la Bovedad y retire el Cassete, despues cierre";
                v_Error.ShowDialog();
            }

            l_Reply = UCoin.cRetiro(Puerto);
            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = "Apertura Realizada con Exito";
                    v_Error.ShowDialog();
                }

                c_status.Text = "Contadores en 0s";
                c_Consulta.PerformClick();
            }

            c_vaciarCassete.Enabled = true;
        }

        private void c_Consulta_Click(object sender, EventArgs e)
        {
            c_Consulta.Enabled = false;
            decimal l_total = 0;
            Dictionary<decimal, int> p_tabla = new Dictionary<decimal, int>();

            CrearTabla();

            l_Reply = UCoin.ObtenerTotales((byte)Puerto, "Admin", out l_total, out p_tabla);


            ErroresUcoin.Error_Ucoin(l_Reply, out l_nuevoError, out l_Error);

            if (l_Reply != 0)
            {
                if (Properties.Settings.Default.Enviar_AlertasGSI)
                    UtilsComunicacion.AlertaGSI(l_nuevoError, l_Error);

                using (FormaError v_Error = new FormaError(true, "warning"))
                {
                    v_Error.c_MensajeError.Text = l_Error;
                    v_Error.ShowDialog();
                }
            }
            else
            {
                int l_valor;
                 foreach ( decimal l_key in p_tabla.Keys )
                {
                    l_valor = 0;
                    if (p_tabla.TryGetValue(l_key, out l_valor))
                        if (l_valor >0)
                        Agregarregistro(l_key, l_valor);
                }

                dg_monedas.DataSource = t_Monedas;
                c_status.Text = " Contenido Fisico: " + l_total.ToString("$#,###,###00.00");

            }

            c_Consulta.Enabled = true;
        }

        private void Agregarregistro(decimal p_moneda,int p_piezas)
        {
            DataRow l_Nueva = t_Monedas.NewRow();
            l_Nueva[0] = p_moneda.ToString("$00.00");
            l_Nueva[1] = p_piezas;
            l_Nueva[2] = (p_piezas * p_moneda).ToString("$#,###00.00");

            t_Monedas.Rows.Add(l_Nueva);
        }

        private void c_status_TextChanged(object sender, EventArgs e)
        {
            Forzardibujado();
        }

        private void c_autorizar_Click( object sender , EventArgs e )
        {
            DialogResult l_respuesta;
            using ( FormaAutenticar v_autenticar = new FormaAutenticar( "Supervisor_ZONA" , true ) )
            {
                l_respuesta = v_autenticar.ShowDialog( );
            }

            if ( l_respuesta == DialogResult.OK )
            {
                c_vaciarCassete.Enabled = true;

            }
            else
                MostarMensaje( "Necesita El password del Administrador" , false , "warning" );
        }
    }
}
