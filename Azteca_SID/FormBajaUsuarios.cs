﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormBajaUsuarios : Azteca_SID.FormBase
    {

        DataTable t_Usuarios;
        String l_usuario;
        public FormBajaUsuarios()
        {
            InitializeComponent();
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Baja_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = new DialogResult();

            using (FormaError f_Error = new FormaError(false, "pregunta"))
            {
                f_Error.c_MensajeError.Text = "Esta Seguro de Eliminar este usuario";

                respuesta = f_Error.ShowDialog();

            }

            if (respuesta == DialogResult.OK)
            {
                if (!BDUsuarios.DeleteUsuario(l_usuario))
                {
                    using (FormaError f_Error = new FormaError(false,"error"))
                    {
                        f_Error.c_MensajeError.Text = "No se pudo eliminar el usuario";

                        respuesta = f_Error.ShowDialog();

                    }
                }
                else
                {
                    using (FormaError f_Error = new FormaError(false, "exito"))
                    {
                        f_Error.c_MensajeError.Text = "Cuenta / Usuario  Eliminado del Sistema";

                        f_Error.ShowDialog();

                    }
                    CargarDatos();

                }
            }
        }

        private void CargarDatos()
        {
            t_Usuarios = BDUsuarios.TraerOperadores();

            c_Usuarios.DataSource = t_Usuarios;
        }

        private void FormBajaUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
            c_Usuarios.DisplayMember = "IdUsuario";
            c_Usuarios.ValueMember = "IdUsuario";
        }

        private void c_Usuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            c_Nombre.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Nombre"].ToString();
            c_Perfil.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Perfil"].ToString();
            l_usuario = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdUsuario"].ToString();
        }
    }
}
