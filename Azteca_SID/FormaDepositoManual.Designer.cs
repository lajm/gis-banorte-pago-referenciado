﻿namespace Azteca_SID
{
    partial class FormaDepositoManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaDepositoManual));
            this.c_empleado = new System.Windows.Forms.Label();
            this.c_totalbilletes = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.c_cancelar = new System.Windows.Forms.Button();
            this.c_confirmar = new System.Windows.Forms.Button();
            this.c_total50 = new System.Windows.Forms.Label();
            this.c_total100 = new System.Windows.Forms.Label();
            this.c_total1000 = new System.Windows.Forms.Label();
            this.c_total200 = new System.Windows.Forms.Label();
            this.c_total500 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.c_total20 = new System.Windows.Forms.Label();
            this.c_resta200 = new System.Windows.Forms.Button();
            this.c_resta50 = new System.Windows.Forms.Button();
            this.c_resta500 = new System.Windows.Forms.Button();
            this.c_resta100 = new System.Windows.Forms.Button();
            this.c_resta1000 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_montototal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_resta20 = new System.Windows.Forms.Button();
            this.c_agregar20 = new System.Windows.Forms.PictureBox();
            this.c_agregar200 = new System.Windows.Forms.PictureBox();
            this.c_agregar500 = new System.Windows.Forms.PictureBox();
            this.c_agregar100 = new System.Windows.Forms.PictureBox();
            this.c_agregar50 = new System.Windows.Forms.PictureBox();
            this.c_agregar1000 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar500)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar1000)).BeginInit();
            this.SuspendLayout();
            // 
            // c_empleado
            // 
            this.c_empleado.AutoSize = true;
            this.c_empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_empleado.Location = new System.Drawing.Point(88, 81);
            this.c_empleado.Name = "c_empleado";
            this.c_empleado.Size = new System.Drawing.Size(89, 20);
            this.c_empleado.TabIndex = 103;
            this.c_empleado.Text = "Empleado";
            // 
            // c_totalbilletes
            // 
            this.c_totalbilletes.AutoSize = true;
            this.c_totalbilletes.BackColor = System.Drawing.Color.Transparent;
            this.c_totalbilletes.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbilletes.Location = new System.Drawing.Point(674, 111);
            this.c_totalbilletes.Name = "c_totalbilletes";
            this.c_totalbilletes.Size = new System.Drawing.Size(56, 41);
            this.c_totalbilletes.TabIndex = 102;
            this.c_totalbilletes.Text = "00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(468, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(183, 24);
            this.label11.TabIndex = 101;
            this.label11.Text = "TOTAL BILLETES:";
            // 
            // c_cancelar
            // 
            this.c_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cancelar.Location = new System.Drawing.Point(448, 458);
            this.c_cancelar.Name = "c_cancelar";
            this.c_cancelar.Size = new System.Drawing.Size(179, 41);
            this.c_cancelar.TabIndex = 100;
            this.c_cancelar.Text = "CANCELAR";
            this.c_cancelar.UseVisualStyleBackColor = true;
            this.c_cancelar.Click += new System.EventHandler(this.c_cancelar_Click);
            // 
            // c_confirmar
            // 
            this.c_confirmar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmar.Location = new System.Drawing.Point(174, 458);
            this.c_confirmar.Name = "c_confirmar";
            this.c_confirmar.Size = new System.Drawing.Size(179, 41);
            this.c_confirmar.TabIndex = 99;
            this.c_confirmar.Text = "CONFIRMAR";
            this.c_confirmar.UseVisualStyleBackColor = true;
            this.c_confirmar.Click += new System.EventHandler(this.c_confirmar_Click);
            // 
            // c_total50
            // 
            this.c_total50.AutoSize = true;
            this.c_total50.BackColor = System.Drawing.Color.Transparent;
            this.c_total50.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total50.Location = new System.Drawing.Point(3, 310);
            this.c_total50.Name = "c_total50";
            this.c_total50.Size = new System.Drawing.Size(56, 41);
            this.c_total50.TabIndex = 98;
            this.c_total50.Text = "00";
            // 
            // c_total100
            // 
            this.c_total100.AutoSize = true;
            this.c_total100.BackColor = System.Drawing.Color.Transparent;
            this.c_total100.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total100.Location = new System.Drawing.Point(3, 406);
            this.c_total100.Name = "c_total100";
            this.c_total100.Size = new System.Drawing.Size(56, 41);
            this.c_total100.TabIndex = 97;
            this.c_total100.Text = "00";
            // 
            // c_total1000
            // 
            this.c_total1000.AutoSize = true;
            this.c_total1000.BackColor = System.Drawing.Color.Transparent;
            this.c_total1000.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total1000.Location = new System.Drawing.Point(739, 408);
            this.c_total1000.Name = "c_total1000";
            this.c_total1000.Size = new System.Drawing.Size(56, 41);
            this.c_total1000.TabIndex = 96;
            this.c_total1000.Text = "00";
            // 
            // c_total200
            // 
            this.c_total200.AutoSize = true;
            this.c_total200.BackColor = System.Drawing.Color.Transparent;
            this.c_total200.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total200.Location = new System.Drawing.Point(739, 212);
            this.c_total200.Name = "c_total200";
            this.c_total200.Size = new System.Drawing.Size(56, 41);
            this.c_total200.TabIndex = 95;
            this.c_total200.Text = "00";
            // 
            // c_total500
            // 
            this.c_total500.AutoSize = true;
            this.c_total500.BackColor = System.Drawing.Color.Transparent;
            this.c_total500.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total500.Location = new System.Drawing.Point(739, 308);
            this.c_total500.Name = "c_total500";
            this.c_total500.Size = new System.Drawing.Size(56, 41);
            this.c_total500.TabIndex = 94;
            this.c_total500.Text = "00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(699, 310);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 41);
            this.label8.TabIndex = 90;
            this.label8.Text = "X";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(699, 408);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 41);
            this.label7.TabIndex = 89;
            this.label7.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(52, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 41);
            this.label6.TabIndex = 88;
            this.label6.Text = "X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(699, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 41);
            this.label5.TabIndex = 87;
            this.label5.Text = "X";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(52, 308);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 41);
            this.label9.TabIndex = 91;
            this.label9.Text = "X";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(52, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 41);
            this.label10.TabIndex = 92;
            this.label10.Text = "X";
            // 
            // c_total20
            // 
            this.c_total20.AutoSize = true;
            this.c_total20.BackColor = System.Drawing.Color.Transparent;
            this.c_total20.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_total20.Location = new System.Drawing.Point(3, 210);
            this.c_total20.Name = "c_total20";
            this.c_total20.Size = new System.Drawing.Size(56, 41);
            this.c_total20.TabIndex = 93;
            this.c_total20.Text = "00";
            // 
            // c_resta200
            // 
            this.c_resta200.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta200.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta200.BackgroundImage")));
            this.c_resta200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta200.Location = new System.Drawing.Point(426, 184);
            this.c_resta200.Name = "c_resta200";
            this.c_resta200.Size = new System.Drawing.Size(80, 59);
            this.c_resta200.TabIndex = 86;
            this.c_resta200.Text = "Restar $200";
            this.c_resta200.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta200.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta200.UseVisualStyleBackColor = false;
            this.c_resta200.Click += new System.EventHandler(this.c_resta200_Click);
            // 
            // c_resta50
            // 
            this.c_resta50.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta50.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta50.BackgroundImage")));
            this.c_resta50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta50.Location = new System.Drawing.Point(282, 284);
            this.c_resta50.Name = "c_resta50";
            this.c_resta50.Size = new System.Drawing.Size(80, 59);
            this.c_resta50.TabIndex = 85;
            this.c_resta50.Text = "Restar $50";
            this.c_resta50.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta50.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta50.UseVisualStyleBackColor = false;
            this.c_resta50.Click += new System.EventHandler(this.c_resta50_Click);
            // 
            // c_resta500
            // 
            this.c_resta500.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta500.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta500.BackgroundImage")));
            this.c_resta500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta500.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta500.Location = new System.Drawing.Point(426, 284);
            this.c_resta500.Name = "c_resta500";
            this.c_resta500.Size = new System.Drawing.Size(80, 59);
            this.c_resta500.TabIndex = 84;
            this.c_resta500.Text = "Restar $500";
            this.c_resta500.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta500.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta500.UseVisualStyleBackColor = false;
            this.c_resta500.Click += new System.EventHandler(this.c_resta500_Click);
            // 
            // c_resta100
            // 
            this.c_resta100.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta100.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta100.BackgroundImage")));
            this.c_resta100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta100.Location = new System.Drawing.Point(282, 384);
            this.c_resta100.Name = "c_resta100";
            this.c_resta100.Size = new System.Drawing.Size(80, 59);
            this.c_resta100.TabIndex = 83;
            this.c_resta100.Text = "Restar $100";
            this.c_resta100.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta100.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta100.UseVisualStyleBackColor = false;
            this.c_resta100.Click += new System.EventHandler(this.c_resta100_Click);
            // 
            // c_resta1000
            // 
            this.c_resta1000.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta1000.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta1000.BackgroundImage")));
            this.c_resta1000.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta1000.Location = new System.Drawing.Point(426, 384);
            this.c_resta1000.Name = "c_resta1000";
            this.c_resta1000.Size = new System.Drawing.Size(80, 59);
            this.c_resta1000.TabIndex = 82;
            this.c_resta1000.Text = "Restar $1000";
            this.c_resta1000.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta1000.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta1000.UseVisualStyleBackColor = false;
            this.c_resta1000.Click += new System.EventHandler(this.c_resta1000_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(702, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 81;
            this.label4.Text = "CANTIDAD:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 80;
            this.label3.Text = "CANTIDAD:";
            // 
            // c_montototal
            // 
            this.c_montototal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_montototal.Location = new System.Drawing.Point(281, 110);
            this.c_montototal.Name = "c_montototal";
            this.c_montototal.Size = new System.Drawing.Size(161, 38);
            this.c_montototal.TabIndex = 79;
            this.c_montototal.Text = "$0";
            this.c_montototal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(97, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 24);
            this.label2.TabIndex = 78;
            this.label2.Text = "MONTO TOTAL:";
            // 
            // c_resta20
            // 
            this.c_resta20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.c_resta20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("c_resta20.BackgroundImage")));
            this.c_resta20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_resta20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resta20.Location = new System.Drawing.Point(282, 184);
            this.c_resta20.Name = "c_resta20";
            this.c_resta20.Size = new System.Drawing.Size(80, 59);
            this.c_resta20.TabIndex = 77;
            this.c_resta20.Text = "Restar $20";
            this.c_resta20.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_resta20.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_resta20.UseVisualStyleBackColor = false;
            this.c_resta20.Click += new System.EventHandler(this.c_resta20_Click);
            // 
            // c_agregar20
            // 
            this.c_agregar20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_agregar20.Image = global::Azteca_SID.Properties.Resources.billete_20_pesos_2007;
            this.c_agregar20.Location = new System.Drawing.Point(93, 171);
            this.c_agregar20.Name = "c_agregar20";
            this.c_agregar20.Size = new System.Drawing.Size(183, 82);
            this.c_agregar20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar20.TabIndex = 76;
            this.c_agregar20.TabStop = false;
            this.c_agregar20.Click += new System.EventHandler(this.c_agregar20_Click);
            // 
            // c_agregar200
            // 
            this.c_agregar200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_agregar200.Image = global::Azteca_SID.Properties.Resources.billete_200_pesos_2008;
            this.c_agregar200.Location = new System.Drawing.Point(512, 169);
            this.c_agregar200.Name = "c_agregar200";
            this.c_agregar200.Size = new System.Drawing.Size(183, 82);
            this.c_agregar200.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar200.TabIndex = 75;
            this.c_agregar200.TabStop = false;
            this.c_agregar200.Click += new System.EventHandler(this.c_agregar200_Click);
            // 
            // c_agregar500
            // 
            this.c_agregar500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_agregar500.Image = global::Azteca_SID.Properties.Resources.billete_500_pesos_2010;
            this.c_agregar500.Location = new System.Drawing.Point(512, 267);
            this.c_agregar500.Name = "c_agregar500";
            this.c_agregar500.Size = new System.Drawing.Size(183, 82);
            this.c_agregar500.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar500.TabIndex = 74;
            this.c_agregar500.TabStop = false;
            this.c_agregar500.Click += new System.EventHandler(this.c_agregar500_Click);
            // 
            // c_agregar100
            // 
            this.c_agregar100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_agregar100.Image = global::Azteca_SID.Properties.Resources.billete_100_pesos_2010;
            this.c_agregar100.Location = new System.Drawing.Point(93, 367);
            this.c_agregar100.Name = "c_agregar100";
            this.c_agregar100.Size = new System.Drawing.Size(183, 82);
            this.c_agregar100.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar100.TabIndex = 73;
            this.c_agregar100.TabStop = false;
            this.c_agregar100.Click += new System.EventHandler(this.c_agregar100_Click);
            // 
            // c_agregar50
            // 
            this.c_agregar50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_agregar50.Image = global::Azteca_SID.Properties.Resources.billete50pesos;
            this.c_agregar50.Location = new System.Drawing.Point(93, 269);
            this.c_agregar50.Name = "c_agregar50";
            this.c_agregar50.Size = new System.Drawing.Size(183, 82);
            this.c_agregar50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar50.TabIndex = 72;
            this.c_agregar50.TabStop = false;
            this.c_agregar50.Click += new System.EventHandler(this.c_agregar50_Click);
            // 
            // c_agregar1000
            // 
            this.c_agregar1000.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.c_agregar1000.Image = global::Azteca_SID.Properties.Resources.billete_1000_pesos_holograma;
            this.c_agregar1000.Location = new System.Drawing.Point(512, 365);
            this.c_agregar1000.Name = "c_agregar1000";
            this.c_agregar1000.Size = new System.Drawing.Size(183, 82);
            this.c_agregar1000.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_agregar1000.TabIndex = 71;
            this.c_agregar1000.TabStop = false;
            this.c_agregar1000.Click += new System.EventHandler(this.c_agregar1000_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(277, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 29);
            this.label1.TabIndex = 70;
            this.label1.Text = "DEPOSITO MANUAL";
            // 
            // FormaDepositoManual
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_empleado);
            this.Controls.Add(this.c_totalbilletes);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.c_cancelar);
            this.Controls.Add(this.c_confirmar);
            this.Controls.Add(this.c_total50);
            this.Controls.Add(this.c_total100);
            this.Controls.Add(this.c_total1000);
            this.Controls.Add(this.c_total200);
            this.Controls.Add(this.c_total500);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.c_total20);
            this.Controls.Add(this.c_resta200);
            this.Controls.Add(this.c_resta50);
            this.Controls.Add(this.c_resta500);
            this.Controls.Add(this.c_resta100);
            this.Controls.Add(this.c_resta1000);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_montototal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_resta20);
            this.Controls.Add(this.c_agregar20);
            this.Controls.Add(this.c_agregar200);
            this.Controls.Add(this.c_agregar500);
            this.Controls.Add(this.c_agregar100);
            this.Controls.Add(this.c_agregar50);
            this.Controls.Add(this.c_agregar1000);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "FormaDepositoManual";
            this.Load += new System.EventHandler(this.FormaDepositoManual_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_agregar1000, 0);
            this.Controls.SetChildIndex(this.c_agregar50, 0);
            this.Controls.SetChildIndex(this.c_agregar100, 0);
            this.Controls.SetChildIndex(this.c_agregar500, 0);
            this.Controls.SetChildIndex(this.c_agregar200, 0);
            this.Controls.SetChildIndex(this.c_agregar20, 0);
            this.Controls.SetChildIndex(this.c_resta20, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.c_montototal, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_resta1000, 0);
            this.Controls.SetChildIndex(this.c_resta100, 0);
            this.Controls.SetChildIndex(this.c_resta500, 0);
            this.Controls.SetChildIndex(this.c_resta50, 0);
            this.Controls.SetChildIndex(this.c_resta200, 0);
            this.Controls.SetChildIndex(this.c_total20, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.c_total500, 0);
            this.Controls.SetChildIndex(this.c_total200, 0);
            this.Controls.SetChildIndex(this.c_total1000, 0);
            this.Controls.SetChildIndex(this.c_total100, 0);
            this.Controls.SetChildIndex(this.c_total50, 0);
            this.Controls.SetChildIndex(this.c_confirmar, 0);
            this.Controls.SetChildIndex(this.c_cancelar, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.c_totalbilletes, 0);
            this.Controls.SetChildIndex(this.c_empleado, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar500)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_agregar1000)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label c_empleado;
        private System.Windows.Forms.Label c_totalbilletes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button c_cancelar;
        private System.Windows.Forms.Button c_confirmar;
        private System.Windows.Forms.Label c_total50;
        private System.Windows.Forms.Label c_total100;
        private System.Windows.Forms.Label c_total1000;
        private System.Windows.Forms.Label c_total200;
        private System.Windows.Forms.Label c_total500;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label c_total20;
        private System.Windows.Forms.Button c_resta200;
        private System.Windows.Forms.Button c_resta50;
        private System.Windows.Forms.Button c_resta500;
        private System.Windows.Forms.Button c_resta100;
        private System.Windows.Forms.Button c_resta1000;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_montototal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button c_resta20;
        private System.Windows.Forms.PictureBox c_agregar20;
        private System.Windows.Forms.PictureBox c_agregar200;
        private System.Windows.Forms.PictureBox c_agregar500;
        private System.Windows.Forms.PictureBox c_agregar100;
        private System.Windows.Forms.PictureBox c_agregar50;
        private System.Windows.Forms.PictureBox c_agregar1000;
        private System.Windows.Forms.Label label1;
    }
}
