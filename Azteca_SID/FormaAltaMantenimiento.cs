﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaAltaMantenimiento : Azteca_SID.FormBase
    {
        public FormaAltaMantenimiento()
        {
            InitializeComponent();
        }

      

        private String EntradaTexto()
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    return l_teclado.Captura;
                else
                    return "";

            }
        }

        private void FormaAltaMantenimiento_Load(object sender, EventArgs e)
        {
            c_tipoMantenimiento.SelectedIndex = 0;
            c_vendor.SelectedIndex = 1;
        }

        private void c_registar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty (c_nombre .Text) )
            {
               
                if (!BDMantenimiento.InsertarEvento (DateTime.Now ,c_nombre .Text.ToUpper(),c_vendor.SelectedItem.ToString().ToUpper(),c_tipoMantenimiento.SelectedItem.ToString().ToUpper() ))
                {
                    using (FormaError f_Error = new FormaError(false, "error"))
                    {
                        f_Error.c_MensajeError.Text = "No se pudo Dar de Alta es Mantenimiento por favor reportelo por telefono";
                        f_Error.ShowDialog();
                        return;
                    }
                }
                else
                {

                    Properties.Settings.Default.UltimoEstadoConfirmado = Globales.Estatus.ToString ();
                    Properties.Settings.Default.Save( );
                   

                    if ( Properties.Settings.Default.Enviar_AlertasGSI )
                        UtilsComunicacion.AlertaGSI( 91 , "Inicia el Mantenimiento en Sitio" );

                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Mantenimiento );

                   

                    using (FormaError f_Error = new FormaError(false, "exito"))
                    {
                        f_Error.c_MensajeError.Text = "Evento Registrado Exitosamente";
                        f_Error.ShowDialog();
                        Close();
                    }
                }


            }
            else {
                using (FormaError f_Error = new FormaError(false, "error"))
                {
                    f_Error.c_MensajeError.Text = "Debe Tener un nombre asociado al Tecnico";
                    f_Error.ShowDialog();
                    return;
                }

            }
        }

        private void c_nombre_Click(object sender, EventArgs e)
        {
            c_nombre.Text = EntradaTexto();
        }
    }
}
