﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaAutenticar : FormBase
    {
        String l_Password = "";
        public String l_IdUsuario;

        public FormaAutenticar(String l_user,bool l_timer):base (l_timer)
        {
            InitializeComponent();
            l_IdUsuario = l_user;
        }
        public FormaAutenticar(String l_user)
        {
            InitializeComponent();
            l_IdUsuario = l_user;
        }

        public FormaAutenticar(  bool l_timer ) : base( l_timer )
        {
            InitializeComponent( );
        }



        private void OprimirTecla(object sender, EventArgs e)
        {
           
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Password += "0";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Password += "1";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Password += "2";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Password += "3";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Password += "4";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Password += "5";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Password += "6";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Password += "7";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Password += "8";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Password += "9";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Password.Length != 0)
                {
                    l_Password = l_Password.Substring(0, l_Password.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Password.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
              
                if (l_Password.Length > 0)
                {

                    if ( !String.IsNullOrEmpty( l_IdUsuario ) )
                    {
                        if ( !Globales.AutenticarUsuario( l_IdUsuario , l_Password ) )
                        {


                            using ( FormaError f_Error = new FormaError( true , "password" ) )
                            {
                                f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                                f_Error.ShowDialog( );
                            }
                            this.DialogResult = DialogResult.Abort;

                        }
                        else
                        {
                            Globales.EscribirBitacora( "LOGIN Detectado: " , Globales.IdUsuario , " Autenticado con el Perfil: " + Globales.IdTipoUsuario , 1 );

                            this.DialogResult = DialogResult.OK;

                        }
                    }else
                    {
                        if ( !Globales.AutenticarAdministrador(out  l_IdUsuario , l_Password ) )
                        {


                            using ( FormaError f_Error = new FormaError( true , "password" ) )
                            {
                                f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                                f_Error.ShowDialog( );
                            }
                            this.DialogResult = DialogResult.Abort;

                        }
                        else
                        {
                            Globales.EscribirBitacora( "LOGIN: " , Globales.IdUsuario , " Autenticado" , 1 );

                            this.DialogResult = DialogResult.OK;

                        }

                    }
                        Close();
                  
                }
                else
                {
                    using (FormaError f_Error = new FormaError(true, "texto"))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir una contraseña";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
            
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
