﻿namespace Azteca_SID
{
    partial class FormaConsultaEfectivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.c_numbilletesManual = new System.Windows.Forms.Label();
            this.c_mov = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.c_TotalManual = new System.Windows.Forms.Label();
            this.c_manual20 = new System.Windows.Forms.Label();
            this.c_manual50 = new System.Windows.Forms.Label();
            this.c_manual100 = new System.Windows.Forms.Label();
            this.c_manual200 = new System.Windows.Forms.Label();
            this.c_manual500 = new System.Windows.Forms.Label();
            this.c_manual1000 = new System.Windows.Forms.Label();
            this.c_numbilletes = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Imprimir = new System.Windows.Forms.Button();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_TotalCajon2 = new System.Windows.Forms.Label();
            this.c_Cajon2_20 = new System.Windows.Forms.Label();
            this.c_Cajon2_50 = new System.Windows.Forms.Label();
            this.c_Cajon2_100 = new System.Windows.Forms.Label();
            this.c_Cajon2_200 = new System.Windows.Forms.Label();
            this.c_Cajon2_500 = new System.Windows.Forms.Label();
            this.c_Cajon2_1000 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.c_totalMonedas_b2 = new System.Windows.Forms.Label();
            this.c_bolsa2_10c = new System.Windows.Forms.Label();
            this.c_bolsa2_20c = new System.Windows.Forms.Label();
            this.c_bolsa2_50c = new System.Windows.Forms.Label();
            this.c_bolsa2_1 = new System.Windows.Forms.Label();
            this.c_bolsa2_2 = new System.Windows.Forms.Label();
            this.c_bolsa2_5 = new System.Windows.Forms.Label();
            this.c_bolsa2_10 = new System.Windows.Forms.Label();
            this.c_bolsa2_20 = new System.Windows.Forms.Label();
            this.c_bolsa1_10c = new System.Windows.Forms.Label();
            this.c_bolsa1_20c = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.c_totalbolsa2 = new System.Windows.Forms.Label();
            this.c_totalMonedas_b1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.c_imprimirMonedas = new System.Windows.Forms.Button();
            this.c_cerrarM = new System.Windows.Forms.Button();
            this.c_totalbolsa1 = new System.Windows.Forms.Label();
            this.c_bolsa1_50c = new System.Windows.Forms.Label();
            this.c_bolsa_1 = new System.Windows.Forms.Label();
            this.c_bolsa1_2 = new System.Windows.Forms.Label();
            this.c_bolsa1_5 = new System.Windows.Forms.Label();
            this.c_bolsa1_10 = new System.Windows.Forms.Label();
            this.c_bolsa1_20 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(41, 53);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(719, 469);
            this.tabControl1.TabIndex = 79;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.c_numbilletesManual);
            this.tabPage1.Controls.Add(this.c_mov);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.c_TotalManual);
            this.tabPage1.Controls.Add(this.c_manual20);
            this.tabPage1.Controls.Add(this.c_manual50);
            this.tabPage1.Controls.Add(this.c_manual100);
            this.tabPage1.Controls.Add(this.c_manual200);
            this.tabPage1.Controls.Add(this.c_manual500);
            this.tabPage1.Controls.Add(this.c_manual1000);
            this.tabPage1.Controls.Add(this.c_numbilletes);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.c_Imprimir);
            this.tabPage1.Controls.Add(this.c_Cerrar);
            this.tabPage1.Controls.Add(this.c_TotalCajon2);
            this.tabPage1.Controls.Add(this.c_Cajon2_20);
            this.tabPage1.Controls.Add(this.c_Cajon2_50);
            this.tabPage1.Controls.Add(this.c_Cajon2_100);
            this.tabPage1.Controls.Add(this.c_Cajon2_200);
            this.tabPage1.Controls.Add(this.c_Cajon2_500);
            this.tabPage1.Controls.Add(this.c_Cajon2_1000);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(711, 443);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "BILLETES";
            // 
            // c_numbilletesManual
            // 
            this.c_numbilletesManual.BackColor = System.Drawing.Color.Transparent;
            this.c_numbilletesManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numbilletesManual.Location = new System.Drawing.Point(519, 339);
            this.c_numbilletesManual.Name = "c_numbilletesManual";
            this.c_numbilletesManual.Size = new System.Drawing.Size(117, 24);
            this.c_numbilletesManual.TabIndex = 92;
            this.c_numbilletesManual.Text = "0";
            this.c_numbilletesManual.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_mov
            // 
            this.c_mov.BackColor = System.Drawing.SystemColors.ControlLight;
            this.c_mov.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mov.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_mov.Location = new System.Drawing.Point(260, 372);
            this.c_mov.Name = "c_mov";
            this.c_mov.Size = new System.Drawing.Size(187, 41);
            this.c_mov.TabIndex = 76;
            this.c_mov.TabStop = false;
            this.c_mov.Text = "Detalle por Movimientos";
            this.c_mov.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_mov.UseVisualStyleBackColor = false;
            this.c_mov.Click += new System.EventHandler(this.c_mov_Click_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(485, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(195, 24);
            this.label11.TabIndex = 91;
            this.label11.Text = "Caja SalvoBuenCobro";
            // 
            // c_TotalManual
            // 
            this.c_TotalManual.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalManual.Location = new System.Drawing.Point(480, 305);
            this.c_TotalManual.Name = "c_TotalManual";
            this.c_TotalManual.Size = new System.Drawing.Size(117, 24);
            this.c_TotalManual.TabIndex = 90;
            this.c_TotalManual.Text = "$0.00";
            this.c_TotalManual.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual20
            // 
            this.c_manual20.BackColor = System.Drawing.Color.Transparent;
            this.c_manual20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual20.Location = new System.Drawing.Point(499, 272);
            this.c_manual20.Name = "c_manual20";
            this.c_manual20.Size = new System.Drawing.Size(98, 24);
            this.c_manual20.TabIndex = 89;
            this.c_manual20.Text = "0";
            this.c_manual20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual50
            // 
            this.c_manual50.BackColor = System.Drawing.Color.Transparent;
            this.c_manual50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual50.Location = new System.Drawing.Point(499, 243);
            this.c_manual50.Name = "c_manual50";
            this.c_manual50.Size = new System.Drawing.Size(98, 24);
            this.c_manual50.TabIndex = 88;
            this.c_manual50.Text = "0";
            this.c_manual50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual100
            // 
            this.c_manual100.BackColor = System.Drawing.Color.Transparent;
            this.c_manual100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual100.Location = new System.Drawing.Point(499, 213);
            this.c_manual100.Name = "c_manual100";
            this.c_manual100.Size = new System.Drawing.Size(98, 24);
            this.c_manual100.TabIndex = 87;
            this.c_manual100.Text = "0";
            this.c_manual100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual200
            // 
            this.c_manual200.BackColor = System.Drawing.Color.Transparent;
            this.c_manual200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual200.Location = new System.Drawing.Point(499, 184);
            this.c_manual200.Name = "c_manual200";
            this.c_manual200.Size = new System.Drawing.Size(98, 24);
            this.c_manual200.TabIndex = 86;
            this.c_manual200.Text = "0";
            this.c_manual200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual500
            // 
            this.c_manual500.BackColor = System.Drawing.Color.Transparent;
            this.c_manual500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual500.Location = new System.Drawing.Point(499, 155);
            this.c_manual500.Name = "c_manual500";
            this.c_manual500.Size = new System.Drawing.Size(98, 24);
            this.c_manual500.TabIndex = 85;
            this.c_manual500.Text = "0";
            this.c_manual500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_manual1000
            // 
            this.c_manual1000.BackColor = System.Drawing.Color.Transparent;
            this.c_manual1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual1000.Location = new System.Drawing.Point(499, 126);
            this.c_manual1000.Name = "c_manual1000";
            this.c_manual1000.Size = new System.Drawing.Size(98, 24);
            this.c_manual1000.TabIndex = 84;
            this.c_manual1000.Text = "0";
            this.c_manual1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_numbilletes
            // 
            this.c_numbilletes.BackColor = System.Drawing.Color.Transparent;
            this.c_numbilletes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numbilletes.Location = new System.Drawing.Point(313, 339);
            this.c_numbilletes.Name = "c_numbilletes";
            this.c_numbilletes.Size = new System.Drawing.Size(117, 24);
            this.c_numbilletes.TabIndex = 83;
            this.c_numbilletes.Text = "0";
            this.c_numbilletes.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(304, 20);
            this.label9.TabIndex = 82;
            this.label9.Text = "NUMERO DE BILLETES EN TOTAL: ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(164, 305);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 24);
            this.label13.TabIndex = 81;
            this.label13.Text = "Totales:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(299, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(175, 24);
            this.label10.TabIndex = 80;
            this.label10.Text = "Contenido en Bolsa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(108, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 79;
            this.label8.Text = "$20.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(108, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 24);
            this.label7.TabIndex = 78;
            this.label7.Text = "$50.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(98, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 77;
            this.label6.Text = "$100.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(98, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 76;
            this.label5.Text = "$200.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(98, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 24);
            this.label4.TabIndex = 75;
            this.label4.Text = "$500.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(83, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 74;
            this.label3.Text = "$1,000.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(83, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 24);
            this.label2.TabIndex = 73;
            this.label2.Text = "Denominación";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(140, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 29);
            this.label1.TabIndex = 72;
            this.label1.Text = "CONSULTA DE EFECTIVO BILLETES";
            // 
            // c_Imprimir
            // 
            this.c_Imprimir.BackColor = System.Drawing.SystemColors.ControlLight;
            this.c_Imprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Imprimir.Location = new System.Drawing.Point(73, 372);
            this.c_Imprimir.Name = "c_Imprimir";
            this.c_Imprimir.Size = new System.Drawing.Size(143, 41);
            this.c_Imprimir.TabIndex = 71;
            this.c_Imprimir.TabStop = false;
            this.c_Imprimir.Text = "Imprimir";
            this.c_Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Imprimir.UseVisualStyleBackColor = false;
            this.c_Imprimir.Click += new System.EventHandler(this.c_Imprimir_Click);
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(503, 372);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(143, 41);
            this.c_Cerrar.TabIndex = 70;
            this.c_Cerrar.TabStop = false;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = false;
            this.c_Cerrar.Click += new System.EventHandler(this.c_Cerrar_Click_1);
            // 
            // c_TotalCajon2
            // 
            this.c_TotalCajon2.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon2.Location = new System.Drawing.Point(283, 305);
            this.c_TotalCajon2.Name = "c_TotalCajon2";
            this.c_TotalCajon2.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon2.TabIndex = 69;
            this.c_TotalCajon2.Text = "$0.00";
            this.c_TotalCajon2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_20
            // 
            this.c_Cajon2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_20.Location = new System.Drawing.Point(302, 272);
            this.c_Cajon2_20.Name = "c_Cajon2_20";
            this.c_Cajon2_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_20.TabIndex = 68;
            this.c_Cajon2_20.Text = "0";
            this.c_Cajon2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_50
            // 
            this.c_Cajon2_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_50.Location = new System.Drawing.Point(302, 243);
            this.c_Cajon2_50.Name = "c_Cajon2_50";
            this.c_Cajon2_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_50.TabIndex = 67;
            this.c_Cajon2_50.Text = "0";
            this.c_Cajon2_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_100
            // 
            this.c_Cajon2_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_100.Location = new System.Drawing.Point(302, 213);
            this.c_Cajon2_100.Name = "c_Cajon2_100";
            this.c_Cajon2_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_100.TabIndex = 66;
            this.c_Cajon2_100.Text = "0";
            this.c_Cajon2_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_200
            // 
            this.c_Cajon2_200.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_200.Location = new System.Drawing.Point(302, 184);
            this.c_Cajon2_200.Name = "c_Cajon2_200";
            this.c_Cajon2_200.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_200.TabIndex = 65;
            this.c_Cajon2_200.Text = "0";
            this.c_Cajon2_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_500
            // 
            this.c_Cajon2_500.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_500.Location = new System.Drawing.Point(302, 155);
            this.c_Cajon2_500.Name = "c_Cajon2_500";
            this.c_Cajon2_500.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_500.TabIndex = 64;
            this.c_Cajon2_500.Text = "0";
            this.c_Cajon2_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_1000
            // 
            this.c_Cajon2_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_1000.Location = new System.Drawing.Point(302, 126);
            this.c_Cajon2_1000.Name = "c_Cajon2_1000";
            this.c_Cajon2_1000.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_1000.TabIndex = 63;
            this.c_Cajon2_1000.Text = "0";
            this.c_Cajon2_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.c_totalMonedas_b2);
            this.tabPage2.Controls.Add(this.c_bolsa2_10c);
            this.tabPage2.Controls.Add(this.c_bolsa2_20c);
            this.tabPage2.Controls.Add(this.c_bolsa2_50c);
            this.tabPage2.Controls.Add(this.c_bolsa2_1);
            this.tabPage2.Controls.Add(this.c_bolsa2_2);
            this.tabPage2.Controls.Add(this.c_bolsa2_5);
            this.tabPage2.Controls.Add(this.c_bolsa2_10);
            this.tabPage2.Controls.Add(this.c_bolsa2_20);
            this.tabPage2.Controls.Add(this.c_bolsa1_10c);
            this.tabPage2.Controls.Add(this.c_bolsa1_20c);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.c_totalbolsa2);
            this.tabPage2.Controls.Add(this.c_totalMonedas_b1);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.c_imprimirMonedas);
            this.tabPage2.Controls.Add(this.c_cerrarM);
            this.tabPage2.Controls.Add(this.c_totalbolsa1);
            this.tabPage2.Controls.Add(this.c_bolsa1_50c);
            this.tabPage2.Controls.Add(this.c_bolsa_1);
            this.tabPage2.Controls.Add(this.c_bolsa1_2);
            this.tabPage2.Controls.Add(this.c_bolsa1_5);
            this.tabPage2.Controls.Add(this.c_bolsa1_10);
            this.tabPage2.Controls.Add(this.c_bolsa1_20);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(711, 443);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MONEDAS";
            // 
            // c_totalMonedas_b2
            // 
            this.c_totalMonedas_b2.BackColor = System.Drawing.Color.Transparent;
            this.c_totalMonedas_b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalMonedas_b2.Location = new System.Drawing.Point(549, 346);
            this.c_totalMonedas_b2.Name = "c_totalMonedas_b2";
            this.c_totalMonedas_b2.Size = new System.Drawing.Size(117, 24);
            this.c_totalMonedas_b2.TabIndex = 125;
            this.c_totalMonedas_b2.Text = "0";
            this.c_totalMonedas_b2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_10c
            // 
            this.c_bolsa2_10c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_10c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_10c.Location = new System.Drawing.Point(548, 287);
            this.c_bolsa2_10c.Name = "c_bolsa2_10c";
            this.c_bolsa2_10c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_10c.TabIndex = 124;
            this.c_bolsa2_10c.Text = "0";
            this.c_bolsa2_10c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_10c.Visible = false;
            // 
            // c_bolsa2_20c
            // 
            this.c_bolsa2_20c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_20c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_20c.Location = new System.Drawing.Point(548, 259);
            this.c_bolsa2_20c.Name = "c_bolsa2_20c";
            this.c_bolsa2_20c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_20c.TabIndex = 123;
            this.c_bolsa2_20c.Text = "0";
            this.c_bolsa2_20c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_20c.Visible = false;
            // 
            // c_bolsa2_50c
            // 
            this.c_bolsa2_50c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_50c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_50c.Location = new System.Drawing.Point(549, 231);
            this.c_bolsa2_50c.Name = "c_bolsa2_50c";
            this.c_bolsa2_50c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_50c.TabIndex = 122;
            this.c_bolsa2_50c.Text = "0";
            this.c_bolsa2_50c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_1
            // 
            this.c_bolsa2_1.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_1.Location = new System.Drawing.Point(549, 203);
            this.c_bolsa2_1.Name = "c_bolsa2_1";
            this.c_bolsa2_1.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_1.TabIndex = 121;
            this.c_bolsa2_1.Text = "0";
            this.c_bolsa2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_2
            // 
            this.c_bolsa2_2.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_2.Location = new System.Drawing.Point(549, 175);
            this.c_bolsa2_2.Name = "c_bolsa2_2";
            this.c_bolsa2_2.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_2.TabIndex = 120;
            this.c_bolsa2_2.Text = "0";
            this.c_bolsa2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_5
            // 
            this.c_bolsa2_5.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_5.Location = new System.Drawing.Point(549, 147);
            this.c_bolsa2_5.Name = "c_bolsa2_5";
            this.c_bolsa2_5.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_5.TabIndex = 119;
            this.c_bolsa2_5.Text = "0";
            this.c_bolsa2_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_10
            // 
            this.c_bolsa2_10.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_10.Location = new System.Drawing.Point(549, 119);
            this.c_bolsa2_10.Name = "c_bolsa2_10";
            this.c_bolsa2_10.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_10.TabIndex = 118;
            this.c_bolsa2_10.Text = "0";
            this.c_bolsa2_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa2_20
            // 
            this.c_bolsa2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa2_20.Location = new System.Drawing.Point(549, 91);
            this.c_bolsa2_20.Name = "c_bolsa2_20";
            this.c_bolsa2_20.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa2_20.TabIndex = 117;
            this.c_bolsa2_20.Text = "0";
            this.c_bolsa2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa2_20.Visible = false;
            // 
            // c_bolsa1_10c
            // 
            this.c_bolsa1_10c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_10c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_10c.Location = new System.Drawing.Point(304, 287);
            this.c_bolsa1_10c.Name = "c_bolsa1_10c";
            this.c_bolsa1_10c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_10c.TabIndex = 116;
            this.c_bolsa1_10c.Text = "0";
            this.c_bolsa1_10c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_10c.Visible = false;
            // 
            // c_bolsa1_20c
            // 
            this.c_bolsa1_20c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_20c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_20c.Location = new System.Drawing.Point(304, 259);
            this.c_bolsa1_20c.Name = "c_bolsa1_20c";
            this.c_bolsa1_20c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_20c.TabIndex = 115;
            this.c_bolsa1_20c.Text = "0";
            this.c_bolsa1_20c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_20c.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(126, 280);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 24);
            this.label41.TabIndex = 114;
            this.label41.Text = "$0.10";
            this.label41.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(126, 253);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 24);
            this.label40.TabIndex = 113;
            this.label40.Text = "$0.20";
            this.label40.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(490, 64);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(190, 24);
            this.label32.TabIndex = 112;
            this.label32.Text = "Contenido en Bolsa 2";
            // 
            // c_totalbolsa2
            // 
            this.c_totalbolsa2.BackColor = System.Drawing.Color.Transparent;
            this.c_totalbolsa2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbolsa2.Location = new System.Drawing.Point(496, 323);
            this.c_totalbolsa2.Name = "c_totalbolsa2";
            this.c_totalbolsa2.Size = new System.Drawing.Size(117, 24);
            this.c_totalbolsa2.TabIndex = 111;
            this.c_totalbolsa2.Text = "$0.00";
            this.c_totalbolsa2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_totalMonedas_b1
            // 
            this.c_totalMonedas_b1.BackColor = System.Drawing.Color.Transparent;
            this.c_totalMonedas_b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalMonedas_b1.Location = new System.Drawing.Point(323, 346);
            this.c_totalMonedas_b1.Name = "c_totalMonedas_b1";
            this.c_totalMonedas_b1.Size = new System.Drawing.Size(117, 24);
            this.c_totalMonedas_b1.TabIndex = 104;
            this.c_totalMonedas_b1.Text = "0";
            this.c_totalMonedas_b1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 350);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(308, 20);
            this.label14.TabIndex = 103;
            this.label14.Text = "NUMERO DE MONEDAS EN TOTAL: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(163, 323);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 24);
            this.label15.TabIndex = 102;
            this.label15.Text = "Totales:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(250, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(190, 24);
            this.label16.TabIndex = 101;
            this.label16.Text = "Contenido en Bolsa 1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(126, 226);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 24);
            this.label17.TabIndex = 100;
            this.label17.Text = "$0.50";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(126, 199);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 24);
            this.label18.TabIndex = 99;
            this.label18.Text = "$1.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(126, 172);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 24);
            this.label19.TabIndex = 98;
            this.label19.Text = "$2.00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(126, 145);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 24);
            this.label20.TabIndex = 97;
            this.label20.Text = "$5.00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(116, 118);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 24);
            this.label21.TabIndex = 96;
            this.label21.Text = "$10.00";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(116, 91);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 24);
            this.label22.TabIndex = 95;
            this.label22.Text = "$20.00";
            this.label22.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(48, 64);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(133, 24);
            this.label23.TabIndex = 94;
            this.label23.Text = "Denominación";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(135, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(436, 29);
            this.label24.TabIndex = 93;
            this.label24.Text = "CONSULTA DE EFECTIVO MONEDAS";
            // 
            // c_imprimirMonedas
            // 
            this.c_imprimirMonedas.BackColor = System.Drawing.SystemColors.ControlLight;
            this.c_imprimirMonedas.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_imprimirMonedas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_imprimirMonedas.Location = new System.Drawing.Point(104, 375);
            this.c_imprimirMonedas.Name = "c_imprimirMonedas";
            this.c_imprimirMonedas.Size = new System.Drawing.Size(143, 41);
            this.c_imprimirMonedas.TabIndex = 92;
            this.c_imprimirMonedas.TabStop = false;
            this.c_imprimirMonedas.Text = "Imprimir";
            this.c_imprimirMonedas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_imprimirMonedas.UseVisualStyleBackColor = false;
            this.c_imprimirMonedas.Click += new System.EventHandler(this.c_imprimirMonedas_Click);
            // 
            // c_cerrarM
            // 
            this.c_cerrarM.BackColor = System.Drawing.SystemColors.ControlLight;
            this.c_cerrarM.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cerrarM.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_cerrarM.Location = new System.Drawing.Point(436, 375);
            this.c_cerrarM.Name = "c_cerrarM";
            this.c_cerrarM.Size = new System.Drawing.Size(143, 41);
            this.c_cerrarM.TabIndex = 91;
            this.c_cerrarM.TabStop = false;
            this.c_cerrarM.Text = "Cerrar";
            this.c_cerrarM.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_cerrarM.UseVisualStyleBackColor = false;
            this.c_cerrarM.Click += new System.EventHandler(this.c_cerrarM_Click);
            // 
            // c_totalbolsa1
            // 
            this.c_totalbolsa1.BackColor = System.Drawing.Color.Transparent;
            this.c_totalbolsa1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbolsa1.Location = new System.Drawing.Point(245, 323);
            this.c_totalbolsa1.Name = "c_totalbolsa1";
            this.c_totalbolsa1.Size = new System.Drawing.Size(117, 24);
            this.c_totalbolsa1.TabIndex = 90;
            this.c_totalbolsa1.Text = "$0.00";
            this.c_totalbolsa1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_50c
            // 
            this.c_bolsa1_50c.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_50c.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_50c.Location = new System.Drawing.Point(305, 231);
            this.c_bolsa1_50c.Name = "c_bolsa1_50c";
            this.c_bolsa1_50c.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_50c.TabIndex = 89;
            this.c_bolsa1_50c.Text = "0";
            this.c_bolsa1_50c.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa_1
            // 
            this.c_bolsa_1.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa_1.Location = new System.Drawing.Point(305, 203);
            this.c_bolsa_1.Name = "c_bolsa_1";
            this.c_bolsa_1.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa_1.TabIndex = 88;
            this.c_bolsa_1.Text = "0";
            this.c_bolsa_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_2
            // 
            this.c_bolsa1_2.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_2.Location = new System.Drawing.Point(305, 175);
            this.c_bolsa1_2.Name = "c_bolsa1_2";
            this.c_bolsa1_2.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_2.TabIndex = 87;
            this.c_bolsa1_2.Text = "0";
            this.c_bolsa1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_5
            // 
            this.c_bolsa1_5.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_5.Location = new System.Drawing.Point(305, 147);
            this.c_bolsa1_5.Name = "c_bolsa1_5";
            this.c_bolsa1_5.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_5.TabIndex = 86;
            this.c_bolsa1_5.Text = "0";
            this.c_bolsa1_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_10
            // 
            this.c_bolsa1_10.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_10.Location = new System.Drawing.Point(305, 119);
            this.c_bolsa1_10.Name = "c_bolsa1_10";
            this.c_bolsa1_10.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_10.TabIndex = 85;
            this.c_bolsa1_10.Text = "0";
            this.c_bolsa1_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_bolsa1_20
            // 
            this.c_bolsa1_20.BackColor = System.Drawing.Color.Transparent;
            this.c_bolsa1_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_bolsa1_20.Location = new System.Drawing.Point(305, 91);
            this.c_bolsa1_20.Name = "c_bolsa1_20";
            this.c_bolsa1_20.Size = new System.Drawing.Size(98, 24);
            this.c_bolsa1_20.TabIndex = 84;
            this.c_bolsa1_20.Text = "0";
            this.c_bolsa1_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_bolsa1_20.Visible = false;
            // 
            // FormaConsultaEfectivo
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormaConsultaEfectivo";
            this.Load += new System.EventHandler(this.FormaConsultaEfectivo_Load);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label c_numbilletesManual;
        private System.Windows.Forms.Button c_mov;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label c_TotalManual;
        private System.Windows.Forms.Label c_manual20;
        private System.Windows.Forms.Label c_manual50;
        private System.Windows.Forms.Label c_manual100;
        private System.Windows.Forms.Label c_manual200;
        private System.Windows.Forms.Label c_manual500;
        private System.Windows.Forms.Label c_manual1000;
        private System.Windows.Forms.Label c_numbilletes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Imprimir;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.Label c_TotalCajon2;
        private System.Windows.Forms.Label c_Cajon2_20;
        private System.Windows.Forms.Label c_Cajon2_50;
        private System.Windows.Forms.Label c_Cajon2_100;
        private System.Windows.Forms.Label c_Cajon2_200;
        private System.Windows.Forms.Label c_Cajon2_500;
        private System.Windows.Forms.Label c_Cajon2_1000;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label c_totalMonedas_b2;
        private System.Windows.Forms.Label c_bolsa2_10c;
        private System.Windows.Forms.Label c_bolsa2_20c;
        private System.Windows.Forms.Label c_bolsa2_50c;
        private System.Windows.Forms.Label c_bolsa2_1;
        private System.Windows.Forms.Label c_bolsa2_2;
        private System.Windows.Forms.Label c_bolsa2_5;
        private System.Windows.Forms.Label c_bolsa2_10;
        private System.Windows.Forms.Label c_bolsa2_20;
        private System.Windows.Forms.Label c_bolsa1_10c;
        private System.Windows.Forms.Label c_bolsa1_20c;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label c_totalbolsa2;
        private System.Windows.Forms.Label c_totalMonedas_b1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button c_imprimirMonedas;
        private System.Windows.Forms.Button c_cerrarM;
        private System.Windows.Forms.Label c_totalbolsa1;
        private System.Windows.Forms.Label c_bolsa1_50c;
        private System.Windows.Forms.Label c_bolsa_1;
        private System.Windows.Forms.Label c_bolsa1_2;
        private System.Windows.Forms.Label c_bolsa1_5;
        private System.Windows.Forms.Label c_bolsa1_10;
        private System.Windows.Forms.Label c_bolsa1_20;
    }
}
