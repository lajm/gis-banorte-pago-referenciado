﻿namespace Azteca_SID
{
    partial class FormBajaUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Usuarios = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Baja = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Perfil = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // c_Usuarios
            // 
            this.c_Usuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Usuarios.FormattingEnabled = true;
            this.c_Usuarios.Location = new System.Drawing.Point(330, 225);
            this.c_Usuarios.Name = "c_Usuarios";
            this.c_Usuarios.Size = new System.Drawing.Size(180, 24);
            this.c_Usuarios.TabIndex = 10;
            this.c_Usuarios.SelectedIndexChanged += new System.EventHandler(this.c_Usuarios_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(248, 262);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 18;
            this.label7.Text = "Perfil";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(147, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 16);
            this.label6.TabIndex = 19;
            this.label6.Text = "Nombre/Descripción";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_Nombre.Enabled = false;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(330, 297);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.ReadOnly = true;
            this.c_Nombre.Size = new System.Drawing.Size(295, 22);
            this.c_Nombre.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(240, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(321, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Seleccione una Cuenta, Usuario  / ETV";
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(435, 418);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(162, 51);
            this.c_Cancelar.TabIndex = 14;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Baja
            // 
            this.c_Baja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Baja.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Baja.Location = new System.Drawing.Point(204, 418);
            this.c_Baja.Name = "c_Baja";
            this.c_Baja.Size = new System.Drawing.Size(162, 51);
            this.c_Baja.TabIndex = 13;
            this.c_Baja.Text = "Dar de Baja";
            this.c_Baja.UseVisualStyleBackColor = true;
            this.c_Baja.Click += new System.EventHandler(this.c_Baja_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(263, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(275, 29);
            this.label4.TabIndex = 15;
            this.label4.Text = "BAJA DE  OPERADOR";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(238, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Usuario";
            // 
            // c_Perfil
            // 
            this.c_Perfil.BackColor = System.Drawing.SystemColors.Info;
            this.c_Perfil.Enabled = false;
            this.c_Perfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Perfil.Location = new System.Drawing.Point(330, 260);
            this.c_Perfil.Name = "c_Perfil";
            this.c_Perfil.ReadOnly = true;
            this.c_Perfil.Size = new System.Drawing.Size(180, 22);
            this.c_Perfil.TabIndex = 11;
            // 
            // FormBajaUsuarios
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_Usuarios);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Baja);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Perfil);
            this.Name = "FormBajaUsuarios";
            this.Load += new System.EventHandler(this.FormBajaUsuarios_Load);
            this.Controls.SetChildIndex(this.c_Perfil, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_Baja, 0);
            this.Controls.SetChildIndex(this.c_Cancelar, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.c_Nombre, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.c_Usuarios, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox c_Usuarios;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Baja;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_Perfil;
    }
}
