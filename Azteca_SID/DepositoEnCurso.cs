﻿using Newtonsoft.Json;
using SidApi;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public class DepositoEnCurso
    {
        public DateTime m_FechaInicio;
        public int m_IdSesion;
        public String m_IdUsuario;
        public String m_Cuenta;
        public string m_Referencia;
        public string m_Nombre;
        public ContadorEfectivo m_ConteoInicialMonedas = null;
        public ContadorEfectivo m_ConteoInicialBilletes = null;

        private const string _K = "isfi84284834u824ouhfabfhadf";
        private const string _ExtensionJson = ".json";
        private const string _ExtensionCifrado = ".ctx";
        private const int _L = 10;

        static string _PrefijoArchivo = "TRXT_";


        public DepositoEnCurso()
        {
        }


        public DepositoEnCurso(int p_IdSesion, DateTime p_FechaInicio, String p_IdUsuario, String p_Cuenta
                                , string p_Referencia , String p_Nombre )
        {
            m_IdSesion = p_IdSesion;
            m_FechaInicio = p_FechaInicio;
            m_IdUsuario = p_IdUsuario;
            m_Nombre = p_Nombre;

            if ( !Properties.Settings.Default.CAJA_EMPRESARIAL )
            {
                m_Cuenta = p_Cuenta;
                m_Referencia = p_Referencia;
            }
            else
            {
                m_Cuenta = p_Referencia;
                m_Referencia = p_Cuenta;
            }
        }


        public int Actualizar(ContadorEfectivo p_ContadorBilletes, ContadorEfectivo p_ContadorMonedas, out string p_Error)
        {
            int l_R = 0;
            p_Error = "";

            try
            {
                if (p_ContadorBilletes != null)
                    m_ConteoInicialBilletes = p_ContadorBilletes;
                else
                    p_ContadorBilletes = null;

                if (p_ContadorMonedas != null)
                    m_ConteoInicialMonedas = p_ContadorMonedas;
                else
                    m_ConteoInicialMonedas = null;

                l_R = 1;
            }
            catch (Exception E)
            {
                p_Error = "DepositoEnCurso.Actualizar() EXCEPTION - " + E.Message;
                l_R = -1;
            }
            return l_R;
        }


        public string Serializar()
        {
            String l_R = JsonConvert.SerializeObject(this, Formatting.Indented);

            return l_R;
        }

        public static DepositoEnCurso Deserializar(String p_Json)
        {
            DepositoEnCurso l_R = JsonConvert.DeserializeObject<DepositoEnCurso>(p_Json);
            return l_R;
        }

        public static String NombreJson(int p_IdSesion)
        {
            String p_Path = _PrefijoArchivo + p_IdSesion.ToString("000000") + _ExtensionJson;

            return p_Path;
        }

        public static String NombreCifrado(int p_IdSesion)
        {
            String p_Path = _PrefijoArchivo + p_IdSesion.ToString("000000") + _ExtensionCifrado;

            return p_Path;
        }

        public void EscribirJson()
        {
            String l_Serial = Serializar();
            String l_Nombre = NombreJson(m_IdSesion);

            File.WriteAllText(l_Nombre, l_Serial);
        }

        public void EscribirCifrado()
        {
            String l_Serial = Serializar();
            String l_Cifrado = cifrar(l_Serial, _K.Substring(_L));
            String l_Nombre = NombreCifrado(m_IdSesion);

            File.WriteAllText(l_Nombre, l_Cifrado);
        }

        public static int EliminarTransaccionTrunca(int p_IdSesion)
        {
            String l_Nombre;
            int l_Resultado = ErroresSid.Codigos.OKAY;
            try
            {
                l_Nombre = NombreJson(p_IdSesion);
                File.Delete(l_Nombre);

                l_Nombre = NombreCifrado(p_IdSesion);
                File.Delete(l_Nombre);
            }
            catch (Exception E)
            {
                l_Resultado = ErroresSid.Codigos.SOFTWARE;
                Globales.EscribirBitacora("DepositoEnCurso", "EliminarTransaccion", E.Message, 3);
            }

            return l_Resultado;
        }


        public static void EliminarTodosArchivos()
        {

            try
            {
                String[] l_Archivos = Directory.GetFiles(".", _PrefijoArchivo + "*.*");
                for (int i = 0; i < l_Archivos.Length; i++)
                {
                    File.Delete(l_Archivos[i]);
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("DepositoEnCurso", "EliminarTodosArchivos", E.Message, 3);
            }
        }




        public static DepositoEnCurso LeerCifrado(int p_Sesion, out int p_Reply, out string p_Error)
        {
            DepositoEnCurso l_R = null;

            try
            {
                p_Reply = ErroresSid.Codigos.OKAY;
                p_Error = "";
                String l_Nombre = NombreCifrado(p_Sesion);
                String l_Contenido = File.ReadAllText(l_Nombre);
                String l_JSon = descifrar(l_Contenido, _K.Substring(_L));
                l_R = JsonConvert.DeserializeObject<DepositoEnCurso>(l_JSon);
            }
            catch (Exception E)
            {
                p_Reply = ErroresSid.Codigos.SOFTWARE;
                p_Error = "DepositoEnCurso.LeerCifrado() - " + E.Message;
                return null;
            }

            return l_R;
        }

        // Función para descifrar una cadena.
        public static string descifrar(string cadena, string clave)
        {

            byte[] llave;

            byte[] arreglo = Convert.FromBase64String(cadena); // Arreglo donde guardaremos la cadena descovertida.

            // Ciframos utilizando el Algoritmo MD5.
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            llave = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(clave));
            md5.Clear();

            //Ciframos utilizando el Algoritmo 3DES.
            TripleDESCryptoServiceProvider tripledes = new TripleDESCryptoServiceProvider();
            tripledes.Key = llave;
            tripledes.Mode = CipherMode.ECB;
            tripledes.Padding = PaddingMode.PKCS7;
            ICryptoTransform convertir = tripledes.CreateDecryptor();
            byte[] resultado = convertir.TransformFinalBlock(arreglo, 0, arreglo.Length);
            tripledes.Clear();

            string cadena_descifrada = UTF8Encoding.UTF8.GetString(resultado); // Obtenemos la cadena
            return cadena_descifrada; // Devolvemos la cadena
        }


        string cifrar(string cadena, string clave)
        {
            byte[] llave; //Arreglo donde guardaremos la llave para el cifrado 3DES.

            byte[] arreglo = UTF8Encoding.UTF8.GetBytes(cadena); //Arreglo donde guardaremos la cadena descifrada.

            // Ciframos utilizando el Algoritmo MD5.
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            llave = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(clave));
            md5.Clear();

            //Ciframos utilizando el Algoritmo 3DES.
            TripleDESCryptoServiceProvider tripledes = new TripleDESCryptoServiceProvider();
            tripledes.Key = llave;
            tripledes.Mode = CipherMode.ECB;
            tripledes.Padding = PaddingMode.PKCS7;
            ICryptoTransform convertir = tripledes.CreateEncryptor(); // Iniciamos la conversión de la cadena
            byte[] resultado = convertir.TransformFinalBlock(arreglo, 0, arreglo.Length); //Arreglo de bytes donde guardaremos la cadena cifrada.
            tripledes.Clear();

            return Convert.ToBase64String(resultado, 0, resultado.Length); // Convertimos la cadena y la regresamos.
        }


        //public static JsonSalidaError Leer(String p_Path, String p_Clave)
        //{
        //    Configuracion l_Configuracion = null;
        //    JsonSalidaError l_Salida;
        //    try
        //    {
        //        Globals.Log("Configuracion.Leer <<");
        //        Globals.Log("Configuracion.Leer 1: Path<" + p_Path + "> ");

        //        String l_TextoCifrado = File.ReadAllText(p_Path);
        //        String l_Texto = descifrar(l_TextoCifrado, p_Clave);
        //        l_Configuracion = JsonConvert.DeserializeObject<Configuracion>(l_Texto);
        //        ConfiguracionActual = l_Configuracion;
        //        l_Salida = new JsonSalidaError();
        //        Globals.Log("Configuracion.Leer 2: Configuración Cargada!");
        //    }
        //    catch (Exception E)
        //    {
        //        Globals.Log("Configuracion.Leer ERROR - " + E.Message);
        //        l_Salida = new JsonSalidaError(Errores.ParametroErroneo
        //                                        , "Error leyendo archivo de configuración de componente 'recicladortf.config'"
        //                                        , E.Message);
        //    }
        //    Globals.Log("Configuracion.Leer >>");

        //    return l_Salida;
        //}




        //int IniciarTablasDetalleDeposito()
        //{
        //    m_DetalleBilleteDeposito = new DetalleDeposito[6];
        //    m_DetalleBilleteDeposito[0] = new DetalleDeposito(19, 20);
        //    m_DetalleBilleteDeposito[1] = new DetalleDeposito(18, 50);
        //    m_DetalleBilleteDeposito[2] = new DetalleDeposito(17, 100);
        //    m_DetalleBilleteDeposito[3] = new DetalleDeposito(16, 200);
        //    m_DetalleBilleteDeposito[4] = new DetalleDeposito(15, 500);
        //    m_DetalleBilleteDeposito[5] = new DetalleDeposito(14, 1000);

        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(29, 0.10m);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(30, 0.20m);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(31, 0.50m);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(32, 1);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(33, 2);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(34, 5);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(35, 10);
        //    m_DetalleMonedaDeposito[0] = new DetalleDeposito(36, 20);
        //    return 0;
        //}


    }
}
