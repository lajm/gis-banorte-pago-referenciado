﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    public class UtilsBD
    {
        public static String s_FormatoFechaTexto = "{0:yyyy-MM-ss hh-mm-ss}";

        private static String CadenaConexion
        {
            get { return Properties.Settings.Default.CadenaConexion; }
        }

        public static SqlConnection
          NuevaConexion()
        {
            SqlConnection l_Conexion;
            try
            {
                l_Conexion = new SqlConnection(CadenaConexion);
                l_Conexion.Open();
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("UtilsBD", "CadenaConexion:", ex.Message, 1);
                return null;

            }

            return l_Conexion;
        }

        //SIDE
        private static String CadenaConexionSIDE
        {
            get { return Properties.Settings.Default.CadenaConexionSIDE; }
        }


        public static SqlConnection
            NuevaConexionSIDE()
        {
            SqlConnection l_Conexion;
            try
            {
                l_Conexion = new SqlConnection(CadenaConexionSIDE);

                l_Conexion.Open();
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("UtilsBD", "CadenaConexionSIDE:", ex.Message, 1);
                return null;
            }

            return l_Conexion;
        }

        public static void PonerInt(SqlCommand p_Comando, String p_Parametro, Int64? p_Valor)
        {
            if (p_Valor != null)
                p_Comando.Parameters.AddWithValue(p_Parametro, p_Valor);
            else
                p_Comando.Parameters.AddWithValue(p_Parametro, DBNull.Value);
        }


        public static void PonerString(SqlCommand p_Comando, String p_Parametro, String p_Valor)
        {
            if (p_Valor != null)
                p_Comando.Parameters.AddWithValue(p_Parametro, p_Valor);
            else
                p_Comando.Parameters.AddWithValue(p_Parametro, DBNull.Value);
        }


        public static void PonerFecha(SqlCommand p_Comando, String p_Parametro, DateTime? p_Valor)
        {
            if (p_Valor != null)
                p_Comando.Parameters.AddWithValue(p_Parametro, p_Valor);
            else
            {
                SqlParameter l_P = new SqlParameter(p_Parametro, SqlDbType.DateTime);
                l_P.Value = DBNull.Value;

                p_Comando.Parameters.Add(l_P);
            }
        }


        public static String FormatDate(DateTime? p_Fecha)
        {
            String l_Result = "";

            if (p_Fecha != null)
                l_Result = ((DateTime)p_Fecha).ToString(s_FormatoFechaTexto);


            return "'" + l_Result + "'";
        }



        public static DataTable
            GenerarTabla(String l_Query)
        {
            using (SqlConnection l_Conexion = NuevaConexion())
            {
                using (SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Query, l_Conexion))
                {
                    DataTable l_Tabla = new DataTable();
                    l_Adaptador.Fill(l_Tabla);

                    return l_Tabla;
                }
            }
        }

        public static DataTable
            GenerarTablaTry(string l_Query, out string p_Error)
        {
            DataTable l_Tabla = null;
            p_Error = "";
            try
            {
                using (SqlConnection l_Conexion = NuevaConexion())
                {
                    using (SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Query, l_Conexion))
                    {
                        l_Tabla = new DataTable();
                        l_Adaptador.Fill(l_Tabla);

                        return l_Tabla;
                    }
                }
            }
            catch (Exception E)
            {
                p_Error = "UtilsBD.GenerarTablaTry()";
            }

            return l_Tabla;
        }



        public static Object
            EjecutarEscalar(String l_Query)
        {
            using (SqlConnection l_Conexion = NuevaConexion())
            {
                l_Conexion.Open();
                using (SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion))
                {
                    return l_Comando.ExecuteScalar();
                }
            }
        }


        public static int?
            EjecutarComando(String l_Query)
        {
            using (SqlConnection l_Conexion = NuevaConexion())
            {
                l_Conexion.Open();
                using (SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion))
                {
                    int l_Resultado = l_Comando.ExecuteNonQuery();
                    l_Conexion.Close();
                    return l_Resultado;
                }
            }
        }


        public static String
            AgregarCondicion(ref String p_Filtro, String p_Condicion)
        {
            if (String.IsNullOrEmpty(p_Filtro))
                p_Filtro = p_Filtro + " WHERE " + p_Condicion + " ";
            else
                p_Filtro = p_Filtro + " AND " + p_Condicion + " ";

            return p_Filtro;
        }


        public static String
            ReplaceQueryIntParameter(ref String p_Query, String p_Parametro, Int64 p_Value)
        {
            p_Query = p_Query.Replace(p_Parametro, " " + p_Value + " ");

            return p_Query;
        }


        public static String
            ReplaceQueryStringParameter(ref String p_Query, String p_Parametro, String p_Value)
        {
            p_Query = p_Query.Replace(p_Parametro, " '" + p_Value + "' ");

            return p_Query;
        }


        public static String
            ReplaceQueryDateParameter(ref String p_Query, String p_Parametro, DateTime p_Value)
        {
            String l_DateString = p_Value.ToString("yyyy-MM-dd");
            p_Query = p_Query.Replace(p_Parametro, " '" + p_Value + "' ");

            return p_Query;
        }


        public static String
            ObtenerString(DataRow p_Datos, String p_Campo)
        {
            try
            {
                return p_Datos[p_Campo].ToString();
            }
            catch (Exception)
            {
            }

            return null;
        }



        public static String
            ObtenerString(DataTable p_Datos, String p_Campo)
        {
            return ObtenerString(p_Datos.Rows[0], p_Campo);
        }


        public static Int64?
            ObtenerIntNull(DataRow p_Datos, String p_Campo)
        {
            try
            {
                return Convert.ToInt64(p_Datos[p_Campo].ToString());
            }
            catch (Exception)
            {
            }

            return null;
        }


        public static Int64?
            ObtenerIntNull(DataTable p_Datos, String p_Campo)
        {

            return ObtenerIntNull(p_Datos.Rows[0], p_Campo);
        }


        public static Int32
            ObtenerInt(DataRow p_Datos, String p_Campo)
        {
            return (Int32)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static Int32
            ObtenerInt(DataTable p_Datos, String p_Campo)
        {
            return (Int32)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static Int64
            ObtenerInt64(DataTable p_Datos, String p_Campo)
        {
            return (Int64)ObtenerIntNull(p_Datos, p_Campo);
        }



        public static Int32?
            ObtenerInt32Null(DataTable p_Datos, String p_Campo)
        {
            return (Int32?)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static Int32
            ObtenerInt32(DataTable p_Datos, String p_Campo)
        {
            return (Int32)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static Int16?
            ObtenerInt16Null(DataTable p_Datos, String p_Campo)
        {
            return (Int16?)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static Int16
            ObtenerInt16(DataTable p_Datos, String p_Campo)
        {
            return (Int16)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static byte
            ObtenerByte(DataTable p_Datos, String p_Campo)
        {
            return (byte)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static byte?
            ObtenerByteNull(DataTable p_Datos, String p_Campo)
        {
            return (byte?)ObtenerIntNull(p_Datos, p_Campo);
        }


        public static String
            FormatoBDFechaHora(DateTime p_FechaHota)
        {
            return p_FechaHota.ToString("yyyy-MM-dd HH:mm:ss");
        }



    }
}
