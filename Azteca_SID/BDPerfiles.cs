﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Azteca_SID
{
    class BDPerfiles
    {

        public static DataTable  TraerPerfiles()
        {
            String l_query
                ="Select *"
                +"From Perfil";

            using (SqlConnection l_connetion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_comando = new SqlCommand(l_query, l_connetion);

                SqlDataAdapter l_adapter = new SqlDataAdapter(l_comando);
                DataTable l_tabla= new DataTable ("Perfiles");

                l_adapter.Fill(l_tabla);

                return l_tabla;
                
            }

        }


    }
}
