﻿using IrdsMensajes;
using Newtonsoft.Json;
using SidApi;
using SymetryDevices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using U_Coin;
using UtilsSymSid;

namespace Azteca_SID
{
    public partial class FormaPrincipal : Form
    {

        String l_VersionDll, l_Version_Doc;
        bool Envio = true;

        public static FormaPrincipal s_Forma;

        public void Gsi_Cronometro( bool p_Activar )
        {
            gsi_cronometro.Enabled = p_Activar;
        }


        public FormaPrincipal( )
        {            
            InitializeComponent( );
           
            s_Forma = this;
            Directory.CreateDirectory( "Bitacoras" );
            Directory.CreateDirectory( "Transacciones" );

            Globales.WorkStation = Environment.MachineName.Length > 10 ? Environment.MachineName.Substring( 0 , 10 ) : Environment.MachineName;

            
        }

        private void FormaPrincipal_Load( object sender , EventArgs e )
        {

            
            Cursor.Show( );
            Cursor.Current = Cursors.AppStarting;

            Globales.Equipo2_Estatus = Globales.EstatusReceptor.Desconocido;


            using ( FormaError f_Error = new FormaError( true , "Iniciando" ) )
            {
                f_Error.c_MensajeError.Text = "Espere... Inicializando el Sistema";
                f_Error.ShowDialog( );
                //  c_BotonCancelar_Click(this, null);
            }

            //IRDSLog
            //Log.s_Activo = Properties.Settings.Default.LogComunicacion;

            Globales.LeerUltimoEstatus( );

            try
            {
                if ( ValidarDirectorio( ) > 0 )
                    Globales.ConfiguracionGSI = true;
                else
                {
                    using ( FormaError l_error = new FormaError( false , "warning" ) )
                    {
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Desconocido );
                        l_error.c_MensajeError.Text = "Error al Cargar Configuración de CR BANORTE, Avise al Administrador";
                        l_error.ShowDialog( );
                        Close( );
                    }
                }
            }
            catch ( Exception exx )
            {
                using ( FormaError l_error = new FormaError( false , "warning" ) )
                {
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Desconocido );
                    l_error.c_MensajeError.Text = "Error al Cargar Configuración de CR BANORTE, Avise al Administrador" + exx.Message;
                    l_error.ShowDialog( );
                    Close( );
                }
            }




            Globales.EscribirBitacora( "FormaPrincipal" , "IniciarSesion()" , "Nombre software: " + Application.ProductName , 3 );
            Globales.EscribirBitacora( "FormaPrincipal" , "IniciarSesion()" , "Version interna software: " + Application.ProductVersion , 3 );
            Globales.EscribirBitacora( "FormaPrincipal" , "IniciarSesion()" , "# de Instalacion Symetry: " + Properties.Settings.Default.Estacion , 3 );
            Globales.EscribirBitacora( "FormaPrincipal" , "IniciarSesion()" , "Ubicacion: " + Properties.Settings.Default.EdoCdUbi , 3 );

            ProbarSID( );



            Globales.LeerBolsaPuesta( );




            c_version.Text += "1.5.0.0";


            c_cronometro.Interval = 1000 * 60 * Properties.Settings.Default.TiempoVivo;
            c_cronometro.Enabled = true;

            if ( !( Properties.Settings.Default.Port_YUGO > 0 ) )
            {
                c_stokMonedas.Visible = false;
                e_porcentaje.Visible = false;
            }



            try
            {
                ComprobarBolsa( );
            }
            catch ( Exception exx )
            {

            }


            try
            {
                CalcularStock( );

            }
            catch ( Exception exx )
            {

            }



            if ( Properties.Settings.Default.ConfirmacionLLaves != true )
            {
                using ( FormaError l_error = new FormaError( false , "warning" ) )
                {
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Desconocido );
                    l_error.c_MensajeError.Text = "Error al Cargar Configuración de APP BANORTE, Avise al Administrador";
                    l_error.TopMost = true;
                    l_error.ShowDialog( );
                    Close( );

                }
            }
            else
            {



                revisarSesion( );

                using ( FormaFueraServicio l_chek = new FormaFueraServicio( ) )
                {
                    l_chek.ShowDialog( );
                }

                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                {
                    gsi_cronometro.Interval = 1000 * 60 * Properties.Settings.Default.TiempoSensado;
                    gsi_cronometro.Enabled = true;
                    UtilsComunicacion.KeepaliveGSI( (int) Globales.Estatus );
                }
            }


            Cursor.Current = Cursors.Default;
            Cursor.Hide( );
            this.Focus( );

            GsiPortal.getConfiguracionResponse l_config = UtilsComunicacion.ConfiguracionCajeroGSI("GSI901");
            Globales.c_keepalive_G.Enabled = false;
        }

        private bool EsNumero( string cadena )
        {
            //Sencillamente, si se logra hacer la conversión, entonces es número
            try
            {
                decimal resp = Convert.ToDecimal( cadena );
                return true;
            }
            catch //caso contrario, es falso.
            {
                return false;
            }

        }

        private void revisarSesion( )
        {
            String l_Error;
            int l_R;

            String l_Envasependiente = "";
            if ( Globales.RetiroPendiente( out l_Envasependiente ) )
            {
                if ( BDSide.BolsaSinRetiro( l_Envasependiente ) && EsNumero( l_Envasependiente ) )
                {

                    using ( FormaRecuperarRetiro v_recuperacion = new FormaRecuperarRetiro( l_Envasependiente ) )
                    {
                        v_recuperacion.ShowDialog( );
                    }
                }
                else
                {
                    //Si existe Archivo pero ya hay retiro, borro archivo
                    Globales.BorrarRetiroPendiente( );
                }
            }

            // -- RECUPERACIÓN Y TRATAMIENTO DE TRANSACCIONES TRUNCAS
            try
            {
                Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "Buscar sesión cerrada abruptamente" , 3 );
                int l_Sesion = BDSesion.ObtenerUltimaSesionAbierta( out l_Error );




                // -- BUSCA DATOS DE TRANSACCIÓN EN CASO DE QUE SE HAYA TRUNCADO 
                if ( l_Sesion >= 1 )
                {
                    Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()"
                        , "Se encontro cierre de sesión abrupto del aplicativo, FolioSesion : " + l_Sesion , 3 );

                 
                    if ( Properties.Settings.Default.Enviar_AlertasGSI )
                    {
                        UtilsComunicacion.AlertaGSI( 93 , "Cierre de Aplicativo Inesperado" );

                    }
                    DepositoEnCurso l_Recuperado = DepositoEnCurso.LeerCifrado( l_Sesion , out l_R , out l_Error );

                    if ( l_R >= 0 )
                    {

                        //Mecanismo para borrar registro Doble en BD
                        Globales.Numero_Sesion = l_Sesion;

                        if ( l_Recuperado != null )
                            using ( FormaRecuperacionDeposito v_recuperacion = new FormaRecuperacionDeposito( l_Recuperado ) )
                            {
                                v_recuperacion.ShowDialog( );
                            }

                        // -- CERRAR SESIONES ABIERTAS ANTERIORES
                        l_R = BDSesion.CerrarSesionesAnteriores( out l_Error );
                        //--- Archivos CTX se eliminan despues de una Recuperacion.
                        Borar_Archivos_Cifrados( );
                        Mover_Archivos_Recuperados( );

                        if ( l_R > 0 )
                        {
                            // Grabar log de cierre de sesión abrupto
                            Globales.EscribirBitacora( "Inicio" , "revisarSesion()" , "Se cierran las sesiones abiertas" , 3 );
                            //Eliminar Archvos cifrados para recuperacion
                           
                        }
                        else if ( l_R < 0 )
                        {
                            // Grabar error de BD
                            Globales.EscribirBitacora( "Inicio" , "revisarSesion()" , "Error en BD por favor revisar que exista Tabla"
                                            + l_Error , 3 );
                        }
                    }
                    else
                        Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion() Recuperando archivo de transacción " , l_Error , 3 );
                    //Por si se recupera informacion u algo mas
                    Globales.ReferenciaGSI = "";

                    if ( Properties.Settings.Default.Enviar_AlertasGSI )
                    {
                        System.Threading.Thread.Sleep(1500 );
                        UtilsComunicacion.AlertaGSI( 0 , "SID_OKAY" );

                    }
                }
                else if ( l_Sesion == 0 )
                // -- NO HAY SESIONES ABIERTAS
                {
                    Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "No hay cierres abruptos" , 3 );
                }
                else if ( l_Sesion < 0 )
                {
                    Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "Error obteniendo última sesión abierta" + l_Error , 3 );
                }
            }
            catch ( Exception E )
            {
                Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , E.Message , 3 );
            }


            // -- CERRAR SESIONES ABIERTAS ANTERIORES
            l_R = BDSesion.CerrarSesionesAnteriores( out l_Error );
            DateTime l_InicioSesion;
            l_R = BDSesion.AbrirNueva( out l_InicioSesion , out l_Error );

            if ( l_R <= 0 )
            {
                // Grabar error BD
                Globales.EscribirBitacora( "Inicio" , "Abrir Nueva Sesion"
                    , "Error en BD por favor revisar que exista Tabla: " + l_Error , 3 );
            }
            else
            {
                // Asignar variable global de # de sesion = l_R
                Globales.Numero_Sesion = l_R;
                Globales.EscribirBitacora( "Inicio" , "revisarSesion()" , "Inicio de Sesion exitoso, FolioSesion: "
                                + l_R + " Fecha Hora Inicio: " + UtilsBD.FormatoBDFechaHora( l_InicioSesion ) , 3 );
            }
        }

        private void Mover_Archivos_Recuperados( )
        {
            string targetPath = Application.StartupPath + "\\Recuperados";


            if ( !System.IO.Directory.Exists( targetPath ) )
            {
                System.IO.Directory.CreateDirectory( targetPath );
            }

            DirectoryInfo di = new DirectoryInfo( Application.StartupPath );
            FileInfo[] files = di.GetFiles( "*.json" )
                                 .Where( p => p.Extension == ".json" ).ToArray( );
            foreach ( FileInfo file in files )
                try
                {
                    string destFile = System.IO.Path.Combine( targetPath , file.Name );
                    File.Copy( file.FullName , destFile , true );
                    file.Attributes = FileAttributes.Normal;
                    File.Delete( file.FullName );
                }
                catch { }
        }

        private void Borar_Archivos_Cifrados( )
        {

            DirectoryInfo di = new DirectoryInfo( Application.StartupPath );
            FileInfo[] files = di.GetFiles( "*.ctx" )
                                 .Where( p => p.Extension == ".ctx" ).ToArray( );
            foreach ( FileInfo file in files )
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    File.Delete( file.FullName );
                }
                catch { }
        }



        private void ComprobarImpresora( )
        {
            try
            {
                string l_Error;
                if ( Properties.Settings.Default.No_Debug_Pc )
                {
                    int l_reply = SidApi.SidLib.SID_Open( false );
                    Globales.EscribirBitacora( "Comprobando Impresora" , "SID_OPEN(): " , l_reply.ToString( ) , 1 );

                    if ( l_reply == SidLib.SID_OKAY || l_reply == SidLib.SID_ALREADY_OPEN )
                    {
                        l_reply = SidLib.SID_PrinterGetStatus( );
                        Globales.EscribirBitacora( "Comprobando Impresora" , "Printer STATUS() :" , l_reply.ToString( ) , 1 );

                        if ( l_reply < 0 )
                        {
                            Error_Sid( l_reply , out l_Error );
                            c_comenzar.Enabled = false;
                          

                            using ( FormaError f_Error = new FormaError( true , "warning" ) )
                            {
                                f_Error.c_MensajeError.Text = "Revise La IMPRESORA de Este Equipo, por Favor avise al Administrador del Equipo: " + l_Error;
                                f_Error.ShowDialog( );

                            }

                        }
                    }

                    SidApi.SidLib.SID_Close( );

                }
                else
                {
                    c_comenzar.Enabled = true;
                   
                }
            }
            catch ( Exception exx )
            {
                Globales.EscribirBitacora( "Comprobando IMpresora" , "Excepcion Encontrada" , exx.Message , 1 );
            }

        }



        private bool ComprobarBolsa( )
        {
            if ( Globales.NumeroSerieBOLSA == "000000" || String.IsNullOrWhiteSpace( Globales.NumeroSerieBOLSA ) )
            {
                c_comenzar.Enabled = false;
               
                using ( FormaError f_Error = new FormaError( false , "Verificación de Envase" ) )
                {
                    f_Error.c_MensajeError.Text = "Es Necesario que exista un Envase Valido, por Favor avise al Administrador del Equipo";
                    f_Error.ShowDialog( );
                    //  c_BotonCancelar_Click(this, null);
                }


                return false;

            }
            else if ( !BDSide.BolsaSinRetiro( Globales.NumeroSerieBOLSA ) )
            {

                using ( FormaFueraServicio l_chek = new FormaFueraServicio( ) )
                {
                    l_chek.ShowDialog( );
                }

                return false;

            }
            else

            {
                c_comenzar.Enabled = true;
              

                return true;
            }

        }

        private void Uc_Bienvenida1_Click( object sender , EventArgs e )
        {
            c_cronometro.Enabled = false;

            using ( FormaOpciones l_menu = new FormaOpciones( true ) )
            {

                l_menu.ShowDialog( );
            }


            c_cronometro.Enabled = true;
        }

        private int ValidarDirectorio( )
        {
            if ( DateTime.Now.Day == 1 && !Properties.Settings.Default.EnviarCorreos )
            {
                try
                {
                    System.IO.File.Delete( Application.StartupPath + "\\SELLADO.TXT" );
                    System.IO.File.Delete( Application.StartupPath + "\\Transacciones.TXT" );
                    System.IO.File.Delete( Application.StartupPath + "\\TransaccionesAcumuladas.TXT" );
                }

                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Error al borrar Archivos de Registro: SELLADO y Transaciones Acumuladas" , "Error Borrando " , ex.Message , 1 );

                }
            }

            if ( File.Exists( "Sid.sys" ) )
            {
                String[] lineas = File.ReadAllLines( "Sid.sys" );

                if ( lineas.Length == 3 )
                {
                    Globales.UsuarioGSI = lineas[0];
                    Globales.ContraseñaGsi = lineas[1];
                    Globales.LlaveGsi = lineas[2];

                    if ( Properties.Settings.Default.Enviar_AlertasGSI )
                        UtilsComunicacion.AlertaGSI( 110 , " Inicializacion de Aplicativo " );

                    Globales.EscribirBitacora( "Lectura de Configuracion" , "Exitosa" , "Llaves Cargadas" , 1 );
                    Globales.GSI_Bitacora( "Lectura de Configuracion" , "Exitosa" , "Llaves Cargadas" , 1 );

                    return 1;
                }
                else
                {

                    Globales.EscribirBitacora( "Lectura de Configuracion" , "Fracaso" , "No se leyeron las llaves" , 1 );
                    Globales.GSI_Bitacora( "Lectura de Configuracion" , "Fracaso" , "No se leyeron las llaves" , 1 );
                    return 0;
                }
            }
            else
            {

                Globales.EscribirBitacora( "Lectura de Configuracion" , "Fracaso" , "No hay Archivo para lectura" , 1 );
                Globales.GSI_Bitacora( "Lectura de Configuracion" , "Fracaso" , "No hay Archivo para lectura" , 1 );
                return -1;
            }

        }

        private void c_cronometro_Tick( object sender , EventArgs e )
        {

            c_cronometro.Enabled = false;
            try
            {
                MensajePing2 l_ping = new MensajePing2( Properties.Settings.Default.Estacion );

                try
                {
                    String l_Json = l_ping.CodificarJson( );

                    String[] l_Servers = Properties.Settings.Default.Servers.Split( ',' );
                    foreach ( String l_Servidor in l_Servers )
                    {
                        int l_IdServidor = Convert.ToInt32( l_Servidor );
                        ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp( l_IdServidor );
                        l_Cola.AgregarMensaje( MensajeIrds.CategoriaMensajeEnum.Servicio
                       , MensajeIrds.TipoMensajeEnum.Servicio_Ping
                       , 2
                       , l_Json );

                        l_Cola.MandarMensajesPendientes( Properties.Settings.Default.Estacion );
                    }
                }
                catch ( Exception p )
                {
                    Globales.EscribirBitacora( "Exception AL procesar Menasajes Pendientes" , "Error" , p.Message , 1 );
                }

            }
            catch ( Exception exxx )
            {
                Globales.EscribirBitacora( "Exception AL procesar Menasajes Pendientes" , "Error" , exxx.Message , 1 );
                Globales.EscribirBitacora( "Exception AL procesar Ping de Conexion" , "Error" , exxx.Message , 1 );
            }



            DateTime l_Comprobar = DateTime.Now;

            if ( l_Comprobar.Hour == 6 && Envio )
            {
                String p_Error = "";

                
                int l_R = UtilsSid.MandarBitacoraAnterior( 1 , Properties.Settings.Default.Estacion , out p_Error );

                if ( l_R < 0 )
                {
                    Globales.EscribirBitacora( "Envio de Bitacora Vencida" , "UtilsSid.MandarBitacoraAnterior" , "Error mandar bitacora anterior : " + p_Error , 3 );
                }
                else
                {
                    Globales.EscribirBitacora( "Envio de Bitacora Vencida" , "UtilsSid.MandarBitacoraAnterior" , "Exito al mandar bitacora anterior : " + p_Error , 3 );
                    Envio = false;

                }

               

            }

            if ( l_Comprobar.Hour != 6 )
            {
                Envio = true;

            }








            c_cronometro.Enabled = true;
        }

        private void uc_Bienvenida1_Load( object sender , EventArgs e )
        {

        }

        private void c_Comenzar_Click( object sender , EventArgs e )
        {


            c_cronometro.Enabled = false;
            gsi_cronometro.Enabled = false;
            Globales.c_keepalive_G.Enabled = false;
            try
            {


                if ( Globales.Alerta_Ups_entrada )
                {
                    using ( FormaError f_Error = new FormaError( true , "warning" ) )
                    {

                        f_Error.c_MensajeError.Text = " No hay Alimentacion de Energia  , ...No Podra hacer DEPOSITOS...";
                        f_Error.ShowDialog( );

                    }
                }
                else if ( Globales.Estatus == Globales.EstatusReceptor.Mantenimiento )
                {
                    using ( FormaError f_Error = new FormaError( true , "warning" ) )
                    {

                        f_Error.c_MensajeError.Text = " Modo Mantenimiento , ...No Podra hacer DEPOSITOS...";
                        f_Error.ShowDialog( );

                    }


                }
                else
                {


                    if ( ComprobarBolsa( ) )
                    {
                        using ( FormCuenta l_usuario = new FormCuenta( true ) )
                        {
                            l_usuario.ShowDialog( );
                        }

                        try
                        {
                            CalcularStock( );

                        }
                        catch ( Exception exx )
                        {
                            Globales.EscribirBitacora( "Comprobando Capacidad" , "Excepcion Encontrada" , exx.Message , 1 );
                        }
                    }
                }
            }
            catch ( Exception exx )
            {

            }

            //using (FormaOpciones l_menu = new FormaOpciones(true))
            //{

            //    l_menu.ShowDialog();
            //}



            Application.DoEvents( );

           // gsi_cronometro_Tick( null , null );

            gsi_cronometro.Enabled = true;
            c_cronometro.Enabled = true;


        }

        private void c_etv_Click( object sender , EventArgs e )
        {
            c_cronometro.Enabled = false;
            Globales.c_keepalive_G.Enabled = false;
            using ( FormaIniciarSesion f_Inicio = new FormaIniciarSesion( ) )
            {

                f_Inicio.l_IdUsuario = "ETV_Traslado";
                f_Inicio.ShowDialog( );
            }




            try
            {

                ComprobarBolsa( );
            }
            catch ( Exception exx )
            {

            }

            try
            {

                CalcularStock( );

            }
            catch ( Exception exx )
            {
                Globales.EscribirBitacora( "Comprobando IMpresora" , "Excepcion Encontrada" , exx.Message , 1 );
            }



            c_cronometro.Enabled = true;
        }

        private void c_Admin_Click( object sender , EventArgs e )
        {
            c_cronometro.Enabled = false;
            Globales.c_keepalive_G.Enabled = false;
            // Nueva VEntana para elegir PErfil o Usuario a Loguear --


            using ( FormaPerfil f_Inicio = new FormaPerfil(true ) )
            {

               
                f_Inicio.ShowDialog( );
            }

            try
            {

                ComprobarBolsa( );
            }
            catch ( Exception exx )
            {

            }

            try
            {
                CalcularStock( );

            }
            catch ( Exception exx )
            {
                Globales.EscribirBitacora( "Comprobando Stock" , "Excepcion Encontrada" , exx.Message , 1 );
            }


            c_cronometro.Enabled = true;
        }

        public void Error_Sid( int l_Reply , out String p_Error )
        {
            String l_Error = "";

            SidLib.Error_Sid( l_Reply , out l_Error );
            p_Error = l_Error;

            if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {
                if ( l_Reply != SidLib.SID_JAM_IN_FEEDER_IN && l_Reply != SidLib.SID_POWER_OFF )
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply != 0 && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                        UtilsComunicacion.AlertaGSI( l_Reply , l_Error );

                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }
            }


        }

        private void ProbarSID( )
        {
            Globales.EscribirBitacora( "Inicio" , "Modo Debug?: " , Properties.Settings.Default.No_Debug_Pc.ToString( ) , 1 );

            if ( Properties.Settings.Default.No_Debug_Pc )
            {
                int l_Reply = -1;
                String l_Error;
                int l_Reintentos = 0;
                DataTable l_datos = null;


                try
                {
                    SidLib.ResetError( );

                    l_Reply = SidLib.SID_Open( Properties.Settings.Default.SalvarImagenes );

                    SidLib.Dll_Configuration l_configuration = new SidLib.Dll_Configuration( );
                    l_Reply = SidLib.GetDLLVersion( ref l_configuration , ref l_datos );

                    foreach ( DataRow linea in l_datos.Rows )
                        Globales.EscribirBitacora( "DLLVersion" , "Caracteristica :" + linea[0].ToString( ) , linea[1].ToString( ) , 1 );
                    l_Version_Doc = l_datos.Rows[7][1].ToString( ) + " FW Version:" + l_datos.Rows[0][1].ToString( );
                    if ( l_configuration.Door_Sensor )
                        Globales.EscribirBitacora( "DLLVersion" , "\r\n Door Sensor" , " Present " , 1 );
                    if ( l_configuration.Lock_Sensor )
                        Globales.EscribirBitacora( "DLLVersion" , "\r\n lock Sensor" , " Present" , 1 );
                    if ( l_configuration.Uv_Sensor )
                        Globales.EscribirBitacora( "DLLVersion" , "\r\n Uv SENSOr" , " Present " , 1 );
                    if ( l_configuration.Magnetic_18Channel )
                        Globales.EscribirBitacora( "DLLVersion" , "\r\n MICR" , " Present " , 1 );

                    l_Reply = SidLib.SID_Initialize( );
                    Globales.EscribirBitacora( "Inicio" , "Inicializando: " , l_Reply.ToString( ) , 1 );

                    //if ( Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                    Error_Sid( l_Reply , out l_Error );

                    if ( l_Reply < 0 )
                    {


                        using ( FormaError f_Error = new FormaError( true , "warning" ) )
                        {
                            f_Error.c_MensajeError.Text = l_Error;
                            f_Error.ShowDialog( );
                            //  c_BotonCancelar_Click(this, null);
                        }
                    }
                    else
                    {
                        if ( Globales.Estatus == Globales.EstatusReceptor.Desconocido )
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                    }


                    SidLib.SID_Close( );
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Inicio" , "GetIdentyfy: " , ex.Message + l_Reply.ToString( ) , 1 );
                }
                //try
                //{
                //    do
                //    {

                //        if ( l_Reintentos >= 3 )
                //            break;
                //        l_Reply = SidLib.ResetPath();
                //        l_Reintentos++;
                //    } while ( l_Reply != 0 );
                //}
                //catch ( Exception ex )
                //{
                //    Globales.EscribirBitacora( "Inicio", "RESET: ", ex.Message + l_Reply.ToString(), 1 );
                //}
                l_Reintentos = 0;
                try
                {
                    do
                    {
                        if ( l_Reintentos >= 3 )
                            break;
                        l_Reply = SidLib.ResetError( );
                        l_Reintentos++;
                    } while ( l_Reply != 0 );
                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "Inicio" , "Reset Software Error: " , ex.Message + l_Reply.ToString( ) , 1 );
                }




                try
                {
                    l_Reply = SidLib.SID_Open( true );

                    StringBuilder l_cadena = new StringBuilder( 64 );

                    l_Reply = SidLib.SID_GetVersion( l_cadena , 64 );
                    if ( l_Reply == SidLib.SID_OKAY )
                        Globales.EscribirBitacora( "ProbarFunciones" , "GetVersion" , "Llama función, return = " + l_cadena.ToString( ) , 1 );
                    else
                    {
                        Error_Sid( l_Reply , out l_Error );
                        Globales.EscribirBitacora( "NS_GetIdentify" , "Retorno = " + l_Reply , l_Error , 1 );
                    }
                    l_VersionDll = l_cadena.ToString( );
                    l_Reply = SidLib.SID_Close( );

                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "ProbarFunciones" , "NS_GetDLLVersion" , "ERROR: " + ex.ToString( ) , 1 );
                }



                try
                {
                    if ( Properties.Settings.Default.Port_YUGO > 0 )
                    {
                        U_Dep l_Receptor = new U_Dep( );
                        byte[] l_Puerto = new byte[4];
                        int l_R = l_Receptor.DLLU_DEP_ConnectAutoCOM( l_Puerto );

                        SymetryDevices.UCoin.cIncializarUCOIN( Properties.Settings.Default.Port_YUGO );
                        l_R = SymetryDevices.UCoin.cgetStatus( Properties.Settings.Default.Port_YUGO );

                        if ( l_R == ErroresUcoin.Codigos.OKAY )
                        {
                            Globales.EscribirBitacora( "ProbarFunciones" , "UCOIN: " , "Equipo encontrado y listo " , 1 );
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Equipo2_Estatus == Globales.EstatusReceptor.No_Operable )
                            {
                                UtilsComunicacion.AlertaGSI( -8000 , "Ucoin_OKAY" );

                            }
                            Globales.Equipo2_CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                        }
                        else
                        {
                            string l_descripcion;
                            int l_Erroru;

                            ErroresUcoin.Error_Ucoin( l_R , out l_Erroru , out l_descripcion );
                            Globales.EscribirBitacora( "ProbarFunciones" , "UCOIN: " , "ERROR: No esta El Equipo listo: " + l_Erroru , 1 );
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Equipo2_Estatus != Globales.EstatusReceptor.No_Operable )
                            {
                                UtilsComunicacion.AlertaGSI( l_Erroru , l_descripcion );
                                Globales.Equipo2_CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                            }
                        }

                    }
                    else
                        Globales.Equipo2_Estatus = Globales.EstatusReceptor.Desconocido;


                }
                catch ( Exception ex )
                {
                    Globales.EscribirBitacora( "ProbarFunciones" , "UCOIN: " , "ERROR: " + ex.ToString( ) , 1 );
                }
            }

            if (Globales.Estatus == Globales.EstatusReceptor.Operable)
                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                    UtilsComunicacion.AlertaGSI( 0 , "SID_OK");

        }

        int l_vecesalerta;


        private void gsi_cronometro_Tick( object sender , EventArgs e )
        {
            gsi_cronometro.Enabled = false;


            if ( !bwsensado.IsBusy )
            {
                bwsensado.RunWorkerAsync( );
            }






        }

        private void TransacionesPendientesGSI( DataTable l_pendientes )
        {
           
            try
            {
                if (l_pendientes.Rows.Count > 0)
                {
                    string l_FolidID;
                    string l_Cuenta;
                    string l_GSICajero;
                    string l_Divisa;
                    string l_fecha;
                    string l_hora;
                    string l_referencia;
                    string l_envase;
                    string l_Monto;

                    DateTime l_Fecha_Envio;
                    DateTime l_Fecha_Valida = DateTime.Parse(l_pendientes.Rows[0][5].ToString());
                    //formaActualizarDepositos.ShowDialog();
                    for (int i = 0; i < l_pendientes.Rows.Count; i++)
                    {
                        DataTable l_Detalle;
                        Globales.GsiDeposito = null;

                        l_FolidID = l_pendientes.Rows[i][0].ToString();
                        l_Cuenta = l_pendientes.Rows[i][1].ToString();
                        l_GSICajero = l_pendientes.Rows[i][2].ToString();
                        l_Divisa = l_pendientes.Rows[i][3].ToString();
                        l_fecha = l_pendientes.Rows[i][5].ToString();
                        l_Monto = l_pendientes.Rows[i][4].ToString();
                        l_hora = l_pendientes.Rows[i][6].ToString();
                        l_referencia = l_pendientes.Rows[i][7].ToString();
                        l_envase = l_pendientes.Rows[i][8].ToString();

                        l_Fecha_Envio = DateTime.Parse(l_fecha);

                        if (l_Fecha_Valida.CompareTo(l_Fecha_Envio) > 0)
                            break;


                        l_Detalle = BDDeposito.ObtenerDetalleDeposito(Int32.Parse(l_FolidID));
                        // string l_usuario =BDDeposito.ObtenerUsuarioDeposito( Int32.Parse( l_FolidID ) );




                        if (l_Detalle != null)
                            Globales.GsiDeposito = UtilsComunicacion.PagoReferenciadoGSI(l_Monto, Globales.claveEmisora, "GSI901", "MXP", Convert.ToDateTime(l_fecha).ToString("yyyy-MM-dd"), Convert.ToDateTime(l_hora).ToString("HH:mm:ss")
                                                                   , Int32.Parse(l_FolidID).ToString("D20"), l_referencia, l_envase, l_Detalle);
                        //Globales.GsiDeposito = UtilsComunicacion.DepositoGsi( l_Cuenta , l_GSICajero , l_Divisa , l_fecha , l_hora , Int32.Parse( l_FolidID ).ToString( "D20" )
                        //                                  , l_referencia , l_envase , l_Detalle,null );

                        if (Globales.GsiDeposito != null)
                        {
                            if (Convert.ToBoolean(UtilsComunicacion.Desencriptar(Globales.GsiDeposito.exito)) == true)
                            {

                                if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo)))
                                {
                                    Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "Exito al reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);

                                    BDDeposito.UpdateDepositoGSI(Int32.Parse(l_FolidID));
                                }
                            }
                            else
                            {
                                if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.mensaje)))
                                {
                                    Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "Exito al reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);

                                    BDDeposito.UpdateDepositoGSI(Int32.Parse(l_FolidID));
                                }
                            }
                        }
                        else
                            Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "No se pudo reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);

                    }

                }
                //formaActualizarDepositos.Close();
            }
            catch
            {
                //formaActualizarDepositos.Close();
            }
        }

        private void TransacionesPendientesRetirosGSI( DataTable l_pendientes )
        {
            if ( l_pendientes.Rows.Count > 0 )
            {
                string l_FolidID;
                string l_GSICajero;
                string l_Divisa;
                string l_fecha;
                string l_envase;

                DateTime l_Fecha_Envio;
                DateTime l_Fecha_Valida = DateTime.Parse( l_pendientes.Rows[0]["Fecha"].ToString( ) );


                for ( int i = 0; i < l_pendientes.Rows.Count; i++ )
                {

                    DataTable l_Detalle;
                    Globales.GsiRecoleccion = null;

                    l_FolidID = l_pendientes.Rows[i]["IdRetiro"].ToString( );
                    l_GSICajero = l_pendientes.Rows[i]["GsiCajero"].ToString( );
                    l_Divisa = l_pendientes.Rows[i]["Divisa"].ToString( );
                    l_fecha = l_pendientes.Rows[i]["Fecha"].ToString( );
                    l_envase = l_pendientes.Rows[i]["Envase"].ToString( );

                    l_Fecha_Envio = DateTime.Parse( l_fecha );

                    if ( l_Fecha_Valida.CompareTo( l_Fecha_Envio ) > 0 )
                        break;

                    l_Detalle = BDRetiro.ObtenerDetalleRetiro( Int32.Parse( l_FolidID ) );


                    Globales.GsiRecoleccion = UtilsComunicacion.RecoleccionGsi( l_GSICajero , l_Divisa , l_fecha , Int32.Parse( l_FolidID ).ToString( "D20" )
                                                     , l_envase , l_Detalle );

                    if ( Globales.GsiRecoleccion != null )
                    {
                        BDRetiro.UpdateRetiroGSI( Int32.Parse( l_FolidID ) );
                        Globales.EscribirBitacora( "Transaccion GSI Pendiente" , "Reenvio Recoleccion" , "Exito al reenviar Transaccion GSI Folio Operativo: " + l_FolidID , 3 );

                    }
                    else
                        Globales.EscribirBitacora( "Transaccion GSI Pendiente" , "Reenvio Recoleccion" , "No se pudo reenviar Transaccion GSI Folio Operativo: " + l_FolidID , 3 );

                }


            }
        }

        private void AlertasPendientesGSI( DataTable l_pendientes )
        {
            if ( l_pendientes.Rows.Count > 0 )
            {
                int l_folio = 0;
                string l_fecha;
                string l_GSICajero;
                string l_clave;
                string l_descripcion;
                string l_ticket;

                for ( int i = 0; i < l_pendientes.Rows.Count; i++ )
                {


                    l_folio = (int) l_pendientes.Rows[i][0];
                    l_fecha = l_pendientes.Rows[i][1].ToString( );
                    l_GSICajero = l_pendientes.Rows[i][2].ToString( );
                    l_clave = l_pendientes.Rows[i][3].ToString( );
                    l_descripcion = l_pendientes.Rows[i][4].ToString( );
                    l_ticket = l_pendientes.Rows[i][5].ToString( );



                    if ( UtilsComunicacion.AlertaGSILineal( l_fecha , l_GSICajero , l_clave , l_descripcion , l_ticket ) )
                    {

                        if ( BDGsi.UpdateMensaje( l_folio , true ) )
                            Globales.EscribirBitacora( "Alerta GSI Pendiente" , "Reenvio Alerta" , "Exito al reenviar Alerta GSI Codigo Operativo: " + l_clave , 3 );
                    }
                    else
                        Globales.EscribirBitacora( "Alerta GSI Pendiente" , "Reenvio de Alerta" , "No se pudo reenviar Alerta GSI Codigo Operativo: " + l_clave , 3 );

                }

            }
        }

        private void CalcularStock( )
        {
            Double l_totalBilletes;
            Double l_numbilletesenbolsa = BDDeposito.ObtenerTotalBilletes( );
            if ( Properties.Settings.Default.Port_YUGO > 0 )
            {
                Double l_totalMonedas;
                Double l_numMonedasbolsa = BDDeposito.ObtenerTotalMonedas( );

                if ( l_numMonedasbolsa == 0 )
                {
                    c_stokMonedas.Text = "0.00 %";
                }
                else
                {
                    Double l_decimalMonedas = ( l_numMonedasbolsa / Properties.Settings.Default.CapacidadMonedas );
                    l_totalMonedas = l_numMonedasbolsa;
                    l_numMonedasbolsa = ( l_decimalMonedas * 100 );
                    c_stokMonedas.Text = l_numMonedasbolsa.ToString( "###.##" ) + "%";
                }
            }


            if ( Globales.Estatus == Globales.EstatusReceptor.Lleno )
            {
                Globales.Alertastock_Equipo_lleno = true;
                Globales.Alertastock_enviada = true;
            }

            if ( l_numbilletesenbolsa == 0 )
            {
                Globales.Alertastock_enviada = false;
                Globales.Alertastock_Equipo_lleno = false;
                label2.Text = "0.00 %";

                if ( Properties.Settings.Default.No_Debug_Pc )
                {
                    // 18-07-2017  Stock

                    if ( Globales.Estatus != Globales.EstatusReceptor.No_Operable && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
                    {
                        Globales.Estatus = Globales.EstatusReceptor.Desconocido;
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                        UtilsComunicacion.AlertaGSI( 0 , "SID_OKAY" );
                    }
                }

            }
            else
            {
                Double l_decimal = ( l_numbilletesenbolsa / Properties.Settings.Default.CapacidadBolsa );


                l_totalBilletes = l_numbilletesenbolsa;
                l_numbilletesenbolsa = ( l_decimal * 100 );
                label2.Text = l_numbilletesenbolsa.ToString( "###.##" ) + "%";


                if ( l_numbilletesenbolsa > 79 && !Globales.Alertastock_enviada )
                {
                    Registrar.AlertaSTOCK( );
                    if ( Properties.Settings.Default.Enviar_AlertasGSI )
                        UtilsComunicacion.AlertaGSI( 80 , "Equipo al 80% del Cupo: #" + l_totalBilletes + "piezas" );
                    Globales.Alertastock_enviada = true;
                }



                if ( l_numbilletesenbolsa > 99.95 )
                {
                    c_comenzar.Enabled = false;
                   
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alertastock_Equipo_lleno )
                        UtilsComunicacion.AlertaGSI( 100 , "Equipo al " + l_numbilletesenbolsa + "% del Cupo: #" + l_totalBilletes + "piezas" );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Lleno );
                    Globales.Alertastock_Equipo_lleno = true;
                }
                else
                {
                    c_comenzar.Enabled = true;
                   
                }
            }


        }

        private bool CheckUPs( )
        {


            if ( Properties.Settings.Default.No_Debug_Pc )
            {

                bool l_upsON = false;
                bool l_Low_batery = false;
                bool Reserved = false;
                String l_Errores;

                int l_reply = SidLib.SID_Open( false );

                if ( l_reply == SidLib.SID_OKAY || l_reply == SidLib.SID_ALREADY_OPEN )
                {

                    l_reply = SidLib.SID_UPSStatus( ref l_upsON , ref l_Low_batery , ref Reserved );

                    if ( l_reply < 0 )
                    {

                        Error_Sid( l_reply , out l_Errores );

                        if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada
                                     && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                            UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                        Globales.Alerta_Ups_entrada = true;
                        Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );
                        //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                        //{
                        //    f_Error.c_MensajeError.Text = "Malfuncionamiento. Causa : UPS " + l_Errores;
                        //    f_Error.ShowDialog( );
                        //}
                        return false;
                    }
                    else
                    {
                        if ( !l_upsON )
                        {
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                                UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                            Globales.Alerta_Ups_entrada = true;
                            Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );

                            //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                            //{
                            //    f_Error.c_MensajeError.Text = "Deposito No Posible UPS APAGADO";
                            //    f_Error.ShowDialog( );
                            //}
                            return false;
                        }
                        if ( l_Low_batery )
                        {
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada
                                 && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                                UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                            Globales.Alerta_Ups_entrada = true;
                            Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );

                            //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                            //{
                            //    f_Error.c_MensajeError.Text = "Deposito No Posible UPS BATERIA Descargada";
                            //    f_Error.ShowDialog( );
                            //}
                            return false;
                        }

                       
                    }

                    SidLib.SID_Close( );
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Alerta_Ups_entrada )
                        UtilsComunicacion.AlertaGSI( 96 , "Salida de UPS" );
                    Globales.Alerta_Ups_entrada = false;
                    Globales.UPS_CambioEstatus( Globales.EstatusReceptor.Operable );
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                return true;

        }

        private void FormaPrincipal_FormClosing( object sender , FormClosingEventArgs e )
        {

            String l_error;
            BDSesion.CerrarSesion( Globales.Numero_Sesion , out l_error );
            Globales.EscribirBitacora( "FormaPrincipal" , "Closing " , "Cerrando Sesion: " + l_error , 3 );

            //if ( Globales.Equipo2_Estatus == Globales.EstatusReceptor.No_Operable )
            //    if ( Properties.Settings.Default.Enviar_AlertasGSI )
            //    {

            //        UtilsComunicacion.AlertaGSI(-8000  , "UCoin_OK" );
            //    }


            //enviar mensaje de Cierre Pc
            if ( Globales.Estatus != Globales.EstatusReceptor.Apagando )
            {
                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                    UtilsComunicacion.AlertaGSI( 93 , "Cierre de Aplicativo por Apagado de PC" );
            }


        }

        public static bool SensadoEquipo( )
        {
            bool l_R = true;
            Byte[] l_Key = new Byte[1];
            Byte[] l_Sensor = new Byte[64];
            String l_Error = "";
            String l_ip = "";

            bool l_SinPapel, l_SensorBoveda_Abierta, l_Boveda_Abierta, l_SinBolsa, l_DialAbierto, l_TapaAbierta, l_DipShiwtch, l_HayAtasco, l_sinRed;

            l_SinPapel = l_SensorBoveda_Abierta = l_Boveda_Abierta = l_SinBolsa = l_DialAbierto = l_TapaAbierta = l_DipShiwtch = l_HayAtasco = l_sinRed = false;



            try
            {
                SidLib.ResetError( );
                System.Threading.Thread.Sleep( 100 );

                SensarSIDcs.UnitStatus l_Status = SensarSIDcs.GetStatus( );

                System.Threading.Thread.Sleep( 100 );
                int l_reply = SidLib.SID_PrinterGetStatus( );


                SidLib.SID_Close( );

                if ( l_Status == null )
                    return false;

                String l_Errores = "";

                if ( l_reply < 0 )
                {
                    l_SinPapel = true;
                    l_Error = "No hay Rollo de Papel";
                    l_Errores += l_Error + ",  ";
                    l_R = false;
                }

                try
                {
                    l_ip = GetLocalIPAddress( );
                }
                catch ( Exception )
                {
                    l_ip = "127.0.0.1";
                }




                if ( !l_ip.Contains( "127.0.0.1" ) && !String.IsNullOrEmpty( l_ip ) )
                {

                }
                else
                {

                    l_sinRed = true;
                    l_Error = "No hay Red Disponible";

                    l_Errores += l_Error + ",  ";
                    l_R = false;

                }



                if ( !( ( l_Status.m_Sensor_bag_present == true )
                   // && ( l_Status.m_Dip_Switch_elevator_position_DOWN == true )
                    && ( l_Status.m_Sensor_bag_ready_for_deposit == true ) ) )
                {
                    l_SinBolsa = true;
                    l_Error = "Bolsa No Presente";
                    //  UtilsComunicacion.AlertaGSI(SidLib.SID_BAG_NOT_PRESENT, l_Error; );
                    //      Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_Errores += l_Error + ",  ";
                    l_R = false;
                }

                if ( l_Status.m_Code_lock_correct )
                {
                    l_DialAbierto = true;
                    l_Error = "Dial Abierto";
                    //   UtilsComunicacion.AlertaGSI(SidLib.SID_LOCK_ERROR, l_Error);
                    //    Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }

                if ( l_Status.m_Interlock_cover_head_open )
                {
                    l_TapaAbierta = true;
                    l_Error = "Tapa Abierta";
                    // UtilsComunicacion.AlertaGSI(SidLib.SID_INTERLOCK_OPEN, l_Error);
                    //     Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }

                if ( !l_Status.m_Interlock_door_strongbox_closed )
                {
                    l_Boveda_Abierta = true;
                    l_Error = "Boveda abierta";
                    //   UtilsComunicacion.AlertaGSI(SidLib.SID_INTERLOCK_DOOR_OPEN, l_Error);
                    //     Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }

                if ( l_Status.m_Sensor_door_open )
                {
                    l_SensorBoveda_Abierta = true;
                    l_Error = "Sensor de Boveda Abierto";
                    //  UtilsComunicacion.AlertaGSI(SidLib.SID_SENSOR_DOOR_OPEN, l_Error);
                    // Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }
                if ( l_Status.m_Sensor_Dip_Switch_Leafer_open )
                {
                    l_DipShiwtch = true;
                    l_Error = "DIP Switch Leafer abierto";
                    // UtilsComunicacion.AlertaGSI(SidLib.SID_DIP_SWITCH_DOOR_LEAFER_OPEN, l_Error);
                    // Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }

                if ( l_Status.m_Sensor_Dip_Switch_Magnetic_door_1_open || l_Status.m_Sensor_Dip_Switch_Magnetic_door_2_open )
                {
                    l_DipShiwtch = true;
                    l_Error = "DIP Switch Magnetico Abierto";
                    // UtilsComunicacion.AlertaGSI(SidLib.SID_DIP_SWITCH_SP_MAGN1_OPEN, l_Error);
                    // Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                    l_R = false;
                    l_Errores += l_Error + ",  ";
                }


                if ( l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Paper_jam
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_never_covered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_not_uncovered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_exit_feeder_sensor_not_uncovered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_feeder_sensor_not_covered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_front_scanners
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Reject_bay_sensor_not_uncovered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_switch_path_sensor_not_covered
            | l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_withdraw_pocket_sensor_not_covered
            )
                {

                    SidLib.ResetPath( );

                    SensarSIDcs.UnitStatus l_Statusv = SensarSIDcs.GetStatus( );

                    if ( l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Paper_jam
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_never_covered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_not_uncovered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_exit_feeder_sensor_not_uncovered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_feeder_sensor_not_covered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_front_scanners
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Reject_bay_sensor_not_uncovered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_switch_path_sensor_not_covered
            | l_Statusv.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_withdraw_pocket_sensor_not_covered
            )
                    {
                        l_HayAtasco = true;
                        l_Error = "Hay Atasco";
                        // UtilsComunicacion.AlertaGSI(SidLib.SID_DIP_SWITCH_SP_MAGN1_OPEN, l_Error);
                        // Globales.EscribirBitacora("ProcesosSid", "SensadoEquipoListoDeposito()", l_Error, 1);
                        l_R = false;
                        l_Errores += l_Error + ",  ";
                    }
                }


                if ( l_R != true )
                {

                    if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
                    {

                        if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                            UtilsComunicacion.AlertaGSI( -9996 , "Equipo No Operable: " + l_Errores );

                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );

                    }

                    //using ( FormaError f_Error = new FormaError( true, "malFucionamiento" ) )
                    //{
                    //    f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde. Causa : " + l_Errores;
                    //    f_Error.ShowDialog();
                    //}

                    using ( FormaFueraServicio l_chek = new FormaFueraServicio( !l_DialAbierto , !l_Boveda_Abierta | !l_SensorBoveda_Abierta , !l_SinBolsa
                                                                                , !l_TapaAbierta , !l_sinRed , l_HayAtasco , l_SinPapel , l_DipShiwtch ) )
                    {
                        l_chek.TopMost = true;
                        l_chek.ShowDialog( );
                    }

                }
                else
                {
                    if ( Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                    {
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                        if ( Properties.Settings.Default.Enviar_AlertasGSI )
                            UtilsComunicacion.AlertaGSI( SidLib.SID_OKAY , "SID_OKAY" );
                    }

                }
                Globales.EscribirBitacora( "ProcesosSid" , "SensadoEquipoListoDeposito() " , l_Errores , 1 );




                //UCOIN SENSADO

                if ( Properties.Settings.Default.Port_YUGO > 0 )
                {

                    int l_statusU = UCoin.cgetStatus( Properties.Settings.Default.Port_YUGO );
                    int l_nuevoError;


                    ErroresUcoin.Error_Ucoin( l_statusU , out l_nuevoError , out l_Error );

                    if ( l_statusU != 0 ) // Equipo 2 status indepeientes
                    {
                        Globales.EscribirBitacora( "SensadoEquipoListo UCOIN" , "Error en Ucoin" , l_Error , 1 );
                        if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Equipo2_Estatus != Globales.EstatusReceptor.No_Operable )
                        {
                            UtilsComunicacion.AlertaGSI( l_nuevoError , l_Error );
                            Globales.Equipo2_CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                        }

                    }
                    else
                    {
                        ErroresUcoin.Error_Ucoin( 0 , out l_nuevoError , out l_Error );
                        if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Equipo2_Estatus == Globales.EstatusReceptor.No_Operable )
                            UtilsComunicacion.AlertaGSI( l_nuevoError , l_Error );
                        Globales.Equipo2_CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                    }
                }
            }
            catch ( Exception E )
            {
                Globales.EscribirBitacora( "SensadoEquipoListoDeposito()" , "EXCEPTION" , E.Message , 1 );
                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                    UtilsComunicacion.AlertaGSI( -9999 , "Error de Software" );
                l_R = false;
            }


            return l_R;
        }



        public static string GetLocalIPAddress( )
        {
            var host = System.Net.Dns.GetHostEntry( System.Net.Dns.GetHostName( ) );
            foreach ( var ip in host.AddressList )
            {
                if ( ip.AddressFamily == AddressFamily.InterNetwork )
                {
                    return ip.ToString( );
                }
            }
            throw new Exception( "IP Local NO Encontrada" );
        }

        private void c_keepalive_Tick( object sender , EventArgs e )
        {
            try
            {
                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                {

                    UtilsComunicacion.KeepaliveGSI( (int) Globales.Estatus );


                    Globales.GSI_Bitacora( "Principal" , "KeepAlive " , "SID: " + Globales.Estatus , 1 );

                  
                }

            }
            catch ( Exception excc )
            {
                Globales.GSI_Bitacora( "Principal" , "KeepAlive Excepcion" , "SID: " + Globales.Estatus + "Excepción: " + excc , 1 );
            }

            if (!Globales.EnDeposito_ahora)
            try
            {
                    //Ping Pings = new Ping();
                    //int timeout = 10000;
                    //PingReply l_pingreply = Pings.Send("www.yahoo.com", timeout);

                  if(Globales.onLine)
                    {
                        DataTable t_Depositos_Pendientes = BDConsulta.ObtenerDepositosGSIPendientes();
                        if (t_Depositos_Pendientes.Rows.Count > 0)
                        {                            
                            Globales.c_keepalive_G.Enabled = false;
                            //TransacionesPendientesGSI( t_Depositos_Pendientes );
                            FormaActualizarDepositos formaActualizarDepositos = new FormaActualizarDepositos();
                            formaActualizarDepositos.TopMost=true;
                            formaActualizarDepositos.ShowDialog();

                            
                        }
                        else
                        {
                            DataTable t_Retiros_Pendientes = BDConsulta.ObtenerRetirosGSIPendientes();
                            if (t_Retiros_Pendientes.Rows.Count > 0)
                                TransacionesPendientesRetirosGSI(t_Retiros_Pendientes);
                        }


                        DataTable t_Alertas_Pendientes = BDGsi.ObtenerMensajesGSIPendientes();
                        if (t_Alertas_Pendientes.Rows.Count > 0)
                            AlertasPendientesGSI(t_Alertas_Pendientes);
                    }

                   

            }
            catch ( Exception excc )
            {
                Globales.GSI_Bitacora( "Principal" , "Depositos Pendientes " , "Excepción: " + excc , 1 );
            }
        }

        private void bwsensado_DoWork( object sender , DoWorkEventArgs e )
        {

            String l_Error = "";


            if ( Properties.Settings.Default.No_Debug_Pc )
            {
                try
                {

                    bool Equipo_operable = SensadoEquipo( );



                    if ( !Equipo_operable && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
                    {


                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                        l_vecesalerta = 0;

                    }

                    if ( Equipo_operable && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                    {

                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                        l_vecesalerta = 0;

                    }

                    if ( Equipo_operable && Globales.Estatus == Globales.EstatusReceptor.Operable )
                    {
                        l_vecesalerta++;

                        if ( l_vecesalerta > 12 )
                        {
                            Error_Sid( 0 , out l_Error );
                            if ( Properties.Settings.Default.Enviar_AlertasGSI ) //envio forzado ya que el Equipo esta Operable
                                UtilsComunicacion.AlertaGSI( 0 , l_Error );
                            l_vecesalerta = 0;
                        }

                    }

                    Globales.GSI_Bitacora( "Estatus del Equipo" , "SENSADO EQUIPO OPERABLE ?: " , Equipo_operable.ToString( ) , 1 );

                  CheckUPs( );

                }
                catch ( Exception exception )
                {
                    SidLib.SID_Close( );
                    Globales.GSI_Bitacora( "Estatus del Equipo" , "SENSADO EQUIPO OPERABLE ?: " , " Excepcion encontrada" + " : " + exception , 1 );
                }

            }


        }

        private void bwsensado_RunWorkerCompleted( object sender , RunWorkerCompletedEventArgs e )
        {
            if ( ( e.Cancelled == true ) )
            {
                gsi_cronometro.Enabled = true;
            }

            else if ( !( e.Error == null ) )
            {
                gsi_cronometro.Enabled = true;
            }

            else
            {
                gsi_cronometro.Enabled = true;
            }

        }

        private void pictureBox2_Click( object sender , EventArgs e )
        {
            //using (FormaFueraServicio l_fuera = new FormaFueraServicio())
            //{
            //    l_fuera.ShowDialog();

            //}
        }



    }
}
