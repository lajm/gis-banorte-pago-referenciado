﻿namespace Azteca_SID
{
    partial class FormBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBase));
            this.c_cierreAutomatico = new System.Windows.Forms.Timer(this.components);
            this.c_imagenes = new System.Windows.Forms.ImageList(this.components);
            this.c_opcionSalir = new System.Windows.Forms.PictureBox();
            this.uc_PanelInferior1 = new Azteca_SID.uc_PanelInferior();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_opcionSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // c_cierreAutomatico
            // 
            this.c_cierreAutomatico.Interval = 30000;
            this.c_cierreAutomatico.Tick += new System.EventHandler(this.c_cierreAutomatico_Tick);
            // 
            // c_imagenes
            // 
            this.c_imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("c_imagenes.ImageStream")));
            this.c_imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.c_imagenes.Images.SetKeyName(0, "correcto");
            this.c_imagenes.Images.SetKeyName(1, "importante");
            this.c_imagenes.Images.SetKeyName(2, "precaucion");
            this.c_imagenes.Images.SetKeyName(3, "informacion");
            this.c_imagenes.Images.SetKeyName(4, "preguntaroja");
            this.c_imagenes.Images.SetKeyName(5, "preguntaAmarilla");
            this.c_imagenes.Images.SetKeyName(6, "incorrecto");
            this.c_imagenes.Images.SetKeyName(7, "reloj");
            this.c_imagenes.Images.SetKeyName(8, "tiempo");
            this.c_imagenes.Images.SetKeyName(9, "paloma");
            this.c_imagenes.Images.SetKeyName(10, "error");
            this.c_imagenes.Images.SetKeyName(11, "informacion2");
            this.c_imagenes.Images.SetKeyName(12, "pregunta");
            this.c_imagenes.Images.SetKeyName(13, "warning");
            this.c_imagenes.Images.SetKeyName(14, "precaucion2");
            this.c_imagenes.Images.SetKeyName(15, "esperar");
            this.c_imagenes.Images.SetKeyName(16, "mensaje");
            // 
            // c_opcionSalir
            // 
            this.c_opcionSalir.BackColor = System.Drawing.Color.Transparent;
            this.c_opcionSalir.Location = new System.Drawing.Point(686, 1);
            this.c_opcionSalir.Name = "c_opcionSalir";
            this.c_opcionSalir.Size = new System.Drawing.Size(102, 53);
            this.c_opcionSalir.TabIndex = 1;
            this.c_opcionSalir.TabStop = false;
            this.c_opcionSalir.Click += new System.EventHandler(this.c_opcionSalir_Click);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 0;
            this.uc_PanelInferior1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Azteca_SID.Properties.Resources.LOGOGSI;
            this.pictureBox1.Location = new System.Drawing.Point(344, 558);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox2.Location = new System.Drawing.Point(335, 523);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(130, 38);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // FormBase
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.c_opcionSalir);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormBase";
            ((System.ComponentModel.ISupportInitialize)(this.c_opcionSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
  
        private System.Windows.Forms.Timer c_cierreAutomatico;
        private System.Windows.Forms.ImageList c_imagenes;
        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.PictureBox c_opcionSalir;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}