﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaIniciarSesion : FormBase
    {
        String l_Password = "";
        public String l_IdUsuario;

        public FormaIniciarSesion(bool l_timer):base (l_timer)
        {
            InitializeComponent();
        }
        public FormaIniciarSesion()
        {
            InitializeComponent();
        }

        private void FormaIniciarSesion_Load(object sender, EventArgs e)
        {
         
        }

        private void OprimirTecla(object sender, EventArgs e)
        {
           
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Password += "0";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Password += "1";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Password += "2";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Password += "3";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Password += "4";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Password += "5";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Password += "6";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Password += "7";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Password += "8";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Password += "9";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Password.Length != 0)
                {
                    l_Password = l_Password.Substring(0, l_Password.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Password.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                
                Globales.Banco = null;
                Globales.CuentaBanco = null;
                Globales.NombreCliente = null;
                Globales.IdCliente = null;


                //if (l_Password == "654321")
                //    l_IdUsuario = "MANTENIMIENTO";

                //if ( l_Password == "193720" )
                //    l_IdUsuario = "SUCURSAL";


                if (l_Password.Length > 0)
                {
                    if (!Globales.AutenticarUsuario(l_IdUsuario, l_Password))
                    {



                        using (FormaError f_Error = new FormaError(true,"password"))
                        {
                            f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                            f_Error.ShowDialog();
                        }
                        Close();
                    }
                    else
                    {


                        Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO", 1);

                        if ( Globales.IdTipoUsuario == 1 )
                        {


                            //if ( Properties.Settings.Default.No_Debug_Pc )//l_IdMoneda == 1)
                            //{
                            //    int lRet;
                            //    int l_Reintentos = 0;
                            //    try
                            //    {
                            //        do
                            //        {
                            //            if ( l_Reintentos >= 1 ) //mayor que cero para cambiar BIN
                            //                break;
                            //            lRet = SidLib.ResetError( );
                            //            l_Reintentos++;
                            //        } while ( lRet != 0 );
                            //    }
                            //    catch ( Exception ex )
                            //    {
                            //    }


                            //}

                            if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                                using ( FormaVerificarcs l_verificar = new FormaVerificarcs( true) )
                                {
                                   
                                    l_verificar.ShowDialog(FormaPrincipal.s_Forma);

                                }

                            else
                            {
                                FormaPrincipal.s_Forma.Gsi_Cronometro( false );

                                if ( Properties.Settings.Default.Port_YUGO > 0 )
                                {
                                    DialogResult l_R;
                                    using ( FormaDepositoMixto2 f_DepositoMixto = new FormaDepositoMixto2( ) )
                                    {
                                       
                                        l_R = f_DepositoMixto.ShowDialog(FormaPrincipal.s_Forma);


                                    }
                                    if (l_R == DialogResult.OK)
                                    {
                                        using (FormFINdeposito l_F = new FormFINdeposito(true))
                                        {
                                           
                                            l_F.ShowDialog(FormaPrincipal.s_Forma);
                                        }
                                    }



                                }
                                else
                                {
                                    FormaPrincipal.s_Forma.Gsi_Cronometro( false );
                                    DialogResult l_R;
                                    using (FormaDeposito f_Deposito = new FormaDeposito())
                                    {
                                      
                                        l_R = f_Deposito.ShowDialog(FormaPrincipal.s_Forma);


                                    }
                                    if (l_R == DialogResult.OK)
                                    {
                                        using (FormFINdeposito l_F = new FormFINdeposito(true))
                                        {
                                            
                                            l_F.ShowDialog(FormaPrincipal.s_Forma);
                                        }
                                    }

                                }
                             //   FormaPrincipal.s_Forma.Gsi_Cronometro( true );
                            }


                            Close( );
                        }
                        else
                        {
                            if ( Globales.IdTipoUsuario == 2 )
                            {
                                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                                    UtilsComunicacion.AlertaGSI( 97 , "Inicio de Recoleccion de ETV" );
                            }
                           



                            using ( FormaActividades f_Opciones = new FormaActividades( ) )
                                {
                                    f_Opciones.ShowDialog( );
                                }

                            if ( Globales.IdTipoUsuario == 2 )
                            {
                                if ( Properties.Settings.Default.Enviar_AlertasGSI )
                                    UtilsComunicacion.AlertaGSI( 98 , "Termino de Recoleccion de ETV" );
                            }
                        }
                        Close();
                    }
                }
                else
                {
                    using (FormaError f_Error = new FormaError(true, "texto"))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir una contraseña";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
            
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
