﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaError : Form
    {

        bool AceptarVisible = true;
        bool CancelarVisible = true;


        public FormaError(bool p_aceptarButton, bool p_cancelarButton, bool p_cierre_automatico, String p_imagen):this(p_cierre_automatico, p_imagen)
        {

           // InitializeComponent();
            AceptarVisible = p_aceptarButton;
            CancelarVisible = p_cancelarButton;

           
        }

        public FormaError(bool p_cierreAutomatico, String p_imagen)
        {

            InitializeComponent();
            c_emblema.Image = imageList1.Images["informacion2"];

            if (p_cierreAutomatico)
                c_cierreAutomatico.Enabled = true;
            //if (String.IsNullOrEmpty(p_imagen))
            //    c_Imagen.Image = imageList1.Images["mensaje"];
            //else
            //    c_Imagen.Image = imageList1.Images[p_imagen];

            CancelarVisible = false;
            AceptarVisible = true;

            c_activarVentana.Enabled = true;
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Forzardibujado()
        {
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();

        }

        private void c_cierreAutomatico_Tick(object sender, EventArgs e)
        {
            c_cierreAutomatico.Stop();
            c_activarVentana.Stop( );
            Forzardibujado();
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void FormaError_Load_1(object sender, EventArgs e)
        {
            Cursor.Hide();
           

            c_BotonAceptar.Visible = AceptarVisible;
            c_BotonCancelar.Visible = CancelarVisible;

            if (AceptarVisible ^ CancelarVisible)
            {
                if (AceptarVisible)
                    c_BotonAceptar.Location = new System.Drawing.Point(137, 210);

                if (CancelarVisible)
                    c_BotonCancelar.Location = new System.Drawing.Point(137, 210);

            }

            c_BotonAceptar.Select();
           
        }

        public  void tamaño_letra(int p_tamano_font)
        {
            c_MensajeError.Font = new Font(c_MensajeError.Font.FontFamily, p_tamano_font,FontStyle.Bold);
        }

        private void c_activarVentana_Tick( object sender , EventArgs e )
        {
            this.Activate( );
           
        }
    }
}
