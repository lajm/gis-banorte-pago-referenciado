﻿using Irds;
using SidApi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    public class ContadorEfectivo
    {
        private int idMoneda;
        private Detalle[] desglose;
        private int ultimoCodigoError;
        private string ultimoMensajeError;

        public const int _IDMON_BILLETES_MXN = 1;
        public const int _IDMON_BILLETES_USD = 9;
        public const int _IDMON_MONEDAS_MXN = 0;


        #region PROPIEDADES
        public Detalle[] Desglose
        {
            get { return desglose; }
            set { desglose = value; }
        }

        public int IdMoneda
        {
            get { return idMoneda; }
            set { idMoneda = value; }
        }

        public int UltimoCodigoError
        {
            get { return ultimoCodigoError; }
            set { ultimoCodigoError = value; }
        }

        public string UltimoMensajeError
        {
            get { return ultimoMensajeError; }
            set { ultimoMensajeError = value; }
        }
        #endregion

        public class Detalle
        {
            public int m_IdDenominacion;
            public int m_IdMoneda;
            public double m_Valor;
            public int m_IdIrds;
            public int? m_CodigoNativo;
            public int m_Cantidad;


            public Detalle()
            {
            }

            public Detalle( int p_IdDenominacion, int p_IdMoneda, double p_Valor, int p_IdIrds, int? p_CodigoNativo = null )
            {
                m_IdDenominacion = p_IdDenominacion;
                m_IdMoneda = p_IdMoneda;
                m_Valor = p_Valor;
                m_IdIrds = p_IdIrds;
                m_CodigoNativo = p_CodigoNativo;
                m_Cantidad = 0;
            }
        }

        public ContadorEfectivo()
        {
        }

        //public ContadorEfectivo( int p_NumeroElementos )
        //{
        //    desglose
        //}


        public int BorrarError()
        {
            UltimoCodigoError = ErroresSid.Codigos.OKAY;
            UltimoMensajeError = "";
            return 0;
        }

        public int GenerarError( int p_Codigo, string p_Error, string p_InfoError )
        {
            ultimoCodigoError = p_Codigo;
            ultimoMensajeError = p_Error + p_InfoError;

            return p_Codigo;
        }


        public int GenerarError( int p_Codigo, string p_InfoError )
        {
            ultimoCodigoError = p_Codigo;
            ultimoMensajeError = "" + p_InfoError;

            return p_Codigo;
        }


        public int CargarMoneda( int p_IdMoneda )
        {
            String l_Query
                = "SELECT IdDenominacion "
                + " , IdMoneda "
                + " , Codigo "
                + " , Irds "
                + " , -1 As CodigoNativo "
                + " , 0 As Cantidad "
                + "FROM Denominacion "
                + "WHERE IdMoneda = " + p_IdMoneda + " "
                + "ORDER BY CAST(Codigo as DECIMAL) ";

            //Globales.EscribirBitacora( "ContadorEfectivo.CargarMoneda() <<" );

            try
            {
                BorrarError();
                String l_Error;
                DataTable l_Datos = UtilsBD.GenerarTablaTry( l_Query, out l_Error );
                if ( l_Datos == null )
                {
                    return GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectio.CargarMoneda()", "" );
                }
                else
                {
                    if ( ( l_Datos.Rows != null ) && ( l_Datos.Rows.Count > 0 ) )
                    {
                        Desglose = new Detalle[ l_Datos.Rows.Count ];

                        for ( int i = 0 ; i < l_Datos.Rows.Count ; i++ )
                        {
                            DataRow l_Fila = l_Datos.Rows[ i ];
                            int l_IdDenominacion = Convert.ToInt32( l_Fila[ "IdDenominacion" ] );
                            int l_IdMoneda = Convert.ToInt32( l_Fila[ "IdMoneda" ] );
                            double l_Valor = Convert.ToDouble( l_Fila[ "Codigo" ] );
                            int l_IdIrds = Convert.ToInt32( l_Fila[ "Irds" ] );
                            Detalle l_D = new Detalle( l_IdDenominacion, l_IdMoneda, l_Valor, l_IdIrds );
                            desglose[ i ] = l_D;
                        }
                    }
                    else
                    {
                        return GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectio.CargarMoneda()"
                            , "No existen registro para esta moneda : " + p_IdMoneda );
                    }
                }
            }
            catch ( Exception E )
            {
                Globales.EscribirBitacora( "ContadorEfectivo.CargarMoneda() ERROR Exception : " + E.Message );

                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.CargarMoneda() EXCEPTION", E.Message );
                return GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.CargarMoneda() EXCEPTION" + E.Message );
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo.CargarMoneda() >>" );
            }
            return 0;
        }


        public static ContadorEfectivo CrearContador( int p_IdMoneda, out string p_Error )
        {
            ContadorEfectivo l_Contador = null;
            int l_R;

            //Globales.EscribirBitacora( "ContadorEfectivo CrearContador() <<" );

            try
            {
                l_Contador = new ContadorEfectivo();
                l_R = l_Contador.CargarMoneda( p_IdMoneda );
                if ( l_R != 0 )
                {
                    p_Error = "Error al crear ContadorEfectivo IdMoneda : " + p_IdMoneda;
                    return null;
                }
                p_Error = "";
                l_Contador.BorrarError();
            }
            catch ( Exception E )
            {
                l_Contador = null;
                p_Error = "ContadorEfectivo.CrearContador()  Exception : " + E.Message;
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo CrearContador() >>" );
            }

            return l_Contador;
        }


        public int AgregarPiezasCodigoNativo( int p_CodigoNativo, int p_Piezas, out double p_Valor )
        {
            BorrarError();
            p_Valor = -1;
            try
            {
                for ( int i = 0 ; i < Desglose.Length ; i++ )
                {
                    if ( desglose[ i ].m_CodigoNativo == p_CodigoNativo )
                    {
                        desglose[ i ].m_Cantidad = desglose[ i ].m_Cantidad + p_Piezas;
                        p_Valor = desglose[ i ].m_Valor;
                        Globales.EscribirBitacora( "Deposito", "Billete reconocido", "Monto : " + desglose[ i ].m_Valor.ToString( "C" ),3 );
                        return desglose[ i ].m_Cantidad;
                    }
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.AgregarPiezasCodigoNativo() EXCEPTION "
                                    , E.Message );
                return -1;

            }

            return -1;
        }


        public Double DenominacionCodigoNativo( int p_CodigoNativo )
        {
            BorrarError();
            try
            {
                for ( int i = 0 ; i < Desglose.Length ; i++ )
                {
                    if ( desglose[ i ].m_CodigoNativo == p_CodigoNativo )
                    {
                        return desglose[ i ].m_Valor;
                    }
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.DenominacionCodigoNativo() EXCEPTION "
                                    , E.Message );
            }
            return -1;
        }


        public int AgregarPiezasValor( double p_Valor, int p_Piezas )
        {
            BorrarError();
            try
            {
                for ( int i = 0 ; i < Desglose.Length ; i++ )
                {
                    if ( desglose[ i ].m_Valor == p_Valor )
                    {
                        desglose[ i ].m_Cantidad = desglose[ i ].m_Cantidad + p_Piezas;
                        return desglose[ i ].m_Cantidad;
                    }
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.AgregarPiezasCodigoNativo() EXCEPTION "
                                    , E.Message );
            }
            return -1;
        }


        public int AgregarCodigoNativo( double p_Valor, int p_Codigo )
        {
            try
            {
                for ( int i = 0 ; i < Desglose.Length ; i++ )
                {
                    if ( Desglose[ i ].m_Valor == p_Valor )
                    {
                        Desglose[ i ].m_CodigoNativo = p_Codigo;
                        return 0;
                    }
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.PonerCodigoNativo()  Exception : " + E.Message );
            }

            return -1;
        }


        public static ContadorEfectivo CrearContadorCts( out string p_Error )
        {

            p_Error = "";
            try
            {
                ContadorEfectivo l_Contador = ContadorEfectivo.CrearContador( _IDMON_BILLETES_MXN, out p_Error );
                if ( l_Contador != null )
                {
                    l_Contador.AgregarCodigoNativo( 20, 16 );
                    Globales.EscribirBitacora( "  >>>>>>>>>>>>>>>>>>>>>>>>>> IDD: " + l_Contador.desglose[ 0 ].m_IdDenominacion
                        + " V: " + l_Contador.desglose[ 0 ].m_Valor + "  CN: " + l_Contador.desglose[ 0 ].m_CodigoNativo );
                    l_Contador.AgregarCodigoNativo( 50, 17 );
                    l_Contador.AgregarCodigoNativo( 100, 18 );
                    l_Contador.AgregarCodigoNativo( 200, 19 );
                    l_Contador.AgregarCodigoNativo( 500, 20 );
                    l_Contador.AgregarCodigoNativo( 1000, 21 );
                }
                else
                {
                    Globales.EscribirBitacora( "ERROR ContadorEfectivo.CrearContadorCts" );
                }
                return l_Contador;
            }
            catch ( Exception E )
            {
                p_Error = "ContadorEfectivo.CrearContadorCts()  Exception : " + E.Message;
            }
            return null;
        }


        public static ContadorEfectivo CrearContadorUCoin( out string p_Error )
        {
            p_Error = "";
            try
            {
                ContadorEfectivo l_Contador = ContadorEfectivo.CrearContador( _IDMON_MONEDAS_MXN, out p_Error );
                if ( l_Contador != null )
                {
                    l_Contador.AgregarCodigoNativo( 0.5, 1 );
                    l_Contador.AgregarCodigoNativo( 1, 2 );
                    l_Contador.AgregarCodigoNativo( 2, 3 );
                    l_Contador.AgregarCodigoNativo( 5, 4 );
                    l_Contador.AgregarCodigoNativo( 10, 5 );
                    //l_Contador.AgregarCodigoNativo( 20, 6 );
                }
                else
                {
                    Globales.EscribirBitacora( "ERROR ContadorEfectivo.CrearContadorUCoin" );
                }
                return l_Contador;
            }
            catch ( Exception E )
            {
                p_Error = "ContadorEfectivo.CrearContadorUCoin()  Exception : " + E.Message;
            }
            return null;
        }


        public static ContadorEfectivo DeTotalesCtsMxn( ushort[] p_TotalNotes, out string p_Error )
        {
            //Globales.EscribirBitacora( "ContadorEfectivo.DeTotalesCtsMxn() <<" );
            ContadorEfectivo l_Contador = null;
            try
            {
                l_Contador = CrearContadorCts( out p_Error );
                if ( l_Contador == null )
                {
                    Globales.EscribirBitacora( "ERROR : ContadorEfectivo.CrearContadorCts() == NULL )" );
                    return l_Contador;
                }
                l_Contador.AgregarPiezasValor( 20, p_TotalNotes[ 0 ] );
                l_Contador.AgregarPiezasValor( 50, p_TotalNotes[ 1 ] );
                l_Contador.AgregarPiezasValor( 100, p_TotalNotes[ 2 ] );
                l_Contador.AgregarPiezasValor( 200, p_TotalNotes[ 3 ] );
                l_Contador.AgregarPiezasValor( 500, p_TotalNotes[ 4 ] );
                l_Contador.AgregarPiezasValor( 1000, p_TotalNotes[ 5 ] );

            }
            catch ( Exception E )
            {
                p_Error = "ContadorEfectivo.DeTotalesCtsMxn() Exception : " + E.Message;
                return null;
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo.DeTotalesCtsMxn() >>" );
            }

            return l_Contador;
        }


        public static ContadorEfectivo DeTotalesUCoin( Dictionary<decimal, int> p_DetalleMonedas, out string p_Error )
        {
            //Globales.EscribirBitacora( "ContadorEfectivo.DeTotalesUCoin() <<" );
            ContadorEfectivo l_Contador = null;
            try
            {
                l_Contador = CrearContadorUCoin( out p_Error );
                if ( l_Contador == null )
                {
                    Globales.EscribirBitacora( "ERROR : ContadorEfectivo.CrearContadorCts() == NULL )" );
                    return l_Contador;
                }
                l_Contador.AgregarPiezasValor( 0.50, p_DetalleMonedas[ 0.50m ] );
                l_Contador.AgregarPiezasValor( 1, p_DetalleMonedas[ 1 ] );
                l_Contador.AgregarPiezasValor( 2, p_DetalleMonedas[ 2 ] );
                l_Contador.AgregarPiezasValor( 5, p_DetalleMonedas[ 5 ] );
                l_Contador.AgregarPiezasValor( 10, p_DetalleMonedas[ 10 ] );
                //l_Contador.AgregarPiezasValor( 20, p_TotalNotes[ 5 ] );

             //   Globales.EscribirBitacora("ContadorEfectivo", "DeTotalesUCoin", l_Contador.ToString(), 3);

            }
            catch ( Exception E )
            {
                p_Error = "ContadorEfectivo.DeTotalesCtsMxn() Exception : " + E.Message;
                return null;
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo.DeTotalesUCoin() >>" );
            }

            return l_Contador;

        }


        public ContadorEfectivo DiferenciaCts( ContadorEfectivo p_ContadorInicial )
        {
            //Globales.EscribirBitacora( "ContadorEfectivo DiferenciaCts <<" );
            String l_Error = "";
            ContadorEfectivo l_Diferencia = null;

            BorrarError();
            try
            {
                l_Diferencia = ContadorEfectivo.CrearContadorCts( out l_Error );
                if ( l_Diferencia == null )
                {
                    GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.DiferenciaCts()  CrearContadorCts" );
                    return l_Diferencia;
                }

                for ( int i = 0 ; i < l_Diferencia.Desglose.Count() ; i++ )
                {
                    l_Diferencia.Desglose[ i ].m_Cantidad = Desglose[ i ].m_Cantidad - p_ContadorInicial.Desglose[ i ].m_Cantidad;
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.DiferenciaCts()  Exception : " + E.Message );
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo DiferenciaCts >>" );
            }

            return l_Diferencia;
        }


        public ContadorEfectivo DiferenciaUCoin( ContadorEfectivo p_ContadorInicial )
        {
            //Globales.EscribirBitacora( "ContadorEfectivo DiferenciaUCoin <<" );
            String l_Error = "";
            ContadorEfectivo l_Diferencia = null;

            BorrarError();
            try
            {
                l_Diferencia = ContadorEfectivo.CrearContadorUCoin( out l_Error );
                if ( l_Diferencia == null )
                {
                    GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.DiferenciaCts()  CrearContadorUCoin" );
                    return l_Diferencia;
                }

                for ( int i = 0 ; i < l_Diferencia.Desglose.Count() ; i++ )
                {
                    l_Diferencia.Desglose[ i ].m_Cantidad = Desglose[ i ].m_Cantidad - p_ContadorInicial.Desglose[ i ].m_Cantidad;
                }
            }
            catch ( Exception E )
            {
                GenerarError( ErroresSid.Codigos.SOFTWARE, "ContadorEfectivo.DiferenciaUCoin()  Exception : " + E.Message );
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo DiferenciaUCoin >>" );
            }

            return l_Diferencia;
        }


        public ContadorEfectivo Suma( ContadorEfectivo p_Contador )
        {
            int l_OriLin = this.desglose.Length;
            Array.Resize( ref desglose,  Desglose.Length + p_Contador.Desglose.Length );

            Array.Copy( p_Contador.Desglose, 0, desglose, l_OriLin, p_Contador.Desglose.Length );

            return this;
        }

        public int PiezasPorDenominacion( double p_Denominacion )
        {
            for ( int i = 0 ; i < Desglose.Count() ; i++ )
                if ( desglose[ i ].m_Valor == p_Denominacion )
                    return desglose[ i ].m_Cantidad;

            return -1;
        }

        public int TotalPiezas()
        {
            //Globales.EscribirBitacora( "ContadorEfectivo.TotalPiezas() <<" );
            int l_Total = 0;
            for ( int i = 0 ; i < Desglose.Count() ; i++ )
                l_Total += desglose[ i ].m_Cantidad;
            //Globales.EscribirBitacora( "Total Piezas : " + l_Total.ToString() );
            //Globales.EscribirBitacora( "ContadorEfectivo.TotalPiezas() >>" );
            return l_Total;
        }


        public double TotalMonto()
        {
            double l_Total = 0;
            for ( int i = 0 ; i < Desglose.Count() ; i++ )
            {
                double l_D = desglose[ i ].m_Valor;
                int l_C = desglose[ i ].m_Cantidad;
                l_Total += l_D * l_C;
            }
            return l_Total;
        }

        public override string ToString()
        {
            double l_Cantidad;
            double l_Denominacion;
            double l_Total;
            StringBuilder l_Renglon = new StringBuilder();

            //Globales.EscribirBitacora( "ContadorEfectivo.ToString() << " );

            try
            {
                l_Renglon.AppendLine( "======================= DESGLOSE EFECTIVO IdMoneda : " + idMoneda );

                double l_TotalTotal = 0;
                for ( int i = 0 ; i < desglose.Length ; i++ )
                {
                    l_Denominacion = desglose[ i ].m_Valor;
                    l_Cantidad = desglose[ i ].m_Cantidad;
                    l_Total = l_Denominacion * l_Cantidad;
                    l_Renglon.AppendLine( " >>>> Renglon{" + i + "}  DENOMINACION : " + l_Denominacion + " CANTIDAD : " + l_Cantidad.ToString() + "  TOTAL : " + l_Total.ToString( "C" ) );
                    l_TotalTotal += l_Total;
                }

                l_Renglon.AppendLine( "======================= FIN DESGLOSE EFECTIVO  Total : " + l_TotalTotal.ToString( "C" ) );
            }
            catch ( Exception E )
            {
                Globales.EscribirBitacora( "ContadorEfectivo", "ToString()", E.Message, 1 );
                return null;
            }
            finally
            {
                //Globales.EscribirBitacora( "ContadorEfectivo.ToString() >> " );
            }

            return l_Renglon.ToString();
        }

    }
}

