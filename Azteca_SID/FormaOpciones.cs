﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaOpciones : FormBase
    {
        public FormaOpciones()
        {
            InitializeComponent();
        }

        public FormaOpciones(bool p_cierreAutomatico) : base(p_cierreAutomatico)
        {
            InitializeComponent();
        }

        private void c_regresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Deposito_Click(object sender, EventArgs e)
        {
            using (FormaUsuario l_usuario = new FormaUsuario())
            {
                l_usuario.ShowDialog();
            }

            //using (FormaLogueo l_logueoazteca = new FormaLogueo())
            //{
            //    l_logueoazteca.ShowDialog();

            //}

            Forzardibujado();
            CalcularStock();
        }

        private void c_Administrar_Click(object sender, EventArgs e)
        {
            using (FormaIniciarSesion f_Inicio = new FormaIniciarSesion())
            {

                f_Inicio.l_IdUsuario = "Supervisor_ZONA";
                f_Inicio.ShowDialog();
            }
            Close();
        }

        private void c_Etv_Click(object sender, EventArgs e)
        {
            using (FormaIniciarSesion f_Inicio = new FormaIniciarSesion())
            {

                f_Inicio.l_IdUsuario = "ETV_Traslado";
                f_Inicio.ShowDialog();
            }


            Close();
        }

        private void FormaOpciones_Load(object sender, EventArgs e)
        {
            //Globales.IdUsuario = "";
            
            CalcularStock();
            ComprobarBolsa();

        }

        private void ComprobarBolsa()
        {
            if (Globales.NumeroSerieBOLSA == "00000")
            {
                c_Deposito.Enabled = false;

                using (FormaError f_Error = new FormaError(false, "Verificación de Envase"))
                {
                    f_Error.c_MensajeError.Text = "Es Necesario que exista un Envase Valido, por Favor avise al Administrador del Equipo";
                    f_Error.ShowDialog();
                    //  c_BotonCancelar_Click(this, null);
                }

               
            }

        }

        private void CalcularStock()
        {
            Double l_numbilletesenbolsa = BDDeposito.ObtenerTotalBilletes();
            if (l_numbilletesenbolsa == 0)
            {
                Globales.Alertastock_enviada = false;
                label2.Text = "0.00 %";

            }



            else
            {
                Double l_decimal = (l_numbilletesenbolsa / Properties.Settings.Default.CapacidadBolsa);
                l_numbilletesenbolsa = (l_decimal * 100);
                label2.Text = l_numbilletesenbolsa.ToString("###.##") + " %";



                if (l_numbilletesenbolsa > 79 && !Globales.Alertastock_enviada)
                {
                    Registrar.AlertaSTOCK();
                    Globales.Alertastock_enviada = true;
                }

                if (l_numbilletesenbolsa > 99)
                {
                    c_Deposito.Enabled = false;
                    pictureBox3.Enabled = false;
                }
                else
                    c_Deposito.Enabled = true;
            }


        }
    }
}
