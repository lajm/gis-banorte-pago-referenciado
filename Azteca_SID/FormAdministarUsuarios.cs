﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormAdministarUsuarios : Azteca_SID.FormBase
    {
        public FormAdministarUsuarios()
        {
            InitializeComponent();
        }

        private void c_visor_ItemActivate(object sender, EventArgs e)
        {
            switch (c_visor.FocusedItem.Index)
            {
                case 0:
                    using (FormAltaUsers f_alta = new FormAltaUsers())
                    {
                        f_alta.ShowDialog();
                    }
                    break;
                case 1:
                    using (FormModificarUsuario f_mod = new FormModificarUsuario())
                    {
                        f_mod.ShowDialog();
                    }

                    break;
                case 2:
                    using (FormBajaUsuarios f_baja = new FormBajaUsuarios())
                    {
                        f_baja.ShowDialog();
                    }

                    break;
                case 3:

                    //using (FormaError f_Error = new FormaError(false, "warning"))
                    //{
                    //    f_Error.c_MensajeError.Text = "No permitido desde Este RECEPTOR";
                    //    f_Error.ShowDialog();
                    //}
                    using (FormCambiarContraseña f_cambio = new FormCambiarContraseña())
                    {
                        f_cambio.ShowDialog();
                    }

                    break;
                case 4:
                    Close();
                    break;
                default:

                    break;
            }
        }

        private void c_visor_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}
