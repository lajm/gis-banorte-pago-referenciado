﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaDepositoManual : Azteca_SID.FormBase
    {
        int l_20 = 0, l_50 = 0, l_100 = 0, l_200 = 0, l_500 = 0, l_1000 = 0;
        Double l_MontoTotal = 0;

        public const int DEN_MX_20 = 0;
        public const int DEN_MX_50 = 1;
        public const int DEN_MX_100 = 2;
        public const int DEN_MX_200 = 3;
        public const int DEN_MX_500 = 4;
        public const int DEN_MX_1000 = 5;
        public const int DEN_MX_UNKNOWN = 6;
        public const int MAX_DEN_MX = 6;

        public static int[] DenominationsMX = { 20, 50, 100, 200, 500, 1000 };

        private void FormaDepositoManual_Load(object sender, EventArgs e)
        {
            c_empleado.Text = Globales.NombreUsuario.ToUpper();
            Iniciliza();
            Application.DoEvents();
        }

        int l_TotalAceptados = 0;
        int[] m_TotalBilletes = new int[10];

        private void c_agregar20_Click(object sender, EventArgs e)
        {
            l_20++;
            Actualiza();
        }

        private void c_agregar50_Click(object sender, EventArgs e)
        {
            l_50++;
            Actualiza();
        }

        private void c_agregar100_Click(object sender, EventArgs e)
        {
            l_100++;
            Actualiza();
        }

        private void c_agregar200_Click(object sender, EventArgs e)
        {
            l_200++;
            Actualiza();
        }

        private void c_agregar500_Click(object sender, EventArgs e)
        {
            l_500++;
            Actualiza();
        }

        private void c_agregar1000_Click(object sender, EventArgs e)
        {
            l_1000++;
            Actualiza();
        }

        private void c_resta20_Click(object sender, EventArgs e)
        {
            if (l_20 > 0)
            {
                l_20--;
                Actualiza();
            }
        }

        private void c_resta50_Click(object sender, EventArgs e)
        {
            if (l_50 > 0)
            {
                l_50--;
                Actualiza();
            }
        }

        private void c_resta100_Click(object sender, EventArgs e)
        {
            if (l_100 > 0)
            {
                l_100--;
                Actualiza();
            }
        }

        private void c_resta200_Click(object sender, EventArgs e)
        {
            if (l_200 > 0)
            {
                l_200--;
                Actualiza();
            }
        }

        private void c_resta500_Click(object sender, EventArgs e)
        {
            if (l_500 > 0)
            {
                l_500--;
                Actualiza();
            }
        }

        private void c_resta1000_Click(object sender, EventArgs e)
        {
            if (l_1000 > 0)
            {
                l_1000--;
                Actualiza();
            }
        }

        public FormaDepositoManual()
        {
            InitializeComponent();
        }

        void Iniciliza()
        {
            c_total20.Text = l_20.ToString("00");
            c_total50.Text = l_50.ToString("00");
            c_total100.Text = l_100.ToString("00");
            c_total200.Text = l_200.ToString("00");
            c_total500.Text = l_500.ToString("00");
            c_total1000.Text = l_1000.ToString("00");
            l_MontoTotal = 0;
            l_TotalAceptados = 0;
        }

        void Actualiza()
        {
            c_total20.Text = l_20.ToString("00");
            c_total50.Text = l_50.ToString("00");
            c_total100.Text = l_100.ToString("00");
            c_total200.Text = l_200.ToString("00");
            c_total500.Text = l_500.ToString("00");
            c_total1000.Text = l_1000.ToString("00");

            l_MontoTotal = l_20 * 20 + l_50 * 50 + l_100 * 100 + l_200 * 200 + l_500 * 500 + l_1000 * 1000; ;
            l_TotalAceptados = l_20 + l_50 + l_100 + l_200 + l_500 + l_1000;
            c_montototal.Text = l_MontoTotal.ToString("$#,###,##0.00");
            c_totalbilletes.Text = l_TotalAceptados.ToString("00");

        }
        private void c_cancelar_Click(object sender, EventArgs e)
        {
            Close();
           // GC.Collect();
        }

        private void c_confirmar_Click(object sender, EventArgs e)
        {
            try
            {
                Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", "<<<", 3);

                if (l_TotalAceptados == 0)
                {
                    // -- SI NO SE HAN PROCESADO BILLETES, CERRAR PROCESO
                    using (FormaError f_Error = new FormaError(true, "noBilletes"))
                    {
                        f_Error.c_MensajeError.Text = "No se Deposito Nada ... saliendo";
                        Globales.EscribirBitacora("Deposito Manual", "No hay Deposito", "Terminando Deposito Manual", 3);
                        f_Error.ShowDialog();
                    }
                    Close();
                }
                else
                {

                    DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);

                    m_TotalBilletes[DEN_MX_20] = l_20;
                    m_TotalBilletes[DEN_MX_50] = l_50;
                    m_TotalBilletes[DEN_MX_100] = l_100;
                    m_TotalBilletes[DEN_MX_200] = l_200;
                    m_TotalBilletes[DEN_MX_500] = l_500;
                    m_TotalBilletes[DEN_MX_1000] = l_1000;

                    // -- GENERAR FILAS DE LA TABLA
                    for (int i = 0; i < MAX_DEN_MX; i++)
                    {
                        Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", "Denominacion: " + i.ToString(), 3);

                        String l_DenominacionString = DenominationsMX[i].ToString();

                        if (m_TotalBilletes[i] > 0)
                        {
                            int l_IdDenominacionBD = BDDenominaciones.ObtenerIdDenominacion(1, l_DenominacionString);

                            Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", "Fila: " + i + l_IdDenominacionBD + " 1: " + m_TotalBilletes[i] + " 2: " + l_DenominacionString, 3);

                            DataRow l_Fila = l_Detalle.NewRow();
                            l_Fila[0] = l_IdDenominacionBD;
                            l_Fila[1] = m_TotalBilletes[i];
                            l_Fila[2] = l_DenominacionString;

                            l_Detalle.Rows.Add(l_Fila);

                        }
                        else
                            Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", "Denominación sin billetes i: " + i + " (" + l_DenominacionString + ") ", 3);

                    }

                    // -- GRABAR EN LA BASE DE DATOS
                    if (!UtilsComunicacion.Enviar_Transaccion(Globales.CuentaBanco, 1, 1, 5, l_MontoTotal,
                        l_Detalle, 0, true, Globales.IdUsuario))
                    {
                        using (FormaError f_Error = new FormaError(false,"warning"))
                        {
                            f_Error.c_MensajeError.Text = "ERROR CRÍTICO DEL SISTEMA AL GRABAR LA TRANSACCIÓN, AVISE AL ADMINISTRADOR";
                            Globales.EscribirBitacora("Deposito Manual", "Grabar Transacción", "ERROR CRÍTICO", 3);
                            f_Error.ShowDialog();
                        }
                    }
                    else
                        Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", "Registro Grabado", 3);
                }

                Close();

            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Deposito", "Loop Principal Validacion", "EXCEPCIÓN - " + ex.Message, 3);
                using (FormaError f_Error = new FormaError(false, "Error"))
                {
                    f_Error.c_MensajeError.Text = "ERROR DEL SISTEMA - " + ex.Message;
                    Globales.EscribirBitacora("Deposito Manual", "Grabar Transacción", "ERROR CRÍTICO", 3);
                    f_Error.ShowDialog();
                }

                            }
            finally
            {
                Globales.EscribirBitacora("Deposito Manual", "TerminarTransaccion", ">>>", 3);
            }
 
        }
    }
}
