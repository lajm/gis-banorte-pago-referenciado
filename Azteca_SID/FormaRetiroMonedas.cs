﻿using SymetryDevices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaRetiroMonedas : Azteca_SID.FormBase
    {
        Double l_TotalMoneadas_B1;
        Double l_TotalMoneadas_B2;
        DataTable l_Retiro;
        DataTable l_Retirojunto;
        DataTable l_DatosMonedas;

        public FormaRetiroMonedas(DataTable l_detallemonedas)
        {
            InitializeComponent();
            l_DatosMonedas = l_detallemonedas;
        }

        private void FormaRetiroMonedas_Load(object sender, EventArgs e)
        {
            TraerDatos();
           // UnirBolsasRetiro( );
        }

        private void c_imprimirMonedas_Click(object sender, EventArgs e)
        {
            c_imprimirMonedas.Enabled = false;
            DialogResult l_dialogo;
            try
            {
                using (FormaError l_ventanaRetiro = new FormaError(true,true,false, "retiro"))
                {
                    l_ventanaRetiro.c_MensajeError.Text = "Se procedera a Limpiar el contenido, Por favor Abra y vacie la Bolsa, cuando termine, cierre con llave y Presione Aceptar";
                    l_dialogo = l_ventanaRetiro.ShowDialog();

                }

                if (l_dialogo == DialogResult.OK)
                {
                    //cambiar 
                    if (Properties.Settings.Default.No_Debug_Pc)
                    {
                        MostarMensaje("Ticket Retiro de Monedas ... Por favor Abra y vacie el Compartimiento de MONEDAS, cuando termine, cierre", false, "precaucion");

                        Forzardibujado();
                        try
                        {

                            l_Reply = UCoin.cRetiro(Properties.Settings.Default.Port_YUGO);
                            Globales.EscribirBitacora("Retiro Monedas", "Retiro", "Rely: " + l_Reply, 3);

                            if (l_Reply == 0)
                                MostarMensaje("Retiro de Monedas Exitoso", true, "exito");

                        }
                        catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Retiro Monedas", "Retiro U-COIN", "Reply: " + exx.Message, 3);
                        }
                    }

                    
                   // UtilsComunicacion.Enviar_Transaccion(Globales.IdUsuario, 1, 0, 2, l_TotalMoneadas_B1, l_Retiro, 0, true, Globales.IdUsuario);
            ImprimirRetiro();
                    Limpiar();
                }

            }
            catch (Exception exr)

            {

            }

            Activate();
            TraerDatos();
            c_cerrarM.Text = "SALIR";



        }



        private void ImprimirRetiro()
        {
           int  contador = 0;

            if (l_TotalMoneadas_B1 > 0)
            {
                ModulePrint imprimir = new ModulePrint();

                imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);

                imprimir.AddHeaderLine("RETIRO de Efectivo MONEDAS");
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("Contenido en BOVEDAD");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                // imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                // imprimir.AddSubHeaderLine(Properties.Settings.Default.Cliente);

            //    imprimir.AddItem(c_bolsa1_20.Text, "$20.00", "20");
                imprimir.AddItem(c_bolsa1_10.Text, "$10.00", "10");
                imprimir.AddItem(c_bolsa1_5.Text, "$5.00", "5");
                imprimir.AddItem(c_bolsa1_2.Text, "$2.00", "2");
                imprimir.AddItem(c_bolsa1_1.Text, "$1.00", "1");
                imprimir.AddItem(c_bolsa1_50c.Text, "$0.50", "0.5");
            //    imprimir.AddItem(c_bolsa1_20c.Text, "$0.20", "0.2");
           //     imprimir.AddItem(c_bolsa1_10c.Text, "$0.10", "0.1");

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Total BOLSA NUMERO 1: " + c_totalbolsa1.Text);

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Numero de MONEDAS: " + c_totalMonedas_b1.Text);

                imprimir.AddFooterLine("LUGAR DE EMISIÓN: " + Properties.Settings.Default.Cliente);

                try
                {

                    imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

                }
                catch (Exception exx)
                {

                }

                while (contador < 3)
                {
                    imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                    imprimir.AddFooterLine("COMPROBANTE COPIA ");
                    contador++;
                }
            }

            contador = 0;

            if (l_TotalMoneadas_B2 > 0)
            {
                ModulePrint imprimir = new ModulePrint();

                imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);
                            
                imprimir.AddHeaderLine("RETIRO de Efectivo MONEDAS");
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("Contenido en BOVEDAD");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                imprimir.AddSubHeaderLine(Properties.Settings.Default.Cliente);

              //  imprimir.AddItem(c_bolsa2_20.Text, "$20.00", "20");
                imprimir.AddItem(c_bolsa2_10.Text, "$10.00", "10");
                imprimir.AddItem(c_bolsa2_5.Text, "$5.00", "5");
                imprimir.AddItem(c_bolsa2_2.Text, "$2.00", "2");
                imprimir.AddItem(c_bolsa2_1.Text, "$1.00", "1");
                imprimir.AddItem(c_bolsa2_50c.Text, "$0.50", "0.5");
            //    imprimir.AddItem(c_bolsa2_20c.Text, "$0.20", "0.2");
            //    imprimir.AddItem(c_bolsa2_10c.Text, "$0.10", "0.1");

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Total en BOLSA NUMERO 2: " + c_totalbolsa2.Text);

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Numero de MONEDAS: " + c_totalMonedas_b2.Text);

                imprimir.AddFooterLine("LUGAR DE EMISIÓN: " + Properties.Settings.Default.Cliente);

                try
                {

                    imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

                }
                catch (Exception exx)
                {

                }

                while (contador < 3)
                {
                    imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                    imprimir.AddFooterLine("COMPROBANTE COPIA ");
                    contador++;
                }
            }

        }


        private void TraerDatos()
        {
            int l_totalmonedas_b2 = 0;
            int l_totalmonedas_b1 = 0;

            Limpiar();

            l_Retiro = BDRetiro.ObtenerDetalleRetiro(0);
            l_Retirojunto = BDRetiro.ObtenerDetalleRetiro(0);
          

            if (l_DatosMonedas.Rows.Count < 1)
            {
                MostarMensaje("No hay depósitos en MONEDAS", true, "aviso");

                c_imprimirMonedas.Enabled = false;

            }

            //DEPOSITO MONEDAS

            foreach (DataRow l_Detalle in l_DatosMonedas.Rows)
            {
                if (l_Detalle[1].ToString().Trim() == "20.0")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "20.0");
                    l_Nueva[2] = "20.00";

                    if (l_Detalle[2].ToString() == "1")
                    {


                        c_bolsa1_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 20;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 20;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);

                }
                if (l_Detalle[1].ToString().Trim() == "10.0")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "10.0");
                    l_Nueva[2] = "10";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 10;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 10;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "5.00")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "5.00");
                    l_Nueva[2] = "5";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 5;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 5;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "2.00")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "2.00");
                    l_Nueva[2] = "2";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 2;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 2;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);

                }
                if (l_Detalle[1].ToString().Trim() == "1.00")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "1.00");
                    l_Nueva[2] = "1";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 1;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 1;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "0.50")
                {


                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.50");
                    l_Nueva[2] = "0.5";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_50c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.5;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_50c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.5;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }

                if (l_Detalle[1].ToString().Trim() == "0.20")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.20");
                    l_Nueva[2] = "0.20";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_20c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.2;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_20c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.2;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }

                if (l_Detalle[1].ToString().Trim() == "0.10")
                {
                    DataRow l_Nueva = l_Retiro.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.10");
                    l_Nueva[2] = "0.10";

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_bolsa1_10c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.1;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_10c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.1;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_Retiro.Rows.Add(l_Nueva);
                }
            }

            c_totalMonedas_b1.Text = l_totalmonedas_b1.ToString();
            c_totalMonedas_b2.Text = l_totalmonedas_b2.ToString();
            c_totalbolsa1.Text = l_TotalMoneadas_B1.ToString("$#,###,###,##0.00");
            c_totalbolsa2.Text = l_TotalMoneadas_B2.ToString("$#,###,###,##0.00");

        }

        private void Limpiar()
        {
            c_bolsa1_20.Text = "0";
            c_bolsa1_10.Text = "0";
            c_bolsa1_5.Text = "0";
            c_bolsa1_1.Text = "0";
            c_bolsa1_2.Text = "0";
            c_bolsa1_50c.Text = "0";
            c_bolsa1_20c.Text = "0";
            c_bolsa1_10c.Text = "0";

            c_bolsa2_20.Text = "0";
            c_bolsa2_10.Text = "0";
            c_bolsa2_5.Text = "0";
            c_bolsa2_1.Text = "0";
            c_bolsa2_2.Text = "0";
            c_bolsa2_50c.Text = "0";
            c_bolsa2_20c.Text = "0";
            c_bolsa2_10c.Text = "0";

            c_totalbolsa1.Text = "0";
            c_totalbolsa2.Text = "0";
            c_totalMonedas_b1.Text = "0";
            c_totalMonedas_b2.Text = "0";

            l_TotalMoneadas_B1 = 0;
            l_TotalMoneadas_B2 = 0;
        }

        private void UnirBolsasRetiro()
        {
            DataRow l_20 = l_Retirojunto.NewRow();
            l_20[0] = BDDenominaciones.ObtenerIdDenominacion(0, "20.0");
            l_20[1] = int.Parse(c_bolsa1_20.Text.Replace(",", "")) + int.Parse(c_bolsa2_20.Text.Replace(",", ""));
            l_20[2] = "20.0";
            l_Retirojunto.Rows.Add(l_20);

            DataRow l_10 = l_Retirojunto.NewRow();
            l_10[0] = BDDenominaciones.ObtenerIdDenominacion(0, "10.0");
            l_10[1] = int.Parse(c_bolsa1_10.Text.Replace(",", "")) + int.Parse(c_bolsa2_10.Text.Replace(",", ""));
            l_10[2] = "10.0";
            l_Retirojunto.Rows.Add(l_10);

            DataRow l_5 = l_Retirojunto.NewRow();
            l_5[0] = BDDenominaciones.ObtenerIdDenominacion(0, "5.00");
            l_5[1] = int.Parse(c_bolsa1_5.Text.Replace(",", "")) + int.Parse(c_bolsa2_5.Text.Replace(",", ""));
            l_5[2] = "5.00";
            l_Retirojunto.Rows.Add(l_5);

            DataRow l_2 = l_Retirojunto.NewRow();
            l_2[0] = BDDenominaciones.ObtenerIdDenominacion(0, "2.00");
            l_2[1] = int.Parse(c_bolsa1_2.Text.Replace(",", "")) + int.Parse(c_bolsa2_2.Text.Replace(",", ""));
            l_2[2] = "2.00";
            l_Retirojunto.Rows.Add(l_2);

            DataRow l_1 = l_Retirojunto.NewRow();
            l_1[0] = BDDenominaciones.ObtenerIdDenominacion(0, "1.00");
            l_1[1] = int.Parse(c_bolsa1_1.Text.Replace(",", "")) + int.Parse(c_bolsa2_1.Text.Replace(",", ""));
            l_1[2] = "1.00";
            l_Retirojunto.Rows.Add(l_1);

            DataRow l_50c = l_Retirojunto.NewRow();
            l_50c[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.50");
            l_50c[1] = int.Parse(c_bolsa1_50c.Text.Replace(",", "")) + int.Parse(c_bolsa2_50c.Text.Replace(",", ""));
            l_50c[2] = "0.50";
            l_Retirojunto.Rows.Add(l_50c);

            DataRow l_20c = l_Retirojunto.NewRow();
            l_20c[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.20");
            l_20c[1] = int.Parse(c_bolsa1_20c.Text.Replace(",", "")) + int.Parse(c_bolsa2_20c.Text.Replace(",", ""));
            l_20c[2] = "0.20";
            l_Retirojunto.Rows.Add(l_20c);

            DataRow l_10c = l_Retirojunto.NewRow();
            l_10c[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.10");
            l_10c[1] = int.Parse(c_bolsa1_10c.Text.Replace(",", "")) + int.Parse(c_bolsa2_10c.Text.Replace(",", ""));
            l_10c[2] = "0.10";
            l_Retirojunto.Rows.Add(l_10c);
        }

        private void c_cerrarM_Click(object sender, EventArgs e)
        {
            Close();
        }

  

        private void c_cerrarM_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
