﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    public class ErroresUcoin
    {

        public class Codigos
        {
            // Errores
            public const int FILE_OPEN_ERROR = -10;
            public const int DOWNLOAD_ERROR = -11;
            public const int WRONG_FILE_FORMAT = -12;
            public const int COINS_DISABLE = -13;
            public const int DLL_BAD_PARAMETER = -43;
            public const int FW_LOG_DISABLE = -50;
            public const int COM_OPEN_ERROR = -1010;
            public const int COM_CLOSE_ERROR = -1011;
            public const int COM_WRITE_ERROR = -1012;
            public const int COM_READ_ERROR = -1013;
            public const int COM_NOT_OPEN = -1014;
            public const int COM_ON_ERROR = -1015;
            public const int OKAY = 0;
            public const int SAFE_OPEN = 30;
            public const int ERASING_EXTERNAL_FLASH_ERROR= 160;
            public const int WRITING_EXTERNAL_FLASH_ERROR = 161;
            public const int EXTERNAL_FLASH__ON_ERROR= 162;
            public const int WRITING_EEPROM_ERROR = 164;
            public const int READING_EEPROM_ERROR = 165;
            public const int WRITING_INTERNAL_FLASH_ERROR = 166;
            public const int BAG_SENSOR_ANOMALY = 176;
            public const int BAG_OPEN_ERROR= 177;
            public const int BAG_CLOSE_ERROR= 178;
            public const int SOLDER_TIME_ERROR= 179;
            public const int SOLDER_PWM_TOO_HIGH = 180;
            public const int SOLDER_PWM_TOO_LOW = 181;
            public const int SOLDER_TENSION_TOO_HIGH = 182;
            public const int SOLDER_TENSION_TOO_LOW = 183;
            public const int SOLDER_CURRENT_TOO_HIGH = 184;
            public const int SOLDER_ERROR= 185;
            public const int COMUNICATION_ERROR_WITH_ITL_IN_DEPSOIT= 196;
            public const int COMUNICATION_ERROR_WITH_ITL = 197;
            //Warnings

            public const int LOGIN_ALREADY_PRESENT = 5;
            public const int SAFE_NOT_OPEN =6;
            public const int BAG_SERIAL_NUMBER_NOT_PRESENT = 10;
            public const int BAG_OR_BOX_NO_PRESENT =12;
            public const int BAG_OR_BOX_FULL_BY_NUMBER=15 ;
            public const int COMMAND_ERROR = 144;
            public const int WRONG_PARAMETER = 145;
            public const int COMMAND_NOT_EXECUTABLE =147 ;
            public const int UKNOW_USER =149 ;
            public const int NO_USER_LOGED = 151;
            public const int DEVICE_BUSY = 152;
            public const int BAG_ALREADY_SOLDERED_OR_BOX_TO_DRAN_IN_SAFE = 155;




        }

        public static void Error_Ucoin( int p_codigo_Error,out int o_nuevo_codigo,  out String o_Descripcion)
        {

            o_Descripcion = "";
            o_nuevo_codigo = p_codigo_Error - 8000;

            switch (p_codigo_Error)
            {
                case Codigos.FILE_OPEN_ERROR:
                    o_Descripcion = "Error al abrir Archivo";
                    break;
                case Codigos.DOWNLOAD_ERROR:
                    o_Descripcion = "Error de Descarga";
                    break;
                case Codigos.WRONG_FILE_FORMAT:
                    o_Descripcion = "Error de formato de Archivo";
                    break;
                case Codigos.COINS_DISABLE:
                    o_Descripcion = "Monedas inhabilitadas";
                    break;
                case Codigos.DLL_BAD_PARAMETER:
                    o_Descripcion = "Mal Parametro en DLL";
                    break;
                case Codigos.FW_LOG_DISABLE :
                    o_Descripcion = "Log deshabilitado FW";
                    break;
                case Codigos.COM_OPEN_ERROR:
                    o_Descripcion = "Error al abrir Puerto COM";
                    break;
                case Codigos.COM_CLOSE_ERROR:
                    o_Descripcion = "Error al cerrar Puerto COM";
                    break;
                case Codigos.COM_WRITE_ERROR:
                    o_Descripcion = "Error al escribir Puerto COM";
                    break;
                case Codigos.COM_READ_ERROR:
                    o_Descripcion = "Error al leer Puerto COM";
                    break;
                case Codigos.COM_NOT_OPEN:
                    o_Descripcion = "Puerto COM sin comunicación";
                    break;
                case Codigos.COM_ON_ERROR:
                    o_Descripcion = "Error en el Puerto";
                    break;
                case Codigos.SAFE_OPEN:
                    o_Descripcion = "Bovedad Abierta";
                    break;
                case Codigos.ERASING_EXTERNAL_FLASH_ERROR:
                    o_Descripcion = "Error  borrando la Memoria Flash Externa";
                    break;
                case Codigos.WRITING_EXTERNAL_FLASH_ERROR:
                    o_Descripcion = "Error de escitura en la Memoria Flash Externa";
                    break;
                case Codigos.EXTERNAL_FLASH__ON_ERROR:
                    o_Descripcion = "Error en memoria Flash Externa";
                    break;
                case Codigos.WRITING_EEPROM_ERROR:
                    o_Descripcion = "Error al escribir en EEPROM";
                    break;
                case Codigos.READING_EEPROM_ERROR:
                    o_Descripcion = "Error al leer en EEPROM";
                    break;
                case Codigos.WRITING_INTERNAL_FLASH_ERROR:
                    o_Descripcion = "Error de esritura en la memoria Flash Interna";
                    break;
                case Codigos.BAG_SENSOR_ANOMALY:
                    o_Descripcion = "Sensor de Bolsa Anómalo";
                    break;
                case Codigos.BAG_OPEN_ERROR:
                    o_Descripcion = "Error al abrir Bolsa";
                    break;
                case Codigos.BAG_CLOSE_ERROR:
                    o_Descripcion = "Error al cerrar Bolsa";
                    break;
                case Codigos.SOLDER_TIME_ERROR:
                    o_Descripcion = "Timeout al sellar";
                    break;
                case Codigos.SOLDER_PWM_TOO_HIGH:
                    o_Descripcion = "PWM de Sellado Alto";
                    break;
                case Codigos.SOLDER_PWM_TOO_LOW:
                    o_Descripcion = "PWM de Sellado Bajo";
                    break;
                case Codigos.SOLDER_TENSION_TOO_HIGH:
                    o_Descripcion = "Tension de Sellado Alto";
                    break;
                case Codigos.SOLDER_TENSION_TOO_LOW:
                    o_Descripcion = "Tension de Sellado bajo";
                    break;
                case Codigos.SOLDER_CURRENT_TOO_HIGH:
                    o_Descripcion = "Valor de Sellado muy Alto";
                    break;
                case Codigos.SOLDER_ERROR:
                    o_Descripcion = "Error en sellado, solicite un Servicio";
                    break;
                case Codigos.COMUNICATION_ERROR_WITH_ITL_IN_DEPSOIT:
                    o_Descripcion = "Error de comunicación con el Validador durante el Deposito";
                    break;
                case Codigos.COMUNICATION_ERROR_WITH_ITL:
                    o_Descripcion = "Error de comunicación con el Validador";
                    break;
                // WARNINGS
                case Codigos.LOGIN_ALREADY_PRESENT:
                    o_Descripcion = "Existe un Logueo en curso";
                    break;
                case Codigos.SAFE_NOT_OPEN :
                    o_Descripcion = "No se abrio la Bovedad";
                    break;
                case Codigos.BAG_SERIAL_NUMBER_NOT_PRESENT:
                    o_Descripcion = "No hay un numero Serial para la Bolsa/CASSET";
                    break;
                case Codigos.BAG_OR_BOX_NO_PRESENT:
                    o_Descripcion = "Bolsa/CASSET No presente";
                    break;
                case Codigos.BAG_OR_BOX_FULL_BY_NUMBER:
                    o_Descripcion = "Bolsa/CASSET lleno por configuración";
                    break;
                case Codigos.COMMAND_ERROR:
                    o_Descripcion = "Error en Comando enviado";
                    break;
                case Codigos.COMMAND_NOT_EXECUTABLE:
                    o_Descripcion = "Comando no aplicable a este equipo";
                    break;
                case Codigos.NO_USER_LOGED:
                    o_Descripcion = "No hay usuario Logueado";
                    break;
                case Codigos.UKNOW_USER:
                    o_Descripcion = "Usuario Desconocido";
                    break;
                case Codigos.DEVICE_BUSY:
                    o_Descripcion = "Equipo Ocupado, Atorado, en USO";
                    break;
                case Codigos.BAG_ALREADY_SOLDERED_OR_BOX_TO_DRAN_IN_SAFE:
                    o_Descripcion = "Bolsa Sellada o Cassete listo para ser Retirado";
                    break;
                default:
                    o_Descripcion = "U-COIN Error Generico";
                    break;
            }
            o_Descripcion += "Codigo Error_Ucoin Numerico = " + o_nuevo_codigo + " ";
        }
    }
}
