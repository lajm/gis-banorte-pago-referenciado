﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaRecuperacionDeposito : Form
    {
        DepositoEnCurso l_transaccion_pendiente;
        public FormaRecuperacionDeposito(DepositoEnCurso p_Recuperacion)
        {
            InitializeComponent();
            l_transaccion_pendiente = p_Recuperacion;
        }

        private void c_Abandonar_Click(object sender, EventArgs e)
        {
            c_Abandonar.Enabled = false;
            DialogResult l_respuesta;

            using (FormaError v_pregunta = new FormaError(true, true, false, "question"))
            {
                v_pregunta.c_MensajeError.Text = "¿Desea salir sin intentar Recuperar la Transacción?";
                l_respuesta = v_pregunta.ShowDialog();
            }

            if (l_respuesta == DialogResult.OK)
            {
                Close();
            }
            c_Abandonar.Enabled = true;
        }

        private void c_recuperar_Click(object sender, EventArgs e)
        {
            c_recuperar.Enabled = false;

            DialogResult l_respuesta;

            using (FormaAutenticar v_autenticar = new FormaAutenticar(true))
            {
              l_respuesta =  v_autenticar.ShowDialog();
            }

            if (l_respuesta == DialogResult.OK)
            {
                // -- SE RECUPERAR LA ÚLTIMA TRANSACCIÓN TRUNCA
                Globales.EscribirBitacora("FormaPrincipal", "revisarSesion()", "Sesion y transacción recuperada , enviando transacción "
                            + Environment.NewLine + l_transaccion_pendiente.Serializar(), 3);
             int l_reply =    ProcesosSid.EnviarTransaccionTrunca(l_transaccion_pendiente);

                if (l_reply == 0)
                {
                    using (FormaError v_pregunta = new FormaError(true, "question"))
                    {
                        v_pregunta.c_MensajeError.Text = "Moveremos los mecanismos si faltan Billetes abra el Equipo para buscarlos";
                        l_respuesta = v_pregunta.ShowDialog();
                    }

                }
                Close();
            }
            else
            {
                using (FormaError v_pregunta = new FormaError(true, "question"))
                {
                    v_pregunta.c_MensajeError.Text = "Por favor Utilize El password de Supervisor";
                    l_respuesta = v_pregunta.ShowDialog();
                }
              
            }
         
            c_recuperar.Enabled = true;
        }
    }
}
