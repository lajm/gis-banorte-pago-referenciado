﻿namespace Azteca_SID
{
    partial class FormaLogueo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Contraseña = new System.Windows.Forms.TextBox();
            this.C_Usuario = new System.Windows.Forms.TextBox();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_mensaje = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(267, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(257, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Por Favor Identifiquese";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(115, 299);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Contraseña";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(146, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Usuario";
            // 
            // c_Contraseña
            // 
            this.c_Contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Contraseña.Location = new System.Drawing.Point(272, 300);
            this.c_Contraseña.Name = "c_Contraseña";
            this.c_Contraseña.Size = new System.Drawing.Size(321, 29);
            this.c_Contraseña.TabIndex = 1;
            this.c_Contraseña.UseSystemPasswordChar = true;
            this.c_Contraseña.Click += new System.EventHandler(this.c_Contraseña_Click);
            this.c_Contraseña.TextChanged += new System.EventHandler(this.c_Contraseña_TextChanged);
            // 
            // C_Usuario
            // 
            this.C_Usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_Usuario.Location = new System.Drawing.Point(272, 237);
            this.C_Usuario.Name = "C_Usuario";
            this.C_Usuario.Size = new System.Drawing.Size(321, 29);
            this.C_Usuario.TabIndex = 0;
            this.C_Usuario.Click += new System.EventHandler(this.C_Usuario_Click);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonCancelar.BackgroundImage = global::Azteca_SID.Properties.Resources.dialog_cancel_2;
            this.c_BotonCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_BotonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Location = new System.Drawing.Point(456, 373);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(193, 89);
            this.c_BotonCancelar.TabIndex = 3;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Cancelar";
            this.c_BotonCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonAceptar.BackgroundImage = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.c_BotonAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Location = new System.Drawing.Point(186, 373);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(193, 89);
            this.c_BotonAceptar.TabIndex = 2;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_mensaje
            // 
            this.c_mensaje.AutoSize = true;
            this.c_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mensaje.Location = new System.Drawing.Point(148, 334);
            this.c_mensaje.Name = "c_mensaje";
            this.c_mensaje.Size = new System.Drawing.Size(0, 16);
            this.c_mensaje.TabIndex = 7;
            // 
            // FormaLogueo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_mensaje);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Contraseña);
            this.Controls.Add(this.C_Usuario);
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.DoubleBuffered = true;
            this.Name = "FormaLogueo";
            this.Text = "FormaLogueo";
            this.Controls.SetChildIndex(this.c_BotonAceptar, 0);
            this.Controls.SetChildIndex(this.c_BotonCancelar, 0);
            this.Controls.SetChildIndex(this.C_Usuario, 0);
            this.Controls.SetChildIndex(this.c_Contraseña, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.c_mensaje, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_Contraseña;
        private System.Windows.Forms.TextBox C_Usuario;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Label c_mensaje;
    }
}