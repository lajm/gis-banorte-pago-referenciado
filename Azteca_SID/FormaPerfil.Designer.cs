﻿namespace Azteca_SID
{
    partial class FormaPerfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.c_administrador = new System.Windows.Forms.Button();
            this.Sucursal = new System.Windows.Forms.Button();
            this.c_Tecnico = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_administrador
            // 
            this.c_administrador.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_administrador.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_administrador.Location = new System.Drawing.Point(81, 225);
            this.c_administrador.Name = "c_administrador";
            this.c_administrador.Size = new System.Drawing.Size(150, 150);
            this.c_administrador.TabIndex = 20;
            this.c_administrador.Text = "Administrador";
            this.c_administrador.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_administrador.UseVisualStyleBackColor = true;
            this.c_administrador.Click += new System.EventHandler(this.c_administrador_Click);
            // 
            // Sucursal
            // 
            this.Sucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sucursal.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Sucursal.Location = new System.Drawing.Point(325, 225);
            this.Sucursal.Name = "Sucursal";
            this.Sucursal.Size = new System.Drawing.Size(150, 150);
            this.Sucursal.TabIndex = 21;
            this.Sucursal.Text = "Sucursal";
            this.Sucursal.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.Sucursal.UseVisualStyleBackColor = true;
            this.Sucursal.Click += new System.EventHandler(this.Sucursal_Click);
            // 
            // c_Tecnico
            // 
            this.c_Tecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Tecnico.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Tecnico.Location = new System.Drawing.Point(569, 225);
            this.c_Tecnico.Name = "c_Tecnico";
            this.c_Tecnico.Size = new System.Drawing.Size(150, 150);
            this.c_Tecnico.TabIndex = 22;
            this.c_Tecnico.Text = "Tecnico";
            this.c_Tecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Tecnico.UseVisualStyleBackColor = true;
            this.c_Tecnico.Click += new System.EventHandler(this.c_Tecnico_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(228, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(345, 29);
            this.label1.TabIndex = 23;
            this.label1.Text = "SELECCIONE UNA OPCIÓN";
            // 
            // FormaPerfil
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Tecnico);
            this.Controls.Add(this.Sucursal);
            this.Controls.Add(this.c_administrador);
            this.Name = "FormaPerfil";
            this.Text = "Sucursal";
            this.Controls.SetChildIndex(this.c_administrador, 0);
            this.Controls.SetChildIndex(this.Sucursal, 0);
            this.Controls.SetChildIndex(this.c_Tecnico, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_administrador;
        private System.Windows.Forms.Button Sucursal;
        private System.Windows.Forms.Button c_Tecnico;
        private System.Windows.Forms.Label label1;
    }
}
