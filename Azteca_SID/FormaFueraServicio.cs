﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaFueraServicio : Form
    {

        bool Cerradura = false;
        bool PuertaBovedad = false;
        bool Comunicaciones = false;
        bool Bolsapuesta = false;
        bool Tapa_abierta = false;
        bool Hay_Atasco = false;
        bool SinPapel =false;
        bool DipswicthAbierto =false;
        bool Mostar_porcentaje = false;
        bool Imprimir_ticket = true;
        bool EquipoOperable = false;
        bool Envase_valido = false;
        bool Envase_desconocido = false;



        public FormaFueraServicio()
        {
            InitializeComponent();
            Imprimir_ticket = false;
            EnServico( );
            TopMost = true;
        }

        private void limpiar()
        {
            Cerradura = false;
             PuertaBovedad = false;
            Comunicaciones = false;
             Bolsapuesta = false;
             Tapa_abierta = false;
             Hay_Atasco = false;
            SinPapel = false;
            DipswicthAbierto = false;
           EquipoOperable = false;
            Envase_valido = false;



        }

        public FormaFueraServicio(bool p_Conteo_Bolsa , bool p_Envase_deconocido )
        {
            InitializeComponent();
            Mostar_porcentaje = p_Conteo_Bolsa;
            Envase_desconocido = p_Envase_deconocido;

            EnServico( );
            TopMost = true;
        }


        public FormaFueraServicio(bool p_Lock,bool  p_Puerta,bool p_bolsa, bool p_tapa, bool p_red,bool p_Atasco,bool p_sinpapel, bool p_dipswitch)
        {
            InitializeComponent();

            Cerradura = p_Lock;
            PuertaBovedad = p_Puerta;
            Bolsapuesta = p_bolsa;
            Tapa_abierta = p_tapa;
            Comunicaciones = p_red;
            Hay_Atasco = p_Atasco;
            SinPapel = p_sinpapel;
            DipswicthAbierto = p_dipswitch;
            Envase_valido = true;


            Imprimir_ticket = false;

            if ( p_Atasco )
            {
                limpiar( );
                EnServico( );
            }
            TopMost = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormaFueraServicio_Load(object sender, EventArgs e)
        {
            Globales.Alarmado = true;

          // EnServico();

            MostarMensajes();

            if (!Bolsapuesta)
                c_login_ETV.Visible = true;




            if (EquipoOperable)
            {
                Globales.Alarmado = false;
                Close( );
            }
               
        }

        private void MostarMensajes()
        {
            if ( !Properties.Settings.Default.CAJA_EMPRESARIAL )
                e_mensaje_gral.Text = "Por el momento no es posible atenderle por favor acuda a la Sucursal";
            else
            {
               e_mensaje_gral.Text = "Llamar al Administrador o personal de esta empresa autorizado para la revision del equipo";

            }

            e_bolsa.Text = Bolsapuesta ? "" : "Bolsa Mal Colocada";
            e_look.Text = Cerradura ? "" : "EL Dial No esta en su Posición";
            e_tapa.Text = Tapa_abierta ? "" : "Tapa superior Abierta o mal Cerrada";
            e_red.Text = Comunicaciones ? "" : "Verificar Cable de Red";
            e_Puerta.Text = PuertaBovedad ? "" : "Boveda Esta Abierta";
            e_Atasco.Text = Hay_Atasco ? "Atasco de Billetes presente" : "";
            e_papel.Text = SinPapel ? "No hay rollo de Papel en Impresora" : "";
            e_dipshiwtch.Text = DipswicthAbierto ? "Compartimiento Abierto" : "";
            e_envase.Text = Envase_valido ?  "": "# de BOLSA Invalido";

            if (Mostar_porcentaje)
            {
             
                e_porcentaje.Visible = true;
            }

        }

        private void c_login_Admin_Click(object sender, EventArgs e)
        {
            c_actualizar.Enabled = false;
            c_login_Admin.Enabled = false;
            c_login_ETV.Enabled = false;
            this.TopMost = false;

            using (FormaIniciarSesion f_Inicio = new FormaIniciarSesion(true))
            {
                
                f_Inicio.l_IdUsuario = "Supervisor_ZONA";
                f_Inicio.ShowDialog();
            }

            this.TopMost = true;
            c_actualizar.Enabled = true;
            c_login_Admin.Enabled = true;
            c_login_ETV.Enabled = true;
        }

        private void c_login_ETV_Click(object sender, EventArgs e)
        {
            c_actualizar.Enabled = false;
            c_login_Admin.Enabled = false;
            c_login_ETV.Enabled = false;
            this.TopMost = false;

            using (FormaIniciarSesion f_Inicio = new FormaIniciarSesion(true))
            {
               
                f_Inicio.l_IdUsuario = "ETV_Traslado";
                f_Inicio.ShowDialog(FormaPrincipal.s_Forma);
            }

            this.TopMost = true;
            c_actualizar.Enabled = true;
            c_login_Admin.Enabled = true;
            c_login_ETV.Enabled = true;
        }

        private void c_actualizar_Click(object sender, EventArgs e)
        {
            c_actualizar.Enabled = false;
            c_login_Admin.Enabled = false;
            c_login_ETV.Enabled = false;


            limpiar();

            EnServico();

            CheckUPs( );
          

            if (!Bolsapuesta)
                c_login_ETV.Visible = true;
            else
                c_login_ETV.Visible = false;

            MostarMensajes();

            if ( EquipoOperable )
            {
                Globales.Alarmado = false;
                Close( );
            }

            c_actualizar.Enabled = true;
            c_login_Admin.Enabled = true;
            c_login_ETV.Enabled = true;
        }

        private bool EnServico()
        {

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            String l_Error = "";

            
            String l_ip = "";

            Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Revisando Equipo", 1);

                if (Properties.Settings.Default.No_Debug_Pc)
                {
                try
                {
                    System.Threading.Thread.Sleep(300);

                    SidLib.ResetPath();
                    SidLib.ResetError();

                    System.Threading.Thread.Sleep(300);


                    SensarSIDcs.UnitStatus l_Status = SensarSIDcs.GetStatus();
                    System.Threading.Thread.Sleep( 100 );
                    int l_reply = SidLib.SID_PrinterGetStatus( );

                    Globales.EscribirBitacora("Estatus del Equipo", "UnitStatus  KEY : ", key[0].ToString("X") + " : " + l_Error, 1);

                    SidLib.SID_Close();

                    if ( l_reply < 0 )
                    {
                      SinPapel = true;
                      
                    }


                    if ((l_Status.m_Sensor_bag_present == true)
                        // && (l_Status.m_Dip_Switch_elevator_position_DOWN == true)
                         && (l_Status.m_Sensor_bag_open == true))
                        Bolsapuesta = true;

                    if (l_Status.m_Interlock_door_strongbox_closed && !l_Status.m_Sensor_door_open)
                        PuertaBovedad = true;



                    if (!l_Status.m_Code_lock_correct)
                        Cerradura = true;

                    if (!l_Status.m_Interlock_cover_head_open)
                        Tapa_abierta = true;

                    if (l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Paper_jam
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_never_covered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Bag_Hole_sensor_not_uncovered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_exit_feeder_sensor_not_uncovered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_feeder_sensor_not_covered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_front_scanners
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_Reject_bay_sensor_not_uncovered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_switch_path_sensor_not_covered
                || l_Status.m_SenseKey == SensarSIDcs.SenseKeyEnum.Jam_in_withdraw_pocket_sensor_not_covered
                )

                        Hay_Atasco = true;

                    if ( l_Status.m_Sensor_Dip_Switch_Magnetic_door_1_open || l_Status.m_Sensor_Dip_Switch_Magnetic_door_2_open )
                    {
                       DipswicthAbierto = true;
                       
                
                    }

                    if ( l_Status.m_Sensor_Dip_Switch_Leafer_open )
                    {
                        DipswicthAbierto =true;
                    }



                        Globales.EscribirBitacora("Estatus del Equipo", "UnitStatus: ", l_Status.m_SenseKey.ToString() + " : " + l_Error, 1);
                    Globales.EscribirBitacora("Estatus del Equipo", "UnitStatus: ", Newtonsoft.Json.JsonConvert.SerializeObject(l_Status), 1);

                   
                }
                catch (Exception exception)
                {
                    SidLib.SID_Close();
                    Globales.EscribirBitacora("Estatus del Equipo", "UnitStatus: ", " Excepcion encontrada" + " : " + exception, 1);

                    using (FormaError v_error = new FormaError(false, "warning"))
                    {
                        v_error.c_MensajeError.Text = "Por favor espere 30 segundos Aprox. y de en Actualizar";
                        v_error.ShowDialog();



                    }
                    
                }
                }
                else
                {
                    
                    Cerradura = true;
                    PuertaBovedad = true;
                    Bolsapuesta = true;
                    Tapa_abierta = true;
                    Hay_Atasco = false;
                    SinPapel = false;
                DipswicthAbierto = false;
                Envase_valido = true;

                }
            try
            {
                l_ip = GetLocalIPAddress();
            }
            catch(Exception)
            {
                l_ip ="127.0.0.1";
            }

            //if ( !BDSide.BolsaSinRetiro( Globales.NumeroSerieBOLSA ) )
            //    Envase_valido = false;
            //else
            //    Envase_valido = true;
            string l_bolsa;
            if ( Globales.RetiroPendiente( out l_bolsa ) || !BDSide.BolsaSinRetiro( Globales.NumeroSerieBOLSA ) )
                Envase_valido = false;
            else
                Envase_valido = true;


            if (!l_ip.Contains("127.0.0.1") && !String.IsNullOrEmpty(l_ip))
            {
                  Comunicaciones = true;
            }

            if (Envase_valido  && !DipswicthAbierto && !SinPapel && !Hay_Atasco && Tapa_abierta && Bolsapuesta && Cerradura && PuertaBovedad && Comunicaciones && Match())
            {
                EquipoOperable = true;

                if (Imprimir_ticket)
                {
                    ModulePrint imprimir = new ModulePrint();
                    imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);
                    try
                    {
                        String[] datos = Properties.Settings.Default.EdoCdUbi.Split('|');

                        imprimir.AddHeaderLine("Estado:         " + datos[0]);
                        imprimir.AddHeaderLine("Ciudad:         " + datos[1]);
                        imprimir.AddHeaderLine("Ubicación:     " + datos[2]);

                    }
                    catch (Exception exx)
                    {
                        imprimir.AddHeaderLine("Estado:         " + "México");
                        imprimir.AddHeaderLine("Ciudad:         " + "México");
                        imprimir.AddHeaderLine("Ubicación:     " + "Suc. Banorte X");
                        Globales.EscribirBitacora("Ticket ", "Escribir Cabecera", "Puebla|Puebla|Central de Abastos", 3);
                    }

                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("Verificación de Estatus Receptor: " + Properties.Settings.Default.NumSerialEquipo);

                    imprimir.AddHeaderLine("========================================");



                    imprimir.AddHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToShortTimeString());
                    imprimir.AddHeaderLine(" ");

                    imprimir.AddHeaderLine( "COMPARTIMIENTOS : " + ( !DipswicthAbierto ? "CERRADOS" : "Incorrectamente Por favor revise" ) );
                    imprimir.AddHeaderLine( " " );
                    imprimir.AddHeaderLine( "PAPEL DE IMPRESORA: " + ( !SinPapel ? "PRESENTE: " : "No hay rollo de Papel" ) );
                    imprimir.AddHeaderLine( " " );
                    imprimir.AddHeaderLine("CUBIERTA COLOCADA: " + (Tapa_abierta ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("ATORAMIENTO: " + (!Hay_Atasco ? "TRAYECTORIA LIBRE" : "Atasco Presente"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("BOLSA COLOCADA: " + (Bolsapuesta ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("DIAL COLOCADO: " + (Cerradura ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("PUERTA CERRADA: " + (PuertaBovedad ? "CORRECTAMENTE" : "Incorrectamente Por favor revise"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine("COMUNICACIÓN: " + (Comunicaciones ? "CORRECTA " : "Fuera de Linea, Por favor revise"));
                    imprimir.AddHeaderLine(" ");
                    imprimir.AddHeaderLine( "ENVASE VALIDO: " + ( Envase_valido ? "CORRECT0" : "Numero de Bolsa Invalido" ) );
                    imprimir.AddHeaderLine( " " );

                    //if (Mostar_porcentaje)
                    //{
                    //    imprimir.AddHeaderLine("Proximo Envase Operativo: " + Globales.NumeroSerieBOLSA);

                    //    imprimir.AddHeaderLine(" ");
                    //}

                    imprimir.AddFooterLine("Comprobación Realizada: ");
                    imprimir.AddFooterLine("             ----RECEPTOR OPERABLE----  ");
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Conclusion: OPERABLE", 1);

                    try
                    {

                        imprimir.FooterImage = Properties.Resources.ok;



                    }
                    catch (Exception exx)
                    {

                    }



                    imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                    if (Mostar_porcentaje)
                        imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());



                    imprimir.Dispose();
                }

                if (Globales.Estatus == Globales.EstatusReceptor.No_Operable || Globales.Estatus == Globales.EstatusReceptor.Desconocido)
                {
                  
                    if (Properties.Settings.Default.Enviar_AlertasGSI)
                        UtilsComunicacion.AlertaGSI(SidLib.SID_OKAY, "SID_OKAY");
                }
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
            }

            else
            {
                String l_Errores = "";
                EquipoOperable = false;
                //imprimir.AddFooterLine("Comprobación Realizada: ");
                //imprimir.AddFooterLine("      ----RECEPTOR NO  OPERABLE----");
                //imprimir.AddFooterLine("Revise segun las Indicaciones");
                Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Conclusion: NO OPERABLE", 1);
                if ( DipswicthAbierto )
                {
                    l_Error = "Compartimiento Abierto";
                    Globales.EscribirBitacora( "Estatus del Equipo" , "Ticket de comprobación" , "Dipswitch abierto" , 1 );
                    l_Errores += l_Error + ",  ";
                }
                if ( SinPapel)
                {
                    l_Error = "No hay Rollo de Papel";
                    Globales.EscribirBitacora( "Estatus del Equipo" , "Ticket de comprobación" , "Sin Rollo de Papel" , 1 );
                    l_Errores += l_Error + ",  ";
                }

                if (Hay_Atasco)
                {
                    l_Error = "Atasco Presente";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Atasco Presente", 1);
                    l_Errores += l_Error + ",  ";
                }
                if (!Tapa_abierta)
                {
                    l_Error = "Tapa Abierta";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Puerta Superior Abierta", 1);
                    l_Errores += l_Error + ",  ";
                }
                if (!Bolsapuesta) {
                    l_Error = "Bolsa No Presente";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Bolsa mal Puesta", 1);
                    l_Errores += l_Error + ",  ";
                }
                if (!PuertaBovedad)
                {
                    l_Error = "Boveda Abierta";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "Puerta Abierta", 1);
                    l_Errores += l_Error + ",  ";
                }
                if (!Cerradura)
                {
                    l_Error = "Dial Abierto";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "DIAL ABIERTO", 1);
                    l_Errores += l_Error + ",  ";
                }
                if (!Comunicaciones)
                {
                    l_Error = "No Hay Red";
                    Globales.EscribirBitacora("Estatus del Equipo", "Ticket de comprobación", "No hay IP o conexión de RED", 1);
                    l_Errores += l_Error + ",  ";
                }
                if ( !Envase_valido )
                {
                    l_Error = "Envase Invalido";
                    Globales.EscribirBitacora( "Estatus del Equipo" , "Ticket de comprobación" , "Envase Invalido folio en Recolección Previa" , 1 );
                    l_Errores += l_Error + ",  ";
                }

                if ( Globales.Estatus != Globales.EstatusReceptor.Lleno  && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
                {

                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                        UtilsComunicacion.AlertaGSI( -9996 , "Equipo No Operable: " + l_Errores );


                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                }                   
              

            }

               return EquipoOperable;
        }



        public string GetLocalIPAddress()
        {
            var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("IP Local NO Encontrada");
        }

        private bool Match()
        {
            try
            {
                if (Mostar_porcentaje)
                {
                    Double l_numbilletesenbolsa = BDDeposito.ObtenerTotalBilletes();
                    Double l_numMonedasbolsa = BDDeposito.ObtenerTotalMonedas();

                    int l_total_fisico = obtener_cantidad_total(Contenido_en_Bolsa());

                    CalcularStock(l_numbilletesenbolsa, l_numMonedasbolsa);


                    if (l_numbilletesenbolsa != l_total_fisico)
                        return false;

                    return true;
                }else
                    return true;



            }
            catch (Exception exx)
            {
                Globales.EscribirBitacora("Servicio_Ok", "Match conteos", exx.Message, 3);
                return false;
            }
        }

        private ushort[] Contenido_en_Bolsa()
        {
            int contador = 1;
            int l_Reply;
            String l_Error;

            if (Properties.Settings.Default.No_Debug_Pc)
            {
                ushort[] l_totalNotes = new ushort[64];

                l_Reply = SidLib.SID_Open(false);
                SidLib. Error_Sid(l_Reply, out l_Error);
                Globales.EscribirBitacora("Contenido bolsa", "sid_open", l_Reply.ToString(), 1);

                do
                {

                    if (contador % 10 == 0)
                    {
                        l_Reply = SidLib.SID_Open(false);
                        SidLib.Error_Sid(l_Reply, out l_Error);
                        Globales.EscribirBitacora("Contenido bolsa", "sid_open", l_Reply.ToString(), 1);
                    }
                    contador++;

                    if (contador == 100)
                        l_Reply = SidLib.SID_CURRENT_ERROR;

                } while (l_Reply == SidLib.SID_COMMAND_IN_EXECUTION_YET);


                if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_ALREADY_OPEN)
                {
                    contador = 0;
                    do
                    {
                        l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);

                        if (contador == 100)
                            break;

                    } while (l_Reply != SidLib.SID_OKAY);

                    SidLib.Error_Sid(l_Reply, out l_Error);
                    Globales.EscribirBitacora("Contenido en bolsa", "Global total notes obtenido", l_Error, 1);

                    if (l_Reply == SidLib.SID_OKAY)
                    {
                        l_Reply = SidLib.SID_Close();
                       // Escribir_ContenidoBolsa(l_totalNotes);
                        return l_totalNotes;
                    }
                    else
                    {

                        l_Reply = SidLib.SID_Close();
                        return null;
                    }
                }
                else
                {
                    Globales.EscribirBitacora("GetTotalCash", "Sid_open", " No se pudo obtener el total en Bolsa: " + l_Error, 1);
                    return null;
                }
            }
            else
                return null;

        }

        private int obtener_cantidad_total(ushort[] l_datos)
        {
            int l_suma_billetes = 0;
            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    l_suma_billetes += l_datos[i];
                }

            Globales.EscribirBitacora("Deposito", "Contenido bolsa", " Cantidad total en bolsa # de billetes: " + l_suma_billetes, 1);
            return l_suma_billetes;
        }

        private void CalcularStock(Double l_numbilletesenbolsa, Double l_numMonedasbolsa)
        {
            Double l_totalBilletes;
            Double l_totalMonedas;
         

            if (l_numMonedasbolsa == 0)
            {
                //c_stokMonedas.Text = "0.00 %";
            }


            if (l_numbilletesenbolsa == 0)
            {
                Globales.Alertastock_enviada = false;
               e_porcentaje .Text = "% de CUPO 0.00 %";

            }
            else
            {
                Double l_decimal = (l_numbilletesenbolsa / Properties.Settings.Default.CapacidadBolsa);
                Double l_decimalMonedas = (l_numMonedasbolsa / 2000);

                l_totalBilletes = l_numbilletesenbolsa;
                l_totalMonedas = l_numMonedasbolsa;
                l_numbilletesenbolsa = (l_decimal * 100);
                l_numMonedasbolsa = (l_decimalMonedas * 100);
                e_porcentaje.Text = "% de CUPO "+ l_numbilletesenbolsa.ToString("###.##") + "%";
              //  c_stokMonedas.Text = l_numMonedasbolsa.ToString("###.##") + "%";

                if (l_numMonedasbolsa == 0)
                {
              //      c_stokMonedas.Text = "0.00 %";
                }


            
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool CheckUPs( )
        {


            if ( Properties.Settings.Default.No_Debug_Pc )
            {

                bool l_upsON = false;
                bool l_Low_batery = false;
                bool Reserved = false;
                String l_Errores;

                int l_reply = SidLib.SID_Open( false );

                if ( l_reply == SidLib.SID_OKAY || l_reply == SidLib.SID_ALREADY_OPEN )
                {

                    l_reply = SidLib.SID_UPSStatus( ref l_upsON , ref l_Low_batery , ref Reserved );

                    if ( l_reply < 0 )
                    {

                        //Error_Sid( l_reply , out l_Errores );

                        if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada
                                     && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                            UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                        Globales.Alerta_Ups_entrada = true;
                        Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );
                        //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                        //{
                        //    f_Error.c_MensajeError.Text = "Malfuncionamiento. Causa : UPS " + l_Errores;
                        //    f_Error.ShowDialog( );
                        //}
                        return false;
                    }
                    else
                    {
                        if ( !l_upsON )
                        {
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                                UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                            Globales.Alerta_Ups_entrada = true;
                            Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );

                            //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                            //{
                            //    f_Error.c_MensajeError.Text = "Deposito No Posible UPS APAGADO";
                            //    f_Error.ShowDialog( );
                            //}
                            return false;
                        }
                        if ( l_Low_batery )
                        {
                            if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada
                                 && Globales.UPS_Estatus != Globales.EstatusReceptor.No_Operable )
                                UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                            Globales.Alerta_Ups_entrada = true;
                            Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );

                            //using ( FormaError f_Error = new FormaError( true , "malFucionamiento" ) )
                            //{
                            //    f_Error.c_MensajeError.Text = "Deposito No Posible UPS BATERIA Descargada";
                            //    f_Error.ShowDialog( );
                            //}
                            return false;
                        }


                    }

                    SidLib.SID_Close( );
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && Globales.Alerta_Ups_entrada )
                        UtilsComunicacion.AlertaGSI( 96 , "Salida de UPS" );
                    Globales.Alerta_Ups_entrada = false;
                    Globales.UPS_CambioEstatus( Globales.EstatusReceptor.Operable );
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                return true;

        }

        private void e_red_Click(object sender, EventArgs e)
        {

        }
    }
}
