﻿namespace Azteca_SID
{
    partial class FormaRecuperacionDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.e_titulo = new System.Windows.Forms.Label();
            this.c_recuperar = new System.Windows.Forms.Button();
            this.c_Abandonar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Azteca_SID.Properties.Resources.bills;
            this.pictureBox1.Location = new System.Drawing.Point(304, 226);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // e_titulo
            // 
            this.e_titulo.BackColor = System.Drawing.Color.Transparent;
            this.e_titulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_titulo.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_titulo.Location = new System.Drawing.Point(58, 97);
            this.e_titulo.Name = "e_titulo";
            this.e_titulo.Size = new System.Drawing.Size(685, 126);
            this.e_titulo.TabIndex = 9;
            this.e_titulo.Text = "Se ha encontrado un Depósito pendiente por favor avise al  Supervisor  para trata" +
    "r de recuperar y enviar la transacción.";
            this.e_titulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_recuperar
            // 
            this.c_recuperar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_recuperar.Location = new System.Drawing.Point(288, 411);
            this.c_recuperar.Name = "c_recuperar";
            this.c_recuperar.Size = new System.Drawing.Size(225, 80);
            this.c_recuperar.TabIndex = 1;
            this.c_recuperar.Text = "Recuperar Transacción";
            this.c_recuperar.UseVisualStyleBackColor = true;
            this.c_recuperar.Click += new System.EventHandler(this.c_recuperar_Click);
            // 
            // c_Abandonar
            // 
            this.c_Abandonar.AutoSize = true;
            this.c_Abandonar.BackgroundImage = global::Azteca_SID.Properties.Resources.btn_banorte_continuar;
            this.c_Abandonar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_Abandonar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Abandonar.Location = new System.Drawing.Point(649, 519);
            this.c_Abandonar.Margin = new System.Windows.Forms.Padding(0);
            this.c_Abandonar.Name = "c_Abandonar";
            this.c_Abandonar.Size = new System.Drawing.Size(140, 59);
            this.c_Abandonar.TabIndex = 2;
            this.c_Abandonar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.c_Abandonar.UseVisualStyleBackColor = true;
            this.c_Abandonar.Visible = false;
            this.c_Abandonar.Click += new System.EventHandler(this.c_Abandonar_Click);
            // 
            // FormaRecuperacionDeposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte_dep;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_Abandonar);
            this.Controls.Add(this.c_recuperar);
            this.Controls.Add(this.e_titulo);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormaRecuperacionDeposito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormaRecuperacionDeposito";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label e_titulo;
        private System.Windows.Forms.Button c_recuperar;
        private System.Windows.Forms.Button c_Abandonar;
    }
}