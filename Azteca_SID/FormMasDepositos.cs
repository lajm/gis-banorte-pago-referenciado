﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormMasDepositos : Azteca_SID.FormBase
    {
        public FormMasDepositos(bool l_timer):base (l_timer)
        {
            Cursor.Hide( );
            InitializeComponent();
        }

        public FormMasDepositos()
        {

            InitializeComponent();
        }

        private void FormMasDepositos_Load(object sender, EventArgs e)
        {

            if ( ContarDepositos( ) < 5 )
            {
                if ( Globales.IdTipoUsuario != 1 )
                {
                    Close( );
                }
                else
                {


                    string CuentaTmp = Globales.IdUsuario;
                    try
                    {

                        CuentaTmp = CuentaTmp.Replace( System.Environment.NewLine , "" );

                        CuentaTmp = "********" + CuentaTmp.Substring( CuentaTmp.Length - 4 , 4 );

                    }
                    catch ( Exception E )
                    {
                        //throw;
                        c_cuenta.Text = Globales.IdUsuario;
                    }
                    c_cuenta.Text = CuentaTmp;

                    e_referencia.Text += Globales.ReferenciaGSI;
                }
            }else
            {
                Close( );
                MostarMensaje("Por favor Capture su Cuenta nuevamente",true,"aviso" );
                    
            }

            if ( Globales.Alerta_Ups_entrada )
            {

                Close( );
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {

                    f_Error.c_MensajeError.Text = " No hay Alimentacion de Energia  , ...No Podra hacer DEPOSITOS...";
                    f_Error.ShowDialog( );

                }
            }



        }

        private void c_misma_Click(object sender, EventArgs e)
        {
            c_misma.Enabled = false;
            c_otra.Enabled = false;
            Application.DoEvents();

            Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO", 1);

           

            if (Globales.ReferenciaGSI.Contains(Properties.Settings.Default.NumSerialEquipo))
                Globales.ReferenciaGSI = "";

            if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
            {


                Close( );
                using ( FormaVerificarcs v_verificar = new FormaVerificarcs( true ) )
                {

                    v_verificar.ShowDialog( FormaPrincipal.s_Forma );
                }

            }
            else
            {
                String l_nombregsi;
                Double l_montogsi;
                //Cambiar Negacion para Productivo
                if ( Globales.ExisteUsuarioGsi( Globales.IdUsuario , out l_Error , out l_nombregsi , out l_montogsi ) )
                {
                    Globales.EscribirBitacora( "Mas Depositos Misma Cuenta: " + Globales.IdUsuario , " Validacion:  " , l_Error , 3 );

                    Globales.AutenticarUsuarioGsi( Globales.IdUsuario , l_nombregsi , l_montogsi );
                                        
                    if ( l_montogsi > 0 )
                        Globales.EscribirBitacora( "LOGIN" , " NUMERO " + Globales.IdUsuario + "Nuevo Limite del Depositante: " + Globales.Limite_MontoGSI.ToString( "C" ) , "EXITOSO" , 1 );

                    Close( );
                    using ( FormaVerificarcs v_verificar = new FormaVerificarcs( true ) )
                    {

                        v_verificar.ShowDialog( FormaPrincipal.s_Forma );
                    }
                }
                else
                {
                    MostarMensaje( "Por el momento No podemos Atenderler Intente mas tarde" , true , "alto" );
                    Globales.EscribirBitacora( "Mas Depositos Misma Cuenta: " + Globales.IdUsuario , " Validacion:  " , l_Error , 3 );
                }
            }


            Close( );
            c_otra.Enabled = true;
            c_misma.Enabled = true;
        }

        private void c_otra_Click(object sender, EventArgs e)
        {
            c_otra.Enabled = false;
            c_misma.Enabled = false;
            Application.DoEvents( );

            Close( );
            using (FormCuenta l_usuario = new FormCuenta(true))
            {
               
                l_usuario.ShowDialog(FormaPrincipal.s_Forma);
            }
          
            c_otra.Enabled = true;
            c_misma.Enabled = true;
        }

        public int  ContarDepositos()
        {
            int contador_depositos = 0;
            foreach ( Form frm in Application.OpenForms )

            {

                if ( frm.GetType( ) == typeof( Formatrancision) )

                {

                    contador_depositos++;

                  
                }

            }
            return contador_depositos;
        }
    }
}
