﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaActualizarDepositos : Form
    {
        public int total = 0;
        private Thread demoThread = null;
        delegate void runDiebold();

        public  FormaActualizarDepositos()
        {
            InitializeComponent();
            this.demoThread = new Thread(new ThreadStart(this.ThreadProcUnsafe));
        }


        private void ThreadProcUnsafe()
        {
            send();
            runDiebold hf = new runDiebold(changeFrm);
            this.Invoke(hf);
        }

        public void changeFrm()
        {
            Globales.c_keepalive_G.Enabled = true;
            this.Close();
            this.Hide();
        }

        private void Caluculate(int i)
        {
            double pow = Math.Pow(i, i);
        }
        private void FormaActualizarDepositos_Load(object sender, EventArgs e)
        {
            this.demoThread.Start();
            //c_barraProgreso.Maximum = 100000;
            //c_barraProgreso.Step = 1;

            //for (int j = 0; j < 100000; j++)
            //{
            //    Caluculate(j);
            //    c_barraProgreso.PerformStep();
            //}
        }

       public void progresa()
        {           
                c_barraProgreso.Maximum = total + 1;
        }
        public void progesaplus()
        {

            c_barraProgreso.Value++;
        }

        public void send()
        {
            try
            {
                DataTable t_Depositos_Pendientes = BDConsulta.ObtenerDepositosGSIPendientes();
                total = t_Depositos_Pendientes.Rows.Count;
                runDiebold hf = new runDiebold(progresa);
                this.Invoke(hf);
                
                if (t_Depositos_Pendientes.Rows.Count > 0)
                {
                    string l_FolidID;
                    string l_Cuenta;
                    string l_GSICajero;
                    string l_Divisa;
                    string l_fecha;
                    string l_hora;
                    string l_referencia;
                    string l_envase;
                    string l_Monto;

                    DateTime l_Fecha_Envio;
                    DateTime l_Fecha_Valida = DateTime.Parse(t_Depositos_Pendientes.Rows[0][5].ToString());

                    for (int i = 0; i < t_Depositos_Pendientes.Rows.Count; i++)
                    {
                        DataTable l_Detalle;
                        Globales.GsiDeposito = null;

                        l_FolidID = t_Depositos_Pendientes.Rows[i][0].ToString();
                        l_Cuenta = t_Depositos_Pendientes.Rows[i][1].ToString();
                        l_GSICajero = t_Depositos_Pendientes.Rows[i][2].ToString();
                        l_Divisa = t_Depositos_Pendientes.Rows[i][3].ToString();
                        l_fecha = t_Depositos_Pendientes.Rows[i][5].ToString();
                        l_Monto = t_Depositos_Pendientes.Rows[i][4].ToString();
                        l_hora = t_Depositos_Pendientes.Rows[i][6].ToString();
                        l_referencia = t_Depositos_Pendientes.Rows[i][7].ToString();
                        l_envase = t_Depositos_Pendientes.Rows[i][8].ToString();

                        l_Fecha_Envio = DateTime.Parse(l_fecha);

                        if (l_Fecha_Valida.CompareTo(l_Fecha_Envio) > 0)
                            break;


                        l_Detalle = BDDeposito.ObtenerDetalleDeposito(Int32.Parse(l_FolidID));
                        // string l_usuario =BDDeposito.ObtenerUsuarioDeposito( Int32.Parse( l_FolidID ) );




                        if (l_Detalle != null)
                            Globales.GsiDeposito = UtilsComunicacion.PagoReferenciadoGSI(l_Monto, Globales.claveEmisora, "GSI901", "MXP", Convert.ToDateTime(l_fecha).ToString("yyyy-MM-dd"), Convert.ToDateTime(l_hora).ToString("HH:mm:ss")
                                                                   , Int32.Parse(l_FolidID).ToString("D20"), l_referencia, l_envase, l_Detalle);
                        //Globales.GsiDeposito = UtilsComunicacion.DepositoGsi( l_Cuenta , l_GSICajero , l_Divisa , l_fecha , l_hora , Int32.Parse( l_FolidID ).ToString( "D20" )
                        //                                  , l_referencia , l_envase , l_Detalle,null );

                        if (Globales.GsiDeposito != null)
                        {
                            if (Convert.ToBoolean(UtilsComunicacion.Desencriptar(Globales.GsiDeposito.exito)) == true)
                            {

                                if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo)))
                                {
                                    Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "Exito al reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);

                                    BDDeposito.UpdateDepositoGSI(Int32.Parse(l_FolidID));
                                }
                            }
                            else
                            {
                                if (BDDeposito.UpdateDeposito(Int32.Parse(l_FolidID), UtilsComunicacion.Desencriptar(Globales.GsiDeposito.mensaje)))
                                {
                                    Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "Exito al reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);

                                    BDDeposito.UpdateDepositoGSI(Int32.Parse(l_FolidID));
                                }
                            }
                        }
                        else
                            Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "No se pudo reenviar Transaccion GSI Folio Operativo: " + l_FolidID, 3);
                        runDiebold hf2 = new runDiebold(progesaplus);
                        this.Invoke(hf2);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Transaccion GSI Pendiente", "Reenvio Deposito", "No se pudo reenviar Transaccion GSI Folio Operativo: " + ex.Message, 3);
                
            }
        }        
    }
}
