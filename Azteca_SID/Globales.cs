﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using Azteca_SID.GsiPortal;
using UtilsSymSid;
using System.Data;

namespace Azteca_SID
{
    class Globales
    {
        // LEON 2017 
        public static int[] _DenominacionesBilletesMexico2017 = { 20, 50, 100, 200, 500, 1000 };
        public static int m_NumeroBilletesBatch = Properties.Settings.Default.BilletesBatch;
        public static int m_MaxReintentosAtoramientoManual = Properties.Settings.Default.MaxReintentosAtoramientoManual;
        public static int m_MaxReintentosPuertas = Properties.Settings.Default.MaxReintentosPuertas;
        public static int m_MaxReintentosAlimentadorVacio;
        public static string refUno= "/eFUNbh3aSU=", claveEmisora, nombreEmisora;
        // LEON 2017 
        public static System.Windows.Forms.Timer c_keepalive_G;


        public enum
           EstatusReceptor
        {
            Operable = 1,
            Mantenimiento = 2,
            No_Operable = 3,
            Desconocido = 4,
                Apagando =5,
            //18-07-2017 STOCK
            Lleno = 6,

        };

       

        public static String
            IdUsuario
        {
            get { return Globales.m_IdUsuario; }
            set { Globales.m_IdUsuario = value; }
        }

        public static String ALiasIdUsuario
        {
            get { return Globales.m_ALiasIdUsuario; }
            set { Globales.m_ALiasIdUsuario = value; }
        }

        public static int
            IdTipoUsuario
        {
            get { return Globales.m_IdTipoUsuario; }
            set { Globales.m_IdTipoUsuario = value; }
        }

        public static String
            NombreUsuario
        {
            get { return Globales.m_NombreUsuario; }
            set { Globales.m_NombreUsuario = value; }
        }

        public static String ReferenciaUsuario
        {
            get { return Globales.m_ReferenciaUsuario; }
            set { Globales.m_ReferenciaUsuario = value; }
        }
        public static String ConevenioDEMUsuario
        {
            get { return Globales.m_ConevenioDEMUsuario; }
            set { Globales.m_ConevenioDEMUsuario = value; }
        }
        public static String ConvenioCIEUsuario
        {
            get { return Globales.m_ConvenioCIEUsuario; }
            set { Globales.m_ConvenioCIEUsuario = value; }
        }

        public static string IdCliente { get; internal set; }
        public static string NombreCliente { get; internal set; }
        public static string Banco { get; internal set; }
        public static string CuentaBanco { get; internal set; }

        //private static GsiPortal.solDepResponse l_gsiDeposito;
        private static pagoReferenciadoResponse l_gsiDeposito;
        private static GsiPortal.recoleccionResponse l_gsiRecoleccion;

        public static DateTime
            FechaHoraSesion
        {
            get { return Globales.m_FechaHoraSesion; }
            set { Globales.m_FechaHoraSesion = value; }
        }


        public static bool
         AutenticarUsuario( String p_IdUsuario )
        {
            String l_NombreUsuario;
            String l_Referencia;
            String l_ConvenioDEM;
            String l_ConvenioCIE;
            String l_Zona;
            String l_Agencia;

            String l_banco;
            String l_cuentabanco;
            String l_cliente;
            String l_idcliente;
            int l_IdPerfil;

            if ( BDUsuarios.IniciarSesion( p_IdUsuario , out l_NombreUsuario , out l_IdPerfil , out l_Referencia
                , out l_ConvenioDEM , out l_ConvenioCIE , out l_Zona , out l_Agencia ) )
            {
                NombreUsuario = l_NombreUsuario;
                IdTipoUsuario = l_IdPerfil;
                IdUsuario = p_IdUsuario;
                ReferenciaUsuario = l_Referencia;
                ConevenioDEMUsuario = l_ConvenioDEM;
                ConvenioCIEUsuario = l_ConvenioCIE;
                ZonaUsuario = l_Zona;
                AgenciaUsuario = l_Agencia;

                FechaHoraSesion = DateTime.Now;


                BDUsuarios.CuentaAsociada( p_IdUsuario , out l_banco , out l_cuentabanco , out l_cliente , out l_idcliente );

                Banco = l_banco;
                CuentaBanco = l_cuentabanco;
                NombreCliente = l_cliente;
                IdCliente = l_idcliente;
                ReferenciaGSI = "";
                return true;
            }
            else
            {
                NombreUsuario = null;
                IdTipoUsuario = 0;
                IdUsuario = null;
                ReferenciaUsuario = null;
                ConevenioDEMUsuario = null;
                ConvenioCIEUsuario = null;
                ZonaUsuario = null;
                AgenciaUsuario = null;
                ReferenciaGSI = "";
                return false;
            }
        }



        public static bool
            AutenticarUsuario(String p_IdUsuario, String  p_Password)
        {
            String l_NombreUsuario;
            String l_Referencia;
            String l_ConvenioDEM;
            String l_ConvenioCIE;
            String l_Zona;
            String l_Agencia;

            String l_banco;
            String l_cuentabanco;
            String l_cliente;
            String l_idcliente;
            int l_IdPerfil;

            if (BDUsuarios.IniciarSesion(p_IdUsuario, p_Password, out l_NombreUsuario, out l_IdPerfil,out l_Referencia
                ,out l_ConvenioDEM ,out l_ConvenioCIE ,out l_Zona ,out l_Agencia ))
            {
                NombreUsuario = l_NombreUsuario;
                IdTipoUsuario = l_IdPerfil;
                IdUsuario = p_IdUsuario;
                ReferenciaUsuario = l_Referencia;
                ConevenioDEMUsuario = l_ConvenioDEM;
                ConvenioCIEUsuario = l_ConvenioCIE;
                ZonaUsuario = l_Zona;
                AgenciaUsuario = l_Agencia;

                FechaHoraSesion = DateTime.Now;


                BDUsuarios.CuentaAsociada(p_IdUsuario, out l_banco, out l_cuentabanco, out l_cliente, out l_idcliente);

                Banco = l_banco;
                CuentaBanco = l_cuentabanco;
                NombreCliente = l_cliente;
                IdCliente = l_idcliente;
                ReferenciaGSI = "";
                return true;
            }
            else
            {
                NombreUsuario = null;
                IdTipoUsuario = 0;
                IdUsuario = null;
                ReferenciaUsuario = null;
                ConevenioDEMUsuario = null;
                ConvenioCIEUsuario = null;
                ZonaUsuario = null;
                AgenciaUsuario = null;
                ReferenciaGSI = "";
                return false;
            }
        }



        internal static bool AutenticarAdministrador( out string l_IdUsuario , string l_Password )
        {
           l_IdUsuario =  BDUsuarios.TraerAdmin( l_Password);

            if ( !String.IsNullOrEmpty( l_IdUsuario ) )
            {
                return AutenticarUsuario( l_IdUsuario , l_Password );
            }
            else
                return false;
                

        }

        public static bool ExisteUsuario(string l_Identificacion)
        {
           
            return BDUsuarios.ValidarUsuarioExistente(l_Identificacion, 1);
        }

        internal static void AutenticarUsuarioGsi(string l_Identificacion, String l_Nombre , Double l_monto )
        {
            NombreUsuario = l_Nombre;
            Limite_MontoGSI = l_monto;
            IdTipoUsuario = 1;
            IdUsuario = l_Identificacion;
            ReferenciaUsuario = Properties.Settings.Default.NumSerialEquipo;
            FechaHoraSesion = DateTime.Now;

            Banco = "Banorte";
            CuentaBanco = l_Identificacion;
            NombreCliente = "Banorte";
            IdCliente = "000001";

            ReferenciaGSI = "";
        }

        public static bool ExisteUsuarioGsi( string p_cuenta , out string p_mensaje , out string p_nombre , out Double p_monto )
        {
            string l_mensaje; //GSI208
            string l_nombre;
            string l_monto;
            p_monto = -1;


            if ( UtilsComunicacion.LoginGsi( p_cuenta , Properties.Settings.Default.GsiCajero , out l_mensaje , out l_nombre , out l_monto ) )
            {
                p_mensaje = l_mensaje;
                p_nombre = l_nombre;

                if ( !String.IsNullOrWhiteSpace( l_monto ) )
                    if ( Double.TryParse( l_monto , out p_monto ) )
                    {
                        if ( p_monto > 3000000 )
                            p_monto = 0;

                    }


                return true;
            }
            else
            {
                p_mensaje = l_mensaje;
                p_nombre = "";

                return false;
            }


        }
        public static int LlamarTeclado()
        {
            string windir = Environment.GetEnvironmentVariable("WINDIR");
            string osk = null;

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "sysnative"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "system32"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = "osk.exe";
            }


            return System.Diagnostics.Process.Start(osk).Id;
        }

        public static EstatusReceptor Estatus { get; internal set; }
        public static EstatusReceptor Equipo2_Estatus { get; internal set; }

        public static EstatusReceptor UPS_Estatus { get; internal set; }

        public static bool
            ValidarTiempoLog()
        {
            if ((DateTime.Now.Minute - FechaHoraSesion.Minute) > Properties.Settings.Default.TiempoVivo)
                return false;
            else
                return true;
        }

        public static void
            CerrarSesion(String p_IdUsuario)
        {
            NombreUsuario = null;
            IdTipoUsuario = 0;
            IdUsuario = null;
        }

        public static void
            EscribirBitacora(String p_Modulo, String p_Funcion, String p_Mensaje, int nivel_detalle)
        {
            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.ToString("MM_dd_yyyy") + ".txt";
            try { 
           if (nivel_detalle <= Properties.Settings.Default.NivelDetalleBitacora )
            {
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                    {
                        l_Archivo.WriteLine(DateTime.Now.ToString ("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                    }
               
                   
            }

        }catch (Exception exx)
            {
                System.Threading.Thread.Sleep(120);
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                {
                    l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                }
            }
            
           
        }

        public static void BorrarRetiroPendiente( )
        {
            try
            {
                if ( File.Exists( "RetiroPendiente.txt" ) )
                {
                    File.Delete( "RetiroPendiente.txt" );
                }
            }
            catch ( Exception exx )
            {

            }

        }
        public static bool RetiroPendiente( out string NumeroBolsa )
        {
            NumeroBolsa = "";
            try
            {
                // TODO: ¿Es necesario validar la longitud del número de bolsa?
                if ( File.Exists( "RetiroPendiente.txt" ) )
                {
                    String[] lineas = File.ReadAllLines( "RetiroPendiente.txt" );

                    if ( lineas.Length > 0 )
                    {
                        string BOLSAPendiente = lineas[0].Trim( ).Replace( " " , "" );

                        if ( !String.IsNullOrEmpty( BOLSAPendiente ) )
                        {
                            NumeroBolsa = BOLSAPendiente;
                            return true;
                        }
                        else
                            return false;


                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch ( Exception exc )
            {
                return false;
            }
        }

        public static void
       EscribirRetiroPendiente( String p_Mensaje )
        {
            using ( StreamWriter l_Archivo = File.CreateText( "RetiroPendiente.txt" ) )
            {
                l_Archivo.WriteLine( p_Mensaje );
            }
        }

        public static void
         EscribirBitacora(String p_Cadena)
        {
            return;
            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.ToString( "MM_dd_yyyy" ) + ".txt";

            try
            {
                l_log = UtilsSid.NombreBitacoraSid(DateTime.Now);
            }
            catch (Exception E) { }
            try
            {
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                {
                    l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Cadena + Environment.NewLine);
                }
            }
            catch (Exception exx)
            {
                System.Threading.Thread.Sleep(120);
                try
                {
                    using (StreamWriter l_Archivo = File.AppendText(l_log))
                    {
                        l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Cadena + Environment.NewLine);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public static void
       GSI_Bitacora( String p_Modulo , String p_Funcion , String p_Mensaje , int nivel_detalle )
        {
            string l_log = ".\\Bitacoras\\GSI_Bitacora_" + DateTime.Now.ToString( "MM_dd_yyyy" ) + ".txt";
            try
            {

                if ( nivel_detalle <= Properties.Settings.Default.NivelDetalleBitacora )
                {
                    using ( StreamWriter l_Archivo = File.AppendText( l_log ) )
                    {
                        l_Archivo.WriteLine( DateTime.Now.ToString( "dd/MM/yyyy HH:mm:ss:fff" ) + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje );
                    }


                }
            }
            catch ( Exception exx )
            {
                System.Threading.Thread.Sleep( 120 );
                using ( StreamWriter l_Archivo = File.AppendText( l_log ) )
                {
                    l_Archivo.WriteLine( DateTime.Now.ToString( "dd/MM/yyyy HH:mm:ss:fff" ) + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje );
                }

            }


        }

        public static void
          EscribirBitacoraAzteca(String p_Modulo, String p_Funcion, String p_Mensaje, int nivel_detalle)
        {
            string l_log = ".\\Bitacoras\\RegistroAzteca.txt";
            if (nivel_detalle <= Properties.Settings.Default.NivelDetalleBitacora)
            {
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                {
                    l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                }


            }


        }

        public static void
          EscribirEstatus( String p_Mensaje )
        {
            using ( StreamWriter l_Archivo = File.CreateText( "EstatusPrevio.txt" ) )
            {
                l_Archivo.WriteLine( p_Mensaje );
            }
        }

        public static void
         EscribirEstatusEquipo2( String p_Mensaje )
        {
            using ( StreamWriter l_Archivo = File.CreateText( "EstatusPrevio_Equipo2.txt" ) )
            {
                l_Archivo.WriteLine( p_Mensaje );
            }
        }

        public static void
          EscribirEstatus_UPS( String p_Mensaje )
        {
            using ( StreamWriter l_Archivo = File.CreateText( "EstatusPrevio_UPS.txt" ) )
            {
                l_Archivo.WriteLine( p_Mensaje );
            }
        }

        public static void CambioEstatusReceptor( Globales.EstatusReceptor p_nuevoEstado )
        {
            //18-07-2017 STOCK
            if ( Globales.Estatus != EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {
                Globales.Estatus = p_nuevoEstado;
                EscribirEstatus( p_nuevoEstado.ToString( ) );
                Globales.EscribirBitacora( "CambioStatusReceptor()" , "Cambio Asignado: " , p_nuevoEstado.ToString( ) , 3 );
            }
            else
                Globales.EscribirBitacora( "CambioStatusReceptor()" , "Cambio NO asignado: " , p_nuevoEstado.ToString( ) , 3 );
                //EscribirEstatus( Globales.Estatus.ToString()  );
        }

        public static void Equipo2_CambioEstatusReceptor( Globales.EstatusReceptor p_nuevoEstado )
        {
           
                Globales.Equipo2_Estatus= p_nuevoEstado;
               EscribirEstatusEquipo2( p_nuevoEstado.ToString( ) );
            Globales.EscribirBitacora( "CambioStatus_Equipo2()" , "Cambio Asignado: " , p_nuevoEstado.ToString( ) , 3 );

        }

        public static void UPS_CambioEstatus( Globales.EstatusReceptor p_nuevoEstado )
        {

            Globales.UPS_Estatus = p_nuevoEstado;
            EscribirEstatus_UPS( p_nuevoEstado.ToString( ) );
            Globales.EscribirBitacora( "UPS_Status()" , "Cambio Asignado: " , p_nuevoEstado.ToString( ) , 3 );

        }

        public static void LeerUltimoEstatus( )
        {

           
            //SID
            try
            {
                if ( File.Exists( "EstatusPrevio.txt" ) )
                {
                    String[] lineas = File.ReadAllLines( "EstatusPrevio.txt" );

                    if ( lineas.Length > 0 )
                    {
                        Globales.Estatus = (EstatusReceptor) Enum.Parse( typeof( EstatusReceptor ) , lineas[0] );

                    }
                    else
                        CambioEstatusReceptor( EstatusReceptor.Desconocido );

                }
                else
                    CambioEstatusReceptor( EstatusReceptor.Desconocido );



            }catch
            {
                CambioEstatusReceptor( EstatusReceptor.Desconocido );
            }

            //Equipo 2

            if (Properties.Settings.Default.Port_YUGO >0)
            try
            {
                if ( File.Exists( "EstatusPrevio_Equipo2.txt" ) )
                {
                    String[] lineas = File.ReadAllLines( "EstatusPrevio_Equipo2.txt" );

                    if ( lineas.Length > 0 )
                    {
                        Globales.Equipo2_Estatus = (EstatusReceptor) Enum.Parse( typeof( EstatusReceptor ) , lineas[0] );

                    }
                    else
                        Equipo2_CambioEstatusReceptor( EstatusReceptor.Desconocido );

                }
                else
                    Equipo2_CambioEstatusReceptor( EstatusReceptor.Desconocido );



            }
            catch
            {
                Equipo2_CambioEstatusReceptor( EstatusReceptor.Desconocido );
            }

            // UPS

            try
            {
                if ( File.Exists( "EstatusPrevio_UPS.txt" ) )
                {
                    String[] lineas = File.ReadAllLines( "EstatusPrevio_UPS.txt" );

                    if ( lineas.Length > 0 )
                    {
                        Globales.UPS_Estatus = (EstatusReceptor) Enum.Parse( typeof( EstatusReceptor ) , lineas[0] );

                    }
                    else
                        UPS_CambioEstatus( EstatusReceptor.Desconocido );

                }
                else
                    UPS_CambioEstatus( EstatusReceptor.Desconocido );



            }
            catch
            {
                UPS_CambioEstatus( EstatusReceptor.Desconocido );
            }

            // Revisar BD para Mantenimiento Abierto
            DataTable l_pendientes = BDMantenimiento.ObtenerConsulta_mantenimientos_abiertos( );
            if ( l_pendientes.Rows.Count > 0 )
                Globales.CambioEstatusReceptor( EstatusReceptor.Mantenimiento );


        }


        public static void LeerBolsaPuesta()
        {
            try
            {
                // TODO: ¿Es necesario validar la longitud del número de bolsa?
                if ( File.Exists( "NumeroBOLSA.txt" ) )
                {
                    String[] lineas = File.ReadAllLines( "NumeroBolsa.txt" );

                    if ( lineas.Length > 0 )
                    {
                        Globales.NumeroSerieBOLSA = lineas[0].Trim( ).Replace( " " , "" );

                        if ( String.IsNullOrEmpty( Globales.NumeroSerieBOLSA ) )
                            Globales.NumeroSerieBOLSA = "000000";

                        try
                        {
                            decimal resp = Convert.ToDecimal( Globales.NumeroSerieBOLSA );
                        }
                        catch //caso contrario, es falso.
                        {
                            Globales.NumeroSerieBOLSA = "000000";
                        }
                    }
                    else
                        Globales.NumeroSerieBOLSA = "000000";
                }
                else
                    Globales.NumeroSerieBOLSA = "000000";
            }catch (Exception exc)
            {
                Globales.NumeroSerieBOLSA = "000000";
            }
               
        }

     

        private static String m_ALiasIdUsuario;
        private static String m_IdUsuario;
        private static int m_IdTipoUsuario;
        private static String m_IdCuenta;
        private static String m_AliasCuenta;
        private static String m_NombreUsuario;
        private static String m_ReferenciaUsuario;
        private static String m_ReferenciaGsi;
        private static String m_ConevenioDEMUsuario;
        private static String m_ConvenioCIEUsuario;
        private static String m_ZonaUsuario;
        private static String m_AgenciaUsuario;
        private static String m_WorkStation;

        

        private static DateTime m_FechaHoraSesion;
        public static bool IniciarSesion=true;
        public static bool EnDeposito_ahora = false;
       
        public static bool Alertastock_enviada = false;
        public static bool Alertastock_Equipo_lleno = false;
        public static bool Alerta_Ups_entrada = false;
        public static bool BolsaCorrecta = true;
        public static String NumeroSerieBOLSA = "";
        public static String NumeroSerieBOLSAanterior = "";

        private static bool configuracionGSI = false;
        private static String usuarioGSI = "";
        private static String contraseñaGsi = "";
        private static String llaveGsi = "";
        private static DateTime m_FechaInicioTransaccionEnviada;
        private static Decimal m_ImporteTotalReceptor;
        private static int m_Numero_Sesion;
        private static Double m_Limite_MontoGSI;
        public static bool onLine = false;

        public static int SensorRemoverBolsa
        {

            get
            {

                if (Properties.Settings.Default.Selenoide)
                    return 63;
                else
                    return 127;


            }

        }

        public static string ZonaUsuario { get; private set; }
        public static string AgenciaUsuario { get; private set; }

        public static string WorkStation
        {
            get
            {
                return m_WorkStation;
            }

            set
            {
                m_WorkStation = value;
            }
        }

        //public static solDepResponse GsiDeposito
        public static pagoReferenciadoResponse GsiDeposito
        {
            get
            {
                return l_gsiDeposito;
            }

            set
            {
                l_gsiDeposito = value;
            }
        }

        public static recoleccionResponse GsiRecoleccion
        {
            get
            {
                return l_gsiRecoleccion;
            }

            set
            {
                l_gsiRecoleccion = value;
            }
        }

        public static bool ConfiguracionGSI
        {
            get
            {
                return configuracionGSI;
            }

            set
            {
                configuracionGSI = value;
            }
        }

        public static string UsuarioGSI
        {
            get
            {
                return usuarioGSI;
            }

            set
            {
                usuarioGSI = value;
            }
        }

        public static string ContraseñaGsi
        {
            get
            {
                return contraseñaGsi;
            }

            set
            {
                contraseñaGsi = value;
            }
        }

        public static string LlaveGsi
        {
            get
            {
                return llaveGsi;
            }

            set
            {
                llaveGsi = value;
            }
        }

        public static string ReferenciaGSI
        {
            get
            {
                return m_ReferenciaGsi;
            }

            set
            {
                m_ReferenciaGsi = value;
            }
        }

        public static DateTime FechaInicioTransaccionEnviada
        {
            get
            {
                return m_FechaInicioTransaccionEnviada;
            }

            set
            {
                m_FechaInicioTransaccionEnviada = value;
            }
        }

        public static decimal ImporteTotalReceptor
        {
            get
            {
                return m_ImporteTotalReceptor;
            }

            set
            {
                m_ImporteTotalReceptor = value;
            }
        }

        public static int Numero_Sesion
        {
            get
            {
                return m_Numero_Sesion;
            }

            set
            {
                m_Numero_Sesion = value;
            }
        }

        public static bool Alarmado { get; internal set; }

        public static double Limite_MontoGSI
        {
            get
            {
                return m_Limite_MontoGSI;
            }

            set
            {
                m_Limite_MontoGSI = value;
            }
        }
    }
}
