﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaDatosDeposito : Azteca_SID.FormBase
    {
        GsiPortal.solDepResponse l_depositoActual;
        public FormaDatosDeposito(GsiPortal.solDepResponse p_depositoActual, bool p_cierreAutomatico):base(
            p_cierreAutomatico)
        {
            InitializeComponent();
            l_depositoActual = p_depositoActual;

        }

        private void FormaDatosDeposito_Load(object sender, EventArgs e)
        {
            c_fecha.Text = UtilsComunicacion.Desencriptar( l_depositoActual.fecha);
            c_hora.Text = UtilsComunicacion.Desencriptar(l_depositoActual.hora);
            c_serial.Text = UtilsComunicacion.Desencriptar(l_depositoActual.idCajero);
            c_CR.Text = UtilsComunicacion.Desencriptar(l_depositoActual.cr);
            c_cuenta.Text = UtilsComunicacion.Desencriptar(l_depositoActual.cuenta);
            c_numSecuencia.Text = UtilsComunicacion.Desencriptar(l_depositoActual.secuencia);
            c_Referencia.Text = UtilsComunicacion.Desencriptar(l_depositoActual.referencia);
            c_Folio.Text = UtilsComunicacion.Desencriptar(l_depositoActual.movimiento);
            c_divisa.Text = UtilsComunicacion.Desencriptar(l_depositoActual.divisa);
            c_importe.Text = UtilsComunicacion.Desencriptar(l_depositoActual.importe);
            c_importeLetra.Text = UtilsComunicacion.Desencriptar(l_depositoActual.importeLetra);
            c_claveRastreo .Text = UtilsComunicacion.Desencriptar(l_depositoActual.claveRastreo);
            c_mensaje.Text = UtilsComunicacion.Desencriptar(l_depositoActual.mensaje);
            c_mensaje.Location = new System.Drawing.Point(400 - c_mensaje.Size.Width / 2, 510);

            this.Activate();
        }
    }
}
