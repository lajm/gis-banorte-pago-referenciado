﻿namespace Azteca_SID
{
    partial class FormaActividades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OPERACIONES = new System.Windows.Forms.TabControl();
            this.Admin = new System.Windows.Forms.TabPage();
            this.c_administrarUsuarios = new System.Windows.Forms.Button();
            this.c_contabilidad = new System.Windows.Forms.Button();
            this.c_OpcionConsultaEfectivo = new System.Windows.Forms.Button();
            this.c_OpcionConsultaOperacion = new System.Windows.Forms.Button();
            this.Sistema = new System.Windows.Forms.TabPage();
            this.c_numeroBolsa = new System.Windows.Forms.Button();
            this.c_Reset = new System.Windows.Forms.Button();
            this.c_CerrarSistema = new System.Windows.Forms.Button();
            this.c_OpcionCreaLayout = new System.Windows.Forms.Button();
            this.Etv = new System.Windows.Forms.TabPage();
            this.c_reset_retiro = new System.Windows.Forms.Button();
            this.c_AperturaForzada = new System.Windows.Forms.Button();
            this.c_OpcionRetiroETV = new System.Windows.Forms.Button();
            this.Mantenimiento = new System.Windows.Forms.TabPage();
            this.c_registroSalida = new System.Windows.Forms.Button();
            this.c_ticket = new System.Windows.Forms.Button();
            this.c_registro = new System.Windows.Forms.Button();
            this.c_ConsultaMante = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BotonSalir = new System.Windows.Forms.Button();
            this.e_Pefil = new System.Windows.Forms.Label();
            this.OPERACIONES.SuspendLayout();
            this.Admin.SuspendLayout();
            this.Sistema.SuspendLayout();
            this.Etv.SuspendLayout();
            this.Mantenimiento.SuspendLayout();
            this.SuspendLayout();
            // 
            // OPERACIONES
            // 
            this.OPERACIONES.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.OPERACIONES.Controls.Add(this.Admin);
            this.OPERACIONES.Controls.Add(this.Sistema);
            this.OPERACIONES.Controls.Add(this.Etv);
            this.OPERACIONES.Controls.Add(this.Mantenimiento);
            this.OPERACIONES.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OPERACIONES.Location = new System.Drawing.Point(23, 109);
            this.OPERACIONES.Name = "OPERACIONES";
            this.OPERACIONES.SelectedIndex = 0;
            this.OPERACIONES.ShowToolTips = true;
            this.OPERACIONES.Size = new System.Drawing.Size(737, 384);
            this.OPERACIONES.TabIndex = 20;
            // 
            // Admin
            // 
            this.Admin.BackColor = System.Drawing.SystemColors.Info;
            this.Admin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Admin.Controls.Add(this.c_administrarUsuarios);
            this.Admin.Controls.Add(this.c_contabilidad);
            this.Admin.Controls.Add(this.c_OpcionConsultaEfectivo);
            this.Admin.Controls.Add(this.c_OpcionConsultaOperacion);
            this.Admin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Admin.Location = new System.Drawing.Point(4, 32);
            this.Admin.Name = "Admin";
            this.Admin.Padding = new System.Windows.Forms.Padding(3);
            this.Admin.Size = new System.Drawing.Size(729, 348);
            this.Admin.TabIndex = 0;
            this.Admin.Text = "ADMINISTRACIÓN";
            this.Admin.ToolTipText = "Opciones administrativas";
            this.Admin.UseVisualStyleBackColor = true;
            // 
            // c_administrarUsuarios
            // 
            this.c_administrarUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_administrarUsuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_administrarUsuarios.Location = new System.Drawing.Point(379, 99);
            this.c_administrarUsuarios.Name = "c_administrarUsuarios";
            this.c_administrarUsuarios.Size = new System.Drawing.Size(150, 150);
            this.c_administrarUsuarios.TabIndex = 19;
            this.c_administrarUsuarios.Text = "Administrar Usuarios";
            this.c_administrarUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_administrarUsuarios.UseVisualStyleBackColor = true;
            this.c_administrarUsuarios.Click += new System.EventHandler(this.c_administrarUsuarios_Click);
            // 
            // c_contabilidad
            // 
            this.c_contabilidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_contabilidad.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_contabilidad.Location = new System.Drawing.Point(559, 99);
            this.c_contabilidad.Name = "c_contabilidad";
            this.c_contabilidad.Size = new System.Drawing.Size(150, 150);
            this.c_contabilidad.TabIndex = 17;
            this.c_contabilidad.Text = "Contabilidad 0";
            this.c_contabilidad.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_contabilidad.UseVisualStyleBackColor = true;
            this.c_contabilidad.Click += new System.EventHandler(this.c_contabilidad_Click);
            // 
            // c_OpcionConsultaEfectivo
            // 
            this.c_OpcionConsultaEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionConsultaEfectivo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionConsultaEfectivo.Location = new System.Drawing.Point(19, 99);
            this.c_OpcionConsultaEfectivo.Name = "c_OpcionConsultaEfectivo";
            this.c_OpcionConsultaEfectivo.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionConsultaEfectivo.TabIndex = 9;
            this.c_OpcionConsultaEfectivo.Text = "Consulta de Efectivo";
            this.c_OpcionConsultaEfectivo.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionConsultaEfectivo.UseVisualStyleBackColor = true;
            this.c_OpcionConsultaEfectivo.Click += new System.EventHandler(this.c_OpcionConsultaEfectivo_Click);
            // 
            // c_OpcionConsultaOperacion
            // 
            this.c_OpcionConsultaOperacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionConsultaOperacion.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionConsultaOperacion.Location = new System.Drawing.Point(199, 99);
            this.c_OpcionConsultaOperacion.Name = "c_OpcionConsultaOperacion";
            this.c_OpcionConsultaOperacion.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionConsultaOperacion.TabIndex = 10;
            this.c_OpcionConsultaOperacion.Text = "Consulta de Operaciones";
            this.c_OpcionConsultaOperacion.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionConsultaOperacion.UseVisualStyleBackColor = true;
            this.c_OpcionConsultaOperacion.Click += new System.EventHandler(this.c_OpcionConsultaOperacion_Click);
            // 
            // Sistema
            // 
            this.Sistema.BackColor = System.Drawing.Color.IndianRed;
            this.Sistema.Controls.Add(this.c_numeroBolsa);
            this.Sistema.Controls.Add(this.c_Reset);
            this.Sistema.Controls.Add(this.c_CerrarSistema);
            this.Sistema.Controls.Add(this.c_OpcionCreaLayout);
            this.Sistema.Location = new System.Drawing.Point(4, 32);
            this.Sistema.Name = "Sistema";
            this.Sistema.Padding = new System.Windows.Forms.Padding(3);
            this.Sistema.Size = new System.Drawing.Size(729, 348);
            this.Sistema.TabIndex = 1;
            this.Sistema.Text = "SISTEMA";
            this.Sistema.ToolTipText = "Opciones de Sistema";
            this.Sistema.UseVisualStyleBackColor = true;
            // 
            // c_numeroBolsa
            // 
            this.c_numeroBolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numeroBolsa.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_numeroBolsa.Location = new System.Drawing.Point(210, 99);
            this.c_numeroBolsa.Name = "c_numeroBolsa";
            this.c_numeroBolsa.Size = new System.Drawing.Size(150, 150);
            this.c_numeroBolsa.TabIndex = 17;
            this.c_numeroBolsa.Text = "Ingresar # Bolsa manualmente";
            this.c_numeroBolsa.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_numeroBolsa.UseVisualStyleBackColor = true;
            this.c_numeroBolsa.Click += new System.EventHandler(this.c_numeroBolsa_Click);
            // 
            // c_Reset
            // 
            this.c_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Reset.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Reset.Location = new System.Drawing.Point(52, 99);
            this.c_Reset.Name = "c_Reset";
            this.c_Reset.Size = new System.Drawing.Size(150, 150);
            this.c_Reset.TabIndex = 15;
            this.c_Reset.Text = "RESET AL SISTEMA";
            this.c_Reset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Reset.UseVisualStyleBackColor = true;
            this.c_Reset.Click += new System.EventHandler(this.c_Reset_Click);
            // 
            // c_CerrarSistema
            // 
            this.c_CerrarSistema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_CerrarSistema.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_CerrarSistema.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_CerrarSistema.Location = new System.Drawing.Point(526, 99);
            this.c_CerrarSistema.Name = "c_CerrarSistema";
            this.c_CerrarSistema.Size = new System.Drawing.Size(150, 150);
            this.c_CerrarSistema.TabIndex = 13;
            this.c_CerrarSistema.Text = "CERRAR SISTEMA";
            this.c_CerrarSistema.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_CerrarSistema.UseVisualStyleBackColor = true;
            this.c_CerrarSistema.Click += new System.EventHandler(this.c_CerrarSistema_Click);
            // 
            // c_OpcionCreaLayout
            // 
            this.c_OpcionCreaLayout.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionCreaLayout.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionCreaLayout.Location = new System.Drawing.Point(368, 99);
            this.c_OpcionCreaLayout.Name = "c_OpcionCreaLayout";
            this.c_OpcionCreaLayout.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionCreaLayout.TabIndex = 11;
            this.c_OpcionCreaLayout.Text = "Consola Monedas";
            this.c_OpcionCreaLayout.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionCreaLayout.UseVisualStyleBackColor = true;
            this.c_OpcionCreaLayout.Click += new System.EventHandler(this.c_OpcionCreaLayout_Click);
            // 
            // Etv
            // 
            this.Etv.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Etv.Controls.Add(this.c_reset_retiro);
            this.Etv.Controls.Add(this.c_AperturaForzada);
            this.Etv.Controls.Add(this.c_OpcionRetiroETV);
            this.Etv.Location = new System.Drawing.Point(4, 32);
            this.Etv.Name = "Etv";
            this.Etv.Padding = new System.Windows.Forms.Padding(3);
            this.Etv.Size = new System.Drawing.Size(729, 348);
            this.Etv.TabIndex = 2;
            this.Etv.Text = "RETIRO ";
            this.Etv.ToolTipText = "Retiro Bolsa corte";
            this.Etv.UseVisualStyleBackColor = true;
            // 
            // c_reset_retiro
            // 
            this.c_reset_retiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_reset_retiro.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_reset_retiro.Location = new System.Drawing.Point(287, 92);
            this.c_reset_retiro.Name = "c_reset_retiro";
            this.c_reset_retiro.Size = new System.Drawing.Size(150, 150);
            this.c_reset_retiro.TabIndex = 16;
            this.c_reset_retiro.Text = "RESET AL SISTEMA";
            this.c_reset_retiro.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_reset_retiro.UseVisualStyleBackColor = true;
            this.c_reset_retiro.Click += new System.EventHandler(this.c_reset_retiro_Click);
            // 
            // c_AperturaForzada
            // 
            this.c_AperturaForzada.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_AperturaForzada.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_AperturaForzada.Location = new System.Drawing.Point(506, 92);
            this.c_AperturaForzada.Name = "c_AperturaForzada";
            this.c_AperturaForzada.Size = new System.Drawing.Size(150, 150);
            this.c_AperturaForzada.TabIndex = 9;
            this.c_AperturaForzada.Text = "APERTURA FORZADA DE BOVEDA";
            this.c_AperturaForzada.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_AperturaForzada.UseVisualStyleBackColor = true;
            this.c_AperturaForzada.Click += new System.EventHandler(this.c_AperturaForzada_Click);
            // 
            // c_OpcionRetiroETV
            // 
            this.c_OpcionRetiroETV.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionRetiroETV.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionRetiroETV.Location = new System.Drawing.Point(70, 92);
            this.c_OpcionRetiroETV.Name = "c_OpcionRetiroETV";
            this.c_OpcionRetiroETV.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionRetiroETV.TabIndex = 8;
            this.c_OpcionRetiroETV.Text = "RETIRO ETV ";
            this.c_OpcionRetiroETV.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionRetiroETV.UseVisualStyleBackColor = true;
            this.c_OpcionRetiroETV.Click += new System.EventHandler(this.c_OpcionRetiroETV_Click);
            // 
            // Mantenimiento
            // 
            this.Mantenimiento.BackColor = System.Drawing.Color.RoyalBlue;
            this.Mantenimiento.Controls.Add(this.c_registroSalida);
            this.Mantenimiento.Controls.Add(this.c_ticket);
            this.Mantenimiento.Controls.Add(this.c_registro);
            this.Mantenimiento.Controls.Add(this.c_ConsultaMante);
            this.Mantenimiento.Location = new System.Drawing.Point(4, 32);
            this.Mantenimiento.Name = "Mantenimiento";
            this.Mantenimiento.Size = new System.Drawing.Size(729, 348);
            this.Mantenimiento.TabIndex = 3;
            this.Mantenimiento.Text = "MANTENIMIENTO";
            // 
            // c_registroSalida
            // 
            this.c_registroSalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_registroSalida.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_registroSalida.Location = new System.Drawing.Point(379, 99);
            this.c_registroSalida.Name = "c_registroSalida";
            this.c_registroSalida.Size = new System.Drawing.Size(150, 150);
            this.c_registroSalida.TabIndex = 23;
            this.c_registroSalida.Text = "Registrar Salida de Mantenimiento";
            this.c_registroSalida.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_registroSalida.UseVisualStyleBackColor = true;
            this.c_registroSalida.Click += new System.EventHandler(this.c_registroSalida_Click);
            // 
            // c_ticket
            // 
            this.c_ticket.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ticket.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_ticket.Location = new System.Drawing.Point(559, 99);
            this.c_ticket.Name = "c_ticket";
            this.c_ticket.Size = new System.Drawing.Size(150, 150);
            this.c_ticket.TabIndex = 22;
            this.c_ticket.Text = "Ticket Comprobante";
            this.c_ticket.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_ticket.UseVisualStyleBackColor = true;
            this.c_ticket.Click += new System.EventHandler(this.c_ticket_Click);
            // 
            // c_registro
            // 
            this.c_registro.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_registro.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_registro.Location = new System.Drawing.Point(19, 99);
            this.c_registro.Name = "c_registro";
            this.c_registro.Size = new System.Drawing.Size(150, 150);
            this.c_registro.TabIndex = 20;
            this.c_registro.Text = "Registrar Mantenimiento";
            this.c_registro.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_registro.UseVisualStyleBackColor = true;
            this.c_registro.Click += new System.EventHandler(this.c_registro_Click);
            // 
            // c_ConsultaMante
            // 
            this.c_ConsultaMante.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ConsultaMante.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_ConsultaMante.Location = new System.Drawing.Point(199, 99);
            this.c_ConsultaMante.Name = "c_ConsultaMante";
            this.c_ConsultaMante.Size = new System.Drawing.Size(150, 150);
            this.c_ConsultaMante.TabIndex = 21;
            this.c_ConsultaMante.Text = "Consulta de Mantenimientos";
            this.c_ConsultaMante.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_ConsultaMante.UseVisualStyleBackColor = true;
            this.c_ConsultaMante.Click += new System.EventHandler(this.c_ConsultaMante_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(228, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(345, 29);
            this.label1.TabIndex = 19;
            this.label1.Text = "SELECCIONE UNA OPCIÓN";
            // 
            // c_BotonSalir
            // 
            this.c_BotonSalir.BackgroundImage = global::Azteca_SID.Properties.Resources.system_log_out_3;
            this.c_BotonSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_BotonSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonSalir.Location = new System.Drawing.Point(614, 499);
            this.c_BotonSalir.Name = "c_BotonSalir";
            this.c_BotonSalir.Size = new System.Drawing.Size(158, 45);
            this.c_BotonSalir.TabIndex = 18;
            this.c_BotonSalir.Text = "Salir";
            this.c_BotonSalir.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_BotonSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonSalir.UseVisualStyleBackColor = true;
            this.c_BotonSalir.Visible = false;
            this.c_BotonSalir.Click += new System.EventHandler(this.c_BotonSalir_Click);
            // 
            // e_Pefil
            // 
            this.e_Pefil.BackColor = System.Drawing.Color.Transparent;
            this.e_Pefil.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_Pefil.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_Pefil.Location = new System.Drawing.Point(23, 497);
            this.e_Pefil.Name = "e_Pefil";
            this.e_Pefil.Size = new System.Drawing.Size(733, 25);
            this.e_Pefil.TabIndex = 21;
            this.e_Pefil.Text = "MODO MANTENIMIENTO";
            this.e_Pefil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.e_Pefil.Visible = false;
            // 
            // FormaActividades
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.OPERACIONES);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_BotonSalir);
            this.Controls.Add(this.e_Pefil);
            this.Name = "FormaActividades";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormaActividades_FormClosing);
            this.Load += new System.EventHandler(this.FormaActividades_Load);
            this.Controls.SetChildIndex(this.e_Pefil, 0);
            this.Controls.SetChildIndex(this.c_BotonSalir, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.OPERACIONES, 0);
            this.OPERACIONES.ResumeLayout(false);
            this.Admin.ResumeLayout(false);
            this.Sistema.ResumeLayout(false);
            this.Etv.ResumeLayout(false);
            this.Mantenimiento.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_BotonSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_OpcionRetiroETV;
        private System.Windows.Forms.Button c_AperturaForzada;
        private System.Windows.Forms.Button c_reset_retiro;
        private System.Windows.Forms.TabPage Etv;
        private System.Windows.Forms.Button c_OpcionCreaLayout;
        private System.Windows.Forms.Button c_CerrarSistema;
        private System.Windows.Forms.Button c_Reset;
        private System.Windows.Forms.Button c_numeroBolsa;
        private System.Windows.Forms.TabPage Sistema;
        private System.Windows.Forms.Button c_OpcionConsultaOperacion;
        private System.Windows.Forms.Button c_OpcionConsultaEfectivo;
        private System.Windows.Forms.Button c_contabilidad;
        private System.Windows.Forms.TabPage Admin;
        private System.Windows.Forms.TabControl OPERACIONES;
        private System.Windows.Forms.Button c_administrarUsuarios;
        private System.Windows.Forms.Label e_Pefil;
        private System.Windows.Forms.TabPage Mantenimiento;
        private System.Windows.Forms.Button c_registroSalida;
        private System.Windows.Forms.Button c_ticket;
        private System.Windows.Forms.Button c_registro;
        private System.Windows.Forms.Button c_ConsultaMante;
    }
}
