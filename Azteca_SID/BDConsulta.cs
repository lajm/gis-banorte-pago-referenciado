﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace Azteca_SID
{
    public class BDConsulta
    {
        public static DataTable
            ObtenerConsultaDepositos(int p_IdMoneda)
        {
            String l_Query
               = "SELECT               SUM(b.Cantidad), c.Codigo, a.IdCajon "
               + "FROM                 Denominacion c, Deposito a, DetalleDeposito b "
               + "WHERE                c.IdDenominacion = b.IdDenominacion "
               + "AND                  a.IdDeposito = b.IdDeposito "
               + "AND                  a.Retirado = 0 "
               + "AND                  a.IdMoneda = @IdMoneda "
               + "GROUP BY             c.Codigo, a.IdCajon ";

            if ( p_IdMoneda == 0 )
                l_Query = "SELECT               SUM(b.Cantidad), c.Codigo, a.IdCajon "
                + "FROM                 Denominacion c, Deposito a, DetalleDeposito b "
                + "WHERE                c.IdDenominacion = b.IdDenominacion "
                + "AND                  a.IdDeposito = b.IdDeposito "
                + "AND                  a.Retirado = 0 "
                + "AND                  c.IdDenominacion > 26 "
                + "GROUP BY             c.Codigo, a.IdCajon ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("ConsultaDeposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
           ObtenerConsulta_Depositos(int p_IdMoneda,String p_Equipo)
        {
            String l_Query
                = "SELECT               SUM(b.Cantidad), c.Codigo, a.IdCajon "
                + "FROM                 Denominacion c, Deposito a, DetalleDeposito b "
                + "WHERE                a.Equipo = @Equipo "
                + "AND                  c.IdDenominacion = b.IdDenominacion "
                + "AND                  a.IdDeposito = b.IdDeposito "
                + "AND                  a.Retirado = 0 "
                + "AND                  a.IdMoneda = @IdMoneda "
                + "GROUP BY             c.Codigo, a.IdCajon ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                 l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("ConsultaDeposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }


        public static DataTable
            ObtenerConsultaOperaciones()
        {
            String l_Query
                = "SELECT               a.IdUsuario, 'DEPÓSITO', '$' + CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1), "
                + "                     b.Descripcion, a.FechaHora "
                + "FROM                 Moneda b, Deposito a " 
                + "WHERE                b.IdMoneda = a.IdMoneda "
                + "AND					DATEPART(DAY, a.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, a.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, a.FechaHora) = @anio "
                + "UNION "
                + "SELECT				c.IdUsuario, ' RETIRO ', '$' + CONVERT(VARCHAR, CONVERT(MONEY, c.Total), 1), " 
                + "                     b.Descripcion, c.FechaHora "
                + "FROM				    Moneda b, Retiro c "
                + "WHERE				b.IdMoneda = c.IdMoneda "
                + "AND					DATEPART(DAY, c.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, c.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, c.FechaHora) = @anio "
                + "ORDER BY			    5 DESC ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@dia", DateTime.Today.Day.ToString());
                l_Comando.Parameters.AddWithValue("@mes", DateTime.Today.Month.ToString());
                l_Comando.Parameters.AddWithValue("@anio", DateTime.Today.Year.ToString());
                DataTable l_Datos = new DataTable("Operaciones");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
         ObtenerConsultaOperaciones(DateTime p_Fecha)
        {
            String l_Query
                = "SELECT               a.IdDeposito, a.IdUsuario, 'DEPÓSITO', '$' + CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1), "
                + "                     b.Descripcion, a.FechaHora, a.Detalle_Deposito, c.Referencia "
                + "FROM                 Moneda b, Deposito a , DepositoGSI c "
                + "WHERE                b.IdMoneda = a.IdMoneda "
                + "AND                  a.IdDeposito = c.IdDeposito "
                + "AND					DATEPART(DAY, a.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, a.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, a.FechaHora) = @anio "
                + "UNION "
                + "SELECT				c.IdRetiro, c.IdUsuario, ' RETIRO ', '$' + CONVERT(VARCHAR, CONVERT(MONEY, c.Total), 1), "
                + "                     b.Descripcion, c.FechaHora, c.Detalle_Retiro,'' "
                + "FROM				    Moneda b, Retiro c "
                + "WHERE				b.IdMoneda = c.IdMoneda "
                + "AND					DATEPART(DAY, c.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, c.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, c.FechaHora) = @anio "
                + "ORDER BY			    6 DESC ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@dia", p_Fecha.Day.ToString());
                l_Comando.Parameters.AddWithValue("@mes", p_Fecha.Month.ToString());
                l_Comando.Parameters.AddWithValue("@anio", p_Fecha.Year.ToString());
                DataTable l_Datos = new DataTable("Operaciones");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
         ObtenerDepositosGSIPendientes()
        {
            String l_Query
                = "SELECT               DepositoGSI.* "
                + "FROM                 DepositoGSI "
                + "INNER JOIN           Deposito "
                + "                    ON DepositoGSI.IdDeposito = Deposito.IdDeposito "
                + "WHERE                Pendiente = 1  OR Deposito.Detalle_Deposito = '' OR Deposito.Detalle_Deposito LIKE '%Pendiente%'"
                + " Order by IdDeposito ";
    

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
              
                DataTable l_Datos = new DataTable("Depositos Pendientes");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
       ObtenerRetirosGSIPendientes()
        {
            String l_Query
                = "SELECT               * "
                + "FROM                RetiroGSI "
                + "WHERE               Pendiente = 1 "
                + " Order by IdRetiro ";
            


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);

                DataTable l_Datos = new DataTable("Retiros Pendientes");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
        ObtenerConsultaOperaciones_mov()
        {
            String l_Query
              = "SELECT          a.FechaHora, a.IdUsuario,  CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1) "
              + "FROM            Deposito a "
              + "WHERE           a.Retirado = 0 "
              + "AND             a.IdMoneda = 1 ";


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Movimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
     ObtenerConsultaOperaciones_mov_M( )
        {
            String l_Query
                = "SELECT              a.FechaHora, a.IdUsuario,  CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1) "
                + "FROM                Deposito a  "
                + "WHERE               a.Retirado =0 "
                + "AND             a.IdMoneda =0 ";


            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                DataTable l_Datos = new DataTable( "Movimientos" );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }

        public static DataTable
       ObtenerConsultaOperaciones_mov_manual( )
        {
            String l_Query
                = "SELECT              a.FechaHora, a.IdUsuario,  CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1) "
                + "FROM                Deposito a "
                + "WHERE            a.Retirado =0 "
                + "AND             a.Manual =1 ";


            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion( ) )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query , l_Conexion );
                DataTable l_Datos = new DataTable( "Movimientos" );
                SqlDataAdapter l_Adaptador = new SqlDataAdapter( l_Comando );
                l_Adaptador.Fill( l_Datos );
                return l_Datos;
            }
        }
        public static DataTable ObtenerConsultaOperaciones_movTotales()
        {
            String l_Query
                = "SELECT               a.IdUsuario, SUM( CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1)) "
                + "FROM                 Deposito a, Usuario u "
                + "WHERE               u.IdUsuario = a.IdUsuario "
                    + "AND             a.Retirado =0 "
                    + "GROUP BY a.IdUsuario" ;


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Movimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }
    }
}
