﻿namespace Azteca_SID
{
    partial class FormaModoDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_billetes = new System.Windows.Forms.Button();
            this.c_Integrado = new System.Windows.Forms.Button();
            this.Monedas = new System.Windows.Forms.Button();
            this.c_manual = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_billetes
            // 
            this.c_billetes.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.c_billetes.BackgroundImage = global::Azteca_SID.Properties.Resources.dinero;
            this.c_billetes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_billetes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c_billetes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_billetes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_billetes.Location = new System.Drawing.Point(154, 160);
            this.c_billetes.Name = "c_billetes";
            this.c_billetes.Size = new System.Drawing.Size(213, 133);
            this.c_billetes.TabIndex = 1;
            this.c_billetes.Text = "SOLO BILLETES";
            this.c_billetes.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.c_billetes.UseVisualStyleBackColor = false;
            // 
            // c_Integrado
            // 
            this.c_Integrado.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.c_Integrado.BackgroundImage = global::Azteca_SID.Properties.Resources.Biletes_y_monedas;
            this.c_Integrado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_Integrado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c_Integrado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Integrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Integrado.Location = new System.Drawing.Point(433, 160);
            this.c_Integrado.Name = "c_Integrado";
            this.c_Integrado.Size = new System.Drawing.Size(213, 133);
            this.c_Integrado.TabIndex = 2;
            this.c_Integrado.Text = "DEPOSITO INTEGRADO";
            this.c_Integrado.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.c_Integrado.UseVisualStyleBackColor = false;
            // 
            // Monedas
            // 
            this.Monedas.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Monedas.BackgroundImage = global::Azteca_SID.Properties.Resources.monedas;
            this.Monedas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Monedas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Monedas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Monedas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Monedas.Location = new System.Drawing.Point(154, 327);
            this.Monedas.Name = "Monedas";
            this.Monedas.Size = new System.Drawing.Size(213, 133);
            this.Monedas.TabIndex = 3;
            this.Monedas.Text = "SOLO MONEDAS";
            this.Monedas.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Monedas.UseVisualStyleBackColor = false;
            // 
            // c_manual
            // 
            this.c_manual.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.c_manual.BackgroundImage = global::Azteca_SID.Properties.Resources.Aguinaldo;
            this.c_manual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.c_manual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c_manual.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_manual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_manual.Location = new System.Drawing.Point(433, 327);
            this.c_manual.Name = "c_manual";
            this.c_manual.Size = new System.Drawing.Size(213, 133);
            this.c_manual.TabIndex = 4;
            this.c_manual.Text = "DEPOSITO MANUAL";
            this.c_manual.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.c_manual.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(170, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(461, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "¿QUE DESEAS DEPOSITAR O REALIZAR?";
            // 
            // FormaModoDeposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_manual);
            this.Controls.Add(this.Monedas);
            this.Controls.Add(this.c_Integrado);
            this.Controls.Add(this.c_billetes);
            this.Name = "FormaModoDeposito";
            this.Text = "FormaModoDeposito";
            this.Controls.SetChildIndex(this.c_billetes, 0);
            this.Controls.SetChildIndex(this.c_Integrado, 0);
            this.Controls.SetChildIndex(this.Monedas, 0);
            this.Controls.SetChildIndex(this.c_manual, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_billetes;
        private System.Windows.Forms.Button c_Integrado;
        private System.Windows.Forms.Button Monedas;
        private System.Windows.Forms.Button c_manual;
        private System.Windows.Forms.Label label1;
    }
}