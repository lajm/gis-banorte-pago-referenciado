﻿namespace Azteca_SID
{
    partial class FormAltaUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.c_convenio = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.c_referencia = new System.Windows.Forms.TextBox();
            this.c_perfiles = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Alta = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_confirmacionContraseña = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Contraseña = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_IdUsuario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.c_refFija = new System.Windows.Forms.TextBox();
            this.c_divisabox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(226, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 16);
            this.label9.TabIndex = 54;
            this.label9.Text = "Convenio";
            // 
            // c_convenio
            // 
            this.c_convenio.BackColor = System.Drawing.SystemColors.Info;
            this.c_convenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_convenio.Location = new System.Drawing.Point(328, 264);
            this.c_convenio.Name = "c_convenio";
            this.c_convenio.Size = new System.Drawing.Size(285, 26);
            this.c_convenio.TabIndex = 53;
            this.c_convenio.Click += new System.EventHandler(this.c_convenio_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(216, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 52;
            this.label8.Text = "Referencia";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // c_referencia
            // 
            this.c_referencia.BackColor = System.Drawing.SystemColors.Info;
            this.c_referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_referencia.Location = new System.Drawing.Point(328, 230);
            this.c_referencia.Name = "c_referencia";
            this.c_referencia.Size = new System.Drawing.Size(285, 26);
            this.c_referencia.TabIndex = 51;
            this.c_referencia.Click += new System.EventHandler(this.c_referencia_Click);
            // 
            // c_perfiles
            // 
            this.c_perfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_perfiles.FormattingEnabled = true;
            this.c_perfiles.Location = new System.Drawing.Point(331, 173);
            this.c_perfiles.Name = "c_perfiles";
            this.c_perfiles.Size = new System.Drawing.Size(180, 24);
            this.c_perfiles.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(254, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 47;
            this.label7.Text = "Perfil";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(171, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 16);
            this.label6.TabIndex = 48;
            this.label6.Text = "Nombre Asociado";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(329, 200);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(346, 26);
            this.c_Nombre.TabIndex = 39;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 526);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 20);
            this.label5.TabIndex = 45;
            this.label5.Text = "Rellene todos los campos";
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(450, 408);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 43;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Alta
            // 
            this.c_Alta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Alta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Alta.Location = new System.Drawing.Point(219, 408);
            this.c_Alta.Name = "c_Alta";
            this.c_Alta.Size = new System.Drawing.Size(122, 47);
            this.c_Alta.TabIndex = 42;
            this.c_Alta.Text = "Dar de Alta";
            this.c_Alta.UseVisualStyleBackColor = true;
            this.c_Alta.Click += new System.EventHandler(this.c_Alta_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(217, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(366, 29);
            this.label4.TabIndex = 44;
            this.label4.Text = "ALTA DE NUEVO OPERADOR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(173, 361);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 16);
            this.label3.TabIndex = 50;
            this.label3.Text = "Confirme Contraseña";
            this.label3.Visible = false;
            // 
            // c_confirmacionContraseña
            // 
            this.c_confirmacionContraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_confirmacionContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmacionContraseña.Location = new System.Drawing.Point(330, 355);
            this.c_confirmacionContraseña.Name = "c_confirmacionContraseña";
            this.c_confirmacionContraseña.PasswordChar = '*';
            this.c_confirmacionContraseña.Size = new System.Drawing.Size(180, 22);
            this.c_confirmacionContraseña.TabIndex = 41;
            this.c_confirmacionContraseña.Visible = false;
            this.c_confirmacionContraseña.Click += new System.EventHandler(this.c_confirmacionContraseña_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(237, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 49;
            this.label2.Text = "Contraseña";
            this.label2.Visible = false;
            // 
            // c_Contraseña
            // 
            this.c_Contraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_Contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Contraseña.Location = new System.Drawing.Point(331, 330);
            this.c_Contraseña.Name = "c_Contraseña";
            this.c_Contraseña.PasswordChar = '*';
            this.c_Contraseña.Size = new System.Drawing.Size(180, 22);
            this.c_Contraseña.TabIndex = 40;
            this.c_Contraseña.Visible = false;
            this.c_Contraseña.Click += new System.EventHandler(this.c_Contraseña_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Numero de Cuenta / Empleado";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // c_IdUsuario
            // 
            this.c_IdUsuario.BackColor = System.Drawing.SystemColors.Info;
            this.c_IdUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_IdUsuario.Location = new System.Drawing.Point(331, 145);
            this.c_IdUsuario.Name = "c_IdUsuario";
            this.c_IdUsuario.Size = new System.Drawing.Size(346, 26);
            this.c_IdUsuario.TabIndex = 37;
            this.c_IdUsuario.Click += new System.EventHandler(this.c_IdUsuario_Click);
            this.c_IdUsuario.TextChanged += new System.EventHandler(this.c_IdUsuario_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(250, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 16);
            this.label10.TabIndex = 55;
            this.label10.Text = "Divisa";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(182, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 16);
            this.label11.TabIndex = 56;
            this.label11.Text = "Cuenta Bancaria";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(141, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(163, 20);
            this.label12.TabIndex = 57;
            this.label12.Text = "Numero de Usuario";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(186, 302);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 16);
            this.label13.TabIndex = 59;
            this.label13.Text = "Referencia Fija";
            // 
            // c_refFija
            // 
            this.c_refFija.BackColor = System.Drawing.SystemColors.Info;
            this.c_refFija.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_refFija.Location = new System.Drawing.Point(328, 298);
            this.c_refFija.MaxLength = 20;
            this.c_refFija.Name = "c_refFija";
            this.c_refFija.Size = new System.Drawing.Size(285, 26);
            this.c_refFija.TabIndex = 58;
            this.c_refFija.Click += new System.EventHandler(this.c_refFija_Click);
            // 
            // c_divisabox
            // 
            this.c_divisabox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_divisabox.FormattingEnabled = true;
            this.c_divisabox.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.c_divisabox.Location = new System.Drawing.Point(328, 266);
            this.c_divisabox.Name = "c_divisabox";
            this.c_divisabox.Size = new System.Drawing.Size(121, 24);
            this.c_divisabox.TabIndex = 60;
            // 
            // FormAltaUsers
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_divisabox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.c_refFija);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.c_convenio);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.c_referencia);
            this.Controls.Add(this.c_perfiles);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Alta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_confirmacionContraseña);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Contraseña);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_IdUsuario);
            this.Name = "FormAltaUsers";
            this.Load += new System.EventHandler(this.FormAltaUsers_Load);
            this.Controls.SetChildIndex(this.c_IdUsuario, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_Contraseña, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.c_confirmacionContraseña, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_Alta, 0);
            this.Controls.SetChildIndex(this.c_Cancelar, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.c_Nombre, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.c_perfiles, 0);
            this.Controls.SetChildIndex(this.c_referencia, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.c_convenio, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.c_refFija, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.c_divisabox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox c_convenio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox c_referencia;
        private System.Windows.Forms.ComboBox c_perfiles;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Alta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_confirmacionContraseña;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Contraseña;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_IdUsuario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox c_refFija;
        private System.Windows.Forms.ComboBox c_divisabox;
    }
}
