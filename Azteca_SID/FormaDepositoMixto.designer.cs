﻿namespace Azteca_SID
{
    partial class FormaDepositoMixto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //  //  base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_tope = new System.Windows.Forms.Label();
            this.c_empleado = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Depositar = new System.Windows.Forms.Button();
            this.c_EtiquetaNumeroCuenta = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_Suma20 = new System.Windows.Forms.TextBox();
            this.c_Suma50 = new System.Windows.Forms.TextBox();
            this.c_Suma100 = new System.Windows.Forms.TextBox();
            this.c_Suma200 = new System.Windows.Forms.TextBox();
            this.c_Suma500 = new System.Windows.Forms.TextBox();
            this.c_Suma1000 = new System.Windows.Forms.TextBox();
            this.c_Cantidad20 = new System.Windows.Forms.TextBox();
            this.c_Cantidad50 = new System.Windows.Forms.TextBox();
            this.c_Cantidad100 = new System.Windows.Forms.TextBox();
            this.c_Cantidad200 = new System.Windows.Forms.TextBox();
            this.c_Cantidad500 = new System.Windows.Forms.TextBox();
            this.c_Cantidad1000 = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.c_accion = new System.Windows.Forms.PictureBox();
            this.c_enviando = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.c_Suma1 = new System.Windows.Forms.TextBox();
            this.c_Suma5 = new System.Windows.Forms.TextBox();
            this.c_Suma50c = new System.Windows.Forms.TextBox();
            this.c_Suma2 = new System.Windows.Forms.TextBox();
            this.c_Suma10 = new System.Windows.Forms.TextBox();
            this.c_Cantidad50c = new System.Windows.Forms.TextBox();
            this.c_Cantidad1 = new System.Windows.Forms.TextBox();
            this.c_Cantidad2 = new System.Windows.Forms.TextBox();
            this.c_Cantidad5 = new System.Windows.Forms.TextBox();
            this.c_Cantidad10 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.c_montomonedas = new System.Windows.Forms.TextBox();
            this.c_MonedasAceptadas = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.c_TotalDeposito = new System.Windows.Forms.TextBox();
            this.c_BilletesRechazados = new System.Windows.Forms.TextBox();
            this.c_BilletesAceptados = new System.Windows.Forms.TextBox();
            this.bw_monedas = new System.ComponentModel.BackgroundWorker();
            this.c_integrado = new System.Windows.Forms.PictureBox();
            this.c_TotalBilletes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.l_ref = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            ( (System.ComponentModel.ISupportInitialize) ( this.pictureBox4 ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize) ( this.c_accion ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize) ( this.c_integrado ) ).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // c_tope
            // 
            this.c_tope.AutoSize = true;
            this.c_tope.BackColor = System.Drawing.Color.Transparent;
            this.c_tope.Font = new System.Drawing.Font( "Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_tope.ForeColor = System.Drawing.Color.Red;
            this.c_tope.Location = new System.Drawing.Point( 513, 61 );
            this.c_tope.Name = "c_tope";
            this.c_tope.Size = new System.Drawing.Size( 206, 25 );
            this.c_tope.TabIndex = 93;
            this.c_tope.Text = "Maximo Permitido:";
            this.c_tope.Visible = false;
            // 
            // c_empleado
            // 
            this.c_empleado.AutoSize = true;
            this.c_empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_empleado.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_empleado.ForeColor = System.Drawing.SystemColors.Highlight;
            this.c_empleado.Location = new System.Drawing.Point( 43, 86 );
            this.c_empleado.Name = "c_empleado";
            this.c_empleado.Size = new System.Drawing.Size( 89, 20 );
            this.c_empleado.TabIndex = 92;
            this.c_empleado.Text = "Empleado";
            this.c_empleado.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label14.Location = new System.Drawing.Point( 12, 66 );
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size( 172, 20 );
            this.label14.TabIndex = 91;
            this.label14.Text = "Nombre de CUENTA";
            this.label14.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point( 640, 136 );
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size( 43, 16 );
            this.label12.TabIndex = 89;
            this.label12.Text = "Suma";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Location = new System.Drawing.Point( 499, 136 );
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size( 62, 16 );
            this.label11.TabIndex = 88;
            this.label11.Text = "Cantidad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point( 394, 274 );
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size( 40, 13 );
            this.label8.TabIndex = 87;
            this.label8.Text = "$20.00";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Black;
            this.label7.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point( 394, 251 );
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size( 40, 13 );
            this.label7.TabIndex = 86;
            this.label7.Text = "$50.00";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point( 388, 228 );
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size( 46, 13 );
            this.label6.TabIndex = 85;
            this.label6.Text = "$100.00";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point( 388, 205 );
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size( 46, 13 );
            this.label5.TabIndex = 84;
            this.label5.Text = "$200.00";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point( 388, 182 );
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size( 46, 13 );
            this.label4.TabIndex = 83;
            this.label4.Text = "$500.00";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Black;
            this.label9.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point( 379, 159 );
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size( 55, 13 );
            this.label9.TabIndex = 82;
            this.label9.Text = "$1,000.00";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point( 88, 190 );
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size( 193, 46 );
            this.label10.TabIndex = 81;
            this.label10.Text = "Coloque el efectivo en la bandeja de conteo";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point( 51, 159 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 266, 25 );
            this.label1.TabIndex = 78;
            this.label1.Text = "INGRESE EL EFECTIVO";
            // 
            // c_Depositar
            // 
            this.c_Depositar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.c_Depositar.BackColor = System.Drawing.Color.Transparent;
            this.c_Depositar.BackgroundImage = global::Azteca_SID.Properties.Resources.btn_banorte_depositar;
            this.c_Depositar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Depositar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Depositar.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Depositar.ForeColor = System.Drawing.Color.Transparent;
            this.c_Depositar.Location = new System.Drawing.Point( 347, 525 );
            this.c_Depositar.Name = "c_Depositar";
            this.c_Depositar.Size = new System.Drawing.Size( 130, 48 );
            this.c_Depositar.TabIndex = 77;
            this.c_Depositar.TabStop = false;
            this.c_Depositar.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_Depositar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Depositar.UseVisualStyleBackColor = true;
            this.c_Depositar.Click += new System.EventHandler( this.c_integrado_Click );
            // 
            // c_EtiquetaNumeroCuenta
            // 
            this.c_EtiquetaNumeroCuenta.AutoSize = true;
            this.c_EtiquetaNumeroCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaNumeroCuenta.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_EtiquetaNumeroCuenta.Location = new System.Drawing.Point( 12, 106 );
            this.c_EtiquetaNumeroCuenta.Name = "c_EtiquetaNumeroCuenta";
            this.c_EtiquetaNumeroCuenta.Size = new System.Drawing.Size( 135, 20 );
            this.c_EtiquetaNumeroCuenta.TabIndex = 76;
            this.c_EtiquetaNumeroCuenta.Text = "00000000000001";
            this.c_EtiquetaNumeroCuenta.Visible = false;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.c_BotonAceptar.BackColor = System.Drawing.Color.Transparent;
            this.c_BotonAceptar.BackgroundImage = global::Azteca_SID.Properties.Resources.Terminar_Transaccion;
            this.c_BotonAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BotonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_BotonAceptar.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_BotonAceptar.ForeColor = System.Drawing.Color.Transparent;
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point( 535, 525 );
            this.c_BotonAceptar.Margin = new System.Windows.Forms.Padding( 0 );
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size( 256, 48 );
            this.c_BotonAceptar.TabIndex = 75;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler( this.c_BotonAceptar_Click );
            // 
            // c_Suma20
            // 
            this.c_Suma20.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma20.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma20.Location = new System.Drawing.Point( 571, 274 );
            this.c_Suma20.Name = "c_Suma20";
            this.c_Suma20.ReadOnly = true;
            this.c_Suma20.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma20.TabIndex = 71;
            this.c_Suma20.TabStop = false;
            this.c_Suma20.Text = "$0.00";
            this.c_Suma20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma50
            // 
            this.c_Suma50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma50.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma50.Location = new System.Drawing.Point( 571, 251 );
            this.c_Suma50.Name = "c_Suma50";
            this.c_Suma50.ReadOnly = true;
            this.c_Suma50.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma50.TabIndex = 70;
            this.c_Suma50.TabStop = false;
            this.c_Suma50.Text = "$0.00";
            this.c_Suma50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma100
            // 
            this.c_Suma100.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma100.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma100.Location = new System.Drawing.Point( 571, 228 );
            this.c_Suma100.Name = "c_Suma100";
            this.c_Suma100.ReadOnly = true;
            this.c_Suma100.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma100.TabIndex = 69;
            this.c_Suma100.TabStop = false;
            this.c_Suma100.Text = "$0.00";
            this.c_Suma100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma200
            // 
            this.c_Suma200.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma200.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma200.Location = new System.Drawing.Point( 571, 205 );
            this.c_Suma200.Name = "c_Suma200";
            this.c_Suma200.ReadOnly = true;
            this.c_Suma200.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma200.TabIndex = 68;
            this.c_Suma200.TabStop = false;
            this.c_Suma200.Text = "$0.00";
            this.c_Suma200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma500
            // 
            this.c_Suma500.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma500.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma500.Location = new System.Drawing.Point( 571, 182 );
            this.c_Suma500.Name = "c_Suma500";
            this.c_Suma500.ReadOnly = true;
            this.c_Suma500.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma500.TabIndex = 67;
            this.c_Suma500.TabStop = false;
            this.c_Suma500.Text = "$0.00";
            this.c_Suma500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma1000
            // 
            this.c_Suma1000.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma1000.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma1000.Location = new System.Drawing.Point( 571, 159 );
            this.c_Suma1000.Name = "c_Suma1000";
            this.c_Suma1000.ReadOnly = true;
            this.c_Suma1000.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma1000.TabIndex = 66;
            this.c_Suma1000.TabStop = false;
            this.c_Suma1000.Text = "$0.00";
            this.c_Suma1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad20
            // 
            this.c_Cantidad20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad20.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad20.Location = new System.Drawing.Point( 494, 274 );
            this.c_Cantidad20.Name = "c_Cantidad20";
            this.c_Cantidad20.ReadOnly = true;
            this.c_Cantidad20.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad20.TabIndex = 65;
            this.c_Cantidad20.TabStop = false;
            this.c_Cantidad20.Text = "0";
            this.c_Cantidad20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad50
            // 
            this.c_Cantidad50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad50.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad50.Location = new System.Drawing.Point( 494, 251 );
            this.c_Cantidad50.Name = "c_Cantidad50";
            this.c_Cantidad50.ReadOnly = true;
            this.c_Cantidad50.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad50.TabIndex = 64;
            this.c_Cantidad50.TabStop = false;
            this.c_Cantidad50.Text = "0";
            this.c_Cantidad50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad100
            // 
            this.c_Cantidad100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad100.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad100.Location = new System.Drawing.Point( 494, 228 );
            this.c_Cantidad100.Name = "c_Cantidad100";
            this.c_Cantidad100.ReadOnly = true;
            this.c_Cantidad100.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad100.TabIndex = 63;
            this.c_Cantidad100.TabStop = false;
            this.c_Cantidad100.Text = "0";
            this.c_Cantidad100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad200
            // 
            this.c_Cantidad200.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad200.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad200.Location = new System.Drawing.Point( 494, 205 );
            this.c_Cantidad200.Name = "c_Cantidad200";
            this.c_Cantidad200.ReadOnly = true;
            this.c_Cantidad200.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad200.TabIndex = 62;
            this.c_Cantidad200.TabStop = false;
            this.c_Cantidad200.Text = "0";
            this.c_Cantidad200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad500
            // 
            this.c_Cantidad500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad500.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad500.Location = new System.Drawing.Point( 494, 182 );
            this.c_Cantidad500.Name = "c_Cantidad500";
            this.c_Cantidad500.ReadOnly = true;
            this.c_Cantidad500.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad500.TabIndex = 61;
            this.c_Cantidad500.TabStop = false;
            this.c_Cantidad500.Text = "0";
            this.c_Cantidad500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad1000
            // 
            this.c_Cantidad1000.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad1000.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad1000.Location = new System.Drawing.Point( 494, 159 );
            this.c_Cantidad1000.Name = "c_Cantidad1000";
            this.c_Cantidad1000.ReadOnly = true;
            this.c_Cantidad1000.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad1000.TabIndex = 60;
            this.c_Cantidad1000.TabStop = false;
            this.c_Cantidad1000.Text = "0";
            this.c_Cantidad1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Azteca_SID.Properties.Resources.pesos;
            this.pictureBox4.Location = new System.Drawing.Point( 631, 92 );
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size( 113, 40 );
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 95;
            this.pictureBox4.TabStop = false;
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_cuenta.Enabled = false;
            this.c_cuenta.Font = new System.Drawing.Font( "Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_cuenta.Location = new System.Drawing.Point( 374, 93 );
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size( 258, 38 );
            this.c_cuenta.TabIndex = 96;
            this.c_cuenta.Text = "******XXXX";
            // 
            // c_accion
            // 
            this.c_accion.BackColor = System.Drawing.Color.Transparent;
            this.c_accion.Image = global::Azteca_SID.Properties.Resources.bills;
            this.c_accion.Location = new System.Drawing.Point( 89, 239 );
            this.c_accion.Name = "c_accion";
            this.c_accion.Size = new System.Drawing.Size( 192, 148 );
            this.c_accion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_accion.TabIndex = 97;
            this.c_accion.TabStop = false;
            // 
            // c_enviando
            // 
            this.c_enviando.BackColor = System.Drawing.Color.Transparent;
            this.c_enviando.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_enviando.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_enviando.Location = new System.Drawing.Point( 5, 390 );
            this.c_enviando.Name = "c_enviando";
            this.c_enviando.Size = new System.Drawing.Size( 354, 43 );
            this.c_enviando.TabIndex = 99;
            this.c_enviando.Text = "Un momento por favor, estamos procesando su Depósito";
            this.c_enviando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.c_enviando.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label15.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label15.Location = new System.Drawing.Point( 2, 440 );
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size( 326, 46 );
            this.label15.TabIndex = 100;
            this.label15.Text = "Para finalizar  su depósito presione Terminar. No olvide tomar su Comprobante";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_Suma1
            // 
            this.c_Suma1.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma1.Location = new System.Drawing.Point( 206, 72 );
            this.c_Suma1.Name = "c_Suma1";
            this.c_Suma1.ReadOnly = true;
            this.c_Suma1.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma1.TabIndex = 146;
            this.c_Suma1.TabStop = false;
            this.c_Suma1.Text = "$0.00";
            this.c_Suma1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma5
            // 
            this.c_Suma5.BackColor = System.Drawing.SystemColors.Control;
            this.c_Suma5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma5.Location = new System.Drawing.Point( 206, 36 );
            this.c_Suma5.Name = "c_Suma5";
            this.c_Suma5.ReadOnly = true;
            this.c_Suma5.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma5.TabIndex = 145;
            this.c_Suma5.TabStop = false;
            this.c_Suma5.Text = "$0.00";
            this.c_Suma5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma50c
            // 
            this.c_Suma50c.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma50c.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma50c.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma50c.Location = new System.Drawing.Point( 206, 90 );
            this.c_Suma50c.Name = "c_Suma50c";
            this.c_Suma50c.ReadOnly = true;
            this.c_Suma50c.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma50c.TabIndex = 144;
            this.c_Suma50c.TabStop = false;
            this.c_Suma50c.Text = "$0.00";
            this.c_Suma50c.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma2
            // 
            this.c_Suma2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma2.Location = new System.Drawing.Point( 206, 54 );
            this.c_Suma2.Name = "c_Suma2";
            this.c_Suma2.ReadOnly = true;
            this.c_Suma2.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma2.TabIndex = 143;
            this.c_Suma2.TabStop = false;
            this.c_Suma2.Text = "$0.00";
            this.c_Suma2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Suma10
            // 
            this.c_Suma10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Suma10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma10.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Suma10.Location = new System.Drawing.Point( 206, 18 );
            this.c_Suma10.Name = "c_Suma10";
            this.c_Suma10.ReadOnly = true;
            this.c_Suma10.Size = new System.Drawing.Size( 170, 15 );
            this.c_Suma10.TabIndex = 142;
            this.c_Suma10.TabStop = false;
            this.c_Suma10.Text = "$0.00";
            this.c_Suma10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad50c
            // 
            this.c_Cantidad50c.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad50c.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad50c.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad50c.Location = new System.Drawing.Point( 129, 90 );
            this.c_Cantidad50c.Name = "c_Cantidad50c";
            this.c_Cantidad50c.ReadOnly = true;
            this.c_Cantidad50c.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad50c.TabIndex = 141;
            this.c_Cantidad50c.TabStop = false;
            this.c_Cantidad50c.Text = "0";
            this.c_Cantidad50c.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad1
            // 
            this.c_Cantidad1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad1.Location = new System.Drawing.Point( 128, 72 );
            this.c_Cantidad1.Name = "c_Cantidad1";
            this.c_Cantidad1.ReadOnly = true;
            this.c_Cantidad1.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad1.TabIndex = 140;
            this.c_Cantidad1.TabStop = false;
            this.c_Cantidad1.Text = "0";
            this.c_Cantidad1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad2
            // 
            this.c_Cantidad2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad2.Location = new System.Drawing.Point( 129, 54 );
            this.c_Cantidad2.Name = "c_Cantidad2";
            this.c_Cantidad2.ReadOnly = true;
            this.c_Cantidad2.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad2.TabIndex = 139;
            this.c_Cantidad2.TabStop = false;
            this.c_Cantidad2.Text = "0";
            this.c_Cantidad2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad5
            // 
            this.c_Cantidad5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad5.Location = new System.Drawing.Point( 129, 36 );
            this.c_Cantidad5.Name = "c_Cantidad5";
            this.c_Cantidad5.ReadOnly = true;
            this.c_Cantidad5.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad5.TabIndex = 138;
            this.c_Cantidad5.TabStop = false;
            this.c_Cantidad5.Text = "0";
            this.c_Cantidad5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c_Cantidad10
            // 
            this.c_Cantidad10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_Cantidad10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad10.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_Cantidad10.Location = new System.Drawing.Point( 129, 18 );
            this.c_Cantidad10.Name = "c_Cantidad10";
            this.c_Cantidad10.ReadOnly = true;
            this.c_Cantidad10.Size = new System.Drawing.Size( 71, 15 );
            this.c_Cantidad10.TabIndex = 137;
            this.c_Cantidad10.TabStop = false;
            this.c_Cantidad10.Text = "0";
            this.c_Cantidad10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Black;
            this.label21.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label21.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label21.Location = new System.Drawing.Point( 32, 94 );
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size( 34, 13 );
            this.label21.TabIndex = 136;
            this.label21.Text = "$0.50";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Black;
            this.label20.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label20.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label20.Location = new System.Drawing.Point( 32, 75 );
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size( 34, 13 );
            this.label20.TabIndex = 135;
            this.label20.Text = "$1.00";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Black;
            this.label19.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label19.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label19.Location = new System.Drawing.Point( 32, 55 );
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size( 34, 13 );
            this.label19.TabIndex = 134;
            this.label19.Text = "$2.00";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Black;
            this.label18.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label18.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label18.Location = new System.Drawing.Point( 32, 35 );
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size( 34, 13 );
            this.label18.TabIndex = 133;
            this.label18.Text = "$5.00";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Black;
            this.label17.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label17.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label17.Location = new System.Drawing.Point( 28, 16 );
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size( 40, 13 );
            this.label17.TabIndex = 132;
            this.label17.Text = "$10.00";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point( 473, 440 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size( 87, 13 );
            this.label2.TabIndex = 131;
            this.label2.Text = "Monto Monedas:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point( 476, 407 );
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size( 105, 13 );
            this.label16.TabIndex = 130;
            this.label16.Text = "Monedas Aceptadas";
            // 
            // c_montomonedas
            // 
            this.c_montomonedas.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_montomonedas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_montomonedas.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_montomonedas.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_montomonedas.Location = new System.Drawing.Point( 476, 456 );
            this.c_montomonedas.Name = "c_montomonedas";
            this.c_montomonedas.ReadOnly = true;
            this.c_montomonedas.Size = new System.Drawing.Size( 99, 19 );
            this.c_montomonedas.TabIndex = 129;
            this.c_montomonedas.TabStop = false;
            this.c_montomonedas.Text = "$0.00";
            // 
            // c_MonedasAceptadas
            // 
            this.c_MonedasAceptadas.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_MonedasAceptadas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_MonedasAceptadas.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_MonedasAceptadas.Location = new System.Drawing.Point( 482, 419 );
            this.c_MonedasAceptadas.Name = "c_MonedasAceptadas";
            this.c_MonedasAceptadas.ReadOnly = true;
            this.c_MonedasAceptadas.Size = new System.Drawing.Size( 79, 15 );
            this.c_MonedasAceptadas.TabIndex = 128;
            this.c_MonedasAceptadas.TabStop = false;
            this.c_MonedasAceptadas.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point( 594, 455 );
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size( 147, 20 );
            this.label13.TabIndex = 127;
            this.label13.Text = "Total a Depositar";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label22.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label22.Location = new System.Drawing.Point( 371, 440 );
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size( 106, 13 );
            this.label22.TabIndex = 125;
            this.label22.Text = "Billetes Rechazados:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label23.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label23.Location = new System.Drawing.Point( 374, 404 );
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size( 94, 13 );
            this.label23.TabIndex = 124;
            this.label23.Text = "Billetes Aceptados";
            // 
            // c_TotalDeposito
            // 
            this.c_TotalDeposito.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_TotalDeposito.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_TotalDeposito.Font = new System.Drawing.Font( "Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_TotalDeposito.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_TotalDeposito.Location = new System.Drawing.Point( 587, 478 );
            this.c_TotalDeposito.Name = "c_TotalDeposito";
            this.c_TotalDeposito.ReadOnly = true;
            this.c_TotalDeposito.Size = new System.Drawing.Size( 157, 24 );
            this.c_TotalDeposito.TabIndex = 123;
            this.c_TotalDeposito.TabStop = false;
            this.c_TotalDeposito.Text = "$0.00";
            this.c_TotalDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_BilletesRechazados
            // 
            this.c_BilletesRechazados.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_BilletesRechazados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesRechazados.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_BilletesRechazados.Location = new System.Drawing.Point( 380, 456 );
            this.c_BilletesRechazados.Name = "c_BilletesRechazados";
            this.c_BilletesRechazados.ReadOnly = true;
            this.c_BilletesRechazados.Size = new System.Drawing.Size( 65, 15 );
            this.c_BilletesRechazados.TabIndex = 122;
            this.c_BilletesRechazados.TabStop = false;
            this.c_BilletesRechazados.Text = "No";
            // 
            // c_BilletesAceptados
            // 
            this.c_BilletesAceptados.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_BilletesAceptados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesAceptados.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_BilletesAceptados.Location = new System.Drawing.Point( 380, 419 );
            this.c_BilletesAceptados.Name = "c_BilletesAceptados";
            this.c_BilletesAceptados.ReadOnly = true;
            this.c_BilletesAceptados.Size = new System.Drawing.Size( 79, 15 );
            this.c_BilletesAceptados.TabIndex = 121;
            this.c_BilletesAceptados.TabStop = false;
            this.c_BilletesAceptados.Text = "0";
            // 
            // bw_monedas
            // 
            this.bw_monedas.DoWork += new System.ComponentModel.DoWorkEventHandler( this.bw_monedas_DoWork );
            this.bw_monedas.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler( this.bw_monedas_RunWorkerCompleted );
            // 
            // c_integrado
            // 
            this.c_integrado.Image = global::Azteca_SID.Properties.Resources.btn_banorte_depositar;
            this.c_integrado.Location = new System.Drawing.Point( 47, 525 );
            this.c_integrado.Name = "c_integrado";
            this.c_integrado.Size = new System.Drawing.Size( 128, 48 );
            this.c_integrado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_integrado.TabIndex = 148;
            this.c_integrado.TabStop = false;
            this.c_integrado.Visible = false;
            this.c_integrado.Click += new System.EventHandler( this.c_integrado_Click );
            // 
            // c_TotalBilletes
            // 
            this.c_TotalBilletes.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_TotalBilletes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_TotalBilletes.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.c_TotalBilletes.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_TotalBilletes.Location = new System.Drawing.Point( 584, 422 );
            this.c_TotalBilletes.Name = "c_TotalBilletes";
            this.c_TotalBilletes.ReadOnly = true;
            this.c_TotalBilletes.Size = new System.Drawing.Size( 157, 19 );
            this.c_TotalBilletes.TabIndex = 149;
            this.c_TotalBilletes.TabStop = false;
            this.c_TotalBilletes.Text = "$0.00";
            this.c_TotalBilletes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point( 601, 401 );
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size( 141, 20 );
            this.label3.TabIndex = 150;
            this.label3.Text = "Subtotal Billetes";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add( this.c_Suma1 );
            this.groupBox1.Controls.Add( this.c_Suma5 );
            this.groupBox1.Controls.Add( this.c_Suma50c );
            this.groupBox1.Controls.Add( this.c_Suma2 );
            this.groupBox1.Controls.Add( this.c_Suma10 );
            this.groupBox1.Controls.Add( this.c_Cantidad50c );
            this.groupBox1.Controls.Add( this.c_Cantidad1 );
            this.groupBox1.Controls.Add( this.c_Cantidad2 );
            this.groupBox1.Controls.Add( this.c_Cantidad5 );
            this.groupBox1.Controls.Add( this.c_Cantidad10 );
            this.groupBox1.Controls.Add( this.label21 );
            this.groupBox1.Controls.Add( this.label20 );
            this.groupBox1.Controls.Add( this.label19 );
            this.groupBox1.Controls.Add( this.label18 );
            this.groupBox1.Controls.Add( this.label17 );
            this.groupBox1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.groupBox1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.groupBox1.Location = new System.Drawing.Point( 365, 291 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size( 394, 111 );
            this.groupBox1.TabIndex = 151;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MONEDAS";
            // 
            // l_ref
            // 
            this.l_ref.AutoSize = true;
            this.l_ref.BackColor = System.Drawing.Color.Transparent;
            this.l_ref.Font = new System.Drawing.Font( "Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.l_ref.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.l_ref.Location = new System.Drawing.Point( 186, 136 );
            this.l_ref.Name = "l_ref";
            this.l_ref.Size = new System.Drawing.Size( 95, 20 );
            this.l_ref.TabIndex = 152;
            this.l_ref.Text = "Referencia: ";
            this.l_ref.Visible = false;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.label24.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label24.Location = new System.Drawing.Point( 42, 66 );
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size( 326, 46 );
            this.label24.TabIndex = 156;
            this.label24.Text = "Verifique si los datos a los que se realizará el Depósito son los correctos";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormaDepositoMixto
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte_dep;
            this.ClientSize = new System.Drawing.Size( 800, 600 );
            this.Controls.Add( this.l_ref );
            this.Controls.Add( this.groupBox1 );
            this.Controls.Add( this.label3 );
            this.Controls.Add( this.c_TotalBilletes );
            this.Controls.Add( this.c_integrado );
            this.Controls.Add( this.label2 );
            this.Controls.Add( this.label16 );
            this.Controls.Add( this.c_montomonedas );
            this.Controls.Add( this.c_MonedasAceptadas );
            this.Controls.Add( this.label13 );
            this.Controls.Add( this.label22 );
            this.Controls.Add( this.label23 );
            this.Controls.Add( this.c_TotalDeposito );
            this.Controls.Add( this.c_BilletesRechazados );
            this.Controls.Add( this.c_BilletesAceptados );
            this.Controls.Add( this.label15 );
            this.Controls.Add( this.c_enviando );
            this.Controls.Add( this.c_accion );
            this.Controls.Add( this.c_cuenta );
            this.Controls.Add( this.pictureBox4 );
            this.Controls.Add( this.c_BotonAceptar );
            this.Controls.Add( this.c_tope );
            this.Controls.Add( this.c_empleado );
            this.Controls.Add( this.label14 );
            this.Controls.Add( this.label12 );
            this.Controls.Add( this.label11 );
            this.Controls.Add( this.label8 );
            this.Controls.Add( this.label7 );
            this.Controls.Add( this.label6 );
            this.Controls.Add( this.label5 );
            this.Controls.Add( this.label4 );
            this.Controls.Add( this.label9 );
            this.Controls.Add( this.label10 );
            this.Controls.Add( this.label1 );
            this.Controls.Add( this.c_Depositar );
            this.Controls.Add( this.c_EtiquetaNumeroCuenta );
            this.Controls.Add( this.c_Suma20 );
            this.Controls.Add( this.c_Suma50 );
            this.Controls.Add( this.c_Suma100 );
            this.Controls.Add( this.c_Suma200 );
            this.Controls.Add( this.c_Suma500 );
            this.Controls.Add( this.c_Suma1000 );
            this.Controls.Add( this.c_Cantidad20 );
            this.Controls.Add( this.c_Cantidad50 );
            this.Controls.Add( this.c_Cantidad100 );
            this.Controls.Add( this.c_Cantidad200 );
            this.Controls.Add( this.c_Cantidad500 );
            this.Controls.Add( this.c_Cantidad1000 );
            this.Controls.Add( this.label24 );
            this.Font = new System.Drawing.Font( "Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormaDepositoMixto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler( this.FormaDeposito_Load );
            ( (System.ComponentModel.ISupportInitialize) ( this.pictureBox4 ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize) ( this.c_accion ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize) ( this.c_integrado ) ).EndInit();
            this.groupBox1.ResumeLayout( false );
            this.groupBox1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label c_tope;
        private System.Windows.Forms.Label c_empleado;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Depositar;
        private System.Windows.Forms.Label c_EtiquetaNumeroCuenta;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_Suma20;
        private System.Windows.Forms.TextBox c_Suma50;
        private System.Windows.Forms.TextBox c_Suma100;
        private System.Windows.Forms.TextBox c_Suma200;
        private System.Windows.Forms.TextBox c_Suma500;
        private System.Windows.Forms.TextBox c_Suma1000;
        private System.Windows.Forms.TextBox c_Cantidad20;
        private System.Windows.Forms.TextBox c_Cantidad50;
        private System.Windows.Forms.TextBox c_Cantidad100;
        private System.Windows.Forms.TextBox c_Cantidad200;
        private System.Windows.Forms.TextBox c_Cantidad500;
        private System.Windows.Forms.TextBox c_Cantidad1000;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.PictureBox c_accion;
        private System.Windows.Forms.Label c_enviando;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox c_Suma1;
        private System.Windows.Forms.TextBox c_Suma5;
        private System.Windows.Forms.TextBox c_Suma50c;
        private System.Windows.Forms.TextBox c_Suma2;
        private System.Windows.Forms.TextBox c_Suma10;
        private System.Windows.Forms.TextBox c_Cantidad50c;
        private System.Windows.Forms.TextBox c_Cantidad1;
        private System.Windows.Forms.TextBox c_Cantidad2;
        private System.Windows.Forms.TextBox c_Cantidad5;
        private System.Windows.Forms.TextBox c_Cantidad10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox c_montomonedas;
        private System.Windows.Forms.TextBox c_MonedasAceptadas;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox c_TotalDeposito;
        private System.Windows.Forms.TextBox c_BilletesRechazados;
        private System.Windows.Forms.TextBox c_BilletesAceptados;
        private System.ComponentModel.BackgroundWorker bw_monedas;
        private System.Windows.Forms.PictureBox c_integrado;
        private System.Windows.Forms.TextBox c_TotalBilletes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label l_ref;
        private System.Windows.Forms.Label label24;
    }
}
