﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormAltaUsers : Azteca_SID.FormBase
    {
        DataTable t_perfil;
        public FormAltaUsers()
        {
            InitializeComponent();
        }

        private void c_Alta_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(c_IdUsuario.Text))
            {
                c_confirmacionContraseña.Text = c_IdUsuario.Text;
                c_Contraseña.Text = c_IdUsuario.Text;

                if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                {
                    c_convenio.Text = c_divisabox.SelectedItem.ToString();

                    if (c_refFija.Text.Length > 20)
                    {
                        using (FormaError f_Error = new FormaError(false, "error"))
                        {
                            f_Error.c_MensajeError.Text = "Referencia FIJA debe ser a lo mas 20 posiciones";
                            f_Error.ShowDialog();
                            return;
                        }

                    }
                    if (c_referencia.Text.Length != 10)

                    {
                        using (FormaError f_Error = new FormaError(false, "error"))
                        {
                            f_Error.c_MensajeError.Text = "La Cuenta Bancaria debe tener exactamente 10 posiciones";
                            f_Error.ShowDialog();
                            return;
                        }

                    }
                }


                if (c_confirmacionContraseña.Text == c_Contraseña.Text)
                {
                    if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                        c_convenio.Text = c_divisabox.SelectedItem.ToString();


                    if (!BDUsuarios.InsertarUsuario(c_IdUsuario.Text, int.Parse(c_perfiles.SelectedValue.ToString()), c_Nombre.Text.ToUpper(), c_referencia.Text, c_convenio.Text, c_refFija.Text,c_IdUsuario.Text))
                    {
                        using (FormaError f_Error = new FormaError(false,"error"))
                        {
                            f_Error.c_MensajeError.Text = "No se pudo Dar de Alta, revise los campos o verifique que el EMPLEADO no exista";
                            f_Error.ShowDialog();
                            return;
                        }
                    }
                    else
                    {

                        using (FormaError f_Error = new FormaError(false, "exito"))
                        {
                            f_Error.c_MensajeError.Text = "EMPLEADO dado de alta exitosamente";
                            f_Error.ShowDialog();
                            Close();
                        }
                    }


                }
                else {
                    using (FormaError f_Error = new FormaError(false, "error"))
                    {
                        f_Error.c_MensajeError.Text = "Las Contraseñas no Coinciden";
                        f_Error.ShowDialog();
                        return;
                    }

                }
            }
            else {
                using (FormaError f_Error = new FormaError(false, "error"))
                {
                    f_Error.c_MensajeError.Text = "Proporciona un numero de EMPLEADO";
                    f_Error.ShowDialog();
                    return;
                }
            }
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormAltaUsers_Load(object sender, EventArgs e)
        {


            if (Properties.Settings.Default.CAJA_EMPRESARIAL)
            {
                label1.Visible =false;
                label8.Visible =false;
                label9.Visible =false;
                c_convenio.Visible = false;
                c_divisabox.SelectedIndex = 1;
                c_referencia.MaxLength = 10;
            }
            else
            {
                label10.Visible = false;
                label11.Visible = false;
                label12.Visible = false;

            }

            t_perfil = BDPerfiles.TraerPerfiles();


            c_perfiles.DataSource = t_perfil;
            c_perfiles.DisplayMember = "Descripcion";
            c_perfiles.ValueMember = "IdPerfil";
        }

        private void c_IdUsuario_Click(object sender, EventArgs e)
        {
            c_IdUsuario.Text = EntradaTexto();
        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            c_confirmacionContraseña.Text = EntradaTexto();
        }

        private void c_Contraseña_Click(object sender, EventArgs e)
        {
            c_Contraseña.Text = EntradaTexto();
        }

        private void c_convenio_Click(object sender, EventArgs e)
        {
            c_convenio.Text = EntradaTexto();
        }

        private void c_referencia_Click(object sender, EventArgs e)
        {
            c_referencia.Text = EntradaTexto();
        }

        private void c_Nombre_Click(object sender, EventArgs e)
        {
            c_Nombre.Text = EntradaTexto();
        }

        private String EntradaTexto()
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    return l_teclado.Captura;
                else
                    return "";

            }
        }

        private void c_IdUsuario_TextChanged(object sender, EventArgs e)
        {
           // c_referencia.Text = c_IdUsuario.Text;
            c_Nombre.Text = c_IdUsuario.Text;
          //  c_convenio.Text = c_IdUsuario.Text;
        }

        private void c_refFija_Click(object sender, EventArgs e)
        {
            c_refFija.Text = EntradaTexto();
        }
    }
}
