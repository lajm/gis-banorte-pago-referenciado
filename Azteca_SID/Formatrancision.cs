﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class Formatrancision : Form
    {
        public Formatrancision( )
        {
            InitializeComponent( );
        }

        private void Formatrancision_Load( object sender , EventArgs e )
        {

            t_accion.Enabled = true;
        }

        private void Lanzardeposito( )
        {
            if ( Properties.Settings.Default.Port_YUGO > 0 )
            {
                DialogResult l_R;
                using ( FormaDepositoMixto2 f_DepositoMixto = new FormaDepositoMixto2( ) )
                {

                    l_R = f_DepositoMixto.ShowDialog( FormaPrincipal.s_Forma );

                    f_DepositoMixto.Dispose( );
                }
                if ( l_R == DialogResult.OK )
                {
                    Activate( );
                    using ( FormFINdeposito l_F = new FormFINdeposito( true ) )
                    {

                        l_F.ShowDialog( FormaPrincipal.s_Forma );
                    }
                }


            }
            else
            {
                DialogResult l_R;
                using ( FormaDeposito f_Deposito = new FormaDeposito( ) )
                {

                    l_R = f_Deposito.ShowDialog( FormaPrincipal.s_Forma );


                }
                if ( l_R == DialogResult.OK )
                {
                    Activate( );
                    
                    using ( FormFINdeposito l_F = new FormFINdeposito( true ) )
                    {

                        l_F.ShowDialog( FormaPrincipal.s_Forma );
                    }
                }

            }

           
            Close( );
        }

        private void t_accion_Tick( object sender , EventArgs e )
        {

            t_accion.Enabled = false;
         
            Lanzardeposito( );
        }
    }
}
