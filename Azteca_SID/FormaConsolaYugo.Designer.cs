﻿namespace Azteca_SID
{
    partial class FormaConsolaYugo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.e_consola = new System.Windows.Forms.Label();
            this.c_resetSW = new System.Windows.Forms.Button();
            this.c_resetHW = new System.Windows.Forms.Button();
            this.c_conectar = new System.Windows.Forms.Button();
            this.c_AbrirBovedad = new System.Windows.Forms.Button();
            this.c_ligthON = new System.Windows.Forms.Button();
            this.c_ligthOFF = new System.Windows.Forms.Button();
            this.c_Consulta = new System.Windows.Forms.Button();
            this.dg_monedas = new System.Windows.Forms.DataGridView();
            this.c_vaciarCassete = new System.Windows.Forms.Button();
            this.g_ucoin = new System.Windows.Forms.GroupBox();
            this.c_status = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_autorizar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg_monedas)).BeginInit();
            this.g_ucoin.SuspendLayout();
            this.SuspendLayout();
            // 
            // e_consola
            // 
            this.e_consola.AutoSize = true;
            this.e_consola.BackColor = System.Drawing.Color.Transparent;
            this.e_consola.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_consola.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_consola.Location = new System.Drawing.Point(243, 62);
            this.e_consola.Name = "e_consola";
            this.e_consola.Size = new System.Drawing.Size(314, 29);
            this.e_consola.TabIndex = 45;
            this.e_consola.Text = "CONSOLA DE MONEDAS";
            // 
            // c_resetSW
            // 
            this.c_resetSW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resetSW.Location = new System.Drawing.Point(143, 19);
            this.c_resetSW.Name = "c_resetSW";
            this.c_resetSW.Size = new System.Drawing.Size(121, 49);
            this.c_resetSW.TabIndex = 46;
            this.c_resetSW.Text = "ResetSW";
            this.c_resetSW.UseVisualStyleBackColor = true;
            this.c_resetSW.Click += new System.EventHandler(this.c_resetSW_Click);
            // 
            // c_resetHW
            // 
            this.c_resetHW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_resetHW.Location = new System.Drawing.Point(270, 19);
            this.c_resetHW.Name = "c_resetHW";
            this.c_resetHW.Size = new System.Drawing.Size(132, 49);
            this.c_resetHW.TabIndex = 47;
            this.c_resetHW.Text = "ResetHW";
            this.c_resetHW.UseVisualStyleBackColor = true;
            this.c_resetHW.Click += new System.EventHandler(this.c_resetHW_Click);
            // 
            // c_conectar
            // 
            this.c_conectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_conectar.Location = new System.Drawing.Point(27, 446);
            this.c_conectar.Name = "c_conectar";
            this.c_conectar.Size = new System.Drawing.Size(121, 49);
            this.c_conectar.TabIndex = 48;
            this.c_conectar.Text = "Conectar";
            this.c_conectar.UseVisualStyleBackColor = true;
            this.c_conectar.Click += new System.EventHandler(this.c_conectar_Click);
            // 
            // c_AbrirBovedad
            // 
            this.c_AbrirBovedad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_AbrirBovedad.Location = new System.Drawing.Point(14, 163);
            this.c_AbrirBovedad.Name = "c_AbrirBovedad";
            this.c_AbrirBovedad.Size = new System.Drawing.Size(121, 49);
            this.c_AbrirBovedad.TabIndex = 49;
            this.c_AbrirBovedad.Text = "Abrir Bovedad";
            this.c_AbrirBovedad.UseVisualStyleBackColor = true;
            this.c_AbrirBovedad.Click += new System.EventHandler(this.c_AbrirBovedad_Click);
            // 
            // c_ligthON
            // 
            this.c_ligthON.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ligthON.Location = new System.Drawing.Point(408, 19);
            this.c_ligthON.Name = "c_ligthON";
            this.c_ligthON.Size = new System.Drawing.Size(130, 49);
            this.c_ligthON.TabIndex = 50;
            this.c_ligthON.Text = "Prender LED";
            this.c_ligthON.UseVisualStyleBackColor = true;
            this.c_ligthON.Click += new System.EventHandler(this.c_ligthON_Click);
            // 
            // c_ligthOFF
            // 
            this.c_ligthOFF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ligthOFF.Location = new System.Drawing.Point(544, 19);
            this.c_ligthOFF.Name = "c_ligthOFF";
            this.c_ligthOFF.Size = new System.Drawing.Size(138, 49);
            this.c_ligthOFF.TabIndex = 51;
            this.c_ligthOFF.Text = "Apagar LED";
            this.c_ligthOFF.UseVisualStyleBackColor = true;
            this.c_ligthOFF.Click += new System.EventHandler(this.c_ligthOFF_Click);
            // 
            // c_Consulta
            // 
            this.c_Consulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Consulta.Location = new System.Drawing.Point(14, 74);
            this.c_Consulta.Name = "c_Consulta";
            this.c_Consulta.Size = new System.Drawing.Size(121, 49);
            this.c_Consulta.TabIndex = 52;
            this.c_Consulta.Text = "Consulta de Monedas";
            this.c_Consulta.UseVisualStyleBackColor = true;
            this.c_Consulta.Click += new System.EventHandler(this.c_Consulta_Click);
            // 
            // dg_monedas
            // 
            this.dg_monedas.AllowUserToAddRows = false;
            this.dg_monedas.AllowUserToDeleteRows = false;
            this.dg_monedas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_monedas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_monedas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_monedas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_monedas.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_monedas.Location = new System.Drawing.Point(175, 74);
            this.dg_monedas.Name = "dg_monedas";
            this.dg_monedas.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_monedas.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dg_monedas.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_monedas.Size = new System.Drawing.Size(478, 245);
            this.dg_monedas.TabIndex = 53;
            // 
            // c_vaciarCassete
            // 
            this.c_vaciarCassete.Enabled = false;
            this.c_vaciarCassete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_vaciarCassete.Location = new System.Drawing.Point(14, 241);
            this.c_vaciarCassete.Name = "c_vaciarCassete";
            this.c_vaciarCassete.Size = new System.Drawing.Size(121, 49);
            this.c_vaciarCassete.TabIndex = 54;
            this.c_vaciarCassete.Text = "Vaciar Cassete";
            this.c_vaciarCassete.UseVisualStyleBackColor = true;
            this.c_vaciarCassete.Click += new System.EventHandler(this.c_vaciarCassete_Click);
            // 
            // g_ucoin
            // 
            this.g_ucoin.Controls.Add(this.c_autorizar);
            this.g_ucoin.Controls.Add(this.c_vaciarCassete);
            this.g_ucoin.Controls.Add(this.dg_monedas);
            this.g_ucoin.Controls.Add(this.c_Consulta);
            this.g_ucoin.Controls.Add(this.c_ligthOFF);
            this.g_ucoin.Controls.Add(this.c_ligthON);
            this.g_ucoin.Controls.Add(this.c_AbrirBovedad);
            this.g_ucoin.Controls.Add(this.c_resetHW);
            this.g_ucoin.Controls.Add(this.c_resetSW);
            this.g_ucoin.Location = new System.Drawing.Point(49, 94);
            this.g_ucoin.Name = "g_ucoin";
            this.g_ucoin.Size = new System.Drawing.Size(702, 344);
            this.g_ucoin.TabIndex = 55;
            this.g_ucoin.TabStop = false;
            this.g_ucoin.Text = "Ucoin";
            this.g_ucoin.Visible = false;
            // 
            // c_status
            // 
            this.c_status.Enabled = false;
            this.c_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_status.ForeColor = System.Drawing.SystemColors.Info;
            this.c_status.Location = new System.Drawing.Point(287, 473);
            this.c_status.Name = "c_status";
            this.c_status.Size = new System.Drawing.Size(464, 22);
            this.c_status.TabIndex = 56;
            this.c_status.TextChanged += new System.EventHandler(this.c_status_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(199, 477);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 57;
            this.label1.Text = "Estatus: ";
            // 
            // c_autorizar
            // 
            this.c_autorizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_autorizar.Location = new System.Drawing.Point(14, 307);
            this.c_autorizar.Name = "c_autorizar";
            this.c_autorizar.Size = new System.Drawing.Size(121, 31);
            this.c_autorizar.TabIndex = 58;
            this.c_autorizar.Text = "Autorizar";
            this.c_autorizar.UseVisualStyleBackColor = true;
            this.c_autorizar.Click += new System.EventHandler(this.c_autorizar_Click);
            // 
            // FormaConsolaYugo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_status);
            this.Controls.Add(this.g_ucoin);
            this.Controls.Add(this.c_conectar);
            this.Controls.Add(this.e_consola);
            this.Name = "FormaConsolaYugo";
            this.Text = "FormaConsolaYugo";
            this.Load += new System.EventHandler(this.FormaConsolaYugo_Load);
            this.Controls.SetChildIndex(this.e_consola, 0);
            this.Controls.SetChildIndex(this.c_conectar, 0);
            this.Controls.SetChildIndex(this.g_ucoin, 0);
            this.Controls.SetChildIndex(this.c_status, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dg_monedas)).EndInit();
            this.g_ucoin.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label e_consola;
        private System.Windows.Forms.Button c_resetSW;
        private System.Windows.Forms.Button c_resetHW;
        private System.Windows.Forms.Button c_conectar;
        private System.Windows.Forms.Button c_AbrirBovedad;
        private System.Windows.Forms.Button c_ligthON;
        private System.Windows.Forms.Button c_ligthOFF;
        private System.Windows.Forms.Button c_Consulta;
        private System.Windows.Forms.DataGridView dg_monedas;
        private System.Windows.Forms.Button c_vaciarCassete;
        private System.Windows.Forms.GroupBox g_ucoin;
        private System.Windows.Forms.TextBox c_status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_autorizar;
    }
}