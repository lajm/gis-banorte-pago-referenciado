﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaCambioBolsa : FormBase
    {
        public FormaCambioBolsa()
        {
            InitializeComponent();
            
        }

        int m_porcentajeAnterior = 0;
        bool Exitoso = false;
        bool SELLADA = false;
        int SensorBolsaRemovida = 63;
        DialogResult l_resultado;

        private void c_empezar_Click(object sender, EventArgs e)
        {
            c_empezar.Enabled = false;
            c_Salir.Enabled = false;
            paso1.Visible = true;
            c_paso1.Visible = true;

         

            Forzardibujado();
            FormaPrincipal.s_Forma.Gsi_Cronometro( false );
            Registrar.Alerta("Intento de Sellado de BOLSA, ...Empieza Procedimiento...");

            //   Secuencia();
            //   Close();

            if ( !bw_cambiobolsa.IsBusy )
            {

                bw_cambiobolsa.WorkerSupportsCancellation = true;


                bw_cambiobolsa.RunWorkerAsync( );
               
            }
          


        }

        private void c_Salir_Click(object sender, EventArgs e)
        {
            GC.Collect();
            this.DialogResult = DialogResult.Abort;
            Close();
        }

        public void
   EscribirBitacora(String p_Funcion, String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.AppendText("Sellado.txt"))
            {
                l_Archivo.WriteLine(DateTime.Now + p_Funcion + ":  " + p_Mensaje);
            }

            Globales.EscribirBitacora("Area DE SELLADO", p_Funcion, p_Mensaje, 3);
        }

        public void
           EscribirNumeroDeBOLSA(String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("NumeroBOLSA.txt"))
            {
                l_Archivo.WriteLine(p_Mensaje);
            }
        }

        public void
          EscribirPorcentaje(int p_Porcentaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("PorcentajeSellado.txt"))
            {
                l_Archivo.WriteLine(p_Porcentaje);
            }
        }


        public void
          EscribirProcesoExitoso(int p_Procesocompleto)
        {
            if (File.Exists("PorcentajeSellado.txt"))
            {
                using (StreamWriter l_Archivo = File.AppendText("PorcentajeSellado.txt"))
                {
                    l_Archivo.WriteLine(p_Procesocompleto);
                }
            }
        }

        public void LeerPorcentaje(out bool p_exitoso)
        {
            int lectura = 0;

            if (File.Exists("PorcentajeSellado.txt"))
            {
                String[] lineas = File.ReadAllLines("PorcentajeSellado.txt");

                if (lineas.Length > 0)
                {
                    m_porcentajeAnterior = Convert.ToInt16(lineas[0]);
                }
                else
                    m_porcentajeAnterior = 100;

                if (lineas.Length > 1)
                {
                    lectura = Convert.ToInt16(lineas[lineas.Length - 1]);

                    if (lectura == 1)
                        p_exitoso = true;
                    else
                        p_exitoso = false;

                }
                else
                    p_exitoso = true;


            }
            else
            {
                m_porcentajeAnterior = 100;
                p_exitoso = true;
            }

        }

        public void Secuencia( )
        {

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_porcentaje = 0;
            bool l_ProcesoAnteriorCompleto = true;
            StringBuilder l_barcode = new StringBuilder( 128 );
            short l_length = 128;
            int l_auxiliarporcentajesellado = 0;
            int l_intentos = 0;
            int l_bolsacorectacontador = 0;

            Forzardibujado( );

            // LeerPorcentaje(out l_ProcesoAnteriorCompleto);
            m_porcentajeAnterior = 100;


            SidLib.ResetError( );

            l_Reply = SidLib.SID_Open( true );
            EscribirBitacora( "OPEN" , l_Reply.ToString( ) );

            char[] l_username = Properties.Settings.Default.NumSerialEquipo.ToCharArray( );

            l_Reply = SidApi.SidLib.SID_Login( l_username );

            Globales.EscribirBitacora( "SELECT ETV" , "SID_LOGIN" , "login SIDserial: " + l_Reply , 3 );



            l_Reply = SidLib.SID_CheckCodeLockCorrect( 30 );




            Error_Sid( l_Reply , out l_Error );
            EscribirBitacora( "CheckCode" , l_Reply.ToString( ) );

            if ( l_Reply == SidLib.SID_OKAY )
            {

                c_paso1.Text = "CODIGO CORRECTO";

                paso2.Visible = true;
                c_paso2.Visible = true;
                c_esperarsellado.Visible = true;
                Forzardibujado( );




                //// REVISAR EL PROCESO DE RECAMBIO \\\\

                if ( !( m_porcentajeAnterior == 100 ) && !l_ProcesoAnteriorCompleto )
                {
                    EscribirBitacora( "EVENTO:::" , "Continuar con Sellado de BOLSA interrumpido" );
                    //El auxiliar es para repetir todo el proceso pero con el porcentaje anterior.
                    l_auxiliarporcentajesellado = m_porcentajeAnterior;
                    m_porcentajeAnterior = 100;
                    l_ProcesoAnteriorCompleto = true;

                }

                if ( m_porcentajeAnterior == 100 && l_ProcesoAnteriorCompleto )
                {

                    EscribirBitacora( "EVENTO:::" , "Empezar Proceso Completo Sellado de BOLSA" );
                    m_porcentajeAnterior = l_auxiliarporcentajesellado;


                    l_Reply = SidLib.SID_JoinBag( m_porcentajeAnterior );
                    Error_Sid( l_Reply , out l_Error );
                    EscribirBitacora( "JoinBAg" , l_Reply.ToString( ) );

                    if ( l_Reply == SidLib.SID_OKAY )
                    {

                        c_esperarsellado.Visible = false;
                        c_paso2.Text = "... SELLANDO BOLSA ...";
                        panelSellado1.Visible = true;
                        panelSellado1.Empezar( );
                        Forzardibujado( );


                        do
                        {

                            l_Reply = SidLib.SID_JoinBagResult( ref l_porcentaje );
                            if ( l_Reply != 0 && l_Reply != 6 )
                                Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "result JoinBAG" , l_Reply.ToString( ) + " :: % " + l_porcentaje );
                            Forzardibujado( );

                        } while ( l_Reply == SidLib.SID_PERIF_BUSY );

                        if ( l_porcentaje != 0 )
                        {
                            EscribirPorcentaje( l_porcentaje );
                            Error_Sid( l_Reply , out l_Error );
                        }

                        if ( l_porcentaje == 100 )
                        {
                            SELLADA = true;

                        }
                        else
                            using ( FormaError f_Error = new FormaError( true , "warning" ) )
                            {
                                f_Error.c_MensajeError.Text = l_Error;
                                f_Error.TopMost = true;
                                f_Error.ShowDialog( );
                                //c_BotonCancelar_Click(this, null);
                            }
                    }
                    else
                    {
                        using ( FormaError f_Error = new FormaError( true , "warning" ) )
                        {
                            f_Error.c_MensajeError.Text = l_Error;
                            f_Error.TopMost = true;
                            f_Error.ShowDialog( );
                            //c_BotonCancelar_Click(this, null);
                        }

                        if ( l_Reply < -98 && l_Reply > -108 || l_Reply == SidLib.SID_ERROR_ON_CLOSE_BAG )
                        {

                            int l_causa = l_Reply;
                            using ( FormaAvisoEsperar l_avisoesperar = new FormaAvisoEsperar( ) )
                            {

                                l_avisoesperar.Mensaje( "Espere Analizando Por que no se puede sellar la Bolsa..." );
                                l_avisoesperar.Focus( );
                                l_avisoesperar.Show( );
                                Forzardibujado( );



                                try
                                {

                                    DirectoryInfo directory = new DirectoryInfo( @".\Webcam" );

                                    FileInfo[] files = directory.GetFiles( "*.*" );



                                    for ( int i = 0; i < files.Length; i++ )
                                    {
                                        File.Delete( files[i].FullName );


                                    }

                                    EscribirBitacora( "Archivos WebCam Borrados" , "Carpeta Limpia" );
                                    directory.Refresh( );

                                }
                                catch ( Exception ex )
                                {
                                    EscribirBitacora( "Delete archivos Webcam" , ex.Message );
                                    Globales.EscribirBitacora( "Cambio Bolsa" , "Delete archivos" , ex.Message , 1 );
                                }

                                l_avisoesperar.Mensaje( "Tomando Nuevas imagenes,... inicializando..." );
                                Forzardibujado( );


                                SidLib.ResetError( );

                                l_Reply = SidLib.SID_Open( false );
                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "OPEN" , l_Error );

                                l_Reply = SidLib.SID_Initialize( );


                                EscribirBitacora( "Inicializando... " , l_Error );

                                if ( l_Reply != 0 )
                                {
                                    string l_Error = "";
                                    Error_Sid( l_Reply , out l_Error );

                                    using ( FormaError f_Error = new FormaError( false , "iniciando" ) )
                                    {
                                        f_Error.c_MensajeError.Text = l_Error;
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                        //c_BotonCancelar_Click(this, null);
                                    }


                                }
                                else
                                {
                                    string l_Error = "";
                                    Error_Sid( l_causa , out l_Error );
                                    EscribirBitacora( "Error en JOIN_BAG" , "ESTAUS: " + l_Reply );

                                    using ( FormaError f_Error = new FormaError( true , "fallo" ) )
                                    {
                                        f_Error.c_MensajeError.Text = " Por favor Intente de nuevo el Procedimiento, Error Previo:  " + l_Error;
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                        //c_BotonCancelar_Click(this, null);
                                    }


                                }



                                l_avisoesperar.Close( );
                            }
                            Forzardibujado( );
                            Exitoso = false;
                            return;
                        }



                        else
                        {

                            using ( FormaError f_Error = new FormaError( true , "falla" ) )
                            {
                                String l_Error;
                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "ERROR JOINBAG: " , l_Error );
                                f_Error.c_MensajeError.Text = "Problema con el Sellado Error: Error Previo:  " + l_Error;
                                f_Error.c_Imagen.Visible = false;
                                // EscribirProcesoExitoso(0);
                                f_Error.ShowDialog( );
                            }
                            Exitoso = false;
                            return;
                        }
                    }
                }

                if ( m_porcentajeAnterior == 100 && !l_ProcesoAnteriorCompleto )
                {
                    EscribirBitacora( "EVENTO:::" , "TERMINAR Retiro el retiro de BOLSA" );
                    EscribirPorcentaje( m_porcentajeAnterior );

                    SELLADA = true;
                }


                panelSellado1.Detener( );
                panelSellado1.Visible = false;
                Forzardibujado( );

                l_Reply = SidLib.SID_EnableToOpenDoor( );
                Error_Sid( l_Reply , out l_Error );

                EscribirBitacora( "Enable OpenDoor" , l_Reply.ToString( ) );
                if ( l_Reply == SidLib.SID_OKAY )
                {
                    paso3.Visible = true;
                    c_paso3.Visible = true;
                    Forzardibujado( );

                    l_Reply = SidLib.SID_WaitDoorOpen( );
                    Error_Sid( l_Reply , out l_Error );

                    EscribirBitacora( "wait Door Open" , l_Reply.ToString( ) );

                    if ( l_Reply == SidLib.SID_OKAY )
                    {

                        c_paso3.Text = "Leer Codigo de Barras Actual: " + Globales.NumeroSerieBOLSA;
                        Forzardibujado( );

                        EscribirBitacora( "Barcode Retirado: " , Globales.NumeroSerieBOLSA );

                        Globales.BolsaCorrecta = true;


                        paso4.Visible = true;
                        c_paso4.Visible = true;
                        Forzardibujado( );

                        int minutos = DateTime.Now.Minute + 2;
                        int dif = 0;
                        if ( minutos > 57 )
                            dif = 60;

                        c_esperarInicio.Visible = true;
                        Forzardibujado( );

                        do
                        { //Wait remove the bag 
                            System.Threading.Thread.Sleep( 300 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            EscribirBitacora( "Remove Bag" , sensor[2].ToString( ) );
                            if ( minutos <= DateTime.Now.Minute + dif )
                                break;

                        } while ( sensor[2] != SensorBolsaRemovida ); //127 para version sid.dll > 2.0.0.2 , 63 para version <= 2.0.0.2 SID_CFE
                        EscribirBitacora( "BOLSA REMOVIDA..." , sensor[2].ToString( ) );
                        Error_Sid( l_Reply , out l_Error );
                        // l_Reply = SidLib.SID_WaitRemoveBag(60); /* Wait the remove of the bag*/
                        c_esperarInicio.Visible = false;
                        paso5.Visible = true;
                        c_paso5.Visible = true;
                        Forzardibujado( );

                        int l_trys = 0;
                        do
                        {
                            SidLib.SID_Open( false );

                            l_length = 128;
                            if ( l_trys > 0 )
                                using ( FormaError f_Error = new FormaError( false , "warning" ) )
                                {
                                    f_Error.c_MensajeError.Text = "Aparentemente El Folio de BOLSA ya se USO, use Otra BOLSA con folio Distinto... ";
                                    f_Error.TopMost = true;
                                    f_Error.ShowDialog( );
                                }

                            Forzardibujado( );

                            l_Reply = SidLib.SID_ReadBarcode( l_barcode , ref l_length , 60 );
                            EscribirBitacora( "Funcion Barcode: " , l_Reply.ToString( ) );

                            if ( l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED || l_Reply == SidLib.SID_DATATRUNC )
                            {
                                try
                                {
                                    string l_barcodelimpio = l_barcode.ToString( ).Substring( 0 , l_barcode.ToString( ).Length - 5 ).Replace( "," , "" );
                                    EscribirNumeroDeBOLSA( l_barcodelimpio );
                                    Globales.NumeroSerieBOLSA = l_barcodelimpio;
                                    EscribirBitacora( "Barcode Nuevo: " , l_barcodelimpio );
                                    c_barcodenuevo.Text = l_barcodelimpio;
                                    c_barcodenuevo.Visible = true;
                                }
                                catch ( Exception l_error )
                                {
                                    EscribirBitacora( "ERROR de LECTURA " , l_error.Message );
                                    if ( String.IsNullOrEmpty( l_barcode.ToString( ) ) )
                                    {
                                        string l_barcode2 = l_barcode.ToString( ).Replace( "," , "" );
                                        EscribirNumeroDeBOLSA( l_barcode2 );
                                        Globales.NumeroSerieBOLSA = l_barcode2;
                                        EscribirBitacora( "Barcode Nuevo: " , l_barcode2 );
                                        c_barcodenuevo.Text = l_barcode2;
                                        c_barcodenuevo.Visible = true;
                                    }
                                    else
                                    {
                                        EscribirNumeroDeBOLSA( "000000" );
                                        Globales.NumeroSerieBOLSA = "000000";
                                        c_barcodenuevo.Text = "NO Capturado";
                                        c_barcodenuevo.Visible = true;
                                    }

                                }

                            }
                            l_trys++;
                            SidLib.SID_Close( );
                        } while ( !ComprobacionEnvaseNuevo( Globales.NumeroSerieBOLSA ) && l_trys < 3 );

                        EscribirBitacora( "Intentos de Lectura Barcode:  " , l_trys.ToString( ) );
                        if ( l_trys >= 3 && !ComprobacionEnvaseNuevo( Globales.NumeroSerieBOLSA ) )
                        {
                            EscribirNumeroDeBOLSA( "000000" );
                            Globales.NumeroSerieBOLSA = "000000";
                            c_barcodenuevo.Text = "NO Capturado";
                            c_barcodenuevo.Visible = true;
                            Forzardibujado( );

                            DialogResult l_respuesta = DialogResult.Abort;

                            do
                            {
                                if ( l_respuesta == DialogResult.Cancel )
                                    using ( FormaError f_Error = new FormaError( false , "warning" ) )
                                    {
                                        f_Error.c_MensajeError.Text = "Necesitamos un Numero de Envase VALIDO para Continuar";
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                    }

                                Forzardibujado( );
                                using ( FormaCambioNumeroBolsa f_cambiobolsa = new FormaCambioNumeroBolsa( ) )
                                {
                                    f_cambiobolsa.TopMost = true;
                                    l_respuesta = f_cambiobolsa.ShowDialog( );

                                }

                            } while ( l_respuesta != DialogResult.OK );
                        }

                        c_barcodenuevo.Text = Globales.NumeroSerieBOLSA;
                        EscribirBitacora( "Cambio de Bolsa: " , Globales.NumeroSerieBOLSA );
                        c_paso5.Text = "PONER BOLSA NUEVA";
                        Forzardibujado( );
                        //  l_Reply = SidLib.SID_WaitReplaceBag(60); /* Wait the replacement of the bag*/
                        SidLib.SID_Open( false );
                        Error_Sid( l_Reply , out l_Error );
                        do
                        { //Wait a new bag insertion 
                            System.Threading.Thread.Sleep( 300 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            EscribirBitacora( "Bag insertion" , sensor[7].ToString( ) );
                        } while ( ( sensor[7] & 0x02 ) == 2 );
                        Error_Sid( l_Reply , out l_Error );
                        EscribirBitacora( "Bag insertion" , sensor[7].ToString( ) );
                        EscribirBitacora( "Bag COrrcet ??" , sensor[7].ToString( ) );
                        c_paso5.Text = "VERIFICANDO BOLSA, Espere...";
                        c_esperarInicio.Visible = true;
                        Forzardibujado( );
                        //VERIFICAR BOLSA PUESTA



                        do
                        {
                            System.Threading.Thread.Sleep( 100 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            l_bolsacorectacontador++;

                        } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );

                        Error_Sid( l_Reply , out l_Error );

                        EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                        EscribirBitacora( "Ciclo completo: " , l_bolsacorectacontador.ToString( ) );

                        if ( l_bolsacorectacontador < 101 )
                        {
                            using ( FormaError f_Error = new FormaError( true , "revisar" ) )
                            {
                                f_Error.c_MensajeError.Text = "Por favor verifique que la  BOLSA este bien puesta, solo tendra una oportunidad mas.";
                                f_Error.c_Imagen.Visible = true;
                                f_Error.ShowDialog( );
                            }

                            EscribirBitacora( "Bolsa Mal Puesta " , "Reintentar  " );
                            c_paso5.Text = "Re - VERIFICANDO BOLSA, Espere...";
                            c_esperarInicio.Visible = true;
                            Forzardibujado( );
                            l_bolsacorectacontador = 0;
                            do
                            {
                                System.Threading.Thread.Sleep( 100 );
                                l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                                l_bolsacorectacontador++;
                            } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );
                            Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                            EscribirBitacora( "SEGUNDO Ciclo completo: " , l_bolsacorectacontador.ToString( ) );
                            //Acciones a tomar
                            if ( l_bolsacorectacontador < 101 )
                            {
                                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                                {
                                    f_Error.c_MensajeError.Text = "Esta es la ultima Oportunidad, Verifique  BOLSA este bien puesta, en caso contrario, usted obtendra su Ticket pero se mandara una alerta de Servicio para este Equipo";
                                    f_Error.c_Imagen.Visible = true;
                                    f_Error.ShowDialog( );
                                }

                                EscribirBitacora( "Bolsa Mal Puesta " , "Ultimo Reintento  " );
                                c_paso5.Text = "Re - VERIFICANDO BOLSA, Espere...";
                                c_esperarInicio.Visible = true;
                                Forzardibujado( );
                                l_bolsacorectacontador = 0;
                                do
                                {
                                    System.Threading.Thread.Sleep( 100 );
                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                                    l_bolsacorectacontador++;
                                } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );

                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                                EscribirBitacora( "SEGUNDO Ciclo completo: " , l_bolsacorectacontador.ToString( ) );
                                //Acciones a tomar
                                //Enviar ALerta
                                //continuar Proceso...

                            }
                            else
                            {
                                //Entregar Ticket

                                c_ok.Visible = true;
                                paso5.Text = "BOLSA OK";
                                Forzardibujado( );
                            }


                        }
                        else
                        {
                            c_ok.Visible = true;
                            paso5.Text = "BOLSA OK";
                            c_esperarInicio.Visible = false;
                            Forzardibujado( );
                        }

                        paso6.Visible = true;
                        c_paso6.Visible = true;
                        c_esperarInicio.Visible = true;
                        Forzardibujado( );

                        l_Reply = SidLib.SID_WaitDoorClosed( 120 ); /*Wait closing the door */
                        Error_Sid( l_Reply , out l_Error );
                        EscribirBitacora( "wait Door CLOSE" , l_Reply.ToString( ) );

                        if ( l_Reply == SidLib.SID_TIME_OUT_EXPIRED )
                        {
                            using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                            {
                                f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta,solo tendra una oportunidad mas.";
                                f_Error.c_Imagen.Visible = false;
                                f_Error.ShowDialog( );
                            }
                            Forzardibujado( );
                            l_Reply = SidLib.SID_WaitDoorClosed( 30 ); /*Wait closing the door */

                            EscribirBitacora( "2do wait Door CLOSE" , l_Reply.ToString( ) );
                        }


                        if ( l_Reply == SidLib.SID_OKAY ) ///check corect instlacion
                        {
                            c_paso6.Text = "Espere inicializando Modulo";
                            Forzardibujado( );

                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                            Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "STATUS : " , l_Reply.ToString( ) );
                            EscribirBitacora( "SENSOR[2] : " , sensor[2].ToString( ) );
                            EscribirBitacora( "SENSOR [7] : " , sensor[7].ToString( ) + ( sensor[7] & 0x02 ).ToString( ) );
                            if ( ( ( sensor[7] & 0x02 ) == 2 ) )
                            {
                                do
                                {
                                    MessageBox.Show( "Revise la colocación de la bolsa" );

                                    MessageBox.Show( "Introduzca la clave" );
                                    l_Reply = SidLib.SID_CheckCodeLockCorrect( 30 );
                                    Error_Sid( l_Reply , out l_Error );
                                    if ( l_Reply == SidLib.SID_OKAY )
                                    {
                                        MessageBox.Show( "Abra la Boveda, revise la bolsa y cierre la puerta" );
                                        l_Reply = SidLib.SID_EnableToOpenDoor( );
                                        Error_Sid( l_Reply , out l_Error );
                                        if ( l_Reply == SidLib.SID_OKAY )
                                        {
                                            l_Reply = SidLib.SID_WaitDoorOpen( );
                                            Error_Sid( l_Reply , out l_Error );
                                        }
                                        else
                                        {
                                            l_Reply = SidLib.SID_ForceOpenDoor( );
                                            Error_Sid( l_Reply , out l_Error );
                                        }


                                        if ( l_Reply == SidLib.SID_OKAY )
                                        {
                                            l_Reply = SidLib.SID_WaitReplaceBag( 60 ); /* Wait the replacement of the bag*/
                                            Error_Sid( l_Reply , out l_Error );

                                            l_Reply = SidLib.SID_WaitDoorClosed( 60 ); /*Wait closing the door */
                                            Error_Sid( l_Reply , out l_Error );

                                        }
                                    }

                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                    Error_Sid( l_Reply , out l_Error );
                                    EscribirBitacora( "STATUS : " , l_Reply.ToString( ) );
                                    EscribirBitacora( "SENSOR[2] : " , sensor[2].ToString( ) );
                                    EscribirBitacora( "SENSOR [7] : " , sensor[7].ToString( ) );

                                } while ( ( ( sensor[7] & 0x02 ) == 2 ) );

                                c_paso6.Text = "Terminado";
                                c_esperarInicio.Visible = false;
                                EscribirProcesoExitoso( 1 );
                                Exitoso = true;
                                Forzardibujado( );


                            }
                            else
                            {
                                c_paso6.Text = "Terminado";
                                c_esperarInicio.Visible = false;
                                EscribirProcesoExitoso( 1 );
                                Exitoso = true;
                                Forzardibujado( );

                            }
                        }
                        else
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                            using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                            {
                                f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta, Problablemente se necesite un Master RESET antes de volver a SELLAR";
                                f_Error.c_Imagen.Visible = false;
                                EscribirProcesoExitoso( 0 );
                                f_Error.ShowDialog( );
                            }
                            Exitoso = true;
                            this.DialogResult = DialogResult.OK;
                            Registrar.Alerta( "MAL Procedimineto de SELLADO, Problablemente No se Verifico la Colocacion de Bolsa o tiempo agotado para cerrar." );
                            c_Salir.Enabled = true;
                            return;
                        }

                    }
                    else
                    {
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                        using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                        {
                            f_Error.c_MensajeError.Text = "Tiempo Agotado para Abrir Puerta repita Operación";
                            f_Error.c_Imagen.Visible = false;
                            EscribirProcesoExitoso( 0 );
                            f_Error.ShowDialog( );
                        }
                        Exitoso = false;
                        return;
                    }
                }
                else
                {
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    using ( FormaError f_Error = new FormaError( true , "llave" ) )
                    {
                        f_Error.c_MensajeError.Text = "Mensaje de Apertura Rechazado: Error: " + l_Reply;
                        f_Error.c_Imagen.Visible = false;
                        EscribirProcesoExitoso( 0 );
                        f_Error.ShowDialog( );
                    }
                    Exitoso = false;
                    return;
                }


                l_Reply = SidApi.SidLib.SID_Logout( );
                Globales.EscribirBitacora( "SELECT ETV" , "SID_LOgout" , "logout: " + l_Reply , 3 );

                l_Reply = SidLib.SID_Close( ); /*Disconnect from unit */

            }
            else
            {
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                {

                    f_Error.c_MensajeError.Text = "Codigo incorecto o se agoto el tiempo de espera";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.ShowDialog( );
                }
                Exitoso = false;
                return;
            }

        }



        private bool ComprobacionEnvaseNuevo(string p_numeroSerieBOLSA)
        {
            if (String.IsNullOrWhiteSpace(p_numeroSerieBOLSA))
            {
                Globales.EscribirBitacora("Comprobar Envase: ", p_numeroSerieBOLSA, "En blanco", 3);
                return false;
            }

            if (BDSide.BuscarENVASE(p_numeroSerieBOLSA))
            {
                Globales.EscribirBitacora("Comprobar Envase: ", p_numeroSerieBOLSA, "No Encontrado", 3);
                return true;
            }
            else
            {
                Globales.EscribirBitacora("Comprobar Envase: ", p_numeroSerieBOLSA, "Encontrado", 3);
                return false;
            }
        }

        private void FormaCambioBolsa_Load(object sender, EventArgs e)
        {
            Forzardibujado();
            SensorBolsaRemovida = Globales.SensorRemoverBolsa;

            using (FormaError f_Error = new FormaError(true, "informacion"))
            {
                f_Error.c_MensajeError.Text = "Por Favor siga la Secuencia Descrita en la siguiente pantalla " + " INSTRUCCIONES";
                f_Error.ShowDialog();
            }
        }

        private void FormaCambioBolsa_FormClosing(object sender, FormClosingEventArgs e)
        {

            if ( bw_cambiobolsa.IsBusy )
            {
                e.Cancel = true;
            }
            else
            {

                if ( SELLADA )
                    this.DialogResult = DialogResult.Ignore;
                else
                    this.DialogResult = DialogResult.Abort;

                if ( Exitoso && SELLADA )
                {
                    this.DialogResult = DialogResult.OK;
                    EscribirProcesoExitoso( 1 );
                }
                else
                    EscribirProcesoExitoso( 0 );
                EscribirBitacora( "Exitoso " , Exitoso.ToString( ) );
                EscribirBitacora( "SELLADA : " , SELLADA.ToString( ) );
            }
        }

        public void CompararBARCODE(String l_barcode_retirado)
        {
            if (Globales.NumeroSerieBOLSA == l_barcode_retirado)
                Globales.BolsaCorrecta = true;
            else
            {
                Globales.EscribirBitacora("Retiro de Bolsa", "CompararBARCODE", "Se esperaba el numero: " + Globales.NumeroSerieBOLSA + "  se obtuvo: " + l_barcode_retirado, 1);
                Globales.BolsaCorrecta = false;
            }

        }

        public void Error_Sid( int l_Reply , out String p_Error )
        {
            String l_Error = "";

            SidLib.Error_Sid( l_Reply , out l_Error );
            p_Error = l_Error;

            if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {

                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply < 0 && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                }


                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }
            }

        }
        private void FormaCambioBolsa_Click(object sender, EventArgs e)
        {

        }

        private void Cambiatexto (TextBox  p_control, String p_mensaje)
        {
            try
            {

                p_control.Invoke( new Action( ( ) => p_control.Text = p_mensaje ) );
            }
            catch(Exception exc)
            {
                Globales.EscribirBitacora(" CambioBolsa", "Cambiatexto", "Exception: " + exc.Message,3);
            }
        }

        private void CambiatextoEtiqueta( Label p_control , String p_mensaje )
        {
            try
            {

                p_control.Invoke( new Action( ( ) => p_control.Text = p_mensaje ) );
            }
            catch ( Exception exc )
            {
                Globales.EscribirBitacora( " CambioBolsa" , "Cambiatexto" , "Exception: " + exc.Message , 3 );
            }
        }
        private void TextoVisible( TextBox p_control , bool p_visible )
        {
            try
            {
                p_control.Invoke( new Action( ( ) => p_control.Visible = p_visible ) );
            }
            catch ( Exception exc )
            {
                Globales.EscribirBitacora( " CambioBolsa" , "TextoVisible" , "Exception: " + exc.Message , 3 );
            }

        }

        private void LabelVisible ( Label p_control, bool p_visible )
        {
            try
            { 
            p_control.Invoke( new Action( ( ) => p_control.Visible = p_visible) );
        }
            catch(Exception exc)
            {
                Globales.EscribirBitacora(" CambioBolsa", "LabelVisible", "Exception: " + exc.Message,3);
            }

        }

        private void PictureVisible( PictureBox  p_control , bool p_visible )
        {
            try
            {
                p_control.Invoke( new Action( ( ) => p_control.Visible = p_visible ) );
            }
            catch ( Exception exc )
            {
                Globales.EscribirBitacora( " CambioBolsa" , "PictureVisible" , "Exception: " + exc.Message , 3 );
            }

        }

        private void bw_cambiobolsa_DoWork( object sender , DoWorkEventArgs e )
        {
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_porcentaje = 0;
            bool l_ProcesoAnteriorCompleto = true;
            StringBuilder l_barcode = new StringBuilder( 128 );
            short l_length = 128;
            int l_auxiliarporcentajesellado = 0;
            int l_intentos = 0;
            int l_bolsacorectacontador = 0;

           

            // LeerPorcentaje(out l_ProcesoAnteriorCompleto);
            m_porcentajeAnterior = 100;


            SidLib.ResetError( );

            l_Reply = SidLib.SID_Open( true );
            EscribirBitacora( "OPEN" , l_Reply.ToString( ) );

            char[] l_username = Properties.Settings.Default.NumSerialEquipo.ToCharArray( );

            l_Reply = SidApi.SidLib.SID_Login( l_username );

            Globales.EscribirBitacora( "SELECT ETV" , "SID_LOGIN" , "login SIDserial: " + l_Reply , 3 );



            l_Reply = SidLib.SID_CheckCodeLockCorrect( 30 );




            Error_Sid( l_Reply , out l_Error );
            EscribirBitacora( "CheckCode" , l_Reply.ToString( ) );

            if ( l_Reply == SidLib.SID_OKAY )
            {


              Cambiatexto (  c_paso1, "CODIGO CORRECTO");

                LabelVisible( paso2 , true );
                TextoVisible( c_paso2 , true );
                PictureVisible( c_esperarsellado , true );


                          


                //// REVISAR EL PROCESO DE RECAMBIO \\\\

                if ( !( m_porcentajeAnterior == 100 ) && !l_ProcesoAnteriorCompleto )
                {
                    EscribirBitacora( "EVENTO:::" , "Continuar con Sellado de BOLSA interrumpido" );
                    //El auxiliar es para repetir todo el proceso pero con el porcentaje anterior.
                    l_auxiliarporcentajesellado = m_porcentajeAnterior;
                    m_porcentajeAnterior = 100;
                    l_ProcesoAnteriorCompleto = true;

                }

                if ( m_porcentajeAnterior == 100 && l_ProcesoAnteriorCompleto )
                {

                    EscribirBitacora( "EVENTO:::" , "Empezar Proceso Completo Sellado de BOLSA" );
                    m_porcentajeAnterior = l_auxiliarporcentajesellado;


                    l_Reply = SidLib.SID_JoinBag( m_porcentajeAnterior );
                    Error_Sid( l_Reply , out l_Error );
                    EscribirBitacora( "JoinBAg" , l_Reply.ToString( ) );

                    if ( l_Reply == SidLib.SID_OKAY )
                    {

                       PictureVisible ( c_esperarsellado,false);
                      Cambiatexto (  c_paso2, "... SELLANDO BOLSA ...");
                        try
                        {
                            panelSellado1.Invoke( new Action( ( ) => panelSellado1.Visible = true ) );
                            panelSellado1.Invoke( new Action( ( ) => panelSellado1.Empezar( ) ) );
                        } catch (Exception exc)
                        {
                            Globales.EscribirBitacora( " CambioBolsa" , "PanelSELLADOVisible" , "Exception: " + exc.Message , 3 );
                        }
                   


                        do
                        {

                            l_Reply = SidLib.SID_JoinBagResult( ref l_porcentaje );
                            if ( l_Reply != 0 && l_Reply != 6 )
                                Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "result JoinBAG" , l_Reply.ToString( ) + " :: % " + l_porcentaje );
                          
                        } while ( l_Reply == SidLib.SID_PERIF_BUSY );

                        if ( l_porcentaje != 0 )
                        {
                            EscribirPorcentaje( l_porcentaje );
                            Error_Sid( l_Reply , out l_Error );
                        }

                        if ( l_porcentaje == 100 )
                        {
                            SELLADA = true;

                            Globales.EscribirRetiroPendiente( Globales.NumeroSerieBOLSAanterior );
                        }
                        else 
                            using ( FormaError f_Error = new FormaError( true , "warning" ) )
                        {
                            f_Error.c_MensajeError.Text = l_Error;
                            f_Error.TopMost = true;
                            f_Error.ShowDialog( );
                            //c_BotonCancelar_Click(this, null);
                        }
                    }
                    else
                    {

                        
                            using ( FormaError f_Error = new FormaError( true , "warning" ) )
                        {
                            f_Error.c_MensajeError.Text = l_Error;
                            f_Error.TopMost = true;
                            f_Error.ShowDialog( );
                            //c_BotonCancelar_Click(this, null);
                        }


                        if ( l_Reply < -98 && l_Reply > -108 || l_Reply == SidLib.SID_ERROR_ON_CLOSE_BAG )
                        {

                            int l_causa = l_Reply;
                            using ( FormaAvisoEsperar l_avisoesperar = new FormaAvisoEsperar( ) )
                            {

                                l_avisoesperar.Mensaje( "Espere Analizando Por que no se puede sellar la Bolsa..." );
                                l_avisoesperar.Focus( );
                                l_avisoesperar.TopMost = true;
                                l_avisoesperar.Show( );
                          



                                try
                                {

                                    DirectoryInfo directory = new DirectoryInfo( @".\Webcam" );

                                    FileInfo[] files = directory.GetFiles( "*.*" );



                                    for ( int i = 0; i < files.Length; i++ )
                                    {
                                        File.Delete( files[i].FullName );


                                    }

                                    EscribirBitacora( "Archivos WebCam Borrados" , "Carpeta Limpia" );
                                    directory.Refresh( );

                                }
                                catch ( Exception ex )
                                {
                                    EscribirBitacora( "Delete archivos Webcam" , ex.Message );
                                    Globales.EscribirBitacora( "Cambio Bolsa" , "Delete archivos" , ex.Message , 1 );
                                }

                                l_avisoesperar.Mensaje( "Tomando Nuevas imagenes,... inicializando..." );
                              


                                SidLib.ResetError( );

                                l_Reply = SidLib.SID_Open( false );
                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "OPEN" , l_Error );

                                l_Reply = SidLib.SID_Initialize( );


                                EscribirBitacora( "Inicializando... " , l_Error );

                                if ( l_Reply != 0 )
                                {
                                    string l_Error = "";
                                    Error_Sid( l_Reply , out l_Error );

                                    using ( FormaError f_Error = new FormaError( false , "iniciando" ) )
                                    {
                                        f_Error.c_MensajeError.Text = l_Error;
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                        //c_BotonCancelar_Click(this, null);
                                    }


                                }
                                else
                                {
                                    string l_Error = "";
                                    Error_Sid( l_causa , out l_Error );
                                    EscribirBitacora( "Error en JOIN_BAG" , "ESTAUS: " + l_Reply );

                                    using ( FormaError f_Error = new FormaError( true , "fallo" ) )
                                    {
                                        f_Error.c_MensajeError.Text = " Por favor Intente de nuevo el Procedimiento, Error Previo:  " + l_Error;
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                        //c_BotonCancelar_Click(this, null);
                                    }


                                }



                                l_avisoesperar.Close( );
                            }
                      
                            Exitoso = false;
                            bw_cambiobolsa.CancelAsync();
                        }else
                        {

                            using ( FormaError f_Error = new FormaError( true , "falla" ) )
                            {
                                String l_Error;
                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "ERROR JOINBAG: " , l_Error );
                                f_Error.c_MensajeError.Text = "Problema con el Sellado Error: Error Previo:  " + l_Error;
                                f_Error.TopMost = true;
                                f_Error.c_Imagen.Visible = false;
                                // EscribirProcesoExitoso(0);
                                f_Error.ShowDialog( );
                            }
                            Exitoso = false;
                            bw_cambiobolsa.CancelAsync();
                        }
                        Exitoso = false;
                        return;
                    }
                }

                if ( m_porcentajeAnterior == 100 && !l_ProcesoAnteriorCompleto )
                {
                    EscribirBitacora( "EVENTO:::" , "TERMINAR Retiro el retiro de BOLSA" );
                    EscribirPorcentaje( m_porcentajeAnterior );

                    SELLADA = true;
                }


                try
                {
                    panelSellado1.Invoke( new Action( ( ) => panelSellado1.Detener( ) ) );
                    panelSellado1.Invoke( new Action( ( ) => panelSellado1.Visible = false ) );
        
                }
                catch ( Exception exc )
                {
                    Globales.EscribirBitacora( " CambioBolsa" , "PanelSELLADOVisible OFF" , "Exception: " + exc.Message , 3 );
                }


                l_Reply = SidLib.SID_EnableToOpenDoor( );
                Error_Sid( l_Reply , out l_Error );

                EscribirBitacora( "Enable OpenDoor" , l_Reply.ToString( ) );
                if ( l_Reply == SidLib.SID_OKAY )
                {
                    LabelVisible( paso3 , true );
                    TextoVisible( c_paso3 , true );

                    l_Reply = SidLib.SID_WaitDoorOpen( );
                    Error_Sid( l_Reply , out l_Error );

                    EscribirBitacora( "wait Door Open" , l_Reply.ToString( ) );

                    if ( l_Reply == SidLib.SID_OKAY )
                    {

                       Cambiatexto( c_paso3, "Leer Codigo de Barras Actual: " + Globales.NumeroSerieBOLSA);
                    

                        EscribirBitacora( "Barcode Retirado: " , Globales.NumeroSerieBOLSA );

                        Globales.BolsaCorrecta = true;

                        LabelVisible( paso4 , true );
                        TextoVisible( c_paso4 , true );



                        int minutos = DateTime.Now.Minute + 2;
                        int dif = 0;
                        if ( minutos > 57 )
                            dif = 60;

                      

                        do
                        { //Wait remove the bag 
                            System.Threading.Thread.Sleep( 300 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            EscribirBitacora( "Remove Bag" , sensor[2].ToString( ) );
                            if ( minutos <= DateTime.Now.Minute + dif )
                                break;

                        } while ( sensor[2] != SensorBolsaRemovida ); //127 para version sid.dll > 2.0.0.2 , 63 para version <= 2.0.0.2 SID_CFE
                        EscribirBitacora( "BOLSA REMOVIDA..." , sensor[2].ToString( ) );
                        Error_Sid( l_Reply , out l_Error );
                        // l_Reply = SidLib.SID_WaitRemoveBag(60); /* Wait the remove of the bag*/

                        PictureVisible( c_esperarInicio , false );
                        LabelVisible( paso5 , true );
                        TextoVisible( c_paso5 , true );

                        int l_trys = 0;
                        do
                        {
                            SidLib.SID_Open( false );

                            l_length = 128;
                            if ( l_trys > 0 )
                                using ( FormaError f_Error = new FormaError( false , "warning" ) )
                                {
                                    f_Error.c_MensajeError.Text = "Aparentemente El Folio de BOLSA ya se USO, use Otra BOLSA con folio Distinto... ";
                                    f_Error.TopMost = true;
                                    f_Error.ShowDialog( );
                                }

                       

                            l_Reply = SidLib.SID_ReadBarcode( l_barcode , ref l_length , 60 );
                            EscribirBitacora( "Funcion Barcode: " , l_Reply.ToString( ) );

                            if ( l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED || l_Reply == SidLib.SID_DATATRUNC )
                            {
                                try
                                {
                                    string l_barcodelimpio = l_barcode.ToString( ).Substring( 0 , l_barcode.ToString( ).Length - 5 ).Replace( "," , "" );
                                    EscribirNumeroDeBOLSA( l_barcodelimpio );
                                    Globales.NumeroSerieBOLSA = l_barcodelimpio;
                                    EscribirBitacora( "Barcode Nuevo: " , l_barcodelimpio );
                                    CambiatextoEtiqueta( c_barcodenuevo , l_barcodelimpio);
                                   LabelVisible(c_barcodenuevo, true );
                                }
                                catch ( Exception l_error )
                                {
                                    EscribirBitacora( "ERROR de LECTURA " , l_error.Message );
                                    if ( String.IsNullOrEmpty( l_barcode.ToString( ) ) )
                                    {
                                        string l_barcode2 = l_barcode.ToString( ).Replace( "," , "" );
                                        EscribirNumeroDeBOLSA( l_barcode2 );
                                        Globales.NumeroSerieBOLSA = l_barcode2;
                                        EscribirBitacora( "Barcode Nuevo: " , l_barcode2 );
                                        CambiatextoEtiqueta( c_barcodenuevo,l_barcode2);
                                        LabelVisible( c_barcodenuevo, true);
                                    }
                                    else
                                    {
                                        EscribirNumeroDeBOLSA( "000000" );
                                        Globales.NumeroSerieBOLSA = "000000";
                                        CambiatextoEtiqueta(c_barcodenuevo , "NO Capturado");
                                        LabelVisible( c_barcodenuevo,true);
                                    }

                                }

                            }
                            l_trys++;
                            SidLib.SID_Close( );
                        } while ( !ComprobacionEnvaseNuevo( Globales.NumeroSerieBOLSA ) && l_trys < 3 );

                        EscribirBitacora( "Intentos de Lectura Barcode:  " , l_trys.ToString( ) );
                        if ( l_trys >= 3 && !ComprobacionEnvaseNuevo( Globales.NumeroSerieBOLSA ) )
                        {
                            EscribirNumeroDeBOLSA( "000000" );
                            Globales.NumeroSerieBOLSA = "000000";
                            CambiatextoEtiqueta( c_barcodenuevo,"NO Capturado");
                            LabelVisible( c_barcodenuevo , true );

                            DialogResult l_respuesta = DialogResult.Abort;

                            do
                            {
                                if ( l_respuesta == DialogResult.Cancel )
                                    using ( FormaError f_Error = new FormaError( false , "warning" ) )
                                    {
                                        f_Error.c_MensajeError.Text = "Necesitamos un Numero de Envase VALIDO para Continuar";
                                        f_Error.TopMost = true;
                                        f_Error.ShowDialog( );
                                    }

                               
                                using ( FormaCambioNumeroBolsa f_cambiobolsa = new FormaCambioNumeroBolsa( ) )
                                {
                                    f_cambiobolsa.TopMost = true;
                                    l_respuesta = f_cambiobolsa.ShowDialog( );

                                }

                            } while ( l_respuesta != DialogResult.OK );
                        }

                        CambiatextoEtiqueta ( c_barcodenuevo,Globales.NumeroSerieBOLSA);
                        EscribirBitacora( "Cambio de Bolsa: " , Globales.NumeroSerieBOLSA );
                       Cambiatexto( c_paso5, "PONER BOLSA NUEVA");
                    
                        //  l_Reply = SidLib.SID_WaitReplaceBag(60); /* Wait the replacement of the bag*/
                        SidLib.SID_Open( false );
                        Error_Sid( l_Reply , out l_Error );
                        do
                        { //Wait a new bag insertion 
                            System.Threading.Thread.Sleep( 300 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            EscribirBitacora( "Bag insertion" , sensor[7].ToString( ) );
                        } while ( ( sensor[7] & 0x02 ) == 2 );
                        Error_Sid( l_Reply , out l_Error );
                        EscribirBitacora( "Bag insertion" , sensor[7].ToString( ) );
                        EscribirBitacora( "Bag COrrcet ??" , sensor[7].ToString( ) );
                        Cambiatexto( c_paso5 , "VERIFICANDO BOLSA, Espere...");
                    
                        //VERIFICAR BOLSA PUESTA

                      do
                        {
                            System.Threading.Thread.Sleep( 100 );
                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                            l_bolsacorectacontador++;

                        } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );

                        Error_Sid( l_Reply , out l_Error );

                        EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                        EscribirBitacora( "Ciclo completo: " , l_bolsacorectacontador.ToString( ) );

                        if ( l_bolsacorectacontador < 101 )
                        {
                            using ( FormaError f_Error = new FormaError( true , "revisar" ) )
                            {
                                f_Error.c_MensajeError.Text = "Por favor verifique que la  BOLSA este bien puesta, solo tendra una oportunidad mas.";
                                f_Error.c_Imagen.Visible = true;
                                f_Error.TopMost = true;
                                f_Error.ShowDialog( );
                            }

                            EscribirBitacora( "Bolsa Mal Puesta " , "Reintentar  " );

                            Cambiatexto( c_paso5 , "Re - VERIFICANDO BOLSA, Espere..." );
                            PictureVisible( c_esperarInicio , true );

                            l_bolsacorectacontador = 0;
                            do
                            {
                                System.Threading.Thread.Sleep( 100 );
                                l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                                l_bolsacorectacontador++;
                            } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );
                            Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                            EscribirBitacora( "SEGUNDO Ciclo completo: " , l_bolsacorectacontador.ToString( ) );
                            //Acciones a tomar
                            if ( l_bolsacorectacontador < 101 )
                            {
                                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                                {
                                    f_Error.c_MensajeError.Text = "Esta es la ultima Oportunidad, Verifique  BOLSA este bien puesta, en caso contrario, usted obtendra su Ticket pero se mandara una alerta de Servicio para este Equipo";
                                    f_Error.c_Imagen.Visible = true;
                                    f_Error.TopMost = true;
                                    f_Error.ShowDialog( );
                                }

                                EscribirBitacora( "Bolsa Mal Puesta " , "Ultimo Reintento  " );
                                Cambiatexto( c_paso5 , "Re - VERIFICANDO BOLSA, Espere...");
                                PictureVisible( c_esperarInicio , true );

                                l_bolsacorectacontador = 0;
                                do
                                {
                                    System.Threading.Thread.Sleep( 100 );
                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );

                                    l_bolsacorectacontador++;
                                } while ( ( sensor[7] & 0x02 ) != 2 && l_bolsacorectacontador < 101 );

                                Error_Sid( l_Reply , out l_Error );
                                EscribirBitacora( "Bag STATUS" , sensor[7].ToString( ) );
                                EscribirBitacora( "SEGUNDO Ciclo completo: " , l_bolsacorectacontador.ToString( ) );
                                //Acciones a tomar
                                //Enviar ALerta
                                //continuar Proceso...

                            }
                            else
                            {
                                //Entregar Ticket
                                PictureVisible( c_ok , true );

                                Cambiatexto( c_paso5 , "BOLSA OK");
                                
                            }


                        }
                        else
                        {
                            PictureVisible( c_ok , true );
                            PictureVisible( c_esperarInicio  , false );

                            Cambiatexto( c_paso5 , "BOLSA OK");
                           
                        }

                        LabelVisible( paso6 , true );
                        TextoVisible( c_paso6 , true );
                        PictureVisible( c_esperarInicio , true);


                        l_Reply = SidLib.SID_WaitDoorClosed( 120 ); /*Wait closing the door */
                        Error_Sid( l_Reply , out l_Error );
                        EscribirBitacora( "wait Door CLOSE" , l_Reply.ToString( ) );

                        if ( l_Reply == SidLib.SID_TIME_OUT_EXPIRED )
                        {
                            using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                            {
                                f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta,solo tendra una oportunidad mas.";
                                f_Error.TopMost = true;
                                f_Error.c_Imagen.Visible = false;
                                f_Error.ShowDialog( );
                            }
                           
                            l_Reply = SidLib.SID_WaitDoorClosed( 30 ); /*Wait closing the door */

                            EscribirBitacora( "2do wait Door CLOSE" , l_Reply.ToString( ) );
                        }


                        if ( l_Reply == SidLib.SID_OKAY ) ///check corect instlacion
                        {
                            Cambiatexto( c_paso6 , "Espere inicializando Modulo");
                        

                            l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                            Error_Sid( l_Reply , out l_Error );
                            EscribirBitacora( "STATUS : " , l_Reply.ToString( ) );
                            EscribirBitacora( "SENSOR[2] : " , sensor[2].ToString( ) );
                            EscribirBitacora( "SENSOR [7] : " , sensor[7].ToString( ) + ( sensor[7] & 0x02 ).ToString( ) );
                            if ( ( ( sensor[7] & 0x02 ) == 2 ) )
                            {
                                do
                                {
                                    MessageBox.Show( "Revise la colocación de la bolsa" );

                                    MessageBox.Show( "Introduzca la clave" );
                                    l_Reply = SidLib.SID_CheckCodeLockCorrect( 30 );
                                    Error_Sid( l_Reply , out l_Error );
                                    if ( l_Reply == SidLib.SID_OKAY )
                                    {
                                        MessageBox.Show( "Abra la Boveda, revise la bolsa y cierre la puerta" );
                                        l_Reply = SidLib.SID_EnableToOpenDoor( );
                                        Error_Sid( l_Reply , out l_Error );
                                        if ( l_Reply == SidLib.SID_OKAY )
                                        {
                                            l_Reply = SidLib.SID_WaitDoorOpen( );
                                            Error_Sid( l_Reply , out l_Error );
                                        }
                                        else
                                        {
                                            l_Reply = SidLib.SID_ForceOpenDoor( );
                                            Error_Sid( l_Reply , out l_Error );
                                        }


                                        if ( l_Reply == SidLib.SID_OKAY )
                                        {
                                            l_Reply = SidLib.SID_WaitReplaceBag( 60 ); /* Wait the replacement of the bag*/
                                            Error_Sid( l_Reply , out l_Error );

                                            l_Reply = SidLib.SID_WaitDoorClosed( 60 ); /*Wait closing the door */
                                            Error_Sid( l_Reply , out l_Error );

                                        }
                                    }

                                    l_Reply = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                                    Error_Sid( l_Reply , out l_Error );
                                    EscribirBitacora( "STATUS : " , l_Reply.ToString( ) );
                                    EscribirBitacora( "SENSOR[2] : " , sensor[2].ToString( ) );
                                    EscribirBitacora( "SENSOR [7] : " , sensor[7].ToString( ) );

                                } while ( ( ( sensor[7] & 0x02 ) == 2 ) );

                                Cambiatexto( c_paso6 , "Terminado --> Presione Salir");
                              
                                EscribirProcesoExitoso( 1 );
                                Exitoso = true;
                              


                            }
                            else
                            {
                                Cambiatexto( c_paso6 , "Terminado --> Presione Salir");
                               
                                EscribirProcesoExitoso( 1 );
                                Exitoso = true;
                              

                            }
                        }
                        else
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                            using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                            {
                                f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta, Problablemente se necesite un Master RESET antes de volver a SELLAR";
                                f_Error.c_Imagen.Visible = false;
                                f_Error.TopMost = true;
                                EscribirProcesoExitoso( 0 );
                                f_Error.ShowDialog( );
                            }
                            Exitoso = true;
                           l_resultado = DialogResult.OK;
                            Registrar.Alerta( "MAL Procedimineto de SELLADO, Problablemente No se Verifico la Colocacion de Bolsa o tiempo agotado para cerrar." );
                           // c_Salir.Enabled = true;
                            bw_cambiobolsa.CancelAsync();
                        }

                    }
                    else
                    {
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                        using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                        {
                            EscribirBitacora("Wait_door_open()","Tiempo Agotado para Abrir Puerta repita Operación" );
                            f_Error.c_MensajeError.Text = "Tiempo Agotado para Abrir Puerta repita Operación";
                            f_Error.c_Imagen.Visible = false;
                            f_Error.TopMost = true;
                            EscribirProcesoExitoso( 0 );
                            f_Error.ShowDialog( );
                        }
                        Exitoso = false;
                        bw_cambiobolsa.CancelAsync();
                    }
                }
                else
                {
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    using ( FormaError f_Error = new FormaError( true , "llave" ) )
                    {
                        EscribirBitacora( "CheckcodeLoocK()" , "Mensaje de Apertura Rechazado" );
                        f_Error.c_MensajeError.Text = "Mensaje de Apertura Rechazado: Error: " + l_Reply;
                        f_Error.c_Imagen.Visible = false;
                        f_Error.TopMost = true;
                        EscribirProcesoExitoso( 0 );
                        f_Error.ShowDialog( );
                    }
                    Exitoso = false;
                    bw_cambiobolsa.CancelAsync();
                }


                l_Reply = SidApi.SidLib.SID_Logout( );
                Globales.EscribirBitacora( "SELECT ETV" , "SID_LOgout" , "logout: " + l_Reply , 3 );

                l_Reply = SidLib.SID_Close( ); /*Disconnect from unit */

            }
            else
            {
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                using ( FormaError f_Error = new FormaError( true , "reloj" ) )
                {

                    f_Error.c_MensajeError.Text = "Codigo incorecto o se agoto el tiempo de espera";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.ShowDialog( );
                }
                Exitoso = false;
                bw_cambiobolsa.CancelAsync();
            }
        }

        private void bw_cambiobolsa_RunWorkerCompleted( object sender , RunWorkerCompletedEventArgs e )
        {

            if ( e.Cancelled  )
            {
                EscribirBitacora( "EVENTO:::" , "Cancelando Cambio de Bolsa" );
                this.DialogResult = DialogResult.Abort;
                Close( );
            }
            else
            {

                c_Salir.Enabled = true;




                if ( Exitoso  || SELLADA )
                    Close( );

                this.DialogResult = l_resultado;
            }


        }
    } 
}
