﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{

    public partial class FormaConsultaEfectivo : Azteca_SID.FormBase
    {
        Double l_totalSaco = 0;

        Double l_TotalBolsa = 0;

        Double l_TotalMoneadas_B1;
        Double l_TotalMoneadas_B2;
        public FormaConsultaEfectivo( )
        {
            InitializeComponent( );
        }

        private void FormaConsultaEfectivo_Load( object sender , EventArgs e )
        {
            Cursor.Show( );
            Cursor.Show( );
            Cursor.Current = Cursors.WaitCursor;
            DataTable l_Datos = BDConsulta.ObtenerConsultaDepositos( 1 );
            DataTable l_DatosMonedas = BDConsulta.ObtenerConsultaDepositos( 0 );
            int l_totalbilletes = 0;
            int l_totalmonedas_b2 = 0;
            int l_totalmonedas_b1 = 0;

            if ( l_Datos.Rows.Count < 1 )
            {
                using ( FormaError f_Error = new FormaError( true , "dinero" ) )
                {
                    f_Error.c_MensajeError.Text = "No hay depósitos en pesos";
                    f_Error.ShowDialog( );
                    Close( );
                }
            }
            foreach ( DataRow l_Detalle in l_Datos.Rows )
            {
                if ( l_Detalle[1].ToString( ).Trim( ) == "1000" )
                {

                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_1000.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 1000;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "500" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_500.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 500;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "200" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_200.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 200;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "100" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_100.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 100;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "50" )
                {

                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_50.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 50;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "20" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_20.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalBolsa += (int) l_Detalle[0] * 20;
                        l_totalbilletes += (int) l_Detalle[0];
                    }

                }

            }

            c_numbilletes.Text = l_totalbilletes.ToString( );
            c_TotalCajon2.Text = l_TotalBolsa.ToString( "#,###,###,##0.00" );
            Cursor.Hide( );

            //DEPOSITO MONEDAS

            foreach ( DataRow l_Detalle in l_DatosMonedas.Rows )
            {
                if ( l_Detalle[1].ToString( ).Trim( ) == "20.0" )
                {

                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_20.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 20;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_20.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 20;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "10.0" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_10.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 10;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_10.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 10;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "5.00" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_5.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 5;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_5.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 5;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "2.00" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_2.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 2;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_2.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 2;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "1.00" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa_1.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 1;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_1.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 1;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "0.50" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_50c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 0.5;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_50c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 0.5;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }
                }

                if ( l_Detalle[1].ToString( ).Trim( ) == "0.20" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_20c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 0.2;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_20c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 0.2;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }
                }

                if ( l_Detalle[1].ToString( ).Trim( ) == "0.10" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_bolsa1_10c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B1 += (int) l_Detalle[0] * 0.1;
                        l_totalmonedas_b1 += (int) l_Detalle[0];
                    }
                    else
                    {
                        c_bolsa2_10c.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        l_TotalMoneadas_B2 += (int) l_Detalle[0] * 0.1;
                        l_totalmonedas_b2 += (int) l_Detalle[0];
                    }
                }
            }

            c_totalMonedas_b1.Text = l_totalmonedas_b1.ToString( );
            c_totalMonedas_b2.Text = l_totalmonedas_b2.ToString( );
            c_totalbolsa1.Text = l_TotalMoneadas_B1.ToString( "$#,###,###,##0.00" );
            c_totalbolsa2.Text = l_TotalMoneadas_B2.ToString( "$#,###,###,##0.00" );
            Cursor.Hide( );

        }

        private void c_Imprimir_Click( object sender , EventArgs e )
        {
            c_Imprimir.Enabled = false;

            if ( l_TotalBolsa > 0 )
            {
                ModulePrint imprimir = new ModulePrint( );

                imprimir.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );


                imprimir.AddHeaderLine( "Consulta de Efectivo" );
                imprimir.AddHeaderLine( "Moneda: Pesos" );
                imprimir.AddHeaderLine( "Contenido en BOVEDAD hasta el momento" );
                imprimir.AddHeaderLine( "" );
                imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
                imprimir.AddSubHeaderLine( "Fecha: " + DateTime.Now.ToString( "dd/M/yyyy" ) + " Hora: " + DateTime.Now.ToLongTimeString( ) );
                imprimir.AddSubHeaderLine( Globales.NombreCliente );

                imprimir.AddItem( c_Cajon2_1000.Text , "$1,000.00" , "1000" );
                imprimir.AddItem( c_Cajon2_500.Text , "$500.00" , "500" );
                imprimir.AddItem( c_Cajon2_200.Text , "$200.00" , "200" );
                imprimir.AddItem( c_Cajon2_100.Text , "$100.00" , "100" );
                imprimir.AddItem( c_Cajon2_50.Text , "$50.00" , "50" );
                imprimir.AddItem( c_Cajon2_20.Text , "$20.00" , "20" );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Total: " + c_TotalCajon2.Text );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Numero de Billetes: " + c_numbilletes.Text );

                try
                {

                    imprimir.FooterImage = Image.FromFile( Properties.Settings.Default.FooterImageTicket );

                }
                catch ( Exception exx )
                {

                }

                
                imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
            }

            c_Imprimir.Enabled = true;
            Close( );
        }


        private void c_Cerrar_Click( object sender , EventArgs e )
        {
            Close( );
        }

        private void c_cerrarM_Click( object sender , EventArgs e )
        {
            Close( );
        }

        private void c_imprimirMonedas_Click( object sender , EventArgs e )
        {
            if ( l_TotalMoneadas_B1 > 0 )
            {
                ModulePrint imprimir = new ModulePrint( );

                imprimir.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );

                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( "Consulta de Efectivo MONEDAS" );
                imprimir.AddHeaderLine( "Moneda: Pesos" );
                imprimir.AddHeaderLine( "Contenido en BOVEDAD hasta el momento" );
                imprimir.AddHeaderLine( "" );
                imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
                //  imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                //  imprimir.AddSubHeaderLine(Properties.Settings.Default.Cliente);

            //    imprimir.AddItem( c_bolsa1_20.Text , "$20.00" , "20" );
                imprimir.AddItem( c_bolsa1_10.Text , "$10.00" , "10" );
                imprimir.AddItem( c_bolsa1_5.Text , "$5.00" , "5" );
                imprimir.AddItem( c_bolsa1_2.Text , "$2.00" , "2" );
                imprimir.AddItem( c_bolsa_1.Text , "$1.00" , "1" );
                imprimir.AddItem( c_bolsa1_50c.Text , "$0.50" , "0.5" );
                //imprimir.AddItem( c_bolsa1_20c.Text , "$0.20" , "0.2" );
                //imprimir.AddItem( c_bolsa1_10c.Text , "$0.10" , "0.1" );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Total BOLSA NUMERO 1: " + c_totalbolsa1.Text );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Numero de MONEDAS: " + c_totalMonedas_b1.Text );



                imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
            }


            if ( l_TotalMoneadas_B2 > 0 )
            {
                ModulePrint imprimir = new ModulePrint( );

                imprimir.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );

                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( " " );
                imprimir.AddHeaderLine( "Consulta de Efectivo MONEDAS" );
                imprimir.AddHeaderLine( "Moneda: Pesos" );
                imprimir.AddHeaderLine( "Contenido en BOVEDAD hasta el momento" );
                imprimir.AddHeaderLine( "" );
                imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
                // imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                //imprimir.AddSubHeaderLine(Properties.Settings.Default.Cliente);

             //   imprimir.AddItem( c_bolsa2_20.Text , "$20.00" , "20" );
                imprimir.AddItem( c_bolsa2_10.Text , "$10.00" , "10" );
                imprimir.AddItem( c_bolsa2_5.Text , "$5.00" , "5" );
                imprimir.AddItem( c_bolsa2_2.Text , "$2.00" , "2" );
                imprimir.AddItem( c_bolsa2_1.Text , "$1.00" , "1" );
                imprimir.AddItem( c_bolsa2_50c.Text , "$0.50" , "0.5" );
                //imprimir.AddItem( c_bolsa2_20c.Text , "$0.20" , "0.2" );
                //imprimir.AddItem( c_bolsa2_10c.Text , "$0.10" , "0.1" );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Total en BOLSA NUMERO 2: " + c_totalbolsa2.Text );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Numero de MONEDAS: " + c_totalMonedas_b2.Text );



                imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
            }

        }

        private void c_mov_Click_1( object sender , EventArgs e )
        {
            c_mov.Enabled = false;
            DataTable l_Movimientos;
            DataTable l_MovimientosM;
            DataTable l_Movimientos_Manuales;
            Double l_total = 0;
            Double l_totalM = 0;
            Double l_totalManual = 0;

            l_Movimientos = BDConsulta.ObtenerConsultaOperaciones_mov( );
            l_MovimientosM = BDConsulta.ObtenerConsultaOperaciones_mov_M( );
            l_Movimientos_Manuales = BDConsulta.ObtenerConsultaOperaciones_mov_manual( );



            if ( l_Movimientos.Rows.Count > 0 )
            {

                ModulePrint imprimir = new ModulePrint( );

                imprimir.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );


                imprimir.AddHeaderLine( "Consulta de MOVIMIENTOS" );
                imprimir.AddHeaderLine( "Moneda: Pesos" );
                imprimir.AddHeaderLine( "Operaciones registradas hasta el momento" );
                imprimir.AddHeaderLine( "Caja de Deposito:         " + Properties.Settings.Default.NumSerialEquipo );
                imprimir.AddHeaderLine( "" );
                imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
                //imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                //imprimir.AddSubHeaderLine(Globales.NombreCliente);

                foreach ( DataRow l_Detalle in l_Movimientos.Rows )
                {
                    Double l_monto = Double.Parse( l_Detalle[2].ToString( ) );
                    imprimir.AddItemUsuario( l_Detalle[1].ToString( ) , l_monto.ToString( "$#,###,###,##0.00" ) , l_Detalle[0].ToString( ) );
                    l_total += l_monto;

                }

                foreach ( DataRow l_Detalle in l_MovimientosM.Rows )
                {
                    Double l_monto = Double.Parse( l_Detalle[2].ToString( ) );
                    imprimir.AddItemMonedas( l_Detalle[1].ToString( ) , l_monto.ToString( "$#,###,###,##0.00" ) , l_Detalle[0].ToString( ) );
                    l_totalM += l_monto;

                }

                foreach ( DataRow l_Detalle in l_Movimientos_Manuales.Rows )
                {
                    Double l_monto = Double.Parse( l_Detalle[2].ToString( ) );
                    imprimir.AddItemManual( l_Detalle[1].ToString( ) , l_monto.ToString( "$#,###,###,##0.00" ) , l_Detalle[0].ToString( ) );
                    l_totalManual += l_monto;

                }

                imprimir.AddFooterLine( "Numero de Depositantes: " + l_Movimientos.Rows.Count.ToString( ) );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Total: " + l_total.ToString( "$#,###,###,##0.00" ) );
                imprimir.AddFooterLine( "Total Monedas: " + l_totalM.ToString( "$#,###,###,##0.00" ) );
                imprimir.AddFooterLine( "Total Salvo Buen Cobro: " + l_totalManual.ToString( "$#,###,###,##0.00" ) );

                imprimir.AddFooterLine( "" );

                try
                {

                    imprimir.FooterImage = Image.FromFile( Properties.Settings.Default.FooterImageTicket );

                }
                catch ( Exception exx )
                {

                }

                imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
            }
            c_mov.Enabled = true;
            Close( );
        }

        private void c_Cerrar_Click_1( object sender , EventArgs e )
        {
            Close( );
        }

    }
}
