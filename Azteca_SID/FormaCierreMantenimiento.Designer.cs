﻿namespace Azteca_SID
{
    partial class FormaCierreMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_nombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.c_folioMante = new System.Windows.Forms.ComboBox();
            this.c_tipoMant = new System.Windows.Forms.TextBox();
            this.c_vendor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.c_comentarios = new System.Windows.Forms.TextBox();
            this.c_cerrarMantenimento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c_nombre
            // 
            this.c_nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_nombre.Enabled = false;
            this.c_nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nombre.Location = new System.Drawing.Point(295, 185);
            this.c_nombre.Name = "c_nombre";
            this.c_nombre.Size = new System.Drawing.Size(418, 29);
            this.c_nombre.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(78, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 24);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nombre del Tecnico";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(246, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 29);
            this.label1.TabIndex = 9;
            this.label1.Text = "Cierre  de Mantenimiento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(85, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 24);
            this.label4.TabIndex = 13;
            this.label4.Text = "Tipo Mantenimiento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(95, 223);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "Empresa Asociada";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(60, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(220, 24);
            this.label5.TabIndex = 14;
            this.label5.Text = "Mantenimiento Abierto";
            // 
            // c_folioMante
            // 
            this.c_folioMante.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_folioMante.FormattingEnabled = true;
            this.c_folioMante.Location = new System.Drawing.Point(295, 131);
            this.c_folioMante.Name = "c_folioMante";
            this.c_folioMante.Size = new System.Drawing.Size(133, 32);
            this.c_folioMante.TabIndex = 15;
            this.c_folioMante.SelectedIndexChanged += new System.EventHandler(this.c_folioMante_SelectedIndexChanged);
            // 
            // c_tipoMant
            // 
            this.c_tipoMant.BackColor = System.Drawing.SystemColors.Info;
            this.c_tipoMant.Enabled = false;
            this.c_tipoMant.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_tipoMant.Location = new System.Drawing.Point(295, 255);
            this.c_tipoMant.Name = "c_tipoMant";
            this.c_tipoMant.Size = new System.Drawing.Size(418, 29);
            this.c_tipoMant.TabIndex = 16;
            // 
            // c_vendor
            // 
            this.c_vendor.BackColor = System.Drawing.SystemColors.Info;
            this.c_vendor.Enabled = false;
            this.c_vendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_vendor.Location = new System.Drawing.Point(295, 220);
            this.c_vendor.Name = "c_vendor";
            this.c_vendor.Size = new System.Drawing.Size(418, 29);
            this.c_vendor.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(51, 306);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(219, 24);
            this.label6.TabIndex = 18;
            this.label6.Text = "Comentarios / Detalles";
            // 
            // c_comentarios
            // 
            this.c_comentarios.BackColor = System.Drawing.SystemColors.Info;
            this.c_comentarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_comentarios.Location = new System.Drawing.Point(295, 303);
            this.c_comentarios.Multiline = true;
            this.c_comentarios.Name = "c_comentarios";
            this.c_comentarios.Size = new System.Drawing.Size(418, 97);
            this.c_comentarios.TabIndex = 19;
            this.c_comentarios.Click += new System.EventHandler(this.c_comentarios_Click);
            // 
            // c_cerrarMantenimento
            // 
            this.c_cerrarMantenimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cerrarMantenimento.Location = new System.Drawing.Point(288, 432);
            this.c_cerrarMantenimento.Name = "c_cerrarMantenimento";
            this.c_cerrarMantenimento.Size = new System.Drawing.Size(225, 65);
            this.c_cerrarMantenimento.TabIndex = 20;
            this.c_cerrarMantenimento.Text = "Cerrar Mantenimiento";
            this.c_cerrarMantenimento.UseVisualStyleBackColor = true;
            this.c_cerrarMantenimento.Click += new System.EventHandler(this.c_cerrarMantenimento_Click);
            // 
            // FormaCierreMantenimiento
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_cerrarMantenimento);
            this.Controls.Add(this.c_comentarios);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_vendor);
            this.Controls.Add(this.c_tipoMant);
            this.Controls.Add(this.c_folioMante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_nombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "FormaCierreMantenimiento";
            this.Load += new System.EventHandler(this.FormaCierreMantenimiento_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.c_nombre, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.c_folioMante, 0);
            this.Controls.SetChildIndex(this.c_tipoMant, 0);
            this.Controls.SetChildIndex(this.c_vendor, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.c_comentarios, 0);
            this.Controls.SetChildIndex(this.c_cerrarMantenimento, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_nombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox c_folioMante;
        private System.Windows.Forms.TextBox c_tipoMant;
        private System.Windows.Forms.TextBox c_vendor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_comentarios;
        private System.Windows.Forms.Button c_cerrarMantenimento;
    }
}
