﻿namespace Azteca_SID
{
    partial class FormCambiarContraseña
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Cambiar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.c_confirmacionContraseña = new System.Windows.Forms.TextBox();
            this.c_nuevacontraseña = new System.Windows.Forms.TextBox();
            this.c_contraseña = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(135, 305);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(232, 20);
            this.label3.TabIndex = 54;
            this.label3.Text = "Confirme nueva Contraseña";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(217, 259);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 20);
            this.label2.TabIndex = 53;
            this.label2.Text = "Nueva contraseña";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(213, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 20);
            this.label1.TabIndex = 52;
            this.label1.Text = "Contraseña Actual";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(287, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(256, 20);
            this.label5.TabIndex = 51;
            this.label5.Text = "Contraseña ADMINISTRADOR";
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(460, 410);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(155, 56);
            this.c_Cancelar.TabIndex = 50;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Cambiar
            // 
            this.c_Cambiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Cambiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cambiar.Location = new System.Drawing.Point(185, 410);
            this.c_Cambiar.Name = "c_Cambiar";
            this.c_Cambiar.Size = new System.Drawing.Size(163, 56);
            this.c_Cambiar.TabIndex = 49;
            this.c_Cambiar.Text = "Cambiar";
            this.c_Cambiar.UseVisualStyleBackColor = true;
            this.c_Cambiar.Click += new System.EventHandler(this.c_Cambiar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(231, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(338, 29);
            this.label4.TabIndex = 48;
            this.label4.Text = "CAMBIO DE CONTRASEÑA";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // c_confirmacionContraseña
            // 
            this.c_confirmacionContraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_confirmacionContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmacionContraseña.Location = new System.Drawing.Point(381, 299);
            this.c_confirmacionContraseña.Name = "c_confirmacionContraseña";
            this.c_confirmacionContraseña.PasswordChar = '*';
            this.c_confirmacionContraseña.Size = new System.Drawing.Size(188, 26);
            this.c_confirmacionContraseña.TabIndex = 47;
            this.c_confirmacionContraseña.Click += new System.EventHandler(this.c_confirmacionContraseña_Click);
            // 
            // c_nuevacontraseña
            // 
            this.c_nuevacontraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_nuevacontraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nuevacontraseña.Location = new System.Drawing.Point(381, 259);
            this.c_nuevacontraseña.Name = "c_nuevacontraseña";
            this.c_nuevacontraseña.PasswordChar = '*';
            this.c_nuevacontraseña.Size = new System.Drawing.Size(188, 26);
            this.c_nuevacontraseña.TabIndex = 46;
            this.c_nuevacontraseña.Click += new System.EventHandler(this.c_nuevacontraseña_Click);
            // 
            // c_contraseña
            // 
            this.c_contraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_contraseña.Location = new System.Drawing.Point(381, 219);
            this.c_contraseña.Name = "c_contraseña";
            this.c_contraseña.PasswordChar = '*';
            this.c_contraseña.Size = new System.Drawing.Size(188, 26);
            this.c_contraseña.TabIndex = 45;
            this.c_contraseña.Click += new System.EventHandler(this.c_contraseña_Click);
            // 
            // FormCambiarContraseña
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Cambiar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.c_confirmacionContraseña);
            this.Controls.Add(this.c_nuevacontraseña);
            this.Controls.Add(this.c_contraseña);
            this.Name = "FormCambiarContraseña";
            this.Load += new System.EventHandler(this.FormCambiarContraseña_Load);
            this.Controls.SetChildIndex(this.c_contraseña, 0);
            this.Controls.SetChildIndex(this.c_nuevacontraseña, 0);
            this.Controls.SetChildIndex(this.c_confirmacionContraseña, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.c_Cambiar, 0);
            this.Controls.SetChildIndex(this.c_Cancelar, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Cambiar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_confirmacionContraseña;
        private System.Windows.Forms.TextBox c_nuevacontraseña;
        private System.Windows.Forms.TextBox c_contraseña;
    }
}
