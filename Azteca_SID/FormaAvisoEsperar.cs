﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaAvisoEsperar : Form
    {
        public FormaAvisoEsperar()
        {
            InitializeComponent();
            c_redibujado.Enabled = true;
        }

        private void FormaAvisoEsperar_Load(object sender, EventArgs e)
        {
            c_redibujado.Start();

        }

        public void Forzardibujado()
        {
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();
        }

        private void c_redibujado_Tick(object sender, EventArgs e)
        {
            c_redibujado.Stop();


            Forzardibujado();
            c_redibujado.Start(); ;
        }

        public void Mensaje(String p_mensaje)
        {
            c_mensaje.Text = p_mensaje;
            Forzardibujado();

        }

        private void FormaAvisoEsperar_FormClosing(object sender, FormClosingEventArgs e)
        {
            c_redibujado.Enabled = false;
        }
    }
}
