﻿namespace Azteca_SID
{
    partial class FormaCambioBolsa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_ok = new System.Windows.Forms.PictureBox();
            this.c_barcode = new System.Windows.Forms.Label();
            this.c_barcodenuevo = new System.Windows.Forms.Label();
            this.c_Salir = new System.Windows.Forms.Button();
            this.c_empezar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.c_esperarInicio = new System.Windows.Forms.PictureBox();
            this.paso3 = new System.Windows.Forms.Label();
            this.c_paso3 = new System.Windows.Forms.TextBox();
            this.paso6 = new System.Windows.Forms.Label();
            this.c_paso6 = new System.Windows.Forms.TextBox();
            this.paso2 = new System.Windows.Forms.Label();
            this.paso4 = new System.Windows.Forms.Label();
            this.paso5 = new System.Windows.Forms.Label();
            this.paso1 = new System.Windows.Forms.Label();
            this.c_paso5 = new System.Windows.Forms.TextBox();
            this.c_paso4 = new System.Windows.Forms.TextBox();
            this.c_paso2 = new System.Windows.Forms.TextBox();
            this.c_paso1 = new System.Windows.Forms.TextBox();
            this.c_esperarsellado = new System.Windows.Forms.PictureBox();
            this.panelSellado1 = new Azteca_SID.PanelSellado();
            this.bw_cambiobolsa = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.c_ok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarsellado)).BeginInit();
            this.SuspendLayout();
            // 
            // c_ok
            // 
            this.c_ok.BackColor = System.Drawing.Color.Transparent;
            this.c_ok.Image = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.c_ok.Location = new System.Drawing.Point(6, 400);
            this.c_ok.Name = "c_ok";
            this.c_ok.Size = new System.Drawing.Size(108, 47);
            this.c_ok.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_ok.TabIndex = 42;
            this.c_ok.TabStop = false;
            this.c_ok.Visible = false;
            // 
            // c_barcode
            // 
            this.c_barcode.AutoSize = true;
            this.c_barcode.BackColor = System.Drawing.Color.Transparent;
            this.c_barcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_barcode.Location = new System.Drawing.Point(641, 265);
            this.c_barcode.Name = "c_barcode";
            this.c_barcode.Size = new System.Drawing.Size(76, 20);
            this.c_barcode.TabIndex = 41;
            this.c_barcode.Text = "Barcode";
            this.c_barcode.Visible = false;
            // 
            // c_barcodenuevo
            // 
            this.c_barcodenuevo.AutoSize = true;
            this.c_barcodenuevo.BackColor = System.Drawing.Color.Transparent;
            this.c_barcodenuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_barcodenuevo.Location = new System.Drawing.Point(641, 386);
            this.c_barcodenuevo.Name = "c_barcodenuevo";
            this.c_barcodenuevo.Size = new System.Drawing.Size(76, 20);
            this.c_barcodenuevo.TabIndex = 40;
            this.c_barcodenuevo.Text = "Barcode";
            this.c_barcodenuevo.Visible = false;
            // 
            // c_Salir
            // 
            this.c_Salir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Salir.Location = new System.Drawing.Point(267, 474);
            this.c_Salir.Name = "c_Salir";
            this.c_Salir.Size = new System.Drawing.Size(224, 45);
            this.c_Salir.TabIndex = 39;
            this.c_Salir.Text = "SALIR";
            this.c_Salir.UseVisualStyleBackColor = true;
            this.c_Salir.Click += new System.EventHandler(this.c_Salir_Click);
            // 
            // c_empezar
            // 
            this.c_empezar.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_empezar.Location = new System.Drawing.Point(287, 45);
            this.c_empezar.Name = "c_empezar";
            this.c_empezar.Size = new System.Drawing.Size(226, 67);
            this.c_empezar.TabIndex = 38;
            this.c_empezar.Text = "EMPEZAR";
            this.c_empezar.UseVisualStyleBackColor = true;
            this.c_empezar.Click += new System.EventHandler(this.c_empezar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(365, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(402, 20);
            this.label1.TabIndex = 37;
            this.label1.Text = "INSTRUCCIONES PARA EL CAMBIO DE BOLSA";
            // 
            // c_esperarInicio
            // 
            this.c_esperarInicio.Image = global::Azteca_SID.Properties.Resources.appointment_new_2;
            this.c_esperarInicio.Location = new System.Drawing.Point(623, 419);
            this.c_esperarInicio.Name = "c_esperarInicio";
            this.c_esperarInicio.Size = new System.Drawing.Size(55, 47);
            this.c_esperarInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_esperarInicio.TabIndex = 36;
            this.c_esperarInicio.TabStop = false;
            this.c_esperarInicio.Visible = false;
            // 
            // paso3
            // 
            this.paso3.AutoSize = true;
            this.paso3.BackColor = System.Drawing.Color.Transparent;
            this.paso3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso3.Location = new System.Drawing.Point(134, 242);
            this.paso3.Name = "paso3";
            this.paso3.Size = new System.Drawing.Size(65, 16);
            this.paso3.TabIndex = 35;
            this.paso3.Text = "PASO 3:";
            this.paso3.Visible = false;
            // 
            // c_paso3
            // 
            this.c_paso3.BackColor = System.Drawing.Color.Yellow;
            this.c_paso3.Enabled = false;
            this.c_paso3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso3.ForeColor = System.Drawing.Color.Navy;
            this.c_paso3.Location = new System.Drawing.Point(175, 258);
            this.c_paso3.Name = "c_paso3";
            this.c_paso3.Size = new System.Drawing.Size(445, 31);
            this.c_paso3.TabIndex = 34;
            this.c_paso3.Text = "ABRA LA BOVEDAD";
            this.c_paso3.Visible = false;
            // 
            // paso6
            // 
            this.paso6.AutoSize = true;
            this.paso6.BackColor = System.Drawing.Color.Transparent;
            this.paso6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso6.Location = new System.Drawing.Point(134, 419);
            this.paso6.Name = "paso6";
            this.paso6.Size = new System.Drawing.Size(65, 16);
            this.paso6.TabIndex = 33;
            this.paso6.Text = "PASO 6:";
            this.paso6.Visible = false;
            // 
            // c_paso6
            // 
            this.c_paso6.BackColor = System.Drawing.Color.Yellow;
            this.c_paso6.Enabled = false;
            this.c_paso6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso6.ForeColor = System.Drawing.Color.Navy;
            this.c_paso6.Location = new System.Drawing.Point(175, 435);
            this.c_paso6.Name = "c_paso6";
            this.c_paso6.Size = new System.Drawing.Size(445, 31);
            this.c_paso6.TabIndex = 32;
            this.c_paso6.Text = "CIERRE LA BOVEDA y ESPERE...";
            this.c_paso6.Visible = false;
            // 
            // paso2
            // 
            this.paso2.AutoSize = true;
            this.paso2.BackColor = System.Drawing.Color.Transparent;
            this.paso2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso2.Location = new System.Drawing.Point(134, 181);
            this.paso2.Name = "paso2";
            this.paso2.Size = new System.Drawing.Size(65, 16);
            this.paso2.TabIndex = 31;
            this.paso2.Text = "PASO 2:";
            this.paso2.Visible = false;
            // 
            // paso4
            // 
            this.paso4.AutoSize = true;
            this.paso4.BackColor = System.Drawing.Color.Transparent;
            this.paso4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso4.Location = new System.Drawing.Point(134, 303);
            this.paso4.Name = "paso4";
            this.paso4.Size = new System.Drawing.Size(65, 16);
            this.paso4.TabIndex = 30;
            this.paso4.Text = "PASO 4:";
            this.paso4.Visible = false;
            // 
            // paso5
            // 
            this.paso5.AutoSize = true;
            this.paso5.BackColor = System.Drawing.Color.Transparent;
            this.paso5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso5.Location = new System.Drawing.Point(134, 363);
            this.paso5.Name = "paso5";
            this.paso5.Size = new System.Drawing.Size(65, 16);
            this.paso5.TabIndex = 29;
            this.paso5.Text = "PASO 5:";
            this.paso5.Visible = false;
            // 
            // paso1
            // 
            this.paso1.AutoSize = true;
            this.paso1.BackColor = System.Drawing.Color.Transparent;
            this.paso1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso1.Location = new System.Drawing.Point(134, 121);
            this.paso1.Name = "paso1";
            this.paso1.Size = new System.Drawing.Size(65, 16);
            this.paso1.TabIndex = 28;
            this.paso1.Text = "PASO 1:";
            this.paso1.Visible = false;
            // 
            // c_paso5
            // 
            this.c_paso5.BackColor = System.Drawing.Color.Yellow;
            this.c_paso5.Enabled = false;
            this.c_paso5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso5.ForeColor = System.Drawing.Color.Navy;
            this.c_paso5.Location = new System.Drawing.Point(175, 379);
            this.c_paso5.Name = "c_paso5";
            this.c_paso5.Size = new System.Drawing.Size(445, 31);
            this.c_paso5.TabIndex = 27;
            this.c_paso5.Text = "Leer Nuevo Codigo de Barras: ";
            this.c_paso5.Visible = false;
            // 
            // c_paso4
            // 
            this.c_paso4.BackColor = System.Drawing.Color.Yellow;
            this.c_paso4.Enabled = false;
            this.c_paso4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso4.ForeColor = System.Drawing.Color.Navy;
            this.c_paso4.Location = new System.Drawing.Point(175, 319);
            this.c_paso4.Name = "c_paso4";
            this.c_paso4.Size = new System.Drawing.Size(445, 31);
            this.c_paso4.TabIndex = 26;
            this.c_paso4.Text = "RETIRE LA BOLSA";
            this.c_paso4.Visible = false;
            // 
            // c_paso2
            // 
            this.c_paso2.BackColor = System.Drawing.Color.Yellow;
            this.c_paso2.Enabled = false;
            this.c_paso2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso2.ForeColor = System.Drawing.Color.Navy;
            this.c_paso2.Location = new System.Drawing.Point(175, 197);
            this.c_paso2.Name = "c_paso2";
            this.c_paso2.Size = new System.Drawing.Size(445, 31);
            this.c_paso2.TabIndex = 25;
            this.c_paso2.Text = "ESPERE... REVISANDO EQUIPO";
            this.c_paso2.Visible = false;
            // 
            // c_paso1
            // 
            this.c_paso1.BackColor = System.Drawing.Color.Yellow;
            this.c_paso1.Enabled = false;
            this.c_paso1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso1.ForeColor = System.Drawing.Color.Navy;
            this.c_paso1.Location = new System.Drawing.Point(175, 137);
            this.c_paso1.Name = "c_paso1";
            this.c_paso1.Size = new System.Drawing.Size(445, 31);
            this.c_paso1.TabIndex = 24;
            this.c_paso1.Text = "INTRODUZCA CODIGO";
            this.c_paso1.Visible = false;
            // 
            // c_esperarsellado
            // 
            this.c_esperarsellado.Image = global::Azteca_SID.Properties.Resources.appointment_new_2;
            this.c_esperarsellado.Location = new System.Drawing.Point(623, 181);
            this.c_esperarsellado.Name = "c_esperarsellado";
            this.c_esperarsellado.Size = new System.Drawing.Size(55, 47);
            this.c_esperarsellado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_esperarsellado.TabIndex = 23;
            this.c_esperarsellado.TabStop = false;
            this.c_esperarsellado.Visible = false;
            // 
            // panelSellado1
            // 
            this.panelSellado1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSellado1.Location = new System.Drawing.Point(0, 512);
            this.panelSellado1.Margin = new System.Windows.Forms.Padding(4);
            this.panelSellado1.Name = "panelSellado1";
            this.panelSellado1.Size = new System.Drawing.Size(800, 38);
            this.panelSellado1.TabIndex = 43;
            this.panelSellado1.Visible = false;
            // 
            // bw_cambiobolsa
            // 
            this.bw_cambiobolsa.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_cambiobolsa_DoWork);
            this.bw_cambiobolsa.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_cambiobolsa_RunWorkerCompleted);
            // 
            // FormaCambioBolsa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_Salir);
            this.Controls.Add(this.panelSellado1);
            this.Controls.Add(this.c_ok);
            this.Controls.Add(this.c_barcode);
            this.Controls.Add(this.c_barcodenuevo);
            this.Controls.Add(this.c_empezar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_esperarInicio);
            this.Controls.Add(this.paso3);
            this.Controls.Add(this.c_paso3);
            this.Controls.Add(this.paso6);
            this.Controls.Add(this.c_paso6);
            this.Controls.Add(this.paso2);
            this.Controls.Add(this.paso4);
            this.Controls.Add(this.paso5);
            this.Controls.Add(this.paso1);
            this.Controls.Add(this.c_paso5);
            this.Controls.Add(this.c_paso4);
            this.Controls.Add(this.c_paso2);
            this.Controls.Add(this.c_paso1);
            this.Controls.Add(this.c_esperarsellado);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormaCambioBolsa";
            this.Text = "FormaCambioBolsa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormaCambioBolsa_FormClosing);
            this.Load += new System.EventHandler(this.FormaCambioBolsa_Load);
            this.Click += new System.EventHandler(this.FormaCambioBolsa_Click);
            this.Controls.SetChildIndex(this.c_esperarsellado, 0);
            this.Controls.SetChildIndex(this.c_paso1, 0);
            this.Controls.SetChildIndex(this.c_paso2, 0);
            this.Controls.SetChildIndex(this.c_paso4, 0);
            this.Controls.SetChildIndex(this.c_paso5, 0);
            this.Controls.SetChildIndex(this.paso1, 0);
            this.Controls.SetChildIndex(this.paso5, 0);
            this.Controls.SetChildIndex(this.paso4, 0);
            this.Controls.SetChildIndex(this.paso2, 0);
            this.Controls.SetChildIndex(this.c_paso6, 0);
            this.Controls.SetChildIndex(this.paso6, 0);
            this.Controls.SetChildIndex(this.c_paso3, 0);
            this.Controls.SetChildIndex(this.paso3, 0);
            this.Controls.SetChildIndex(this.c_esperarInicio, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_empezar, 0);
            this.Controls.SetChildIndex(this.c_barcodenuevo, 0);
            this.Controls.SetChildIndex(this.c_barcode, 0);
            this.Controls.SetChildIndex(this.c_ok, 0);
            this.Controls.SetChildIndex(this.panelSellado1, 0);
            this.Controls.SetChildIndex(this.c_Salir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_ok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarsellado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox c_ok;
        private System.Windows.Forms.Label c_barcode;
        private System.Windows.Forms.Label c_barcodenuevo;
        private System.Windows.Forms.Button c_Salir;
        private System.Windows.Forms.Button c_empezar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox c_esperarInicio;
        private System.Windows.Forms.Label paso3;
        private System.Windows.Forms.TextBox c_paso3;
        private System.Windows.Forms.Label paso6;
        private System.Windows.Forms.TextBox c_paso6;
        private System.Windows.Forms.Label paso2;
        private System.Windows.Forms.Label paso4;
        private System.Windows.Forms.Label paso5;
        private System.Windows.Forms.Label paso1;
        private System.Windows.Forms.TextBox c_paso5;
        private System.Windows.Forms.TextBox c_paso4;
        private System.Windows.Forms.TextBox c_paso2;
        private System.Windows.Forms.TextBox c_paso1;
        private System.Windows.Forms.PictureBox c_esperarsellado;
        private PanelSellado panelSellado1;
        private System.ComponentModel.BackgroundWorker bw_cambiobolsa;
    }
}