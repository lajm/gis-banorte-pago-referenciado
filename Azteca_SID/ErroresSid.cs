﻿using Azteca_SID;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SidApi
{
    public class ErroresSid
    {

        public class Codigos
        {
            public const int OKAY = 0;
            public const int SYSTEM_ERROR = -1;
            public const int USB_ERROR = -2;
            public const int UNIT_NOT_FOUND = -3;
            public const int HARDWARE_ERROR = -4;
            public const int INVALID_COMMAND = -9;
            public const int COMMAND_IN_EXECUTION_YET = -10;
            public const int COMMAND_SEQUENCE_ERROR = -11;
            public const int PC_HW_ERROR = -12;
            public const int INVALID_HANDLE = -13;
            public const int IMAGE_NO_FILMED = -14;
            public const int BMP_ERROR = -15;
            public const int MISSING_IMAGE = -16;
            public const int NO_LIBRARY_LOAD = -17;
            public const int IMAGE_NOT_PRESENT = -18;
            public const int OPEN_NOT_DONE = -19;
            public const int INVALID_CLEARBLACK = -20;
            public const int INVALID_SIDE = -21;
            public const int INVALID_SCANMODE = -22;
            public const int INVALID_CMD_HISTORY = -23;
            public const int MISSING_BUFFER_HISTORY = -24;
            public const int CALIBRATION_FAILED = -25;
            public const int INVALID_DOC_LENGTH = -26;
            public const int INVALID_RESET_TYPE = -27;
            public const int INVALID_CASHTYPE = -28;
            public const int INVALID_TYPE = -29;
            public const int INVALID_VALUE = -30;
            public const int MISSING_FILENAME = -32;
            public const int OPEN_FILE_ERROR_OR_NOT_FOUND = -33;
            public const int POWER_OFF = -34;
            public const int UPS_NOT_ANSWER = -35;
            public const int OPERATION_ERROR = -36;
            public const int UPS_BATTERY_KO = -37;
            public const int UPS_ERROR_COMUNICATION = -38;
            public const int INVALID_NRDOC = -39;
            public const int INVALID_QUALITY = -40;
            public const int JPEG_ERROR = -41;
            public const int INVALID_VALIDATION = -42;
            public const int PERIPHERAL_RESERVED = -43;
            public const int CALIBRATION_WEBCAM_ERROR = -99;
            public const int INVALID_WEBCAM_TYPE = -100;
            public const int INVALID_CAPTURE = -101;
            public const int ERROR_FRAME = -102;
            public const int NOTES_CHECKED = -103;
            public const int INVALID_SHAPE_FOUND = -104;
            public const int LIGHT_LOW = -105;
            public const int ERROR_WEB = -106;
            public const int TEMPLATE_FILE_NOT_FOUND = -107;
            public const int JAM_IN_FEEDER_IN = -200;
            public const int JAM_SWITCH_PATH_IN = -201;
            public const int JAM_BOX_REJECT_IN = -202;
            public const int JAM_ENTER_BAG_IN = -203;
            public const int JAM_IN_FEEDER_OUT = -204;
            public const int JAM_SWITCH_PATH_OUT = -205;
            public const int JAM_BOX_REJECT_OUT = -206;
            public const int JAM_ENTER_BAG_OUT = -207;
            public const int REJECT_BAY_FULL = -208;
            public const int JAM_PHOTO_SCANNER = -209;
            public const int UNIT_DOC_TOO_LONG = -213;
            public const int JAM_PRESS_NOTES = -214;
            public const int WELD_NOT_POSSIBLE = -215;
            public const int WELD_NOT_PERMITTED_NO_DOC_INSERTED = -216;
            public const int ERROR_ON_CLOSE_BAG = -217;
            public const int CURRENT_ERROR = -218;
            public const int ERROR_WELD_PROTECTION = -219;
            public const int TIME_OUT_EXPIRED = -220;
            public const int SENSOR_DOOR_OPEN = -221;
            public const int BAG_NOT_PRESENT = -222;
            public const int BAG_CLOSED = -223;
            public const int BAG_NOT_OPEN = -224;
            public const int LOCK_ERROR = -225;
            public const int BAG_PRESENT = -226;
            public const int DIP_SWITCH_HIGHT = -227;
            public const int DIP_SWITCH_NOT_LOW = -228;
            public const int CODELOCCK_NOT_INSERTED = -229;
            public const int ERROR_ELEVATOR = -230;
            public const int ERROR_TENSIONER = -231;
            public const int BAG_NOT_REMOVED = -232;
            public const int DIP_SWITCH_DOOR_LEAFER_OPEN = -240;
            public const int DIP_SWITCH_SP_MAGN1_OPEN = -241;
            public const int DIP_SWITCH_SP_MAGN2_OPEN = -242;
            public const int DIP_SWITCH_SID_OPEN = -243;
            public const int INTERLOCK_DOOR_OPEN = -244;
            public const int INTERLOCK_OPEN = -245;
            public const int REJECT_DOCUMENT_NOT_RECOGNIZED = -345;
            public const int REJECT_STOP_DOCUBLE_LEAFING = -346;
            public const int REJECT_SUSPECT = -347;
            public const int BARCODE_TIME_OUT_EXPIRED = -800;
            public const int BARCODE_COM_ERROR = -801;
            public const int BARCODE_STRING_TRUNCATED = -802;
            public const int ERROR_RECOGNIZE_SKEWED = -1101;
            public const int ERROR_RECOGNIZE_VALIDATION_SIZE = -1102;
            public const int ERROR_RECOGNIZE_CLASSIFICATION = -1110;
            public const int ERROR_RECOGNIZE_VALIDATION = -1111;
            public const int ERROR_RECOGNIZE_IR = -1112;
            public const int ERROR_RECOGNIZE_MGT = -1113;
            public const int ERROR_RECOGNIZE_UV = -1114;
            public const int BADGE_NOT_PRESENT = -1200;
            public const int BADGE_CHECK_CARD_PRESENCE_ERROR = -1201;
            public const int BADGE_SELECT_MEMCARDTYPE_ERROR = -1202;
            public const int BADGE_MEMORY_READ_DATA = -1203;
            public const int BADGE_NO_VALID_DATA = -1204;
            public const int BADGE_OPEN_ERROR = -1205;
            public const int BADGE_STATUS_ERROR = -1206;
            public const int PRINTER_ERROR = -1230;
            public const int PRINTER_STATUS_ERROR = -1231;
            public const int PRINTER_PAPER_NOT_PRESENT = -1232;
            public const int RESTART_WELD = -1999;
            public const int FEEDER_EMPTY = 1;
            public const int ALREADY_OPEN = 5;
            public const int PERIF_BUSY = 6;
            public const int REPORT_NOT_DEPOSTITED = 7;
            public const int COMMAND_NOT_SUPPORTED = 13;
            public const int TRY_TO_RESET = 14;
            public const int STRING_TRUNCATED = 15;
            public const int DOUBLE_LEAFING_WARNING = 16;
            public const int DATATRUNC = 17;
            public const int WARNING_ALARM_CODE = 30;

            public const int WARNING_CAPACITY = 1000;

            public const int NOT_AVAILABLE_IN_DEBUG_MODE = -9995;
            public const int NOT_READY_FOR_DEPOSIT = -9996;
            public const int MAX_CAPACITY = -9997;
            public const int SOFTWARE = -9999;

        }

        public static bool? EsAtoramiento( int p_CodigoNativo )
        {
            bool? l_R = null;
            try
            {
                switch (p_CodigoNativo)
                {
                    case Codigos.JAM_IN_FEEDER_IN:
                    case Codigos.JAM_SWITCH_PATH_IN:
                    case Codigos.JAM_BOX_REJECT_IN:
                    case Codigos.JAM_ENTER_BAG_IN:
                    case Codigos.JAM_IN_FEEDER_OUT:
                    case Codigos.JAM_SWITCH_PATH_OUT:
                    case Codigos.JAM_BOX_REJECT_OUT:
                    case Codigos.JAM_ENTER_BAG_OUT:
                    case Codigos.REJECT_BAY_FULL:
                    case Codigos.JAM_PHOTO_SCANNER:
                    case Codigos.UNIT_DOC_TOO_LONG:
                    case Codigos.JAM_PRESS_NOTES:
                        l_R = true;
                        break;

                    default:
                        l_R = false;
                        break;
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ErroresSid", "EsAtoramiento", E.Message, 1);
            }

            return l_R;
        }


        public static bool? EsSensoresPuertas(int p_CodigoNativo)
        {
            bool? l_R = null;
            try
            {
                switch (p_CodigoNativo)
                {
                    case Codigos.DIP_SWITCH_DOOR_LEAFER_OPEN:
                    case Codigos.DIP_SWITCH_SP_MAGN1_OPEN:
                    case Codigos.DIP_SWITCH_SP_MAGN2_OPEN:
                    case Codigos.DIP_SWITCH_SID_OPEN:
                    case Codigos.INTERLOCK_DOOR_OPEN:
                    case Codigos.INTERLOCK_OPEN:
                        l_R = true;
                        break;

                    default:
                        l_R = false;
                        break;
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("ErroresSid", "EsSensoresPuertas()", E.Message, 1);
            }

            return l_R;
        }



    }
}
