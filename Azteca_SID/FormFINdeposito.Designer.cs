﻿namespace Azteca_SID
{
    partial class FormFINdeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Terminar = new System.Windows.Forms.PictureBox();
            this.c_masdep = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_Terminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_masdep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Terminar
            // 
            this.c_Terminar.BackColor = System.Drawing.Color.Transparent;
            this.c_Terminar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_terminar;
            this.c_Terminar.Location = new System.Drawing.Point(674, 527);
            this.c_Terminar.Name = "c_Terminar";
            this.c_Terminar.Size = new System.Drawing.Size(126, 46);
            this.c_Terminar.TabIndex = 4;
            this.c_Terminar.TabStop = false;
            this.c_Terminar.Visible = false;
            this.c_Terminar.Click += new System.EventHandler(this.c_Terminar_Click);
            // 
            // c_masdep
            // 
            this.c_masdep.BackColor = System.Drawing.Color.Transparent;
            this.c_masdep.Image = global::Azteca_SID.Properties.Resources.btn_banorte_masdepositos;
            this.c_masdep.Location = new System.Drawing.Point(0, 528);
            this.c_masdep.Name = "c_masdep";
            this.c_masdep.Size = new System.Drawing.Size(128, 48);
            this.c_masdep.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_masdep.TabIndex = 5;
            this.c_masdep.TabStop = false;
            this.c_masdep.Click += new System.EventHandler(this.c_masdep_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(311, 264);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "¡FELICIDADES!";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(275, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 56);
            this.label2.TabIndex = 7;
            this.label2.Text = "Su depósito se ha realizado correctamente";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(256, 370);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(288, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "No olvide tomar su comprabante.";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = global::Azteca_SID.Properties.Resources.emblem_default;
            this.pictureBox5.Location = new System.Drawing.Point(323, 160);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(154, 95);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            // 
            // FormFINdeposito
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_masdep);
            this.Controls.Add(this.c_Terminar);
            this.Name = "FormFINdeposito";
            this.ShowInTaskbar = false;
            this.Controls.SetChildIndex(this.c_Terminar, 0);
            this.Controls.SetChildIndex(this.c_masdep, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.pictureBox5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_Terminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_masdep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox c_Terminar;
        private System.Windows.Forms.PictureBox c_masdep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}
