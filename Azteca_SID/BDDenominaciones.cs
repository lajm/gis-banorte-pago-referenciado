﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Azteca_SID
{
    public class BDDenominaciones
    {
        public static bool
            DesactivarDenominaciones()
        {
            String l_Query
                = "UPDATE                   Denominacion "
                + "SET                      Activa = 0 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Desactivar Cuentas", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }
            return true;
        }

        public static bool
            ValidarDenominacionExistente(String p_Denominacion, int p_IdMoneda, String p_Tipo)
        {
            if (p_Tipo != "B")
                return false;
            String l_Query
                = "SELECT                   Activa "
                + "FROM                     Denominacion "
                + "WHERE                    Codigo = @Codigo "
                + "AND                      IdMoneda = @IdMoneda ";
            String l_Codigo = "";
            switch (p_Denominacion)
            {
                case "1":
                    l_Codigo = "31";
                    break;
                case "2":
                    l_Codigo = "32";
                    break;
                case "5":
                    l_Codigo = "33";
                    break;
                case "10":
                    l_Codigo = "34";
                    break;
                case "20":
                    l_Codigo = "35";
                    break;
                case "50":
                    l_Codigo = "36";
                    break;
                case "100":
                    l_Codigo = "37";
                    break;
                case "200":
                    l_Codigo = "38";
                    break;
                case "500":
                    l_Codigo = "39";
                    break;
                case "1000":
                    l_Codigo = "3A";
                    break;
            }

            if (l_Codigo == "")
                return false;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Codigo", l_Codigo);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                    return InsertarDenominacion(p_Denominacion, p_IdMoneda, l_Codigo);
                else
                {
                    if (!(bool)l_Lector[0])
                        return ActualizarDenominacion(p_IdMoneda, l_Codigo);
                    else
                        return true;
                }
            }
        }

        public static bool
            InsertarDenominacion(String p_Denominacion, int p_IdMoneda, String p_Codigo)
        {
            String l_Query
                = "INSERT INTO              Denominacion "
                + "                         (Codigo, IdMoneda, Descripcion, Activa) "
                + "VALUES                   (@Codigo, @IdMoneda, @Descripcion, 1) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Codigo", p_Codigo);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@Descripcion", p_Denominacion);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Insertar Denominaciones", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
            ActualizarDenominacion(int p_IdMoneda, String p_Codigo)
        {
            String l_Query
                = "UPDATE                   Denominacion "
                + "SET                      Activa = 1 "
                + "WHERE                    IdMoneda = @IdMoneda "
                + "AND                      Codigo = @Codigo ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@Alias", p_Codigo);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Update Denominacion", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static int
            ObtenerIdDenominacion(int p_IdMoneda, String p_Codigo)
        {
            String l_Query
                = "SELECT               IdDenominacion "
                + "FROM                 Denominacion "
                + "WHERE                IdMoneda = @IdMoneda "
                + "AND                  Codigo = @Codigo ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@Codigo", p_Codigo);

                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                    return (int)l_Lector[0];
                else
                    return 0;
            }

        }
    }
}
