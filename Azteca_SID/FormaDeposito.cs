﻿using SidApi;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaDeposito : Form
    {

        bool m_NoScanImages;
        String m_DirectorioImagenes;
        int m_NumeroBilleteF;
        int m_NumeroBilleteR;
        DateTime m_FechaHoraInicio;
        bool m_BanderaEnviado;
        uint m_TotalAceptados;
        uint m_TotalRechazados;
        ContadorEfectivo m_InventarioInicialFW;
        ContadorEfectivo m_DetalleBilletes;
        int m_Reply;
        String m_Error;
        int m_NumeroBatchDeposito;
        bool m_EnDeposito;
        bool m_TerminarDeposito;


        DepositoEnCurso m_RecuperaDeposito;


        public FormaDeposito()
        {
            InitializeComponent();

            try
            {
                m_NoScanImages = false;
                m_DirectorioImagenes = "C:\\DetalleDepositos";
                m_NumeroBilleteF = 1;
                m_NumeroBilleteR = 1;
                m_Reply = -1;
                m_FechaHoraInicio = DateTime.Now;
                m_BanderaEnviado = false;
                m_TotalAceptados = 0;
                m_TotalRechazados = 0;
                m_InventarioInicialFW = null;
                m_DetalleBilletes = null;
                m_NumeroBatchDeposito = 1;
                m_TerminarDeposito = false;
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "Constructor", E.Message, 1);
            }
        }


        private void FormaDeposito_Load(object sender, EventArgs e)
        {
            string Aux ="";
            try
            {
                Globales.EnDeposito_ahora =true ;
                //Cada Deposito lleva una Fecha-Hora unica a diferencia de segundos...
                Globales.FechaHoraSesion = DateTime.Now;

                Globales.EscribirBitacora("FormaDeposito", "FormaDeposito_Load()", "Inicio de Deposito, Inicialización Forma de Depósito <<", 3);

                t_inactividad.Interval = Properties.Settings.Default.DelayDeposito * 1000;

                t_inactividad.Enabled = true;

                GC.Collect();
                String l_Error;

                if (Properties.Settings.Default.SalvarImagenes)
                {
                    m_NoScanImages = true;
                    Creardirectoriodestino();
                }



                c_empleado.Text = Globales.NombreUsuario.ToUpper();
                c_EtiquetaNumeroCuenta.Text = Globales.NombreCliente;

                string CuentaTmp = Globales.IdUsuario;
                if (!Properties.Settings.Default.CAJA_EMPRESARIAL)
                {
                    try
                    {
                        CuentaTmp = CuentaTmp.Replace(System.Environment.NewLine, "");
                        CuentaTmp = "********" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);
                    }
                    catch (Exception E)
                    {
                        Globales.EscribirBitacora("Deposito", "No se pudo formatear cuenta Tmp", CuentaTmp, 1);
                        c_cuenta.Text = Globales.IdUsuario;
                    }
                }
                if (!String.IsNullOrEmpty(Globales.ReferenciaGSI))
                {
                    l_ref.Visible = true;
                    l_ref.Text += " " + Globales.ReferenciaGSI;
                    Globales.EscribirBitacora("Deposito", "FormaDeposito_Load()", "Referencia GSI: {" + l_ref.Text + "} ", 1);
                  Aux = Globales.ReferenciaGSI;
                }
                c_cuenta.Text = CuentaTmp;
                Cursor.Hide();


                if (Properties.Settings.Default.No_Debug_Pc)
                {
                    Globales.EscribirBitacora("Deposito", "DETALLE BOLSA ANTES DEL DEPOSITO", "CONTABILIDAD", 1);

                    m_Reply = SidLib.SID_Open(false);

                    // -- OBTENER INVENTARIO INICIAL 
                    if (m_Reply == ErroresSid.Codigos.OKAY)
                    {
                        m_InventarioInicialFW = ObtenerTotalBolsaBilletes();
                        if (m_InventarioInicialFW == null)
                            Globales.EscribirBitacora("Deposito", "FormLoad", "No se pudo obtener el total de la bolsa por FW", 1);
                        else
                            Globales.EscribirBitacora("Deposito", "Inventario Inicial" + Environment.NewLine, m_InventarioInicialFW.ToString(), 1);
                        SidLib.SID_Reset((char)SidLib.RESET_ERROR);
                        SidLib.SID_Close();
                    }
                    else
                        Globales.EscribirBitacora("FormaDeposito", "FormaDeposito_Load", "Obtener totales bolsa, no puede conectarse al equipo", 1);

                    if (m_InventarioInicialFW == null)
                    {
                        using (FormaError f_Error = new FormaError(true, "noDisponible"))
                        {
                            Error_Sid(m_Reply, out l_Error);
                            Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "Funcion 'Contenido_en_Bolsa'", 1);
                            Registrar.Alerta("Problema al obtener Billetes de Bolsa, Sugerir reinicio de Sistema");
                            f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde, Reinicie el Equipo si persiste este Error ";
                            f_Error.ShowDialog();
                            c_Depositar.Enabled = false;
                            pictureBox3.Enabled = false;
                            Close();
                        }
                    }

                    Globales.EscribirBitacora("Deposito", "FIN DETALLE BOLSA ANTES DEL DEPOSITO", "CONTABILIDAD", 1);
                }

                l_Error = "Inicio de depósito  FolioSesion: " + Globales.Numero_Sesion
                           + ",  Usuario: " + c_empleado.Text + "  ,  Cuenta: " + c_cuenta.Text
                           + ",  Fecha/Hora: " + UtilsBD.FormatDate(m_FechaHoraInicio);

                Globales.EscribirBitacora("Deposito", "FormaDeposito_Load", l_Error, 1);

                // -- LOS DEPÓSITOS SE HARAN POR LOTES
                m_NumeroBatchDeposito = 1;

                // -- 
                m_DetalleBilletes = ContadorEfectivo.CrearContadorCts(out l_Error);

                if ( !Properties.Settings.Default.CAJA_EMPRESARIAL ) //VEB
                m_RecuperaDeposito = new DepositoEnCurso(Globales.Numero_Sesion, m_FechaHoraInicio, Globales.IdUsuario, Globales.CuentaBanco, Aux,Globales.NombreUsuario);
                else   //CEB
                m_RecuperaDeposito = new DepositoEnCurso( Globales.Numero_Sesion , m_FechaHoraInicio , Globales.IdUsuario , Aux , Globales.ReferenciaUsuario , Globales.NombreUsuario);

                if (m_RecuperaDeposito.Actualizar(m_InventarioInicialFW, null, out l_Error)
                    < 0)
                {
                    Globales.EscribirBitacora("FormaDeposito", "Load - Grabar DepositoEnCurso", l_Error, 3);
                }
                m_RecuperaDeposito.EscribirJson();
                m_RecuperaDeposito.EscribirCifrado();
                m_EnDeposito = false;

                CalcularCapacidadRestante();

                if ( Globales.Limite_MontoGSI > 0 )
                {
                    e_restriccion.Text += Globales.Limite_MontoGSI.ToString( "C" );
                    e_restriccion.Visible = true;
                }

            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "FormaDeposito_Load()", "Fin de Inicialización >>", 3);
            }
            finally
            {
                Globales.EscribirBitacora("FormaDeposito", "FormaDeposito_Load()", "Fin de Inicialización >>", 3);

            }
        }


        private void EscribirResultados()
        {
            try
            {

                int l_Piezas20 = m_DetalleBilletes.PiezasPorDenominacion(20);
                int l_Piezas50 = m_DetalleBilletes.PiezasPorDenominacion(50);
                int l_Piezas100 = m_DetalleBilletes.PiezasPorDenominacion(100);
                int l_Piezas200 = m_DetalleBilletes.PiezasPorDenominacion(200);
                int l_Piezas500 = m_DetalleBilletes.PiezasPorDenominacion(500);
                int l_Piezas1000 = m_DetalleBilletes.PiezasPorDenominacion(1000);

                c_BilletesAceptados.Text = m_TotalAceptados.ToString();
                if (m_TotalRechazados > 1)
                    c_BilletesRechazados.Text = "Si";

                c_Cantidad20.Text = l_Piezas20.ToString();
                c_Cantidad50.Text = l_Piezas50.ToString();
                c_Cantidad100.Text = l_Piezas100.ToString();
                c_Cantidad200.Text = l_Piezas200.ToString();
                c_Cantidad500.Text = l_Piezas500.ToString();
                c_Cantidad1000.Text = l_Piezas1000.ToString();

                Double l_Efectivo20 = l_Piezas20 * 20;
                Double l_Efectivo50 = l_Piezas50 * 50;
                Double l_Efectivo100 = l_Piezas100 * 100;
                double l_Efectivo200 = l_Piezas200 * 200;
                double l_Efectivo500 = l_Piezas500 * 500;
                double l_Efectivo1000 = l_Piezas1000 * 1000;
                c_Suma100.Text = l_Efectivo100.ToString("$#,###,##0.00");
                c_Suma1000.Text = l_Efectivo1000.ToString("$#,###,##0.00");
                c_Suma20.Text = l_Efectivo20.ToString("$#,###,##0.00");
                c_Suma200.Text = l_Efectivo200.ToString("$#,###,##0.00");
                c_Suma50.Text = l_Efectivo50.ToString("$#,###,##0.00");
                c_Suma500.Text = l_Efectivo500.ToString("$#,###,##0.00");
                double l_TotalEfectivo = l_Efectivo500 + l_Efectivo50 + l_Efectivo200
                    + l_Efectivo20 + l_Efectivo1000 + l_Efectivo100;
                c_TotalDeposito.Text = l_TotalEfectivo.ToString("$#,###,##0.00");


                // ACTUALIZAR ÚNICAMENTE CAMPOS DE VALORES DE BILLETES Y TOTALES
                c_TotalDeposito.Refresh();
                c_BilletesAceptados.Refresh();
                c_BilletesRechazados.Refresh();
                c_Cantidad100.Refresh();
                c_Cantidad1000.Refresh();
                c_Cantidad20.Refresh();
                c_Cantidad200.Refresh();
                c_Cantidad50.Refresh();
                c_Cantidad500.Refresh();
                c_Suma100.Refresh();
                c_Suma1000.Refresh();
                c_Suma20.Refresh();
                c_Suma200.Refresh();
                c_Suma50.Refresh();
                c_Suma500.Refresh();
                c_TotalDeposito.Refresh();
                c_tope.Refresh();
                c_TotalDeposito.Refresh();
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "EscribirResultados()", "Exception - " + E.Message, 3);
            }
            finally
            {
            }
        }


        int? CalcularCapacidadRestante()
        {
            m_Error = "";
            int? l_R = 0;
            try
            {
                int l_TotalPiezasDepositadas = m_InventarioInicialFW.TotalPiezas() + m_DetalleBilletes.TotalPiezas();
                l_R = Properties.Settings.Default.CapacidadBolsa - l_TotalPiezasDepositadas;
                c_tope.Text = "Maximo Permitido: " + l_R + " Billetes";
                Globales.EscribirBitacora("FormaDeposito", "CalcularCapacidadRestante()", " Máximo: " + Properties.Settings.Default.CapacidadBolsa
                        + ",  Depositadas: " + l_TotalPiezasDepositadas, 3);

                if (l_R <= 0)
                {
                    c_tope.Text = "DEJE DE ALIMENTAR EL EQUIPO POR FAVOR";
                    c_tope.BackColor = Color.Yellow;
                    Globales.EscribirBitacora("FormaDeposito", "CalcularCapacidadRestante()", "Equipo lleno", 3);
                    // TODO: Mandar alerta GSI
                }
                else if ((int)l_R < Globales.m_NumeroBilletesBatch)
                {
                    c_tope.Text = "Maximo Permitido:  " + l_R + " Billetes, suministre los billetes de 50 en 50";
                    c_tope.Visible = true;
                }
                c_tope.Refresh();
            }
            catch (Exception E)
            {
                l_R = null;
                Globales.EscribirBitacora("FormaDeposito", "CalcularCapacidadRestante()", "Exception - " + E.Message, 3);
            }
            return l_R;
        }


        private void c_Depositar_Click(object sender, EventArgs e)
        {
            try
            {
                //if ( DateTime.Now - m_FechaHoraInicio < new TimeSpan( 0, 0, 1 ) )
                //    return;

                if (m_EnDeposito == false)
                {
                    m_EnDeposito = true;
                    // -- ACTUALIZAR ESTATUS BOTONES
                    c_Depositar.Enabled = false;
                    pictureBox3.Enabled = false;
                    c_BotonAceptar.Enabled = false;
                    Application.DoEvents();
                    //Refresh();
                    ProcesoDeposito();
                }

            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "C_BDepositarClick", E.Message, 3);

            }
            finally
            {
                //m_FechaHoraInicio = DateTime.Now;
                Application.DoEvents();
                m_EnDeposito = false;
            }
        }


        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                

                Globales.EscribirBitacora("FormaDeposito", "c_BotonAceptar_Click", "En Deposito: " + m_EnDeposito, 3);

                if (m_EnDeposito == false)
                {
                    c_Depositar.Enabled = false;
                    c_BotonAceptar.Enabled = false;
                    Forzardibujado();
                    TerminarDeposito();                    
                }
            }
            catch (Exception E)
            {
            }
            finally
            {
              //  REVISAR esta Asignacion
              // m_EnDeposito = false;
                Application.DoEvents();
            }
        }


        public void TerminarDeposito()
        {
            Globales.EscribirBitacora("FormaDeposito", "TerminarDeposito", "<<", 3);

            int l_ReplyEstatus = 0;

            if (Properties.Settings.Default.No_Debug_Pc)
            {
                try
                {
                    System.Threading.Thread.Sleep(200);  //Esperar que SID termine ejecucion de Cashin

                    // -- VERIFICAR TRAYECTORIA DE BILLETES
                    m_Reply = SidLib.SID_Open(m_NoScanImages);
                    Globales.EscribirBitacora("VERIFICAR TRAYECTORIA DE BILLETES", "OPEN", m_Reply.ToString(), 1);

                    if (m_Reply == SidLib.SID_OKAY || m_Reply == SidLib.SID_ALREADY_OPEN)
                        l_ReplyEstatus = ProcesosSid.Estatus();

                    //// -- MANEJO DE ATASCOS
                    //if ( (bool) ErroresSid.EsAtoramiento( m_Reply ) )
                    //{
                    //    m_Reply = ProcesosSid.SensadoYManejoAtoramientosManual( Globales.m_MaxReintentosAtoramientoManual );
                    //    if ( m_Reply == SidLib.SID_OKAY )
                    //    {
                    //        Globales.EscribirBitacora( "VERIFICAR TRAYECTORIA DE BILLETES", "UnitStatus", m_Reply.ToString(), 1 );
                    //        c_BotonAceptar.Enabled = true;
                    //        return;
                    //    }
                    //}


                    ////
                    //if ( (bool) ErroresSid.EsSensoresPuertas( m_Reply ) )
                    //{
                    //    m_Reply = ProcesosSid.SensadoYManejoManualdePuerta( 3 );
                    //}

                }
                catch (Exception exx)
                {
                    Globales.EscribirBitacora("VERIFICAR TRAYECTORIA DE BILLETES", "excepcion Encontrada", exx.Message, 1);
                }
            } // NO DEBUG


            try
            {

                // -- VERIFICAR QUE LA HORA DE INICIO SEA DISTINTA A LA TRANSACCION ANTERIOR
                if (Globales.FechaInicioTransaccionEnviada == m_FechaHoraInicio)
                {
                    Globales.EscribirBitacora("Deposito", "Se ha detectado envio Duplicado", "Motivo Fecha hora Duplicada", 1);
                    UtilsComunicacion.AlertaGSI(90, "SID_BLOQUEO_DOBLE_ENVIO");
                    this.DialogResult = DialogResult.Abort;
                    return;
                }

                // -- VERIFICAR QUE NO SE HAYA ENVIADO ANTERIORMENTE
                if (m_BanderaEnviado)
                {
                    Globales.EscribirBitacora("Deposito", "Se ha detectado envio Duplicado", "Motivo Candado Aplicado", 1);
                    UtilsComunicacion.AlertaGSI(90, "SID_BLOQUEO_DOBLE_ENVIO");
                    return;
                }
              /// previa posisicion de Enviado =true

                pictureBox3.Enabled = false;
                //c_accion.Image = Properties.Resources.appointment_soon;
                //c_accion.SizeMode = PictureBoxSizeMode.Zoom;
                c_enviando.Visible = true;
                Forzardibujado();

                if (Properties.Settings.Default.No_Debug_Pc)
                {
                  ContadorEfectivo l_bolsafinal =   ObtenerTotalBolsaBilletes();
                    if ( l_bolsafinal != null )
                    {
                        Globales.EscribirBitacora( "FormaDeposito" , "TerminarDeposito-- Contenido Final: " , l_bolsafinal.ToString( ) , 3 );
                    }
                    else
                    {
                        using ( FormaError v_tiempo = new FormaError( true , "pregunta" ) )
                        {
                            v_tiempo.c_MensajeError.Text = "Por favor espere un momento (15 seg) y vuelva a intentar terminar su Operación";
                            v_tiempo.TopMost = true;
                            v_tiempo.ShowDialog( );
                        }


                        c_Depositar.Enabled = true;
                        c_BotonAceptar.Enabled = true;
                        Forzardibujado( );
                        return;
                    }

                }

                if (m_DetalleBilletes.TotalPiezas() > 0)
                {
                   // Globales.EscribirBitacora("Deposito", "Contabilidad en Pantalla", "BIlletes Validados", 1);
                    m_DetalleBilletes.ToString();

                    // -- GENERAR TABLA DETALLE DEPOSITO
                    DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
                    for (int i = 0; i < m_DetalleBilletes.Desglose.Length; i++)
                    {
                        if (m_DetalleBilletes.Desglose[i].m_Cantidad > 0)
                        {
                            DataRow l_Nueva = l_Detalle.NewRow();
                            l_Nueva[0] = m_DetalleBilletes.Desglose[i].m_IdDenominacion;
                            l_Nueva[1] = m_DetalleBilletes.Desglose[i].m_Cantidad;
                            l_Nueva[2] = m_DetalleBilletes.Desglose[i].m_Valor;
                            l_Detalle.Rows.Add(l_Nueva);
                        }
                    }

                    Cursor.Current = Cursors.Default;

                    if (!UtilsComunicacion.Enviar_Transaccion(Globales.NombreCliente, 1, 1, 1, m_DetalleBilletes.TotalMonto(),
                        l_Detalle, 0, true, Globales.IdUsuario))
                    {
                        // TODO: No deberia actualizarse la fecha de envío
                        Globales.FechaInicioTransaccionEnviada = m_FechaHoraInicio;

                        //if ( m_TotalRechazados > 0 )
                        //{
                        //    using ( FormaError f_Error = new FormaError( true, "retireBilletes" ) )
                        //    {
                        //        f_Error.c_MensajeError.Text = "Por favor retire los billetes rechazados";
                        //        f_Error.ShowDialog();
                        //        m_TotalRechazados = 0;
                        //    }
                        //}
                        Globales.EscribirBitacora( "FormaDeposito" , "TerminarDeposito" , "Hay un Problema al Registar la Transaccion" , 3 );

                        using (FormaError f_Error = new FormaError(false, "warning"))
                        {
                            f_Error.c_MensajeError.Text = "Hay un Problema al Registar la Transaccion Avise Al Administrador";
                            f_Error.ShowDialog();
                        }
                        Close();
                        Visible = false;
                        Application.DoEvents();
                        if ((bool)ErroresSid.EsAtoramiento(l_ReplyEstatus))
                            using (FormaFueraServicio l_Forma = new FormaFueraServicio(false, false, false, false, false, true,false,false))
                            {
                                l_Forma.ShowDialog();
                            }

                        return;
                    }
                    else
                    {
                        Globales.FechaInicioTransaccionEnviada = m_FechaHoraInicio;
                        m_BanderaEnviado = true;
                        //if ( m_TotalRechazados > 0 )
                        //{
                        //    using ( FormaError f_Error = new FormaError( true, "retireBilletes" ) )
                        //    {
                        //        f_Error.c_MensajeError.Text = "Por favor retire los billetes rechazados";
                        //        f_Error.ShowDialog();
                        //        m_TotalRechazados = 0;
                        //    }
                        //}

                        DialogResult = DialogResult.OK;

                        // c_accion.Image = Properties.Resources.appointment_new_2;

                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        Close();
                        Visible = false;
                        Application.DoEvents();
                      

                        if ((bool)ErroresSid.EsAtoramiento(l_ReplyEstatus))
                            using (FormaFueraServicio l_Forma = new FormaFueraServicio(false, false, false, false, false, true,false,false))
                            {
                                l_Forma.ShowDialog();
                            }

                        return;
                    }

                }
                else
                {
                    m_BanderaEnviado = true;
                    DepositoEnCurso.EliminarTransaccionTrunca( Globales.Numero_Sesion );
                    using (FormaError f_Error = new FormaError(true, "sinDinero"))
                    {
                        Globales.EscribirBitacora("Deposito", "SIN Deposito", m_Error, 1);
                        f_Error.c_MensajeError.Text = "No ha depositado nada";
                        f_Error.ShowDialog();
                        Dispose();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        if (l_ReplyEstatus < SidLib.SID_OKAY)
                        {
                            Globales.EscribirBitacora("FormaDeposito", "TerminarDeposito", "Error Estatus Reply : " + m_Reply, 3);
                            c_Depositar.Enabled = true;
                            c_BotonAceptar.Enabled = true;
                            using (FormaFueraServicio l_Forma = new FormaFueraServicio(false, false, false, false, false, true,false,false))
                            {
                                l_Forma.ShowDialog();
                            }
                            // TODO: Aqui return?
                            //return;
                        }

                        Close();

                    }
                    if (m_TotalRechazados > 0)
                    {
                        //using ( FormaError f_Error = new FormaError( true, "retireBilletes" ) )
                        //{
                        //    f_Error.c_MensajeError.Text = "Por favor retire los billetes rechazados";
                        //    f_Error.ShowDialog();
                        //    m_TotalRechazados = 0;
                        //}
                        if (l_ReplyEstatus < SidLib.SID_OKAY)
                        {
                            Globales.EscribirBitacora("FormaDeposito", "TerminarDeposito", "Error Estatus Reply : " + m_Reply, 3);
                            c_Depositar.Enabled = true;
                            c_BotonAceptar.Enabled = true;
                            Close();
                            Visible = false;
                            Application.DoEvents();
                            using (FormaFueraServicio l_Forma = new FormaFueraServicio(false, false, false, false, false, true,false,false))
                            {
                                l_Forma.ShowDialog();
                            }
                            // TODO: Aqui return?
                            //return;
                        }

                    }

                    return;
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("Deposito", "Registrando y mandando deposito", E.Message, 1);
            }
            finally
            {
                if (Properties.Settings.Default.No_Debug_Pc)
                {
                    SidLib.SID_Reset((char)SidLib.RESET_ERROR);
                    SidLib.SID_Close();
                    m_EnDeposito = false;
                }
            }
        }


        public void Error_Sid(int l_Reply, out String p_Error)
        {
            String l_Error = "";

            SidLib.Error_Sid(l_Reply, out l_Error);
            p_Error = l_Error;

            if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {

                if ( l_Reply != SidLib.SID_JAM_IN_FEEDER_IN && l_Reply != SidLib.SID_POWER_OFF )
                    if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply < 0 && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                    {
                        UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                        Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );
                    }


                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }
            }


        }


        ContadorEfectivo ObtenerTotalBolsa()
        {
            //Globales.EscribirBitacora( "ObtenerTotalBolsa() << " );
            ContadorEfectivo l_Contador = null;
            String l_Error;
            try
            {
                int l_Reply;
                l_Contador = ProcesosSid.TotalBolsaFW(out l_Reply, out l_Error);
                if (l_Reply != ErroresSid.Codigos.OKAY)
                {
                    Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "ObtenerTotalBolsa()", 1);
                }
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("Deposito", "ObtenerTotalBolsa() Exception ", E.Message, 1);
            }
            //Globales.EscribirBitacora( "ObtenerTotalBolsa() >> " );
            return l_Contador;
        }


        public int PrepararEquipo(String p_Usuario, int p_MaxIntentosEquipo, int p_MaxIntentosIntervencion, out int p_CapacidadRestante)
        {
            int l_Reply = 0;
            String l_Error;
            p_CapacidadRestante = 0;
            //bool l_SID = false;
            System.Threading.Thread.Sleep(300);
            try
            {
                Globales.EscribirBitacora("FormaDeposito.PrepararEquipo() <<");
                // -- ABRIR EQUIPO CON TIMEOUT
                DateTime l_InicioAbrir = DateTime.Now;
                TimeSpan l_Tiempo = new TimeSpan();
                do
                {
                    l_Reply = SidLib.SID_Open(false);
                    if ((l_Reply != ErroresSid.Codigos.ALREADY_OPEN)
                            || (l_Reply != 0))
                        break;
                    System.Threading.Thread.Sleep(100);
                    l_Tiempo = DateTime.Now - l_InicioAbrir;
                } while ((l_Reply != ErroresSid.Codigos.OKAY)
                          && l_Tiempo.TotalSeconds < 5);
                if (l_Reply == ErroresSid.Codigos.ALREADY_OPEN)
                    l_Reply = ErroresSid.Codigos.OKAY;
                if (l_Reply != ErroresSid.Codigos.OKAY)
                {
                    using (FormaError f_Error = new FormaError(false, "error"))
                    {
                        SidLib.Error_Sid(l_Reply, out l_Error);
                        f_Error.c_MensajeError.Text = l_Error + "No se pudo abrir la conexion con el depositador, ID: " + l_Reply.ToString() + " - " + l_Error;
                        f_Error.ShowDialog();
                    }
                    return l_Reply;
                }
                //l_SID = true;

                // - VALIDAR ESPACIO EN LA BOLSA
                int? l_CapacidadRestante = CalcularCapacidadRestante();
                if (l_CapacidadRestante == null)
                {
                    Globales.EscribirBitacora("FormaDeposito", "PrepararEquipo", m_Error, 1);
                    ProcesosSid.EnviarErrorSid(ErroresSid.Codigos.SOFTWARE, " FormaDeposito.PrepararEquipo no se pudo calcular capacidad restante : " + m_DetalleBilletes.TotalPiezas().ToString());
                    return ErroresSid.Codigos.SOFTWARE;
                }
                else if (l_CapacidadRestante <= 0)
                {
                    ProcesosSid.EnviarErrorSid(ErroresSid.Codigos.MAX_CAPACITY, "No hay mas espacio en la bolsa Tope : " + m_DetalleBilletes.TotalPiezas().ToString());
                    Globales.EscribirBitacora("FormaDeposito", "PrepararEquipo", "No hay mas espacio en la bolsa Tope: " + m_DetalleBilletes.TotalPiezas().ToString(), 1);
                    c_BotonAceptar.Enabled = true;
                    c_Depositar.Enabled = false;
                    Refresh();
                    p_CapacidadRestante = (int)l_CapacidadRestante;
                    return ErroresSid.Codigos.MAX_CAPACITY;
                }
                p_CapacidadRestante = (int)l_CapacidadRestante;

                // -- CONFIGURAR
                l_Reply = SidLib.SID_ConfigDoubleLeafing(SidLib.DOUBLE_LEAFING_WARNING
                    , (short)Properties.Settings.Default.ValorDoubleLeafing, (short)Properties.Settings.Default.ValorPolimero, 0);
                SidLib.Error_Sid(l_Reply, out l_Error);
                Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: ALGODON " + Properties.Settings.Default.ValorDoubleLeafing, l_Error, 1);
                Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: POLYMERO " + Properties.Settings.Default.ValorPolimero, l_Error, 1);

                // -- LOGIN SID
                char[] l_username = Globales.IdUsuario.ToCharArray();
                l_Reply = SidApi.SidLib.SID_Login(l_username);
                Globales.EscribirBitacora("SELECT DEPOSITO", "SID_LOGIN", "login SIDserial: " + Properties.Settings.Default.NumSerialEquipo, 3);

                // -- SENSADO EQUIPO LISTO DEPOSITO
                bool? l_EquipoListo = ProcesosSid.SensadoEquipoListoDeposito();
                if (l_EquipoListo != true)
                    return ErroresSid.Codigos.NOT_READY_FOR_DEPOSIT;

                if ( p_MaxIntentosIntervencion > 0 )
                {
                    // -- ATASCOS Y MANEJO DE ERRORES 
                    l_Reply = ProcesosSid.SensadoYManejoAtoramientosManual( p_MaxIntentosIntervencion );

                    if ( l_Reply == ErroresSid.Codigos.OKAY )
                        return l_Reply;
                    // -- SENSADO DE PUERTAS Y MANEJO DE ERRORES 
                    bool? l_SensoresPuertasOk = ErroresSid.EsSensoresPuertas( l_Reply );

                    if ( l_SensoresPuertasOk == true )
                    {
                        SidLib.Error_Sid( l_Reply , out l_Error );
                        l_Reply = ProcesosSid.ManejoSensoresPuertasManual( p_MaxIntentosIntervencion , l_Error );

                    }

                    if ( l_Reply < 0 )
                    {
                       
                        Globales.EscribirBitacora( "SensadoYManejoAtoramientosManual()" , "Reply:  " + l_Reply , "TerminandoTransaccion " , 3 );
                        c_BotonAceptar_Click( null , null );
                        Close( );
                    }



                    return l_Reply;
                }

                // -- DETECCIÓN DE BILLETES
                //if ( ProcesosSid.SensadoYProcesamientoHayBilletes() != true )
                //{
                //    c_Depositar.Enabled = true;
                //    l_Reply = 1;
                //    return l_Reply;
                //}

                //  CalcularCapacidadRestante();
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "PrepararEquipo()", "login SIDserial: " + Properties.Settings.Default.NumSerialEquipo
                    + "  EXCEPTION : " + E.Message, 3);
            }
            finally
            {
                //if ( l_SID )
                //{
                //    SidLib.SID_Close();
                //}
                Globales.EscribirBitacora("FormaDeposito.PrepararEquipo() >>");
            }

            return l_Reply;
        }


        private void Llenar_Aleatorio()
        {

            Random rd = new Random();
            int l_ValorR;
            for (int i = 0; i < 50; i++) //50
            {
                l_ValorR = rd.Next(0, 1001); //1001

                m_TotalAceptados++;
                if (l_ValorR <= 20)
                    m_DetalleBilletes.AgregarPiezasValor(20, 1);
                else if (l_ValorR <= 50)
                    m_DetalleBilletes.AgregarPiezasValor(50, 1);
                else if (l_ValorR <= 100)
                    m_DetalleBilletes.AgregarPiezasValor(100, 1);
                else if (l_ValorR <= 200)
                    m_DetalleBilletes.AgregarPiezasValor(200, 1);
                else if (l_ValorR <= 500)
                    m_DetalleBilletes.AgregarPiezasValor(500, 1);
                else if (l_ValorR <= 1000)
                    m_DetalleBilletes.AgregarPiezasValor(1000, 1);
                else
                {
                    m_TotalAceptados--;
                    m_TotalRechazados++;
                }
            }
        }


        public void Forzardibujado()
        {
            //this.Invalidate();
            //this.Update();
            //this.Refresh();
            //Application.DoEvents();

            c_BotonAceptar.Refresh( );
            c_Depositar.Refresh( );
            c_enviando.Refresh( );
        }


        private void Creardirectoriodestino()
        {
            try
            {
                String l_carpetaDia = DateTime.Today.ToString("d_dddd");
                String l_caprteaMes = DateTime.Today.ToString("MMMM");
                String l_horaTransaccion = DateTime.Now.ToString("HH_mm_ss");

                m_DirectorioImagenes = m_DirectorioImagenes + "\\" + l_caprteaMes + "\\" + l_carpetaDia + "\\" + Globales.IdUsuario + "\\" + l_horaTransaccion;
                Directory.CreateDirectory(m_DirectorioImagenes);
                Globales.EscribirBitacora("Deposito con Imagenes ", "Creardirectoriodestino", m_DirectorioImagenes, 1);
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Deposito con Imagenes ", ex.Message, m_DirectorioImagenes, 1);
            }

        }


        private string CrearPathDestinoFrente(string l_sufijo)
        {

            String l_fin = "\\" + m_NumeroBilleteF.ToString("00000") + l_sufijo;
            m_NumeroBilleteF++;
            return m_DirectorioImagenes + l_fin;
        }


        private string CrearPathDestinoReverso(string l_sufijo)
        {

            String l_fin = "\\" + m_NumeroBilleteR.ToString("00000") + l_sufijo;
            m_NumeroBilleteR++;
            return m_DirectorioImagenes + l_fin;
        }


        int AjustarConteoParcial()
        {
            int l_R = 0;
            try
            {
                Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial", "", 1);
                ContadorEfectivo l_InventarioFinalFW = null;
                Globales.EscribirBitacora(">>> AJUSTAR 1");
                //m_Reply = SidLib.SID_Open( false );
                Globales.EscribirBitacora(">>>> AJUSTAR 1A  R: " + m_Reply);

                //if ( ( m_Reply == ErroresSid.Codigos.OKAY )
                //    || ( m_Reply == ErroresSid.Codigos.ALREADY_OPEN ) )
                //{
                Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial ", "Obtener Inventario Final FW", 1);

                l_InventarioFinalFW = ObtenerTotalBolsaBilletes();
                if (l_InventarioFinalFW != null)
                {
                    Globales.EscribirBitacora(m_InventarioInicialFW.ToString());

                    ContadorEfectivo l_TotalDepositoFW = ContadorEfectivo.CrearContadorCts(out m_Error);

                    // -- CALCULAR TOTAL DEPÓSITO FW
                    Globales.EscribirBitacora(">>> AJUSTAR 3");
                    l_TotalDepositoFW.CargarMoneda(1);
                    l_TotalDepositoFW = l_InventarioFinalFW.DiferenciaCts(m_InventarioInicialFW);
                    Globales.EscribirBitacora(">>> AJUSTAR 5");
                    Globales.EscribirBitacora(m_DetalleBilletes.ToString());
                    Globales.EscribirBitacora(">>> AJUSTAR 6");

                    ContadorEfectivo l_Diferencia = m_DetalleBilletes.DiferenciaCts(l_TotalDepositoFW);

                    m_TotalAceptados = (uint)l_TotalDepositoFW.TotalPiezas();

                    if (l_TotalDepositoFW.TotalPiezas() == m_DetalleBilletes.TotalPiezas())
                    {
                        Globales.EscribirBitacora("Deposito", "Contabilidad", "Deposito Congruente, sin ajustes", 1);
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial", "Deposito SW-FW"
                            + Environment.NewLine + m_DetalleBilletes.ToString(), 1);
                    }
                    else
                    {
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial", "Calcular Diferencias FW-SW "
                            + Environment.NewLine + l_Diferencia.ToString(), 1);
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial - Inventario Final FW" + Environment.NewLine
                                                    , l_InventarioFinalFW.ToString(), 1);
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial - Inventario Inicial FW" + Environment.NewLine
                                                    , m_InventarioInicialFW.ToString(), 1);
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial", "Deposito FW : "
                            + Environment.NewLine + l_TotalDepositoFW.ToString(), 1);
                        Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial", "Deposito Software"
                            + Environment.NewLine + m_DetalleBilletes.ToString(), 1);
                        Globales.EscribirBitacora("Deposito", "DIFERENCIA DEPOSITO VS ACEPTADOS", "Empieza AJUSTE "+ l_Diferencia.Desglose.Length , 1);
                        for (int i = 0; i < l_Diferencia.Desglose.Length; i++)
                        {
                            Globales.EscribirBitacora(">>> AJUSTAR 7 D: " + m_DetalleBilletes.Desglose[i].m_Valor.ToString("C"));
                            int l_Cantidad = m_DetalleBilletes.Desglose[i].m_Cantidad;
                            int l_CantidadFW = l_Diferencia.Desglose[i].m_Cantidad;
                            int l_Dif = l_Cantidad - l_CantidadFW;
                            if (l_Dif > 0)
                            {
                                Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito"
                                    , "  Denominacion : " + m_DetalleBilletes.Desglose[i].m_Valor.ToString("C")
                                    + ", Cantidad Actual : " + m_DetalleBilletes.Desglose[i].m_Cantidad.ToString()
                                    + ", Cantidad Real FW : " + l_TotalDepositoFW.Desglose[i].m_Cantidad.ToString()
                                    + ", Faltante/Sobrante : " + l_Diferencia.Desglose[i].m_Cantidad.ToString()
                                    , 1);
                            }
                        }
                        m_DetalleBilletes = l_TotalDepositoFW;
                        Globales.EscribirBitacora("Registro Deposito", "Registro de FALTANTE/SOBRANTE en CONTEO", "FIN Registro", 1);
                    }

                }
                else
                {
                    Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial ", "No se pudo obtener el total de la bolsa por FW", 1);
                    Registrar.Alerta("Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario: "
                                        + Globales.NombreUsuario + "Hora evento: " + DateTime.Now);
                    Globales.EscribirBitacora("Comparar Bolsas", "total_deposito_en_bolsa"
                                        , "Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario:  "
                                        + Globales.NombreUsuario + "Hora evento: " + DateTime.Now, 3);
                    for (int i = 0; i < m_DetalleBilletes.Desglose.Length; i++)
                        Globales.EscribirBitacora("Deposito", " Registro SID ", "  Denominacion: " + m_DetalleBilletes.Desglose[i].m_Valor.ToString("C") + " Cantidad Esperada: NO DETERMINADO", 1);

                    Double l_TotalEfectivo = 0;
                    for (int i = 0; i < m_DetalleBilletes.Desglose.Length; i++)
                    {
                        int l_Denominacion = m_DetalleBilletes.Desglose[i].m_IdDenominacion;
                        int l_Cantidad = m_DetalleBilletes.Desglose[i].m_Cantidad;
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito"
                            , "  Denominacion: " + l_Denominacion.ToString("C")
                                + "Cantidad PRESUNTAMENTE en bolsa: " + l_Cantidad, 1);
                        l_TotalEfectivo += l_Denominacion * l_Cantidad;
                    }
                    Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito"
                        , "  Cantidad total PRESUNTAMENTE en bolsa: " + l_TotalEfectivo + "\n\r", 1);

                    for (int i = 0; i < m_DetalleBilletes.Desglose.Length; i++)
                    {
                        if (m_DetalleBilletes.Desglose[i].m_Cantidad != 0)
                        {
                            Globales.EscribirBitacora("Deposito", "Macheo de billetes"
                                                    , "Diferencia de Billetes Cantidad :  " + m_DetalleBilletes.Desglose[i].m_Cantidad
                                                    + "  Denominacion $ : " + m_DetalleBilletes.Desglose[i].m_Cantidad.ToString("C"), 1);
                        }
                    }
                    Globales.EscribirBitacora("Registro Deposito", "Registro sin CONFRONTAR", "FIN Registro", 1);
                }

                EscribirResultados();
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "AjustarConteoParcial  EXCEPTION", E.Message, 1);
                l_R = ErroresSid.Codigos.SOFTWARE;
            }

            return l_R;
        }


        ContadorEfectivo ObtenerTotalBolsaBilletes()
        {
            //Globales.EscribirBitacora( "ObtenerTotalBolsa() << " );
            ContadorEfectivo l_Contador = null;
            String l_Error;
            try
            {
                int l_Reply;
                l_Contador = ProcesosSid.TotalBolsaFW(out l_Reply, out l_Error);
                if (l_Reply != ErroresSid.Codigos.OKAY)
                {
                    Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "ObtenerTotalBolsa()", 1);
                }

                
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("Deposito", "ObtenerTotalBolsa() Exception ", E.Message, 1);
            }
            return l_Contador;
        }


        private int ProcesoDeposito()
        {
            Int32 Frente, Back, IRFrente, IRBack, MgFrente, Mgback;
            int Reserved = 0;
            bool? l_HayBilletes = true;
            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            ushort[] l_totalNotes = new ushort[10];
            Cursor l_CursorActivo = Cursor.Current;
            bool l_SidAbierto = false;
            int l_Reply;
            c_BilletesRechazados.Text = "No";
            m_TotalRechazados = 0;

            try
            {
                // -- DEPOSITO REAL O SIMULADO
                if (Properties.Settings.Default.No_Debug_Pc)
                {
                    char[] l_username = Globales.IdUsuario.ToCharArray();

                    Cursor.Current = Cursors.WaitCursor;
                    try
                    {
                        // -- ABRIR 
                        int l_CapacidadRestante;
                        l_Reply = PrepararEquipo(Globales.UsuarioGSI, Globales.m_MaxReintentosAtoramientoManual
                                                , Globales.m_MaxReintentosPuertas, out l_CapacidadRestante);

                        if ((l_Reply == ErroresSid.Codigos.ALREADY_OPEN)
                            || (l_Reply > 0))
                            l_Reply = 0;

                        if (l_Reply < 0)
                        {
                         
                            Error_Sid(l_Reply, out m_Error);
                            Globales.EscribirBitacora("FormaDeposito", "ProcesoDeposito() SID_Open", m_Error, 1);
                            c_BotonAceptar.Enabled = true;
                            c_Depositar.Enabled = true;
                            return l_Reply;
                        }
                        Globales.EscribirBitacora("FormaDeposito", "ProcesoDeposito() SID_Open", m_Error, 1);

                        l_SidAbierto = true;

                        // -- NO HAY DOCUMENTOS
                        //if ( ProcesosSid.AlimentadorVacio() )
                        //{
                        //    using ( FormaError f_Error = new FormaError( false, "sin billetes" ) )
                        //    {
                        //        //l_Error = "ProcesosSid.SensadoYManejoAtoramientosAutomatizado()";
                        //        Error_Sid( ErroresSid.Codigos.FEEDER_EMPTY, out m_Error );
                        //        Globales.EscribirBitacora( "Deposito", "Alimentado3r Vacío", m_Error, 1 );
                        //        //ProcesosSid.EnviarErrorSid( 1, m_Error );
                        //        f_Error.c_MensajeError.Text = " Por favor ponga billetes en el alimentador - " + m_Error;
                        //        f_Error.ShowDialog();
                        //    }

                        //    //if ( ProcesosSid.AlimentadorVacio() )
                        //    //{
                        //    //    string l_Error;
                        //    //    l_Reply = ErroresSid.Codigos.FEEDER_EMPTY;
                        //    //    Error_Sid( l_Reply, out l_Error );
                        //    //    Globales.EscribirBitacora( "FormaDeposito", "ProcesoDeposito", "Máximos reintentos - "
                        //    //            + l_Error, 3 );
                        //    return l_Reply;
                        //    //}
                        //}


                        // -- DEPOSITAR
                        m_NumeroBatchDeposito = 1;

                        int l_Procesados = 0;

                        Globales.EscribirBitacora("CICLO ", "DE DEPOSITO", "Boton Depositar Presionado", 3);
                        do // While ( l_Haybilletes)
                        {
                            Application.DoEvents( );
                            int l_NumeroBilletes = ( Globales.m_NumeroBilletesBatch < l_CapacidadRestante ) ? Globales.m_NumeroBilletesBatch : l_CapacidadRestante;
                            Globales.EscribirBitacora( "FormaDeposito" , "CashIn"
                                    , "Procesar batch (" + m_NumeroBatchDeposito + ") de billetes de :" + l_NumeroBilletes , 3 );
                            // JUNIO 2017 Correcion Maximo de Billetes
                            if ( l_CapacidadRestante <= 0 )
                            {
                                SidLib.SID_Close( );
                                Globales.EscribirBitacora( "FormaDeposito" , "CashIn" , " Alcanzado el Máximo de Billetes... Terminando Transacción " + l_Reply , 3 );
                                break;
                            }
                            SidLib.SID_Reset( (char) SidLib.RESET_ERROR );
                            l_Reply = SidLib.SID_CashIn( (ushort) Globales.m_NumeroBilletesBatch , 1 , 1 , m_NoScanImages , 0 , 0 );
                            Globales.EscribirBitacora( "FormaDeposito" , "CashIn" , "Resultado Cashin: " + l_Reply , 3 );

                            //  UPS OFF
                            if ( l_Reply == SidLib.SID_POWER_OFF )
                            {

                                Error_Sid( l_Reply , out m_Error );
                                Globales.EscribirBitacora( "Deposito" , "Error En el Deposito: " , m_Error , 1 );
                                Globales.EscribirBitacora( "Deposito de Efectivo " , "Error durante Deposito: " , l_Reply.ToString( ) , 1 );
                                m_EnDeposito = false;
                                if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada )
                                {
                                    UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                                    Globales.Alerta_Ups_entrada = true;

                                    Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );
                                }
                                goto CONTINUAR;

                            }


                            // -- NO HAY MAS DOCUMENTOS
                            if ( l_Reply == ErroresSid.Codigos.FEEDER_EMPTY )
                            {
                                if ( l_Procesados == 0 )
                                    using ( FormaError f_Error = new FormaError( true , "sin billetes" ) )
                                    {
                                        //l_Error = "ProcesosSid.SensadoYManejoAtoramientosAutomatizado()";
                                        Error_Sid( ErroresSid.Codigos.FEEDER_EMPTY , out m_Error );
                                        Globales.EscribirBitacora( "Deposito" , "Alimentador Vacío" , m_Error , 1 );
                                        //ProcesosSid.EnviarErrorSid( 1, m_Error );
                                        f_Error.c_MensajeError.Text = "Coloque los billetes en el validador y presione Depositar ";// + m_Error;
                                        f_Error.ShowDialog( );
                                    }

                                //c_Depositar.Enabled = true;
                                //c_BotonAceptar.Enabled = true;
                                //pictureBox3.Enabled = true;
                                c_Depositar.Text = "ALGO MAS";
                                l_HayBilletes = false;
                                continue;
                            }
                            else
                                l_Procesados++;
                            //
                            // -- PROCESAR BILLETES LEIDOS
                            //
                            short[] result = new short[1];
                            l_Reply = SidLib.SID_WaitValidationResult( result );
                            System.Threading.Thread.Sleep( 30 );

                            while ( l_Reply == SidLib.SID_PERIF_BUSY
                                 || l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING
                                 || l_Reply == SidLib.SID_UNIT_DOC_TOO_LONG )
                            {
                                Application.DoEvents( );
                                //if (result[0] != 0)
                                //    Globales.EscribirBitacora("FormaDeposito", "ProcesarDeposito", " Id del billete R[0]:" + result[0], 3);

                                // -- MANEJO DE RECHAZADOS DOBLES
                                if ( ( l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING )
                                        || ( l_Reply == SidLib.SID_UNIT_DOC_TOO_LONG ) )
                                {
                                    m_TotalRechazados++;
                                    SidLib.SID_ClearDenominationNote( );
                                    if ( l_Reply == SidLib.SID_DOUBLE_LEAFING_WARNING )
                                    {
                                        m_TotalRechazados++;
                                        Globales.EscribirBitacora( "Deposito" , " MOTIVO RECHAZO " , "Billete alimentado DOBLE" , 2 );
                                    }
                                    else
                                        Globales.EscribirBitacora( "Deposito" , " MOTIVO RECHAZO " , "Billete muy largo o Doble Desfazado" , 2 );
                                }
                                else if ( ( result[0] == 0xe0 ) || ( result[0] == 0xe2 ) )
                                {
                                    // - MANEJO DE RECHAZOS FALSOS, SUCIOS Y OTROS
                                    m_TotalRechazados++;
                                    int l_detalle_reconocimiento = SidApi.SidLib.SID_RecognizeErrorDetail( );
                                    SidApi.SidLib.Error_Sid( l_detalle_reconocimiento , out m_Error );
                                    Globales.EscribirBitacora( "Deposito" , " MOTIVO RECHAZO " , m_Error , 2 );
                                    SidLib.SID_ClearDenominationNote( );
                                }
                                else // -- AGREGAR BILLETES 
                                    if ( result[0] != 0 )
                                {
                                    int l_RValor = result[0];

                                    // BILLETES RECONOCIDOS 
                                    if ( ( l_RValor >= 16 )
                                        && ( l_RValor <= 21 ) )
                                    {
                                        double l_Valor;
                                        m_DetalleBilletes.AgregarPiezasCodigoNativo( l_RValor , 1 , out l_Valor );
                                        m_TotalAceptados++;
                                        l_CapacidadRestante--;
                                        //Globales.EscribirBitacora("FormaDeposito", "ProcesarDeposito"
                                        //        , " Agregar Billete, Código Nativo: " + result[0] + " Valor: " + l_Valor.ToString("C"), 3);
                                    }
                                    else
                                    {
                                        if ( !( ( l_RValor == 224 ) || ( l_RValor == 226 ) || ( l_RValor == 227 ) || ( l_RValor == 228 ) || ( l_RValor == 255 ) ) )
                                        {
                                            Globales.EscribirBitacora( "Deposito" , "Billete NO RECONOCIDO: " ,
                                                    "Denominacion desconocida: " + l_RValor.ToString( ) , 3 );
                                        }
                                        else if ( result[0] != 0 )
                                            Globales.EscribirBitacora( "FormaDeposito" , "ProcesarDeposito" ,
                                                    "Valor result no reconocido :" + result[0].ToString( ) , 3 );

                                        m_TotalRechazados++;
                                    }
                                    /* Wait the end of the previous command*/
                                    System.Threading.Thread.Sleep( 30 );

                                    EscribirResultados( );
                                    #region SCAN IMAGES
                                    if ( m_NoScanImages )
                                    {
                                        Frente = Back = IRFrente = IRBack = Mgback = MgFrente = 0;

                                        Reserved = SidLib.SID_ReadImage( (int) SidLib.NO_CLEAR_BLACK , ref Frente , ref Back , ref IRFrente , ref IRBack , ref MgFrente , ref Mgback , ref Reserved );
                                        Globales.EscribirBitacora( "Deposito" , "SID_READIMAGE: " , Reserved.ToString( ) , 3 );

                                        Reserved = SidLib.SID_SaveDIB( Frente , CrearPathDestinoFrente( "Frente.bmp" ) );
                                        Globales.EscribirBitacora( "Deposito" , "SID_DIB FRENTE: " , Reserved.ToString( ) , 3 );
                                        Reserved = SidLib.SID_SaveDIB( Back , CrearPathDestinoReverso( "Reverso.bmp" ) );
                                        Globales.EscribirBitacora( "Deposito" , "SID_DIB BACK: " , Reserved.ToString( ) , 3 );

                                        Reserved = SidLib.SID_SaveDIB( IRFrente , CrearPathDestinoFrente( "FrenteIR.bmp" ) );
                                        Globales.EscribirBitacora( "Deposito" , "SID_DIB IRFRENTE: " , Reserved.ToString( ) , 3 );
                                        Reserved = SidLib.SID_SaveDIB( IRBack , CrearPathDestinoReverso( "ReversoIR.bmp" ) );
                                        Globales.EscribirBitacora( "Deposito" , "SID_DIB IRBACK: " , Reserved.ToString( ) , 3 );

                                        if ( Frente != 0 )
                                            SidLib.SID_FreeImage( Frente );
                                        if ( Back != 0 )
                                            SidLib.SID_FreeImage( Back );
                                        if ( IRFrente != 0 )
                                            SidLib.SID_FreeImage( IRFrente );
                                        if ( IRBack != 0 )
                                            SidLib.SID_FreeImage( IRBack );
                                        if ( MgFrente != 0 )
                                            SidLib.SID_FreeImage( MgFrente );
                                        if ( Mgback != 0 )
                                            SidLib.SID_FreeImage( Mgback );
                                    }
                                    #endregion

                                    SidLib.SID_ClearDenominationNote( );
                                }
                                // -- LEON
                                l_Reply = SidLib.SID_WaitValidationResult( result );
                            }
                            SidLib.SID_ClearDenominationNote( );
                            m_NumeroBatchDeposito++;

                            //Globales.EscribirBitacora( "Deposito 10.1" );

                            // -- ERROR DEPOSITO -13
                            if ( l_Reply == SidLib.SID_INVALID_HANDLE )
                            {
                                //03/07/2017 Correcion de invalid Handle
                                SidLib.Error_Sid( l_Reply , out m_Error );
                                Globales.EscribirBitacora( "Deposito" , "ERROR en el Deposito" , m_Error , 1 );

                                System.Threading.Thread.Sleep(300);

                                // Intentamos abrir la conexion de nuevo
                               int l_auxReply =   SidLib.SID_Open( m_NoScanImages );
                                Globales.EscribirBitacora( "Deposito" , "Reintento de Conexion a DLL" , l_auxReply.ToString() , 1 );


                            }
                            // -- MANEJO ERROR -360
                            else if ( l_Reply == -360 )
                            {
                               
                                Error_Sid( l_Reply , out m_Error );
                                Globales.EscribirBitacora( "Deposito" , "ERROR en el RECEPTOR, Recomendacion apagar unos minutos" , m_Error , 1 );
                                Globales.EscribirBitacora( "Deposito de Efectivo " , "Error durante Deposito: " , l_Reply.ToString( ) , 1 );
                                m_EnDeposito = false;
                                c_BotonAceptar_Click( null , null );
                                return l_Reply;
                            }

                            //misael 08/03/2017
                            Error_Sid( l_Reply , out m_Error );
                            Globales.EscribirBitacora( "Estatus del Deposito Actual: " , l_Reply.ToString( ) , " " + m_Error , 3 );
                            if ( l_Reply<0 )
                                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable );

                            AjustarConteoParcial();
                            //01/08/2017 -222 Bolsa movida al entrar Billetes

                            if ( l_Reply == SidLib.SID_BAG_NOT_PRESENT)
                            {
                               
                                Error_Sid( l_Reply , out m_Error );
                                Globales.EscribirBitacora( "Deposito" , "Error En el Deposito: " , m_Error , 1 );
                                Globales.EscribirBitacora( "Deposito de Efectivo " , "Error durante Deposito: " , l_Reply.ToString( ) , 1 );
                                m_EnDeposito = false;
                                c_BotonAceptar_Click( null , null );
                                return l_Reply;
                            }

                            if ( l_Reply == SidLib.SID_POWER_OFF )
                            {

                                Error_Sid( l_Reply , out m_Error );
                                Globales.EscribirBitacora( "Deposito" , "Error En el Deposito: " , m_Error , 1 );
                                Globales.EscribirBitacora( "Deposito de Efectivo " , "Error durante Deposito: " , l_Reply.ToString( ) , 1 );
                                m_EnDeposito = false;
                                if ( Properties.Settings.Default.Enviar_AlertasGSI && !Globales.Alerta_Ups_entrada )
                                {
                                    UtilsComunicacion.AlertaGSI( 95 , "Entrada de UPS" );

                                    Globales.Alerta_Ups_entrada = true;

                                    Globales.UPS_CambioEstatus( Globales.EstatusReceptor.No_Operable );
                                }
                                goto CONTINUAR;
                               
                            }

                            // -- LIBERAMIENTO DE ATORAMIENTOS AUTOMATIZADO
                            if ( l_Reply < ErroresSid.Codigos.OKAY)
                            {
                                l_Reply = ProcesosSid.SensadoYManejoAtoramientosAutomatizado( Globales.m_MaxReintentosAtoramientoManual );
                                if ( ( l_Reply < ErroresSid.Codigos.OKAY )
                                    || ( Globales.Estatus == Globales.EstatusReceptor.No_Operable ) )
                                {
                                    Error_Sid( l_Reply , out m_Error );
                                    Globales.EscribirBitacora( "FormaDeposito" , "ProcesarDeposito" , "SensadoYManejoAtoramientosAutomatizado :" + m_Error + "  Oper: " + Globales.Estatus.ToString( ) , 1 );
                                    m_EnDeposito = false;
                                    c_BotonAceptar_Click(null, null);
                                    return l_Reply;
                                }

                                l_Reply = ProcesosSid.SensadoYManejoManualdePuerta( Globales.m_MaxReintentosPuertas );
                                if ( l_Reply < ErroresSid.Codigos.OKAY )
                                {
                                    Error_Sid( l_Reply , out m_Error );
                                    Globales.EscribirBitacora( "FormaDeposito" , "ProcesarDeposito" , "SensadoYManejoManualdePuerta :" + m_Error , 1 );
                                    m_EnDeposito = false;
                                    c_BotonAceptar_Click( null , null );
                                   // Close( );
                                    return l_Reply;
                                }
                                else
                                {

                                    //c_Depositar.Enabled = true;
                                    //c_BotonAceptar.Enabled = true;
                                    Globales.EscribirBitacora( "FormaDeposito" , "ProcesarDeposito" , "SensadoYManejoManualdePuerta OK" , 1 );
                                }

                            }
                            else
                            {
                                //c_Depositar.Enabled = true;
                                //c_BotonAceptar.Enabled = true;
                                Globales.EscribirBitacora( "FormaDeposito" , "ProcesarDeposito" , "SensadoYManejoManualdePuerta OK" , 1 );
                            }

                            //if(l_Reply == ErroresSid.Codigos.FEEDER_EMPTY)
                            //{
                            //    l_HayBilletes = false;
                            //    //m_TerminarDeposito = false;
                            //}

                        } while ((l_HayBilletes == true) && (m_TerminarDeposito == false));

                        CONTINUAR:
                        // JUNIO 2017 Correcion Maximo de Billetes
                        if ( l_CapacidadRestante <= 0 )
                        {
                            m_TerminarDeposito = true;
                            //c_BotonAceptar.Enabled = true;
                            //c_Depositar.Enabled = false;
                        }
                        else
                        {

                            // TEST LEON 20170207
                            //c_Depositar.Enabled = true;
                            //c_BotonAceptar.Enabled = true;
                            //pictureBox3.Enabled = true;
                            //c_Depositar.Text = "ALGO MAS";
                        }
                        Application.DoEvents();

                        if (m_TerminarDeposito)
                        {
                            m_EnDeposito = false;
                            c_BotonAceptar_Click(null, null);
                        }


                        Globales.EscribirBitacora("l_HayBilletes : " + l_HayBilletes);
                    }
                    catch (Exception ex)
                    {
                        Globales.EscribirBitacora("Deposito", "EXCEPTION", ex.Message, 1);
                    }
                    finally
                    {
                        Globales.EscribirBitacora("Deposito 16");
                        Cursor.Current = l_CursorActivo;
                        if (l_SidAbierto)
                        {
                            Globales.EscribirBitacora("Deposito 17");

                           // SidLib.SID_Reset((char)SidLib.RESET_ERROR);
                            if (!m_BanderaEnviado)
                                AjustarConteoParcial();
                          //  l_Reply = SidApi.SidLib.SID_Logout();
                         //   Globales.EscribirBitacora("FormaDeposito", "ProcesoDeposito.SID_LOgout", "logout: " + l_Reply, 3);
                            l_Reply = SidLib.SID_Close();
                        }
                    }
                }
                else
                {
                    Globales.EscribirBitacora("Deposito DEBUG <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< DEPOSITAR RANDOM");
                    //Linea Siempre comentada en Productivo
                    Llenar_Aleatorio();
                }

                EscribirResultados();
                //Globales.EscribirBitacora( "Deposito 18" );

                //if ( l_HayBilletes == true )
                //{
                //    c_BotonAceptar_Click( null, null );
                //}
                //else
                //{


                c_Depositar.Enabled = true;
                pictureBox3.Enabled = true;
                c_BotonAceptar.Text = "Terminar";
                c_BotonAceptar.Enabled = true;

                Globales.EscribirBitacora("Deposito", "Total aceptados parcial", m_TotalAceptados.ToString(), 1);
                c_Depositar.Text = "ALGO MAS";
                //}
            }
            catch (Exception E)
            {
                Globales.EscribirBitacora("FormaDeposito", "ProcesarDeposito", E.Message, 3);
            }

            return 0;
        }

        private void t_inactividad_Tick(object sender, EventArgs e)
        {
            t_inactividad.Enabled = false;
            DialogResult l_respuesta;
            Globales.EscribirBitacora( "FormaDeposito" , "Pedir mas Tiempo" ,"Pregunta ¿?", 3 );
            using (FormaError v_pregunta = new FormaError(true, "pregunta"))
            {
                v_pregunta.c_MensajeError.Text = "¿Desea mas tiempo para su transacción?";
                l_respuesta = v_pregunta.ShowDialog();
            }

            if (l_respuesta != DialogResult.OK)
            {
                Globales.EscribirBitacora( "FormaDeposito" , "Tiempo Agotado" , "SIN Respuesta" , 3 );
                c_BotonAceptar.PerformClick();

            }
            else
                t_inactividad.Enabled = true;
        }


        private void FormaDeposito_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!m_BanderaEnviado)
            {
                m_TerminarDeposito = true;
                if (m_EnDeposito)
                {
                    e.Cancel = true;
                }
                else
                {
                    c_BotonAceptar.PerformClick();
                }
            }

            Globales.EnDeposito_ahora = false;

            t_inactividad.Tick -= new System.EventHandler(this.t_inactividad_Tick);
            t_inactividad.Enabled = false;
            t_inactividad.Stop();
            t_inactividad.Dispose();

    


        }

        private void c_Depositar_EnabledChanged(object sender, EventArgs e)
        {
            if (c_Depositar.Enabled == true)
                t_inactividad.Enabled = true;
            else
                t_inactividad.Enabled = false;
        }
    }
}

