﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormFINdeposito : Azteca_SID.FormBase
    {
        public FormFINdeposito()
           
        {
            InitializeComponent();
        }

        public FormFINdeposito(bool p_cierreAutomatico) : base(p_cierreAutomatico)
        {
            InitializeComponent( );
            Activate( );
        
    }

    private void c_masdep_Click(object sender, EventArgs e)
        {
            Close();
            using (FormMasDepositos l_mas = new FormMasDepositos(true))
            {
                l_mas.ShowDialog();
               

            }
           
        }

        private void c_Terminar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }


    }
}
