﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using IrdsMensajes;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel;
using System.Net.NetworkInformation;

namespace Azteca_SID
{
    class UtilsComunicacion
    {



        public static DateTime horaLAJM = DateTime.Now;



        public static bool
          Enviar_Transaccion(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, int p_TipoTransaccion,
          Double p_TotalTransaccion, DataTable p_DetalleTransaccion, int p_Folio, bool p_Insertar, String p_IdUsuario)
        {
            horaLAJM = DateTime.Now;
            int l_numbilletes = 0;
            int contador=0;

            if (p_DetalleTransaccion != null)
            {
                foreach (DataRow fila in p_DetalleTransaccion.Rows)
                {
                    l_numbilletes += (int) fila["Cantidad"];
                }
            }



            if (p_TipoTransaccion == 1)
            {
                //Insertar Depósito
                if (p_Insertar)
                {
                    //Verificar unicidad de Deposito con la HoraSESION 
                    if ( !BDDeposito.VerificarUnicidad( Globales.FechaHoraSesion ) )
                    {
                        Globales.EscribirBitacora( "Deposito" , "Se ha detectado envio Duplicado en BD" , "Motivo Fecha hora minuto segundo Duplicado" , 1 );
                        Globales.EscribirBitacora( "Deposito" , "Cuenta: " + p_IdUsuario ," Monto Total: "+ p_TotalTransaccion.ToString("C")  , 1 );
                        UtilsComunicacion.AlertaGSI( 90 , "SID_BLOQUEO_DOBLE_REGISTRO en BD" );
                        DepositoEnCurso.EliminarTransaccionTrunca( Globales.Numero_Sesion );
                        return false;
                    }

                    // -- LEON 2016-04-06
                    decimal l_MontoActual = (decimal)BDDeposito.ObtenerMontoActual();
                    Globales.GsiDeposito = null;

                    

                    if (!BDDeposito.InsertarDeposito(p_IdCuenta, p_IdCajon, p_IdMoneda, p_TotalTransaccion
                        , p_DetalleTransaccion,horaLAJM, p_IdUsuario, out p_Folio))
                        return false;
                    else
                    {
                        DepositoEnCurso.EliminarTransaccionTrunca( Globales.Numero_Sesion );
                        try
                        {



                            BDSide.InsertarTransaccion(p_Folio, Properties.Settings.Default.NumSerialEquipo, Globales.IdUsuario, 14, horaLAJM, horaLAJM
                                    , Globales.NumeroSerieBOLSA, Int32.Parse(Globales.IdUsuario), Int32.Parse(Globales.ReferenciaUsuario.Replace("0", "")), 0
                                    , 0, "Deposito", (int)p_TotalTransaccion, l_numbilletes, 0, 0, 0, 0, "", "MXN", p_DetalleTransaccion, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

                        }
                        catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Insertar Deposito", "Escribir Transaccion SIDE/webgsi", exx.Message, 3);
                        }

                        if (Properties.Settings.Default.WebGSI)
                          try
                        {





                                if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                                {
                                    if ( String.IsNullOrEmpty( Globales.ReferenciaGSI ) )
                                        Globales.ReferenciaGSI = Globales.ConvenioCIEUsuario;

                                    if ( String.IsNullOrEmpty( Globales.ConvenioCIEUsuario ) && String.IsNullOrEmpty( Globales.ReferenciaGSI ) )
                                        Globales.ReferenciaGSI = Properties.Settings.Default.NumSerialEquipo + p_Folio.ToString( "D5" );
                                    //convenio CIE es referencia Fija

                                    //DEPOSITO WEBGSI siempre Guardado en BD ´para Reenvio
                                    if ( BDDeposito.InsetarDepositoGsi( Globales.ReferenciaUsuario , Properties.Settings.Default.GsiCajero , "MXP" , horaLAJM.ToString( "yyyy-MM-dd" ) , horaLAJM.ToString( "HH:mm:ss" ) , p_Folio.ToString( "D20" )
                                                , p_TotalTransaccion , Globales.ReferenciaGSI , Globales.NumeroSerieBOLSA , true ) )
                                        Globales.EscribirBitacora( "Insertar Deposito" , "Escribir Transaccion Webgsi" , "Guardada en BD local" , 3 );

                                    //Usuairo --GlobalesUsuario ´para Reenvio
                                    //Globales.GsiDeposito = DepositoGsi( Globales.ReferenciaUsuario , Properties.Settings.Default.GsiCajero , "MXP" , DateTime.Now.ToString( "yyyy-MM-dd" ) , DateTime.Now.ToString( "HH:mm:ss" ) , p_Folio.ToString( "D20" )
                                    //             , Globales.ReferenciaGSI , Globales.NumeroSerieBOLSA , p_DetalleTransaccion , Globales.IdUsuario );
                                    Globales.GsiDeposito= UtilsComunicacion.PagoReferenciadoGSI(p_TotalTransaccion.ToString(), "", "GSI901", "MXP", horaLAJM.ToString("yyyy-MM-dd"), horaLAJM.ToString("HH:mm:ss")
                                                               , p_Folio.ToString("D20"), Globales.ReferenciaGSI, Globales.NumeroSerieBOLSA, p_DetalleTransaccion);
                                }
                                else
                                {
                                    if ( String.IsNullOrEmpty( Globales.ReferenciaGSI ) )
                                        Globales.ReferenciaGSI = Properties.Settings.Default.NumSerialEquipo + p_Folio.ToString( "D5" );

                                    //DEPOSITO WEBGSI siempre Guardado en BD ´para Reenvio
                                    if ( BDDeposito.InsetarDepositoGsi( Globales.CuentaBanco , Properties.Settings.Default.GsiCajero , "MXP" , horaLAJM.ToString( "yyyy-MM-dd" ) , horaLAJM.ToString( "HH:mm:ss" ) , p_Folio.ToString( "D20" )
                                                , p_TotalTransaccion , Globales.ReferenciaGSI , Globales.NumeroSerieBOLSA , true ) )
                                        Globales.EscribirBitacora( "Insertar Deposito" , "Escribir Transaccion Webgsi" , "Guardada en BD local" , 3 );


                                    //Globales.GsiDeposito = DepositoGsi( Globales.CuentaBanco , Properties.Settings.Default.GsiCajero , "MXP" , DateTime.Now.ToString( "yyyy-MM-dd" ) , DateTime.Now.ToString( "HH:mm:ss" ) , p_Folio.ToString( "D20" )
                                    //             , Globales.ReferenciaGSI , Globales.NumeroSerieBOLSA , p_DetalleTransaccion ,null );
                                    Globales.GsiDeposito= UtilsComunicacion.PagoReferenciadoGSI(p_TotalTransaccion.ToString(), "", "GSI901", "MXP", horaLAJM.ToString("yyyy-MM-dd"), horaLAJM.ToString("HH:mm:ss")
                                                               , p_Folio.ToString("D20"), Globales.ReferenciaGSI, Globales.NumeroSerieBOLSA, p_DetalleTransaccion);

                                }


                            }
                            catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Insertar Deposito", "Escribir Transaccion Webgsi", exx.Message, 3);
                        }

                        #region Irds Protocolo2
                        MensajeDeposito2 l_irds = new MensajeDeposito2(Properties.Settings.Default.Estacion
       , Properties.Settings.Default.Cliente
       , BDDeposito.FolioCiclo()
       , p_Folio
       , Moneda.MonedaPesos.ToString()
       , Globales.FechaHoraSesion
       , DateTime.Now
       , Globales.IdCliente
       , Globales.NombreCliente
       , Globales.Banco
       , Globales.CuentaBanco
       , Globales.ReferenciaUsuario
       , Globales.IdUsuario
       , Globales.NombreUsuario
       , l_MontoActual
       , (decimal)p_TotalTransaccion
       , 0
       , 0
       );

                        //Form.ActiveForm.Refresh();
                        //Form.ActiveForm.Update();

                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            int l_den = 0;
                            switch ( l_Detalle[2].ToString( ) )
                            {
                                case "0.10":
                                l_den = 1;
                                break;
                                case "0.20":
                                l_den = 2;
                                break;
                                case "0.5":
                                l_den = 3;
                                break;
                                case "1":
                                l_den = 4;
                                break;
                                case "2":
                                l_den = 5;
                                break;
                                case "5":
                                l_den = 6;
                                break;
                                case "10":
                                l_den = 7;
                                break;
                                case "20.00":
                                l_den = 8;
                                break;
                                case "20":
                                l_den = 20;
                                break;
                                case "50":
                                l_den = 50;
                                break;
                                case "100":
                                l_den = 100;
                                break;
                                case "200":
                                l_den = 200;
                                break;
                                case "500":
                                l_den = 500;
                                break;
                                case "1000":
                                l_den = 1000;
                                break;

                            }

                            l_irds.AgregarDenominacion( 1 , l_den , (Int32) l_Detalle[1] ); // 10 de 100$ en contenedor 1
                        }

                        Globales.EscribirBitacora("Deposito", "Informacion", "Preparando mensaje a Portal", 1);

                        Application.DoEvents();


                        try
                        {
                            String l_Json = l_irds.CodificarJson();

                            String[] l_Servers = Properties.Settings.Default.Servers.Split(',');
                            foreach (String l_Servidor in l_Servers)
                            {
                                Globales.EscribirBitacora("Deposito", "Informacion", "Preparando Sevidor:" + l_Servidor, 1);
                                int l_IdServidor = Convert.ToInt32(l_Servidor);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Encolando a Servidor:" + l_Servidor, 1);
                                ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(l_IdServidor);
                                l_Cola.AgregarMensaje(MensajeIrds.CategoriaMensajeEnum.Transaccion
                               , MensajeIrds.TipoMensajeEnum.Transaccion_Deposito
                               , 2
                               , l_Json);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Enviando Mensajes Pendientes al Sevidor:" + l_Servidor, 1);
                                l_Cola.MandarMensajesPendientes(Properties.Settings.Default.Estacion);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Termino y Respuesta del Sevidor:" + l_Servidor, 1);
                            }
                        }
                        catch (Exception p)
                        {
                            Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", p.Message, 1);
                        }
                        #endregion

                        //if (Properties.Settings.Default.WebGSI && Globales.GsiDeposito !=null)
                        //{
                        //    if (Globales.GsiDeposito != null)
                        //    {
                        //        FormaDatosDeposito l_mostarDatos = new FormaDatosDeposito(Globales.GsiDeposito, true);

                        //        l_mostarDatos.Show();

                        //    }

                        //}

                        ModulePrint imprimir = new ModulePrint();
                        imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);

                        imprimir.AddHeaderLine("         BANCO MERCANTIL DEL NORTE, SA       ");
                        if (!Properties.Settings.Default.WebGSI )
                        {
                        imprimir.AddHeaderLine("              DEPOSITO DE EFECTIVO           ");
                        imprimir.AddHeaderLine("No. de Comprobante:   " + p_Folio);
                        imprimir.AddHeaderLine("Caja de Deposito:         " + Properties.Settings.Default.NumSerialEquipo);
                        string CuentaTmp = Globales.IdUsuario;
                        try
                        {

                            CuentaTmp = CuentaTmp.Replace(System.Environment.NewLine, "");

                            CuentaTmp = "********" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);

                        }
                        catch (Exception E)
                        {
                            //throw;
                            imprimir.AddHeaderLine("No. Cuenta:            00000000");
                        }

                        imprimir.AddHeaderLine("No. de Sesion:              " + CuentaTmp);
                        imprimir.AddHeaderLine("No. de Operacion:        " + p_Folio);
                        imprimir.AddHeaderLine("DIVISA:                       " + "MXP");
                    }else
                        {
                            try
                            {
                                String[] datos = Properties.Settings.Default.EdoCdUbi.Split('|');

                                imprimir.AddHeaderLine("Estado:         " + datos[0]);
                                imprimir.AddHeaderLine("Ciudad:         " + datos[1]);
                                imprimir.AddHeaderLine("Sucursal:      " + datos[2]);
                            
                            }
                            catch (Exception exx)
                            {
                                imprimir.AddHeaderLine("Estado:         " + "México");
                                imprimir.AddHeaderLine("Ciudad:         " + "México");
                                imprimir.AddHeaderLine("Sucursal:      " + "Suc. Banorte X");
                                Globales.EscribirBitacora("Ticket ", "Escribir Cabezera", Properties.Settings.Default.EdoCdUbi, 3);
                            }
                            //AGREGAR EXCEPCION DE COMUNICACION //
                  
                                if (Globales.GsiDeposito != null)
                            {
                                //string CuentaTmp = UtilsComunicacion.Desencriptar(Globales.GsiDeposito.cuenta);
                                //CuentaTmp = "******" + CuentaTmp.Substring(CuentaTmp.Length - 4, 4);

                                //String l_auxiliar_ref = UtilsComunicacion.Desencriptar( Globales.GsiDeposito.referencia );
                                String l_auxiliar_ref = Globales.ReferenciaGSI;
                                

                                imprimir.AddHeaderLine( "Fecha:         " + horaLAJM.ToString("yyyy-MM-dd") );
                                imprimir.AddHeaderLine( "Hora:         " + horaLAJM.ToString("HH:mm:ss"));
                                imprimir.AddHeaderLine("Id Cajero:         " + "GSI901");//UtilsComunicacion.Desencriptar(Globales.GsiDeposito.idCajero));
                                imprimir.AddHeaderLine("CR:       " + UtilsComunicacion.Desencriptar(Globales.GsiDeposito.cr));
                                if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                                    imprimir.AddHeaderLine("#Usuario:       " + Globales.IdUsuario);
                                //imprimir.AddHeaderLine("#Cuenta:         " + CuentaTmp);
                                //if ( !Properties.Settings.Default.CAJA_EMPRESARIAL )
                                //    if ( String.IsNullOrWhiteSpace( UtilsComunicacion.Desencriptar( Globales.GsiDeposito.titular ) ) )
                                //        imprimir.AddHeaderLine( "Nombre:          " + Globales.NombreUsuario );
                                //    else
                                //        imprimir.AddHeaderLine( "Nombre:          " + UtilsComunicacion.Desencriptar( Globales.GsiDeposito.titular ) );
                                imprimir.AddHeaderLine("#Secuencia:        " + p_Folio.ToString().PadLeft(20,'0'));//UtilsComunicacion.Desencriptar(Globales.GsiDeposito.secuencia));
                                if (l_auxiliar_ref .Length < 15 )
                                imprimir.AddHeaderLine("#Referencia:       " + l_auxiliar_ref );
                                else
                                {
                                    imprimir.AddHeaderLine( "#Referencia:       ");
                                    imprimir.AddHeaderLine( "      " + l_auxiliar_ref );
                                }

                                imprimir.AddHeaderLine("Movimiento:        " /*+ UtilsComunicacion.Desencriptar(Globales.GsiDeposito.movimiento) +*/ + "Depósito Referenciado");
                                imprimir.AddHeaderLine("Nombre de la empresa:           " + Globales.nombreEmisora);// Globales.nombreEmisora);
                                imprimir.AddHeaderLine("Divisa:         " + "MXP");//Desencriptar(Globales.GsiDeposito.divisa));
                                imprimir.AddHeaderLine("Importe:         " + string.Format("{0:C2}", p_TotalTransaccion) ); //Double .Parse (UtilsComunicacion.Desencriptar(Globales.GsiDeposito.importe)).ToString ("C"));
                                imprimir.AddHeaderLine("Importe Letra:         "+  enletras(p_TotalTransaccion.ToString()) + " PESOS 00/100 M.N."); //+ UtilsComunicacion.Desencriptar(Globales.GsiDeposito.importeLetra));
                                imprimir.AddHeaderLine("Clave de Rastreo:         " + UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo));
                                Globales.EscribirBitacora("Ticket ", "Escribir Clave Rastreo:", UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo), 3);
                                String l_mensajeAux = UtilsComunicacion.Desencriptar( Globales.GsiDeposito.mensaje );
                                Globales.EscribirBitacora( "Ticket " , "Escribir Clave Rastreo:" , l_mensajeAux , 3 );
                                try
                                {

                                    if ( l_mensajeAux.Contains( "Su depósito está en proceso" ) && !Properties.Settings.Default.CAJA_EMPRESARIAL )
                                    {
                                        imprimir.AddHeaderLine( " " );
                                        imprimir.AddHeaderLine( "Depósito No Acreditado, " + l_mensajeAux );
                                        imprimir.Underline1 = true;
                                    }
                                    if (Convert.ToBoolean( UtilsComunicacion.Desencriptar(Globales.GsiDeposito.exito)) == true)
                                    {
                                        BDDeposito.UpdateDeposito(p_Folio, UtilsComunicacion.Desencriptar(Globales.GsiDeposito.claveRastreo));
                                    }
                                    else
                                    {
                                        BDDeposito.UpdateDeposito(p_Folio, UtilsComunicacion.Desencriptar(Globales.GsiDeposito.mensaje));
                                    }
                                  BDDeposito.UpdateDepositoGSI( p_Folio );
                                    
                            }
                            catch (Exception p)
                            {
                                //MessageBox.Show(p.Message);
                            }
                        }else
                            {
                                imprimir.Underline1 = true;
                                imprimir.AddHeaderLine( "Fecha:         " + horaLAJM.ToString("yyyy-MM-dd") );
                                imprimir.AddHeaderLine("Hora:         " + horaLAJM.ToString("HH:mm:ss") );
                                imprimir.AddHeaderLine("Id Cajero:     " +Properties.Settings.Default.GsiCajero);
                                if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                                {
                                    imprimir.AddHeaderLine("#Usuario:       " + Globales.IdUsuario );
                                    //imprimir.AddHeaderLine( "#Cuenta:         " + "******" + Globales.ReferenciaUsuario.Substring( Globales.IdUsuario.Length - 4 , 4 ) );
                                }else
                                    //imprimir.AddHeaderLine( "#Cuenta:         " + "******" + Globales.IdUsuario.Substring( Globales.IdUsuario.Length - 4 , 4 ) );

                                imprimir.AddHeaderLine("#Secuencia:        " + p_Folio.ToString("D20").PadLeft(20,'0'));
                                imprimir.AddHeaderLine("#Referencia:       " + Globales.ReferenciaGSI);
                                imprimir.AddHeaderLine("Movimiento:        " + "Depósito Referenciado");
                                imprimir.AddHeaderLine("Nombre de la empresa:           " + Globales.nombreEmisora);// Globales.nombreEmisora);
                                imprimir.AddHeaderLine("Divisa:         " + "MXP");
                                imprimir.AddHeaderLine("Importe:         " + p_TotalTransaccion.ToString("C"));
                                if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                                {
                                   // imprimir.AddHeaderLine( "\r\n             ¡¡Depósito No Acreditado!!\r\n    Favor de Reportar inmediatamente al\r\n  teléfono de Aclaraciones BANORTE o GSI" );
                                    imprimir.BodyImage = Properties.Resources.BO_BN_2;
                                }
                                else
                                {
                                    //imprimir.AddHeaderLine( "\r\n            ¡¡Depósito No Acreditado!!\r\n       Acuda Inmediatamente a Sucursal\r\n                   con este Ticket" );
                                    imprimir.BodyImage = Properties.Resources.BO_BN;
                                }


                                Globales.EscribirBitacora("Ticket ", "Escribir Clave Rastreo", "Acuda Inmediatamente a Sucursal con este Ticket", 3);
                                try
                                {
                                    BDDeposito.UpdateDeposito( p_Folio , "GSI" + p_Folio.ToString( "D10" ) + "Pendiente" );
                                }
                                catch ( Exception p )
                                {
                                   // MessageBox.Show( p.Message );
                                }
                            }
                            

                        }
                        
                       foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            try
                            {

                                imprimir.AddItem(l_Detalle[1].ToString(), Double.Parse(l_Detalle[2].ToString()).ToString("C"),
                                    Double.Parse(l_Detalle[2].ToString()).ToString());
                            }
                            catch (Exception p)
                            {
                                //MessageBox.Show(p.Message);
                            }
                        }

                        imprimir.AddFooterLine2(l_numbilletes.ToString(),"Total Depositado: "+ p_TotalTransaccion.ToString( "C" ) );
                     
                        // imprimir.AddFooterLine("");
                        // imprimir.AddFooterLine("EMISIÓN: "+ Globales.ClienteProsegur   );
                        //  imprimir.AddFooterLine("");

                        try
                        {

                            imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.FooterImageTicket);

                        }
                        catch (Exception exx)
                        {

                        }


                        while (contador < Properties.Settings.Default.NumeroTicketsDeposito)
                        {
                            imprimir.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                            imprimir.AddFooterLine("COMPROBANTE COPIA para Usuario: " + p_IdUsuario);
                            contador++;
                        }
                       
                        imprimir.Dispose();


                  

                        Application.DoEvents();
                     //   Registrar.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Registrar.l_tipoTransaccion.Deposito, p_Folio);

                       
                    }
                }
              
            }
            else if (p_TipoTransaccion == 2)
            {
                if (p_Insertar)
                {
                    Globales.GsiRecoleccion = null;
                    //Insertar Retiro
                    if (!BDRetiro.InsertarRetiro(p_IdMoneda, p_TotalTransaccion, p_DetalleTransaccion, p_IdCajon, out p_Folio))
                        return false;
                    else
                    {
                       BDRetiro.ActualizarRetiros();
                        
                        try { 

                        BDSide.InsertarTransaccion(p_Folio, Properties.Settings.Default.NumSerialEquipo, Globales.IdUsuario, 44, horaLAJM, horaLAJM
                             , Globales.NumeroSerieBOLSAanterior, 000, 000, 0
                             , p_Folio, "Retiro", (int)p_TotalTransaccion, l_numbilletes, 0, 0, 0, 0, "", "MXN", p_DetalleTransaccion, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

                        }
                        catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Insertar Retiro", "Escribir Transaccion SIDE", exx.Message, 3);
                        }

                        if (Properties.Settings.Default.WebGSI)
                        try
                        {
                                //Guardar Retiro´para Reenvio

                                if ( BDRetiro.InsetarRetiroGsi( Properties.Settings.Default.GsiCajero , "MXP" , horaLAJM.ToString( "yyyy-MM-dd  HH:mm:ss" ) , p_Folio.ToString( "D20" )
                                               , p_TotalTransaccion , Globales.NumeroSerieBOLSAanterior ,  true  ))
                                    Globales.EscribirBitacora( "Insertar Retiro" , "Escribir Transaccion Webgsi" , "Guardada en BD local" , 3 );

                                DataTable t_Depositos_Pendientes = BDConsulta.ObtenerDepositosGSIPendientes( );
                                if ( t_Depositos_Pendientes.Rows.Count == 0 )
                                {
                                    Globales.GsiRecoleccion = RecoleccionGsi( Properties.Settings.Default.GsiCajero , "MXP"
                                                        , horaLAJM.ToString( "yyyy-MM-dd HH:mm:ss" )
                                                        , p_Folio.ToString( "D20" ) , Globales.NumeroSerieBOLSAanterior , p_DetalleTransaccion );

                                    if ( Globales.GsiRecoleccion != null )
                                        BDRetiro.UpdateRetiroGSI( p_Folio );
                                }

                               

                            }
                        catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Insertar Retiro", "Escribir Transaccion WebGsi", exx.Message, 3);
                        }

                      //  Registrar.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Registrar.l_tipoTransaccion.Retiro, p_Folio);

                       

                        if (Registrar.LeerAcumulado())
                        {
                           Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Layout creado y enviado con exito", 1);
                            
                        }
                        else
                        {

                           Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Error en la creación, o en el envio del Layout", 1);
                        }
                    }
                }
               
            }
            else if (p_TipoTransaccion == 3)
            {
                if (p_Insertar)
                {

                    if (!BDCorte.InsertarCorte(p_IdUsuario, out p_Folio))
                        return false;
                }
                //Armar cadena envío

            }
            else if (p_TipoTransaccion == 5)
            {
                //Insertar Depósito MANUAL
                if (p_Insertar)
                {
                    if (!BDDeposito.InsertarDepositoManual(p_IdCuenta, p_IdCajon, p_IdMoneda, p_TotalTransaccion
                        , p_DetalleTransaccion, horaLAJM, p_IdUsuario, out p_Folio))
                        return false;
                    else
                    {



                        ModulePrint imprimirsobre = new ModulePrint();
                        imprimirsobre.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);



                        imprimirsobre.AddHeaderLine("Depósito MANUAL de EFECTIVO");
                        imprimirsobre.AddHeaderLine("ZONA Centro");
                        imprimirsobre.AddHeaderLine("AGENCIA Centro");

                        imprimirsobre.AddHeaderLine(" ");
                        if (p_IdMoneda == 1)
                            imprimirsobre.AddHeaderLine("Moneda: Pesos");
                        else
                            imprimirsobre.AddHeaderLine("Moneda: Dólares");
                        // imprimir.AddHeaderLine(" ");
                        imprimirsobre.AddHeaderLine("Nombre:");
                        imprimirsobre.AddHeaderLine(Globales.NombreUsuario.ToUpper());

                        imprimirsobre.AddHeaderLine("Referencia: " + Globales.ReferenciaUsuario);
                        imprimirsobre.AddHeaderLine("Usuario:  " + p_IdUsuario);
                        imprimirsobre.AddHeaderLine("Convenio: " + Globales.ConevenioDEMUsuario);
                        imprimirsobre.AddHeaderLine("DECLARACION DE BILLETES POR USUARIO");

                        imprimirsobre.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToShortTimeString());
                        //  imprimir.AddHeaderLine("");
                        // imprimir.AddSubHeaderLine( "Hora: " + DateTime.Now.ToShortTimeString());
                        imprimirsobre.AddHeaderLine("");
                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            imprimirsobre.AddItem(l_Detalle[1].ToString(), Double.Parse(l_Detalle[2].ToString()).ToString("$#,###,##0.00"),
                                Double.Parse(int.Parse(l_Detalle[2].ToString()).ToString()).ToString());
                        }

                        imprimirsobre.AddFooterLine("BILLETES TOTALES:           " + l_numbilletes.ToString());

                        imprimirsobre.AddFooterLine("Total Depósito: " + p_TotalTransaccion.ToString("$#,###,###,##0.00"));
                        imprimirsobre.AddFooterLine("");
                        imprimirsobre.AddFooterLine("EMISIÓN: " + Globales.NombreCliente);
                        imprimirsobre.AddFooterLine("Folio OPERACION SOBRE: " + p_Folio);
                        imprimirsobre.AddFooterLine("NOTA: INCLUIR ESTE TICKET EN SOBRE");

                        imprimirsobre.AddFooterLine("");

                        imprimirsobre.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());


                        imprimirsobre.Dispose();

                        using (FormaError f_Error = new FormaError(false, "Sobre"))
                        {
                            f_Error.c_MensajeError.Text = "Por favor Ponga El ticket En el sobre con sus billetes y Ponga el sobre en la Ranura Indicada";
                            f_Error.ShowDialog();

                        }

                        ModulePrint imprimircopia = new ModulePrint();
                        imprimircopia.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicket);



                        imprimircopia.AddHeaderLine("Depósito MANUAL de EFECTIVO");
                        imprimircopia.AddHeaderLine("ZONA CENTRO");
                        imprimircopia.AddHeaderLine("AGENCIA CENTRO");

                        imprimircopia.AddHeaderLine(" ");
                        if (p_IdMoneda == 1)
                            imprimircopia.AddHeaderLine("Moneda: Pesos");
                        else
                            imprimircopia.AddHeaderLine("Moneda: Dólares");
                        // imprimir.AddHeaderLine(" ");
                        imprimircopia.AddHeaderLine("Nombre:");
                        imprimircopia.AddHeaderLine(Globales.NombreUsuario.ToUpper());

                        imprimircopia.AddHeaderLine("Referencia: " + Globales.ReferenciaUsuario);
                        imprimircopia.AddHeaderLine("Usuario:  " + p_IdUsuario);
                        imprimircopia.AddHeaderLine("Convenio: " + Globales.ConevenioDEMUsuario);
                        imprimircopia.AddHeaderLine("MONTO SALVO BUEN COBRO POR ETV");

                        imprimircopia.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToShortTimeString());
                        //  imprimir.AddHeaderLine("");
                        // imprimir.AddSubHeaderLine( "Hora: " + DateTime.Now.ToShortTimeString());
                        imprimircopia.AddHeaderLine("");
                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            imprimircopia.AddItem(l_Detalle[1].ToString(), Double.Parse(l_Detalle[2].ToString()).ToString("$#,###,##0.00"),
                                Double.Parse(int.Parse(l_Detalle[2].ToString()).ToString()).ToString());
                        }

                        imprimircopia.AddFooterLine("BILLETES TOTALES:           " + l_numbilletes.ToString());

                        imprimircopia.AddFooterLine("Total Depósito: " + p_TotalTransaccion.ToString("$#,###,###,##0.00"));
                        imprimircopia.AddFooterLine("");
                        imprimircopia.AddFooterLine("EMISIÓN: " + Globales.NombreCliente);
                        imprimircopia.AddFooterLine("Folio OPERACION SOBRE: " + p_Folio);
                        imprimircopia.AddFooterLine("NOTA: COPIA PARA USUARIO");

                        imprimircopia.AddFooterLine("");

                        while (contador < Properties.Settings.Default.NumeroTicketsDeposito)
                        {
                            imprimircopia.PrintTicket(Properties.Settings.Default.Impresora, DateTime.Now.ToShortTimeString());
                            contador++;
                        }


                        imprimircopia.Dispose();


                        // Form.ActiveForm.Refresh();
                        //  Form.ActiveForm.Update();
                        Application.DoEvents();

                     //  Registrar.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Registrar.l_tipoTransaccion.DepositoManual, p_Folio);
                    }
                }

            }
            else if (p_TipoTransaccion == 6)
            {
                if (p_Insertar)
                {
                    //Insertar Retiro Manual
                    if (!BDRetiro.InsertarRetiroManual(p_IdMoneda, p_TotalTransaccion, p_DetalleTransaccion, p_IdCajon, out p_Folio))
                        return false;
                    else
                    {
                        BDRetiro.ActualizarRetirosManuales();
                     //   Registrar.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Registrar.l_tipoTransaccion.RetiroManual, p_Folio);

                    }
                }

            }
            else
            {
              string  l_Alerta = p_IdCuenta;
                if (p_Insertar)
                {
                    // Insertar Mensaje
                    if (!BDAlerta.InsertarAlerta(l_Alerta, out p_Folio))
                        return false;
                }
                //Armar cadena envío
               
            }

       //     Registrar.LeerTransacciones(); 
            return true;
        }

        private static string enletras(string num)
        {
            string res, dec = "";


            Int64 entero;


            int decimales;


            double nro;


            try


            {


                nro = Convert.ToDouble(num);


            }


            catch


            {


                return "";


            }


            entero = Convert.ToInt64(Math.Truncate(nro));


            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));


            if (decimales > 0)


            {


                dec = " CON " + decimales.ToString() + "/ 100";


            }


            res = toText(Convert.ToDouble(entero)) + dec;


            return res;
        }

        private static string toText(double value)
        {
            string Num2Text = "";


            value = Math.Truncate(value);


            if (value == 0) Num2Text = "CERO";


            else if (value == 1) Num2Text = "UNO";


            else if (value == 2) Num2Text = "DOS";


            else if (value == 3) Num2Text = "TRES";


            else if (value == 4) Num2Text = "CUATRO";


            else if (value == 5) Num2Text = "CINCO";


            else if (value == 6) Num2Text = "SEIS";


            else if (value == 7) Num2Text = "SIETE";


            else if (value == 8) Num2Text = "OCHO";


            else if (value == 9) Num2Text = "NUEVE";


            else if (value == 10) Num2Text = "DIEZ";


            else if (value == 11) Num2Text = "ONCE";


            else if (value == 12) Num2Text = "DOCE";


            else if (value == 13) Num2Text = "TRECE";


            else if (value == 14) Num2Text = "CATORCE";


            else if (value == 15) Num2Text = "QUINCE";


            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);


            else if (value == 20) Num2Text = "VEINTE";


            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);


            else if (value == 30) Num2Text = "TREINTA";


            else if (value == 40) Num2Text = "CUARENTA";


            else if (value == 50) Num2Text = "CINCUENTA";


            else if (value == 60) Num2Text = "SESENTA";


            else if (value == 70) Num2Text = "SETENTA";


            else if (value == 80) Num2Text = "OCHENTA";


            else if (value == 90) Num2Text = "NOVENTA";


            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);


            else if (value == 100) Num2Text = "CIEN";


            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);


            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";


            else if (value == 500) Num2Text = "QUINIENTOS";


            else if (value == 700) Num2Text = "SETECIENTOS";


            else if (value == 900) Num2Text = "NOVECIENTOS";


            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);


            else if (value == 1000) Num2Text = "MIL";


            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);


            else if (value < 1000000)


            {


                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";


                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);


            }


            else if (value == 1000000) Num2Text = "UN MILLON";


            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);


            else if (value < 1000000000000)


            {


                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";


                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);


            }


            else if (value == 1000000000000) Num2Text = "UN BILLON";


            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);


            else


            {


                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";


                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);


            }


            return Num2Text;
        }

        public static bool
          Enviar_Transaccion_retiro(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, int p_TipoTransaccion,
          Double p_TotalTransaccion, DataTable p_DetalleTransaccion, int p_Folio, DataTable p_DetalleMonedas, Double p_TotalMonedas )
        {
            int l_numbilletes = 0;
            int l_numMonedas = 0;
            int contador = 0;

            if (p_DetalleTransaccion != null)
            {
                foreach (DataRow fila in p_DetalleTransaccion.Rows)
                {
                    l_numbilletes += (int)fila["Cantidad"];
                }
            }


            foreach (DataRow fila in p_DetalleMonedas. Rows)
            {
              DataRow  l_nueva_linea = p_DetalleTransaccion.NewRow();
                l_nueva_linea[0] = fila[0];
                l_nueva_linea[1] = fila[1];
                l_nueva_linea[2] = fila[2];
                p_DetalleTransaccion.Rows.Add(l_nueva_linea);


            }

                Globales.GsiRecoleccion = null;
                    //Insertar Retiro
                    if (!BDRetiro.InsertarRetiro(p_IdMoneda, p_TotalTransaccion +p_TotalMonedas,  p_DetalleTransaccion, p_IdCajon, out p_Folio))
                        return false;
                    else
                    {
                     BDRetiro.ActualizarRetiros();

                        try
                        {

                            BDSide.InsertarTransaccion(p_Folio, Properties.Settings.Default.NumSerialEquipo, Globales.IdUsuario, 44, horaLAJM, horaLAJM
                                 , Globales.NumeroSerieBOLSAanterior, 000, 000, 0
                                 , p_Folio, "Retiro", (int)(p_TotalTransaccion+p_TotalMonedas) , l_numbilletes, 0, 0, 0, 0, "", "MXN", p_DetalleTransaccion, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

                        }
                        catch (Exception exx)
                        {
                            Globales.EscribirBitacora("Insertar Retiro", "Escribir Transaccion SIDE", exx.Message, 3);
                        }

                        if (Properties.Settings.Default.WebGSI)
                            try
                            {


                        //Guardar Retiro´para Reenvio

                        if ( BDRetiro.InsetarRetiroGsi( Properties.Settings.Default.GsiCajero , "MXP" , horaLAJM.ToString( "yyyy-MM-dd" ) , p_Folio.ToString( "D20" )
                                       , p_TotalTransaccion + p_TotalMonedas , Globales.NumeroSerieBOLSAanterior , true ) )
                            Globales.EscribirBitacora( "Insertar Retiro" , "Escribir Transaccion Webgsi" , "Guardada en BD local" , 3 );

                        //regla de Primero Depositos luego Retiros
                        DataTable t_Depositos_Pendientes = BDConsulta.ObtenerDepositosGSIPendientes( );
                        if ( t_Depositos_Pendientes.Rows.Count == 0 )
                        {
                            Globales.GsiRecoleccion = RecoleccionGsi( Properties.Settings.Default.GsiCajero , "MXP"
                                                            , horaLAJM.ToString( "yyyy-MM-dd HH:mm:ss" )
                                                            , p_Folio.ToString( "D20" ) , Globales.NumeroSerieBOLSAanterior , p_DetalleTransaccion );

                            if ( Globales.GsiRecoleccion != null )
                                BDRetiro.UpdateRetiroGSI( p_Folio);

                        }


                        

                    }
                            catch (Exception exx)
                            {
                                Globales.EscribirBitacora("Insertar Retiro", "Escribir Transaccion WebGsi", exx.Message, 3);
                            }

                        //  Registrar.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Registrar.l_tipoTransaccion.Retiro, p_Folio);



                        //if (Registrar.LeerAcumulado())
                        //{
                        //    Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Layout creado y enviado con exito", 1);

                        //}
                        //else
                        //{

                        //    Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Error en la creación, o en el envio del Layout", 1);
                        //}
                    }
                

        
            return true;
        }

        public static String Encriptar(String l_data)
        {
            string cipherdata = l_data;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(cipherdata);

            string key = Globales.LlaveGsi;

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.CBC;
            tdes.Padding = PaddingMode.PKCS7;
            tdes.IV = new byte[tdes.BlockSize / 8];



            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            string Resutl = Convert.ToBase64String(resultArray, 0, resultArray.Length);

            return Resutl;
        }

        public static String Desencriptar(string p_mensaje)
        {
            try
            {
                string cipherString = p_mensaje;

                byte[] keyArray;
                byte[] toEncryptArray = Convert.FromBase64String(cipherString);

                string key = Globales.LlaveGsi;

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.PKCS7;
                tdes.IV = new byte[tdes.BlockSize / 8];

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                tdes.Clear();
                string Resutl = UTF8Encoding.UTF8.GetString(resultArray);

                return Resutl;
            }
            catch (Exception)
            {

                return "";

            }
        }

        public static String[] Encriptar(String[] l_data)
        {
            byte[] keyArray;

            byte[][] toEncryptArrays = new byte[l_data.Length][];

            int i = 0;
            foreach (string l_dato in l_data)
            {
                byte[] l_buffer = UTF8Encoding.UTF8.GetBytes(l_dato);
                toEncryptArrays[i] = new byte[l_buffer.Length];
                l_buffer.CopyTo(toEncryptArrays[i++], 0);

            }


            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            string key = "HG58YZ3CR9";
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.CBC;
            tdes.Padding = PaddingMode.PKCS7;
            tdes.IV = new byte[tdes.BlockSize / 8];

            String[] Resutl = new String[l_data.Length];
            i = 0;

            ICryptoTransform cTransform = tdes.CreateEncryptor();

            foreach (byte[] l_encripcion in toEncryptArrays)
            {
                byte[] resultArray = cTransform.TransformFinalBlock(l_encripcion, 0, l_encripcion.Length);
                Resutl[i++] = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }

            tdes.Clear();


            return Resutl;
        }

        public static String[] Desencriptar(string[] p_mensajes)
        {
            try
            {


                byte[] keyArray;
                byte[][] toDEncryptArrays = new byte[p_mensajes.Length][];

                int i = 0;
                foreach (string l_mensaje in p_mensajes)
                {
                    byte[] l_buffer = Convert.FromBase64String(l_mensaje);
                    toDEncryptArrays[i] = new byte[l_buffer.Length];
                    l_buffer.CopyTo(toDEncryptArrays[i++], 0);

                }

                string key = "HG58YZ3CR9";

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.PKCS7;
                tdes.IV = new byte[tdes.BlockSize / 8];

                ICryptoTransform cTransform = tdes.CreateDecryptor();

                i = 0;
                String[] Resutl = new String[p_mensajes.Length];
                foreach (byte[] l_encripcion in toDEncryptArrays)
                {
                    byte[] resultArray = cTransform.TransformFinalBlock(l_encripcion, 0, l_encripcion.Length);
                    Resutl[i++] = UTF8Encoding.UTF8.GetString(resultArray);
                }



                tdes.Clear();


                return Resutl;
            }
            catch (Exception exx)
            {
               // MessageBox.Show(exx.Message);
                return null;

            }
        }


        public static bool LoginGsi(string p_cuenta,string p_cajero, out string p_mensaje , out string p_nombre , out string p_monto )
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
    ((sender, certificate, chain, sslPolicyErrors) => true);

            try
            {

                // String l_Uri = "https://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";
                // String l_Uri = "  http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

                String l_Uri = Properties.Settings.Default.GSIWebService;

                AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion","gsi", Encriptar(Globales.UsuarioGSI+"|"+Globales.ContraseñaGsi));
                EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);

                GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient("cuentaPortSoap11", l_End);




            GsiPortal.cuentaRequest l_Req = new GsiPortal.cuentaRequest();

            l_Req.cuenta = Encriptar(p_cuenta);
            l_Req.idCajero = Encriptar(p_cajero);
          
           
               
                l_Cliente.Open();


                GsiPortal.cuentaResponse l_R = l_Cliente.cuenta(l_Req);
      

                l_Cliente.Close();

                p_mensaje = Desencriptar(l_R.mensaje);

                Globales.GSI_Bitacora( "GsiPortal" , "Login Cuenta Bancaria: " + p_cuenta , " Resultado: " + p_mensaje , 3 );

                if ( String.Equals( "ACTIVA" , Desencriptar( l_R.codigo ) ) )
                {
                    p_nombre = Desencriptar( l_R.titular );
                    p_monto = Desencriptar( l_R.limiteDeposito );
                    return true;
                }

                else
                {
                    p_monto = "";
                    p_nombre = "";
                    return false;
                }


        
              
            }
            catch (EndpointNotFoundException exx)
            {
                Globales.GSI_Bitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                Globales.EscribirBitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                p_mensaje = "Por el momento No es Posible Atenderle Por Favor acuda a la Sucursal, -No hay Endpoint Disponible-";
                p_monto = "";
                p_nombre = "";
                return false;
            }
            catch (TimeoutException exx)
            {
                Globales.GSI_Bitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                Globales.EscribirBitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                p_mensaje = "Tiempo Agotado para la Respuesta del Banco Por Favor acuda a la Sucursal,-Timeout Endpoint No Disponible- ";
                p_monto = "";
                p_nombre = "";
                return false;
            }
            catch (Exception exx)
            {
                Globales.GSI_Bitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                Globales.EscribirBitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + exx.Message, 3);
                p_mensaje = "Exepcion Encontrada:  " + exx.Message;
                p_monto = "";
                p_nombre = "";
                return false;
            }

        }

        public static GsiPortal.solDepResponse DepositoGsi(string p_cuenta, string p_idcajero,string p_divisa, string p_fecha, string p_hora
                                      ,string  p_folio,string p_referencia, string p_envase, DataTable l_detalle, String p_Usuario )
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
  ((sender, certificate, chain, sslPolicyErrors) => true);

            // String l_Uri = "http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

            String l_Uri = Properties.Settings.Default.GSIWebService;


            AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion","gsi" ,Encriptar(Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi));
            EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);
            
            GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient("cuentaPortSoap11", l_End);
            try
            {
                l_Cliente.InnerChannel.OperationTimeout = new TimeSpan( 0 , 1 , Properties.Settings.Default.TimeoutExtra );
            }catch(Exception exx )
            {
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Definiendo tiempo de Envio Deposito: " +exx.Message , 3 );
            }



            GsiPortal.solDepRequest l_solDeposito = new GsiPortal.solDepRequest();

            l_solDeposito.cuenta = Encriptar ( p_cuenta);
            l_solDeposito.idCajero = Encriptar(p_idcajero);
            l_solDeposito.divisa = Encriptar(p_divisa);
            l_solDeposito.fecha = Encriptar(p_fecha);
            l_solDeposito.hora = Encriptar(p_hora);
            l_solDeposito.numeroSecuencia = Encriptar(p_folio);
            l_solDeposito.referencia = Encriptar(p_referencia);
            l_solDeposito.folioComprobante = Encriptar(p_envase);
            l_solDeposito.ListaDenominaciones = null;

            GsiPortal.Denominacion[] lista = new GsiPortal.Denominacion[l_detalle.Rows.Count];

            int i = 0;
            foreach (DataRow l_Detalle in l_detalle.Rows)
            {
                GsiPortal.Denominacion l_denominacion = new GsiPortal.Denominacion();

                l_denominacion.cantidad =Encriptar( l_Detalle[1].ToString());
                l_denominacion.denominacion = Encriptar(l_Detalle[2].ToString());
                if ( (int) l_Detalle[0] < 29 )
                    l_denominacion.tipo = Encriptar( "B" );
                else
                    l_denominacion.tipo = Encriptar( "M" );

                lista[i++] = l_denominacion;
            }


            l_solDeposito.ListaDenominaciones = lista;

            if ( !String.IsNullOrWhiteSpace( p_Usuario ) )
                l_solDeposito.usuario = Encriptar( p_Usuario );
     


            try
            {
               
                l_Cliente.Open();



                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Enviando Deposito: "  , 3 );

                GsiPortal.solDepResponse l_depositoResponse = l_Cliente.solDep(l_solDeposito);
                
                


                l_Cliente.Close();

                Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI",  "Deposito Enviado: " + Desencriptar(l_depositoResponse.mensaje) , 3);


                return l_depositoResponse;

            }
           catch ( EndpointNotFoundException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Exepción Encontrada al enviar un Deposito " + exx.Message, 3);

                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                        f_Error.c_MensajeError.Text = "Favor de verificar este Ticket con el Administrador, -No hay Endpoint Disponible- ";
                    else
                    f_Error.c_MensajeError.Text = "Acuda Inmediatamente a la Sucursal con este Ticket, -No hay Endpoint Disponible- ";
                    f_Error.ShowDialog( );

                }

          
                return null;
            }
            catch ( TimeoutException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Exepción Encontrada al enviar un Deposito " + exx.Message, 3);
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        f_Error.c_MensajeError.Text = "Tiempo agotado para la respuesta del Servidor favor de verificar este Ticket con el Administrador, -Timeout Endpoint No Disponible- ";
                    else
                    f_Error.c_MensajeError.Text = "Tiempo Agotado para la Respuesta del Banco Por Favor Acuda inmediatamente a la Sucursal con este Ticket,-Timeout Endpoint No Disponible- ";
                    f_Error.ShowDialog( );

                }

                
                return null;
            }
            catch ( Exception exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Exepción Encontrada al enviar un Deposito " + exx.Message, 3);
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Excepcion Encontrada:  " + exx.Message;
                    f_Error.ShowDialog( );

                }

               
                return null;
            }

        }

        public static GsiPortal.getConfiguracionResponse ConfiguracionCajeroGSI( string p_idcajero )
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
   ( ( sender , certificate , chain , sslPolicyErrors ) => true );

            //String l_Uri = "http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

            String l_Uri = Properties.Settings.Default.GSIWebService;

            AddressHeader l_Head = AddressHeader.CreateAddressHeader( "autorizacion" , "gsi" , Encriptar( Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi ) );
            EndpointAddress l_End = new EndpointAddress( new Uri( l_Uri ) , l_Head );
            GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient( "cuentaPortSoap11" , l_End );

            GsiPortal.getConfiguracionRequest l_getConfig = new GsiPortal.getConfiguracionRequest() ;
            l_getConfig.idCajero =  Encriptar( p_idcajero ) ;


            try
            {

                l_Cliente.Open( );



                Globales.EscribirBitacora( "GsiPortal" , "Enviando GetConfiguracion" , "Enviando Solicitud: " , 3 );

                GsiPortal.getConfiguracionResponse l_getConfigResponse = l_Cliente.getConfiguracion (l_getConfig);


                

                l_Cliente.Close( );

                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Deposito Enviado: " + Desencriptar( l_getConfigResponse.mensaje ) , 3 );
                //Properties.Settings.Default.claveEmisora = l_getConfigResponse.listaEmisoras[0].claveEmisora;
                //Properties.Settings.Default.refUno = l_getConfigResponse.listaEmisoras[0].refUno;
                //Configuration config = ConfigurationManager.OpenExeConfiguration(@"app1.config");
                //config.AppSettings.Settings["refUno"].Value= l_getConfigResponse.listaEmisoras[0].refUno;
                //config.AppSettings.Settings["claveEmisora"].Value = l_getConfigResponse.listaEmisoras[0].claveEmisora;
                //config.AppSettings.Settings["nombreEmisora"].Value = Desencriptar(l_getConfigResponse.listaEmisoras[0].nombreEmisora);
                //config.Save(ConfigurationSaveMode.Modified);
                Globales.refUno = l_getConfigResponse.listaEmisoras[0].refUno;
                Globales.claveEmisora = l_getConfigResponse.listaEmisoras[0].claveEmisora;
                Globales.nombreEmisora = Desencriptar(l_getConfigResponse.listaEmisoras[0].nombreEmisora);
                return l_getConfigResponse;

            }
            catch ( EndpointNotFoundException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );

                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        f_Error.c_MensajeError.Text = "Favor de verificar la configuracion del cajero, -No hay Endpoint Disponible- ";
                    else
                        f_Error.c_MensajeError.Text = "Favor de verificar la configuracion del cajero, -No hay Endpoint Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( TimeoutException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        f_Error.c_MensajeError.Text = "Tiempo agotado para la respuesta del Servidor favor de verificar las configuraciones del cajero con el Administrador, -Timeout Endpoint No Disponible- ";
                    else
                        f_Error.c_MensajeError.Text = "Tiempo agotado para la respuesta del Servidor favor de verificar las configuraciones del cajero con el Administrador, -Timeout Endpoint No Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( Exception exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Excepcion Encontrada:  " + exx.Message;
                    f_Error.ShowDialog( );

                }


                return null;
            }

        }

        public static GsiPortal.pagoReferenciadoResponse PagoReferenciadoGSI(string importe, string p_idemisora, string p_idcajero , string p_divisa , string p_fecha , string p_hora
                                                                             , string p_folio, string p_referencia , string p_envase , DataTable l_detalle ) 
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
   ( ( sender , certificate , chain , sslPolicyErrors ) => true );

            //String l_Uri = "http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

            String l_Uri = Properties.Settings.Default.GSIWebService;

            AddressHeader l_Head = AddressHeader.CreateAddressHeader( "autorizacion" , "gsi" , Encriptar( Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi ) );
            EndpointAddress l_End = new EndpointAddress( new Uri( l_Uri ) , l_Head );
            GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient( "cuentaPortSoap11" , l_End );

            GsiPortal.pagoReferenciadoRequest l_Pagoref = new GsiPortal.pagoReferenciadoRequest( );

            l_Pagoref.idEmisora = Globales.claveEmisora;// Globales.claveEmisora;
            l_Pagoref.idCajero = Encriptar("GSI901");
            l_Pagoref.opcion = Encriptar( "C" );
            l_Pagoref.divisa = Encriptar("MXP");
            l_Pagoref.secuencia = Encriptar(p_folio);
            l_Pagoref.fecha = Encriptar( p_fecha );
            l_Pagoref.hora = Encriptar( p_hora );
            l_Pagoref.referenciaUno = Encriptar( p_referencia );
            l_Pagoref.folioComprobante = Encriptar(p_envase) ;
            l_Pagoref.fechaVencimiento = Encriptar(DateTime.Now.ToString("yyyy-mm-dd"));
            l_Pagoref.importe = Encriptar(importe);
            l_Pagoref.referenciaCinco = Encriptar("");
            l_Pagoref.referenciaCuatro = Encriptar("");
            l_Pagoref.referenciaDos = Encriptar("");
            l_Pagoref.referenciaTres = Encriptar("");
            l_Pagoref.listaDenominaciones = null;

            if ( l_detalle != null )
            {
                GsiPortal.Denominacion[] lista = new GsiPortal.Denominacion[l_detalle.Rows.Count];

                int i = 0;
                foreach ( DataRow l_Detalle in l_detalle.Rows )
                {
                    GsiPortal.Denominacion l_denominacion = new GsiPortal.Denominacion( );

                    l_denominacion.cantidad = Encriptar( l_Detalle[1].ToString( ) );
                    l_denominacion.denominacion = Encriptar( l_Detalle[2].ToString( ) );
                    if ( (int) l_Detalle[0] < 29 )
                        l_denominacion.tipo = Encriptar( "B" );
                    else
                        l_denominacion.tipo = Encriptar( "M" );

                    lista[i++] = l_denominacion;
                }


                l_Pagoref.listaDenominaciones = lista;
            }
            
            try
            {

                l_Cliente.Open( );



                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Enviando Deposito: " , 3 );

                GsiPortal.pagoReferenciadoResponse l_pagoResponse = l_Cliente.pagoReferenciado ( l_Pagoref );




                l_Cliente.Close( );

                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Deposito Enviado: " + Desencriptar( l_pagoResponse.mensaje ) , 3 );


                return l_pagoResponse;

            }
            catch ( EndpointNotFoundException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );

                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        f_Error.c_MensajeError.Text = "Favor de verificar este Ticket con el Administrador, -No hay Endpoint Disponible- ";
                    else
                        f_Error.c_MensajeError.Text = "Acuda Inmediatamente a la Sucursal con este Ticket, -No hay Endpoint Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( TimeoutException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        f_Error.c_MensajeError.Text = "Tiempo agotado para la respuesta del Servidor favor de verificar este Ticket con el Administrador, -Timeout Endpoint No Disponible- ";
                    else
                        f_Error.c_MensajeError.Text = "Tiempo Agotado para la Respuesta del Banco Por Favor Acuda inmediatamente a la Sucursal con este Ticket,-Timeout Endpoint No Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( Exception exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Envio DepositoGSI" , "Exepción Encontrada al enviar un Deposito " + exx.Message , 3 );
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Excepcion Encontrada:  " + exx.Message;
                    f_Error.ShowDialog( );

                }


                return null;
            }


        }

        public static GsiPortal.recoleccionResponse RecoleccionGsi(string p_idcajero, string p_divisa, string p_fecha
                                      , string p_folio, string p_envase, DataTable l_detalle)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
   ((sender, certificate, chain, sslPolicyErrors) => true);

            //String l_Uri = "http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

            String l_Uri = Properties.Settings.Default.GSIWebService;

            AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion", "gsi",Encriptar(Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi));
            EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);
            GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient("cuentaPortSoap11", l_End);




            GsiPortal.recoleccionRequest l_solRecoleccion = new GsiPortal.recoleccionRequest();


            l_solRecoleccion.idCajero = Encriptar(p_idcajero);
            l_solRecoleccion.divisa = Encriptar(p_divisa);
            l_solRecoleccion.fecha = Encriptar(p_fecha);

            l_solRecoleccion.secuencia = Encriptar(p_folio);

            l_solRecoleccion.folioComprobante = Encriptar(p_envase);
            l_solRecoleccion.ListaDenominaciones = null;
           

            GsiPortal.Denominacion[] lista = new GsiPortal.Denominacion[l_detalle.Rows.Count];

            int i = 0;
            foreach (DataRow l_Detalle in l_detalle.Rows)
            {
                GsiPortal.Denominacion l_denominacion = new GsiPortal.Denominacion();

                l_denominacion.cantidad = Encriptar(l_Detalle[1].ToString());
                l_denominacion.denominacion = Encriptar(l_Detalle[2].ToString());
                if ( (int) l_Detalle[0] < 29 )
                    l_denominacion.tipo = Encriptar( "B" );
                else
                    l_denominacion.tipo = Encriptar( "M" );

                lista[i++] = l_denominacion;
            }


            l_solRecoleccion.ListaDenominaciones = lista;






            try
            {

                l_Cliente.Open();





                GsiPortal.recoleccionResponse l_Recoleccion= l_Cliente.recoleccion(l_solRecoleccion);




                l_Cliente.Close();

                Globales.EscribirBitacora("GsiPortal", "Envio RecoleccionGSI", "Respuesta: " + Desencriptar( l_Recoleccion.codigo), 3);


                return l_Recoleccion;

            }
            catch ( EndpointNotFoundException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio RecoleccionSI" , "Excepción Encontrada: " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio RecoleccionGSI", "Excepción Encontrada: " + exx.Message, 3);

                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Acuda Inmediatamente a la Sucursal con este Ticket, -No hay Endpoint Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( TimeoutException exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio RecoleccionGSI" , "Excepción Encontrada: " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio RecoleccionGSI", "Excepción Encontrada: " + exx.Message, 3);
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Tiempo Agotado para la Respuesta del Banco Por Favor Acuda inmediatamente a la Sucursal con este Ticket,-Timeout Endpoint No Disponible- ";
                    f_Error.ShowDialog( );

                }


                return null;
            }
            catch ( Exception exx )
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Envio RecoleccionGSI" , "Excepción Encontrada: " + exx.Message , 3 );
                Globales.EscribirBitacora("GsiPortal", "Envio RecoleccionGSI", "Excepción Encontrada: " + exx.Message, 3);
                using ( FormaError f_Error = new FormaError( true , "warning" ) )
                {
                    f_Error.c_MensajeError.Text = "Excepcion Encontrada:  " + exx.Message;
                    f_Error.ShowDialog( );

                }


                return null;
            }

        }

        public static bool AlertaGSILineal (String p_Fecha, String p_IdCajero, String p_codigo,String p_desc, String p_ticket )
        {
            try
            {
                
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
     ( ( l_sender , certificate , chain , sslPolicyErrors ) => true );
                String l_Uri = Properties.Settings.Default.GSIWebService;

                AddressHeader l_Head = AddressHeader.CreateAddressHeader( "autorizacion" , "gsi" , Encriptar( Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi ) );
                EndpointAddress l_End = new EndpointAddress( new Uri( l_Uri ) , l_Head );

                GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient( "cuentaPortSoap11" , l_End );

                GsiPortal.fallasRequest l_request = new GsiPortal.fallasRequest( );

                l_request.fecha = Encriptar( p_Fecha);
                l_request.idCajero = Encriptar( p_IdCajero );
                l_request.claveFalla = Encriptar(p_codigo);
                l_request.descFalla = Encriptar( p_desc);
                l_request.ticket = Encriptar( p_ticket );



                l_Cliente.Open( );





                GsiPortal.fallasResponse l_Response = l_Cliente.fallas( l_request );




                l_Cliente.Close( );

                Globales.GSI_Bitacora( "GsiPortal" , "Alertas" , "Respuesta: " + Desencriptar( l_Response.codigo ) + " " + Desencriptar( l_Response.mensaje ) , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Respuesta: " + Desencriptar( l_Response.codigo ) + " " + Desencriptar( l_Response.mensaje ) , 3 );
                return true;
            }
            catch ( Exception exx )
            {
                if (!Properties.Settings.Default.No_Debug_Pc)
                {
                    //MessageBox.Show( "Exepcion Encontrada al enviar Alerta:  " + exx.Message );
                }
                Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar ALERTAGSI:  " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar ALERTAGSI:  " + exx.Message , 3 );

                Ping Pings = new Ping( );
                int timeout = 10000;
                PingReply l_pingreply = Pings.Send( "www.yahoo.com" , timeout );

                if ( l_pingreply.Status == IPStatus.Success )
                {
                    Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime + " milisegundos " , 3 );
                    Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime , 3 );
                }
                else
                {
                    Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime + " milisegundos " , 3 );
                    Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime , 3 );
                }
                return false;
            }
        }

        public static void KeepaliveGSI( int p_status )
        {
            Globales.GSI_Bitacora( "GsiPortal" , "Keep alive: " + DateTime.Now.ToShortDateString( ) , "Estatus a Enviar: " + p_status , 3 );

            BackgroundWorker bw_keepAlive = new BackgroundWorker( );
            bw_keepAlive.DoWork += new DoWorkEventHandler( bw_keep_DoWork );
            bw_keepAlive.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bw_keep_RunWorkerCompleted );

            //18-07-2017 STOCK Lleno x NO_operable
            if ( p_status == 6 )
                p_status = 3;
                

           if ( !(p_status == 5) )   //Comentar cuando agreguen este Estado a SIDE
                  bw_keepAlive.RunWorkerAsync( p_status );

            Globales.GSI_Bitacora( "GsiPortal" , "Keep alive: " + DateTime.Now.ToShortDateString( ) , "Hilo de comunicación Enviado: " + p_status , 3 );
        }

        public static void AlertaGSI( int p_codigo , String p_mensaje )
        {
            if ( Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {
                switch ( p_codigo )
                {
                    case 91:

                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo: MTT01 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo: MTT01  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    case 92:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo: MTT02 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo: MTT02  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    case 95:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo: UPS001 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo: UPS001  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    case 96:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo: UPS002 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo: UPS002  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    case 97:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo:  ETV001 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo:  ETV001  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    case 98:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo:  ETV002 Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo:  ETV002  Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                    default:
                    {
                        Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Codigo: " + p_codigo + " Descripcion: " + p_mensaje , 3 );
                        Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Codigo: " + p_codigo + " Descripcion: " + p_mensaje , 3 );
                    }
                    break;
                }




                BackgroundWorker bw_Falla = new BackgroundWorker( );
                bw_Falla.DoWork += new DoWorkEventHandler( bw_falla_DoWork );
                bw_Falla.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bw_falla_RunWorkerCompleted );

                bw_Falla.RunWorkerAsync( new Alerta( p_codigo , p_mensaje ) );
                Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Hilo de comunicación Enviado:" , 3 );
            }
            else
            {
                Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI Omitida" , "Codigo: " + p_codigo + " Descripcion: " + p_mensaje , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI Omitida" , "Codigo: " + p_codigo + " Descripcion: " + p_mensaje , 3 );
            }
        }

        private static void bw_keep_DoWork( object sender , DoWorkEventArgs e )
        {

            try
            {
                int p_status = (int) e.Argument;
            

            System.Net.ServicePointManager.ServerCertificateValidationCallback =
            ( ( l_sender , certificate , chain , sslPolicyErrors ) => true );

            String l_Uri = Properties.Settings.Default.GSIWebService;

            AddressHeader l_Head = AddressHeader.CreateAddressHeader( "autorizacion" , "gsi" , Encriptar( Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi ) );
            EndpointAddress l_End = new EndpointAddress( new Uri( l_Uri ) , l_Head );


            GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient( "cuentaPortSoap11" , l_End );

            GsiPortal.cajeroEstatusRequest l_request = new GsiPortal.cajeroEstatusRequest( );

            l_request.fecha = Encriptar( DateTime.Now.ToString( "yyyy-MM-dd HH:mm:ss" ) );
            l_request.estadoEquipo = Encriptar( p_status.ToString( ) );
            l_request.idCajero = Encriptar( Properties.Settings.Default.GsiCajero );


                l_Cliente.Open( );





                GsiPortal.cajeroEstatusResponse l_keepaliveResponse = l_Cliente.cajeroEstatus( l_request );




                l_Cliente.Close( );
                Globales.onLine = true;

                Globales.GSI_Bitacora( "GsiPortal" , "Keep alive" , "Respuesta: " + Desencriptar( l_keepaliveResponse.codigo ) + " " + Desencriptar( l_keepaliveResponse.mensaje ) , 3 );



            }
            catch ( Exception exx )
            {
                Globales.onLine =false;
                if ( !Properties.Settings.Default.No_Debug_Pc )
                {

                }
                    //MessageBox.Show( "Exepcion Encontrada al enviar PING:  " + exx.Message );
                Globales.GSI_Bitacora( "GsiPortal" , "Keep alive" , "Exepcion Encontrada al enviar PING:  " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "Keep alive" , "Exepcion Encontrada al enviar PING:  " + exx.Message , 3 );

                try
                {
                    Ping Pings = new Ping();
                    int timeout = 10000;
                    PingReply l_pingreply = Pings.Send("www.yahoo.com", timeout);

                    if (l_pingreply.Status == IPStatus.Success)
                    {
                        Globales.GSI_Bitacora("GsiPortal", "keepAliveGSI", "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime + " milisegundos ", 3);
                        Globales.EscribirBitacora("GsiPortal", "keepAliveGSI", "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime, 3);
                    }
                    else
                    {
                        Globales.GSI_Bitacora("GsiPortal", "keepAliveGSI", "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime + " milisegundos ", 3);
                        Globales.EscribirBitacora("GsiPortal", "keepAliveGSI", "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime, 3);
                    }
                }
                catch
                {

                }

            }
        }
        private static void bw_keep_RunWorkerCompleted( object sender , RunWorkerCompletedEventArgs e )
        {
           
        if ( !( e.Error == null ) )
            {
                Globales.EscribirBitacora( "GsiPortal" , "Keep alive" , "Exepcion Encontrada al enviar PING: Error: " + e.Error.Message ,3);
            }

            else
            {
                Globales.GSI_Bitacora( "GsiPortal" , "Keep alive" , "Hilo de comunicación terminado", 3 );
            }
        }

        private static void bw_falla_DoWork( object sender , DoWorkEventArgs e )
        {
            try
            {

                string l_aux = "";
                Alerta l_alerta = e.Argument as Alerta;
                bool l_guardar = true;

                System.Net.ServicePointManager.ServerCertificateValidationCallback =
     ( ( l_sender , certificate , chain , sslPolicyErrors ) => true );
                String l_Uri = Properties.Settings.Default.GSIWebService;

                AddressHeader l_Head = AddressHeader.CreateAddressHeader( "autorizacion" , "gsi" , Encriptar( Globales.UsuarioGSI + "|" + Globales.ContraseñaGsi ) );
                EndpointAddress l_End = new EndpointAddress( new Uri( l_Uri ) , l_Head );

                GsiPortal.cuentaPortClient l_Cliente = new GsiPortal.cuentaPortClient( "cuentaPortSoap11" , l_End );

                GsiPortal.fallasRequest l_request = new GsiPortal.fallasRequest( );

                l_request.fecha = Encriptar( DateTime.Now.ToString( "yyyy-MM-dd HH:mm:ss" ) );
                l_request.idCajero = Encriptar( Properties.Settings.Default.GsiCajero );

                // Mantenimiento MMTT01 y MTT02, UPS y ETVs
                switch ( l_alerta.Codigo )
                {
                    case 91:

                    {
                        l_request.claveFalla = Encriptar( "MTT01" );
                        l_aux = "MTT01";
                    }
                    break;
                    case 92:
                    {
                        l_request.claveFalla = Encriptar( "MTT02" );
                        l_aux = "MTT02";
                    }
                    break;
                    case 95:
                    {
                        l_request.claveFalla = Encriptar( "UPS001" );
                        l_aux = "UPS001";
                    }
                    break;
                    case 96:
                    {
                        l_request.claveFalla = Encriptar( "UPS002" );
                        l_aux = "UPS002";
                    }
                    break;
                    case 97:
                    {
                        l_request.claveFalla = Encriptar( "ETV001" );
                        l_aux = "ETV001";
                    }
                    break;
                    case 98:
                    {
                        l_request.claveFalla = Encriptar( "ETV002" );
                        l_aux = "ETV002";
                    }
                    break;
                    default:
                    {
                        l_request.claveFalla = Encriptar( l_alerta.Codigo.ToString( ) );
                        l_aux = l_alerta.Codigo.ToString( );
                        l_guardar = false;
                    }
                    break;
                }

                // Guardar Alerta para la cola de Envios
                int l_folioActual = 0;
                string ticket = DateTime.Now.ToString( "yyyyMMdd" ) + DateTime.Now.DayOfYear.ToString( "D3" ) + DateTime.Now.Hour.ToString( "D2" ) + DateTime.Now.Minute.ToString( "D2" );

                if ( l_guardar )
                    BDGsi.Insertar_Mensaje( DateTime.Now.ToString( "yyyy-MM-dd HH:mm:ss" ) , Properties.Settings.Default.GsiCajero , l_aux 
                                                            , l_alerta.Descripcion , ticket ,out l_folioActual );




                l_request.descFalla = Encriptar( l_alerta.Descripcion );
                l_request.ticket = Encriptar( ticket );



                l_Cliente.Open( );





                GsiPortal.fallasResponse l_Response = l_Cliente.fallas( l_request );




                l_Cliente.Close( );

                Globales.GSI_Bitacora( "GsiPortal" , "Alertas" , "Respuesta: " + Desencriptar( l_Response.codigo ) + " " + Desencriptar( l_Response.mensaje ) , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Respuesta: " + Desencriptar( l_Response.codigo ) + " " + Desencriptar( l_Response.mensaje ) , 3 );
                // Actualizar como enviado en la cola de Envios
                if (  l_guardar && l_folioActual != 0)
                    BDGsi.UpdateMensaje( l_folioActual , true );


            }
            catch ( Exception exx )
            {
                if ( !Properties.Settings.Default.No_Debug_Pc )
                { }
                    //MessageBox.Show( "Exepcion Encontrada al enviar Alerta:  " + exx.Message );
                Globales.GSI_Bitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar ALERTAGSI:  " + exx.Message , 3 );
                Globales.EscribirBitacora( "GsiPortal" , "AlertaGSI" , "Exepcion Encontrada al enviar ALERTAGSI:  " + exx.Message , 3 );

                Ping Pings = new Ping( );
                int timeout = 10000;
                try
                {
                    PingReply l_pingreply = Pings.Send("www.yahoo.com", timeout);
                    if (l_pingreply.Status == IPStatus.Success)
                    {
                        Globales.GSI_Bitacora("GsiPortal", "AlertaGSI", "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime + " milisegundos ", 3);
                        Globales.EscribirBitacora("GsiPortal", "AlertaGSI", "Ping a www.yahoo.com + tiempoRespuesta: " + l_pingreply.RoundtripTime, 3);
                    }
                    else
                    {
                        Globales.GSI_Bitacora("GsiPortal", "AlertaGSI", "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime + " milisegundos ", 3);
                        Globales.EscribirBitacora("GsiPortal", "AlertaGSI", "Exepcion Encontrada al enviar Ping a www.yahoo.com " + l_pingreply.RoundtripTime, 3);
                    }
                }
                catch
                {

                }              

            }


        }
        private static void bw_falla_RunWorkerCompleted( object sender , RunWorkerCompletedEventArgs e )
        {
           if ( !( e.Error == null ) )
            {
                Globales.EscribirBitacora( "GsiPortal" , "ALERTAGSI" , "Exepcion Encontrada al enviar ALERTAGSI: Error: " + e.Error.Message , 3 );
            }

            else
            {
                Globales.GSI_Bitacora( "GsiPortal" , "ALERTAGSI" , "Hilo de comunicacion terminado" , 3 );
            }
        }
        
    }
}

 class Alerta
{
    public Alerta(int p_codigo,string p_descripcion )
    {
        Codigo = p_codigo;
        Descripcion = p_descripcion;
    }
    public int Codigo { get; set; }
    public string Descripcion { get; set; }
}
