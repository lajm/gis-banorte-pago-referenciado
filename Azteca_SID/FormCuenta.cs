﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormCuenta : Azteca_SID.FormBase
    {
        public FormCuenta(bool l_timerActivo) : base(l_timerActivo)
        {
            InitializeComponent();
          //  c_cuenta.Focus();
          
        }

        public FormCuenta() : base()
        {
            InitializeComponent();
         //   c_cuenta.Focus();
          

            
        }

        String l_Identificacion = "";
        String l_llave = "00000";
        bool Validada = false;
        private void c_volver_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Oprimir_tecla(object sender, EventArgs e)
        {
           
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;

            if (c_cuenta.Text == "Ingrese su número de cuenta" || c_cuenta.Text == "Ingrese su número de Usuario")
                c_cuenta.Text = "";

            if (l_Identificacion.Length < 10)
            {

                if (sender.Equals(c_Boton0))
                {
                    l_Identificacion += "0";
                    c_cuenta.Text += "0";
                }
                else if (sender.Equals(c_Boton1))
                {
                    l_Identificacion += "1";
                    c_cuenta.Text += "1";
                }
                else if (sender.Equals(c_Boton2))
                {
                    l_Identificacion += "2";
                    c_cuenta.Text += "2";
                }
                else if (sender.Equals(c_Boton3))
                {
                    l_Identificacion += "3";
                    c_cuenta.Text += "3";
                }
                else if (sender.Equals(c_Boton4))
                {
                    l_Identificacion += "4";
                    c_cuenta.Text += "4";
                }
                else if (sender.Equals(c_Boton5))
                {
                    l_Identificacion += "5";
                    c_cuenta.Text += "5";
                }
                else if (sender.Equals(c_Boton6))
                {
                    l_Identificacion += "6";
                    c_cuenta.Text += "6";
                }
                else if (sender.Equals(c_Boton7))
                {
                    l_Identificacion += "7";
                    c_cuenta.Text += "7";
                }
                else if (sender.Equals(c_Boton8))
                {
                    l_Identificacion += "8";
                    c_cuenta.Text += "8";
                }
                else if (sender.Equals(c_Boton9))
                {
                    l_Identificacion += "9";
                    c_cuenta.Text += "9";
                }
            }

            if (sender.Equals(c_BackSpace))
            {
                if (l_Identificacion.Length != 0)
                {
                    l_Identificacion = l_Identificacion.Substring(0, l_Identificacion.Length - 1);
                    c_cuenta.Text = c_cuenta.Text.Substring(0, l_Identificacion.Length);
                }
            }
            else if (sender.Equals(c_aceptar))
            {
                Cursor.Hide( );

                if (l_Identificacion.Length > 0)
                {
                    if ( Properties.Settings.Default.CAJA_EMPRESARIAL )
                        if ( !Globales.ExisteUsuario( l_Identificacion ) )
                        {
                            using ( FormaError f_Error = new FormaError( true , "noUsuario" ) )
                            {
                                f_Error.c_MensajeError.Text = "El Usuario NO Existe";
                                f_Error.ShowDialog( );
                            }
                            Validada = false;
                        }
                        else
                        {
                            this.Visible = false;
                            using ( FormaIniciarSesion f_Inicio = new FormaIniciarSesion( true ) )
                            {

                                f_Inicio.l_IdUsuario = l_Identificacion;
                                f_Inicio.ShowDialog( FormaPrincipal.s_Forma );
                            }
                        }
                    else
                    {

                        if ( l_Identificacion.Length == 10 )
                        {
                            String l_mensajegsi;
                            String l_nombregsi;
                            Double l_montogsi;


                            if ( Validada == true )

                            {
                                if ( l_Identificacion == l_llave )
                                {
                                    c_enviando.Visible = true;
                                    //  c_accion.Visible = true;


                                    if ( Globales.ExisteUsuarioGsi( l_Identificacion , out l_mensajegsi , out l_nombregsi , out l_montogsi ))
                                    {
                                        Globales.AutenticarUsuarioGsi( l_Identificacion , l_nombregsi , l_montogsi );

                                        Globales.EscribirBitacora( "LOGIN" , " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper( ) , "EXITOSO" , 1 );
                                        if ( l_montogsi > 0 )
                                            Globales.EscribirBitacora( "LOGIN" , " NUMERO " + Globales.IdUsuario + " Limite del Depositante: " + Globales.Limite_MontoGSI.ToString( "C" ) , "EXITOSO" , 1 );
                                        this.Visible = false;

                                        DialogResult l_respuesta;

                                        using ( FormaUSUARIOgsi v_confirmar = new FormaUSUARIOgsi( true ) )
                                        {

                                            l_respuesta = v_confirmar.ShowDialog( FormaPrincipal.s_Forma );


                                        }

                                        if ( l_respuesta == DialogResult.OK )
                                        {

                                            using ( FormaVerificarcs f_verificar = new FormaVerificarcs( true ) )
                                            {

                                                f_verificar.ShowDialog( FormaPrincipal.s_Forma );


                                            }
                                        }
                                    }
                                    else
                                        using ( FormaError f_Error = new FormaError( true , "texto" ) )
                                        {
                                            f_Error.c_MensajeError.Text = l_mensajegsi;
                                            f_Error.ShowDialog( );
                                        }


                                }
                                else
                                {
                                    using ( FormaError f_Error = new FormaError( true , "texto" ) )
                                    {
                                        f_Error.c_MensajeError.Text = "No Coinciden las Cuentas, Deben ser la misma";
                                        f_Error.ShowDialog( );
                                    }

                                }
                            }
                            else
                            {

                                Validada = true;
                                l_llave = l_Identificacion;
                                l_Identificacion = "";
                                c_cuenta.Text = "";
                                c_mensajeCuenta.Text = "Ingrese Nuevamente su número de Cuenta";
                                Forzardibujado( );
                                return;
                            }
                        }
                        else
                        {

                            using ( FormaError f_Error = new FormaError( true , "texto" ) )
                            {
                                f_Error.c_MensajeError.Text = "Debe escribir una Cuenta de 10 DIGITOS para continuar";
                                f_Error.ShowDialog( );
                            }

                        }


                    }



                    Close( );
                    return;

                }

                else
                {

                    using (FormaError f_Error = new FormaError(true, "texto"))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir un numero de Empleado/Cuenta para continuar";
                        f_Error.ShowDialog();
                    }

                }
            }
          
            Reiniciartimer();
        }

        private void FormCuenta_Load(object sender, EventArgs e)
        {
            //  c_BackSpace.Focus();

            if (Properties.Settings.Default.CAJA_EMPRESARIAL)
                c_cuenta.Text = "Ingrese su número de Usuario";

            Globales.ReferenciaGSI = "";
        }
    }
}
