﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormCambiarContraseña : Azteca_SID.FormBase
    {
        public FormCambiarContraseña()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void c_contraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_contraseña.Text = l_teclado.Captura;

            }
        }

        private void c_nuevacontraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_nuevacontraseña.Text = l_teclado.Captura;

            }
        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_confirmacionContraseña.Text = l_teclado.Captura;

            }
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Cambiar_Click(object sender, EventArgs e)
        {
            if (c_nuevacontraseña.Text == c_confirmacionContraseña.Text)
            {
                if (!Globales.AutenticarUsuario(Globales.IdUsuario, c_contraseña.Text))
                {
                    using (FormaError f_Error = new FormaError(false,"alto"))
                    {
                        f_Error.c_MensajeError.Text = "Contraseña Actual  Inválida";
                        f_Error.ShowDialog();
                        return;
                    }

                }
                else
                {
                    if (!BDUsuarios.ActualizarContraseña(Globales.IdUsuario, 3, c_nuevacontraseña.Text))
                    {
                        using (FormaError f_Error = new FormaError(false,"error"))
                        {
                            f_Error.c_MensajeError.Text = "No se pudo Actualizar la contraseña";
                            f_Error.ShowDialog();
                            return;
                        }
                    }
                    else
                    {
                        using (FormaError f_Error = new FormaError(false,"exito"))
                        {
                            f_Error.c_MensajeError.Text = "Contraseña Cambiada";
                            f_Error.ShowDialog();
                            Close();
                        }
                    }

                }
            }
            else
            {
                using (FormaError f_error = new FormaError(false, "error"))
                {
                    f_error.c_MensajeError.Text = "La Confirmación de contraseña no coindice";
                    f_error.ShowDialog();
                    return;
                }
            }


        }

        private void FormCambiarContraseña_Load(object sender, EventArgs e)
        {

        }
    }
}
