﻿namespace Azteca_SID
{
    partial class Formatrancision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            this.e_mensaje = new System.Windows.Forms.Label();
            this.t_accion = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // e_mensaje
            // 
            this.e_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_mensaje.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_mensaje.Location = new System.Drawing.Point(132, 242);
            this.e_mensaje.Name = "e_mensaje";
            this.e_mensaje.Size = new System.Drawing.Size(537, 117);
            this.e_mensaje.TabIndex = 0;
            this.e_mensaje.Text = "... ESPERE POR FAVOR ...";
            this.e_mensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // t_accion
            // 
            this.t_accion.Interval = 200;
            this.t_accion.Tick += new System.EventHandler(this.t_accion_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Azteca_SID.Properties.Resources.LOGOGSI;
            this.pictureBox1.Location = new System.Drawing.Point(337, 527);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Formatrancision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte_dep;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.e_mensaje);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Formatrancision";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formatrancision";
            this.Load += new System.EventHandler(this.Formatrancision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label e_mensaje;
        private System.Windows.Forms.Timer t_accion;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}