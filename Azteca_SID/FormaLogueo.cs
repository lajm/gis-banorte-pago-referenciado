﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DepositadorService;
using SidApi;

namespace Azteca_SID
{
    public partial class FormaLogueo : FormBase
    {
        int l_intentos = 2;
        public FormaLogueo()
        {
            InitializeComponent();
        }

        private void C_Usuario_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    C_Usuario.Text = l_teclado.Captura;



            }
        }

        private void c_Contraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                   c_Contraseña.Text = l_teclado.Captura;

            }

            Forzardibujado();

            c_BotonAceptar.PerformClick();
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            l_intentos--;

            try {
                ServicioDepositador l_login = new ServicioDepositador();

                if (C_Usuario.Text.Length > 0 && c_Contraseña.Text.Length > 0)
                {
                    Globales.EscribirBitacoraAzteca("Login usuario", "Validar Usuario", C_Usuario.Text + " : "+ c_Contraseña.Text, 3);
                    l_login.ValidaEmpleado(C_Usuario.Text.ToUpper(), c_Contraseña.Text);

                    if (l_login.Respuesta)
                    {
                        Globales.EscribirBitacoraAzteca("Login usuario", "Validar Usuario", "RespuestaAzteca: " + l_login.Respuesta, 3);
                        Globales.NombreUsuario = C_Usuario.Text;
                        Globales.IdTipoUsuario = 1;
                        Globales.IdUsuario = C_Usuario.Text;
                        Globales.ReferenciaUsuario = C_Usuario.Text;
                        Globales.ConevenioDEMUsuario = "";
                        Globales.ConvenioCIEUsuario = "";

                        Globales.Banco = "Azteca";
                        Globales.CuentaBanco = "";
                        Globales.NombreCliente = "Elektra";
                        Globales.IdCliente = "";


                        Globales.FechaHoraSesion = DateTime.Now;

                        Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO", 1);

                        if (Properties.Settings.Default.No_Debug_Pc)
                        {

                            int l_Reintentos = 0;
                            try
                            {
                                do
                                {
                                    if (l_Reintentos >= 1) //mayor que cero para cambiar BIN
                                        break;
                                    l_Reply = SidLib.ResetError();
                                    l_Reintentos++;
                                } while (l_Reply != 0);
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        using (FormaDeposito f_Deposito = new FormaDeposito())
                        {


                            f_Deposito.ShowDialog();

                            Close();
                            return;
                        }


                    }
                    else
                    {
                        c_mensaje.Text = l_login.Mensaje;
                       
                        Globales.EscribirBitacoraAzteca("Login Usuario", "Validar Usuario: ", l_login.Mensaje, 3);

                    }

                }else
                {
                    using (FormaError f_Error = new FormaError(true, "texto"))
                    {
                        f_Error.c_MensajeError.Text = "Por Favor ingresa ambos campos";
                        f_Error.ShowDialog();
                             

                    }
                }
            }catch (Exception exxc)
            {
                Globales.EscribirBitacoraAzteca("Login usuario", "Validar Usuario","Execpcion Encontrada: "+ exxc.Message ,3);
            }

            if (l_intentos< 0)
                using (FormaError f_Error = new FormaError(true, "NoUsuario"))
                {
                    f_Error.c_MensajeError.Text = "Por Favor intente mas Tarde";
                    f_Error.ShowDialog();


                    Close();
                   
                }

        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Contraseña_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
