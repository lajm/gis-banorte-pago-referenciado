﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    class BDRegistroAzteca
    {
        public static DataTable
   ObtenerRegistro(int p_folio)
        {
            String l_Query
                = "SELECT               IdDeposito, Fecha , Monto , Divisa , Usuario ,WorkStation ,NumRegistro"
                + "FROM                 RegistroAzteca "
                + "WHERE                Folio = @IdFolio ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdFolio", p_folio);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Pendientes");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }



        public static DataTable
            BuscarARegistrosPendientes()
        {
            String l_Query
                = "SELECT              IdDeposito, Fecha , Monto , Divisa , Usuario ,WorkStation "
                + "FROM                 RegistroAzteca "
                + "WHERE                NumRegistro = -1 "
                + "ORDER BY             IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Pendientes");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

       
        public static bool
            ActualizarRegistro(int p_Folio, int  p_NumRegistro)
        {
            String l_Query
                = "UPDATE           RegistroAzteca "
                + "SET              NumRegistro = @NumRegistro, "
                 + "                DescripcionOnError = 'OK' "
                + "WHERE            IdDeposito = @Folio ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@NumRegistro", p_NumRegistro);
                l_Comando.Parameters.AddWithValue("@Folio", p_Folio);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
         ActualizarRegistro(int p_Folio, string p_Descripcion)
        {
            String l_Query
                = "UPDATE           RegistroAzteca "
                + "SET              NumRegistro = -1, "
                + "                 DescripcionOnError = @Descripcion "
                + "WHERE            IdDeposito = @Folio ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Descripcion", p_Descripcion);
                l_Comando.Parameters.AddWithValue("@Folio", p_Folio);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            InsertarRegistro(int p_folio, DateTime p_fecha, decimal p_monto, int p_divisa, string p_usuario, string p_workstation)
        {
           
            String l_Query
                = "INSERT INTO          RegistroAzteca "
                + "                     (IdDeposito, Fecha, Monto, Divisa, Usuario, WorkStation, NumRegistro) "
                + "VALUES               (@IdDeposito, @Fecha, @Monto, @Divisa, @Usuario, @WorkStation,-1) ";


           
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_folio);
                l_Comando.Parameters.AddWithValue("@Fecha", p_fecha);
                l_Comando.Parameters.AddWithValue("@Monto", p_monto);
                l_Comando.Parameters.AddWithValue("@Divisa", p_divisa);
                l_Comando.Parameters.AddWithValue("@Usuario", p_usuario);
                l_Comando.Parameters.AddWithValue("@Workstation", p_workstation);

                Globales.EscribirBitacoraAzteca("Insertar Deposito", "Escribir Deposito Azteca", "Ejecutando Query en BD", 3);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    Globales.EscribirBitacoraAzteca("Insertar Deposito", "Escribir Deposito Azteca", "Error de Ejecucion", 3);
                    l_Transaccion.Rollback();
                    return false;
                   
                }
                else
                {
                    l_Transaccion.Commit();
                    Globales.EscribirBitacoraAzteca("Insertar Deposito", "Escribir Deposito Azteca", "Exitoso", 3);
                    return true;
                }
            }
        }
    }

    public class RespuestaAzteca
    {
        int m_Cod;
        string m_DesCod;

        public RespuestaAzteca()
        {

        }
        public int Cod
        {
            get
            {
                return m_Cod;
            }

            set
            {
                m_Cod = value;
            }
        }

        public string DesCod
        {
            get
            {
                return m_DesCod;
            }

            set
            {
                m_DesCod = value;
            }
        }

        
    }
}
