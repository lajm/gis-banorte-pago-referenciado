﻿namespace Azteca_SID
{
    partial class FormaFueraServicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.e_mensaje_gral = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.e_look = new System.Windows.Forms.Label();
            this.e_Puerta = new System.Windows.Forms.Label();
            this.e_bolsa = new System.Windows.Forms.Label();
            this.e_red = new System.Windows.Forms.Label();
            this.e_tapa = new System.Windows.Forms.Label();
            this.e_Atasco = new System.Windows.Forms.Label();
            this.c_login_ETV = new System.Windows.Forms.Button();
            this.c_login_Admin = new System.Windows.Forms.Button();
            this.c_actualizar = new System.Windows.Forms.Button();
            this.e_porcentaje = new System.Windows.Forms.Label();
            this.e_papel = new System.Windows.Forms.Label();
            this.e_dipshiwtch = new System.Windows.Forms.Label();
            this.e_envase = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.btn_banorte_volver;
            this.pictureBox3.Location = new System.Drawing.Point(0, 528);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(127, 49);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // e_mensaje_gral
            // 
            this.e_mensaje_gral.BackColor = System.Drawing.Color.Transparent;
            this.e_mensaje_gral.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_mensaje_gral.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_mensaje_gral.Location = new System.Drawing.Point(6, 192);
            this.e_mensaje_gral.Name = "e_mensaje_gral";
            this.e_mensaje_gral.Size = new System.Drawing.Size(788, 62);
            this.e_mensaje_gral.TabIndex = 5;
            this.e_mensaje_gral.Text = "NO DISPONIBLE";
            this.e_mensaje_gral.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::Azteca_SID.Properties.Resources.process_stop_3;
            this.pictureBox4.Location = new System.Drawing.Point(322, 80);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(168, 113);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // e_look
            // 
            this.e_look.BackColor = System.Drawing.Color.Transparent;
            this.e_look.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_look.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_look.Location = new System.Drawing.Point(12, 326);
            this.e_look.Name = "e_look";
            this.e_look.Size = new System.Drawing.Size(315, 55);
            this.e_look.TabIndex = 7;
            this.e_look.Text = "Lock";
            this.e_look.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_Puerta
            // 
            this.e_Puerta.BackColor = System.Drawing.Color.Transparent;
            this.e_Puerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_Puerta.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_Puerta.Location = new System.Drawing.Point(473, 254);
            this.e_Puerta.Name = "e_Puerta";
            this.e_Puerta.Size = new System.Drawing.Size(315, 55);
            this.e_Puerta.TabIndex = 8;
            this.e_Puerta.Text = "Bovedad";
            this.e_Puerta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_bolsa
            // 
            this.e_bolsa.BackColor = System.Drawing.Color.Transparent;
            this.e_bolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_bolsa.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_bolsa.Location = new System.Drawing.Point(473, 326);
            this.e_bolsa.Name = "e_bolsa";
            this.e_bolsa.Size = new System.Drawing.Size(315, 55);
            this.e_bolsa.TabIndex = 9;
            this.e_bolsa.Text = "Bolsa";
            this.e_bolsa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_red
            // 
            this.e_red.BackColor = System.Drawing.Color.Transparent;
            this.e_red.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_red.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_red.Location = new System.Drawing.Point(244, 286);
            this.e_red.Name = "e_red";
            this.e_red.Size = new System.Drawing.Size(315, 55);
            this.e_red.TabIndex = 10;
            this.e_red.Text = "RED";
            this.e_red.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.e_red.Click += new System.EventHandler(this.e_red_Click);
            // 
            // e_tapa
            // 
            this.e_tapa.BackColor = System.Drawing.Color.Transparent;
            this.e_tapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_tapa.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_tapa.Location = new System.Drawing.Point(12, 254);
            this.e_tapa.Name = "e_tapa";
            this.e_tapa.Size = new System.Drawing.Size(314, 55);
            this.e_tapa.TabIndex = 11;
            this.e_tapa.Text = "TAPA";
            this.e_tapa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_Atasco
            // 
            this.e_Atasco.BackColor = System.Drawing.Color.Transparent;
            this.e_Atasco.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_Atasco.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_Atasco.Location = new System.Drawing.Point(244, 468);
            this.e_Atasco.Name = "e_Atasco";
            this.e_Atasco.Size = new System.Drawing.Size(315, 55);
            this.e_Atasco.TabIndex = 12;
            this.e_Atasco.Text = "Atasco";
            this.e_Atasco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_login_ETV
            // 
            this.c_login_ETV.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_login_ETV.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_login_ETV.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_login_ETV.Location = new System.Drawing.Point(549, 437);
            this.c_login_ETV.Name = "c_login_ETV";
            this.c_login_ETV.Size = new System.Drawing.Size(190, 62);
            this.c_login_ETV.TabIndex = 13;
            this.c_login_ETV.Text = "ETV";
            this.c_login_ETV.UseVisualStyleBackColor = true;
            this.c_login_ETV.Visible = false;
            this.c_login_ETV.Click += new System.EventHandler(this.c_login_ETV_Click);
            // 
            // c_login_Admin
            // 
            this.c_login_Admin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_login_Admin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_login_Admin.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_login_Admin.Location = new System.Drawing.Point(62, 437);
            this.c_login_Admin.Name = "c_login_Admin";
            this.c_login_Admin.Size = new System.Drawing.Size(190, 62);
            this.c_login_Admin.TabIndex = 14;
            this.c_login_Admin.Text = "Administrador";
            this.c_login_Admin.UseVisualStyleBackColor = true;
            this.c_login_Admin.Click += new System.EventHandler(this.c_login_Admin_Click);
            // 
            // c_actualizar
            // 
            this.c_actualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_actualizar.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_actualizar.Location = new System.Drawing.Point(298, 505);
            this.c_actualizar.Name = "c_actualizar";
            this.c_actualizar.Size = new System.Drawing.Size(209, 56);
            this.c_actualizar.TabIndex = 15;
            this.c_actualizar.Text = "Actualizar";
            this.c_actualizar.UseVisualStyleBackColor = true;
            this.c_actualizar.Click += new System.EventHandler(this.c_actualizar_Click);
            // 
            // e_porcentaje
            // 
            this.e_porcentaje.BackColor = System.Drawing.Color.Transparent;
            this.e_porcentaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_porcentaje.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_porcentaje.Location = new System.Drawing.Point(545, 387);
            this.e_porcentaje.Name = "e_porcentaje";
            this.e_porcentaje.Size = new System.Drawing.Size(216, 55);
            this.e_porcentaje.TabIndex = 17;
            this.e_porcentaje.Text = "% de Cupo:  ";
            this.e_porcentaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.e_porcentaje.Visible = false;
            // 
            // e_papel
            // 
            this.e_papel.BackColor = System.Drawing.Color.Transparent;
            this.e_papel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_papel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_papel.Location = new System.Drawing.Point(298, 358);
            this.e_papel.Name = "e_papel";
            this.e_papel.Size = new System.Drawing.Size(261, 55);
            this.e_papel.TabIndex = 18;
            this.e_papel.Text = "PAPEL";
            this.e_papel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_dipshiwtch
            // 
            this.e_dipshiwtch.BackColor = System.Drawing.Color.Transparent;
            this.e_dipshiwtch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_dipshiwtch.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_dipshiwtch.Location = new System.Drawing.Point(11, 387);
            this.e_dipshiwtch.Name = "e_dipshiwtch";
            this.e_dipshiwtch.Size = new System.Drawing.Size(315, 55);
            this.e_dipshiwtch.TabIndex = 19;
            this.e_dipshiwtch.Text = "DipShiwtch";
            this.e_dipshiwtch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_envase
            // 
            this.e_envase.BackColor = System.Drawing.Color.Transparent;
            this.e_envase.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_envase.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.e_envase.Location = new System.Drawing.Point(282, 422);
            this.e_envase.Name = "e_envase";
            this.e_envase.Size = new System.Drawing.Size(273, 55);
            this.e_envase.TabIndex = 20;
            this.e_envase.Text = "Envase";
            this.e_envase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormaFueraServicio
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte_dep;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.ControlBox = false;
            this.Controls.Add(this.e_papel);
            this.Controls.Add(this.e_red);
            this.Controls.Add(this.c_login_Admin);
            this.Controls.Add(this.c_login_ETV);
            this.Controls.Add(this.c_actualizar);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.e_mensaje_gral);
            this.Controls.Add(this.e_dipshiwtch);
            this.Controls.Add(this.e_porcentaje);
            this.Controls.Add(this.e_bolsa);
            this.Controls.Add(this.e_Puerta);
            this.Controls.Add(this.e_look);
            this.Controls.Add(this.e_tapa);
            this.Controls.Add(this.e_Atasco);
            this.Controls.Add(this.e_envase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormaFueraServicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormaFueraServicio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label e_mensaje_gral;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label e_look;
        private System.Windows.Forms.Label e_Puerta;
        private System.Windows.Forms.Label e_bolsa;
        private System.Windows.Forms.Label e_red;
        private System.Windows.Forms.Label e_tapa;
        private System.Windows.Forms.Label e_Atasco;
        private System.Windows.Forms.Button c_login_ETV;
        private System.Windows.Forms.Button c_login_Admin;
        private System.Windows.Forms.Button c_actualizar;
        private System.Windows.Forms.Label e_porcentaje;
        private System.Windows.Forms.Label e_papel;
        private System.Windows.Forms.Label e_dipshiwtch;
        private System.Windows.Forms.Label e_envase;
    }
}
