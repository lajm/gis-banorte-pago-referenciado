﻿namespace Azteca_SID
{
    partial class FormaAltaMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_vendor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_nombre = new System.Windows.Forms.TextBox();
            this.c_registar = new System.Windows.Forms.Button();
            this.c_tipoMantenimiento = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_vendor
            // 
            this.c_vendor.DisplayMember = "1";
            this.c_vendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_vendor.FormattingEnabled = true;
            this.c_vendor.Items.AddRange(new object[] {
            "TRANTOR",
            "SYMETRY",
            "JYKEN",
            "OTRO"});
            this.c_vendor.Location = new System.Drawing.Point(315, 245);
            this.c_vendor.Name = "c_vendor";
            this.c_vendor.Size = new System.Drawing.Size(230, 32);
            this.c_vendor.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(237, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(327, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Registro de Mantenimiento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(110, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "Empresa Asociada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(100, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Nombre del Tecnico";
            // 
            // c_nombre
            // 
            this.c_nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nombre.Location = new System.Drawing.Point(317, 172);
            this.c_nombre.Name = "c_nombre";
            this.c_nombre.Size = new System.Drawing.Size(418, 29);
            this.c_nombre.TabIndex = 8;
            this.c_nombre.Click += new System.EventHandler(this.c_nombre_Click);
            // 
            // c_registar
            // 
            this.c_registar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_registar.Location = new System.Drawing.Point(295, 423);
            this.c_registar.Name = "c_registar";
            this.c_registar.Size = new System.Drawing.Size(211, 52);
            this.c_registar.TabIndex = 9;
            this.c_registar.Text = "Registar";
            this.c_registar.UseVisualStyleBackColor = true;
            this.c_registar.Click += new System.EventHandler(this.c_registar_Click);
            // 
            // c_tipoMantenimiento
            // 
            this.c_tipoMantenimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_tipoMantenimiento.FormattingEnabled = true;
            this.c_tipoMantenimiento.Items.AddRange(new object[] {
            "Mantenimiento Preventivo",
            "Mantenimiento Correctivo",
            "Cambio de Refacción/pieza",
            "Cambio de Modulo Validador",
            "Cambio de Bandas",
            "Visita Diagnostico",
            "Otro"});
            this.c_tipoMantenimiento.Location = new System.Drawing.Point(315, 308);
            this.c_tipoMantenimiento.Name = "c_tipoMantenimiento";
            this.c_tipoMantenimiento.Size = new System.Drawing.Size(420, 32);
            this.c_tipoMantenimiento.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(100, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tipo Mantenimiento";
            // 
            // FormaAltaMantenimiento
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.c_tipoMantenimiento);
            this.Controls.Add(this.c_registar);
            this.Controls.Add(this.c_nombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_vendor);
            this.Name = "FormaAltaMantenimiento";
            this.Load += new System.EventHandler(this.FormaAltaMantenimiento_Load);
            this.Controls.SetChildIndex(this.c_vendor, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.c_nombre, 0);
            this.Controls.SetChildIndex(this.c_registar, 0);
            this.Controls.SetChildIndex(this.c_tipoMantenimiento, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox c_vendor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_nombre;
        private System.Windows.Forms.Button c_registar;
        private System.Windows.Forms.ComboBox c_tipoMantenimiento;
        private System.Windows.Forms.Label label4;
    }
}
