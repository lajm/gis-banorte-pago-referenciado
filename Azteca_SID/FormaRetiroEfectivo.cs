﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaRetiroEfectivo : Azteca_SID.FormBase
    {
        DataTable l_Retiro;
        DataTable l_RetiroMonedas;
        int lRet;
        Double l_TotalPesos = 0;
        Double l_TotalBolsa = 0;
        Double l_TotalDolares = 0;
        String l_Equipo = "Equipo1";
        bool l_Equipo1_vacio, l_Equipo2_vacio;
        DataTable l_Datos;
        DataTable l_DatosMonedas;
        string l_bolsaAnterior;
        int m_porcentajeAnterior;
        ModulePrint l_auxGuardado;
        bool RetiroMonedas = false;
        bool realizarRetiro_forzado = false;
        // # MONEDAS  TRAER Datos
        int l_total_num_monedas = 0;
       

        //total efectivo monedas
        Double l_TotalMoneadas = 0;

        public FormaRetiroEfectivo( bool p_Forzado )
        {
            InitializeComponent( );
            realizarRetiro_forzado = p_Forzado;
        }
        public FormaRetiroEfectivo( )
        {
            InitializeComponent( );
        }

        private void c_BotonAceptar_Click( object sender , EventArgs e )
        {
            Cursor.Show( );
            Cursor.Show( );
            Cursor.Current = Cursors.WaitCursor;
            DialogResult l_respuesta = DialogResult.None;
            bool Exitoso = true;

            c_BotonAceptar.Enabled = false;
            //  Globales.CambiaEquipo(l_Equipo);

            //UtilsComunicacion.MantenerVivo();

            Globales.LeerBolsaPuesta( );
            l_bolsaAnterior = Globales.NumeroSerieBOLSA;
            Globales.NumeroSerieBOLSAanterior = l_bolsaAnterior;
            //  LeerPorcentaje(out Exitoso);
            m_porcentajeAnterior = 100;

          

            if ( Exitoso )
                ImprimirPesos( );




            // ImprimirDolares();
            Cursor.Hide( );





            if ( Properties.Settings.Default.No_Debug_Pc && !realizarRetiro_forzado )
            {
                Cursor.Show( );
                Cursor.Show( );
                Cursor.Current = Cursors.WaitCursor;

                Byte[] key = new Byte[1];
                Byte[] sensor = new Byte[64];

                if ( Exitoso )
                {

                    using ( FormaError f_Error = new FormaError( true , true , false , "alimentar" ) )
                    {
                        f_Error.c_MensajeError.Text = "Ponga El TICKET en el alimentador y presione Aceptar";
                        DialogResult l_pideTicket = f_Error.ShowDialog( );

                        if ( l_pideTicket == DialogResult.Cancel )
                            return;
                    }

                    try
                    {

                        SidLib.ResetError( );

                        lRet = SidLib.SID_Open( true );

                        if ( -215 <= lRet && lRet <= -199 )
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable);
                            Error_Sid( lRet , out l_Error );
                            Globales.EscribirBitacora( "EnableDeposit" , "Atasco de Billetes::" , l_Error , 1 );


                            using ( FormaError f_Error = new FormaError( false , "atasco" ) )
                            {
                                f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                                f_Error.ShowDialog( );
                                SidLib.ResetError( );
                                SidLib.SID_Close( );
                                c_BotonAceptar.Enabled = true;

                                // c_BotonCancelar_Click(this, null);
                            }
                            try
                            {
                                SidLib.ResetPath( );
                            }
                            catch
                            {
                            }
                            return;

                        }

                        if ( -246 <= lRet && lRet <= -239 )
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable);
                            Error_Sid( lRet , out l_Error );
                            Globales.EscribirBitacora( "EnableDeposit" , "Problema DIP_SWITCH::" , l_Error , 1 );


                            using ( FormaError f_Error = new FormaError( false , "switch" ) )
                            {
                                f_Error.c_MensajeError.Text = l_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                                f_Error.ShowDialog( );
                                SidLib.ResetError( );
                                SidLib.SID_Close( );
                                c_BotonAceptar.Enabled = true;
                                return;
                                // c_BotonCancelar_Click(this, null);
                            }



                        }



                        lRet = SidLib.SID_UnitStatus( key , sensor , null , null , null );
                        if ( lRet < 0 )
                        {
                            Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable);
                            Error_Sid( lRet , out l_Error );
                        }
                        Globales.EscribirBitacora( "RetiroEfectivo" , "UnitStatus" , lRet.ToString( ) , 1 );
                        Globales.EscribirBitacora( "UnitStatus" , "sensor[7]" , sensor[7].ToString( ) , 1 );

                        //versiones anterioes 4ta g // valor 20 5ta generacion con perno //  valor 4 5ta generacion sin perno 
                        if ( sensor[7] == 148 || sensor[7] == 20 || sensor[7] == 4 )
                        {
                            Globales.EscribirBitacora( "CashIn" , "sensor[7]" , sensor[7].ToString( ) , 1 );
                            //  lRet = SidLib.SID_GetCashTotalBag(l_TotalBag);
                            // foreach (ushort billetes in l_totalNotes)
                            // c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;

                            // lRet = SidLib.SID_CashIn(0, 1, 0, false, 0, 0);/*Capture the notes */
                            do
                            {
                                SidLib.Error_Sid( lRet , out l_Error );
                                Globales.EscribirBitacora( "Cash" , "sensor[7]" , sensor[7].ToString( ) , 1 );

                                lRet = SidLib.SID_CashIn( 0 , 1 , 0 , false , 0 , 0 );/*Capture the notes */
                                if ( lRet == 1 )
                                    using ( FormaError f_Error = new FormaError( false , "alimentar" ) )
                                    {
                                        SidLib.Error_Sid( lRet , out l_Error );
                                        Globales.EscribirBitacora( "CashIN" , "Feedervacio" , l_Error , 1 );
                                        f_Error.c_MensajeError.Text = "Ponga El TICKET en el alimentador y presione Aceptar";
                                        f_Error.ShowDialog( );
                                    }


                            } while ( lRet == 1 );


                            short[] result = new short[1];
                            lRet = SidLib.SID_WaitValidationResult( result );

                            while ( lRet == SidLib.SID_PERIF_BUSY )
                            {

                                // c_Respuesta.Text += "\r\n" + result[0].ToString("X2");
                                // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");
                                if ( result[0] != 0 )
                                {
                                    Globales.EscribirBitacora( "RetiroEfectivo" , "waitValidation" , lRet.ToString( ) , 1 );
                                    lRet = SidLib.SID_ClearDenominationNote( ); /* Clear the type of note recognized*/
                                    Globales.EscribirBitacora( "RetiroEfectivo" , "ClearDenomination" , lRet.ToString( ) , 1 );
                                }

                                lRet = SidLib.SID_WaitValidationResult( result );
                            }

                            SidLib.ResetError( );

                            SidLib.SID_Close( );
                        }


                        SidLib.ResetError( );

                        SidLib.SID_Close( );

                    }
                    catch ( Exception ex )
                    {
                        Globales.EscribirBitacora( "Intro Ticket" , "cashIn o waitvalidation" , ex.Message , 1 );
                    }

                    using ( FormaCambioBolsa f_bolsa = new FormaCambioBolsa( ) )
                    {
                        f_bolsa.Refresh( );

                        l_respuesta = f_bolsa.ShowDialog( );

                        if ( l_respuesta == DialogResult.Abort )

                            using ( FormaError f_Error = new FormaError( false , "warning" ) )
                            {
                                f_Error.c_MensajeError.Text = "Error en el procedimiento de Cambio de Bolsa Intente de nuevo";
                                f_Error.ShowDialog( );
                                c_BotonAceptar.Enabled = true;
                                return;
                            }

                    }
                }
                else

                    using ( FormaCambioBolsa f_bolsa = new FormaCambioBolsa( ) )
                    {
                        f_bolsa.Refresh( );

                        l_respuesta = f_bolsa.ShowDialog( );

                        if ( l_respuesta == DialogResult.Abort )

                            using ( FormaError f_Error = new FormaError( false , "warning" ) )
                            {
                                f_Error.c_MensajeError.Text = "SEGUNDO Error en el procedimiento de Cambio de Bolsa comuniquese con el Administrador";
                                f_Error.ShowDialog( );
                                return;

                            }

                        l_respuesta = DialogResult.Retry;

                    }
            }
            else
            {

                if ( !RevisarBolsaRetiro())
                {
                    MostarMensaje( "La Bolsa no coincide Con la BOLSA de los Depositos Realizados, o hay mas de una Bolsa por retirar" , false , "warning" );
                    l_respuesta = DialogResult.Abort;

                }
                else
                {

                    //Caso ideal de Operacion //
                    l_respuesta = DialogResult.OK;
                    Globales.BolsaCorrecta = true;
                }

            }



            Refresh( );
            Application.DoEvents( );


            if ( l_respuesta == DialogResult.OK )
            {
                UtilsComunicacion.Enviar_Transaccion_retiro( "" , 1 , 1 , 2 , Double.Parse( c_TotalRetiroEfectivo.Text.Replace( "," , "" ).Replace( "$" , "" ) ) ,
                l_Retiro , 0 ,l_RetiroMonedas,l_TotalMoneadas );

                ImprimirPesos( Globales.BolsaCorrecta , true );


                //if ( Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                //{
                //    UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );

                //    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                //}

                if (RetiroMonedas && l_DatosMonedas.Rows.Count >0)
                {

                    using (FormaRetiroMonedas l_retiroM = new FormaRetiroMonedas(l_DatosMonedas))
                    {
                        l_retiroM.ShowDialog();

                        //MostarMensaje("Ticket Retiro de Monedas ... Por favor Abra y vacie las Bolsas de MONEDAS, cuando termine, cierre con llave y Presione Aceptar", false,"precaucion");

                        RetiroMonedas = false;

                    }



                }


                Globales.BorrarRetiroPendiente( );

                DialogResult l_result;

                
                using (FormaFueraServicio l_chek = new FormaFueraServicio( true , realizarRetiro_forzado ) )
                {
                   l_result= l_chek.ShowDialog();
                }

               
                UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );
                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );

            }

            if ( l_respuesta == DialogResult.Ignore )
            {
                UtilsComunicacion.Enviar_Transaccion_retiro("", 1, 1, 2, Double.Parse(c_TotalRetiroEfectivo.Text.Replace(",", "").Replace("$", "")),
                l_Retiro, 0, l_RetiroMonedas, l_TotalMoneadas);
                ImprimirPesos( Globales.BolsaCorrecta , false );

                if (RetiroMonedas && l_DatosMonedas.Rows.Count > 0)
                {

                    using (FormaRetiroMonedas l_retiroM = new FormaRetiroMonedas(l_DatosMonedas))
                    {
                        l_retiroM.ShowDialog();

                        //MostarMensaje("Ticket Retiro de Monedas ... Por favor Abra y vacie las Bolsas de MONEDAS, cuando termine, cierre con llave y Presione Aceptar", false,"precaucion");

                        RetiroMonedas = false;

                    }



                }

                if ( Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( 0 , "SID_OK" );

                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }


                using (FormaFueraServicio l_chek = new FormaFueraServicio(true , realizarRetiro_forzado ) )
                {
                    l_chek.ShowDialog();
                }



                Globales.CambioEstatusReceptor( Globales.EstatusReceptor.No_Operable);

            }
            if ( l_respuesta == DialogResult.Abort )
            {

            }

            if ( l_respuesta == DialogResult.Retry )
            {
                if ( l_auxGuardado != null )
                {
                    l_auxGuardado.AddFooterLine( ":::RECUPERACION BOLSA::: " );
                    l_auxGuardado.AddFooterLine( "FECHA ::" + DateTime.Now.ToLongDateString( ) + DateTime.Now.ToLongTimeString( ) );


                    l_auxGuardado.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
                    l_auxGuardado = null;
                }
            }


            Cursor.Hide( );
            c_BotonAceptar.Enabled = true;

            Close( );
        }

        private bool RevisarBolsaRetiro( )
        {
          DataTable l_bolsas =   BDDeposito.ChekBolsa( );

            if ( l_bolsas.Rows.Count == 1 )
            {
                int l_bolsaBD;
                int l_numBolsa;

                if ( int.TryParse( Globales.NumeroSerieBOLSA , out l_numBolsa ) && int.TryParse( l_bolsas.Rows[0][0].ToString( ) , out l_bolsaBD ) )
                {
                    try
                    {
                        if ( l_bolsaBD == l_numBolsa )
                            return true;
                        else
                            return false;
                    }
                    catch ( Exception xxc )
                    {
                        return false;
                    }
                }
                else if ( l_bolsas.Rows[0][0].ToString( ) == Globales.NumeroSerieBOLSA )
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void c_BotonCancelar_Click( object sender , EventArgs e )
        {
            Close( );
        }

        private void FormaRetiroEfectivo_Load( object sender , EventArgs e )
        {
            Cursor.Show( );
            Cursor.Current = Cursors.WaitCursor;
            l_Equipo1_vacio = false;
            l_Equipo2_vacio = false;
            TraerDatos( );
            ImprimirMovimientos( );


            if (Properties.Settings.Default.Port_YUGO > 0 && l_total_num_monedas >0)
                RetiroMonedas = true;
            
            Cursor.Hide( );
        }

        private void Limpiar( )
        {
            l_TotalPesos = 0;
            l_TotalDolares = 0;
            c_TotalRetiroEfectivo.Text = "0";
            c_1000.Text = "0";
            c_500.Text = "0";
            c_200.Text = "0";
            c_100.Text = "0";
            c_50.Text = "0";
            c_20.Text = "0";
        }

        public void ImprimirMovimientos( )
        {
            DataTable l_Movimientos;
            Double l_total = 0;

            l_Movimientos = BDConsulta.ObtenerConsultaOperaciones_mov( );

            if ( l_Movimientos.Rows.Count > 0 )
            {
                using ( FormaError f_Error = new FormaError( true , "introducir" ) )
                {
                    f_Error.c_MensajeError.Text = "TICKET PARA USO EXCLUSIVO DE ETV favor de ENTREGAR en PROCESO junto con la bolsa";
                    f_Error.ShowDialog( );


                }
                ModulePrint imprimir = new ModulePrint( );

                imprimir.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );


                imprimir.AddHeaderLine( "Consulta de MOVIMIENTOS" );
                imprimir.AddHeaderLine( "Moneda: Pesos" );
                imprimir.AddHeaderLine( "Operaciones registradas hasta el momento" );
                imprimir.AddHeaderLine( "" );
                imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
                imprimir.AddSubHeaderLine( "Fecha: " + DateTime.Now.ToString( "dd/M/yyyy" ) + " Hora: " + DateTime.Now.ToLongTimeString( ) );
                imprimir.AddSubHeaderLine( Globales.NombreCliente );

                foreach ( DataRow l_Detalle in l_Movimientos.Rows )
                {
                    Double l_monto = Double.Parse( l_Detalle[2].ToString( ) );
                    imprimir.AddItemUsuario( l_Detalle[1].ToString( ) , l_monto.ToString( "$#,###,###,##0.00" ) , l_Detalle[0].ToString( ) );
                    l_total += l_monto;

                }

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Numero de Depositantes: " + l_Movimientos.Rows.Count.ToString( ) );

                imprimir.AddFooterLine( "" );
                imprimir.AddFooterLine( "Total: " + l_total.ToString( "$#,###,###,##0.00" ) );

                imprimir.AddFooterLine( "" );

                try
                {

                    imprimir.FooterImage = Image.FromFile( Properties.Settings.Default.FooterImageTicket );

                }
                catch ( Exception exx )
                {

                }

                imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
            }
        }

        private void PedirTotalBolsaSID( )
        {

            if ( Properties.Settings.Default.No_Debug_Pc )
            {
                ushort[] l_totalNotes = new ushort[64];
                int l_totalbilletes = 0;
                l_Reply = SidLib.SID_Open( true );
                if ( l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_ALREADY_OPEN)
                    l_Reply = SidLib.SID_GetCashTotalBag( l_totalNotes );
                if ( l_Reply == SidLib.SID_OKAY )
                {
                    c_1000.Text = l_totalNotes[5].ToString( );
                    c_500.Text = l_totalNotes[4].ToString( );
                    c_200.Text = l_totalNotes[3].ToString( );
                    c_100.Text = l_totalNotes[2].ToString( );
                    c_50.Text = l_totalNotes[1].ToString( );
                    c_20.Text = l_totalNotes[0].ToString( );

                    l_totalbilletes = (int) l_totalNotes[5] + (int) l_totalNotes[4] + (int) l_totalNotes[3] +
                                      (int) l_totalNotes[2] + (int) l_totalNotes[1] + (int) l_totalNotes[0];
                }

                c_totalbilletesbolsa.Text = l_totalbilletes.ToString( );

                l_Reply = SidLib.SID_Close( );


            }

        }

        private void TraerDatos( )
        {
            int l_totalBilletes = 0;

            Limpiar( );

            PedirTotalBolsaSID( );

            l_Retiro = BDRetiro.ObtenerDetalleRetiro( 0 );

            l_Datos = BDConsulta.ObtenerConsultaDepositos( 1 );
          l_DatosMonedas = BDConsulta.ObtenerConsultaDepositos(0);

            if ( l_Datos.Rows.Count < 1 )
            {
                using ( FormaError f_Error = new FormaError( true , "sindinero" ) )
                {
                    f_Error.c_MensajeError.Text = "No hay depósitos en pesos";
                    f_Error.ShowDialog( );
                    //Close();
                }
            }
            foreach ( DataRow l_Detalle in l_Datos.Rows )
            {
                if ( l_Detalle[1].ToString( ).Trim( ) == "1000" )
                {

                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_1000.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );

                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "1000" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "1000";
                        l_Retiro.Rows.Add( l_Nueva );

                        l_TotalBolsa += (int) l_Detalle[0] * 1000;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "500" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_500.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "500" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "500";
                        l_Retiro.Rows.Add( l_Nueva );
                        l_TotalBolsa += (int) l_Detalle[0] * 500;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "200" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_200.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "200" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "200";
                        l_Retiro.Rows.Add( l_Nueva );
                        l_TotalBolsa += (int) l_Detalle[0] * 200;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "100" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_100.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "100" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "100";
                        l_Retiro.Rows.Add( l_Nueva );
                        l_TotalBolsa += (int) l_Detalle[0] * 100;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "50" )
                {

                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_50.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "50" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "50";
                        l_Retiro.Rows.Add( l_Nueva );
                        l_TotalBolsa += (int) l_Detalle[0] * 50;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }
                if ( l_Detalle[1].ToString( ).Trim( ) == "20" )
                {
                    if ( l_Detalle[2].ToString( ) == "1" )
                    {
                        c_Cajon2_20.Text = Int32.Parse( l_Detalle[0].ToString( ) ).ToString( "#,##0" );
                        DataRow l_Nueva = l_Retiro.NewRow( );
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion( 1 , "20" );
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "20";
                        l_Retiro.Rows.Add( l_Nueva );
                        l_TotalBolsa += (int) l_Detalle[0] * 20;
                        l_totalBilletes += (int) l_Detalle[0];
                    }

                }

            }


            c_totalenDB.Text = l_totalBilletes.ToString( );
            c_TotalRetiroEfectivo.Text = l_TotalBolsa.ToString( "$#,###,##0.00" );

            // # MONEDAS  TRAER Datos
            int l_totalmonedas_b2 = 0;
            int l_totalmonedas_b1 = 0;

            //total efectivo monedas
            Double l_TotalMoneadas_B1 = 0;
            Double l_TotalMoneadas_B2 = 0;


            //DEPOSITO MONEDAS
            l_RetiroMonedas = BDRetiro.ObtenerDetalleRetiro(0);

            foreach (DataRow l_Detalle in l_DatosMonedas.Rows)
            {
                if (l_Detalle[1].ToString().Trim() == "20.0")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "20.0");
                    l_Nueva[2] = "20.00";

                    if (l_Detalle[2].ToString() == "1")
                    {


                      //  c_bolsa1_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 20;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                      //  c_bolsa2_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 20;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);

                }
                if (l_Detalle[1].ToString().Trim() == "10.0")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "10.0");
                    l_Nueva[2] = "10";

                    if (l_Detalle[2].ToString() == "1")
                    {
                      //  c_bolsa1_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 10;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                       // c_bolsa2_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 10;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "5.00")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "5.00");
                    l_Nueva[2] = "5";

                    if (l_Detalle[2].ToString() == "1")
                    {
                      //  c_bolsa1_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 5;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                      //  c_bolsa2_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 5;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "2.00")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "2.00");
                    l_Nueva[2] = "2";

                    if (l_Detalle[2].ToString() == "1")
                    {
                       // c_bolsa1_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 2;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                       // c_bolsa2_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 2;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);

                }
                if (l_Detalle[1].ToString().Trim() == "1.00")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "1.00");
                    l_Nueva[2] = "1";

                    if (l_Detalle[2].ToString() == "1")
                    {
                     //   c_bolsa1_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 1;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                       // c_bolsa2_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 1;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }
                if (l_Detalle[1].ToString().Trim() == "0.50")
                {


                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.50");
                    l_Nueva[2] = "0.5";

                    if (l_Detalle[2].ToString() == "1")
                    {
                      //  c_bolsa1_50c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.5;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                      //  c_bolsa2_50c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.5;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }

                if (l_Detalle[1].ToString().Trim() == "0.20")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.20");
                    l_Nueva[2] = "0.20";

                    if (l_Detalle[2].ToString() == "1")
                    {
                      //  c_bolsa1_20c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.2;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                      //  c_bolsa2_20c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.2;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }

                if (l_Detalle[1].ToString().Trim() == "0.10")
                {
                    DataRow l_Nueva = l_RetiroMonedas.NewRow();
                    int l_cantidad = 0;
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(0, "0.10");
                    l_Nueva[2] = "0.10";

                    if (l_Detalle[2].ToString() == "1")
                    {
                      //  c_bolsa1_10c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B1 += (int)l_Detalle[0] * 0.1;
                        l_totalmonedas_b1 += (int)l_Detalle[0];
                    }
                    else
                    {
                     //   c_bolsa2_10c.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_cantidad += (int)l_Detalle[0];
                        l_TotalMoneadas_B2 += (int)l_Detalle[0] * 0.1;
                        l_totalmonedas_b2 += (int)l_Detalle[0];
                    }
                    l_Nueva[1] = l_cantidad;
                    l_RetiroMonedas.Rows.Add(l_Nueva);
                }
            }

            l_total_num_monedas = l_totalmonedas_b1 + l_totalmonedas_b2;
            l_TotalMoneadas = l_TotalMoneadas_B1 + l_TotalMoneadas_B2;

          

        }

        private void ImprimirPesos( bool p_bolsacorrecta , bool p_procedimientocorrecto )
        {
            int contador = 0;
         

            ModulePrint imprimir_copia = new ModulePrint( );

            imprimir_copia.HeaderImage = Image.FromFile( Properties.Settings.Default.ImagenTicket );


            imprimir_copia.AddHeaderLine( "COPIA para ETV " );
            if ( p_procedimientocorrecto )
            {

                imprimir_copia.AddHeaderLine( "Procedimiento CORRECTO" );
            }
            else
            {
                imprimir_copia.AddHeaderLine( "...Procedimiento INCORRECTO..." );


            }

            imprimir_copia.AddHeaderLine( "BOLSA RETIRADA: " + l_bolsaAnterior );
            if ( p_bolsacorrecta )
            {

                imprimir_copia.AddHeaderLine( "...::::COINCIDENCIA CORRECTA:::::..." );
            }
            else
            {
                imprimir_copia.AddHeaderLine( "...INCONSISTENCIA EN LAS BOLSAS..." );


            }

      //      imprimir_copia.AddHeaderLine( "BOLSA Nueva Puesta: " + Globales.NumeroSerieBOLSA );


            imprimir_copia.AddHeaderLine( "" );
            imprimir_copia.AddHeaderLine( "#SERIAL EQUIPO: SID" + Properties.Settings.Default.NumSerialEquipo );
            imprimir_copia.AddHeaderLine( "" );
            imprimir_copia.AddHeaderLine( "Moneda: Pesos" );
            imprimir_copia.AddHeaderLine( "TICKET IMPRESO :: " + DateTime.Now.ToLongTimeString( ) );
            imprimir_copia.AddHeaderLine( "BOLSA con el NUMERO:: " + l_bolsaAnterior );

       
            imprimir_copia.AddHeaderLine("NUMERO de ENVASES:: " + (RetiroMonedas? 2: 1).ToString());
            imprimir_copia.AddHeaderLine("IMPORTE TOTAL RETIRO: " + (l_TotalBolsa + l_TotalMoneadas).ToString("$#,###,##0.00"));


            if (RetiroMonedas)
            {
              

                imprimir_copia.AddHeaderLine("CANTIDAD DE MONEDAS: " + l_total_num_monedas.ToString());
                imprimir_copia.AddHeaderLine("TOTAL EN MONEDAS: " + l_TotalMoneadas.ToString("$#,###,##0.00"));
            }

            imprimir_copia.AddHeaderLine("CANTIDAD DE BILLETES: " + c_totalenDB.Text);
            imprimir_copia.AddHeaderLine("TOTAL EN BILLETES: " + c_TotalRetiroEfectivo.Text);

            imprimir_copia.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );

            imprimir_copia.AddSubHeaderLine( "Fecha: " + DateTime.Now.ToString( "dd/M/yyyy" ) + "Hora: " + DateTime.Now.ToShortTimeString( ) );
            // imprimir_copia.AddHeaderLine("");
            //imprimir_copia.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());


            imprimir_copia.AddSubHeaderLine( "CONTENIDO BOLSA A DETALLE: " );

            imprimir_copia.AddItem( c_Cajon2_1000.Text , "$1,000.00" , "1000" );
            imprimir_copia.AddItem( c_Cajon2_500.Text , "$500.00" , "500" );
            imprimir_copia.AddItem( c_Cajon2_200.Text , "$200.00" , "200" );
            imprimir_copia.AddItem( c_Cajon2_100.Text , "$100.00" , "100" );
            imprimir_copia.AddItem( c_Cajon2_50.Text , "$50.00" , "50" );
            imprimir_copia.AddItem( c_Cajon2_20.Text , "$20.00" , "20" );

            imprimir_copia.AddFooterLine( "TOTAL:              " + c_TotalRetiroEfectivo.Text );
            imprimir_copia.AddFooterLine( "Cantidad DE BILLETES:   " + c_totalenDB.Text );
            imprimir_copia.AddFooterLine( "" );
            imprimir_copia.AddFooterLine( "LUGAR DE EMISIÓN: " + Globales.NombreCliente );
            imprimir_copia.AddFooterLine( "" );
            imprimir_copia.AddFooterLine( "--------------------------" );
            imprimir_copia.AddFooterLine( "NOMBRE Y FIRMA" );


            imprimir_copia.AddFooterLine( "" );

            imprimir_copia.AddFooterLine( "FIN DEL REPORTE" );

            try
            {

                imprimir_copia.FooterImage = Image.FromFile( Properties.Settings.Default.FooterImageTicket );

            }
            catch ( Exception exx )
            {

            }


            while ( contador < 3 )
            {
                imprimir_copia.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );
                imprimir_copia.AddFooterLine( "COMPROBANTE COPIA" );
                contador++;
            }

            if ( !p_procedimientocorrecto )
                l_auxGuardado = imprimir_copia;

        }

        private void ImprimirPesos( )
        {

            

            ModulePrint imprimir = new ModulePrint( );

            //  imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);


            imprimir.AddHeaderLine( " " );
            imprimir.AddHeaderLine( " TICKET de REFERENCIA " );

            imprimir.AddHeaderLine( "BOLSA con el NUMERO:: " + l_bolsaAnterior );
            imprimir.AddHeaderLine( "REPORTE SYM  Retiro de Efectivo ETV " );

            imprimir.AddHeaderLine( "#SERIAL EQUIPO: SID" + Properties.Settings.Default.NumSerialEquipo );
            imprimir.AddHeaderLine( "Moneda: Pesos" );
            imprimir.AddHeaderLine( "TICKET IMPRESO ::" + DateTime.Now.ToLongTimeString( ) );
            //  imprimir.AddHeaderLine("BOLSA con el NUMERO :: " + Globales.NumeroSerieBOLSA);
            imprimir.AddHeaderLine( "Total en BOLSA:   " + c_TotalRetiroEfectivo.Text );
            imprimir.AddHeaderLine( "Cantidad DE BILLETES: " + c_totalenDB.Text );
            imprimir.AddSubHeaderLine( "Usuario: " + Globales.IdUsuario );
            imprimir.AddSubHeaderLine( "Fecha: " + DateTime.Now.ToString( "dd/M/yyyy" ) + "Hora: " + DateTime.Now.ToLongTimeString( ) );
            // imprimir.AddHeaderLine("");
            // imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToLongTimeString());


            imprimir.AddSubHeaderLine( "CONTENIDO BOLSA A DETALLE: " );

            imprimir.AddItem( c_Cajon2_1000.Text , "$1,000.00" , "1000" );
            imprimir.AddItem( c_Cajon2_500.Text , "$500.00" , "500" );
            imprimir.AddItem( c_Cajon2_200.Text , "$200.00" , "200" );
            imprimir.AddItem( c_Cajon2_100.Text , "$100.00" , "100" );
            imprimir.AddItem( c_Cajon2_50.Text , "$50.00" , "50" );
            imprimir.AddItem( c_Cajon2_20.Text , "$20.00" , "20" );

            imprimir.AddFooterLine( "TOTAL:                 " + c_TotalRetiroEfectivo.Text );
            // imprimir.AddFooterLine("");
            imprimir.AddFooterLine( "LUGAR DE EMISIÓN:" + Globales.NombreCliente );


            imprimir.PrintTicket( Properties.Settings.Default.Impresora , DateTime.Now.ToShortTimeString( ) );


        }

        public void Error_Sid( int l_Reply , out String p_Error )
        {
            String l_Error = "";

            SidLib.Error_Sid( l_Reply , out l_Error );
            p_Error = l_Error;

            if ( Globales.Estatus != Globales.EstatusReceptor.Lleno && Globales.Estatus != Globales.EstatusReceptor.Mantenimiento )
            {

                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply < 0 && Globales.Estatus != Globales.EstatusReceptor.No_Operable )
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );


                if ( Properties.Settings.Default.Enviar_AlertasGSI && l_Reply == 0 && Globales.Estatus == Globales.EstatusReceptor.No_Operable )
                {
                    UtilsComunicacion.AlertaGSI( l_Reply , l_Error );
                    Globales.CambioEstatusReceptor( Globales.EstatusReceptor.Operable );
                }
            }

        }
    }
}
