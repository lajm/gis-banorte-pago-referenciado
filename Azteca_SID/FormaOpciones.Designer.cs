﻿namespace Azteca_SID
{
    partial class FormaOpciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_regresar = new System.Windows.Forms.PictureBox();
            this.c_Etv = new System.Windows.Forms.Button();
            this.c_Administrar = new System.Windows.Forms.Button();
            this.c_Deposito = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_regresar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.SlateGray;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox4.Image = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.pictureBox4.Location = new System.Drawing.Point(527, 410);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(62, 46);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 19;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.SlateGray;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.pictureBox3.Location = new System.Drawing.Point(175, 258);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(63, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.SlateGray;
            this.pictureBox2.BackgroundImage = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Image = global::Azteca_SID.Properties.Resources.dialog_ok_apply_2;
            this.pictureBox2.Location = new System.Drawing.Point(82, 410);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(63, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(494, 513);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 25);
            this.label2.TabIndex = 16;
            this.label2.Text = "% ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(247, 513);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 25);
            this.label1.TabIndex = 15;
            this.label1.Text = "PORCENTAJE CUPO:";
            // 
            // c_regresar
            // 
            this.c_regresar.BackColor = System.Drawing.Color.LightSlateGray;
            this.c_regresar.Image = global::Azteca_SID.Properties.Resources.edit_undo_4;
            this.c_regresar.Location = new System.Drawing.Point(698, 112);
            this.c_regresar.Name = "c_regresar";
            this.c_regresar.Size = new System.Drawing.Size(90, 77);
            this.c_regresar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_regresar.TabIndex = 14;
            this.c_regresar.TabStop = false;
            this.c_regresar.Click += new System.EventHandler(this.c_regresar_Click);
            // 
            // c_Etv
            // 
            this.c_Etv.BackColor = System.Drawing.Color.LightSlateGray;
            this.c_Etv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Etv.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Etv.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Etv.ForeColor = System.Drawing.Color.White;
            this.c_Etv.Location = new System.Drawing.Point(508, 392);
            this.c_Etv.Name = "c_Etv";
            this.c_Etv.Size = new System.Drawing.Size(226, 88);
            this.c_Etv.TabIndex = 13;
            this.c_Etv.Text = "ETV ";
            this.c_Etv.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Etv.UseVisualStyleBackColor = false;
            this.c_Etv.Click += new System.EventHandler(this.c_Etv_Click);
            // 
            // c_Administrar
            // 
            this.c_Administrar.BackColor = System.Drawing.Color.LightSlateGray;
            this.c_Administrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Administrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Administrar.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Administrar.ForeColor = System.Drawing.Color.White;
            this.c_Administrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Administrar.Location = new System.Drawing.Point(66, 392);
            this.c_Administrar.Name = "c_Administrar";
            this.c_Administrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.c_Administrar.Size = new System.Drawing.Size(384, 88);
            this.c_Administrar.TabIndex = 12;
            this.c_Administrar.Text = "ADMINISTRAR";
            this.c_Administrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Administrar.UseVisualStyleBackColor = false;
            this.c_Administrar.Click += new System.EventHandler(this.c_Administrar_Click);
            // 
            // c_Deposito
            // 
            this.c_Deposito.AutoEllipsis = true;
            this.c_Deposito.BackColor = System.Drawing.Color.LightSlateGray;
            this.c_Deposito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Deposito.FlatAppearance.BorderSize = 0;
            this.c_Deposito.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Deposito.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Deposito.ForeColor = System.Drawing.Color.White;
            this.c_Deposito.Location = new System.Drawing.Point(134, 191);
            this.c_Deposito.Name = "c_Deposito";
            this.c_Deposito.Size = new System.Drawing.Size(545, 173);
            this.c_Deposito.TabIndex = 11;
            this.c_Deposito.Text = "DEPOSITAR";
            this.c_Deposito.UseVisualStyleBackColor = false;
            this.c_Deposito.Click += new System.EventHandler(this.c_Deposito_Click);
            // 
            // FormaOpciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_regresar);
            this.Controls.Add(this.c_Etv);
            this.Controls.Add(this.c_Administrar);
            this.Controls.Add(this.c_Deposito);
            this.Name = "FormaOpciones";
            this.Text = "FormaOpciones";
            this.Load += new System.EventHandler(this.FormaOpciones_Load);
            this.Controls.SetChildIndex(this.c_Deposito, 0);
            this.Controls.SetChildIndex(this.c_Administrar, 0);
            this.Controls.SetChildIndex(this.c_Etv, 0);
            this.Controls.SetChildIndex(this.c_regresar, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            this.Controls.SetChildIndex(this.pictureBox3, 0);
            this.Controls.SetChildIndex(this.pictureBox4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_regresar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox c_regresar;
        private System.Windows.Forms.Button c_Etv;
        private System.Windows.Forms.Button c_Administrar;
        private System.Windows.Forms.Button c_Deposito;
    }
}