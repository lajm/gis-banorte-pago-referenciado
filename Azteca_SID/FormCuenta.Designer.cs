﻿namespace Azteca_SID
{
    partial class FormCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.c_volver = new System.Windows.Forms.PictureBox();
            this.c_aceptar = new System.Windows.Forms.PictureBox();
            this.c_Boton1 = new System.Windows.Forms.PictureBox();
            this.c_BackSpace = new System.Windows.Forms.PictureBox();
            this.c_Boton9 = new System.Windows.Forms.PictureBox();
            this.c_Boton8 = new System.Windows.Forms.PictureBox();
            this.c_Boton7 = new System.Windows.Forms.PictureBox();
            this.c_Boton6 = new System.Windows.Forms.PictureBox();
            this.c_Boton5 = new System.Windows.Forms.PictureBox();
            this.c_Boton4 = new System.Windows.Forms.PictureBox();
            this.c_Boton3 = new System.Windows.Forms.PictureBox();
            this.c_Boton2 = new System.Windows.Forms.PictureBox();
            this.c_BotonBlanco = new System.Windows.Forms.PictureBox();
            this.c_Boton0 = new System.Windows.Forms.PictureBox();
            this.c_accion = new System.Windows.Forms.PictureBox();
            this.c_mensajeCuenta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_enviando = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BackSpace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BotonBlanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_accion)).BeginInit();
            this.SuspendLayout();
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_cuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.c_cuenta.Enabled = false;
            this.c_cuenta.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.c_cuenta.Location = new System.Drawing.Point(74, 194);
            this.c_cuenta.Margin = new System.Windows.Forms.Padding(5);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(313, 33);
            this.c_cuenta.TabIndex = 2;
            this.c_cuenta.Text = "Ingrese su número de cuenta";
            // 
            // c_volver
            // 
            this.c_volver.BackColor = System.Drawing.Color.Transparent;
            this.c_volver.Image = global::Azteca_SID.Properties.Resources.btn_banorte_volver;
            this.c_volver.Location = new System.Drawing.Point(0, 527);
            this.c_volver.Name = "c_volver";
            this.c_volver.Size = new System.Drawing.Size(127, 48);
            this.c_volver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_volver.TabIndex = 5;
            this.c_volver.TabStop = false;
            this.c_volver.Click += new System.EventHandler(this.c_volver_Click);
            // 
            // c_aceptar
            // 
            this.c_aceptar.BackColor = System.Drawing.Color.Transparent;
            this.c_aceptar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_aceptar;
            this.c_aceptar.Location = new System.Drawing.Point(673, 526);
            this.c_aceptar.Name = "c_aceptar";
            this.c_aceptar.Size = new System.Drawing.Size(127, 48);
            this.c_aceptar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_aceptar.TabIndex = 6;
            this.c_aceptar.TabStop = false;
            this.c_aceptar.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton1
            // 
            this.c_Boton1.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton1.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_1;
            this.c_Boton1.Location = new System.Drawing.Point(473, 126);
            this.c_Boton1.Name = "c_Boton1";
            this.c_Boton1.Size = new System.Drawing.Size(86, 72);
            this.c_Boton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton1.TabIndex = 9;
            this.c_Boton1.TabStop = false;
            this.c_Boton1.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_BackSpace
            // 
            this.c_BackSpace.BackColor = System.Drawing.Color.Transparent;
            this.c_BackSpace.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_borrar;
            this.c_BackSpace.Location = new System.Drawing.Point(473, 360);
            this.c_BackSpace.Name = "c_BackSpace";
            this.c_BackSpace.Size = new System.Drawing.Size(86, 72);
            this.c_BackSpace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_BackSpace.TabIndex = 10;
            this.c_BackSpace.TabStop = false;
            this.c_BackSpace.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton9
            // 
            this.c_Boton9.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton9.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_9;
            this.c_Boton9.Location = new System.Drawing.Point(679, 282);
            this.c_Boton9.Name = "c_Boton9";
            this.c_Boton9.Size = new System.Drawing.Size(86, 72);
            this.c_Boton9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton9.TabIndex = 11;
            this.c_Boton9.TabStop = false;
            this.c_Boton9.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton8
            // 
            this.c_Boton8.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton8.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_8;
            this.c_Boton8.Location = new System.Drawing.Point(576, 282);
            this.c_Boton8.Name = "c_Boton8";
            this.c_Boton8.Size = new System.Drawing.Size(86, 72);
            this.c_Boton8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton8.TabIndex = 12;
            this.c_Boton8.TabStop = false;
            this.c_Boton8.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton7
            // 
            this.c_Boton7.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton7.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_7;
            this.c_Boton7.Location = new System.Drawing.Point(473, 282);
            this.c_Boton7.Name = "c_Boton7";
            this.c_Boton7.Size = new System.Drawing.Size(86, 72);
            this.c_Boton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton7.TabIndex = 13;
            this.c_Boton7.TabStop = false;
            this.c_Boton7.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton6
            // 
            this.c_Boton6.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton6.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_6;
            this.c_Boton6.Location = new System.Drawing.Point(679, 204);
            this.c_Boton6.Name = "c_Boton6";
            this.c_Boton6.Size = new System.Drawing.Size(86, 72);
            this.c_Boton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton6.TabIndex = 14;
            this.c_Boton6.TabStop = false;
            this.c_Boton6.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton5
            // 
            this.c_Boton5.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton5.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_5;
            this.c_Boton5.Location = new System.Drawing.Point(576, 204);
            this.c_Boton5.Name = "c_Boton5";
            this.c_Boton5.Size = new System.Drawing.Size(86, 72);
            this.c_Boton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton5.TabIndex = 15;
            this.c_Boton5.TabStop = false;
            this.c_Boton5.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton4
            // 
            this.c_Boton4.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton4.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_4;
            this.c_Boton4.Location = new System.Drawing.Point(473, 204);
            this.c_Boton4.Name = "c_Boton4";
            this.c_Boton4.Size = new System.Drawing.Size(86, 72);
            this.c_Boton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton4.TabIndex = 16;
            this.c_Boton4.TabStop = false;
            this.c_Boton4.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton3
            // 
            this.c_Boton3.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton3.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_3;
            this.c_Boton3.Location = new System.Drawing.Point(679, 126);
            this.c_Boton3.Name = "c_Boton3";
            this.c_Boton3.Size = new System.Drawing.Size(86, 72);
            this.c_Boton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton3.TabIndex = 17;
            this.c_Boton3.TabStop = false;
            this.c_Boton3.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton2
            // 
            this.c_Boton2.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton2.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_2;
            this.c_Boton2.Location = new System.Drawing.Point(576, 126);
            this.c_Boton2.Name = "c_Boton2";
            this.c_Boton2.Size = new System.Drawing.Size(86, 72);
            this.c_Boton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton2.TabIndex = 18;
            this.c_Boton2.TabStop = false;
            this.c_Boton2.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_BotonBlanco
            // 
            this.c_BotonBlanco.BackColor = System.Drawing.Color.Transparent;
            this.c_BotonBlanco.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_blanco;
            this.c_BotonBlanco.Location = new System.Drawing.Point(679, 360);
            this.c_BotonBlanco.Name = "c_BotonBlanco";
            this.c_BotonBlanco.Size = new System.Drawing.Size(86, 72);
            this.c_BotonBlanco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_BotonBlanco.TabIndex = 19;
            this.c_BotonBlanco.TabStop = false;
            // 
            // c_Boton0
            // 
            this.c_Boton0.BackColor = System.Drawing.Color.Transparent;
            this.c_Boton0.Image = global::Azteca_SID.Properties.Resources.botones_banorte_teclado_0;
            this.c_Boton0.Location = new System.Drawing.Point(576, 360);
            this.c_Boton0.Name = "c_Boton0";
            this.c_Boton0.Size = new System.Drawing.Size(86, 72);
            this.c_Boton0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_Boton0.TabIndex = 20;
            this.c_Boton0.TabStop = false;
            this.c_Boton0.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_accion
            // 
            this.c_accion.BackColor = System.Drawing.Color.Transparent;
            this.c_accion.Image = global::Azteca_SID.Properties.Resources.appointment_soon;
            this.c_accion.Location = new System.Drawing.Point(119, 250);
            this.c_accion.Name = "c_accion";
            this.c_accion.Size = new System.Drawing.Size(192, 148);
            this.c_accion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.c_accion.TabIndex = 100;
            this.c_accion.TabStop = false;
            this.c_accion.Visible = false;
            // 
            // c_mensajeCuenta
            // 
            this.c_mensajeCuenta.AutoSize = true;
            this.c_mensajeCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_mensajeCuenta.Font = new System.Drawing.Font("Gotham Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mensajeCuenta.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.c_mensajeCuenta.Location = new System.Drawing.Point(54, 150);
            this.c_mensajeCuenta.Name = "c_mensajeCuenta";
            this.c_mensajeCuenta.Size = new System.Drawing.Size(297, 22);
            this.c_mensajeCuenta.TabIndex = 103;
            this.c_mensajeCuenta.Text = "Comencemos con su Depósito";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Gotham Bold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(53, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 28);
            this.label1.TabIndex = 102;
            this.label1.Text = "BIENVENIDO";
            // 
            // c_enviando
            // 
            this.c_enviando.BackColor = System.Drawing.Color.Transparent;
            this.c_enviando.Font = new System.Drawing.Font("Gotham Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_enviando.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.c_enviando.Location = new System.Drawing.Point(44, 414);
            this.c_enviando.Name = "c_enviando";
            this.c_enviando.Size = new System.Drawing.Size(376, 70);
            this.c_enviando.TabIndex = 104;
            this.c_enviando.Text = "Un minuto por favor, estamos verificando su cuenta";
            this.c_enviando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.c_enviando.Visible = false;
            // 
            // FormCuenta
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_enviando);
            this.Controls.Add(this.c_mensajeCuenta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_volver);
            this.Controls.Add(this.c_aceptar);
            this.Controls.Add(this.c_accion);
            this.Controls.Add(this.c_Boton0);
            this.Controls.Add(this.c_BotonBlanco);
            this.Controls.Add(this.c_Boton2);
            this.Controls.Add(this.c_Boton3);
            this.Controls.Add(this.c_Boton4);
            this.Controls.Add(this.c_Boton5);
            this.Controls.Add(this.c_Boton6);
            this.Controls.Add(this.c_Boton7);
            this.Controls.Add(this.c_Boton8);
            this.Controls.Add(this.c_Boton9);
            this.Controls.Add(this.c_BackSpace);
            this.Controls.Add(this.c_Boton1);
            this.Controls.Add(this.c_cuenta);
            this.Name = "FormCuenta";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.FormCuenta_Load);
            this.Controls.SetChildIndex(this.c_cuenta, 0);
            this.Controls.SetChildIndex(this.c_Boton1, 0);
            this.Controls.SetChildIndex(this.c_BackSpace, 0);
            this.Controls.SetChildIndex(this.c_Boton9, 0);
            this.Controls.SetChildIndex(this.c_Boton8, 0);
            this.Controls.SetChildIndex(this.c_Boton7, 0);
            this.Controls.SetChildIndex(this.c_Boton6, 0);
            this.Controls.SetChildIndex(this.c_Boton5, 0);
            this.Controls.SetChildIndex(this.c_Boton4, 0);
            this.Controls.SetChildIndex(this.c_Boton3, 0);
            this.Controls.SetChildIndex(this.c_Boton2, 0);
            this.Controls.SetChildIndex(this.c_BotonBlanco, 0);
            this.Controls.SetChildIndex(this.c_Boton0, 0);
            this.Controls.SetChildIndex(this.c_accion, 0);
            this.Controls.SetChildIndex(this.c_aceptar, 0);
            this.Controls.SetChildIndex(this.c_volver, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.c_mensajeCuenta, 0);
            this.Controls.SetChildIndex(this.c_enviando, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BackSpace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_BotonBlanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Boton0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_accion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.PictureBox c_volver;
        private System.Windows.Forms.PictureBox c_aceptar;
        private System.Windows.Forms.PictureBox c_Boton1;
        private System.Windows.Forms.PictureBox c_BackSpace;
        private System.Windows.Forms.PictureBox c_Boton9;
        private System.Windows.Forms.PictureBox c_Boton8;
        private System.Windows.Forms.PictureBox c_Boton7;
        private System.Windows.Forms.PictureBox c_Boton6;
        private System.Windows.Forms.PictureBox c_Boton5;
        private System.Windows.Forms.PictureBox c_Boton4;
        private System.Windows.Forms.PictureBox c_Boton3;
        private System.Windows.Forms.PictureBox c_Boton2;
        private System.Windows.Forms.PictureBox c_BotonBlanco;
        private System.Windows.Forms.PictureBox c_Boton0;
        private System.Windows.Forms.PictureBox c_accion;
        private System.Windows.Forms.Label c_mensajeCuenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label c_enviando;
    }
}
