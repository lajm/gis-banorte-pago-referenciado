﻿namespace Azteca_SID
{
    partial class FormaUSUARIOgsi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.e_nombre = new System.Windows.Forms.Label();
            this.e_confirmacion = new System.Windows.Forms.Label();
            this.e_mensaje = new System.Windows.Forms.Label();
            this.e_instrucciones = new System.Windows.Forms.Label();
            this.c_aceptar = new System.Windows.Forms.PictureBox();
            this.c_volver = new System.Windows.Forms.PictureBox();
            this.e_restriccion = new System.Windows.Forms.Label();
            this.e_mensajeRestriccion = new System.Windows.Forms.Label();
            this.i_alerta = new System.Windows.Forms.PictureBox();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.i_alerta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // e_nombre
            // 
            this.e_nombre.BackColor = System.Drawing.Color.Transparent;
            this.e_nombre.Font = new System.Drawing.Font("Gotham Medium", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_nombre.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_nombre.Location = new System.Drawing.Point(50, 189);
            this.e_nombre.Name = "e_nombre";
            this.e_nombre.Size = new System.Drawing.Size(712, 73);
            this.e_nombre.TabIndex = 4;
            this.e_nombre.Text = "Nombre GSI";
            this.e_nombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // e_confirmacion
            // 
            this.e_confirmacion.AutoSize = true;
            this.e_confirmacion.Font = new System.Drawing.Font("Gotham Medium", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_confirmacion.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_confirmacion.Location = new System.Drawing.Point(25, 74);
            this.e_confirmacion.Name = "e_confirmacion";
            this.e_confirmacion.Size = new System.Drawing.Size(187, 27);
            this.e_confirmacion.TabIndex = 5;
            this.e_confirmacion.Text = "Confirmación";
            // 
            // e_mensaje
            // 
            this.e_mensaje.AutoSize = true;
            this.e_mensaje.Font = new System.Drawing.Font("Gotham Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_mensaje.ForeColor = System.Drawing.SystemColors.GrayText;
            this.e_mensaje.Location = new System.Drawing.Point(25, 115);
            this.e_mensaje.Name = "e_mensaje";
            this.e_mensaje.Size = new System.Drawing.Size(346, 23);
            this.e_mensaje.TabIndex = 6;
            this.e_mensaje.Text = "Depósito a realizar a nombre de: ";
            // 
            // e_instrucciones
            // 
            this.e_instrucciones.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_instrucciones.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.e_instrucciones.Location = new System.Drawing.Point(314, 374);
            this.e_instrucciones.Name = "e_instrucciones";
            this.e_instrucciones.Size = new System.Drawing.Size(474, 78);
            this.e_instrucciones.TabIndex = 7;
            this.e_instrucciones.Text = "Para continuar presione ACEPTAR o SALIR / VOLVER para Corregir Cuenta";
            this.e_instrucciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // c_aceptar
            // 
            this.c_aceptar.BackColor = System.Drawing.Color.Transparent;
            this.c_aceptar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_aceptar;
            this.c_aceptar.Location = new System.Drawing.Point(673, 505);
            this.c_aceptar.Name = "c_aceptar";
            this.c_aceptar.Size = new System.Drawing.Size(127, 48);
            this.c_aceptar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_aceptar.TabIndex = 8;
            this.c_aceptar.TabStop = false;
            this.c_aceptar.Click += new System.EventHandler(this.c_aceptar_Click);
            // 
            // c_volver
            // 
            this.c_volver.BackColor = System.Drawing.Color.Transparent;
            this.c_volver.Image = global::Azteca_SID.Properties.Resources.btn_banorte_volver;
            this.c_volver.Location = new System.Drawing.Point(0, 505);
            this.c_volver.Name = "c_volver";
            this.c_volver.Size = new System.Drawing.Size(127, 49);
            this.c_volver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_volver.TabIndex = 9;
            this.c_volver.TabStop = false;
            this.c_volver.Click += new System.EventHandler(this.c_volver_Click);
            // 
            // e_restriccion
            // 
            this.e_restriccion.Font = new System.Drawing.Font("Roboto", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_restriccion.ForeColor = System.Drawing.Color.Red;
            this.e_restriccion.Location = new System.Drawing.Point(346, 283);
            this.e_restriccion.Name = "e_restriccion";
            this.e_restriccion.Size = new System.Drawing.Size(355, 64);
            this.e_restriccion.TabIndex = 10;
            this.e_restriccion.Text = "Restricción en cuenta Monto Límite Permitido:  ";
            this.e_restriccion.Visible = false;
            // 
            // e_mensajeRestriccion
            // 
            this.e_mensajeRestriccion.Font = new System.Drawing.Font("Roboto", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_mensajeRestriccion.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.e_mensajeRestriccion.Location = new System.Drawing.Point(274, 374);
            this.e_mensajeRestriccion.Name = "e_mensajeRestriccion";
            this.e_mensajeRestriccion.Size = new System.Drawing.Size(514, 97);
            this.e_mensajeRestriccion.TabIndex = 11;
            this.e_mensajeRestriccion.Text = "Para continuar presione ACEPTAR, para corregir la cuenta VOLVER, si el monto a de" +
    "positar es mayor, presione SALIR y acuda con el ejecutivo";
            this.e_mensajeRestriccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.e_mensajeRestriccion.Visible = false;
            // 
            // i_alerta
            // 
            this.i_alerta.Image = global::Azteca_SID.Properties.Resources.process_stop_3;
            this.i_alerta.Location = new System.Drawing.Point(697, 265);
            this.i_alerta.Name = "i_alerta";
            this.i_alerta.Size = new System.Drawing.Size(103, 82);
            this.i_alerta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.i_alerta.TabIndex = 12;
            this.i_alerta.TabStop = false;
            this.i_alerta.Visible = false;
            // 
            // c_cuenta
            // 
            this.c_cuenta.BackColor = System.Drawing.SystemColors.HighlightText;
            this.c_cuenta.Enabled = false;
            this.c_cuenta.Font = new System.Drawing.Font("Roboto Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.Location = new System.Drawing.Point(174, 164);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(307, 43);
            this.c_cuenta.TabIndex = 14;
            this.c_cuenta.Text = "******XXXX";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.pesos;
            this.pictureBox3.Location = new System.Drawing.Point(482, 165);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(144, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // FormaUSUARIOgsi
            // 
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_cuenta);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.i_alerta);
            this.Controls.Add(this.e_restriccion);
            this.Controls.Add(this.c_volver);
            this.Controls.Add(this.c_aceptar);
            this.Controls.Add(this.e_instrucciones);
            this.Controls.Add(this.e_mensaje);
            this.Controls.Add(this.e_confirmacion);
            this.Controls.Add(this.e_nombre);
            this.Controls.Add(this.e_mensajeRestriccion);
            this.Name = "FormaUSUARIOgsi";
            this.Load += new System.EventHandler(this.FormaUSUARIOgsi_Load);
            this.Controls.SetChildIndex(this.e_mensajeRestriccion, 0);
            this.Controls.SetChildIndex(this.e_nombre, 0);
            this.Controls.SetChildIndex(this.e_confirmacion, 0);
            this.Controls.SetChildIndex(this.e_mensaje, 0);
            this.Controls.SetChildIndex(this.e_instrucciones, 0);
            this.Controls.SetChildIndex(this.c_aceptar, 0);
            this.Controls.SetChildIndex(this.c_volver, 0);
            this.Controls.SetChildIndex(this.e_restriccion, 0);
            this.Controls.SetChildIndex(this.i_alerta, 0);
            this.Controls.SetChildIndex(this.pictureBox3, 0);
            this.Controls.SetChildIndex(this.c_cuenta, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_volver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.i_alerta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label e_nombre;
        private System.Windows.Forms.Label e_confirmacion;
        private System.Windows.Forms.Label e_mensaje;
        private System.Windows.Forms.Label e_instrucciones;
        private System.Windows.Forms.PictureBox c_aceptar;
        private System.Windows.Forms.PictureBox c_volver;
        private System.Windows.Forms.Label e_restriccion;
        private System.Windows.Forms.Label e_mensajeRestriccion;
        private System.Windows.Forms.PictureBox i_alerta;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
