﻿namespace Azteca_SID
{
    partial class FormMasDepositos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_otra = new System.Windows.Forms.PictureBox();
            this.c_misma = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.c_cuenta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.e_referencia = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c_otra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_misma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // c_otra
            // 
            this.c_otra.BackColor = System.Drawing.Color.Transparent;
            this.c_otra.Image = global::Azteca_SID.Properties.Resources.botones_banorte_otra_cuenta;
            this.c_otra.Location = new System.Drawing.Point(462, 223);
            this.c_otra.Name = "c_otra";
            this.c_otra.Size = new System.Drawing.Size(122, 122);
            this.c_otra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_otra.TabIndex = 4;
            this.c_otra.TabStop = false;
            this.c_otra.Click += new System.EventHandler(this.c_otra_Click);
            // 
            // c_misma
            // 
            this.c_misma.BackColor = System.Drawing.Color.Transparent;
            this.c_misma.Image = global::Azteca_SID.Properties.Resources.botones_banorte_misma_cuenta;
            this.c_misma.Location = new System.Drawing.Point(223, 223);
            this.c_misma.Name = "c_misma";
            this.c_misma.Size = new System.Drawing.Size(122, 122);
            this.c_misma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_misma.TabIndex = 5;
            this.c_misma.TabStop = false;
            this.c_misma.Click += new System.EventHandler(this.c_misma_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Azteca_SID.Properties.Resources.pesos;
            this.pictureBox3.Location = new System.Drawing.Point(309, 364);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(105, 42);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // c_cuenta
            // 
            this.c_cuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_cuenta.Location = new System.Drawing.Point(157, 366);
            this.c_cuenta.Name = "c_cuenta";
            this.c_cuenta.Size = new System.Drawing.Size(152, 38);
            this.c_cuenta.TabIndex = 7;
            this.c_cuenta.Text = "******XXXX";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label1.Location = new System.Drawing.Point(271, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 29);
            this.label1.TabIndex = 8;
            this.label1.Text = "Realiza más Depósitos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(176, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(448, 24);
            this.label2.TabIndex = 9;
            this.label2.Text = "¿A que cuenta desea realizar su siguiente depósito?";
            // 
            // e_referencia
            // 
            this.e_referencia.AutoSize = true;
            this.e_referencia.BackColor = System.Drawing.Color.Transparent;
            this.e_referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_referencia.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.e_referencia.Location = new System.Drawing.Point(153, 407);
            this.e_referencia.Name = "e_referencia";
            this.e_referencia.Size = new System.Drawing.Size(107, 20);
            this.e_referencia.TabIndex = 10;
            this.e_referencia.Text = "Referencia: ";
            this.e_referencia.Visible = false;
            // 
            // FormMasDepositos
            // 
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.Fondo_Banorte;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.e_referencia);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_cuenta);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.c_misma);
            this.Controls.Add(this.c_otra);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMasDepositos";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.FormMasDepositos_Load);
            this.Controls.SetChildIndex(this.c_otra, 0);
            this.Controls.SetChildIndex(this.c_misma, 0);
            this.Controls.SetChildIndex(this.pictureBox3, 0);
            this.Controls.SetChildIndex(this.c_cuenta, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.e_referencia, 0);
            ((System.ComponentModel.ISupportInitialize)(this.c_otra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_misma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox c_otra;
        private System.Windows.Forms.PictureBox c_misma;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox c_cuenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label e_referencia;
    }
}
