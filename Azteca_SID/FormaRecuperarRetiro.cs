﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaRecuperarRetiro : Form
    {
        String l_Folio_Envase_pendiente;

        public FormaRecuperarRetiro(String p_bolsa )
        {
            InitializeComponent( );
            l_Folio_Envase_pendiente = p_bolsa;
        }

        private void c_Abandonar_Click( object sender , EventArgs e )
        {
            c_Abandonar.Enabled = false;
            DialogResult l_respuesta;

            using ( FormaError v_pregunta = new FormaError( true , true , false , "question" ) )
            {
                v_pregunta.c_MensajeError.Text = "¿Desea salir sin intentar Recuperar el RETIRO?";
                l_respuesta = v_pregunta.ShowDialog( );
            }

            if ( l_respuesta == DialogResult.OK )
            {
                Close( );
            }
            c_Abandonar.Enabled = true;
        }

        private void c_recuperar_Click( object sender , EventArgs e )
        {
            c_recuperar.Enabled = false;

            DialogResult l_respuesta;

            using ( FormaAutenticar v_autenticar = new FormaAutenticar( true ) )
            {
                l_respuesta = v_autenticar.ShowDialog( );
            }

            if ( l_respuesta == DialogResult.OK )
            {
                // -- Realizara el Retiro Pendiente con el folio listado.
                Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "Retiro Pendiente, enviando transacción "
                            + Environment.NewLine + "Retiro con Folio: "+ l_Folio_Envase_pendiente , 3 );
                int l_reply = ProcesosSid.EnviarTransaccionTruncaRetiro( l_Folio_Envase_pendiente );

                if ( l_reply == 0 )
                {
                    Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "Retiro Pendiente, enviando exitoso "
                            + Environment.NewLine + "Retiro con Folio: " + l_Folio_Envase_pendiente , 3 );

                    using ( FormaError v_pregunta = new FormaError( true , "alerta" ) )
                    {
                        v_pregunta.c_MensajeError.Text = "Por favor REVISE el Numero de Bolsa para completar el Proceso o no podra Depositar";
                        l_respuesta = v_pregunta.ShowDialog( );
                    }

                }else
                {
                    Globales.EscribirBitacora( "FormaPrincipal" , "revisarSesion()" , "Retiro Pendiente, hubo problemas al realizarlo"
                       + Environment.NewLine + "Retiro con Folio: " + l_Folio_Envase_pendiente , 3 );
                }


                Close( );
            }
            else
            {
                using ( FormaError v_pregunta = new FormaError( true , "question" ) )
                {
                    v_pregunta.c_MensajeError.Text = "Por favor Utilize El password de Supervisor";
                    l_respuesta = v_pregunta.ShowDialog( );
                }

            }

            c_recuperar.Enabled = true;
        }
    }
}
