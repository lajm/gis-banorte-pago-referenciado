﻿namespace Azteca_SID
{
    partial class uc_Bienvenida
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_SimularTarjeta = new System.Windows.Forms.Button();
            this.c_Doc = new System.Windows.Forms.Label();
            this.c_Dll = new System.Windows.Forms.Label();
            this.c_Logueo = new System.Windows.Forms.Button();
            this.c_mensaje = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_SimularTarjeta
            // 
            this.c_SimularTarjeta.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_SimularTarjeta.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_SimularTarjeta.Location = new System.Drawing.Point(787, 107);
            this.c_SimularTarjeta.Name = "c_SimularTarjeta";
            this.c_SimularTarjeta.Size = new System.Drawing.Size(13, 20);
            this.c_SimularTarjeta.TabIndex = 1;
            this.c_SimularTarjeta.Text = "Simular tarjeta";
            this.c_SimularTarjeta.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_SimularTarjeta.UseVisualStyleBackColor = true;
            this.c_SimularTarjeta.Visible = false;
            // 
            // c_Doc
            // 
            this.c_Doc.AutoSize = true;
            this.c_Doc.BackColor = System.Drawing.Color.Transparent;
            this.c_Doc.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Doc.Location = new System.Drawing.Point(3, 572);
            this.c_Doc.Name = "c_Doc";
            this.c_Doc.Size = new System.Drawing.Size(35, 13);
            this.c_Doc.TabIndex = 2;
            this.c_Doc.Text = "label1";
            // 
            // c_Dll
            // 
            this.c_Dll.AutoSize = true;
            this.c_Dll.BackColor = System.Drawing.Color.Transparent;
            this.c_Dll.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Dll.Location = new System.Drawing.Point(3, 587);
            this.c_Dll.Name = "c_Dll";
            this.c_Dll.Size = new System.Drawing.Size(35, 13);
            this.c_Dll.TabIndex = 3;
            this.c_Dll.Text = "label2";
            // 
            // c_Logueo
            // 
            this.c_Logueo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Logueo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Logueo.Location = new System.Drawing.Point(702, 146);
            this.c_Logueo.Name = "c_Logueo";
            this.c_Logueo.Size = new System.Drawing.Size(98, 30);
            this.c_Logueo.TabIndex = 4;
            this.c_Logueo.Text = "Identificarse";
            this.c_Logueo.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Logueo.UseVisualStyleBackColor = true;
            this.c_Logueo.Visible = false;
            // 
            // c_mensaje
            // 
            this.c_mensaje.AutoSize = true;
            this.c_mensaje.BackColor = System.Drawing.Color.Transparent;
            this.c_mensaje.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mensaje.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.c_mensaje.Location = new System.Drawing.Point(173, 377);
            this.c_mensaje.Name = "c_mensaje";
            this.c_mensaje.Size = new System.Drawing.Size(454, 30);
            this.c_mensaje.TabIndex = 5;
            this.c_mensaje.Text = "Ó TOCA LA PANTALLA PARA EMPEZAR";
            this.c_mensaje.Visible = false;
            // 
            // uc_Bienvenida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources.SID_pantalla_bienvenida_gsi_banorte_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.c_mensaje);
            this.Controls.Add(this.c_Logueo);
            this.Controls.Add(this.c_Dll);
            this.Controls.Add(this.c_Doc);
            this.Controls.Add(this.c_SimularTarjeta);
            this.DoubleBuffered = true;
            this.Name = "uc_Bienvenida";
            this.Size = new System.Drawing.Size(800, 600);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button c_SimularTarjeta;
        public System.Windows.Forms.Label c_Doc;
        public System.Windows.Forms.Label c_Dll;
        public System.Windows.Forms.Button c_Logueo;
        private System.Windows.Forms.Label c_mensaje;

    }
}
