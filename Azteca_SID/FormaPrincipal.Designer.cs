﻿namespace Azteca_SID
{
    partial class FormaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaPrincipal));
            this.c_cronometro = new System.Windows.Forms.Timer(this.components);
            this.c_comenzar = new System.Windows.Forms.PictureBox();
            this.c_etv = new System.Windows.Forms.PictureBox();
            this.c_Admin = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gsi_cronometro = new System.Windows.Forms.Timer(this.components);
            this.c_version = new System.Windows.Forms.Label();
            this.c_stokMonedas = new System.Windows.Forms.Label();
            this.e_porcentaje = new System.Windows.Forms.Label();
            Globales.c_keepalive_G = new System.Windows.Forms.Timer(this.components);
            //this.c_keepalive = new System.Windows.Forms.Timer(this.components);
            this.bwsensado = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_comenzar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_etv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Admin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_cronometro
            // 
            this.c_cronometro.Interval = 3000;
            this.c_cronometro.Tick += new System.EventHandler(this.c_cronometro_Tick);
            // 
            // c_comenzar
            // 
            this.c_comenzar.BackColor = System.Drawing.Color.Transparent;
            this.c_comenzar.Image = global::Azteca_SID.Properties.Resources.btn_banorte_comenzar;
            this.c_comenzar.Location = new System.Drawing.Point(604, 509);
            this.c_comenzar.Name = "c_comenzar";
            this.c_comenzar.Size = new System.Drawing.Size(196, 79);
            this.c_comenzar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_comenzar.TabIndex = 2;
            this.c_comenzar.TabStop = false;
            this.c_comenzar.Click += new System.EventHandler(this.c_Comenzar_Click);
            // 
            // c_etv
            // 
            this.c_etv.BackColor = System.Drawing.Color.Transparent;
            this.c_etv.Location = new System.Drawing.Point(89, 564);
            this.c_etv.Name = "c_etv";
            this.c_etv.Size = new System.Drawing.Size(75, 34);
            this.c_etv.TabIndex = 3;
            this.c_etv.TabStop = false;
            this.c_etv.Click += new System.EventHandler(this.c_etv_Click);
            // 
            // c_Admin
            // 
            this.c_Admin.BackColor = System.Drawing.Color.Transparent;
            this.c_Admin.Location = new System.Drawing.Point(4, 564);
            this.c_Admin.Name = "c_Admin";
            this.c_Admin.Size = new System.Drawing.Size(75, 34);
            this.c_Admin.TabIndex = 4;
            this.c_Admin.TabStop = false;
            this.c_Admin.Click += new System.EventHandler(this.c_Admin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.PaleVioletRed;
            this.label2.Location = new System.Drawing.Point(289, 570);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 31);
            this.label2.TabIndex = 6;
            this.label2.Text = "0.00%";
            // 
            // gsi_cronometro
            // 
            this.gsi_cronometro.Interval = 300000;
            this.gsi_cronometro.Tick += new System.EventHandler(this.gsi_cronometro_Tick);
            // 
            // c_version
            // 
            this.c_version.AutoSize = true;
            this.c_version.BackColor = System.Drawing.Color.Transparent;
            this.c_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_version.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.c_version.Location = new System.Drawing.Point(9, 549);
            this.c_version.Name = "c_version";
            this.c_version.Size = new System.Drawing.Size(75, 18);
            this.c_version.TabIndex = 8;
            this.c_version.Text = "Version: ";
            // 
            // c_stokMonedas
            // 
            this.c_stokMonedas.AutoSize = true;
            this.c_stokMonedas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_stokMonedas.ForeColor = System.Drawing.Color.PaleVioletRed;
            this.c_stokMonedas.Location = new System.Drawing.Point(533, 571);
            this.c_stokMonedas.Name = "c_stokMonedas";
            this.c_stokMonedas.Size = new System.Drawing.Size(85, 29);
            this.c_stokMonedas.TabIndex = 9;
            this.c_stokMonedas.Text = "0.00%";
            // 
            // e_porcentaje
            // 
            this.e_porcentaje.AutoSize = true;
            this.e_porcentaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.e_porcentaje.ForeColor = System.Drawing.SystemColors.GrayText;
            this.e_porcentaje.Location = new System.Drawing.Point(377, 576);
            this.e_porcentaje.Name = "e_porcentaje";
            this.e_porcentaje.Size = new System.Drawing.Size(186, 20);
            this.e_porcentaje.TabIndex = 10;
            this.e_porcentaje.Text = "Procentaje monedas:";
            // 
            // c_keepalive
            // 
            
            Globales.c_keepalive_G.Interval = 300000; //300000;//60000;
            Globales.c_keepalive_G.Tick += new System.EventHandler(this.c_keepalive_Tick);
            // 
            // bwsensado
            // 
            this.bwsensado.WorkerReportsProgress = true;
            this.bwsensado.WorkerSupportsCancellation = true;
            this.bwsensado.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwsensado_DoWork);
            this.bwsensado.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwsensado_RunWorkerCompleted);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBox1.Image = global::Azteca_SID.Properties.Resources.LOGOGSI;
            this.pictureBox1.Location = new System.Drawing.Point(12, 512);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // FormaPrincipal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BackgroundImage = global::Azteca_SID.Properties.Resources._1;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.e_porcentaje);
            this.Controls.Add(this.c_stokMonedas);
            this.Controls.Add(this.c_version);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Admin);
            this.Controls.Add(this.c_etv);
            this.Controls.Add(this.c_comenzar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "FormaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormaPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.FormaPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c_comenzar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_etv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_Admin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer c_cronometro;
        private System.Windows.Forms.PictureBox c_comenzar;
        private System.Windows.Forms.PictureBox c_etv;
        private System.Windows.Forms.PictureBox c_Admin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer gsi_cronometro;
        private System.Windows.Forms.Label c_version;
        private System.Windows.Forms.Label c_stokMonedas;
        private System.Windows.Forms.Label e_porcentaje;
        //private System.Windows.Forms.Timer c_keepalive;
        private System.ComponentModel.BackgroundWorker bwsensado;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

