﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaUsuario : Azteca_SID.FormBase
    {
        String l_Identificacion = "";
        public FormaUsuario()
        {
            InitializeComponent();
        }

        private void Oprimir_tecla(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Identificacion += "0";
                c_EtiquetaPassword.Text += "0";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Identificacion += "1";
                c_EtiquetaPassword.Text += "1";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Identificacion += "2";
                c_EtiquetaPassword.Text += "2";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Identificacion += "3";
                c_EtiquetaPassword.Text += "3";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Identificacion += "4";
                c_EtiquetaPassword.Text += "4";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Identificacion += "5";
                c_EtiquetaPassword.Text += "5";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Identificacion += "6";
                c_EtiquetaPassword.Text += "6";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Identificacion += "7";
                c_EtiquetaPassword.Text += "7";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Identificacion += "8";
                c_EtiquetaPassword.Text += "8";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Identificacion += "9";
                c_EtiquetaPassword.Text += "9";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Identificacion.Length != 0)
                {
                    l_Identificacion = l_Identificacion.Substring(0, l_Identificacion.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Identificacion.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                if (l_Identificacion.Length > 0)
                {
                if(!Properties.Settings.Default.WebGSI)
                    if (!Globales.ExisteUsuario(l_Identificacion))
                    {
                        using (FormaError f_Error = new FormaError(true, "noUsuario"))
                        {
                            f_Error.c_MensajeError.Text = "El Usuario NO Existe";
                            f_Error.ShowDialog();
                        }

                    }
                    else
                        using (FormaIniciarSesion f_Inicio = new FormaIniciarSesion(true))
                        {
                             
                                f_Inicio.l_IdUsuario = l_Identificacion;
                            f_Inicio.ShowDialog(FormaPrincipal.s_Forma);
                        }
                    else
                    {
                        String l_mensajegsi;
                        String l_nombregsi;
                        Double l_montogsi;

                        if (Globales.ExisteUsuarioGsi(l_Identificacion, out l_mensajegsi , out l_nombregsi , out l_montogsi ) )
                        {
                            Globales.AutenticarUsuarioGsi(l_Identificacion , l_nombregsi , l_montogsi );

                            Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO", 1);


                            FormaPrincipal.s_Forma.Gsi_Cronometro( false );
                            using (FormaDeposito f_Deposito = new FormaDeposito())
                            {
                               
                                f_Deposito.ShowDialog(FormaPrincipal.s_Forma);


                            }
                            FormaPrincipal.s_Forma.Gsi_Cronometro( true );
                        }
                    else
                            using (FormaError f_Error = new FormaError(true, "noUsuario"))
                            {
                                f_Error.c_MensajeError.Text = l_mensajegsi;
                                f_Error.ShowDialog();
                            }


                    }



                    Close();

                }
                else
                {

                    using (FormaError f_Error = new FormaError(true, "texto"))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir un numero de Empleado/Cuenta para continuar";
                        f_Error.ShowDialog();
                    }
                  
                }
            }
            Cursor.Hide();
        }
    }
}
