﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormTeclado : Form
    {
        String m_captura;

        public String Captura
        {
            get { return m_captura; }
            set { m_captura = value; }
        }

        public FormTeclado()
        {
            InitializeComponent();
            
        }

        private void keyboardcontrol1_UserKeyPressed(object sender, KeyboardClassLibrary.KeyboardEventArgs e)
        {
            c_Texto.Focus();
            SendKeys.Send(e.KeyboardKeyPressed);
        }

        public void Password(bool p_pasword)
        {
            if (p_pasword )
                c_Texto.PasswordChar = '*';
        }

        private void c_Texto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                Captura = c_Texto.Text;
                    DialogResult = DialogResult.OK;
            }
            else if (e.KeyData == Keys.Escape)
                DialogResult = DialogResult.Cancel;
        }

        private void c_salir_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void c_cambiar_Click(object sender, EventArgs e)
        {
            Captura = c_Texto.Text;
            DialogResult = DialogResult.OK;
        }

        private void c_Texto_TextChanged( object sender , EventArgs e )
        {

        }

        private void FormTeclado_Load( object sender , EventArgs e )
        {
            TopMost = true;
        }
    }
}
