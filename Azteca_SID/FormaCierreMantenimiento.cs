﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormaCierreMantenimiento : Azteca_SID.FormBase
    {
        DataTable l_Mantenimientos;

        public FormaCierreMantenimiento()
        {
            InitializeComponent();
        }

        private void CargarDatos()
        {
            l_Mantenimientos = BDMantenimiento.ObtenerConsulta_mantenimientos_abiertos();

            c_folioMante.DataSource = l_Mantenimientos;
        }

    

        private void c_comentarios_Click(object sender, EventArgs e)
        {
            c_comentarios.Text = EntradaTexto();
        }

        private void c_cerrarMantenimento_Click(object sender, EventArgs e)
        {

            if ( l_Mantenimientos.Rows.Count < 1)
            {


                using ( FormaError f_Error = new FormaError( true , "error" ) )
                {
                    f_Error.c_MensajeError.Text = "Debe existir un Registro activo, por favor presione Salir";
                    f_Error.ShowDialog( );
                   
                }

                return;
            }


            if (!String.IsNullOrEmpty(c_comentarios.Text))
            {

                if (!BDMantenimiento.ActualizarMantenimiento((int)l_Mantenimientos.Rows[c_folioMante.SelectedIndex]["idRegistro"],DateTime.Now,c_comentarios.Text))
                {
                    using (FormaError f_Error = new FormaError(false, "error"))
                    {
                        f_Error.c_MensajeError.Text = "No se puede Cerrar el  Mantenimiento por favor reportelo por telefono";
                        f_Error.TopMost = true;
                        f_Error.ShowDialog();
                        return;
                    }
                }
                else
                {
                    Globales.Estatus =  Globales.EstatusReceptor.Desconocido;
                    try
                    {
                        Globales.CambioEstatusReceptor( (Globales.EstatusReceptor) Enum.Parse( typeof( Globales.EstatusReceptor )
                                                                , Properties.Settings.Default.UltimoEstadoConfirmado ) );
                        Globales.EscribirBitacora( "Cierre Mantenimiento" , "Cerrar Mantenimiento" , "Cambio de Estado a: " + Properties.Settings.Default.UltimoEstadoConfirmado , 3 );

                    }
                    catch (Exception exc)
                    {
                        Globales.EscribirBitacora("Cierre Mantenimiento","Cerrar Mantenimiento","Excepcion al leer ultimo Estado del Equipo: " +exc.Message,3 );
                    }

                    if (Properties.Settings.Default.Enviar_AlertasGSI)
                        UtilsComunicacion.AlertaGSI(92, "Cierre de Mantenimiento en Sitio");

                    using (FormaError f_Error = new FormaError(true, "exito"))
                    {
                        f_Error.c_MensajeError.Text = "Evento Cerrado  Exitosamente";
                        f_Error.ShowDialog();
                        Close();
                    }
                }


            }
            else {
                using (FormaError f_Error = new FormaError(true, "error"))
                {
                    f_Error.c_MensajeError.Text = "Debe Comentar por lo menos una palabra";
                    f_Error.ShowDialog();
                    return;
                }

            }
        }


        private String EntradaTexto()
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    return l_teclado.Captura;
                else
                    return "";

            }
        }

        private void FormaCierreMantenimiento_Load(object sender, EventArgs e)
        {
            CargarDatos();
            c_folioMante. DisplayMember = "idRegistro";
            c_folioMante.ValueMember = "idRegistro";

            if (c_folioMante.Items.Count > 0)
                c_folioMante.SelectedIndex = c_folioMante.Items.Count - 1;
            else
                using (FormaError f_Error = new FormaError(false, "Alto"))
                {
                    f_Error.c_MensajeError.Text = "";
                    f_Error.ShowDialog();
                    return;
                }
        }

        private void c_folioMante_SelectedIndexChanged(object sender, EventArgs e)
        {
            c_nombre.Text = l_Mantenimientos.Rows[c_folioMante.SelectedIndex]["Tecnico"].ToString();
            c_vendor .Text = l_Mantenimientos.Rows[c_folioMante.SelectedIndex]["Multivendor"].ToString();
            c_tipoMant .Text = l_Mantenimientos.Rows[c_folioMante.SelectedIndex]["TipoMantenimiento"].ToString();
        }
    }
}
