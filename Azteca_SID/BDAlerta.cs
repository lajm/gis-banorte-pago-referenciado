﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Azteca_SID
{
   public class BDAlerta
    {
        public static DataTable
           ObtenerAlerta(int p_Alerta)
        {
            String l_Query
                = "SELECT               AlertaEnviada, FechaHora,  Enviada , ProcessId "
                + "FROM                 Alerta "
                + "WHERE                IdAlerta = @IdAlerta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdAlerta", p_Alerta);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Alerta");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }



        public static DataTable
            BuscarAlertasNoEnviadas()
        {
            String l_Query
                = "SELECT               ProcessId, IdAlerta , AlertaEnviada "
                + "FROM                 Alerta "
                + "WHERE                Enviada = 0 "
                + "ORDER BY             IdAlerta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Alertas");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static bool
            ConfirmarAlerta(int p_IdAlerta)
        {
            String l_Query
                = "UPDATE               Alerta "
                + "SET                  Enviada = 1 "
                + "WHERE                IdAlerta = @IdAlerta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdAlerta", p_IdAlerta);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            ActualizarProcessId(int p_IdAlerta, Int64 p_IdProceso)
        {
            String l_Query
                = "UPDATE           Alerta "
                + "SET              ProcessId = @ProcessId "
                + "WHERE            IdAlerta = @IdAlerta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@ProcessId", p_IdProceso);
                l_Comando.Parameters.AddWithValue("@IdAlerta", p_IdAlerta);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            InsertarAlerta(String p_Mensaje, out int p_IdAlerta)
        {
            p_IdAlerta = FolioAlerta();
            String l_Query
                = "INSERT INTO          Alerta "
                + "                     (IdAlerta, FechaHora, AlertaEnviada, Enviada ) "
                + "VALUES               (@IdAlerta, @FechaHora, @AlertaEnviada,  0) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdAlerta", p_IdAlerta);
                l_Comando.Parameters.AddWithValue("@AlertaEnviada", p_Mensaje);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();
                    return true;
                }
            }
        }


        private static int
            FolioAlerta()
        {
            String l_Query
                = "SELECT MAX           (IdAlerta) "
                + "FROM                 Alerta ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                }
            }
            l_Folio++;
            return l_Folio;
        }
    }
}
