﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Azteca_SID
{
    public partial class FormBase : Form
    {
        public int l_Reply = -1;
        public String l_Error = "";

        public FormBase()
            :base()
        {
            InitializeComponent();
            try
            {
                c_cierreAutomatico.Interval = Properties.Settings.Default.DelayGeneral * 1000;
            
            } catch (Exception exx)
            {
                c_cierreAutomatico.Interval = 30 * 1000;

            }
        }

        public FormBase(bool p_cierreAutomatico) : this()
        {
            c_cierreAutomatico.Enabled = true;
        }

        public void Forzardibujado()
        {
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();

        }

        public void Reiniciartimer()
        {
            c_cierreAutomatico.Stop();
            c_cierreAutomatico.Start();
        }

        private void c_cierreAutomatico_Tick(object sender, EventArgs e)
        {
            c_cierreAutomatico.Stop();
            Forzardibujado();
            Close();


        }

        public void MostarMensaje (String p_mensaje, bool p_autoCierre, String p_nombreImagen)
        {
            using (FormaError l_ventanaError = new FormaError(p_autoCierre, p_nombreImagen))
            {
                l_ventanaError.c_MensajeError.Text = p_mensaje;
                l_ventanaError.TopMost = true;
                l_ventanaError.ShowDialog();

            }
        }

        private void c_opcionSalir_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            Close();
            Globales.c_keepalive_G.Enabled = true;
        }
    }
}
