﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using IrdsMensajes;

namespace Azteca_SID
{
    public class BDRetiro
    {
        public static DataTable
            BuscarRetirosNoEnviados()
        {
            String l_Query
                = "SELECT               ProcessId, IdRetiro, IdMoneda, Total, IdUsuario, Equipo "
                + "FROM                 Retiro "
                + "WHERE                Enviado = 0 "
                + "ORDER BY             IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Retiros");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
            ObtenerRetiro(int p_IdRetiro)
        {
            String l_Query
                = "SELECT               IdUsuario, , IdMoneda, FechaHora, Total, Enviado "
                + "FROM                 Retiro "
                + "WHERE                IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Retiro");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
     ObtenerDetalleRetiro(int p_IdRetiro)
        {
            String l_Query
                = "SELECT           a.IdDenominacion, a.Cantidad, b.Codigo "
                + "FROM             Denominacion b, DetalleRetiro a "
                + "WHERE            b.IdDenominacion = a.IdDenominacion "
                + "AND              a.IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
              
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("DetalleRetiro");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
            ObtenerDetalle_Retiro(int p_IdRetiro, String p_Equipo)
        {
            String l_Query
                = "SELECT           a.IdDenominacion, a.Cantidad, b.Codigo "
                + "FROM             Denominacion b, DetalleRetiro a "
                + "WHERE            b.IdDenominacion = a.IdDenominacion "
                + "AND              b.Equipo =@Equipo "
                + "AND              a.IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("DetalleRetiro");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static bool
            ConfirmarRetiro(int p_IdRetiro)
        {
            String l_Query
                = "UPDATE               Retiro "
                + "SET                  Enviado = 1 "
                + "WHERE                IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            ActualizarProcessId(int p_IdRetiro, Int64 p_IdProceso)
        {
            String l_Query
                = "UPDATE           Retiro "
                + "SET              ProcessId = @ProcessId "
                + "WHERE            IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@ProcessId", p_IdProceso);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        internal static bool InsetarRetiroGsi( string p_GsiCajero, string p_Divisa, string p_Fecha, string p_Folio
                                            , Double p_Montototal, string p_NumeroSerieBOLSA, bool p_Exitoso)
        {

            String l_Query
                = "INSERT INTO         RetiroGSI "
                + "                     (IdRetiro, GsiCajero, Monto, Divisa, Fecha,  Envase, Pendiente) "
                + "VALUES             (@Folio, @GsiCajero, @Monto,@Divisa, @Fecha,  @Envase, @Pendiente) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {

                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
               
                l_Comando.Parameters.AddWithValue("@GsiCajero", p_GsiCajero);
                l_Comando.Parameters.AddWithValue("@Monto", p_Montototal);
                l_Comando.Parameters.AddWithValue("@Divisa", p_Divisa);
                l_Comando.Parameters.AddWithValue("@Fecha", p_Fecha);
                l_Comando.Parameters.AddWithValue("@Folio", int.Parse(p_Folio));
                l_Comando.Parameters.AddWithValue("@Envase", p_NumeroSerieBOLSA);
                l_Comando.Parameters.AddWithValue("@Pendiente", p_Exitoso);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    return false;
                }
                else
                {
                    return true;

                }
            }
        }


        public static bool
            InsertarRetiro(int p_IdMoneda, Double p_TotalRetiro
                , DataTable p_DatosDetalle, int p_IdCajon, out int p_IdRetiro)
        {
            p_IdRetiro = FolioRetiro();
            String l_Query
                = "INSERT INTO          Retiro "
                + "                     (IdRetiro, IdUsuario, IdMoneda, IdCajon, FechaHora, Total, Enviado,Equipo) "
                + "VALUES               (@IdRetiro, @IdUsuario, @IdMoneda, @IdCajon, @FechaHora, @Total, 0, @Equipo) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                l_Comando.Parameters.AddWithValue("@IdUsuario", Globales.IdUsuario);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@IdCajon", p_IdCajon);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                l_Comando.Parameters.AddWithValue("@Total", p_TotalRetiro);
                l_Comando.Parameters.AddWithValue("@Equipo", "Equipo1");

                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                if (!InsertarDetalleRetiro(p_DatosDetalle, p_IdRetiro, l_Conexion, l_Transaccion))
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();

                    InsertarCiclo(p_IdRetiro);
                    ActualizarCiclo();

                    MensajeColeccion2 l_Irds = new MensajeColeccion2(
                        Properties.Settings.Default.Estacion
                        , Properties.Settings.Default.Sucursal
                        , FolioCiclo()
                        , p_IdRetiro
                        , Globales.FechaHoraSesion
                        , DateTime.Now
                        , Moneda.MonedaPesos.ToString()
                        , Globales.IdUsuario
                        , Globales.NombreUsuario
                        , (decimal)BDDeposito.ObtenerMontoActual()
                        , (decimal)(p_TotalRetiro * -1)
                        , 0    // Deposito Manual
                        , 0);


                    foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
                    {
                        int l_den = 0;
                        switch ( l_Detalle[2].ToString( ) )
                        {
                            case "0.10":
                            l_den = 1;
                            break;
                            case "0.20":
                            l_den = 2;
                            break;
                            case "0.5":
                            l_den = 3;
                            break;
                            case "1":
                            l_den = 4;
                            break;
                            case "2":
                            l_den = 5;
                            break;
                            case "5":
                            l_den = 6;
                            break;
                            case "10":
                            l_den = 7;
                            break;
                            case "20.00":
                            l_den = 8;
                            break;
                            case "20":
                            l_den = 20;
                            break;
                            case "50":
                            l_den = 50;
                            break;
                            case "100":
                            l_den = 100;
                            break;
                            case "200":
                            l_den = 200;
                            break;
                            case "500":
                            l_den = 500;
                            break;
                            case "1000":
                            l_den = 1000;
                            break;

                        }
                        l_Irds.AgregarDenominacion(1, l_den, (Int32)l_Detalle[1]); // 10 de 100$ en contenedor 1
                    }

                    Globales.EscribirBitacora("Retiro", "Informacion", "Preparando mensaje a Portal", 1);

                    try
                    {
                        String l_Json = l_Irds.CodificarJson();

                        string[] l_Servers = Properties.Settings.Default.Servers.Split(',');
                        foreach (String l_Servidor in l_Servers)
                        {
                            Globales.EscribirBitacora("Retiro", "Informacion", "Preparando Sevidor:" + l_Servidor, 1);
                            int l_IdServidor = Convert.ToInt32(l_Servidor);
                            ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(l_IdServidor);
                            Globales.EscribirBitacora("Retiro", "Informacion", "Encolando a Servidor:" + l_Servidor, 1);
                            l_Cola.AgregarMensaje(MensajeIrds.CategoriaMensajeEnum.Transaccion
                           , MensajeIrds.TipoMensajeEnum.Transaccion_Coleccion
                           , 2
                           , l_Json);
                            Globales.EscribirBitacora("Retiro", "Informacion", "Enviando Mensajes Pendientes al Sevidor:" + l_Servidor, 1);
                            l_Cola.MandarMensajesPendientes(Properties.Settings.Default.Estacion);
                            Globales.EscribirBitacora("Retiro", "Informacion", "Termino y Respuesta del Sevidor:" + l_Servidor, 1);
                        }
                    }
                    catch (Exception p)
                    {
                        Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", p.Message, 1);
                    }

                    //MensajeDeposito1 l_Irds = new MensajeDeposito1(
                    //Properties.Settings.Default.Estacion     // id estacion
                    //, p_IdRetiro   // folio transacción
                    //, FolioCiclo()    // ciclo operacional
                    //, DateTime.Now.AddMinutes(-3) // fecha hora inicio
                    //, DateTime.Now // fecha hora fin
                    //, Properties.Settings.Default.IdUsuario // string p_IdOperadorLocal
                    //, Properties.Settings.Default.NombreUsuario   // String nombre completo del operador local
                    //, "SuperUser"               // Usuario global del sistema que realiza la operación o que creo el operador local
                    //, Moneda.MonedaPesos // Moneda p_IdMoneda
                    //, 1 // IdCuenta  ID Global cuenta del sistema
                    //, Properties.Settings.Default.ReferenciaUsuario //string p_Referencia
                    //, (decimal)BDDeposito.ObtenerMontoActual() // decimal p_SaldoAnterior
                    //, (decimal)(p_TotalRetiro*-1) // decimal p_MontoTransaccion
                    //, 0
                    //, 0         // int m_TotalIncidentes
                    //);

                    //foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
                    //{

                    //    l_Irds.AgregarDenominacion(1, (int)l_Detalle[0], (Int32)l_Detalle[1]); // 10 de 100$ en contenedor 1
                    //}

                    //ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(1);

                    //l_Cola.AgregarMensaje(l_Irds);

                    //try
                    //{
                    //    ColaMensajesIrdsHttp l_Cola_envios = new ColaMensajesIrdsHttp(2);

                    //    l_Cola_envios.MandarMensajesPendientes(Properties.Settings.Default.Estacion);
                    //   Globales.EscribirBitacora("Mensajes Pendientes ", "Lanzados ", "Sin Problemas", 1);

                    //}
                    //catch (Exception exxx)
                    //{
                    //   Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", exxx.Message, 1);
                    //}

                    return true;
                   
                }
            }
        }

        internal static void UpdateRetiroGSI(int p_folio)
        {
            String l_Query
                = "UPDATE               RetiroGSI "
                + "SET                  Pendiente = 0 "
                 + "WHERE               IdRetiro = @IdRetiro ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                 l_Comando.Parameters.AddWithValue("@IdRetiro", p_folio);
                l_Comando.ExecuteNonQuery();
            }
        }

        public static bool
          InsertarRetiroManual(int p_IdMoneda, Double p_TotalRetiro
              , DataTable p_DatosDetalle, int p_IdCajon, out int p_IdRetiro)
        {
            int l_Contenedor = 0;

            l_Contenedor = 0001;


            p_IdRetiro = FolioRetiro();
            String l_Query
                = "INSERT INTO          Retiro "
                + "                     (IdRetiro, IdUsuario, IdMoneda, IdCajon, FechaHora, Total, Enviado,Equipo) "
                + "VALUES               (@IdRetiro, @IdUsuario, @IdMoneda, @IdCajon, @FechaHora, @Total, 0,@Equipo) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                l_Comando.Parameters.AddWithValue("@IdUsuario", Globales.IdUsuario);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@IdCajon", p_IdCajon);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                l_Comando.Parameters.AddWithValue("@Total", p_TotalRetiro);
                l_Comando.Parameters.AddWithValue("@Equipo","Equipo1" );
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                if (!InsertarDetalleRetiro(p_DatosDetalle, p_IdRetiro, l_Conexion, l_Transaccion))
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();

                    InsertarCiclo(p_IdRetiro);


                    //BDIRDS.InsertarTransaccion(Globales.Num_Estacion, UtilsIRDS.TipoTransaccion.Retiro, p_IdRetiro, FolioCiclo(), DateTime.Now.AddMinutes(-5), DateTime.Now, 0
                    //             , p_TotalRetiro, 0, 0, "Transaccion exitosa", p_DatosDetalle, l_Contenedor);




                    return true;
                }
            }
        }

        public static bool
            Insertar_Retiro(int p_IdMoneda, Double p_TotalRetiro
                , DataTable p_DatosDetalle, int p_IdCajon, out int p_IdRetiro, String p_Equipo)
        {
            p_IdRetiro = FolioRetiro();
            String l_Query
                = "INSERT INTO          Retiro "
                + "                     (IdRetiro, IdUsuario, IdMoneda, IdCajon, FechaHora, Total, Enviado,Equipo) "
                + "VALUES               (@IdRetiro, @IdUsuario, @IdMoneda, @IdCajon, @FechaHora, @Total, 0,@Equipo) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                l_Comando.Parameters.AddWithValue("@IdUsuario", Globales.IdUsuario);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@IdCajon", p_IdCajon);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                l_Comando.Parameters.AddWithValue("@Total", p_TotalRetiro);
                l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                if (!InsertarDetalleRetiro(p_DatosDetalle, p_IdRetiro, l_Conexion, l_Transaccion))
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();
                    return true;
                }
            }
        }
        public static DataTable
            NuevoRetiro()
        {
            String l_Query
                = "SELECT               IdCajon, IdMoneda, SUM(TotalDeposito) "
                + "FROM                 Deposito "
                + "WHERE                Retirado = 0 AND Equipo = 'Equipo1' "
                + "GROUP BY             IdCajon, IdMoneda ";
            DataTable l_Datos = new DataTable("Retiro");
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                    l_Adaptador.Fill(l_Datos);
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Nuevo Retiro", "Error Consulta : ", ex.Message, 1);
                }
            }
            return l_Datos;
        }

        public static DataTable
           Nuevo_Retiro(String p_Equipo)
        {
            String l_Query
                = "SELECT               IdCajon, IdMoneda, SUM(TotalDeposito) "
                + "FROM                 Deposito "
                + "WHERE                Retirado = 0 AND Equipo = @Equipo "
                + "GROUP BY             IdCajon, IdMoneda ";
            DataTable l_Datos = new DataTable("Retiro");
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    l_Comando.Parameters.AddWithValue("@Equipo",p_Equipo);
                    SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                  
                    l_Adaptador.Fill(l_Datos);
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Nuevo Retiro", "Error Consulta : ", ex.Message, 1);
                }
            }
            return l_Datos;
        }
        public static void
            ActualizarRetiros()
        {
            String l_Query
                = "UPDATE               Deposito "
                + "SET                  Retirado = 1 "
                 + "WHERE               Manual = 0 OR Manual is NULL";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
               // l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                l_Comando.ExecuteNonQuery();
            }
        }

        public static void
      ActualizarRetirosManuales()
        {
            String l_Query
                = "UPDATE               Deposito "
                + "SET                  Retirado = 1 "
                + "WHERE                Manual =1 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
              
                l_Comando.ExecuteNonQuery();
            }
        }

        private static bool
            InsertarDetalleRetiro(DataTable p_DatosDetalle, int p_IdRetiro, SqlConnection p_Conexion
                , SqlTransaction p_Transaccion)
        {
            String l_Query
                = "INSERT INTO              DetalleRetiro "
                + "                         (IdRetiro, IdDenominacion, Cantidad) "
                + "VALUES                   (@IdRetiro, @IdDenominacion, @Cantidad) ";

            foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, p_Conexion, p_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_IdRetiro);
                l_Comando.Parameters.AddWithValue("@IdDenominacion", int.Parse(l_Detalle[0].ToString()));
                l_Comando.Parameters.AddWithValue("@Cantidad", int.Parse(l_Detalle[1].ToString()));
                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Insertar Detalle Retiro", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }

            return true;
        }

        private static int
            FolioRetiro()
        {
            String l_Query
                = "SELECT MAX           (IdRetiro) "
                + "FROM                 Retiro ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Folo RETIRO", "Error Consulta : ", ex.Message, 1);
                }
            }
            l_Folio++;
            return l_Folio;
        }

        public static void
          ActualizarCiclo()
        {
            int p_IdCiclo = FolioCiclo();
            String l_Query
                = "Insert INTO   CicloOperacion "
                + " (IdCicloOperacion, IdDeposito, IdRetiro) "
                + "VALUES (@IdCicloOperacion,0,0)";
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCicloOperacion", ++p_IdCiclo);
                l_Comando.ExecuteNonQuery();
            }


        }

        public static void
           InsertarCiclo(int p_folioRetiro)
        {
            int p_IdCiclo = FolioCiclo();
            String l_Query
                = "Insert INTO   CicloOperacion "
                + " (IdCicloOperacion, IdDeposito, IdRetiro) "
                + "VALUES (@IdCicloOperacion,0,@idRetiro)";
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCicloOperacion", p_IdCiclo++);
                l_Comando.Parameters.AddWithValue("@IdRetiro", p_folioRetiro);
                l_Comando.ExecuteNonQuery();
            }

        }

        private static int
           FolioCiclo()
        {
            String l_Query
                = "SELECT MAX           (IdCicloOperacion) "
                + "FROM                 CicloOperacion ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                   Globales.EscribirBitacora("Folo CicloOperacion", "Error Consulta : ", ex.Message, 1);
                }
            }

            return l_Folio;
        }
   
    }
}
