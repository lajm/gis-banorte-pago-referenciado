﻿using SidApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Azteca_SID
{
    class SensarSIDcs
    {
       
            public class SidHLException : Exception
            {
                public SidHLException(int p_ErrorCode, String p_ErrorMessage)
                    : base(p_ErrorCode + " - " + p_ErrorMessage)
                {
                }
            }

            static int m_LastError;

            public const int MAX_DENOMINATION = 10;

            public const int DEN_MX_Pendiente = -1;
            public const int DEN_MX_20 = 0;
            public const int DEN_MX_50 = 1;
            public const int DEN_MX_100 = 2;
            public const int DEN_MX_200 = 3;
            public const int DEN_MX_500 = 4;
            public const int DEN_MX_1000 = 5;
            public const int DEN_MX_UNKNOWN = 6;
            public const int MAX_DEN_MX = 6;

            public static int[] DenominationsMX = { 20, 50, 100, 200, 500, 1000 };


        public static bool
         IsDoorOpen()
        {

            return GetStatus().m_Sensor_door_open;
        }

        public static bool
        IsLockOpen( )
        {

            return GetStatus( ).m_Code_lock_correct;
        }

        public static bool
            IsBagReadyForDeposit()
        {
            UnitStatus l_Status = GetStatus();


            if ((l_Status.m_SenseKey == SenseKeyEnum.Unit_Ok)
                && (l_Status.m_Sensor_bag_present == true)
                //&& (l_Status.m_Dip_Switch_elevator_position_DOWN == true)
                && (l_Status.m_Sensor_bag_ready_for_deposit == true))
                return true;
            else
                return false;
        }

        public enum SenseKeyEnum
            {
                Unit_Ok,
                Unit_busy,
                Paper_jam,
                Hardware_error,
                Illegal_request,
                Document_not_present,
                Double_Leafing_warning,
                Jam_in_feeder_sensor_not_covered,
                Jam_in_switch_path_sensor_not_covered,
                Jam_in_withdraw_pocket_sensor_not_covered,
                Jam_in_Bag_Hole_sensor_never_covered,
                Document_too_long,
                Press_notes_Blocked,
                Jam_in_exit_feeder_sensor_not_uncovered,
                Exit_switch_Blocked_sensor_not_uncovered,
                Jam_in_Reject_bay_sensor_not_uncovered,
                Jam_in_Bag_Hole_sensor_not_uncovered,
                Reject_bay_full,
                Jam_in_front_scanners,
                Weld_not_done_for_document_in_the_path,
                Weld_not_done_because_bag_empty,
                Time_Out_expired_bag_opening_or_closing,
                Low_Power_for_weld_the_bag,
                Bar_weld_in_safe_protection,
                Time_Out_Open_Door_expired,
                Micro_switch_door_Leafer_open,
                Micro_switch_1_door_magnetic_head_open,
                Micro_switch_2_door_magnetic_head_open,
                Micro_switch_Head_Unit_open,
                Interlock_open_test_the_SensorSatus_byte_2_or_9_to_see_which_open,
                Time_Out_expired_during_elevator_movement,
                Error_during_bag_tensioner_movement,
                Magnetic_Sensor_not_calibrate,
                Illegal_request_with_Strongbox_Door_Open,
                Illegal_request_with_Bag_not_Present,
                Illegal_request_with_Bag_Closed,
                Illegal_request_with_Bag_not_completely_Open,
                Illegal_request_with_Safe_lock_Open,
                Illegal_request_with_Lifter_Up,
                Illegal_request_with_Lifter_not_completely_Down,
                Illegal_request_with_Bag_present,
                Illegal_request_with_Safe_lock_Code_not_Inserted,
                Unknwon
            }


            public class UnitStatus
            {
                public SenseKeyEnum m_SenseKey;
                public bool m_Entrance_Sensor_1;
                public bool m_Entrance_Sensor_2;
                public bool m_Shutter_Sensor_1;
                public bool m_Feeder_Flap_Sensor;
                public bool m_Reject_Entrance_Sensor;
                public bool m_Bag_Entrance_Sensor_1;
                public bool m_Feeder_Sensor;
                public bool m_Feeder_Empty_Sensor;  // (note: if the banknotes aren’t pushed until the end or are
                                                    // not perfectly stretched it’s possible that this sensor might not be
                                                    // covered)
                public bool m_Double_Leafing_Sensor_1;
                public bool m_Double_Leafing_Sensor_2;
                public bool m_Double_Leafing_Sensor_3;
                public bool m_Bag_Tensioner_1_UP;
                public bool m_Bag_Tensioner_2_UP;
                public bool m_Reject_Bay_empty;
                public bool m_Shutter_Sensor_2;
                public bool m_Bag_sensor_1_present;    // (0 bag present 1 no bag).
                public bool m_Bag_sensor_2_present;    // (0 bag present 1 no bag).
                public bool m_Bag_sensor_3_present;    // (0 bag present 1 no bag).
                public bool m_Bag_sensor_4_present;    // (0 bag present 1 no bag).
                public bool m_Bag_sensor_5_present;    // (0 bag present 1 no bag).
                public bool m_Bag_sensor_6_present;    // (0 bag present 1 no bag).
                public bool m_Counter_Motor_Step_bag_sensor_present;
                public bool m_Interlock_door_strongbox_closed;
                public bool m_Validation_progress;
                public byte m_Currency;                // where n identifies the currency (range 1 to 4)
                                                       // where m identifies the denomination (range depends on type of
                                                       // currency)
                public bool m_Note_to_much_swewed;       // 0xe0 No Note.
                public bool m_Note_suspect;  // 0xe2 Note Suspect recognized.
                public bool m_Note_wrong_dimension;  // 0xe3 Note Suspect recognized.
                public bool m_Note_No_Magnetic_found;  // 0xe4 Note Suspect recognized.
                public bool m_Note_IR_wrong;  // 0xe5 Note Suspect recognized.
                public bool m_time_out;         // 0xff time-out, more time for recognize the note, see SenSorStatus byte 5

                public bool m_Sensor_door_open;
                public bool m_Sensor_bag_present;      // 0 present, 1 missing
                public bool m_Sensor_bag_open;         // 0 close ready for weld, 1 open
                public bool m_Sensor_bag_ready_for_deposit; // 0 open ready for introduce notes, 1 close
                public bool m_Code_lock_correct;                   //  correct equal to 0
                public bool m_Alarm_Code_entered;      // (CENCOM – normal code + 10)
                public bool m_Dip_Switch_elevator_position_UP;
                public bool m_Dip_Switch_elevator_position_DOWN;

                public bool m_Sensor_bag_1;
                public bool m_Sensor_bag_2;
                public bool m_Sensor_bag_3;
                public bool m_Sensor_bag_4;
                public bool m_Sensor_bag_5;
                public bool m_Sensor_bag_6;
                public bool m_Sensor_bag_7;
                public bool m_Sensor_bag_8;
                public bool m_Sensor_Dip_Switch_Leafer_open;
                public bool m_Sensor_Dip_Switch_Magnetic_door_1_open;
                public bool m_Sensor_Dip_Switch_Magnetic_door_2_open;
                public bool m_Bag_Entrance_Sensor_2;
                public bool m_Sensor_Dip_Switch_Unit_Open;
                public bool m_Reject_EntranceSensor_2;
                public bool m_Interlock_cover_head_open;
                public byte m_Percentage_of_welding_executed;
            }


            public static UnitStatus
                GetStatus()
            {
                Byte[] l_key = new Byte[1];
                Byte[] l_sensor = new Byte[64];

                UnitStatus l_Status = new UnitStatus();


                if (Open())
                {

                    m_LastError = SidLib.SID_UnitStatus(l_key, l_sensor, null, null, null);

                    switch (l_key[0].ToString("X"))
                        {
                            case "00":
                                l_Status.m_SenseKey = SenseKeyEnum.Unit_Ok;
                                break;
                            case "02":
                                l_Status.m_SenseKey = SenseKeyEnum.Unit_busy;
                                break;
                            case "03":
                                l_Status.m_SenseKey = SenseKeyEnum.Paper_jam;
                                break;
                            case "04":
                                l_Status.m_SenseKey = SenseKeyEnum.Hardware_error;
                                break;
                            case "05":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request;
                                break;
                            case "06":
                                l_Status.m_SenseKey = SenseKeyEnum.Document_not_present;
                                break;
                            case "19":
                                l_Status.m_SenseKey = SenseKeyEnum.Double_Leafing_warning;
                                break;
                            case "40":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_feeder_sensor_not_covered;
                                break;
                            case "41":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_switch_path_sensor_not_covered;
                                break;
                            case "42":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_withdraw_pocket_sensor_not_covered;
                                break;
                            case "43":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Bag_Hole_sensor_never_covered;
                                break;
                            case "48":
                                l_Status.m_SenseKey = SenseKeyEnum.Document_too_long;
                                break;
                            case "49":
                                l_Status.m_SenseKey = SenseKeyEnum.Press_notes_Blocked;
                                break;
                            case "4A":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_exit_feeder_sensor_not_uncovered;
                                break;
                            case "4B":
                                l_Status.m_SenseKey = SenseKeyEnum.Exit_switch_Blocked_sensor_not_uncovered;
                                break;
                            case "4C":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Reject_bay_sensor_not_uncovered;
                                break;
                            case "4D":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Bag_Hole_sensor_not_uncovered;
                                break;
                            case "4E":
                                l_Status.m_SenseKey = SenseKeyEnum.Reject_bay_full;
                                break;
                            case "4F":
                                l_Status.m_SenseKey = SenseKeyEnum.Jam_in_front_scanners;
                                break;
                            case "70":
                                l_Status.m_SenseKey = SenseKeyEnum.Weld_not_done_for_document_in_the_path;
                                break;
                            case "71":
                                l_Status.m_SenseKey = SenseKeyEnum.Weld_not_done_because_bag_empty;
                                break;
                            case "72":
                                l_Status.m_SenseKey = SenseKeyEnum.Time_Out_expired_bag_opening_or_closing;
                                break;
                            case "73":
                                l_Status.m_SenseKey = SenseKeyEnum.Low_Power_for_weld_the_bag;
                                break;
                            case "74":
                                l_Status.m_SenseKey = SenseKeyEnum.Bar_weld_in_safe_protection;
                                break;
                            case "75":
                                l_Status.m_SenseKey = SenseKeyEnum.Time_Out_Open_Door_expired;
                                break;
                            case "76":
                                l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_door_Leafer_open;
                                break;
                            case "77":
                                l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_1_door_magnetic_head_open;
                                break;
                            case "78":
                                l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_2_door_magnetic_head_open;
                                break;
                            case "79":
                                l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_Head_Unit_open;
                                break;
                            case "7A":
                                l_Status.m_SenseKey = SenseKeyEnum.Interlock_open_test_the_SensorSatus_byte_2_or_9_to_see_which_open;
                                break;
                            case "7B":
                                l_Status.m_SenseKey = SenseKeyEnum.Time_Out_expired_during_elevator_movement;
                                break;
                            case "7C":
                                l_Status.m_SenseKey = SenseKeyEnum.Error_during_bag_tensioner_movement;
                                break;
                            case "7D":
                                l_Status.m_SenseKey = SenseKeyEnum.Magnetic_Sensor_not_calibrate;
                                break;
                            case "90":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Strongbox_Door_Open;
                                break;
                            case "91":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_not_Present;
                                break;
                            case "92":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_Closed;
                                break;
                            case "93":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_not_completely_Open;
                                break;
                            case "94":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Safe_lock_Open;
                                break;
                            case "95":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Lifter_Up;
                                break;
                            case "96":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Lifter_not_completely_Down;
                                break;
                            case "97":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_present;
                                break;
                            case "98":
                                l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Safe_lock_Code_not_Inserted;
                                break;

                        }

                        //m_sensekey = l_Status.m_SenseKey;
                        //SENSOR [0]
                        l_Status.m_Entrance_Sensor_1 = (l_sensor[0] & 0x01) != 0 ? true : false;
                        l_Status.m_Entrance_Sensor_2 = (l_sensor[0] & 0x02) != 0 ? true : false;
                        l_Status.m_Shutter_Sensor_1 = (l_sensor[0] & 0x04) != 0 ? true : false;
                        l_Status.m_Feeder_Flap_Sensor = (l_sensor[0] & 0x08) != 0 ? true : false;
                        l_Status.m_Reject_Entrance_Sensor = (l_sensor[0] & 0x10) != 0 ? true : false;
                        l_Status.m_Bag_Entrance_Sensor_1 = (l_sensor[0] & 0x20) != 0 ? true : false;
                        l_Status.m_Feeder_Sensor = (l_sensor[0] & 0x40) != 0 ? true : false;
                        l_Status.m_Feeder_Empty_Sensor = (l_sensor[0] & 0x80) != 0 ? true : false;

                        //SENSOR [1]
                        l_Status.m_Double_Leafing_Sensor_1 = (l_sensor[1] & 0x01) != 0 ? true : false;
                        l_Status.m_Double_Leafing_Sensor_2 = (l_sensor[1] & 0x02) != 0 ? true : false;
                        l_Status.m_Double_Leafing_Sensor_3 = (l_sensor[1] & 0x04) != 0 ? true : false;
                        // l_Status = (l_sensor[1] & 0x08) != 0 ? true : false;
                        l_Status.m_Bag_Tensioner_1_UP = (l_sensor[1] & 0x10) != 0 ? true : false;
                        l_Status.m_Bag_Tensioner_2_UP = (l_sensor[1] & 0x20) != 0 ? true : false;
                        l_Status.m_Reject_Bay_empty = (l_sensor[1] & 0x40) != 0 ? true : false;
                        l_Status.m_Shutter_Sensor_2 = (l_sensor[1] & 0x80) != 0 ? true : false;
                        //SENSOR [2]  //INVERTIDO 
                        l_Status.m_Bag_sensor_1_present = (l_sensor[2] & 0x01) != 0 ? false : true;
                        l_Status.m_Bag_sensor_2_present = (l_sensor[2] & 0x02) != 0 ? false : true;
                        l_Status.m_Bag_sensor_3_present = (l_sensor[2] & 0x04) != 0 ? false : true;
                        l_Status.m_Bag_sensor_4_present = (l_sensor[2] & 0x08) != 0 ? false : true;
                        l_Status.m_Bag_sensor_5_present = (l_sensor[2] & 0x10) != 0 ? false : true;
                        l_Status.m_Bag_sensor_6_present = (l_sensor[2] & 0x20) != 0 ? false : true;
                        // l_Status = (l_sensor[2] & 0x40) != 0 ? true : false;
                        l_Status.m_Interlock_door_strongbox_closed = (l_sensor[2] & 0x80) != 0 ? true : false;

                        //SENSOR [6] validation Banknote
                        l_Status.m_Validation_progress = (l_sensor[6] & 0x00) != 0 ? true : false;
                        l_Status.m_Currency = l_sensor[6];
                        l_Status.m_Note_to_much_swewed = (l_sensor[6] & 0xe0) != 0 ? false : true;
                        l_Status.m_Note_suspect = (l_sensor[6] & 0xe2) != 0 ? true : false;
                        l_Status.m_Note_wrong_dimension = (l_sensor[6] & 0xe3) != 0 ? true : false;
                        l_Status.m_Note_No_Magnetic_found = (l_sensor[6] & 0xe4) != 0 ? true : false;
                        l_Status.m_Note_IR_wrong = (l_sensor[6] & 0xe5) != 0 ? true : false;
                        l_Status.m_Validation_progress = (l_sensor[6] & 0x00) != 0 ? true : false;

                        //SENSOR [7]] Sensor strongbox and bag
                        l_Status.m_Sensor_door_open = (l_sensor[7] & 0x01) != 0 ? true : false;
                        l_Status.m_Sensor_bag_present = (l_sensor[7] & 0x02) != 0 ? false : true;
                        l_Status.m_Sensor_bag_open = (l_sensor[7] & 0x04) != 0 ? true : false;
                        l_Status.m_Sensor_bag_ready_for_deposit = (l_sensor[7] & 0x08) != 0 ? false : true;
                        l_Status.m_Code_lock_correct = (l_sensor[7] & 0x10) != 0 ? false : true;
                        l_Status.m_Alarm_Code_entered = (l_sensor[7] & 0x20) != 0 ? false : true;
                        l_Status.m_Dip_Switch_elevator_position_UP = (l_sensor[7] & 0x40) != 0 ? true : false;
                        l_Status.m_Dip_Switch_elevator_position_DOWN = (l_sensor[7] & 0x80) != 0 ? true : false;

                        //SENSOR [8] BAG SENSORS
                        l_Status.m_Sensor_bag_1 = (l_sensor[8] & 0x01) != 0 ? true : false;
                        l_Status.m_Sensor_bag_2 = (l_sensor[8] & 0x02) != 0 ? true : false;
                        l_Status.m_Sensor_bag_3 = (l_sensor[8] & 0x04) != 0 ? true : false;
                        l_Status.m_Sensor_bag_4 = (l_sensor[8] & 0x08) != 0 ? true : false;
                        l_Status.m_Sensor_bag_5 = (l_sensor[8] & 0x10) != 0 ? true : false;
                        l_Status.m_Sensor_bag_6 = (l_sensor[8] & 0x20) != 0 ? true : false;
                        l_Status.m_Sensor_bag_7 = (l_sensor[8] & 0x40) != 0 ? true : false;
                        l_Status.m_Sensor_bag_8 = (l_sensor[8] & 0x80) != 0 ? true : false;
                        //SENSOR [9]
                        l_Status.m_Sensor_Dip_Switch_Leafer_open = (l_sensor[9] & 0x01) != 0 ? true : false;
                        l_Status.m_Sensor_Dip_Switch_Magnetic_door_1_open = (l_sensor[9] & 0x02) != 0 ? true : false;
                        l_Status.m_Sensor_Dip_Switch_Magnetic_door_2_open = (l_sensor[9] & 0x04) != 0 ? true : false;
                        l_Status.m_Bag_Entrance_Sensor_2 = (l_sensor[9] & 0x08) != 0 ? true : false;
                        l_Status.m_Sensor_Dip_Switch_Unit_Open = (l_sensor[9] & 0x10) != 0 ? true : false;
                        l_Status.m_Reject_EntranceSensor_2 = (l_sensor[9] & 0x20) != 0 ? true : false;
                        l_Status.m_Interlock_cover_head_open = (l_sensor[9] & 0x40) != 0 ? true : false;
                        // l_Status= (l_sensor[9] & 0x80) != 0 ? true : false;
                        //SENSOR [15]
                        l_Status.m_Percentage_of_welding_executed = l_sensor[9];
                   


                }
                else
                    return null;

                return l_Status;
            }

        public static String
            LastErrorMessage()
        {
            String l_Mensaje;
            SidLib.Error_Sid(m_LastError, out l_Mensaje);

            return l_Mensaje;
        }

        public static bool
          Open()
        {
            m_LastError = SidLib.SID_Open(false);

            if (m_LastError == SidLib.SID_ALREADY_OPEN)
                m_LastError = SidLib.SID_OKAY;
            else if (m_LastError == SidLib.SID_TRY_TO_RESET)
            {
                m_LastError = SidLib.SID_Reset((char)SidLib.RESET_ERROR);
            }

            return (m_LastError == SidLib.SID_OKAY);
        }

        public static UnitStatus
          GetStatusOpened()
        {
            Byte[] l_key = new Byte[1];
            Byte[] l_sensor = new Byte[64];
            UnitStatus l_Status = new UnitStatus();

            m_LastError = SidLib.SID_UnitStatus(l_key, l_sensor, null, null, null);

            switch (l_key[0].ToString("X"))
            {
                case "00":
                    l_Status.m_SenseKey = SenseKeyEnum.Unit_Ok;
                    break;
                case "02":
                    l_Status.m_SenseKey = SenseKeyEnum.Unit_busy;
                    break;
                case "03":
                    l_Status.m_SenseKey = SenseKeyEnum.Paper_jam;
                    break;
                case "04":
                    l_Status.m_SenseKey = SenseKeyEnum.Hardware_error;
                    break;
                case "05":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request;
                    break;
                case "06":
                    l_Status.m_SenseKey = SenseKeyEnum.Document_not_present;
                    break;
                case "19":
                    l_Status.m_SenseKey = SenseKeyEnum.Double_Leafing_warning;
                    break;
                case "40":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_feeder_sensor_not_covered;
                    break;
                case "41":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_switch_path_sensor_not_covered;
                    break;
                case "42":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_withdraw_pocket_sensor_not_covered;
                    break;
                case "43":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Bag_Hole_sensor_never_covered;
                    break;
                case "48":
                    l_Status.m_SenseKey = SenseKeyEnum.Document_too_long;
                    break;
                case "49":
                    l_Status.m_SenseKey = SenseKeyEnum.Press_notes_Blocked;
                    break;
                case "4A":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_exit_feeder_sensor_not_uncovered;
                    break;
                case "4B":
                    l_Status.m_SenseKey = SenseKeyEnum.Exit_switch_Blocked_sensor_not_uncovered;
                    break;
                case "4C":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Reject_bay_sensor_not_uncovered;
                    break;
                case "4D":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_Bag_Hole_sensor_not_uncovered;
                    break;
                case "4E":
                    l_Status.m_SenseKey = SenseKeyEnum.Reject_bay_full;
                    break;
                case "4F":
                    l_Status.m_SenseKey = SenseKeyEnum.Jam_in_front_scanners;
                    break;
                case "70":
                    l_Status.m_SenseKey = SenseKeyEnum.Weld_not_done_for_document_in_the_path;
                    break;
                case "71":
                    l_Status.m_SenseKey = SenseKeyEnum.Weld_not_done_because_bag_empty;
                    break;
                case "72":
                    l_Status.m_SenseKey = SenseKeyEnum.Time_Out_expired_bag_opening_or_closing;
                    break;
                case "73":
                    l_Status.m_SenseKey = SenseKeyEnum.Low_Power_for_weld_the_bag;
                    break;
                case "74":
                    l_Status.m_SenseKey = SenseKeyEnum.Bar_weld_in_safe_protection;
                    break;
                case "75":
                    l_Status.m_SenseKey = SenseKeyEnum.Time_Out_Open_Door_expired;
                    break;
                case "76":
                    l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_door_Leafer_open;
                    break;
                case "77":
                    l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_1_door_magnetic_head_open;
                    break;
                case "78":
                    l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_2_door_magnetic_head_open;
                    break;
                case "79":
                    l_Status.m_SenseKey = SenseKeyEnum.Micro_switch_Head_Unit_open;
                    break;
                case "7A":
                    l_Status.m_SenseKey = SenseKeyEnum.Interlock_open_test_the_SensorSatus_byte_2_or_9_to_see_which_open;
                    break;
                case "7B":
                    l_Status.m_SenseKey = SenseKeyEnum.Time_Out_expired_during_elevator_movement;
                    break;
                case "7C":
                    l_Status.m_SenseKey = SenseKeyEnum.Error_during_bag_tensioner_movement;
                    break;
                case "7D":
                    l_Status.m_SenseKey = SenseKeyEnum.Magnetic_Sensor_not_calibrate;
                    break;
                case "90":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Strongbox_Door_Open;
                    break;
                case "91":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_not_Present;
                    break;
                case "92":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_Closed;
                    break;
                case "93":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_not_completely_Open;
                    break;
                case "94":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Safe_lock_Open;
                    break;
                case "95":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Lifter_Up;
                    break;
                case "96":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Lifter_not_completely_Down;
                    break;
                case "97":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Bag_present;
                    break;
                case "98":
                    l_Status.m_SenseKey = SenseKeyEnum.Illegal_request_with_Safe_lock_Code_not_Inserted;
                    break;
            }

            l_Status.m_Entrance_Sensor_1 = (l_sensor[0] & 0x01) != 0 ? true : false;
            l_Status.m_Entrance_Sensor_2 = (l_sensor[0] & 0x02) != 0 ? true : false;
            l_Status.m_Shutter_Sensor_1 = (l_sensor[0] & 0x04) != 0 ? true : false;
            l_Status.m_Feeder_Flap_Sensor = (l_sensor[0] & 0x08) != 0 ? true : false;
            l_Status.m_Reject_Entrance_Sensor = (l_sensor[0] & 0x10) != 0 ? true : false;
            l_Status.m_Bag_Entrance_Sensor_1 = (l_sensor[0] & 0x20) != 0 ? true : false;
            l_Status.m_Feeder_Sensor = (l_sensor[0] & 0x40) != 0 ? true : false;
            l_Status.m_Feeder_Empty_Sensor = (l_sensor[0] & 0x80) != 0 ? true : false;

            //SENSOR [1]
            l_Status.m_Double_Leafing_Sensor_1 = (l_sensor[1] & 0x01) != 0 ? true : false;
            l_Status.m_Double_Leafing_Sensor_2 = (l_sensor[1] & 0x02) != 0 ? true : false;
            l_Status.m_Double_Leafing_Sensor_3 = (l_sensor[1] & 0x04) != 0 ? true : false;
            // l_Status = (l_sensor[1] & 0x08) != 0 ? true : false;
            l_Status.m_Bag_Tensioner_1_UP = (l_sensor[1] & 0x10) != 0 ? true : false;
            l_Status.m_Bag_Tensioner_2_UP = (l_sensor[1] & 0x20) != 0 ? true : false;
            l_Status.m_Reject_Bay_empty = (l_sensor[1] & 0x40) != 0 ? true : false;
            l_Status.m_Shutter_Sensor_2 = (l_sensor[1] & 0x80) != 0 ? true : false;
            //SENSOR [2]  //INVERTIDO 
            l_Status.m_Bag_sensor_1_present = (l_sensor[2] & 0x01) != 0 ? false : true;
            l_Status.m_Bag_sensor_2_present = (l_sensor[2] & 0x02) != 0 ? false : true;
            l_Status.m_Bag_sensor_3_present = (l_sensor[2] & 0x04) != 0 ? false : true;
            l_Status.m_Bag_sensor_4_present = (l_sensor[2] & 0x08) != 0 ? false : true;
            l_Status.m_Bag_sensor_5_present = (l_sensor[2] & 0x10) != 0 ? false : true;
            l_Status.m_Bag_sensor_6_present = (l_sensor[2] & 0x20) != 0 ? false : true;
            // l_Status = (l_sensor[2] & 0x40) != 0 ? true : false;
            l_Status.m_Interlock_door_strongbox_closed = (l_sensor[2] & 0x80) != 0 ? true : false;

            //SENSOR [6] validation Banknote
            l_Status.m_Validation_progress = (l_sensor[6] & 0x00) != 0 ? true : false;
            l_Status.m_Currency = l_sensor[6];
            l_Status.m_Note_to_much_swewed = (l_sensor[6] & 0xe0) != 0 ? false : true;
            l_Status.m_Note_suspect = (l_sensor[6] & 0xe2) != 0 ? true : false;
            l_Status.m_Note_wrong_dimension = (l_sensor[6] & 0xe3) != 0 ? true : false;
            l_Status.m_Note_No_Magnetic_found = (l_sensor[6] & 0xe4) != 0 ? true : false;
            l_Status.m_Note_IR_wrong = (l_sensor[6] & 0xe5) != 0 ? true : false;
            l_Status.m_Validation_progress = (l_sensor[6] & 0x00) != 0 ? true : false;

            //SENSOR [7]] Sensor strongbox and bag
            l_Status.m_Sensor_door_open = (l_sensor[7] & 0x01) != 0 ? true : false;
            l_Status.m_Sensor_bag_present = (l_sensor[7] & 0x02) != 0 ? false : true;
            l_Status.m_Sensor_bag_open = (l_sensor[7] & 0x04) != 0 ? true : false;
            l_Status.m_Sensor_bag_ready_for_deposit = (l_sensor[7] & 0x08) != 0 ? false : true;
            l_Status.m_Code_lock_correct = (l_sensor[7] & 0x10) != 0 ? false : true;
            l_Status.m_Alarm_Code_entered = (l_sensor[7] & 0x20) != 0 ? false : true;
            l_Status.m_Dip_Switch_elevator_position_UP = (l_sensor[7] & 0x40) != 0 ? true : false;
            l_Status.m_Dip_Switch_elevator_position_DOWN = (l_sensor[7] & 0x80) != 0 ? true : false;

            //SENSOR [8] BAG SENSORS
            l_Status.m_Sensor_bag_1 = (l_sensor[8] & 0x01) != 0 ? true : false;
            l_Status.m_Sensor_bag_2 = (l_sensor[8] & 0x02) != 0 ? true : false;
            l_Status.m_Sensor_bag_3 = (l_sensor[8] & 0x04) != 0 ? true : false;
            l_Status.m_Sensor_bag_4 = (l_sensor[8] & 0x08) != 0 ? true : false;
            l_Status.m_Sensor_bag_5 = (l_sensor[8] & 0x10) != 0 ? true : false;
            l_Status.m_Sensor_bag_6 = (l_sensor[8] & 0x20) != 0 ? true : false;
            l_Status.m_Sensor_bag_7 = (l_sensor[8] & 0x40) != 0 ? true : false;
            l_Status.m_Sensor_bag_8 = (l_sensor[8] & 0x80) != 0 ? true : false;
            //SENSOR [9]
            l_Status.m_Sensor_Dip_Switch_Leafer_open = (l_sensor[9] & 0x01) != 0 ? true : false;
            l_Status.m_Sensor_Dip_Switch_Magnetic_door_1_open = (l_sensor[9] & 0x02) != 0 ? true : false;
            l_Status.m_Sensor_Dip_Switch_Magnetic_door_2_open = (l_sensor[9] & 0x04) != 0 ? true : false;
            l_Status.m_Bag_Entrance_Sensor_2 = (l_sensor[9] & 0x08) != 0 ? true : false;
            l_Status.m_Sensor_Dip_Switch_Unit_Open = (l_sensor[9] & 0x10) != 0 ? true : false;
            l_Status.m_Reject_EntranceSensor_2 = (l_sensor[9] & 0x20) != 0 ? true : false;
            l_Status.m_Interlock_cover_head_open = (l_sensor[9] & 0x40) != 0 ? true : false;
            // l_Status= (l_sensor[9] & 0x80) != 0 ? true : false;
            //SENSOR [15]
            l_Status.m_Percentage_of_welding_executed = l_sensor[9];

            return l_Status;
        }


        public static int LastError
            {
                get { return SensarSIDcs.m_LastError; }
                // set { SidHL.m_LastError = value; }
            }
        }
}
