﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace __DemoFID_B
{
    class BDCuentas
    {
        public static bool
            DesactivarCuentas()
        {
            String l_Query
                = "UPDATE                   Cuenta "
                + "SET                      Activa = 0 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Desactivar Cuentas", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }
            return true;
        }

        public static bool
            ValidarCuentaExistente(String p_AccountId, int p_IdMoneda, String p_Alias)
        {
            String l_Query
                = "SELECT                   Activa "
                + "FROM                     Cuenta "
                + "WHERE                    IdCuenta = @IdCuenta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_AccountId);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                    return InsertarCuenta(p_AccountId, p_IdMoneda, p_Alias);
                else
                {
                    if (!(bool)l_Lector[0])
                        return ActualizarCuenta(p_AccountId, p_IdMoneda, p_Alias);
                    else
                        return true;
                }
            }
        }

        public static bool
            InsertarCuenta(String p_AccountId, int p_IdMoneda, String p_Alias)
        {
            String l_Query
                = "INSERT INTO              Cuenta "
                + "                         (IdCuenta, IdMoneda, Alias, Activa) "
                + "VALUES                   (@IdCuenta, @IdMoneda, @Alias, 1) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_AccountId);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@Alias", p_Alias);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("INSERTAR CUENTA", "Error", "Execpcion " + ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
            ActualizarCuenta(String p_AccountId, int p_IdMoneda, String p_Alias)
        {
            String l_Query
                = "UPDATE                   Cuenta "
                + "SET                      Activa = 1, "
                + "                         IdMoneda = @IdMoneda, "
                + "                         Alias = @Alias "
                + "WHERE                    IdCuenta = @IdCuenta ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_AccountId);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@Alias", p_Alias);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Actualizar Cuentas", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
            ValidarNoCuenta(String p_AccountId, out String p_Alias, out int p_IdMoneda)
        {
            String l_Query
                = "SELECT               IdCuenta, Alias, IdMoneda, Activa "
                + "FROM                 Cuenta "
                + "WHERE                IdCuenta = @IdCuenta "
                + "AND                  Activa = 1 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_AccountId);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                {
                    p_Alias = "";
                    p_IdMoneda = 0;
                    return false;
                }
                else
                {
                    p_Alias = l_Lector[1].ToString();
                    p_IdMoneda = (int)l_Lector[2];
                    return true;
                }
            }
        }
    }
}
