﻿using Irds;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace __DemoFID_B
{
    class BDOperador
    {
        public static int Insertar1(String p_Clave, String p_Nombre, string p_refer, string p_Pass, bool p_status, string p_ClientId)
        {
            int p_profId = 1;

            String l_Query
                = "INSERT INTO Usuario "
                + "     ( IdUsuario  "
                + "     , Nombre  "
                + "     , Referencia "
                + "     , Password  "
                + "     , Activo "
                + "     , IdPerfil "
                + "     , ClaveCliente ) "
                + "VALUES "
                + "     ( '" + p_Clave + "'  "
                + "     , '" + p_Nombre + "'  "
                + "     , '" + p_refer + "'  "
                + "     , '" + p_Pass + "'  "
                + "     , '" + p_status + "'  "
                + "     , '" + p_profId + "'  "
                + "     , '" + p_ClientId + "' ) ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();

                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return 0;
                }
                catch (Exception E)

                {
                    MessageBox.Show("Por favor, introduzca un valor único para la Clave, Esta Clave ya existen para otro cliente , Por favor inténtelo de nuevo", "Important Note",
    MessageBoxButtons.OK,
    MessageBoxIcon.Error);

                    return 0;
                }

                return 1;
            }

        }


        public static void Update1(String p_Nombre, string p_refer, bool p_status, string p_ClientId, string p_Pass)
        {
            String l_Query
                = "UPDATE           Usuario "
                + "SET           Nombre    = @Nombre "
                + ",          Referencia     = @refer "
                + ",           Activo    = @status "
                + ",           Password    = @Password "
                + "WHERE          IdUsuario   = @clientid ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);
                l_Comando.Parameters.AddWithValue("@refer", p_refer);
                l_Comando.Parameters.AddWithValue("@status", p_status);
                l_Comando.Parameters.AddWithValue("@Password", p_Pass);
                l_Comando.Parameters.AddWithValue("@clientid", p_ClientId);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception E) { throw; }
            }


        }

        public static DataTable TraerOperador(string p_Clave)
        {
            String l_Query
                = "SELECT IdUsuario "
                + " , Nombre "
                + " , Referencia "
                + ",  Activo "
                + "FROM Usuario "
                + " WHERE ClaveCliente = '" + p_Clave + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    return UtilsDB.GenerarTabla(l_Query);
                }
                catch (Exception E)
                {

                    throw E;
                }
            }

        }

        public static void DelyInsert1(String p_Clave, String p_Nombre, string p_refer, string p_Pass, bool p_status, string p_ClientId)
        {

            String l_Query
                = "DELETE "
                + "FROM Usuario "
                + "WHERE IdUsuario = '" + p_Clave + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception E)
                {

                    throw E;
                }
            }

            //p_status = true;

            Insertar1(p_Clave, p_Nombre, p_refer, p_Pass, p_status, p_ClientId);

        }

        public static bool SelcOperador(string ClientID, out string p_id, out string p_Nombre, out string p_Refer, out string p_status, out string p_Pass)
        {
            String l_Query
                = "SELECT IdUsuario "
                + " , Nombre "
                + " , Referencia "
                + ",  Activo "
                 + ",  Password "
                + "FROM Usuario "
                + " WHERE IdUsuario = '" + ClientID + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    DataTable l_Datos = UtilsDB.GenerarTabla(l_Query);

                    if ((l_Datos != null)
                        || (l_Datos.Rows.Count != 0))
                    {

                        p_id = l_Datos.Rows[0]["IdUsuario"].ToString();
                        p_Nombre = l_Datos.Rows[0]["Nombre"].ToString();
                        p_Refer = l_Datos.Rows[0]["Referencia"].ToString();
                        p_status = l_Datos.Rows[0]["Activo"].ToString();
                        p_Pass = l_Datos.Rows[0]["Password"].ToString();

                        return true;
                    }
                }
                catch (Exception E)
                {
                    throw E;
                }
            }
            p_Nombre = p_id = p_Refer = p_status = p_Pass = "";

            return false;
        }
    }
}
