﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;
using System.IO;

namespace __DemoFID_B
{
    public partial class FormRetiroEfectivo : Form
    {
        DataTable l_Retiro;
        int lRet;
        int l_Reply;
        String l_Error;
        Double l_TotalPesos = 0;
        Double l_TotalBolsa = 0;
        Double l_TotalDolares = 0;
        String l_Equipo = "Equipo1";
        bool l_Equipo1_vacio, l_Equipo2_vacio;
        DataTable l_Datos;
        string l_bolsaAnterior;
        int m_porcentajeAnterior;
        ModulePrint l_auxGuardado;

        
  




        public FormRetiroEfectivo()
        {
            InitializeComponent();
        }

        private void FormRetiroEfectivo_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            l_Equipo1_vacio = false;
            l_Equipo2_vacio = false;
            TraerDatos();
            ImprimirMovimientos();
            Cursor.Hide();

        }

        private void Limpiar()
        {
            l_TotalPesos = 0;
            l_TotalDolares = 0;
            c_TotalRetiroEfectivo.Text = "0";
            c_1000.Text = "0";
            c_500.Text = "0";
            c_200.Text = "0";
            c_100.Text = "0";
            c_50.Text = "0";
            c_20.Text = "0";
        }

        public void ImprimirMovimientos()
        {
            DataTable l_Movimientos;
            Double l_total = 0;

            l_Movimientos = BDConsulta.ObtenerConsultaOperaciones_mov();

            if (l_Movimientos.Rows.Count > 0)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "TICKET PARA USO EXCLUSIVO DE LOCK favor de ENTREGAR en PROCESO junto con la bolsa";
                    f_Error.ShowDialog();
                    

                }
                ModulePrint imprimir = new ModulePrint();

                imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);

                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine("Consulta de MOVIMIENTOS");
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("Operaciones registradas hasta el momento");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                imprimir.AddSubHeaderLine(Globales.ClienteProsegur);

                foreach (DataRow l_Detalle in l_Movimientos.Rows)
                {
                    Double l_monto = Double.Parse(l_Detalle[2].ToString());
                    imprimir.AddItemUsuario(l_Detalle[1].ToString(), l_monto.ToString("$#,###,###,##0.00"), l_Detalle[0].ToString());
                    l_total += l_monto;

                }

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Numero de Depositantes: " + l_Movimientos.Rows.Count.ToString());

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Total: " + l_total.ToString("$#,###,###,##0.00"));

                imprimir.AddFooterLine("");

                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }
        }

        private void PedirTotalBolsaSID()
        {

            if (Globales.DEBUG)
            {
                ushort[] l_totalNotes = new ushort[64];
                int l_totalbilletes=0;
                l_Reply = SidLib.SID_Open(true);
                if (l_Reply == SidLib.SID_OKAY)
                    l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);
                if (l_Reply == SidLib.SID_OKAY)
                {
                    c_1000.Text = l_totalNotes[5].ToString();
                    c_500.Text = l_totalNotes[4].ToString();
                    c_200.Text = l_totalNotes[3].ToString();
                    c_100.Text = l_totalNotes[2].ToString();
                    c_50.Text = l_totalNotes[1].ToString();
                    c_20.Text = l_totalNotes[0].ToString();

                    l_totalbilletes = (int) l_totalNotes[5]+ (int) l_totalNotes[4]+ (int)l_totalNotes[3]+
                                      (int) l_totalNotes[2]+ (int) l_totalNotes[1] + (int)l_totalNotes[0];
                }

                c_totalbilletesbolsa.Text  = l_totalbilletes.ToString ();

                l_Reply = SidLib.SID_Close();
                

            }

        }

        private void TraerDatos()
        {
            int l_totalBilletes=0;

            Limpiar();

            PedirTotalBolsaSID();

            l_Retiro = BDRetiro.ObtenerDetalleRetiro(0);

            l_Datos = BDConsulta.ObtenerConsultaDepositos(1);

            if (l_Datos.Rows.Count < 1)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "No hay depósitos en pesos";
                    f_Error.ShowDialog();
                    //Close();
                }
            }
            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
                if (l_Detalle[1].ToString().Trim() == "1000")
                {

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_1000.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");

                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "1000");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "1000";
                        l_Retiro.Rows.Add(l_Nueva);

                        l_TotalBolsa += (int)l_Detalle[0] * 1000;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }
                if (l_Detalle[1].ToString().Trim() == "500")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_500.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "500");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "500";
                        l_Retiro.Rows.Add(l_Nueva);
                        l_TotalBolsa += (int)l_Detalle[0] * 500;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }
                if (l_Detalle[1].ToString().Trim() == "200")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_200.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "200");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "200";
                        l_Retiro.Rows.Add(l_Nueva);
                        l_TotalBolsa += (int)l_Detalle[0] * 200;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }
                if (l_Detalle[1].ToString().Trim() == "100")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "100");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "100";
                        l_Retiro.Rows.Add(l_Nueva);
                        l_TotalBolsa += (int)l_Detalle[0] * 100;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }
                if (l_Detalle[1].ToString().Trim() == "50")
                {

                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "50");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "50";
                        l_Retiro.Rows.Add(l_Nueva);
                        l_TotalBolsa += (int)l_Detalle[0] * 50;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }
                if (l_Detalle[1].ToString().Trim() == "20")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        DataRow l_Nueva = l_Retiro.NewRow();
                        l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "20");
                        l_Nueva[1] = l_Detalle[0];
                        l_Nueva[2] = "20";
                        l_Retiro.Rows.Add(l_Nueva);
                        l_TotalBolsa += (int)l_Detalle[0] * 20;
                        l_totalBilletes += (int)l_Detalle[0];
                    }

                }

            }


            c_totalenDB.Text = l_totalBilletes.ToString ();
            c_TotalRetiroEfectivo.Text = l_TotalBolsa.ToString("$#,###,##0.00");

        }




        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            DialogResult l_respuesta = DialogResult.None;
            bool Exitoso=true;

            c_BotonAceptar.Enabled = false;
            //  Globales.CambiaEquipo(l_Equipo);

            //UtilsComunicacion.MantenerVivo();

            Globales.LeerBolsaPuesta();
            l_bolsaAnterior = Globales.NumeroSerieBOLSA;
            Globales.NumeroSerieBOLSAanterior = l_bolsaAnterior;
          //  LeerPorcentaje(out Exitoso);
            m_porcentajeAnterior = 100;

            if (Exitoso)
                ImprimirPesos();




            // ImprimirDolares();
            Cursor.Hide();

           
               


                if (Globales.DEBUG)
                {
                    Cursor.Show();
                    Cursor.Show();
                    Cursor.Current = Cursors.WaitCursor;

                    Byte[] key = new Byte[1];
                    Byte[] sensor = new Byte[64];

                    if ( Exitoso )
                    {

                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Ponga El TICKET en el alimentador y presione Aceptar";
                           DialogResult l_pideTicket = f_Error.ShowDialog();

                           if (l_pideTicket == DialogResult.Cancel)
                               return;
                        }

                        try
                        {

                            SidLib.ResetError();

                            lRet = SidLib.SID_Open(true);

                            if (-215 <= lRet && lRet <= -199)
                            {

                                SidLib.Error_Sid(lRet, out l_Error);
                                Globales.EscribirBitacora("EnableDeposit", "Atasco de Billetes::", l_Error, 1);


                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                                    f_Error.ShowDialog();
                                    SidLib.ResetError();
                                    SidLib.SID_Close();
                                    c_BotonAceptar.Enabled = true;

                                    // c_BotonCancelar_Click(this, null);
                                }
                                try
                                {
                                    SidLib.ResetPath();
                                }
                                catch
                                {
                                }
                                return;

                            }

                            if (-246 <= lRet && lRet <= -239)
                            {

                                SidLib.Error_Sid(lRet, out l_Error);
                                Globales.EscribirBitacora("EnableDeposit", "Problema DIP_SWITCH::", l_Error, 1);


                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = l_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                                    f_Error.ShowDialog();
                                    SidLib.ResetError();
                                    SidLib.SID_Close();
                                    c_BotonAceptar.Enabled = true;
                                    return;
                                    // c_BotonCancelar_Click(this, null);
                                }



                            }

                            

                            lRet = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                            Globales.EscribirBitacora("RetiroEfectivo", "UnitStatus", lRet.ToString(),1);
                            Globales.EscribirBitacora("UnitStatus", "sensor[7]", sensor[7].ToString(),1);

                            if (sensor[7] == 148)
                            {
                                Globales.EscribirBitacora("CashIn", "sensor[7]", sensor[7].ToString(),1);
                                //  lRet = SidLib.SID_GetCashTotalBag(l_TotalBag);
                                // foreach (ushort billetes in l_totalNotes)
                                // c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;

                                // lRet = SidLib.SID_CashIn(0, 1, 0, false, 0, 0);/*Capture the notes */
                                do
                                {
                                    SidLib.Error_Sid(lRet, out l_Error);
                                    Globales.EscribirBitacora("Cash", "sensor[7]", sensor[7].ToString(),1);

                                    lRet = SidLib.SID_CashIn(0, 1, 0, false, 0, 0);/*Capture the notes */
                                    if (lRet == 1)
                                        using (FormError f_Error = new FormError(false))
                                        {
                                            SidLib.Error_Sid(lRet, out l_Error);
                                            Globales.EscribirBitacora("CashIN", "Feedervacio" , l_Error ,1);
                                            f_Error.c_MensajeError.Text = "Ponga El TICKET en el alimentador y presione Aceptar";
                                            f_Error.ShowDialog();
                                        }


                                } while (lRet == 1);


                                short[] result = new short[1];
                                lRet = SidLib.SID_WaitValidationResult(result);

                                while (lRet == SidLib.SID_PERIF_BUSY)
                                {

                                    // c_Respuesta.Text += "\r\n" + result[0].ToString("X2");
                                    // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");
                                    if (result[0] != 0)
                                    {
                                        Globales.EscribirBitacora("RetiroEfectivo", "waitValidation", lRet.ToString(),1);
                                        lRet = SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                                        Globales.EscribirBitacora("RetiroEfectivo", "ClearDenomination", lRet.ToString(),1);
                                    }

                                    lRet = SidLib.SID_WaitValidationResult(result);
                                }

                                SidLib.ResetError();

                                SidLib.SID_Close();
                            }

                            SidLib.ResetError();

                            SidLib.SID_Close();

                        }
                        catch (Exception ex)
                        {
                            Globales.EscribirBitacora("Intro Ticket", "cashIn o waitvalidation", ex.Message,1);
                        }

                        using (FormCambioBolsa f_bolsa = new FormCambioBolsa())
                        {
                            f_bolsa.Refresh();

                            l_respuesta = f_bolsa.ShowDialog();

                            if (l_respuesta == DialogResult.Abort)

                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = "Error en el procedimiento de Cambio de Bolsa Intente de nuevo";
                                    f_Error.ShowDialog();
                                    c_BotonAceptar.Enabled = true;
                                    return;
                                }

                        }
                    }else 

                    using (FormCambioBolsa f_bolsa = new FormCambioBolsa())
                    {
                        f_bolsa.Refresh();

                        l_respuesta = f_bolsa.ShowDialog();

                        if (l_respuesta == DialogResult.Abort)

                            using (FormError f_Error = new FormError(false))
                            {
                                f_Error.c_MensajeError.Text = "SEGUNDO Error en el procedimiento de Cambio de Bolsa comuniquese con el Administrador";
                                f_Error.ShowDialog();
                                return;

                            }
                        
                        l_respuesta = DialogResult.Retry;

                   }
                }
                else
                {
                    //Caso ideal de Operacion //
                    l_respuesta = DialogResult.OK;
                    Globales.BolsaCorrecta = true;

                }



                Refresh();
                Application.DoEvents();


            if (l_respuesta == DialogResult.OK)
            {
                UtilsComunicacion.Enviar_Transaccion("", 1, 1, 2, Double.Parse(c_TotalRetiroEfectivo.Text.Replace(",", "").Replace("$", "")),
                l_Retiro, 0, true, Globales.IdUsuario);
                ImprimirPesos(Globales.BolsaCorrecta,true);
               
                
             
            }

            if (l_respuesta == DialogResult.Ignore)
            {
                UtilsComunicacion.Enviar_Transaccion("", 1, 1, 2, Double.Parse(c_TotalRetiroEfectivo.Text.Replace(",", "").Replace("$", "")),
                l_Retiro, 0, true, Globales.IdUsuario);
                ImprimirPesos(Globales.BolsaCorrecta, false);
                

            }
            if (l_respuesta== DialogResult.Abort )
            {
                
            }

            if (l_respuesta == DialogResult.Retry )
            {
                if (l_auxGuardado != null)
                {
                    l_auxGuardado.AddFooterLine(":::RECUPERACION BOLSA::: ");
                    l_auxGuardado.AddFooterLine("FECHA ::" + DateTime.Now.ToLongDateString() + DateTime.Now.ToLongTimeString());


                    l_auxGuardado.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
                    l_auxGuardado = null;
                }
            }

           
            Cursor.Hide();
            c_BotonAceptar.Enabled = true;

            Close();
        }

        private void ImprimirPesos(bool p_bolsacorrecta , bool p_procedimientocorrecto)
        {
            ModulePrint imprimir_copia = new ModulePrint();

            imprimir_copia.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);


            imprimir_copia.AddHeaderLine(" ");
            imprimir_copia.AddHeaderLine(" ");
            imprimir_copia.AddHeaderLine(" ");
            imprimir_copia.AddHeaderLine(" ");
            imprimir_copia.AddHeaderLine("COPIA para ETV ");
            if (p_procedimientocorrecto )
            {

                imprimir_copia.AddHeaderLine("Procedimiento CORRECTO");
            }
            else
            {
                imprimir_copia.AddHeaderLine("...Procedimiento INCORRECTO...");


            }
            
            imprimir_copia.AddHeaderLine("BOLSA RETIRADA: " + l_bolsaAnterior);
            if (p_bolsacorrecta)
            {
              
                imprimir_copia.AddHeaderLine("...::::COINCIDENCIA CORRECTA:::::...");
            }
            else
            {
                imprimir_copia.AddHeaderLine("...INCONSISTENCIA EN LAS BOLSAS...");
                
              
            }
            
                 imprimir_copia.AddHeaderLine("BOLSA Nueva Puesta: " + Globales.NumeroSerieBOLSA );
           

            imprimir_copia.AddHeaderLine("");
            imprimir_copia.AddHeaderLine("#SERIAL EQUIPO: SID" + Globales.Serial1.ToString());
            imprimir_copia.AddHeaderLine("");
            imprimir_copia.AddHeaderLine("Moneda: Pesos");
            imprimir_copia.AddHeaderLine("TICKET IMPRESO ::" + DateTime.Now.ToLongTimeString());
            imprimir_copia.AddHeaderLine("BOLSA con el NUMERO:: " + l_bolsaAnterior );
            imprimir_copia.AddHeaderLine("CANTIDAD DE BILLETES: " + c_totalenDB .Text);
            imprimir_copia.AddHeaderLine("Total en BOLSA: " + c_TotalRetiroEfectivo.Text);
            
            imprimir_copia.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);

            imprimir_copia.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + "Hora: " + DateTime.Now.ToShortTimeString());
         // imprimir_copia.AddHeaderLine("");
            //imprimir_copia.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());


            imprimir_copia.AddSubHeaderLine("CONTENIDO BOLSA A DETALLE: ");

            imprimir_copia.AddItem(c_Cajon2_1000.Text, "$1,000.00", "1000");
            imprimir_copia.AddItem(c_Cajon2_500.Text, "$500.00", "500");
            imprimir_copia.AddItem(c_Cajon2_200.Text, "$200.00", "200");
            imprimir_copia.AddItem(c_Cajon2_100.Text, "$100.00", "100");
            imprimir_copia.AddItem(c_Cajon2_50.Text, "$50.00", "50");
            imprimir_copia.AddItem(c_Cajon2_20.Text, "$20.00", "20");

            imprimir_copia.AddFooterLine("TOTAL:              " + c_TotalRetiroEfectivo.Text);
            imprimir_copia.AddHeaderLine("Cantidad DE BILLETES:   " + c_totalenDB.Text);
            imprimir_copia.AddFooterLine("");
            imprimir_copia.AddFooterLine("LUGAR DE EMISIÓN: " + Globales.ClienteProsegur );
            imprimir_copia.AddFooterLine("");
            imprimir_copia.AddFooterLine("--------------------------");
            imprimir_copia.AddFooterLine("NOMBRE Y FIRMA");


            imprimir_copia.AddFooterLine("");
          
            imprimir_copia.AddFooterLine("FIN DEL REPORTE");

           
           
            imprimir_copia.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());

            if (!p_procedimientocorrecto)
              l_auxGuardado = imprimir_copia;

        }

        private void ImprimirPesos()
        {


            
            
                ModulePrint imprimir = new ModulePrint();

              //  imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);
               
                             
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" TICKET de REFERENCIA ");
               
                imprimir.AddHeaderLine("BOLSA con el NUMERO:: " + l_bolsaAnterior );
                imprimir.AddHeaderLine("REPORTE SYM  Retiro de Efectivo ETV ");
              
                imprimir.AddHeaderLine("#SERIAL EQUIPO: SID" + Globales.Serial1.ToString());
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("TICKET IMPRESO ::" + DateTime.Now.ToLongTimeString());
              //  imprimir.AddHeaderLine("BOLSA con el NUMERO :: " + Globales.NumeroSerieBOLSA);
                imprimir.AddHeaderLine("Total en BOLSA:   " + c_TotalRetiroEfectivo.Text);
                imprimir.AddHeaderLine("Cantidad DE BILLETES: " + c_totalenDB.Text);
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + "Hora: " + DateTime.Now.ToLongTimeString());
               // imprimir.AddHeaderLine("");
               // imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToLongTimeString());


                imprimir.AddSubHeaderLine("CONTENIDO BOLSA A DETALLE: ");

                imprimir.AddItem(c_Cajon2_1000.Text, "$1,000.00", "1000");
                imprimir.AddItem(c_Cajon2_500.Text, "$500.00", "500");
                imprimir.AddItem(c_Cajon2_200.Text, "$200.00", "200");
                imprimir.AddItem(c_Cajon2_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon2_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon2_20.Text, "$20.00", "20");

                imprimir.AddFooterLine("TOTAL:                 " + c_TotalRetiroEfectivo.Text);
               // imprimir.AddFooterLine("");
                imprimir.AddFooterLine("LUGAR DE EMISIÓN:" + Globales.ClienteProsegur);

                
                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void c_totalbilletesbolsa_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void c_20_Click(object sender, EventArgs e)
        {

        }

        private void c_50_Click(object sender, EventArgs e)
        {

        }

        private void c_100_Click(object sender, EventArgs e)
        {

        }

        private void c_200_Click(object sender, EventArgs e)
        {

        }

        private void c_500_Click(object sender, EventArgs e)
        {

        }

        private void c_1000_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_20_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_50_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_100_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_200_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_500_Click(object sender, EventArgs e)
        {

        }

        private void c_Cajon2_1000_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void c_totalenDB_TextChanged(object sender, EventArgs e)
        {

        }

        private void c_TotalRetiroEfectivo_TextChanged(object sender, EventArgs e)
        {

        }

        public void LeerPorcentaje(out bool p_exitoso)
        {
            int lectura = 0;


            if (File.Exists("PorcentajeSellado.txt"))
            {
                String[] lineas = File.ReadAllLines("PorcentajeSellado.txt");

                if (lineas.Length > 0)
                {
                    m_porcentajeAnterior = Convert.ToInt16(lineas[0]);
                }
                else
                    m_porcentajeAnterior = 100;

                if (lineas.Length > 1)
                {
                    lectura = Convert.ToInt16(lineas[lineas.Length - 1]);

                    if (lectura == 1)
                        p_exitoso = true;
                    else
                        p_exitoso = false;

                }
                else
                    p_exitoso = true;


            }
            else
            {
                m_porcentajeAnterior = 100;
                p_exitoso = true;
            }

        }

    }
}
