﻿namespace __DemoFID_B
{
    partial class FormSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelect));
            this.c_Deposito = new System.Windows.Forms.Button();
            this.c_Administrar = new System.Windows.Forms.Button();
            this.c_Etv = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.c_contabilidad = new System.Windows.Forms.Button();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Deposito
            // 
            this.c_Deposito.AutoEllipsis = true;
            this.c_Deposito.BackColor = System.Drawing.Color.DarkGreen;
            this.c_Deposito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Deposito.FlatAppearance.BorderSize = 0;
            this.c_Deposito.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Deposito.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Deposito.ForeColor = System.Drawing.Color.White;
            this.c_Deposito.Location = new System.Drawing.Point(129, 163);
            this.c_Deposito.Name = "c_Deposito";
            this.c_Deposito.Size = new System.Drawing.Size(545, 173);
            this.c_Deposito.TabIndex = 1;
            this.c_Deposito.Text = "DEPOSITAR";
            this.c_Deposito.UseVisualStyleBackColor = false;
            this.c_Deposito.Click += new System.EventHandler(this.c_Deposito_Click);
            // 
            // c_Administrar
            // 
            this.c_Administrar.BackColor = System.Drawing.Color.DarkGreen;
            this.c_Administrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Administrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Administrar.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Administrar.ForeColor = System.Drawing.Color.White;
            this.c_Administrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Administrar.Location = new System.Drawing.Point(66, 361);
            this.c_Administrar.Name = "c_Administrar";
            this.c_Administrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.c_Administrar.Size = new System.Drawing.Size(384, 88);
            this.c_Administrar.TabIndex = 2;
            this.c_Administrar.Text = "ADMINISTRAR";
            this.c_Administrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Administrar.UseVisualStyleBackColor = false;
            this.c_Administrar.Click += new System.EventHandler(this.c_Administrar_Click);
            // 
            // c_Etv
            // 
            this.c_Etv.BackColor = System.Drawing.Color.DarkGreen;
            this.c_Etv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.c_Etv.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.c_Etv.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Etv.ForeColor = System.Drawing.Color.White;
            this.c_Etv.Location = new System.Drawing.Point(508, 361);
            this.c_Etv.Name = "c_Etv";
            this.c_Etv.Size = new System.Drawing.Size(226, 88);
            this.c_Etv.TabIndex = 3;
            this.c_Etv.Text = "ETV ";
            this.c_Etv.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Etv.UseVisualStyleBackColor = false;
            this.c_Etv.Click += new System.EventHandler(this.c_Etv_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.edit_undo_3;
            this.pictureBox1.Location = new System.Drawing.Point(695, 108);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(61, 522);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "PORCENTAJE CUPO:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(308, 522);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "% ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.BackgroundImage = global::@__DemoFID_B.Properties.Resources.tick;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(82, 379);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(63, 50);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.c_Administrar_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox3.BackgroundImage = global::@__DemoFID_B.Properties.Resources.tick;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.Location = new System.Drawing.Point(175, 227);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(63, 50);
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.c_Deposito_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.BackgroundImage = global::@__DemoFID_B.Properties.Resources.tick;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox4.Location = new System.Drawing.Point(527, 379);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(62, 46);
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.c_Etv_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // c_contabilidad
            // 
            this.c_contabilidad.Enabled = false;
            this.c_contabilidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_contabilidad.ForeColor = System.Drawing.Color.Black;
            this.c_contabilidad.Location = new System.Drawing.Point(508, 507);
            this.c_contabilidad.Name = "c_contabilidad";
            this.c_contabilidad.Size = new System.Drawing.Size(226, 47);
            this.c_contabilidad.TabIndex = 11;
            this.c_contabilidad.Text = "Detalle Contabilidad";
            this.c_contabilidad.UseVisualStyleBackColor = true;
            this.c_contabilidad.Visible = false;
            this.c_contabilidad.Click += new System.EventHandler(this.c_contabilidad_Click);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 560);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 40);
            this.uc_PanelInferior1.TabIndex = 0;
            // 
            // FormSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_contabilidad);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_Etv);
            this.Controls.Add(this.c_Administrar);
            this.Controls.Add(this.c_Deposito);
            this.Controls.Add(this.uc_PanelInferior1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSelect";
            this.Load += new System.EventHandler(this.FormSelect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Button c_Deposito;
        private System.Windows.Forms.Button c_Administrar;
        private System.Windows.Forms.Button c_Etv;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button c_contabilidad;
    }
}