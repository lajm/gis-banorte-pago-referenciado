﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;
using System.IO;

namespace __DemoFID_B
{
    public partial class FormCambioBolsa : Form
    {
        public FormCambioBolsa()
        {
            InitializeComponent();
        }

        int l_Reply;
        int m_porcentajeAnterior = 0;
        bool Exitoso = false;
        bool SELLADA = false;
        int SensorBolsaRemovida = 63;
      

        private void Forzardibujado()
        {
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();
        }

        public void CompararBARCODE(String l_barcode_retirado)
        {
            if (Globales.NumeroSerieBOLSA == l_barcode_retirado)
                Globales.BolsaCorrecta = true;
            else
            {
                Globales.EscribirBitacora("Retiro de Bolsa", "CompararBARCODE", "Se esperaba el numero: " + Globales.NumeroSerieBOLSA + "  se obtuvo: " + l_barcode_retirado,1);
                Globales.BolsaCorrecta = false;
            }

        }



        public void Secuencia()
        {

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            int l_porcentaje = 0;
            bool l_ProcesoAnteriorCompleto=true;
            StringBuilder l_barcode = new StringBuilder(128);
            short l_length = 128;
            int l_auxiliarporcentajesellado = 0;
            int l_intentos = 0;
            int l_bolsacorectacontador = 0;

            Forzardibujado();

           // LeerPorcentaje(out l_ProcesoAnteriorCompleto);
            m_porcentajeAnterior = 100;


            SidLib.ResetError();

            l_Reply = SidLib.SID_Open(true);
            EscribirBitacora("OPEN", l_Reply.ToString());

            char[] l_username = Globales.Serial1.ToCharArray();
          
            l_Reply = SidApi.SidLib.SID_Login(l_username);

            Globales.EscribirBitacora("SELECT ETV", "SID_LOGIN", "login SIDserial: " + l_Reply, 3);



            l_Reply = SidLib.SID_CheckCodeLockCorrect(30);

            EscribirBitacora("CheckCode", l_Reply.ToString());

            if (l_Reply == SidLib.SID_OKAY)
            {

                c_paso1.Text = "CODIGO CORRECTO";

                paso2.Visible = true;
                c_paso2.Visible = true;
                c_esperarsellado.Visible = true;
                Forzardibujado();


                //// REVISAR EL PROCESO DE RECAMBIO \\\\

                if (!(m_porcentajeAnterior == 100) && !l_ProcesoAnteriorCompleto)
                {
                    EscribirBitacora("EVENTO:::", "Continuar con Sellado de BOLSA interrumpido");
                    //El auxiliar es para repetir todo el proceso pero con el porcentaje anterior.
                    l_auxiliarporcentajesellado = m_porcentajeAnterior;
                    m_porcentajeAnterior = 100;
                    l_ProcesoAnteriorCompleto = true;

                }

                if (m_porcentajeAnterior == 100 && l_ProcesoAnteriorCompleto)
                {

                    EscribirBitacora("EVENTO:::", "Empezar Proceso Completo Sellado de BOLSA");
                    m_porcentajeAnterior = l_auxiliarporcentajesellado;


                    l_Reply = SidLib.SID_JoinBag(m_porcentajeAnterior);
                    EscribirBitacora("JoinBAg", l_Reply.ToString());

                    if (l_Reply == SidLib.SID_OKAY)
                    {

                        c_esperarsellado.Visible = false;
                        c_paso2.Text = "... SELLANDO BOLSA ...";
                        panelSellado1.Visible = true;
                        panelSellado1.Empezar();
                        Forzardibujado();


                        do
                        {

                            l_Reply = SidLib.SID_JoinBagResult(ref l_porcentaje);
                            EscribirBitacora("result JoinBAG", l_Reply.ToString() + " :: % " + l_porcentaje);
                            Forzardibujado();

                        } while (l_Reply == SidLib.SID_PERIF_BUSY);

                        if (l_porcentaje != 0)
                        EscribirPorcentaje(l_porcentaje);

                        if (l_porcentaje == 100)
                        {
                            SELLADA = true;

                        }
                    }
                    else
                    {
                        if (l_Reply < -98 && l_Reply > -108 || l_Reply==SidLib.SID_ERROR_ON_CLOSE_BAG)
                        {
                            int l_causa = l_Reply;
                            using (FormAvisoEsperar l_avisoesperar = new FormAvisoEsperar () )
                              {

                                  l_avisoesperar.Mensaje("Espere Analizando Por que no se puede sellar la Bolsa...");
                                  l_avisoesperar.Focus();
                                  l_avisoesperar.Show();
                                  Forzardibujado();
                                  
                                  

                            try
                            {

                                DirectoryInfo directory = new DirectoryInfo(@".\Webcam");

                                FileInfo[] files = directory.GetFiles("*.*");



                                for (int i = 0; i < files.Length; i++)
                                {
                                    File.Delete(files[i].FullName);


                                }

                                EscribirBitacora("Archivos WebCam Borrados", "Carpeta Limpia");
                                directory.Refresh();

                            }
                            catch (Exception ex)
                            {
                                EscribirBitacora("Delete archivos Webcam", ex.Message);
                                Globales.EscribirBitacora("Cambio Bolsa", "Delete archivos", ex.Message,1);
                            }

                            l_avisoesperar.Mensaje("Tomando Nuevas imagenes,... inicializando...");
                            Forzardibujado();
                           

                            SidLib.ResetError();

                            l_Reply = SidLib.SID_Open(true);
                            EscribirBitacora("OPEN", l_Reply.ToString());
                            
                            l_Reply = SidLib.SID_Initialize();
                            EscribirBitacora("Inicializando... ", l_Reply.ToString());

                            if (l_Reply != 0)
                            {
                                string l_Error="";
                                SidLib.Error_Sid(l_Reply , out l_Error);

                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = l_Error;
                                    f_Error.ShowDialog();
                                    //c_BotonCancelar_Click(this, null);
                                }

                               
                            }
                            else
                            {
                                string l_Error = "";
                                SidLib.Error_Sid(l_causa, out l_Error);
                                EscribirBitacora("Error en JOIN_BAG", "ESTAUS: " + l_Reply);

                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = " Por favor Intente de nuevo el Procedimiento, Error Previo:  "+ l_Error;
                                    f_Error.ShowDialog();
                                    //c_BotonCancelar_Click(this, null);
                                }

                               
                            }


                            
                            l_avisoesperar.Close();
                            }
                            Forzardibujado();
                            Exitoso = false;
                            return;
                        }



                        else
                        {

                            using (FormError f_Error = new FormError(false))
                            {
                                String l_Error;
                                SidLib.Error_Sid(l_Reply, out l_Error);
                                EscribirBitacora("ERROR JOINBAG: ", l_Error);
                                f_Error.c_MensajeError.Text = "Problema con el Sellado Error: Error Previo:  " + l_Error;
                                f_Error.c_Imagen.Visible = false;
                                // EscribirProcesoExitoso(0);
                                f_Error.ShowDialog();
                            }
                            Exitoso = false;
                            return;
                        }
                    }
                }

                    if (m_porcentajeAnterior == 100 && !l_ProcesoAnteriorCompleto)
                    {
                        EscribirBitacora("EVENTO:::", "TERMINAR Retiro el retiro de BOLSA");
                        EscribirPorcentaje(m_porcentajeAnterior);

                        SELLADA = true;
                    }


                    panelSellado1.Detener();
                    panelSellado1.Visible = false;
                    Forzardibujado();

                    l_Reply = SidLib.SID_EnableToOpenDoor();
                    EscribirBitacora("Enable OpenDoor", l_Reply.ToString());
                    if (l_Reply == SidLib.SID_OKAY)
                    {
                        paso3.Visible = true;
                        c_paso3.Visible = true;
                        Forzardibujado();

                        l_Reply = SidLib.SID_WaitDoorOpen();
                        EscribirBitacora("wait Door Open", l_Reply.ToString());

                        if (l_Reply == SidLib.SID_OKAY)
                        {

                            c_paso3.Text = "Leer Codigo de Barras Actual: ";
                            Forzardibujado();

                            l_Reply = SidLib.SID_ReadBarcode(l_barcode, ref l_length, 60);
                            EscribirBitacora("Funcion Barcode: ", l_Reply.ToString());

                            if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED  ||l_Reply == SidLib.SID_DATATRUNC)
                            {
                                try{
                                string l_barcodelimpio = l_barcode.ToString().Substring(0, l_barcode.ToString().Length - 5).Replace(",", "");

                                c_barcode.Text = l_barcodelimpio;
                                c_barcode.Visible = true;
                                c_paso3.Text += l_barcodelimpio;
                                CompararBARCODE(l_barcodelimpio);
                                Forzardibujado();
                                EscribirBitacora("Barcode Retirado: ", l_barcodelimpio);

                                 }catch (Exception l_error)
                                {
                                     EscribirBitacora("ERROR de LECTURA Barcode ", l_error.Message );
                                     if (String.IsNullOrEmpty(l_barcode.ToString()))
                                     {
                                         string l_barcode2 = l_barcode.ToString().Replace(",", "");
                                         EscribirNumeroDeBOLSA(l_barcode2);
                                         Globales.NumeroSerieBOLSA = l_barcode2;
                                         EscribirBitacora("Barcode Nuevo: ", l_barcode2);
                                         c_barcodenuevo.Text = l_barcode2;
                                         c_barcodenuevo.Visible = true;
                                     }
                                     else
                                     {
                                         EscribirNumeroDeBOLSA("000000");
                                         Globales.NumeroSerieBOLSA = "000000";
                                         c_barcodenuevo.Text = "NO Capturado";
                                         c_barcodenuevo.Visible = true;
                                     }

                                }
                            }
                            else
                                Globales.BolsaCorrecta = false;


                            paso4.Visible = true;
                            c_paso4.Visible = true;
                            Forzardibujado();

                            int minutos = DateTime.Now.Minute+2;
                            int dif = 0;
                            if (minutos > 57)
                                dif = 60;
                                                          
                            c_esperarInicio.Visible = true;
                            Forzardibujado();

                            do
                            { //Wait remove the bag 
                                System.Threading.Thread.Sleep(300);
                                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                EscribirBitacora("Remove Bag", sensor[2].ToString());
                                if (minutos <= DateTime.Now.Minute + dif)
                                    break;
                              
                            } while (sensor[2] != SensorBolsaRemovida  ); //127 para version sid.dll > 2.0.0.2 , 63 para version <= 2.0.0.2 SID_CFE
                            EscribirBitacora("BOLSA REMOVIDA...", sensor[2].ToString());

                            // l_Reply = SidLib.SID_WaitRemoveBag(60); /* Wait the remove of the bag*/
                            c_esperarInicio.Visible = false;
                            paso5.Visible = true;
                            c_paso5.Visible = true;
                            Forzardibujado();

                            l_Reply = SidLib.SID_ReadBarcode(l_barcode, ref l_length, 60);
                            EscribirBitacora("Funcion Barcode: ", l_Reply.ToString());

                            if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_STRING_TRUNCATED || l_Reply == SidLib.SID_DATATRUNC)
                            {
                                try
                                {
                                    string l_barcodelimpio = l_barcode.ToString().Substring(0, l_barcode.ToString().Length - 5).Replace(",", "");
                                    EscribirNumeroDeBOLSA(l_barcodelimpio);
                                    Globales.NumeroSerieBOLSA = l_barcodelimpio;
                                    EscribirBitacora("Barcode Nuevo: ", l_barcodelimpio);
                                    c_barcodenuevo.Text = l_barcodelimpio;
                                    c_barcodenuevo.Visible = true;
                                }catch (Exception l_error)
                                {
                                     EscribirBitacora("ERROR de LECTURA ", l_error.Message );
                                     if (String.IsNullOrEmpty(l_barcode.ToString()))
                                     {
                                         string l_barcode2 = l_barcode.ToString().Replace(",", "");
                                         EscribirNumeroDeBOLSA(l_barcode2);
                                         Globales.NumeroSerieBOLSA = l_barcode2;
                                         EscribirBitacora("Barcode Nuevo: ", l_barcode2);
                                         c_barcodenuevo.Text = l_barcode2;
                                         c_barcodenuevo.Visible = true;
                                     }
                                     else
                                     {
                                         EscribirNumeroDeBOLSA("000000");
                                         Globales.NumeroSerieBOLSA = "000000";
                                         c_barcodenuevo.Text = "NO Capturado";
                                         c_barcodenuevo.Visible = true;
                                     }

                                }
                               
                            }

                            c_paso5.Text = "PONER BOLSA NUEVA";
                            Forzardibujado();
                            //  l_Reply = SidLib.SID_WaitReplaceBag(60); /* Wait the replacement of the bag*/

                            do
                            { //Wait a new bag insertion 
                                System.Threading.Thread.Sleep(300);
                                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                EscribirBitacora("Bag insertion", sensor[7].ToString());
                            } while ((sensor[7] & 0x02) == 2);

                            EscribirBitacora("Bag insertion", sensor[7].ToString());
                            EscribirBitacora("Bag COrrcet ??", sensor[7].ToString());
                            c_paso5.Text = "VERIFICANDO BOLSA, Espere...";
                            c_esperarInicio.Visible = true;
                            Forzardibujado();
                            //VERIFICAR BOLSA PUESTA

                           

                            do
                            {
                                System.Threading.Thread.Sleep(100);
                                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);

                                l_bolsacorectacontador++;
                               
                            } while ((sensor[7] & 0x02) != 2 && l_bolsacorectacontador < 101);

                           

                            EscribirBitacora("Bag STATUS", sensor[7].ToString());
                            EscribirBitacora("Ciclo completo: ", l_bolsacorectacontador.ToString());

                            if (l_bolsacorectacontador < 101)
                            {
                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = "Por favor verifique que la  BOLSA este bien puesta, solo tendra una oportunidad mas.";
                                    f_Error.c_Imagen.Visible = true;
                                    f_Error.ShowDialog();
                                }

                                EscribirBitacora("Bolsa Mal Puesta ", "Reintentar  ");
                                c_paso5.Text = "Re - VERIFICANDO BOLSA, Espere...";
                                c_esperarInicio.Visible = true;
                                Forzardibujado();
                                l_bolsacorectacontador = 0;
                                do
                                {
                                    System.Threading.Thread.Sleep(100);
                                    l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);

                                    l_bolsacorectacontador++;
                                } while ((sensor[7] & 0x02) != 2 && l_bolsacorectacontador < 101);
                                EscribirBitacora("Bag STATUS", sensor[7].ToString());
                                EscribirBitacora("SEGUNDO Ciclo completo: ", l_bolsacorectacontador.ToString());
                                //Acciones a tomar
                                if (l_bolsacorectacontador < 101)
                                {
                                    using (FormError f_Error = new FormError(false))
                                    {
                                        f_Error.c_MensajeError.Text = "Esta es la ultima Oportunidad, Verifique  BOLSA este bien puesta, en caso contrario, usted obtendra su Ticket pero se mandara una alerta de Servicio para este Equipo";
                                        f_Error.c_Imagen.Visible = true;
                                        f_Error.ShowDialog();
                                    }

                                    EscribirBitacora("Bolsa Mal Puesta ", "Ultimo Reintento  ");
                                    c_paso5.Text = "Re - VERIFICANDO BOLSA, Espere...";
                                    c_esperarInicio.Visible = true;
                                    Forzardibujado();
                                    l_bolsacorectacontador = 0;
                                    do
                                    {
                                        System.Threading.Thread.Sleep(100);
                                        l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);

                                        l_bolsacorectacontador++;
                                    } while ((sensor[7] & 0x02) != 2 && l_bolsacorectacontador < 101);

                                    EscribirBitacora("Bag STATUS", sensor[7].ToString());
                                    EscribirBitacora("SEGUNDO Ciclo completo: ", l_bolsacorectacontador.ToString());
                                    //Acciones a tomar
                                    //Enviar ALerta
                                    //continuar Proceso...

                                }
                                else
                                {
                                    //Entregar Ticket

                                    c_ok.Visible = true;
                                    paso5.Text = "BOLSA OK";
                                    Forzardibujado();
                                }


                            }
                            else
                            {
                                c_ok.Visible = true;
                                paso5.Text = "BOLSA OK";
                                c_esperarInicio.Visible = false;
                                Forzardibujado();
                            }

                            paso6.Visible = true;
                            c_paso6.Visible = true;
                            c_esperarInicio.Visible = true;
                            Forzardibujado();

                            l_Reply = SidLib.SID_WaitDoorClosed(120); /*Wait closing the door */

                            EscribirBitacora("wait Door CLOSE", l_Reply.ToString());

                            if (l_Reply == SidLib.SID_TIME_OUT_EXPIRED)
                            {
                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta,solo tendra una oportunidad mas.";
                                    f_Error.c_Imagen.Visible = false;
                                    f_Error.ShowDialog();
                                }
                                Forzardibujado();
                                l_Reply = SidLib.SID_WaitDoorClosed(30); /*Wait closing the door */

                                EscribirBitacora("2do wait Door CLOSE", l_Reply.ToString());
                            }


                            if (l_Reply == SidLib.SID_OKAY) ///check corect instlacion
                            {
                                c_paso6.Text = "Espere inicializando Modulo";
                                Forzardibujado();

                                l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                EscribirBitacora("STATUS : ", l_Reply.ToString());
                                EscribirBitacora("SENSOR[2] : ", sensor[2].ToString());
                                EscribirBitacora("SENSOR [7] : ", sensor[7].ToString());
                                if (((sensor[7] & 0x02) == 2))
                                {
                                    do
                                    {
                                        MessageBox.Show("Revise la colocación de la bolsa");

                                        MessageBox.Show("Introduzca la clave");
                                        l_Reply = SidLib.SID_CheckCodeLockCorrect(30);

                                        if (l_Reply == SidLib.SID_OKAY)
                                        {
                                            MessageBox.Show("Abra la Boveda, revise la bolsa y cierre la puerta");
                                            l_Reply = SidLib.SID_EnableToOpenDoor();
                                            if (l_Reply == SidLib.SID_OKAY)
                                            {
                                                l_Reply = SidLib.SID_WaitDoorOpen();
                                            }
                                            else
                                                l_Reply = SidLib.SID_ForceOpenDoor();


                                            if (l_Reply == SidLib.SID_OKAY)
                                            {
                                                l_Reply = SidLib.SID_WaitReplaceBag(60); /* Wait the replacement of the bag*/


                                                l_Reply = SidLib.SID_WaitDoorClosed(60); /*Wait closing the door */


                                            }
                                        }

                                        l_Reply = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                        EscribirBitacora("STATUS : ", l_Reply.ToString());
                                        EscribirBitacora("SENSOR[2] : ", sensor[2].ToString());
                                        EscribirBitacora("SENSOR [7] : ", sensor[7].ToString());

                                    } while (((sensor[7] & 0x02) == 2));

                                        c_paso6.Text = "Terminado";
                                        c_esperarInicio.Visible = false;
                                        EscribirProcesoExitoso(1);
                                        Exitoso = true;
                                        Forzardibujado();
                                    

                                }
                                else
                                {
                                    c_paso6.Text = "Terminado";
                                    c_esperarInicio.Visible = false;
                                    EscribirProcesoExitoso(1);
                                    Exitoso = true;
                                    Forzardibujado();

                                }
                            }
                            else
                            {
                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = "Tiempo Agotado para Cerrar Puerta, Problablemente se necesite un Master RESET antes de volver a SELLAR";
                                    f_Error.c_Imagen.Visible = false;
                                    EscribirProcesoExitoso(0);
                                    f_Error.ShowDialog();
                                }
                                Exitoso = true;
                                this.DialogResult = DialogResult.OK;
                                Prosegur.Alerta("MAL Procedimineto de SELLADO, Problablemente No se Verifico la Colocacion de Bolsa o tiempo agotado para cerrar.");
                                c_Salir.Enabled = true;
                                return;
                            }

                        }
                        else
                        {
                            using (FormError f_Error = new FormError(false))
                            {
                                f_Error.c_MensajeError.Text = "Tiempo Agotado para Abrir Puerta repita Operación";
                                f_Error.c_Imagen.Visible = false;
                                EscribirProcesoExitoso(0);
                                f_Error.ShowDialog();
                            }
                            Exitoso = false;
                            return;
                        }
                    }
                    else
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Mensaje de Apertura Rechazado: Error: " + l_Reply;
                            f_Error.c_Imagen.Visible = false;
                            EscribirProcesoExitoso(0);
                            f_Error.ShowDialog();
                        }
                        Exitoso = false;
                        return;
                    }


                    l_Reply  = SidApi.SidLib.SID_Logout();
                    Globales.EscribirBitacora("SELECT ETV", "SID_LOgout", "logout: " + l_Reply , 3);

                l_Reply = SidLib.SID_Close(); /*Disconnect from unit */

            }
            else
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Codigo incorecto o se agoto el tiempo de espera";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.ShowDialog();
                }
                Exitoso = false;
                return;
            }

        }

        private void FormCambioBolsa_FormClosing(object sender, FormClosingEventArgs e)
        {

           
            if (SELLADA)
                this.DialogResult = DialogResult.Ignore;
            else
                this.DialogResult = DialogResult.Abort;

            if (Exitoso && SELLADA)
            {
                this.DialogResult = DialogResult.OK;
                EscribirProcesoExitoso(1);
            }
            else
                EscribirProcesoExitoso(0);
            EscribirBitacora("Exitoso ", Exitoso.ToString());
            EscribirBitacora("SELLADA : ", SELLADA.ToString());
        }

        private void FormCambioBolsa_Load(object sender, EventArgs e)
        {
            Forzardibujado();
            SensorBolsaRemovida = Globales.SensorRemoverBolsa;

            using (FormError f_Error = new FormError(false))
            {
                f_Error.c_MensajeError.Text = "Por Favor siga la Secuencia Descrita en la siguiente pantalla " +" INSTRUCCIONES";
                f_Error.ShowDialog();
            }

            

        }

        private void c_empezar_Click(object sender, EventArgs e)
        {
            c_empezar.Enabled = false;
            c_Salir.Enabled = false;
            paso1.Visible = true;
            c_paso1.Visible = true;
            Forzardibujado();

            Prosegur.Alerta("Intento de Sellado de BOLSA, ...Empieza Procedimiento...");

            Secuencia();

            Close();
        }

        public void
           EscribirBitacora(String p_Funcion, String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.AppendText("Sellado.txt"))
            {
                l_Archivo.WriteLine(DateTime.Now + p_Funcion + ":  " + p_Mensaje);
            }

            Globales.EscribirBitacora("Area DE SELLADO", p_Funcion, p_Mensaje, 3);
        }

        public void
           EscribirNumeroDeBOLSA(String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("NumeroBOLSA.txt"))
            {
                l_Archivo.WriteLine(p_Mensaje);
            }
        }

        public void
          EscribirPorcentaje(int p_Porcentaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("PorcentajeSellado.txt"))
            {
                l_Archivo.WriteLine(p_Porcentaje);
            }
        }


        public void
          EscribirProcesoExitoso(int p_Procesocompleto)
        {
            if (File.Exists("PorcentajeSellado.txt"))
            {
                using (StreamWriter l_Archivo = File.AppendText("PorcentajeSellado.txt"))
                {
                    l_Archivo.WriteLine(p_Procesocompleto);
                }
            }
        }

        public void LeerPorcentaje(out bool p_exitoso)
        {
            int lectura = 0;

            if (File.Exists("PorcentajeSellado.txt"))
            {
                String[] lineas = File.ReadAllLines("PorcentajeSellado.txt");

                if (lineas.Length > 0)
                {
                    m_porcentajeAnterior = Convert.ToInt16(lineas[0]);
                }
                else
                    m_porcentajeAnterior = 100;

                if (lineas.Length > 1)
                {
                    lectura = Convert.ToInt16(lineas[lineas.Length - 1]);

                    if (lectura == 1)
                        p_exitoso = true;
                    else
                        p_exitoso = false;

                }
                else
                    p_exitoso = true;


            }
            else
            {
                m_porcentajeAnterior = 100;
                p_exitoso = true;
            }

        }

        private void c_Salir_Click(object sender, EventArgs e)
        {
            GC.Collect();
            this.DialogResult = DialogResult.Abort;
            Close();
        }

        private void c_paso4_TextChanged(object sender, EventArgs e)
        {
            
        }


    }
}
