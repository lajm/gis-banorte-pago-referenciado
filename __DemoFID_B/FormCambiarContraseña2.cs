﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormCambiarContraseña2 : Form
    {
        int l_idTeclado = 0;

        public FormCambiarContraseña2()
        {
            InitializeComponent();
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Cambiar_Click(object sender, EventArgs e)
        {

            if (c_nuevacontraseña.Text == c_confirmacionContraseña.Text)
            {
                if (!Globales.AutenticarUsuario(Globales.IdUsuario, c_contraseña.Text))
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Contraseña Actual  Inválida";
                        f_Error.ShowDialog();
                        return;
                    }

                }
                else
                {
                    if (!BDUsuarios.ActualizarContraseña(Globales.IdUsuario, 3, c_nuevacontraseña.Text))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "No se pudo Actualizar la contraseña";
                            f_Error.ShowDialog();
                            return;
                        }
                    }
                    else
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Contraseña Cambiada";
                            f_Error.ShowDialog();
                            Close();
                        }
                    }

                }
            }
            else
            {
                using (FormError f_error = new FormError(false))
                {
                    f_error.c_MensajeError.Text = "La Confirmación de contraseña no coindice";
                    f_error.ShowDialog();
                    return;
                }
            }



        }

        private void FormCambiarContraseña_Load(object sender, EventArgs e)
        {
            Application.DoEvents();
            Update();
            Refresh();
            Application.DoEvents();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_contraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_contraseña.Text = l_teclado.Captura;

            }
        }

        private void c_nuevacontraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_nuevacontraseña.Text = l_teclado.Captura;

            }
        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_confirmacionContraseña.Text = l_teclado.Captura;

            }
        }

    }
}
