﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace __DemoFID_B
{
    public partial class FormOpciones : Form
    {
        public FormOpciones()
        {
            InitializeComponent();
            
        }

        private void FormOpciones_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
            if (Globales.IdTipoUsuario == 1)
            {
               c_BotonHerramientas.Visible = false;
               // c_BotonHerramientas.Location = new Point(84, 214);
                c_OpcionConsultaEfectivo.Visible = false;
                c_OpcionConsultaOperacion.Visible = false;
                c_OpcionCreaLayout.Visible = false;
                c_OpcionRetiroETV.Visible = false;
            }
            else if (Globales.IdTipoUsuario == 3)
            {
                OPERACIONES.TabPages.Remove(tabPage3);
               

              //  c_OpcionConsultaEfectivo.Location = new Point(84, 167);
              //  c_OpcionConsultaOperacion.Location = new Point(84, 214);
              //  c_BotonHerramientas.Location = new Point(84, 261);
                c_OpcionCreaLayout.Visible=true;
                c_OpcionDeposito.Visible = false;
                c_OpcionRetiroETV.Visible = false;
                //c_BotonHerramientas.Visible = false;
            }
            else
            {
                OPERACIONES.TabPages.Remove(tabPage1);
                OPERACIONES.TabPages.Remove(tabPage2);

                c_numeroBolsa.Visible = false;
                c_OpcionDeposito.Visible = true;
                c_BotonHerramientas.Visible = false;
                c_OpcionConsultaEfectivo.Visible = false;
                c_OpcionConsultaOperacion.Visible = false;
                c_OpcionCreaLayout.Visible = false;
              //  c_OpcionRetiroETV.Location = new Point(84, 167);
                c_administarusuarios.Visible = false;
                //c_OpcionRetiroETV.Visible = false;
            }
        }

        private void c_OpcionDeposito_Click(object sender, EventArgs e)
        {
            int l_Reply = 0;
            String l_Error;

            using (FormError f_Mensaje = new FormError(false))
            {
                f_Mensaje.c_Imagen.Visible = false;
                f_Mensaje.c_MensajeError.Text =
                            " SE GENERARA UNA ALERTA DE APERTURA DE BOVEDAD ¿Desea Continuar?";
                DialogResult l_respuesta = f_Mensaje.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                {
                    Prosegur.Alerta("INTENTO DE APERTURA FORZADA DE BOVEDAD POR ETV: " +DateTime.Now.ToLongDateString ()+ DateTime.Now.ToLongTimeString() );
                    if (Globales.DEBUG)
                    {
                        Globales.EscribirBitacora("APERTURA DE BOVEDA FORZADA", " INICIAR", Globales.IdUsuario,1);
                        l_Reply = SidApi.SidLib.SID_Open(true);
                        if (l_Reply == SidApi.SidLib.SID_OKAY || l_Reply == SidApi.SidLib.SID_ALREADY_OPEN)
                        {
                            MessageBox.Show("Presione Enter y despues Introduzca el Codigo y Gire, tiene 30 segundos");
                            l_Reply = SidApi.SidLib.SID_CheckCodeLockCorrect(30);
                           
                            if (l_Reply == SidApi.SidLib.SID_OKAY)
                            {
                                Globales.EscribirBitacora("APERTURA DE BOVEDA FORZADA", " CODIGO CORRECTO ", Globales.IdUsuario,1);
                                l_Reply = SidApi.SidLib.SID_ForceOpenDoor();
                                if (l_Reply == SidApi.SidLib.SID_OKAY)
                                {
                                    Prosegur.Alerta("APERTURA FORZADA DE BOVEDAD POR ETV REALIZADA CON EXITO, El Equipo esta abierto : " + DateTime.Now.ToLongDateString() + DateTime.Now.ToLongTimeString());
                                   
                                    Globales.EscribirBitacora("APERTURA DE BOVEDA FORZADA", "EXITOSA", Globales.IdUsuario,1);
                                    MessageBox.Show("....Exito al abrir la puerta...");
                                }
                                else
                                {
                                    SidApi.SidLib.Error_Sid(l_Reply, out l_Error);
                                    Globales.EscribirBitacora("APERTURA DE BOVEDA FORZADA", "NO REALIZADA:: ", l_Error,1);

                                    MessageBox.Show("Intentelo mas tarde posible razon: " + l_Error);


                                }
                            }

                        }
                        else
                        {
                            SidApi.SidLib.Error_Sid(l_Reply, out l_Error);
                            Globales.EscribirBitacora("APERTURA DE BOVEDA FORZADA", "NO REALIZADA:: ", l_Error,1);
                            using (FormError f_Mensaje2 = new FormError(false))
                            {
                                f_Mensaje2.c_Imagen.Visible = false;
                                f_Mensaje2.c_MensajeError.Text =
                                  " NO POSIBLE POR LE MOMENTO " + l_Error;
                            }
                        }
                        l_Reply = SidApi.SidLib.SID_Close();
                    }
                }
            }
        }

        private void c_BotonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_OpcionConsultaEfectivo_Click(object sender, EventArgs e)
        {
            using (FormConsultaEfectivo f_ConsultaPesos = new FormConsultaEfectivo())
            {
                f_ConsultaPesos.ShowDialog();
            }

          /*  using (FormConsultaEfectivoDolares f_ConsultaDolares = new FormConsultaEfectivoDolares())
            {
                f_ConsultaDolares.ShowDialog();
            }*/
            //Close();
        }

        private void c_OpcionConsultaOperacion_Click(object sender, EventArgs e)
        {
            using (FormConsultaOperaciones f_Operaciones = new FormConsultaOperaciones())
            {
                f_Operaciones.ShowDialog();
              //  Close();
            }
        }

        private void c_OpcionRetiroETV_Click(object sender, EventArgs e)
        {
            using (FormRetiroEfectivo f_Retiro = new FormRetiroEfectivo())
            {
                f_Retiro.ShowDialog();
                Close();
            }
        }

        private void c_BotonHerramientas_Click(object sender, EventArgs e)
        {
            /*if (!UtilsComunicacion.EnviarCorte(1,Globales.IdUsuario, true))
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "No se pudo insertar el corte, recoja el dinero del ESCROW";
                    f_Error.ShowDialog();

                    return;
                }
            }
            else
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "CORTE ENVIADO EXITOSAMENTE";
                    f_Error.ShowDialog();

                    return;
                }
            }
            */
            Prosegur.Alerta("Apagando Equipo desde SISTEMA");
            Application.Exit();

          
        }

        private void c_OpcionConsultaRechazos_Click(object sender, EventArgs e)
        {
          

            if (Prosegur.MandarBitacoraActual())
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Bitacora Mandada con Exito";
                    f_Error.ShowDialog();

                }

                Globales.EscribirBitacora("Enviar Bitacora Actual", "MandarBitacoraActual", "Bitacora Mandada con Exito", 1);
            }
            else
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Error Enviando Bitacora...";
                    f_Error.ShowDialog();

                }
                Globales.EscribirBitacora("Enviar Bitacora Actual", "MandarBitacoraActual", "Erro en la creación, o en el envio de Bitacora", 1);
            }
        }

        private void c_Reset_Click(object sender, EventArgs e)
        {
            c_Reset.Enabled = false;


            SidApi.SidLib.ResetPath();

            SidApi.SidLib.ResetPath();

            c_Reset.Enabled = true;

        }

        private void c_administarusuarios_Click(object sender, EventArgs e)
        {
            //using (FormAdministarUsers f_administrar = new FormAdministarUsers())
            //{

            //    f_administrar.ShowDialog();

            //}

            using (FormAbcClientes f_adminClinete = new FormAbcClientes())
            {

                f_adminClinete.ShowDialog();

            }
        }

        private void c_numeroBolsa_Click(object sender, EventArgs e)
        {
            using (FormCambioNumeroBolsa f_cambiobolsa = new FormCambioNumeroBolsa ())
            {

                f_cambiobolsa . ShowDialog();

            }


           
        }

        private void c_contabilidad_Click(object sender, EventArgs e)
        {

            using (FormInciarContabilidad f_Cont = new FormInciarContabilidad())
            {

                f_Cont.l_IdUsuario = "Supervisor_CONT";
                f_Cont.ShowDialog();
                f_Cont.Close();
            }

            //using (FormError f_Mensaje = new FormError(false))
            //{
            //    f_Mensaje.c_Imagen.Visible = false;
            //    f_Mensaje.c_MensajeError.Text =
            //                " SE GENERARA UNA ALERTA PARA EL RESET DE CONTABILIDAD ¿Desea Continuar?";
            //    DialogResult l_respuesta = f_Mensaje.ShowDialog();

            //    if (l_respuesta == DialogResult.OK)
            //    {
            //        using (FormError f_MensajeOK = new FormError(false))
            //        {
            //            f_MensajeOK.c_Imagen.Visible = false;
            //            f_MensajeOK.c_MensajeError.Text =
            //                        " CONFIRME EL RESET DE CONTABILIDAD ¡No podra deshacer esta Operación!....";
            //            DialogResult l_respuestaOK = f_MensajeOK.ShowDialog();

            //            if (l_respuestaOK == DialogResult.OK)
            //            {

            //                BDRetiro.ActualizarRetiros("Equipo1");
            //                using (FormError f_OK = new FormError(true))
            //                {
            //                    f_OK.c_Imagen.Visible = true;
            //                    f_OK.c_MensajeError.Text = "Contabilidad en 0s";
            //                    f_OK.ShowDialog();
            //                }
            //            }
            //            Globales.EscribirBitacora("OPCION ADMINISTRADOR", "Confirmación RESET CONTABILIDAD", l_respuestaOK.ToString(), 1);
            //        }
            //    }

            //    Globales.EscribirBitacora("OPCION ADMINISTRADOR", "RESET CONTABILIDAD", l_respuesta.ToString(), 1);

            //}



        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FormAdministarUsers f_administrar = new FormAdministarUsers())
            {

                f_administrar.ShowDialog();

            }
        }

        private void c_importar_Click(object sender, EventArgs e)
        {
            using (FormImportCliente l_Forma = new FormImportCliente())
            {
                l_Forma.ShowDialog();
                l_Forma.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (FormImportCliente1 l_Forma = new FormImportCliente1())
            {
                l_Forma.ShowDialog();
                l_Forma.Close();
            }
        }
    }
}
