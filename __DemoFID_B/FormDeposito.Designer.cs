﻿namespace __DemoFID_B
{
    partial class FormDeposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDeposito));
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_tope = new System.Windows.Forms.Label();
            this.c_empleado = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Depositar = new System.Windows.Forms.Button();
            this.c_EtiquetaNumeroCuenta = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_TotalDeposito = new System.Windows.Forms.TextBox();
            this.c_BilletesRechazados = new System.Windows.Forms.TextBox();
            this.c_BilletesAceptados = new System.Windows.Forms.TextBox();
            this.c_Suma20 = new System.Windows.Forms.TextBox();
            this.c_Suma50 = new System.Windows.Forms.TextBox();
            this.c_Suma100 = new System.Windows.Forms.TextBox();
            this.c_Suma200 = new System.Windows.Forms.TextBox();
            this.c_Suma500 = new System.Windows.Forms.TextBox();
            this.c_Suma1000 = new System.Windows.Forms.TextBox();
            this.c_Cantidad20 = new System.Windows.Forms.TextBox();
            this.c_Cantidad50 = new System.Windows.Forms.TextBox();
            this.c_Cantidad100 = new System.Windows.Forms.TextBox();
            this.c_Cantidad200 = new System.Windows.Forms.TextBox();
            this.c_Cantidad500 = new System.Windows.Forms.TextBox();
            this.c_Cantidad1000 = new System.Windows.Forms.TextBox();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.c_tope);
            this.panel1.Controls.Add(this.c_empleado);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_Depositar);
            this.panel1.Controls.Add(this.c_EtiquetaNumeroCuenta);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Controls.Add(this.c_TotalDeposito);
            this.panel1.Controls.Add(this.c_BilletesRechazados);
            this.panel1.Controls.Add(this.c_BilletesAceptados);
            this.panel1.Controls.Add(this.c_Suma20);
            this.panel1.Controls.Add(this.c_Suma50);
            this.panel1.Controls.Add(this.c_Suma100);
            this.panel1.Controls.Add(this.c_Suma200);
            this.panel1.Controls.Add(this.c_Suma500);
            this.panel1.Controls.Add(this.c_Suma1000);
            this.panel1.Controls.Add(this.c_Cantidad20);
            this.panel1.Controls.Add(this.c_Cantidad50);
            this.panel1.Controls.Add(this.c_Cantidad100);
            this.panel1.Controls.Add(this.c_Cantidad200);
            this.panel1.Controls.Add(this.c_Cantidad500);
            this.panel1.Controls.Add(this.c_Cantidad1000);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // c_tope
            // 
            this.c_tope.AutoSize = true;
            this.c_tope.BackColor = System.Drawing.Color.White;
            this.c_tope.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_tope.ForeColor = System.Drawing.Color.Red;
            this.c_tope.Location = new System.Drawing.Point(21, 25);
            this.c_tope.Name = "c_tope";
            this.c_tope.Size = new System.Drawing.Size(206, 25);
            this.c_tope.TabIndex = 59;
            this.c_tope.Text = "Maximo Permitido:";
            this.c_tope.Visible = false;
            // 
            // c_empleado
            // 
            this.c_empleado.AutoSize = true;
            this.c_empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_empleado.ForeColor = System.Drawing.SystemColors.Highlight;
            this.c_empleado.Location = new System.Drawing.Point(66, 111);
            this.c_empleado.Name = "c_empleado";
            this.c_empleado.Size = new System.Drawing.Size(79, 16);
            this.c_empleado.TabIndex = 58;
            this.c_empleado.Text = "Empleado";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(500, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(162, 20);
            this.label14.TabIndex = 57;
            this.label14.Text = "Nombre EMPRESA";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(351, 468);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 26);
            this.label13.TabIndex = 56;
            this.label13.Text = "Total Depósito:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(598, 194);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 24);
            this.label12.TabIndex = 55;
            this.label12.Text = "Suma";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(373, 194);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 24);
            this.label11.TabIndex = 54;
            this.label11.Text = "Cantidad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(133, 426);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 53;
            this.label8.Text = "$20.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(133, 390);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 24);
            this.label7.TabIndex = 52;
            this.label7.Text = "$50.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(123, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 51;
            this.label6.Text = "$100.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(123, 318);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 50;
            this.label5.Text = "$200.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(123, 282);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 24);
            this.label4.TabIndex = 49;
            this.label4.Text = "$500.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(108, 246);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 24);
            this.label9.TabIndex = 48;
            this.label9.Text = "$1,000.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(65, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 24);
            this.label10.TabIndex = 47;
            this.label10.Text = "Denominación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(92, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "BILLETES RECHAZADOS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(108, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 20);
            this.label2.TabIndex = 38;
            this.label2.Text = "BILLETES ACEPTADOS:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(306, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 24);
            this.label1.TabIndex = 37;
            this.label1.Text = "DEPOSITO PESOS";
            // 
            // c_Depositar
            // 
            this.c_Depositar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Depositar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Depositar.Location = new System.Drawing.Point(144, 496);
            this.c_Depositar.Name = "c_Depositar";
            this.c_Depositar.Size = new System.Drawing.Size(237, 53);
            this.c_Depositar.TabIndex = 36;
            this.c_Depositar.TabStop = false;
            this.c_Depositar.Text = "DEPOSITAR";
            this.c_Depositar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Depositar.UseVisualStyleBackColor = true;
            this.c_Depositar.Click += new System.EventHandler(this.c_Depositar_Click);
            // 
            // c_EtiquetaNumeroCuenta
            // 
            this.c_EtiquetaNumeroCuenta.AutoSize = true;
            this.c_EtiquetaNumeroCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaNumeroCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaNumeroCuenta.Location = new System.Drawing.Point(500, 161);
            this.c_EtiquetaNumeroCuenta.Name = "c_EtiquetaNumeroCuenta";
            this.c_EtiquetaNumeroCuenta.Size = new System.Drawing.Size(135, 20);
            this.c_EtiquetaNumeroCuenta.TabIndex = 35;
            this.c_EtiquetaNumeroCuenta.Text = "00000000000001";
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(419, 496);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(237, 53);
            this.c_BotonAceptar.TabIndex = 33;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "TERMINAR";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_TotalDeposito
            // 
            this.c_TotalDeposito.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_TotalDeposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalDeposito.Location = new System.Drawing.Point(514, 468);
            this.c_TotalDeposito.Name = "c_TotalDeposito";
            this.c_TotalDeposito.ReadOnly = true;
            this.c_TotalDeposito.Size = new System.Drawing.Size(188, 22);
            this.c_TotalDeposito.TabIndex = 32;
            this.c_TotalDeposito.TabStop = false;
            this.c_TotalDeposito.Text = "$0.00";
            this.c_TotalDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_BilletesRechazados
            // 
            this.c_BilletesRechazados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesRechazados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesRechazados.Location = new System.Drawing.Point(347, 158);
            this.c_BilletesRechazados.Name = "c_BilletesRechazados";
            this.c_BilletesRechazados.ReadOnly = true;
            this.c_BilletesRechazados.Size = new System.Drawing.Size(86, 19);
            this.c_BilletesRechazados.TabIndex = 31;
            this.c_BilletesRechazados.TabStop = false;
            this.c_BilletesRechazados.Text = "No";
            // 
            // c_BilletesAceptados
            // 
            this.c_BilletesAceptados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_BilletesAceptados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesAceptados.Location = new System.Drawing.Point(347, 133);
            this.c_BilletesAceptados.Name = "c_BilletesAceptados";
            this.c_BilletesAceptados.ReadOnly = true;
            this.c_BilletesAceptados.Size = new System.Drawing.Size(86, 19);
            this.c_BilletesAceptados.TabIndex = 30;
            this.c_BilletesAceptados.TabStop = false;
            this.c_BilletesAceptados.Text = "0";
            this.c_BilletesAceptados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma20
            // 
            this.c_Suma20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma20.Location = new System.Drawing.Point(532, 428);
            this.c_Suma20.Name = "c_Suma20";
            this.c_Suma20.ReadOnly = true;
            this.c_Suma20.Size = new System.Drawing.Size(170, 22);
            this.c_Suma20.TabIndex = 29;
            this.c_Suma20.TabStop = false;
            this.c_Suma20.Text = "$0.00";
            this.c_Suma20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma50
            // 
            this.c_Suma50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma50.Location = new System.Drawing.Point(532, 392);
            this.c_Suma50.Name = "c_Suma50";
            this.c_Suma50.ReadOnly = true;
            this.c_Suma50.Size = new System.Drawing.Size(170, 22);
            this.c_Suma50.TabIndex = 28;
            this.c_Suma50.TabStop = false;
            this.c_Suma50.Text = "$0.00";
            this.c_Suma50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma100
            // 
            this.c_Suma100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma100.Location = new System.Drawing.Point(532, 356);
            this.c_Suma100.Name = "c_Suma100";
            this.c_Suma100.ReadOnly = true;
            this.c_Suma100.Size = new System.Drawing.Size(170, 22);
            this.c_Suma100.TabIndex = 27;
            this.c_Suma100.TabStop = false;
            this.c_Suma100.Text = "$0.00";
            this.c_Suma100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma200
            // 
            this.c_Suma200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma200.Location = new System.Drawing.Point(532, 320);
            this.c_Suma200.Name = "c_Suma200";
            this.c_Suma200.ReadOnly = true;
            this.c_Suma200.Size = new System.Drawing.Size(170, 22);
            this.c_Suma200.TabIndex = 26;
            this.c_Suma200.TabStop = false;
            this.c_Suma200.Text = "$0.00";
            this.c_Suma200.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma500
            // 
            this.c_Suma500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma500.Location = new System.Drawing.Point(532, 284);
            this.c_Suma500.Name = "c_Suma500";
            this.c_Suma500.ReadOnly = true;
            this.c_Suma500.Size = new System.Drawing.Size(170, 22);
            this.c_Suma500.TabIndex = 25;
            this.c_Suma500.TabStop = false;
            this.c_Suma500.Text = "$0.00";
            this.c_Suma500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma1000
            // 
            this.c_Suma1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Suma1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma1000.Location = new System.Drawing.Point(532, 248);
            this.c_Suma1000.Name = "c_Suma1000";
            this.c_Suma1000.ReadOnly = true;
            this.c_Suma1000.Size = new System.Drawing.Size(170, 22);
            this.c_Suma1000.TabIndex = 24;
            this.c_Suma1000.TabStop = false;
            this.c_Suma1000.Text = "$0.00";
            this.c_Suma1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad20
            // 
            this.c_Cantidad20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad20.Location = new System.Drawing.Point(337, 428);
            this.c_Cantidad20.Name = "c_Cantidad20";
            this.c_Cantidad20.ReadOnly = true;
            this.c_Cantidad20.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad20.TabIndex = 23;
            this.c_Cantidad20.TabStop = false;
            this.c_Cantidad20.Text = "0";
            this.c_Cantidad20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad50
            // 
            this.c_Cantidad50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad50.Location = new System.Drawing.Point(337, 392);
            this.c_Cantidad50.Name = "c_Cantidad50";
            this.c_Cantidad50.ReadOnly = true;
            this.c_Cantidad50.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad50.TabIndex = 22;
            this.c_Cantidad50.TabStop = false;
            this.c_Cantidad50.Text = "0";
            this.c_Cantidad50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad100
            // 
            this.c_Cantidad100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad100.Location = new System.Drawing.Point(337, 356);
            this.c_Cantidad100.Name = "c_Cantidad100";
            this.c_Cantidad100.ReadOnly = true;
            this.c_Cantidad100.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad100.TabIndex = 21;
            this.c_Cantidad100.TabStop = false;
            this.c_Cantidad100.Text = "0";
            this.c_Cantidad100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad200
            // 
            this.c_Cantidad200.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad200.Location = new System.Drawing.Point(337, 320);
            this.c_Cantidad200.Name = "c_Cantidad200";
            this.c_Cantidad200.ReadOnly = true;
            this.c_Cantidad200.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad200.TabIndex = 20;
            this.c_Cantidad200.TabStop = false;
            this.c_Cantidad200.Text = "0";
            this.c_Cantidad200.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad500
            // 
            this.c_Cantidad500.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad500.Location = new System.Drawing.Point(337, 284);
            this.c_Cantidad500.Name = "c_Cantidad500";
            this.c_Cantidad500.ReadOnly = true;
            this.c_Cantidad500.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad500.TabIndex = 19;
            this.c_Cantidad500.TabStop = false;
            this.c_Cantidad500.Text = "0";
            this.c_Cantidad500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad1000
            // 
            this.c_Cantidad1000.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_Cantidad1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad1000.Location = new System.Drawing.Point(337, 248);
            this.c_Cantidad1000.Name = "c_Cantidad1000";
            this.c_Cantidad1000.ReadOnly = true;
            this.c_Cantidad1000.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad1000.TabIndex = 18;
            this.c_Cantidad1000.TabStop = false;
            this.c_Cantidad1000.Text = "0";
            this.c_Cantidad1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // FormDeposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDeposito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDeposito";
            this.Load += new System.EventHandler(this.FormDeposito_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_TotalDeposito;
        private System.Windows.Forms.TextBox c_BilletesRechazados;
        private System.Windows.Forms.TextBox c_BilletesAceptados;
        private System.Windows.Forms.TextBox c_Suma20;
        private System.Windows.Forms.TextBox c_Suma50;
        private System.Windows.Forms.TextBox c_Suma100;
        private System.Windows.Forms.TextBox c_Suma200;
        private System.Windows.Forms.TextBox c_Suma500;
        private System.Windows.Forms.TextBox c_Suma1000;
        private System.Windows.Forms.TextBox c_Cantidad20;
        private System.Windows.Forms.TextBox c_Cantidad50;
        private System.Windows.Forms.TextBox c_Cantidad100;
        private System.Windows.Forms.TextBox c_Cantidad200;
        private System.Windows.Forms.TextBox c_Cantidad500;
        private System.Windows.Forms.TextBox c_Cantidad1000;
        private System.Windows.Forms.Button c_Depositar;
        private System.Windows.Forms.Label c_EtiquetaNumeroCuenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label c_empleado;
        private System.Windows.Forms.Label c_tope;
    }
}