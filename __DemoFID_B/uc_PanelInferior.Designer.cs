﻿namespace __DemoFID_B
{
    partial class uc_PanelInferior
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.c_FechaHora = new System.Windows.Forms.Label();
            this.c_TimerFechaHora = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_usuario = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c_FechaHora
            // 
            this.c_FechaHora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.c_FechaHora.AutoSize = true;
            this.c_FechaHora.BackColor = System.Drawing.Color.Transparent;
            this.c_FechaHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_FechaHora.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.c_FechaHora.Location = new System.Drawing.Point(600, 14);
            this.c_FechaHora.Name = "c_FechaHora";
            this.c_FechaHora.Size = new System.Drawing.Size(197, 25);
            this.c_FechaHora.TabIndex = 0;
            this.c_FechaHora.Text = "20-05-2011 10:03:05";
            // 
            // c_TimerFechaHora
            // 
            this.c_TimerFechaHora.Interval = 1000;
            this.c_TimerFechaHora.Tick += new System.EventHandler(this.c_TimerFechaHora_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Copyright © 2011 Symetry S.A de C.V.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(347, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Usuario Identificado :";
            // 
            // c_usuario
            // 
            this.c_usuario.AutoSize = true;
            this.c_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_usuario.Location = new System.Drawing.Point(383, 26);
            this.c_usuario.Name = "c_usuario";
            this.c_usuario.Size = new System.Drawing.Size(62, 16);
            this.c_usuario.TabIndex = 3;
            this.c_usuario.Text = "Usuario";
            // 
            // uc_PanelInferior
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FondoInferior;
            this.Controls.Add(this.c_usuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_FechaHora);
            this.Enabled = false;
            this.Name = "uc_PanelInferior";
            this.Size = new System.Drawing.Size(800, 50);
            this.Load += new System.EventHandler(this.uc_PanelInferior_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label c_FechaHora;
        private System.Windows.Forms.Timer c_TimerFechaHora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label c_usuario;
    }
}
