﻿namespace __DemoFID_B
{
    partial class FormDepositoC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDepositoC));
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_nombreUsuario = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Empleado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.c_equipo2 = new System.Windows.Forms.RadioButton();
            this.c_equipo1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_EtiquetaMoneda = new System.Windows.Forms.Label();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.c_nombreUsuario);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.c_Empleado);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.c_equipo2);
            this.panel1.Controls.Add(this.c_equipo1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_EtiquetaMoneda);
            this.panel1.Controls.Add(this.c_BotonCancelar);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
           // this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // c_nombreUsuario
            // 
            this.c_nombreUsuario.AutoSize = true;
            this.c_nombreUsuario.BackColor = System.Drawing.Color.Transparent;
            this.c_nombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nombreUsuario.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.c_nombreUsuario.Location = new System.Drawing.Point(39, 265);
            this.c_nombreUsuario.Name = "c_nombreUsuario";
            this.c_nombreUsuario.Size = new System.Drawing.Size(722, 42);
            this.c_nombreUsuario.TabIndex = 57;
            this.c_nombreUsuario.Text = "################################";
            this.c_nombreUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(278, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 24);
            this.label6.TabIndex = 56;
            this.label6.Text = "NOMBRE DEL EMPLEADO";
            // 
            // c_Empleado
            // 
            this.c_Empleado.AutoSize = true;
            this.c_Empleado.BackColor = System.Drawing.Color.Transparent;
            this.c_Empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Empleado.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.c_Empleado.Location = new System.Drawing.Point(325, 196);
            this.c_Empleado.Name = "c_Empleado";
            this.c_Empleado.Size = new System.Drawing.Size(150, 42);
            this.c_Empleado.TabIndex = 55;
            this.c_Empleado.Text = "######";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(283, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(235, 24);
            this.label4.TabIndex = 54;
            this.label4.Text = "NUMERO DE EMPLEADO";
            // 
            // c_equipo2
            // 
            this.c_equipo2.AutoSize = true;
            this.c_equipo2.Enabled = false;
            this.c_equipo2.Location = new System.Drawing.Point(684, 337);
            this.c_equipo2.Name = "c_equipo2";
            this.c_equipo2.Size = new System.Drawing.Size(64, 17);
            this.c_equipo2.TabIndex = 53;
            this.c_equipo2.Text = "Equipo2";
            this.c_equipo2.UseVisualStyleBackColor = true;
            this.c_equipo2.Visible = false;
            this.c_equipo2.CheckedChanged += new System.EventHandler(this.c_equipo2_CheckedChanged);
            // 
            // c_equipo1
            // 
            this.c_equipo1.AutoSize = true;
            this.c_equipo1.Checked = true;
            this.c_equipo1.Enabled = false;
            this.c_equipo1.Location = new System.Drawing.Point(570, 337);
            this.c_equipo1.Name = "c_equipo1";
            this.c_equipo1.Size = new System.Drawing.Size(64, 17);
            this.c_equipo1.TabIndex = 52;
            this.c_equipo1.TabStop = true;
            this.c_equipo1.Text = "Equipo1";
            this.c_equipo1.UseVisualStyleBackColor = true;
            this.c_equipo1.Visible = false;
            this.c_equipo1.CheckedChanged += new System.EventHandler(this.c_equipo1_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(249, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(303, 29);
            this.label3.TabIndex = 51;
            this.label3.Text = "DEPÓSITO DE EFECTIVO";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 386);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(776, 102);
            this.label2.TabIndex = 50;
            this.label2.Text = "Verifique su nombre y numero de Usuario  para que  los depósito sean correctos y " +
                "oprima continuar";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(282, 311);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 49;
            this.label1.Text = "Moneda";
            // 
            // c_EtiquetaMoneda
            // 
            this.c_EtiquetaMoneda.AutoSize = true;
            this.c_EtiquetaMoneda.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaMoneda.Location = new System.Drawing.Point(336, 346);
            this.c_EtiquetaMoneda.Name = "c_EtiquetaMoneda";
            this.c_EtiquetaMoneda.Size = new System.Drawing.Size(128, 42);
            this.c_EtiquetaMoneda.TabIndex = 25;
            this.c_EtiquetaMoneda.Text = "Pesos";
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonCancelar.Location = new System.Drawing.Point(417, 496);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(257, 53);
            this.c_BotonCancelar.TabIndex = 23;
            this.c_BotonCancelar.Text = "CANCELAR";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(127, 496);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(257, 53);
            this.c_BotonAceptar.TabIndex = 22;
            this.c_BotonAceptar.Text = "CONTINUAR";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // FormDepositoC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDepositoC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDepositoC";
            this.Load += new System.EventHandler(this.FormDepositoC_Load);
            this.Activated += new System.EventHandler(this.FormDepositoC_Activated);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        public System.Windows.Forms.Label c_EtiquetaMoneda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton c_equipo2;
        private System.Windows.Forms.RadioButton c_equipo1;
        public System.Windows.Forms.Label c_Empleado;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label c_nombreUsuario;
        private System.Windows.Forms.Label label6;
    }
}