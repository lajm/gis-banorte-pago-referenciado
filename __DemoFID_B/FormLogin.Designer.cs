﻿namespace __DemoFID_B
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.C_Usuario = new System.Windows.Forms.TextBox();
            this.c_Contraseña = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_BotonAceptar.Location = new System.Drawing.Point(117, 239);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(75, 43);
            this.c_BotonAceptar.TabIndex = 24;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Image = global::@__DemoFID_B.Properties.Resources.block;
            this.c_BotonCancelar.Location = new System.Drawing.Point(198, 239);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(75, 43);
            this.c_BotonCancelar.TabIndex = 25;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // C_Usuario
            // 
            this.C_Usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.C_Usuario.Location = new System.Drawing.Point(164, 97);
            this.C_Usuario.Name = "C_Usuario";
            this.C_Usuario.Size = new System.Drawing.Size(184, 26);
            this.C_Usuario.TabIndex = 26;
            this.C_Usuario.Text = "symet002";
            // 
            // c_Contraseña
            // 
            this.c_Contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Contraseña.Location = new System.Drawing.Point(164, 154);
            this.c_Contraseña.Name = "c_Contraseña";
            this.c_Contraseña.Size = new System.Drawing.Size(184, 26);
            this.c_Contraseña.TabIndex = 27;
            this.c_Contraseña.Text = "222222";
            this.c_Contraseña.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 28;
            this.label1.Text = "Usuario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Contraseña";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 24);
            this.label3.TabIndex = 30;
            this.label3.Text = "Por Favor Identifiquese";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(309, 205);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormLogin
            // 
            this.AcceptButton = this.c_BotonAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.c_BotonCancelar;
            this.ClientSize = new System.Drawing.Size(391, 286);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Contraseña);
            this.Controls.Add(this.C_Usuario);
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.TextBox C_Usuario;
        private System.Windows.Forms.TextBox c_Contraseña;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}