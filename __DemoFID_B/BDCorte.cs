﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace __DemoFID_B
{
   public class BDCorte
    {

        public static DataTable
            ObtenerCorte(int p_Corte)
        {
            String l_Query
                = "SELECT               IdUsuario, FechaHora,  Enviado, ProcessId "
                + "FROM                 Cortes "
                + "WHERE                IdCorte = @IdCorte ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCorte", p_Corte);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Corte");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

      

        public static DataTable
            BuscarCortesNoEnviados()
        {
            String l_Query
                = "SELECT               ProcessId, IdCorte, IdUsuario "
                + "FROM                 Cortes "
                + "WHERE                Enviado = 0 "
                + "ORDER BY             IdCorte ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Cortes");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static bool
            ConfirmarCorte(int p_IdCorte)
        {
            String l_Query
                = "UPDATE               Cortes "
                + "SET                  Enviado = 1 "
                + "WHERE                IdCorte = @IdCorte ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCorte", p_IdCorte);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            ActualizarProcessId(int p_IdCorte, Int64 p_IdProceso)
        {
            String l_Query
                = "UPDATE           Cortes "
                + "SET              ProcessId = @ProcessId "
                + "WHERE            IdCorte = @IdCorte ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@ProcessId", p_IdProceso);
                l_Comando.Parameters.AddWithValue("@IdCorte", p_IdCorte);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            InsertarCorte(String p_IdUsuario, out int p_IdCorte)
        {
            p_IdCorte = FolioCorte();
            String l_Query
                = "INSERT INTO          Cortes "
                + "                     (IdCorte, IdUsuario, FechaHora, Enviado) "
                + "VALUES               (@IdCorte, @IdUsuario, @FechaHora, 0) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdCorte", p_IdCorte);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();
                    return true;
                }
            }
        }

    
        private static int
            FolioCorte()
        {
            String l_Query
                = "SELECT MAX           (IdCorte) "
                + "FROM                 Cortes ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                }
            }
            l_Folio++;
            return l_Folio;
        }
    }
}
