﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarDatosOperadoresAgregar : Form
    {
        public enum Modo { Agregar, Editar };

        Modo m_Modo;

        string ClientID;
        public FormEditarDatosOperadoresAgregar( string text )
        {
            InitializeComponent();
            ClientID = text;
   
        }

        public FormEditarDatosOperadoresAgregar()
        {
            InitializeComponent();
           
        }

       /* public static DialogResult EjecutarAgregar( out String p_Clave )
        {
            using ( FormEditarDatosOperadoresAgregar l_Forma = new FormEditarDatosOperadoresAgregar() )
            {
                l_Forma.m_Modo = Modo.Agregar;
                DialogResult l_Resultado = l_Forma.ShowDialog();
                p_Clave = l_Forma.c_Clave.Text;

                return l_Resultado;
            }
        }


        public static DialogResult EjecutarEditar( String p_Clave )
        {
            using ( FormEditarDatosGeneralesCliente l_Forma = new FormEditarDatosGeneralesCliente() )
            {
                l_Forma.m_Modo = Modo.Editar;
                if ( l_Forma.CargaUsuario( p_Clave ) )
                {
                    l_Forma.c_Clave.ReadOnly = true;
                    return l_Forma.ShowDialog();
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }

        public bool CargaUsuario( String p_Clave )
        {
            String l_Nombre, l_Telefono;

            if ( BDCliente.TraerCliente( p_Clave, out l_Nombre, out l_Telefono ) )
            {
                c_Clave.Text = p_Clave;
                c_Nombre.Text = l_Nombre;
                c_Telefono.Text = l_Telefono;
                return true;
            }

            return false;
        }*/

        private void c_BAceptar_Click( object sender, EventArgs e )
        {
            if ( m_Modo == Modo.Agregar )

            {
               /* bool l_PasswordFlag = false;
                l_PasswordFlag = IsValidPassword( c_Pass.Text );
                if ( l_PasswordFlag == false)
                {
                    MessageBox.Show( "Por favor, introduzca una contraseña segura" );
                    return;
                }*/

                if ( c_PassConf.Text == "" || c_Pass.Text == "" )
                {
                    MessageBox.Show( "Por favor, introduzca los valores" );
                    return;
                }
                if ( c_PassConf.Text != c_Pass.Text )
                {
                    MessageBox.Show( "confirmar la contraseña no coincide con la contraseña nueva " );
                    c_PassConf.Focus();
                    return;
                }
                    BDOperador.Insertar1( c_IdUsario.Text, c_Nombre.Text, c_Refer.Text,c_Pass.Text,c_Status.Checked, ClientID );
                DialogResult = DialogResult.OK;
            }
            else
            {
                // TODO: Hacer
            }
        }


        public static bool IsValidPassword( string input )
        {
            Match match = Regex.Match( input, @"(?=^.{5,40}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*" );

            if ( match.Success && match.Index == 0 && match.Length == input.Length )
                return true;
            else
                return false;

        }
        int l_idTeclado = 0;
        private void pictureBox1_Click( object sender, EventArgs e )
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_IdUsario_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_IdUsario.Text = l_teclado.Captura;

            }
        }

        private void c_Nombre_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Nombre.Text = l_teclado.Captura;

            }
        }

        private void c_Refer_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Refer.Text = l_teclado.Captura;

            }
        }

        private void c_Pass_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Pass.Text = l_teclado.Captura;

            }
        }

     

        private void c_Cancelar_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        private void c_PassConf_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_PassConf.Text = l_teclado.Captura;

            }
        }
    }
}
