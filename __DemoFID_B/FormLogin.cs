﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace __DemoFID_B
{
    public partial class FormLogin : Form
    {
        int Id_Teclado = 0;



        public FormLogin()
        {
            InitializeComponent();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            c_BotonAceptar.Focus();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            string windir = Environment.GetEnvironmentVariable("WINDIR");
            string osk = null;

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "sysnative"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "system32"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = "osk.exe";
            }


            Id_Teclado = System.Diagnostics.Process.Start(osk).Id;
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
           
            Close();
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            if (Id_Teclado != 0)
                System.Diagnostics.Process.GetProcessById(Id_Teclado).Kill();

            if (C_Usuario.Text.Length > 0 && c_Contraseña.Text.Length > 0)
            {
                if (c_Contraseña.Text.Length == 6)

                    if (!Globales.AutenticarUsuario(C_Usuario.Text.ToLower(), c_Contraseña.Text))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                            f_Error.ShowDialog();
                        }

                    }
                    else
                    {
                        using (FormOpciones f_Opciones = new FormOpciones())
                        {
                            f_Opciones.ShowDialog();
                        }

                    }

            }

        }
    }
}
