﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarDatosGenerales : Form
    {
   

        public FormEditarDatosGenerales()
        {
            InitializeComponent();
           
         }

        public FormEditarDatosGenerales( string p_clave, string p_Nombre, string p_Telefono )
        {
            InitializeComponent();
            using ( FormEditarDatosGenerales form1 = new FormEditarDatosGenerales() )
            {
                form1.c_Clave.Text = p_clave;
                form1.c_Nombre.Text = p_Nombre;
                form1.c_Telephone.Text = p_Telefono;
                form1.ShowDialog();
            }
        }
        private void c_BAceptar_Click( object sender, EventArgs e )
        {


            String l_Query
                     = "UPDATE           Cliente "
                     + "SET           Nombre    = @Nombre "
                     + ",           Telefono    = @Telefono "
                     + "WHERE          ClaveCliente   = @clientid ";

            using ( SqlConnection l_Conexion = UtilsBD.NuevaConexion() )
            {
                SqlCommand l_Comando = new SqlCommand( l_Query, l_Conexion );

                l_Comando.Parameters.AddWithValue( "@Nombre", c_Nombre.Text );
                l_Comando.Parameters.AddWithValue( "@Telefono", c_Telephone.Text );
                l_Comando.Parameters.AddWithValue( "@clientid", c_Clave.Text );

                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch ( Exception E ) { throw; }

                DialogResult = DialogResult.OK;


            }
        }
        int l_idTeclado = 0;
        private void pictureBox1_Click( object sender, EventArgs e )
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_Nombre_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {
                           
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Nombre.Text = l_teclado.Captura;

            }
        }

        private void c_Telephone_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Telephone.Text = l_teclado.Captura;

            }
        }

        private void c_Cancelar_Click( object sender, EventArgs e )
        {
            this.Close();
        }
    }
}
