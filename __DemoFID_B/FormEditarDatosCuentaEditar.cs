﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarDatosCuentaEditar : Form
    {
        public enum Modo { Agregar, Editar };

        Modo m_Modo;

        
        public FormEditarDatosCuentaEditar( string text )
        {
            string ClientID;
            InitializeComponent();
            ClientID = text;



            string l_Alias, l_Cuenta, l_AceptaReferencias, l_Banco, l_IdCuenta, l_status;

            using ( FormEditarDatosCuentaEditar l_Forma = new FormEditarDatosCuentaEditar() )
            {
                if ( BDCuentaOperador.SelcOperador( ClientID, out l_Alias, out l_Cuenta, out l_AceptaReferencias, out l_status, out l_Banco ,out l_IdCuenta ) )
                {
                    l_Forma.c_Alias.Text = l_Alias;
                    l_Forma.c_Cuenta.Text = l_Cuenta;
                    //l_Forma.c_AcptRef.Text = l_AceptaReferencias;
                    //l_Forma.c_Status.Text = l_status;
                    l_Forma.c_Banco.Text = l_Banco;
                    l_Forma.c_IdCuenta.Text = l_IdCuenta;

                    if (l_AceptaReferencias == "True")
                        l_Forma.c_AcptRef.Checked = true;
                    else
                        l_Forma.c_AcptRef.Checked = false;
                    if (l_status == "True")
                    {
                        l_Forma.c_Status.Checked = true;
                    }
                    else
                    {
                        l_Forma.c_Status.Checked = false;
                    }

                    l_Forma.ShowDialog();

                }
            }
        }

        public FormEditarDatosCuentaEditar()
        {
            InitializeComponent();

        }

       /* public static DialogResult EjecutarAgregar( out String p_Clave )
        {
            using ( FormEditarDatosOperadoresAgregar l_Forma = new FormEditarDatosOperadoresAgregar() )
            {
                l_Forma.m_Modo = Modo.Agregar;
                DialogResult l_Resultado = l_Forma.ShowDialog();
                p_Clave = l_Forma.c_Clave.Text;

                return l_Resultado;
            }
        }


        public static DialogResult EjecutarEditar( String p_Clave )
        {
            using ( FormEditarDatosGeneralesCliente l_Forma = new FormEditarDatosGeneralesCliente() )
            {
                l_Forma.m_Modo = Modo.Editar;
                if ( l_Forma.CargaUsuario( p_Clave ) )
                {
                    l_Forma.c_Clave.ReadOnly = true;
                    return l_Forma.ShowDialog();
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }

        public bool CargaUsuario( String p_Clave )
        {
            String l_Nombre, l_Telefono;

            if ( BDCliente.TraerCliente( p_Clave, out l_Nombre, out l_Telefono ) )
            {
                c_Clave.Text = p_Clave;
                c_Nombre.Text = l_Nombre;
                c_Telefono.Text = l_Telefono;
                return true;
            }

            return false;
        }*/

        private void c_BAceptar_Click( object sender, EventArgs e )
        {
            if ( m_Modo == Modo.Agregar )
            {
                
                BDCuentaOperador.Update1( c_Alias.Text,c_Banco.Text,c_Cuenta.Text, c_AcptRef.Checked,c_Status.Checked , c_IdCuenta.Text);
                DialogResult = DialogResult.OK;
            }
            else
            {
                // TODO: Hacer
            }
        }
        int l_idTeclado = 0;
        private void pictureBox1_Click( object sender, EventArgs e )
        {
           
            l_idTeclado = Globales.LlamarTeclado();
        

    }

        private void c_Alias_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Alias.Text = l_teclado.Captura;

            }
        }

        private void c_Banco_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Banco.Text = l_teclado.Captura;

            }
        }

        private void c_Cuenta_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Cuenta.Text = l_teclado.Captura;

            }
        }

        private void c_Cancelar_Click( object sender, EventArgs e )
        {
            this.Close();
        }
    }
}
