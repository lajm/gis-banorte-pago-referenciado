﻿namespace __DemoFID_B
{
    partial class FormAbcClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_BSalir = new System.Windows.Forms.Button();
            this.c_BEliminar = new System.Windows.Forms.Button();
            this.c_BEditar = new System.Windows.Forms.Button();
            this.c_BAgregar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.c_Datos = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Datos)).BeginInit();
            this.SuspendLayout();
            // 
            // c_BSalir
            // 
            this.c_BSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BSalir.Location = new System.Drawing.Point(682, 375);
            this.c_BSalir.Margin = new System.Windows.Forms.Padding(2);
            this.c_BSalir.Name = "c_BSalir";
            this.c_BSalir.Size = new System.Drawing.Size(82, 38);
            this.c_BSalir.TabIndex = 21;
            this.c_BSalir.Text = "Salir";
            this.c_BSalir.UseVisualStyleBackColor = true;
            this.c_BSalir.Click += new System.EventHandler(this.c_BSalir_Click);
            // 
            // c_BEliminar
            // 
            this.c_BEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BEliminar.Location = new System.Drawing.Point(681, 300);
            this.c_BEliminar.Margin = new System.Windows.Forms.Padding(2);
            this.c_BEliminar.Name = "c_BEliminar";
            this.c_BEliminar.Size = new System.Drawing.Size(82, 38);
            this.c_BEliminar.TabIndex = 20;
            this.c_BEliminar.Text = "Eliminar";
            this.c_BEliminar.UseVisualStyleBackColor = true;
            this.c_BEliminar.Click += new System.EventHandler(this.c_BEliminar_Click);
            // 
            // c_BEditar
            // 
            this.c_BEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BEditar.Location = new System.Drawing.Point(682, 241);
            this.c_BEditar.Margin = new System.Windows.Forms.Padding(2);
            this.c_BEditar.Name = "c_BEditar";
            this.c_BEditar.Size = new System.Drawing.Size(82, 38);
            this.c_BEditar.TabIndex = 19;
            this.c_BEditar.Text = "Editar";
            this.c_BEditar.UseVisualStyleBackColor = true;
            this.c_BEditar.Click += new System.EventHandler(this.c_BEditar_Click);
            // 
            // c_BAgregar
            // 
            this.c_BAgregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BAgregar.Location = new System.Drawing.Point(682, 187);
            this.c_BAgregar.Margin = new System.Windows.Forms.Padding(2);
            this.c_BAgregar.Name = "c_BAgregar";
            this.c_BAgregar.Size = new System.Drawing.Size(82, 38);
            this.c_BAgregar.TabIndex = 18;
            this.c_BAgregar.Text = "Agregar";
            this.c_BAgregar.UseVisualStyleBackColor = true;
            this.c_BAgregar.Click += new System.EventHandler(this.c_BAgregar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.c_Datos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 102);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(656, 397);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Clientes";
            // 
            // c_Datos
            // 
            this.c_Datos.AllowUserToAddRows = false;
            this.c_Datos.AllowUserToDeleteRows = false;
            this.c_Datos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_Datos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c_Datos.Location = new System.Drawing.Point(2, 17);
            this.c_Datos.Name = "c_Datos";
            this.c_Datos.ReadOnly = true;
            this.c_Datos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.c_Datos.Size = new System.Drawing.Size(652, 378);
            this.c_Datos.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(318, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "MANTENIMIENTO DE AGENDA DE CLIENTE";
            // 
            // FormAbcClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.c_BSalir);
            this.Controls.Add(this.c_BEliminar);
            this.Controls.Add(this.c_BEditar);
            this.Controls.Add(this.c_BAgregar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAbcClientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAbcClientes";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c_Datos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_BSalir;
        private System.Windows.Forms.Button c_BEliminar;
        private System.Windows.Forms.Button c_BEditar;
        private System.Windows.Forms.Button c_BAgregar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView c_Datos;
        private System.Windows.Forms.Label label4;
    }
}