﻿/******************************************************************************
 * 
 * COMPATIBLE CHECK READER PROJECT
 * 
 * PROGRAMMED BY: LEONARDO PAGÉS TUÑÓN
 * 
 * COPYRIGHT(c) 2011: SYMETRY S.A. DE C.V
 * DERECHOS DE AUTOR 2011: SYMETRY S.A. DE C.V.
 * 
 * PROHIBIDO QUITAR LOS COMENTARIOS DE ESTA SECCIÓN
 * PROHIBIDO MODIFICAR LOS NOMBRES DE COMPONENTES
 * PROHIBIDA SU COMERCIALIZACIÓN
 * 
 *****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;

namespace __DemoFID_B
{
    class HCR360
    {
        #region CONSTANTES

        // COM port codes
        public const short HCR360_COM1 = 0;
        public const short HCR360_COM2 = 1;
        public const short HCR360_COM3 = 2;
        public const short HCR360_COM4 = 3;
        public const short HCR360_COM5 = 4;
        public const short HCR360_COM6 = 5;
        public const short HCR360_COM7 = 6;
        public const short HCR360_COM8 = 7;
        public const short HCR360_COM9 = 8;
        public const short HCR360_COM10 = 9;
        public const short HCR360_COM11 = 10;
        public const short HCR360_COM12 = 11;
        public const short HCR360_COM13 = 12;
        public const short HCR360_COM14 = 13;
        public const short HCR360_COM15 = 14;
        public const short HCR360_COM16 = 15;


        // Parity Codes
        public const short HCR360_NoParity = 0;
        public const short HCR360_OddParity = 1;
        public const short HCR360_EvenParity = 2;
        public const short HCR360_MarkParity = 3;
        public const short HCR360_SpaceParity = 4;


        // Stop Bit Codes
        public const short HCR360_OneStopBit = 0;
        public const short HCR360_TwoStopBits = 2;

        // Word Length Codes
        public const short HCR360_WordLength5 = 5;
        public const short HCR360_WordLength6 = 6;
        public const short HCR360_WordLength7 = 7;
        public const short HCR360_WordLength8 = 8;

        // baud codes
        public const short HCR360_BAUD1200 = 2;
        public const short HCR360_BAUD2400 = 3;
        public const short HCR360_BAUD4800 = 4;
        public const short HCR360_BAUD9600 = 5;
        public const short HCR360_BAUD19200 = 6;
        public const short HCR360_BAUD38400 = 7;  //'(*)
        public const short HCR360_BAUD57600 = 8;
        public const short HCR360_BAUD115200 = 9;

        // Rem Define IC Card VCC
        public const Byte V1_8 = 1;
        public const Byte V3 = 3;
        public const Byte V5 = 5;

        #endregion

        [DllImport("HCR360.dll")]
        //[DllImportAttribute("HCR360.dll", EntryPoint = "OpenCom", CallingConvention = CallingConvention.StdCall)]
        public static extern int OpenCom(int port, int baud, int Databits);

        [DllImport("HCR360.dll")]
        public static extern void CloseCom(int port);

        // Usi2
        [DllImport("HCR360.dll")]
        public static extern void usi2SendData(long port, Byte cmd, int DataLen, Byte DataBuf);

        [DllImport("HCR360.dll")]
        public static extern int usi2ReceiveData(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern int usi2Exchange(long port, Byte cmd, int SndDataLen, Byte SndData, Byte RcvData);

        [DllImport("HCR360.dll")]
        //[DllImportAttribute("HCR360.dll", EntryPoint = "usi2CheckCardPresence", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2CheckCardPresence(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2CardMoveNotify", CallingConvention = CallingConvention.StdCall)]
        public static extern long usi2CardMoveNotify(int port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2Retransmit(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2ReaderVerReport(long port, Byte SubCmd, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2SwitchStatus(long port);

        [DllImport("HCR360.dll")]
        public static extern long usi2SetVerboseMode(long port);

        [DllImport("HCR360.dll")]
        //[DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReaderStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReaderStatus(int port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2SetGreenLed(long port, Byte LedStatus);

        [DllImport("HCR360.dll")]
        public static extern long usi2SetRedLed(long port, Byte LedStatus);

        [DllImport("HCR360.dll")]
        public static extern long usi2Latch(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2Unlatch(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2ConfigReport(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2WarmReset(long port);


        // Reader and CPU card
        // IC card
        [DllImport("HCR360.dll")]
        public static extern long usi2CardPowerUp(long port, Byte Vcc, Byte EmvComply, Byte Atr);

        [DllImport("HCR360.dll")]
        public static extern long usi2CardPowerOff(long port);

        [DllImport("HCR360.dll")]
        public static extern long usi2SendApdu(long port, int ApduLen, Byte ApduCmd, Byte RcvData, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2SelectIccConnector(long port, Byte IccConnector);

        // Memory Card
        [DllImport("HCR360.dll")]
        public static extern long usi2SelectMemCardType(long port, Byte MemCardType, Byte PageSize);

        [DllImport("HCR360.dll")]
        public static extern long usi2SendMemCardApu(long port, int ApduLen, Byte ApduCmd, Byte RcvData, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2ReportMemCardType(long port);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryVerify(long port, int Nums, Byte Password, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryAuthenticate(long port, int Nums, Byte DataBuf, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryExAuthenticate(long port, int Nums, Byte DataBuf, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryReadData(long port, int Addr, int Nums, Byte RcvData, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryWriteData(long port, int Addr, int Nums, Byte DataBuf, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryEraseData(long port, int Addr, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2RestoreData(long port, int Addr, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryWriteProtect(long port, int Addr, int Nums, Byte DataBuf, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemroyReadProtect(long port, int Addr, int Nums, Byte RcvData, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2MemoryEraseUserArea(long port, int Nums, Byte ErsPSC, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2ChangePSC(long port, int Nums, Byte NewPsc, Byte SW);

        [DllImport("HCR360.dll")]
        public static extern long usi2ReadErrCount(long port, Byte ErrCounte, Byte SW);

        // Mag card cm
        [DllImport("HCR360.dll")]
        public static extern long usi2ArmtoRead(long port);

        [DllImport("HCR360.dll")]
        public static extern long usi2Abort(long port);

        [DllImport("HCR360.dll")]
        public static extern long usi2ReadMagData(long port, Byte selectTK, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2TK1Data(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2TK2Data(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2TK3Data(long port, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2ISOData(long port, Byte Track, Byte viaISO, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2FowardCusData(long port, Byte Track, Byte viaNull, Byte chrNums, Byte RcvData);

        [DllImport("HCR360.dll")]
        public static extern long usi2ReverseCusData(long port, Byte Track, Byte viaNull, Byte chrNums, Byte RcvData);

        // TLP224 Turbo Protocol
        [DllImport("HCR360.dll")]
        public static extern void TLP224Turbo_SendData(long port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImport("HCR360.dll")]
        public static extern long TLP224Turbo_ReceiveData(long port, Byte RcvData, long lDelayTime);

        [DllImport("HCR360.dll")]
        public static extern long TLP224Turbo_Exchange(long port, Byte Cmd, int SndDataLen, Byte RdvData, long lDelayTime);

        // TLP334 Protocol
        [DllImport("HCR360.dll")]
        public static extern void TLP224_SendData(long port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImport("HCR360.dll")]
        public static extern long TLP224_ReceiveData(long port, Byte RcvData, long lDelayTime);

        [DllImport("HCR360.dll")]
        public static extern long TLP224_Exchange(long port, Byte Cmd, int SndDataLen, Byte SndData, Byte RcvData, long lDelayTime);

        // USI1 Protocol
        [DllImport("HCR360.dll")]
        public static extern void USI1_SendData(long port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImport("HCR360.dll")]
        public static extern long USI1_ReceiveData(long port, Byte RcvData, long lDelayTime);

        [DllImport("HCR360.dll")]
        public static extern long USI1(long port, Byte Cmd, int SndDataLen, Byte SndData, Byte RcvData, long lDelayTime);

        //---------------------------------------------------------------
        // Genernal Purpose Functions
        //---------------------------------------------------------------
        // '1'2'3'4'5' -> "3132333435"
        public static String
            BinToHexStr(Byte[] baInPut, int nLen)
        {
            String Buf = "";
            Byte Value;

            for (int i = 0; i < (nLen - 1); i++)
            {
                Value = baInPut[i];
                if (Value <= 15)
                    Buf = Buf + "0" + Value.ToString("X");
                else
                    Buf = Buf + Value.ToString("X");
            }
            return Buf;
        }

        // "3132333435"-> '1'2'3'4'5'
        public static int
            HexStrToBin(String strInput, Byte[] baOutPut)
        {
            int nStrLen;
            String Buf = "";
            int j = 0;

            nStrLen = strInput.Length;
            for (int i = 0; i < nStrLen; i = i + 2)
            {
                try
                {
                    baOutPut[j] = Byte.Parse(strInput.Substring(i, 2));
                }
                catch (Exception ex)
                {
                }
                j++;
            }
            return j;
        }

        // '1'2'3'4'5' -> "12345"
        public static String
            BinAryToAsciiStr(Byte[] baInPut, int nLen)
        {
            Byte Value = new Byte();
            String Buf = "";
            for (int i = 0; i < (nLen - 1); i++)
            {
                Value = baInPut[i];
                if (Value <= 15)
                    Buf += "<0" + Value.ToString("X") + ">";
                else if (Value <= 31 || Value >= 127)
                    Buf += "<" + Value.ToString("X") + ">";
                else
                    Buf += Encoding.Unicode.GetString(baInPut, i, Value.ToString().Length);
            }
            return Buf;
        }

        //---------------------------------------------------------------
        // communication
        //---------------------------------------------------------------
        public static int c_AbrirCom(int port, int baud)
        {
            return OpenCom(port, baud, 0);
        }

        public static void c_CerrarCom(int port)
        {
            CloseCom(port);
        }
        //**************************************************************************
        //
        // Usi2 functions for VB
        //
        //**************************************************************************
        //---------------------------------------------------------------
        //Usi2 Send data, receive data, exchange data
        //---------------------------------------------------------------

        public static long c_EnviarData(long port, String strCmd, String strDataBuf)
        {
            Byte bCmd;
            Byte[] baSend;
            int DataLen;
            long Rtn;

            bCmd = Byte.Parse(strCmd);
            //DataLen = HexStrToBin(strDataBuf, baSend);
            //usi
                return 214334234;
        }
    }
}
