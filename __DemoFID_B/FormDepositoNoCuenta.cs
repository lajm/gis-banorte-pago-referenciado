﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormDepositoNoCuenta : Form
    {
        String l_NumeroCuenta = "";
        public FormDepositoNoCuenta()
        {
            InitializeComponent();
            
        }

        private void OprimirTecla(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
                l_NumeroCuenta += "0";
            else if (sender.Equals(c_Boton1))
                l_NumeroCuenta += "1";
            else if (sender.Equals(c_Boton2))
                l_NumeroCuenta += "2";
            else if (sender.Equals(c_Boton3))
                l_NumeroCuenta += "3";
            else if (sender.Equals(c_Boton4))
                l_NumeroCuenta += "4";
            else if (sender.Equals(c_Boton5))
                l_NumeroCuenta += "5";
            else if (sender.Equals(c_Boton6))
                l_NumeroCuenta += "6";
            else if (sender.Equals(c_Boton7))
                l_NumeroCuenta += "7";
            else if (sender.Equals(c_Boton8))
                l_NumeroCuenta += "8";
            else if (sender.Equals(c_Boton9))
                l_NumeroCuenta += "9";
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_NumeroCuenta.Length > 0)
                {
                    c_NoCuenta.Text = c_NoCuenta.Text.Substring(0, c_NoCuenta.Text.Length - 1);
                    l_NumeroCuenta = l_NumeroCuenta.Substring(0, l_NumeroCuenta.Length - 1);
                    return;
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                int l_IdMoneda;
                String l_Alias;
                if (!BDCuentas.ValidarNoCuenta(c_NoCuenta.Text, out l_Alias, out l_IdMoneda))
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Cuenta no valida";
                        f_Error.ShowDialog();
                    }
                    Close();
                }
                else
                {
                    // Mandar a abrir ventana depósitos
                    using (FormDepositoC f_Deposito = new FormDepositoC())
                    {
                        Globales.EmpresaCliente = c_NoCuenta.Text;
                        Globales.AliasCuenta = l_Alias;
                        f_Deposito.l_IdMoneda = l_IdMoneda;
                     //   f_Deposito.c_EtiquetaNumeroCuenta.Text = l_Alias;
                        if (l_IdMoneda == 1)
                            f_Deposito.c_EtiquetaMoneda.Text = "Pesos";
                        else
                            f_Deposito.c_EtiquetaMoneda.Text = "Dólares";
                        f_Deposito.ShowDialog();
                    }
                    Globales.EmpresaCliente = null;
                    Globales.AliasCuenta = null;
                    Close();
                    return;
                }
            }
            c_NoCuenta.Text = l_NumeroCuenta;
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();
            //for(int i=0;i<l_NumeroCuenta.Length;i++)
        }

    }
}
