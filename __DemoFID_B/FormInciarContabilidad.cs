﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormInciarContabilidad : Form
    {
        String l_Password = "";
        public String l_IdUsuario;

        public FormInciarContabilidad()
        {
            InitializeComponent();
           
        }

        private void OprimirTecla(object sender, EventArgs e)
        {


            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Password += "0";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Password += "1";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Password += "2";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Password += "3";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Password += "4";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Password += "5";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Password += "6";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Password += "7";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Password += "8";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Password += "9";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Password.Length != 0)
                {
                    l_Password = l_Password.Substring(0, l_Password.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Password.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                if (l_Password.Length > 1)
                {
                    if (!Globales.AutenticarUsuario(l_IdUsuario,l_Password))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                            f_Error.ShowDialog();
                        }
                        Close();
                    }
                    else
                    {
                        Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO",1);

                        this.Visible = false;

                       
                        using (FormError f_Mensaje = new FormError(false))
                        {
                            f_Mensaje.c_Imagen.Visible = false;
                            f_Mensaje.c_MensajeError.Text =
                                        " SE GENERARA UNA ALERTA PARA EL RESET DE CONTABILIDAD ¿Desea Continuar?";
                            DialogResult l_respuesta = f_Mensaje.ShowDialog();

                            if (l_respuesta == DialogResult.OK)
                            {
                                using (FormError f_MensajeOK = new FormError(false))
                                {
                                    f_MensajeOK.c_Imagen.Visible = false;
                                    f_MensajeOK.c_MensajeError.Text =
                                                " CONFIRME EL RESET DE CONTABILIDAD ¡No podra deshacer esta Operación!....";
                                    DialogResult l_respuestaOK = f_MensajeOK.ShowDialog();

                                    if (l_respuestaOK == DialogResult.OK)
                                    {

                                        BDRetiro.ActualizarRetiros("Equipo1");
                                        using (FormError f_OK = new FormError(true))
                                        {
                                            f_OK.c_Imagen.Visible = true;
                                            f_OK.c_MensajeError.Text = "Contabilidad en 0s";
                                            f_OK.ShowDialog();
                                        }
                                    }
                                    Globales.EscribirBitacora("OPCION ADMINISTRADOR", "Confirmación RESET CONTABILIDAD", l_respuestaOK.ToString(), 1);
                                }
                            }

                            Globales.EscribirBitacora("OPCION ADMINISTRADOR", "RESET CONTABILIDAD", l_respuesta.ToString(), 1);

                        }

                        Close();
                    }
                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir una contraseña";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
            //for (int i = 0; i < l_Password.Length; i++)
        }

        private void FormIniciarSesion_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
        }
    }
}
