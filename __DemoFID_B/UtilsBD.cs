﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace __DemoFID_B
{
    public class UtilsBD
    {
        private static String CadenaConexion
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["CadenaConexion"]; }
        }
        private static String CadenaConexionSIDE
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["CadenaConexionSIDE"]; }
        }

          public static SqlConnection
            NuevaConexion()
        {
            SqlConnection l_Conexion;
            try
            {
                l_Conexion = new SqlConnection(CadenaConexion);
                l_Conexion.Open();
            }
            catch (Exception ex)
            {
                return null;
            }

            return l_Conexion;
        }
        
         public static SqlConnection
            NuevaConexionSIDE()
        {
            SqlConnection l_Conexion;
            try
            {
                l_Conexion = new SqlConnection(CadenaConexionSIDE );
        
                l_Conexion.Open();
            }
            catch (Exception ex)
            {
                return null;
            }

            return l_Conexion;
        }
    }
}
