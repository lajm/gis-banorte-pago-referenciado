﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Irds;
using System.Reflection;
using System.Threading;

namespace __DemoFID_B
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (FirstInstance)
            {
                Conexion.PonerCadenaConexion(System.Configuration.ConfigurationSettings.AppSettings["CadenaConexion"]);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormMenu());
            }
        }

        private static bool FirstInstance
        {
            get
            {
                bool created;
                string name = Assembly.GetEntryAssembly().FullName;
                // created will be True if the current thread creates and owns the mutex.
                // Otherwise created will be False if a previous instance already exists.
                Mutex mutex = new Mutex(true, name, out created);
                return created;
            }
        }
    }



}
