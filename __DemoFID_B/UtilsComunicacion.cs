﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using IrdsMensajes;
using System.Configuration;

namespace __DemoFID_B
{
    class UtilsComunicacion
    {
        #region Encripcion/Desencripcion

        static string accumulated_output_info = "";
        static bool trace_output = false;

        private static void accumulate_output(string str)
        {
            if (trace_output)
                accumulated_output_info += str + System.Environment.NewLine;
        }

        private static void accumulate_bitstring(string label, int[] ary, int spacing)
        {
            if (trace_output)
            {
                accumulated_output_info += label;

                // add bits
                for (int i = 1; i < ary.Length; i++)
                {
                    if ((i % spacing) == 1)
                        accumulated_output_info += " ";	// time to add a space
                    accumulated_output_info += ary[i].ToString();	// and the bit
                }

                // insert trailing end-of-line
                accumulated_output_info += System.Environment.NewLine;
            }
        }

        // special value stored in x[0] to indicate a problem
        static int ERROR_VAL = -9876;

        // initial permutation (split into left/right halves )
        // since DES numbers bits starting at 1, we will ignore x[0]
        static int[] IP_perm = {-1,
	        58, 50, 42, 34, 26, 18, 10, 2,
	        60, 52, 44, 36, 28, 20, 12, 4,
	        62, 54, 46, 38, 30, 22, 14, 6,
	        64, 56, 48, 40, 32, 24, 16, 8,
	        57, 49, 41, 33, 25, 17, 9, 1,
	        59, 51, 43, 35, 27, 19, 11, 3,
	        61, 53, 45, 37, 29, 21, 13, 5,
	        63, 55, 47, 39, 31, 23, 15, 7};

        // final permutation (inverse initial permutation)
        static int[] FP_perm = {-1,
	        40, 8, 48, 16, 56, 24, 64, 32,
	        39, 7, 47, 15, 55, 23, 63, 31,
	        38, 6, 46, 14, 54, 22, 62, 30,
	        37, 5, 45, 13, 53, 21, 61, 29,
	        36, 4, 44, 12, 52, 20, 60, 28,
	        35, 3, 43, 11, 51, 19, 59, 27,
	        34, 2, 42, 10, 50, 18, 58, 26,
	        33, 1, 41, 9, 49, 17, 57, 25};

        // per-round expansion
        static int[] E_perm = {-1,
	        32, 1, 2, 3, 4, 5,
	        4, 5, 6, 7, 8, 9,
	        8, 9, 10, 11, 12, 13,
	        12, 13, 14, 15, 16, 17,
	        16, 17, 18, 19, 20, 21,
	        20, 21, 22, 23, 24, 25,
	        24, 25, 26, 27, 28, 29,
	        28, 29, 30, 31, 32, 1};

        // per-round permutation
        static int[] P_perm = {-1,
	        16, 7, 20, 21, 29, 12, 28, 17,
	        1, 15, 23, 26, 5, 18, 31, 10,
	        2, 8, 24, 14, 32, 27, 3, 9,
	        19, 13, 30, 6, 22, 11, 4, 25};

        // note we do use element 0 in the S-Boxes
        static int[] S1 = {
	        14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
	        0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
	        4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
	        15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13};
        static int[] S2 = {
	        15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
	        3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
	        0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
	        13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9};
        static int[] S3 = {
	        10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
	        13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
	        13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
	        1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12};
        static int[] S4 = {
	        7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
	        13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
	        10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
	        3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14};
        static int[] S5 = {
	        2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
	        14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
	        4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
	        11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3};
        static int[] S6 = {
	        12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
	        10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
	        9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
	        4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13};
        static int[] S7 = {
	        4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
	        13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
	        1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
	        6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12};
        static int[] S8 = {
	        13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
	        1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
	        7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
	        2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11};

        //, first, key, permutation
        static int[] PC_1_perm = {-1, 
	        // C subkey bits
	        57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18,
	        10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36,
	        // D subkey bits
	        63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22,
	        14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4};

        //, per-round, key, selection, permutation
        static int[] PC_2_perm = {-1, 
	        14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10,
	        23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2,
	        41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48,
	        44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32};

        // save output in case we want to reformat it later
        static int[] DES_output = new int[65];
        
        // remove spaces from input
        private static string remove_spaces(string instr)
        {
            string outstr = "";
           if(instr != null)
                for (int i = 0; i < instr.Length; i++)
                    if (instr[i] != ' ')
                        // not a space, include it
                        outstr += instr[i];
            
            return outstr;
        }

        // split an integer into bits
        // ary   = array to store bits in
        // start = starting subscript
        // bitc  = number of bits to convert
        // val   = number to convert
        private static void split_int(ref int[] ary, int start, int bitc, int val)
        {
            int i = start;
            for(int j = bitc - 1; j >= 0; j--)
            {
                // isolate low-order bit
                ary[i + j] = (val & 1);
                // remove that bit
                val >>= 1;
            }
        }
        
        // get the message to encrypt/decrypt
        private static void get_value(ref int[] bitarray, string str, bool isASCII)
        {
            int val; // one hex digit

            // insert note we probably are ok
            bitarray[0] = -1;

            if (isASCII)
            {
                // check length of data
                if (str.Length != 8)
                {
                    //MessageBox.Show("Message and key must be 64 bits (8 ASCII characters)");
                    bitarray[0] = ERROR_VAL;
                    return;
                }

                // have ASCII data
                for (int i = 0; i < 8; i++)
                {
                    split_int(ref bitarray, i * 8 + 1, 8, (int)str[i]);
                }
            }
            else
            {
                // have hex data - remove any spaces they used, then convert
                str = remove_spaces(str);

                // check length of data
                if (str.Length != 16)
                {
                    //MessageBox.Show("Message and key must be 64 bits (16 hex digits)");
                    bitarray[0] = ERROR_VAL;
                    return;
                }

                for (int i = 0; i < 16; i++)
                {
                    // get the next hex digit
                    val = (int)str[i];

                    // do some error checking
                    if (val >= 48 && val <= 57)
                        // have a valid digit 0-9
                        val -= 48;
                    else if (val >= 65 && val <= 70)
                        // have a valid digit A-F
                        val -= 55;
                    else if (val >= 97 && val <= 102)
                        // have a valid digit A-F
                        val -= 87;
                    else
                    {
                        // not 0-9 or A-F, complain
                        //MessageBox.Show(str[i] + " is not a valid hex digit");
                        bitarray[0] = ERROR_VAL;
                        return;
                    }

                    // add this digit to the array
                    split_int(ref bitarray, i * 4 + 1, 4, val - 48);
                }
            }
        }
        
        // format the output in the desired form
        // if -1, use existing value
        // -- uses the global array DES_output
        private static string format_DES_output(bool IsAsciiOutput)
        {
            int bits;
            string str = "";

            // what type of data do we have to work with?
            if (IsAsciiOutput)
            {
                // convert each set of bits back to ASCII
                for (int i = 1; i <= 64; i += 8)
                {
                    str += (char)(
                        DES_output[i  ]*128 + DES_output[i+1]*64  +
                        DES_output[i+2]*32  + DES_output[i+3]*16  +
                        DES_output[i+4]*8   + DES_output[i+5]*4   +
                        DES_output[i+6]*2   + DES_output[i+7]);
                }
            }
            else 
            {
                // output hexdecimal data
                for (int i = 1; i <= 64; i += 4)
                {
                    bits = DES_output[i  ]*8   + DES_output[i+1]*4   +
                        DES_output[i+2]*2   + DES_output[i+3];

                    // 0-9 or A-F?
                    if ( bits <= 9 )
                        str += (char)( bits+48 );
                    else
                        str += (char)( bits+87 );
                }
            }
            // copy to textbox
            return str;
        }

        // copy bits in a permutation
        //   dest = where to copy the bits to
        //   src  = Where to copy the bits from
        //   perm = The order to copy/permute the bits
        // note: since DES ingores x[0], we do also
        private static void permute(ref int[] dest, int[] src, int[] perm)
        {
            int fromloc = 0;
            for (int i = 1; i < perm.Length; i++)
            {
                fromloc = perm[i];
                dest[i] = src[fromloc];
            }
        }

        // do an array XOR
        // assume all array entries are 0 or 1
        private static void xor(ref int[] a1, int[] a2)
        {
            for (int i = 1; i < a1.Length; i++)
                a1[i] = a1[i] ^ a2[i];
        }

        // process one S-Box, return integer from S-Box
        private static int do_S(int[] SBox, int index, int[] inbits)
        {
            // collect the 6 bits into a single integer
            int S_index = inbits[index  ]*32 + inbits[index+5]*16 +
                          inbits[index+1]*8  + inbits[index+2]*4 +
                          inbits[index+3]*2  + inbits[index+4];
            // do lookup
            return SBox[S_index];
        }

        // do one round of DES encryption
        private static void des_round(ref int[] L, ref int[] R, ref int[] KeyR)
        {
            int[] E_result = new int[49];
            int[] S_out = new int[33];

            // copy the existing L bits, then set new L = old R
            int[] temp_L = new int[33];
            for (int i = 0; i < 33; i++)
            {
                // copy exising L bits
                temp_L[i] = L[i];

                // set L = R
                L[i] = R[i];
            }

            // expand R using E permutation
            permute(ref E_result, R, E_perm);
            accumulate_bitstring("E   : ", E_result, 6);
            accumulate_bitstring("KS  : ", KeyR, 6);

            // exclusive-or with current key
            xor(ref E_result, KeyR);
            accumulate_bitstring("E xor KS: ", E_result, 6);

            // put through the S-Boxes
            split_int(ref S_out,  1, 4, do_S(S1,  1, E_result));
            split_int(ref S_out,  5, 4, do_S( S2,  7, E_result));
            split_int(ref S_out,  9, 4, do_S( S3, 13, E_result));
            split_int(ref S_out, 13, 4, do_S( S4, 19, E_result));
            split_int(ref S_out, 17, 4, do_S( S5, 25, E_result));
            split_int(ref S_out, 21, 4, do_S( S6, 31, E_result));
            split_int(ref S_out, 25, 4, do_S( S7, 37, E_result));
            split_int(ref S_out, 29, 4, do_S( S8, 43, E_result));
            accumulate_bitstring("Sbox: ", S_out, 4);

            // do the P permutation
            permute(ref R, S_out, P_perm);
            accumulate_bitstring("P   :", R, 8);

            // xor this with old L to get the new R
            xor(ref R, temp_L);
            accumulate_bitstring("L[i]:", L, 8);
            accumulate_bitstring("R[i]:", R, 8);
        }
        
        // shift the CD values left 1 bit
        private static void shift_CD_1(ref int[] CD)
        {
            // note we use [0] to hold the bit shifted around the end
            for (int i = 0; i <= 55; i++)
                CD[i] = CD[i+1];

            // shift D bit around end
            CD[56] = CD[28];
            // shift C bit around end
            CD[28] = CD[0];
        }
        
        // shift the CD values left 2 bits
        private static void shift_CD_2(ref int[] CD)
        {
            int C1 = CD[1];

            // note we use [0] to hold the bit shifted around the end
            for (int i = 0; i <= 54; i++)
                CD[i] = CD[i + 2];

            // shift D bits around end
            CD[55] = CD[27];
            CD[56] = CD[28];
            // shift C bits around end
            CD[27] = C1;
            CD[28] = CD[0];
        }

        // do the actual DES encryption/decryption
        private static int[] des_encrypt(int[] inData, int[] Key, bool do_encrypt)
        {
            int[] tempData = new int[65];   // output bits
            int[] CD = new int[57];         // halves of current key
            int[][] KS = new int[17][];     // per-round key schedules
            int[] L = new int[33];          // left half of current data
            int[] R = new int[33];          // right half of current data
            int[] result = new int[65];     // DES output

            // do the initial key permutation
            permute(ref CD, Key, PC_1_perm);
            accumulate_bitstring("CD[0]: ", CD, 7);

            // create the subkeys
            for (int i = 1; i <= 16; i++)
            {
                // create a new array for each round
                KS[i] = new int[49];

                // how much should we shift C and D?
                if ( i==1 || i==2 || i==9 || i == 16 )
                    shift_CD_1(ref CD);
                else
                    shift_CD_2(ref CD);
                accumulate_bitstring("CD["+i+"]: ", CD, 7);

                // create the actual subkey
                permute(ref KS[i], CD, PC_2_perm);
                accumulate_bitstring("KS["+i+"]: ", KS[i], 6);
            }

            // handle the initial permutation
            permute(ref tempData, inData, IP_perm);

            // split data into L/R parts
            for (int i = 1; i <= 32; i++)
            {
                L[i] = tempData[i];
                R[i] = tempData[i+32];
            }
            accumulate_bitstring("L[0]: ", L, 8);
            accumulate_bitstring("R[0]: ", R, 8);

            // encrypting or decrypting?
            if (do_encrypt)
            {
                // encrypting
                for (int i = 1; i <= 16; i++)
                {
                    accumulate_output("Round " + i);
                    des_round(ref L, ref R, ref KS[i]);
                }
            }
            else
            {
                // decrypting
                for (int i = 16; i >= 1; i--)
                {
                    accumulate_output("Round " + (17-i));
                    des_round(ref L, ref R, ref KS[i]);
                }
            }

            // create the 64-bit preoutput block = R16/L16
            for (int i = 1; i <= 32; i++)
            {
                // copy R bits into left half of block, L bits into right half
                tempData[i] = R[i];
                tempData[i+32] = L[i];
            }
            accumulate_bitstring("LR[16] ", tempData, 8);

            // do final permutation and return result
            permute(ref result, tempData, FP_perm);
            return result;
        }
        
        // do encrytion/decryption
        // do_encrypt is TRUE for encrypt, FALSE for decrypt
        private static string do_des(bool do_encrypt, string Message, bool IsAsciiInput,
            string Key1, bool IsAsciiOutput, ref string stTrace, bool TraceOutput)
        {
            int[] inData = new int[65];	// input message bits
            int[] Key = new int[65];

            accumulated_output_info = "";
            trace_output = TraceOutput;

            // get the message from the user
            // also check if it is ASCII or hex
            get_value(ref inData, Message, IsAsciiInput);

            // problems??
            if ( inData[0] == ERROR_VAL )
            {
                stTrace = accumulated_output_info;
                return "ERROR";
            }
            accumulate_bitstring( "Input bits:", inData, 8 );

            // get the key from the user
            get_value(ref Key, Key1, false);
            // problems??
            if (Key[0] == ERROR_VAL)
            {
                stTrace = accumulated_output_info;
                return "ERROR";
            }
            accumulate_bitstring( "Key bits:", Key, 8 );

            // do the encryption/decryption, put output in DES_output for display
            DES_output = des_encrypt(inData, Key, do_encrypt);

            accumulate_bitstring ("Output ", DES_output, 8 );

            // process output
            stTrace = accumulated_output_info;
            return format_DES_output(IsAsciiOutput);
        }
        
        // do Triple-DES encrytion/decryption
        // do_encrypt is TRUE for encrypt, FALSE for decrypt
        private static string do_tdes(bool do_encrypt, string Message, bool IsAsciiInput, 
            string Key1, string Key2, bool IsAsciiOutput, ref string stTrace, bool TraceOutput)
        {
            if ((do_encrypt) && (Message.Length < 8))
                Message = Message.PadRight(8, ' ');

            int[] inData = new int[65];     // input message bits
            int[] tempdata = new int[65];   // interm result bits
            int[] KeyA = new int[65];
            int[] KeyB = new int[65];

            accumulated_output_info = "";
            trace_output = TraceOutput;

            // get the message from the user
            // also check if it is ASCII or hex
            get_value(ref inData, Message, IsAsciiInput);

            // problems??
            if ( inData[0] == ERROR_VAL )
            {
                stTrace = accumulated_output_info;
                return "ERROR";
            }
            accumulate_bitstring( "Input bits:", inData, 8 );

            // get the key part A from the user
            get_value(ref KeyA, Key1, false);
            // problems??
            if ( KeyA[0] == ERROR_VAL )
            {
                stTrace = accumulated_output_info;
                return "ERROR";
            }
            accumulate_bitstring( "Key A bits:", KeyA, 8 );

            // get the key part B from the user
            get_value(ref KeyB, Key2, false);
            // problems??
            if ( KeyB[0] == ERROR_VAL )
            {
                stTrace = accumulated_output_info;
                return "ERROR";
            }
            accumulate_bitstring( "Key B bits:", KeyB, 8 );

            if (do_encrypt)
            {
                // TDES encrypt = DES encrypt/decrypt/encrypt
                accumulate_output("---- Starting first encryption ----");
                tempdata = des_encrypt( inData, KeyA, true );
                accumulate_output("---- Starting second decryption ----");
                tempdata = des_encrypt( tempdata, KeyB, false );
                accumulate_output("---- Starting third encryption ----");
                DES_output = des_encrypt( tempdata, KeyA, true );
            }
            else
            {
                // TDES decrypt = DES decrypt/encrypt/decrypt
                accumulate_output("---- Starting first decryption ----");
                tempdata = des_encrypt( inData, KeyA, false );
                accumulate_output("---- Starting second decryption ----");
                tempdata = des_encrypt( tempdata, KeyB, true );
                accumulate_output("---- Starting third decryption ----");
                DES_output = des_encrypt( tempdata, KeyA, false );
            }

            accumulate_bitstring ("Output ", DES_output, 8 );

            // process output
            stTrace = accumulated_output_info;
            if (do_encrypt)
                return format_DES_output(false);
            else
                return format_DES_output(IsAsciiOutput);
        }

        internal static string EncryptString(string st, long ContractNumber)
        {
            DEBCrypto cryptic = new DEBCrypto("TripleDES");
            cryptic.Key = "wqdj~yriu!@" + ContractNumber.ToString("".PadLeft(10, '0')) + "1%p$#=@hd+^";
            try
            {
                st = cryptic.Encrypt(st);
            }
            catch
            {
                return "";
            }
            return (st);
        }

        internal static string DecryptString(string st, long ContractNumber)
        {
            DEBCrypto cryptic = new DEBCrypto("TripleDES");
            cryptic.Key = "wqdj~yriu!@" + ContractNumber.ToString("".PadLeft(10, '0')) + "1%p$#=@hd+^";
            try
            {
                st = cryptic.Decrypt(st);
            }
            catch
            {
                return "";
            }
            return (st);
        }

        private byte SetBitOn(byte num, byte bit)
        {
            return (byte)((int)num | (1 << Math.Abs(bit-7)));
        }

        private byte SetBitOff(byte num, byte bit)
        {
            return (byte)((int)num & ((1 << Math.Abs(bit - 7)) ^ 255));
        }

        internal bool GetBitOn(int num, int bit)
        {
            return (((num >> Math.Abs(bit - 7)) & 1) == 1);
        }

        internal string Dec2Bin(int num)
        {
            char ch = (char)num;
            string RetVal = "";
            for (int i = 0; i < 8; i++)
                if ((((int)ch >> i) & 1) == 1) RetVal += "1"; else RetVal += "0";
            return RetVal;
        }

        internal int Bin2Dec(string num)
        {
            byte RetVal = 0;
            for (byte i = 0; i < 8; i++)
                RetVal = SetBitOn(RetVal, i);
            return RetVal;
        }

        #endregion

        public static DateTime UltimaTransaccion;

        public static bool
            IniciarLink()
        {
            Globales.EscribirBitacora("UtilsComunicacion", "IniciarLink", "Inicia",1);
            String l_ContractNumber;
            String l_ProviderId;
            String l_InstallationId;
            String l_SerialNumber;
            String l_SystemUser;
            String l_Key1;
            String l_Key2;
            String l_Mensaje;
            String l_MensajeEncriptado = "";
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("01|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            int l_ReturnValue = 0;

            if (BDEquipo.ObtenerEquipo(out l_SerialNumber, out l_ContractNumber, out l_ProviderId, out l_InstallationId,
                out l_SystemUser, out l_Key1, out l_Key2, Globales.PruebaBanamex))
            {
                m_InstallationId = l_InstallationId;
                m_FIDSerialNumber = l_SerialNumber;
                m_ContractNumber = l_ContractNumber;
                m_ProviderId = l_ProviderId;
                if (l_SystemUser.Length >= 1)
                    m_SystemUserId = l_SystemUser;

                if (l_Key1.Length >= 1)
                {
                    m_CryptKey1 = l_Key1;
                    m_CryptKey2 = l_Key2;
                    return IniciarSesion();
                }
                else
                {
                    m_CryptKey1 = l_ContractNumber.PadLeft(16, '0');
                    m_CryptKey2 = l_ProviderId.PadLeft(16, '0');
                }

                RetVal.Append(l_ContractNumber + "|");
                l_Mensaje = m_ProviderId + "|" + m_InstallationId;
                l_MensajeEncriptado = EncriptarMensaje(l_Mensaje);
                RetVal.Append(l_MensajeEncriptado);
                l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
                l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

                l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
                try
                {
                    if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                    {
                        if (l_ReturnValue == 0)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "Envío exitoso");
                            m_SystemUserId = DesencriptarMensaje(l_Respuesta.Substring(4)).Trim();
                            BDEquipo.ActualizarSystemUserId(m_FIDSerialNumber, m_SystemUserId, Globales.PruebaBanamex);
                            return FinalizarLink();
                        }
                        else if (l_ReturnValue == 1)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "Encripción no válida");
                            return false;
                        }
                        else if (l_ReturnValue == 2)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "ProviderID no válido");
                            return false;
                        }
                        else if (l_ReturnValue == 3)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "ContractID no está asignado a ProviderID");
                            return false;
                        }
                        else if (l_ReturnValue == 4)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "ContractID ya tiene asignado un InstallationID");
                                return TraerCredenciales();
                        }
                        else if (l_ReturnValue == 5)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "Installation ID inválido");
                            return false;
                        }
                        else if (l_ReturnValue == 6)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "BeginLink ya fue registrado para esta instalación");
                            return false;
                        }
                        else if (l_ReturnValue == 98)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "QueryString no esta en Base64");
                            return false;
                        }
                        else if (l_ReturnValue == 99)
                        {
                            BDLog.InsertarLog(1, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                            return IniciarLink();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("UtilsComunicacion", "IniciarLink", ex.ToString(),1);
                    return false;
                }

                return true;
            }
            else
                return false;
        }

        public static bool 
            FinalizarLink()
        {
            Globales.EscribirBitacora("UtilsComunicacion", "FinalizarLink", "Inicia",1);
            String l_Mensaje;
            StringBuilder RetVal = new StringBuilder();
            String l_Key1;
            String l_Key2;
            RetVal.Append("02|" + m_SystemUserId);
            String l_Respuesta;
            String l_RespuestaEncriptada;
            int l_ReturnValue = 0;

                l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
                l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

                l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
                try
                {
                    if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                    {
                        if (l_ReturnValue == 0)
                        {
                            BDLog.InsertarLog(2, l_ReturnValue, "Envío exitoso");
                            Byte[] bytes = ASCIIEncoding.ASCII.GetBytes(m_SystemUserId);
                            l_Key1 = Int64.Parse(m_ContractNumber).ToString().PadLeft(10, '0')
                                + bytes[13].ToString("X") + bytes[4].ToString("X") + bytes[17].ToString("X");
                            l_Key2 = Int64.Parse(m_ProviderId).ToString().PadLeft(10, '0') 
                                + bytes[2].ToString("X") + bytes[11].ToString("X") + bytes[5].ToString("X");
                            l_Respuesta = DesencriptarMensaje(l_Respuesta.Substring(4), l_Key1, l_Key2);
                            m_CryptKey1 = l_Respuesta.Substring(0, 16);
                            m_CryptKey2 = l_Respuesta.Substring(16, 16);
                            BDEquipo.ActualizarCryptKeys(m_FIDSerialNumber, m_CryptKey1, m_CryptKey2, Globales.PruebaBanamex);
                            return IniciarSesion();
                        }
                        else if (l_ReturnValue == 1)
                        {
                            BDLog.InsertarLog(2, 1, "UserID no válido");
                            return TraerCredenciales();
                        }
                        else if (l_ReturnValue == 2)
                        {
                            BDLog.InsertarLog(2, 2, "UserID no existe (BeginLink no ha sido registrado)");
                            //BDEquipo.BorrarVariables(m_FIDSerialNumber);
                            return IniciarLink();
                        }
                        else if (l_ReturnValue == 3)
                        {
                            BDLog.InsertarLog(2, 3, "EndLink ya fue registrado para esta instalación");
                            return false;
                        }
                        else if (l_ReturnValue == 98)
                        {
                            BDLog.InsertarLog(2, 98, "QueryString no esta en Base64");
                            return false;
                        }
                        else if (l_ReturnValue == 99)
                        {
                            BDLog.InsertarLog(2, 99, "El sistema se encuentra ocupado reintentar");
                            return FinalizarLink();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("UtilsComunicacion", "FinalizarLink", ex.ToString(),1);
                    return false;
                }

                return true;

        }

        public static bool
            IniciarSesion()
        {
            Globales.EscribirBitacora("UtilsComunicacion", "IniciarSesion", "Inicio",1);
            String l_Mensaje;
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("10|" + m_SystemUserId);
            String l_Respuesta;
            String l_RespuestaEncriptada;
            String l_DenominacionesMXP;
            String l_DenominacionesUSD;

            int l_ReturnValue = 0;
            int l_Usuarios;
            int l_Cuentas;

            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
            try
            {
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "Envío exitoso");
                        String[] lines = DesencriptarMensaje(l_Respuesta.Substring(4)).Split(
                            new String[] { Environment.NewLine }, StringSplitOptions.None);
                        m_SystemUserId = lines[0];
                        BDEquipo.ActualizarSystemUserId(m_FIDSerialNumber, m_SystemUserId, Globales.PruebaBanamex);
                        if (lines[1].Trim().Length > 0)
                        {
                            m_CryptKey1 = lines[1];
                            m_CryptKey2 = lines[2];
                            BDEquipo.ActualizarCryptKeys(m_FIDSerialNumber, m_CryptKey1, m_CryptKey2, Globales.PruebaBanamex);
                        }
                        
                        // Usuarios 
                        BDUsuarios.DesactivarUsuarios();
                        l_Usuarios = int.Parse(lines[5]);
                        for (int i = 0; i < l_Usuarios; i++)
                        {
                            BDUsuarios.ValidarUsuarioExistente(lines[6 + (i * 3)], int.Parse(lines[8 + (i * 3)]),
                                lines[7 + (i * 3)]);
                             
                        }

                        BDUsuarios.ValidarUsuarioExistente("ETV_Traslado", 2, "Pruebas de Retiro");
                        BDUsuarios.ValidarUsuarioExistente("Supervisor_ZONA", 3, "Pruebas de Administrador");
                        // Cuentas
                        BDCuentas.DesactivarCuentas();
                        l_Cuentas = int.Parse(lines[6 + (l_Usuarios * 3)]);
                        for (int i = 0; i < l_Cuentas; i++)
                        {
                            BDCuentas.ValidarCuentaExistente(lines[7 + (l_Usuarios * 3) + (i * 3)], int.Parse(lines[9 + (l_Usuarios * 3) + (i * 3)]),
                                lines[8 + (l_Usuarios * 3) + (i * 3)]);
                        }

                        BDDenominaciones.DesactivarDenominaciones();
                        l_DenominacionesMXP = lines[7 + (l_Usuarios * 3) + (l_Cuentas * 3)];
                        l_DenominacionesUSD = lines[8 + (l_Usuarios * 3) + (l_Cuentas * 3)];
                        for (int i = 0; i < l_DenominacionesMXP.Length / 11; i++)
                        {
                            BDDenominaciones.ValidarDenominacionExistente(((decimal)Int64.Parse(l_DenominacionesMXP.Substring((i * 11) + 1, 10)) / (decimal)100).ToString("C"),
                                1, l_DenominacionesMXP.Substring((i * 11), 1));
                        }

                        for (int i = 0; i < l_DenominacionesUSD.Length / 11; i++)
                        {
                            BDDenominaciones.ValidarDenominacionExistente(((decimal)Int64.Parse(l_DenominacionesUSD.Substring((i * 11) + 1, 10)) / (decimal)100).ToString("C"),
                                1, l_DenominacionesUSD.Substring((i * 11), 1));
                        }
                        return true;
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "UserID no válido");
                        return TraerCredenciales();
                    }
                    else if (l_ReturnValue == 2)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "UserID no existe (BeginLink no ha sido registrado)");
                        //BDEquipo.BorrarVariables(m_FIDSerialNumber);
                        return IniciarLink();
                    }
                    else if (l_ReturnValue == 3)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "ContractNumber ya no existe");
                        return false;
                    }
                    else if (l_ReturnValue == 4)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "ContractNumber asociado a otro ProviderID");
                        return false;
                    }
                    else if (l_ReturnValue == 5)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "ContractNumber asociado a otro InstallationID");
                        return false;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "QueryString no esta en Base64");
                        return false;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(10, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return IniciarSesion();
                    }
                }
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("UtilsComunicacion", "IniciarSesion", ex.ToString(),1);
                return true;
            }

            return true;

        }

        public static bool
            EnviarTransaccion(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, int p_TipoTransaccion,
            Double p_TotalTransaccion, DataTable p_DetalleTransaccion, int p_Folio, bool p_Insertar, String p_IdUsuario )
        {
            String l_Mensaje;
            String l_Alerta="";
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("20|" + m_SystemUserId + "|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            
            
            if (p_TipoTransaccion == 1)
            {
                //Insertar Depósito
                if (p_Insertar)
                {
                    if (!BDDeposito.InsertarDeposito(p_IdCuenta, p_IdCajon, p_IdMoneda, p_TotalTransaccion
                        , p_DetalleTransaccion, p_IdUsuario, out p_Folio))
                        return false;
                    else
                    {
                        ModulePrint imprimir = new ModulePrint();
                        imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);
                        imprimir.FontSize = 10;


                        imprimir.AddHeaderLine("Depósito");
                        imprimir.AddHeaderLine("");
                        imprimir.AddHeaderLine("NUMERIO SERIAL DEL EQUIPO RECEPTOR: FID" + Globales.Serial1.ToString());
                        imprimir.AddHeaderLine("");
                        imprimir.AddHeaderLine("Cuenta No: " + p_IdCuenta);
                        if (p_IdMoneda == 1)
                            imprimir.AddHeaderLine("Moneda: Pesos");
                        else
                            imprimir.AddHeaderLine("Moneda: Dólares");
                        imprimir.AddSubHeaderLine("Usuario: " + p_IdUsuario);
                        imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
                        imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());
                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            imprimir.AddItem(l_Detalle[1].ToString(), Double.Parse(l_Detalle[2].ToString()).ToString("$#,###,##0.00"),
                                Double.Parse(int.Parse(l_Detalle[2].ToString()).ToString()).ToString());
                        }
                        imprimir.AddFooterLine("Total Depósito: " + p_TotalTransaccion.ToString("$#,###,###,##0.00")); 
                        imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"],DateTime.Now.ToShortTimeString());
                    }
                }
                //Armar cadena envío
                RetVal.Append(EncriptarMensaje("1|" + p_TotalTransaccion.ToString("0.00")));
            }
            else if (p_TipoTransaccion == 2)
            {
                if (p_Insertar)
                {
                    //Insertar Retiro
                    if (!BDRetiro.InsertarRetiro(p_IdMoneda, p_TotalTransaccion, p_DetalleTransaccion, p_IdCajon, out p_Folio))
                        return false;
                }
                //Armar cadena envío
                RetVal.Append(EncriptarMensaje("2|" + p_TotalTransaccion.ToString("0.00")));
            }
            else if (p_TipoTransaccion == 3)
            {
                if (p_Insertar)
                {

                   if(!BDCorte.InsertarCorte(p_IdUsuario,out p_Folio))
                    return false;
                }
                //Armar cadena envío
                RetVal.Append(EncriptarMensaje("3|0"));
            }
            else
            {
                l_Alerta = p_IdCuenta;
                if (p_Insertar)
                {
                    // Insertar Mensaje
                    if (!BDAlerta.InsertarAlerta(l_Alerta, out p_Folio))
                        return false;
                }
                //Armar cadena envío
                RetVal.Append(EncriptarMensaje("4|0"));
            }
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));

            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));

           
            try
            {
                Int32 l_ReturnValue;
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Envío exitoso");
                        l_Respuesta = DesencriptarMensaje(l_Respuesta.Substring(4));
                        m_ProcessId = Int64.Parse(l_Respuesta);

                        Globales.EscribirBitacora("Comunicaciones","Obtener ProdecssId:",m_ProcessId.ToString() ,1);

                        if (p_TipoTransaccion == 1)
                        {
                            if (BDDeposito.ActualizarProcessId(p_Folio, m_ProcessId))
                            {
                                if (EnviarDetalleTransaccion(p_IdCuenta, p_IdMoneda, p_TipoTransaccion,
                                    p_Folio, p_TotalTransaccion, p_DetalleTransaccion, m_ProcessId, p_IdUsuario))
                                    return ConfirmarTransaccion(m_ProcessId);
                            }
                        }
                        else if (p_TipoTransaccion == 2)
                        {
                            if (BDRetiro.ActualizarProcessId(p_Folio, m_ProcessId))
                            {
                                if (EnviarDetalleTransaccion(p_IdCuenta, p_IdMoneda, p_TipoTransaccion,
                                    p_Folio, p_TotalTransaccion, p_DetalleTransaccion, m_ProcessId, p_IdUsuario))
                                    return ConfirmarTransaccion(m_ProcessId);
                            }
                        }
                        // Faltan mensajes y cortes
                        else if (p_TipoTransaccion == 3)
                        {
                            if (BDCorte.ActualizarProcessId(p_Folio, m_ProcessId))
                            {
                                if (EnviarDetalleTransaccion("0", 0, p_TipoTransaccion,
                                    p_Folio, 0, "0", m_ProcessId, p_IdUsuario))
                                    return ConfirmarTransaccion(m_ProcessId);
                            }
                        }
                        else if (p_TipoTransaccion == 4)
                        {
                            if (BDAlerta.ActualizarProcessId(p_Folio, m_ProcessId))
                            {
                                if (EnviarDetalleTransaccion(l_Alerta, 0, p_TipoTransaccion,
                                    p_Folio, 0, l_Alerta, m_ProcessId, p_IdUsuario))
                                    return ConfirmarTransaccion(m_ProcessId);
                            }
                        }
                        return true;
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Encripción no válida");
                        return true;
                    }
                    else if (l_ReturnValue == 2)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Servicio no disponible");
                        //BDEquipo.BorrarVariables(m_FIDSerialNumber);
                        return true;
                    }
                    else if (l_ReturnValue == 3)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Usuario no válido");
                        return TraerCredenciales();
                    }
                    else if (l_ReturnValue == 4)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Operación no soportada");
                        return true;
                    }
                    else if (l_ReturnValue == 5)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "Importe no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "QueryString no esta en Base64");
                        return true;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(20, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return Enviar_Transaccion(p_IdCuenta, p_IdCajon, p_IdMoneda, p_TipoTransaccion,
                            p_TotalTransaccion, p_DetalleTransaccion, p_Folio, false, p_IdUsuario);
                    }
                    else
                    {
                        Globales.EscribirBitacora("Enviar_Transaccion","Error",l_ReturnValue.ToString(),1);
                    }
                }
            }
            catch (Exception ex)
            {
                // cambiar a false cuando todo funcione
                Globales.EscribirBitacora("Enviar_Transaccion", "Error", "Execpcion " + ex.Message ,1);
                return false;
            }
            return true;
        }

        public static bool
            EnviarDetalleTransaccion(String p_IdCuenta, int p_IdMoneda, int p_TipoTransaccion, int p_Folio,
                Double p_TotalTransaccion, DataTable p_DetalleTransaccion, Int64 p_IdProceso, String p_IdUsuario)
        {
            String l_DetalleTransaccion = "";
            String l_Mensaje;
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("21|" + p_IdProceso.ToString() + "|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
            {
                l_DetalleTransaccion += "B" + (int.Parse(l_Detalle[2].ToString()) * 100).ToString("0000000000")
                    + Int32.Parse(l_Detalle[1].ToString()).ToString("0000000000");
            }
                //l_DetalleTransaccion
            if (p_TipoTransaccion == 1)
            {
                RetVal.Append(EncriptarMensaje(p_Folio + Environment.NewLine + DateTime.Now.ToString("yyyyMMddHHmmss")
                    + Environment.NewLine + m_FIDSerialNumber + Environment.NewLine + p_IdMoneda.ToString() + Environment.NewLine 
                        + p_TotalTransaccion.ToString("#######0.00") + Environment.NewLine + m_ContractNumber + Environment.NewLine
                            + p_IdCuenta + Environment.NewLine + p_IdUsuario + Environment.NewLine
                                + l_DetalleTransaccion + Environment.NewLine));
            }
            else if (p_TipoTransaccion == 2)
            {
                RetVal.Append(EncriptarMensaje(p_Folio + Environment.NewLine + DateTime.Now.ToString("yyyyMMddHHmmss")
                    + Environment.NewLine + m_FIDSerialNumber + Environment.NewLine + p_IdMoneda.ToString() + Environment.NewLine
                        + p_TotalTransaccion.ToString("#######0.00") + Environment.NewLine + m_ContractNumber + Environment.NewLine
                            + "8700006558" + Environment.NewLine + p_IdUsuario + Environment.NewLine
                                + l_DetalleTransaccion + Environment.NewLine));
            }
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));

            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);
            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));

            try
            {
                Int32 l_ReturnValue;
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Envío exitoso");
                        if (ConfirmarTransaccion(p_IdProceso))
                        {
                            if (p_TipoTransaccion == 1)
                                BDDeposito.ConfirmarDeposito(p_Folio);
                            else if (p_TipoTransaccion == 2)
                                BDRetiro.ConfirmarRetiro(p_Folio);
                        }
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Encripción no válida");
                        return true;
                    }
                    else if (l_ReturnValue == 2 || l_ReturnValue == 20)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Servicio no disponible");
                        return true;
                    }
                    else if (l_ReturnValue == 3 || l_ReturnValue == 30)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId no existe");
                        return true;
                    }
                    else if (l_ReturnValue == 4 || l_ReturnValue == 40)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId inválido");
                        return true;
                    }
                    else if (l_ReturnValue == 5 || l_ReturnValue == 50)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId arribado previamente");
                        return true;
                    }
                    else if (l_ReturnValue == 6 || l_ReturnValue == 60)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "OperacionId no continuo");
                        return true;
                    }
                    else if (l_ReturnValue == 7 || l_ReturnValue == 70)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Currency no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 8 || l_ReturnValue == 80)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Los montos de dialogos 20 y 21 no corresponden");
                        return true;
                    }
                    else if (l_ReturnValue == 9 || l_ReturnValue == 90)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ContractNumber no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 10)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "AccountNumber no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 11)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ContractUserID no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 12)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Error al leer el desgloce de denominaciones");
                        return true;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "QueryString no esta en Base64");
                        return true;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return EnviarDetalleTransaccion(p_IdCuenta, p_IdMoneda, p_TipoTransaccion, p_Folio, p_TipoTransaccion,
                            p_DetalleTransaccion, p_IdProceso, p_IdUsuario);
                    }
                }
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Enviar_Transaccion", "Error", "Execpcion " + ex.Message, 1);
                return true;
            }
            return true;
        }

        public static bool
            EnviarDetalleTransaccion(String p_Alerta, int p_IdMoneda, int p_TipoTransaccion, int p_Folio,
                Double p_TotalTransaccion, String p_Detalle, Int64 p_IdProceso, String p_IdUsuario)
        {
            
            String l_Mensaje;
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("21|" + p_IdProceso.ToString() + "|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
           
            //l_DetalleTransaccion CORTE y MENSAJE
            if (p_TipoTransaccion == 3)
            {
                RetVal.Append(EncriptarMensaje(p_Folio + Environment.NewLine + DateTime.Now.ToString("yyyyMMddHHmmss")
                    + Environment.NewLine + m_FIDSerialNumber + Environment.NewLine + p_IdMoneda.ToString() + Environment.NewLine
                        + p_TotalTransaccion.ToString() + Environment.NewLine + m_ContractNumber + Environment.NewLine
                            + "0" + Environment.NewLine + p_IdUsuario + Environment.NewLine
                                + "0" + Environment.NewLine));
            }
            else if (p_TipoTransaccion == 4)
            {
                RetVal.Append(EncriptarMensaje(p_Folio + Environment.NewLine + DateTime.Now.ToString("yyyyMMddHHmmss")
                    + Environment.NewLine + m_FIDSerialNumber + Environment.NewLine + p_IdMoneda.ToString() + Environment.NewLine
                        + p_TotalTransaccion.ToString() + Environment.NewLine + m_ContractNumber + Environment.NewLine
                            + "0" + Environment.NewLine + p_IdUsuario + Environment.NewLine
                                + p_Alerta + Environment.NewLine));
            }
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));

            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);
            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));

            try
            {
                Int32 l_ReturnValue;
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Envío exitoso");
                        if (ConfirmarTransaccion(p_IdProceso))
                        {
                            if (p_TipoTransaccion == 1)
                                BDDeposito.ConfirmarDeposito(p_Folio);
                            else if (p_TipoTransaccion == 2)
                                BDRetiro.ConfirmarRetiro(p_Folio);
                            else if (p_TipoTransaccion == 3)
                                BDCorte.ConfirmarCorte(p_Folio);
                            else if (p_TipoTransaccion == 4)
                                BDAlerta.ConfirmarAlerta(p_Folio);
                        }
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Encripción no válida");
                        return true;
                    }
                    else if (l_ReturnValue == 2 || l_ReturnValue == 20)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Servicio no disponible");
                        return true;
                    }
                    else if (l_ReturnValue == 3 || l_ReturnValue == 30)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId no existe");
                        return true;
                    }
                    else if (l_ReturnValue == 4 || l_ReturnValue == 40)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId inválido");
                        return true;
                    }
                    else if (l_ReturnValue == 5 || l_ReturnValue == 50)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ProcessId arribado previamente");
                        return true;
                    }
                    else if (l_ReturnValue == 6 || l_ReturnValue == 60)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "OperacionId no continuo");
                        return true;
                    }
                    else if (l_ReturnValue == 7 || l_ReturnValue == 70)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Currency no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 8 || l_ReturnValue == 80)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Los montos de dialogos 20 y 21 no corresponden");
                        return true;
                    }
                    else if (l_ReturnValue == 9 || l_ReturnValue == 90)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ContractNumber no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 10)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "AccountNumber no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 11)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "ContractUserID no válido");
                        return true;
                    }
                    else if (l_ReturnValue == 12)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "Error al leer el desgloce de denominaciones");
                        return true;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "QueryString no esta en Base64");
                        return true;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(21, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return EnviarDetalleTransaccion(p_Alerta, p_IdMoneda, p_TipoTransaccion, p_Folio, p_TipoTransaccion,
                            p_Detalle, p_IdProceso, p_IdUsuario);
                    }
                }
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Enviar_detalle_Transaccion", "Error", "Execpcion " + ex.Message, 1);
                return true;
            }
            return true;
        }

        public static bool
            ConfirmarTransaccion(Int64 p_IdProceso)
        {
            String l_Mensaje;
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("22|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            RetVal.Append(p_IdProceso.ToString());
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);
            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
            Int32 l_ReturnValue;
            try
            {
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "Envío exitoso");
                        if (l_Respuesta.Substring(3) == "1")
                        {
                            BDEquipo.BorrarVariables(m_FIDSerialNumber,Globales.PruebaBanamex);
                            return IniciarLink();
                        }
                        else
                            return true;
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "Servicio no disponible");
                        return false;
                    }
                    else if (l_ReturnValue == 2)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "ProcessID no existe");
                        return false;
                    }
                    else if (l_ReturnValue == 3)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "ProcessID no válido");
                        return false;
                    }
                    else if (l_ReturnValue == 4)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "Transacción asociada a ProcessID no a sido completada");
                        // Mandar nuevamente el detalle
                        return false;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "QueryString no esta en Base64");
                        return false;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(22, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return ConfirmarTransaccion(p_IdProceso);
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Confirmar_Transaccion", "Error", "Execpcion " + ex.Message, 1);
                return true;
            }
            return true;
        }

        public static bool
            TraerCredenciales()
        {
            String l_Mensaje;
            String l_MensajeEncriptado = "";
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("99|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            int l_ReturnValue = 0;

            m_CryptKey1 = m_ContractNumber.PadLeft(16, '0');
            m_CryptKey2 = m_ProviderId.PadLeft(16, '0');

            RetVal.Append(m_ContractNumber + "|");
            l_Mensaje = m_ProviderId + "|" + m_InstallationId;
            l_MensajeEncriptado = EncriptarMensaje(l_Mensaje);
            RetVal.Append(l_MensajeEncriptado);
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
            try
            {
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "Envío exitoso");
                        l_Respuesta = DesencriptarMensaje(l_Respuesta.Substring(4));
                        m_SystemUserId = l_Respuesta.Substring(0, 20);
                        BDEquipo.ActualizarSystemUserId(m_FIDSerialNumber, m_SystemUserId,Globales.PruebaBanamex);
                        m_CryptKey1 = l_Respuesta.Substring(20, 16);
                        m_CryptKey2 = l_Respuesta.Substring(36, 16);
                        BDEquipo.ActualizarCryptKeys(m_FIDSerialNumber, m_CryptKey1, m_CryptKey2, Globales.PruebaBanamex);
                        IniciarSesion();
                        return true;
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "Encripción no válida");
                        return false;
                    }
                    else if (l_ReturnValue == 2)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "ProviderID no válido");
                        return false;
                    }
                    else if (l_ReturnValue == 3)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "ContratID no está asignado a ProviderID");
                        return false;
                    }
                    else if (l_ReturnValue == 4)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "Installation ID inválido");
                        return false;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "QueryString no esta en Base64");
                        return false;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(99, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return TraerCredenciales();
                    }
                }
                else 
                    return false;
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Traer Credenciales", "Error", "Excepcion " + ex.Message, 1);
                return false;
            }
            return true;
        }

        public static bool
            MantenerVivo()
        {
            Globales.EscribirBitacora("UtilsComunicacion", "MantenerVivo", "Inicia",1);
            String l_ContractNumber;
            String l_ProviderId;
            String l_InstallationId;
            String l_SerialNumber;
            String l_SystemUser;
            String l_Key1;
            String l_Key2;
            String l_Mensaje;
            String l_MensajeEncriptado = "";
            StringBuilder RetVal = new StringBuilder();
            RetVal.Append("90|");
            String l_Respuesta;
            String l_RespuestaEncriptada;
            int l_ReturnValue = 0;
            

         //   if (m_ContractNumber == null)   //equipo virtual
          //  {
                BDEquipo.ObtenerEquipo(out l_SerialNumber, out l_ContractNumber, out l_ProviderId,
                    out l_InstallationId, out l_SystemUser, out l_Key1, out l_Key2,Globales.PruebaBanamex);
                m_ContractNumber = l_ContractNumber;
                m_FIDSerialNumber = l_SerialNumber;
                m_InstallationId = l_InstallationId;
                m_ProviderId = l_ProviderId;
                m_CryptKey1 = l_Key1;
                m_CryptKey2 = l_Key2;
                m_SystemUserId = l_SystemUser;

                if (m_FIDSerialNumber == "25647") //equipo virtual
                    Globales.Equipo = "Equipo2"; //equipo virtual
                if (m_SystemUserId == "")
                    IniciarLink();
          
          

            l_Key1 = m_ContractNumber.PadLeft(16, '0');
            l_Key2 = m_ProviderId.PadLeft(16, '0');

            RetVal.Append(m_ContractNumber + "|");
            l_Mensaje = m_ProviderId + "|" + m_InstallationId;
            l_MensajeEncriptado = EncriptarMensaje(l_Mensaje, l_Key1, l_Key2);
            RetVal.Append(l_MensajeEncriptado);
            l_Mensaje = ServerName + "?" + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(RetVal.ToString()));
            l_RespuestaEncriptada = EnviarMensaje(l_Mensaje);

            l_Respuesta = ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(l_RespuestaEncriptada));
            try
            {
                if (Int32.TryParse(l_Respuesta.Substring(2, 2), out l_ReturnValue))
                {
                    if (l_ReturnValue == 0)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "Envío exitoso");
                        int l_Usuarios = BDUsuarios.ContarUsuarios();
                        Globales.EscribirBitacora("UtilsComunicacion", "MantenerVivo", "Usuarios " + l_Usuarios.ToString(),1);
                        if (Globales.IniciarSesion)
                        {
                            IniciarSesion();
                            Globales.IniciarSesion = false;
                        }
                        return true;
                    }
                    else if (l_ReturnValue == 1)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "Encripción no válida");
                        return false;
                    }
                    else if (l_ReturnValue == 2)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "ProviderID no válido");
                        return false;
                    }
                    else if (l_ReturnValue == 3)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "ContratID no está asignado a ProviderID");
                        return TraerCredenciales();
                    }
                    else if (l_ReturnValue == 4)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "Installation ID inválido");
                        return false;
                    }
                    else if (l_ReturnValue == 98)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, " QueryString no esta en Base64");
                        return false;
                    }
                    else if (l_ReturnValue == 99)
                    {
                        BDLog.InsertarLog(90, l_ReturnValue, "El sistema se encuentra ocupado reintentar");
                        return MantenerVivo();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("UtilsComunicacion", "MantenerVivo", ex.ToString(),1);
                return true;
            }
        }

        public static bool
            BuscarDepositosNoEnviados()
        {
            DataTable l_Datos = BDDeposito.BuscarDepositosNoEnviados();
            bool l_Resultado = true;
            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
               Globales.CambiaEquipo(l_Detalle[6].ToString());
               UtilsComunicacion.MantenerVivo();


                DataTable l_DatosDetalle = BDDeposito.ObtenerDetalleDeposito((int)l_Detalle[1]);
                if (l_Detalle[0] != DBNull.Value)
                {
                    if (!EnviarDetalleTransaccion(l_Detalle[2].ToString(), (int)l_Detalle[3], 1, (int)l_Detalle[1],
                        Double.Parse(l_Detalle[4].ToString()), l_DatosDetalle, Int64.Parse(l_Detalle[0].ToString()), 
                            l_Detalle[5].ToString()))
                        l_Resultado = false;
                }
                else
                {
                    Globales.EscribirBitacora("EnviarTransaccion", "Comunicaciones", "¿?",1);

                    if (!Enviar_Transaccion(l_Detalle[2].ToString(), 1, (int)l_Detalle[3], 1, Double.Parse(l_Detalle[4].ToString()),
                        l_DatosDetalle, (int)l_Detalle[1], false, l_Detalle[5].ToString()))
                        l_Resultado = false;
                }
            }
            l_Datos = BDRetiro.BuscarRetirosNoEnviados();
            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
                Globales.CambiaEquipo(l_Detalle[5].ToString());
                UtilsComunicacion.MantenerVivo();

                DataTable l_DetalleRetiro = BDRetiro.ObtenerDetalleRetiro((int)l_Detalle[1]);
                if (l_Detalle[0] != DBNull.Value)
                {
                    if (!EnviarDetalleTransaccion("", (int)l_Detalle[2], 2, (int)l_Detalle[1],
                        Double.Parse(l_Detalle[3].ToString()), l_DetalleRetiro, Int64.Parse(l_Detalle[0].ToString()),
                            l_Detalle[4].ToString()))
                        l_Resultado = false;
                }
                else
                {
                    if (!Enviar_Transaccion("", 1, (int)l_Detalle[2], 2, Double.Parse(l_Detalle[3].ToString()),
                        l_DetalleRetiro, (int)l_Detalle[1], false, l_Detalle[4].ToString()))
                        l_Resultado = false;
                }
            }
            l_Datos = BDCorte.BuscarCortesNoEnviados();
            
            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
                
                if (l_Detalle[0] != DBNull.Value)
                {
                    if (!EnviarDetalleTransaccion("", 0, 3, Convert.ToInt16(l_Detalle[1]), 0, "0", Int64.Parse(l_Detalle[0].ToString()), l_Detalle[2].ToString()))
                        l_Resultado = false;
                }
                else
                {
                    if (!EnviarCorte((int)l_Detalle[1],l_Detalle[2].ToString(),false))
                        l_Resultado = false;
                }
            }
            l_Datos = BDAlerta.BuscarAlertasNoEnviadas();

            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
               
                if (l_Detalle[0] != DBNull.Value)
                {
                    if (!EnviarDetalleTransaccion(l_Detalle[2].ToString(), 0, 4, (int)l_Detalle[1], 0, l_Detalle[2].ToString(), Int64.Parse(l_Detalle[0].ToString()), "FIDSymetry"))
                        l_Resultado = false;
                }
                else
                {
                    if (!EnviarAlerta(l_Detalle[2].ToString(),false))
                        l_Resultado = false;
                }
            }


            return l_Resultado;
        }

        private static String 
            DesencriptarMensaje(String p_MensajeEncriptado)
        {
            String stTrace = "";
            String l_Mensaje = "";

            Globales.EscribirBitacora("Comunicacion", "DesEncriptar", p_MensajeEncriptado,1);

            while (p_MensajeEncriptado.Length > 0)
            {
                l_Mensaje += do_tdes(false,
                    p_MensajeEncriptado.Substring(0, true ? 16 : 8), !true,
                    m_CryptKey1, m_CryptKey2, !false, ref stTrace, false);
                p_MensajeEncriptado = p_MensajeEncriptado.Substring(true ? 16 : 8);
            }

            return l_Mensaje;
        }

        private static String
            DesencriptarMensaje(String p_MensajeEncriptado, String p_Key1, String p_Key2)
        {
            String stTrace = "";
            String l_Mensaje = "";

            Globales.EscribirBitacora("Comunicacion", "DesEncriptar", p_MensajeEncriptado,1);

            while (p_MensajeEncriptado.Length > 0)
            {
                l_Mensaje += do_tdes(false,
                    p_MensajeEncriptado.Substring(0, true ? 16 : 8), !true,
                    p_Key1, p_Key2, !false, ref stTrace, false);
                p_MensajeEncriptado = p_MensajeEncriptado.Substring(true ? 16 : 8);
            }

            return l_Mensaje;
        }

        private static String 
            EncriptarMensaje(String p_Mensaje)
        {
            String stTrace = "";
            String l_MensajeEncriptado = "";

            Globales.EscribirBitacora("Comunicacion","Encriptar",p_Mensaje,1);

            while (p_Mensaje.Length > 0)
            {
                try
                {
                    l_MensajeEncriptado += do_tdes(true, p_Mensaje.Substring(0, false ? 16 : 8),
                        !false, m_CryptKey1, m_CryptKey2, !false, ref stTrace, false);
                    p_Mensaje = p_Mensaje.Substring(false ? 16 : 8);
                }
                catch
                {
                    l_MensajeEncriptado += do_tdes(true, p_Mensaje.Substring(0, p_Mensaje.Length),
                        !false, m_CryptKey1, m_CryptKey2, !false, ref stTrace, false);
                    p_Mensaje = "";

                }
            }
            return l_MensajeEncriptado;
        }

        private static String
            EncriptarMensaje(String p_Mensaje, String p_Key1, String p_Key2)
        {
            String stTrace = "";
            String l_MensajeEncriptado = "";

            Globales.EscribirBitacora("Comunicacion", "Encriptar", p_Mensaje,1);

            while (p_Mensaje.Length > 0)
            {
                try
                {
                    l_MensajeEncriptado += do_tdes(true, p_Mensaje.Substring(0, false ? 16 : 8),
                        !false, p_Key1, p_Key2, !false, ref stTrace, false);
                    p_Mensaje = p_Mensaje.Substring(false ? 16 : 8);
                }
                catch
                {
                    l_MensajeEncriptado += do_tdes(true, p_Mensaje.Substring(0, p_Mensaje.Length),
                        !false, p_Key1, p_Key2, !false, ref stTrace, false);
                    p_Mensaje = "";

                }
            }
            return l_MensajeEncriptado;
        }

        private static String
            EnviarMensaje(String p_Mensaje)
        {
            Globales.EscribirBitacora("UtilsComunicacion", "EnviarMensaje", "Inicia",1);
            String l_Respuesta = "";
           if (Globales.DEBUG)//
           {//
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                    WebRequest request = WebRequest.Create(p_Mensaje);

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    l_Respuesta = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    UltimaTransaccion = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("UtilsComunicacion", "EnviarMensaje", ex.ToString(),1);
                }
           }//
            else//
              l_Respuesta = "0000";//
            UltimaTransaccion = DateTime.Now;
            return l_Respuesta;
        }

        private static String ServerName
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["ServerName"]; }
        }

        public static bool EnviarAlerta(String p_Mensaje,bool p_insertar)
        {
            return UtilsComunicacion.Enviar_Transaccion(p_Mensaje, 0, 0,4, 0, null,0, p_insertar,"FIDSymetry");
        }
        public static bool EnviarCorte(int p_folio, String p_IdUsuario,bool p_insertar)
        {
            return UtilsComunicacion.Enviar_Transaccion("0", 0, 0, 3, 0, null,p_folio, p_insertar, p_IdUsuario);
        }

        public static bool
          Enviar_Transaccion(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, int p_TipoTransaccion,
          Double p_TotalTransaccion, DataTable p_DetalleTransaccion, int p_Folio, bool p_Insertar, String p_IdUsuario)
        {
            int l_numbilletes = 0;
            int contador=0;

            if (p_DetalleTransaccion != null)
            {
                foreach (DataRow fila in p_DetalleTransaccion.Rows)
                {
                    l_numbilletes += (int) fila["Cantidad"];
                }
            }



            if (p_TipoTransaccion == 1)
            {
                //Insertar Depósito
                if (p_Insertar)
                {
                    // -- LEON 2016-04-06
                    decimal l_MontoActual = (decimal)BDDeposito.ObtenerMontoActual();


                    if (!BDDeposito.InsertarDeposito(p_IdCuenta, p_IdCajon, p_IdMoneda, p_TotalTransaccion
                        , p_DetalleTransaccion, p_IdUsuario, out p_Folio))
                        return false;
                    else
                    {
                       
                         
                   
                        ModulePrint imprimir = new ModulePrint();
                        imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);
                       
                       
                      
                        imprimir.AddHeaderLine(" ");
                        imprimir.AddHeaderLine(" ");
                        imprimir.AddHeaderLine(" ");

                        imprimir.AddHeaderLine("Depósito de EFECTIVO");
                        //imprimir.AddHeaderLine( "ZONA CENTRO" );
                        imprimir.AddHeaderLine( "Ubicación: " + Globales.SucursalProsegur  );

                        imprimir.AddHeaderLine(" ");
                        if (p_IdMoneda == 1)
                            imprimir.AddHeaderLine("Moneda: Pesos");
                        else
                            imprimir.AddHeaderLine("Moneda: Dólares");
                       // imprimir.AddHeaderLine(" ");
                        imprimir.AddHeaderLine("Nombre:" );
                        imprimir.AddHeaderLine( Globales.NombreUsuario.ToUpper());


                        imprimir.AddHeaderLine("Usuario:  " + p_IdUsuario);
                        imprimir.AddHeaderLine("Cliente: " + Globales.EmpresaCliente);
                        imprimir.AddHeaderLine("Cuenta: " + Globales.CuentaBanco);
                
                        //imprimir.AddHeaderLine("Referencia: " +Globales.ReferenciaUsuario );
                        //imprimir.AddHeaderLine("Usuario:  " + p_IdUsuario);
                        //imprimir.AddHeaderLine("Convenio: "+ Globales.ConevenioDEMUsuario );

                        imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") +" Hora: " + DateTime.Now.ToShortTimeString());
                      //  imprimir.AddHeaderLine("");
                       // imprimir.AddSubHeaderLine( "Hora: " + DateTime.Now.ToShortTimeString());
                        imprimir.AddHeaderLine("");
                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {
                            imprimir.AddItem(l_Detalle[1].ToString(), Double.Parse(l_Detalle[2].ToString()).ToString("$#,###,##0.00"),
                                Double.Parse(int.Parse(l_Detalle[2].ToString()).ToString()).ToString());
                        }

                        imprimir.AddFooterLine("BILLETES TOTALES:           " + l_numbilletes.ToString () );

                        imprimir.AddFooterLine("Total Depósito: " + p_TotalTransaccion.ToString("$#,###,###,##0.00"));
                        imprimir.AddFooterLine("");
                        imprimir.AddFooterLine("EMISIÓN: "+ Globales.ClienteProsegur );
                        imprimir.AddFooterLine("SERIAL EQUIPO: " + Globales.Serial1);

                        imprimir.AddFooterLine("");
                        

                        

                        while (contador < Globales.TicketsDeposito)
                        {
                            imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
                            imprimir.AddFooterLine("COMPROBANTE COPIA para Usuario: " + p_IdUsuario);
                            contador++;
                        }
                       
                        imprimir.Dispose();


                        // Form.ActiveForm.Refresh();
                        // Form.ActiveForm.Update();
                        MensajeDeposito2 l_irds = new MensajeDeposito2(Globales.Estacion
                     , Globales.SucursalProsegur
                     , BDDeposito. FolioCiclo()
                     , p_Folio
                     , Moneda.MonedaPesos.ToString()
                     , Globales.FechaHoraSesion
                     , DateTime.Now
                     , Globales.IdCliente
                     , Globales.NombreCliente
                     , Globales.Banco
                     , Globales.CuentaBanco
                     , Globales.ReferenciaUsuario
                     , Globales.IdUsuario
                     , Globales.NombreUsuario
                     , l_MontoActual
                     , (decimal)p_TotalTransaccion
                     , 0
                     , 0
                     );

                        foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
                        {

                            l_irds.AgregarDenominacion(1, Int32.Parse(l_Detalle[2].ToString()), (Int32)l_Detalle[1]); // 10 de 100$ en contenedor 1
                        }
                        Globales.EscribirBitacora("Deposito", "Informacion", "Preparando mensaje a Portal", 1);

                        try
                        {
                            String l_Json = l_irds.CodificarJson();

                            String[] l_Servers = ConfigurationManager.AppSettings["Servers"].Split(',');
                            foreach (String l_Servidor in l_Servers)
                            {
                                Globales.EscribirBitacora("Deposito", "Informacion", "Preparando Sevidor:" + l_Servidor, 1);
                                int l_IdServidor = Convert.ToInt32(l_Servidor);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Encolando a Servidor:" + l_Servidor, 1);
                                ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(l_IdServidor);
                                l_Cola.AgregarMensaje(MensajeIrds.CategoriaMensajeEnum.Transaccion
                               , MensajeIrds.TipoMensajeEnum.Transaccion_Deposito
                               , 2
                               , l_Json);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Enviando Mensajes Pendientes al Sevidor:" + l_Servidor, 1);
                                l_Cola.MandarMensajesPendientes(Globales.Estacion);
                                Globales.EscribirBitacora("Deposito", "Informacion", "Termino y Respuesta del Sevidor:" + l_Servidor, 1);
                            }
                        }
                        catch (Exception p)
                        {
                            Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", p.Message, 1);
                        }

                        Application.DoEvents();

                        Prosegur.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Prosegur.l_tipoTransaccion.Deposito, p_Folio);

                        //BDSide.InsertarTransaccion(p_Folio, Globales.Serial1, Globales.IdUsuario, 14, DateTime.Now, DateTime.Now
                        //        , Globales.NumeroSerieBOLSA, int.Parse(Globales.NoCuenta), int.Parse(Globales.ReferenciaUsuario.Replace("0","")), 0
                        //        , 0, "Deposito",(int) p_TotalTransaccion, l_numbilletes, 0, 0, 0, 0, "", "Pesos", p_DetalleTransaccion, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
                    }
                }
              
            }
            else if (p_TipoTransaccion == 2)
            {
                if (p_Insertar)
                {
                    //Insertar Retiro
                    if (!BDRetiro.InsertarRetiro(p_IdMoneda, p_TotalTransaccion, p_DetalleTransaccion, p_IdCajon, out p_Folio))
                        return false;
                    else
                    {
                        BDRetiro.ActualizarRetiros("Equipo1");
                        Prosegur.CrearTransaccion(p_IdUsuario, p_DetalleTransaccion, Prosegur.l_tipoTransaccion.Retiro, p_Folio);

                        //BDSide.InsertarTransaccion(p_Folio, Globales.Serial1, Globales.IdUsuario, 44, DateTime.Now, DateTime.Now
                        //       , Globales.NumeroSerieBOLSAanterior, 000, 000, 0
                        //       , p_Folio, "Retiro", (int)p_TotalTransaccion, l_numbilletes, 0, 0, 0, 0, "", "Pesos", p_DetalleTransaccion, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

                        if (Prosegur.LeerAcumulado())
                        {
                            Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Layout creado y enviado con exito", 1);
                            
                        }
                        else
                        {

                            Globales.EscribirBitacora("Enviar Correo", "LeerACUMULADO", "Error en la creación, o en el envio del Layout", 1);
                        }
                    }
                }
               
            }
            else if (p_TipoTransaccion == 3)
            {
                if (p_Insertar)
                {

                    if (!BDCorte.InsertarCorte(p_IdUsuario, out p_Folio))
                        return false;
                }
                //Armar cadena envío
              
            }
            else
            {
              string  l_Alerta = p_IdCuenta;
                if (p_Insertar)
                {
                    // Insertar Mensaje
                    if (!BDAlerta.InsertarAlerta(l_Alerta, out p_Folio))
                        return false;
                }
                //Armar cadena envío
               
            }

            Prosegur.LeerTransacciones(); 
            return true;
        }

        private static String m_FIDSerialNumber;

        private static String m_ContractNumber;

        private static String m_ProviderId;

        private static String m_InstallationId;

        private static String m_SystemUserId;

        private static Int64 m_ProcessId;

        private static String m_CryptKey1;

        private static String m_CryptKey2;

    }
}
