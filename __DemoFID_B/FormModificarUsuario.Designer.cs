﻿namespace __DemoFID_B
{
    partial class FormModificarUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Usuarios = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Modificar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_confirmacionContraseña = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Contraseña = new System.Windows.Forms.TextBox();
            this.c_perfiles = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.c_referencia = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.c_convenio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Usuarios
            // 
            this.c_Usuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Usuarios.FormattingEnabled = true;
            this.c_Usuarios.Location = new System.Drawing.Point(242, 128);
            this.c_Usuarios.Name = "c_Usuarios";
            this.c_Usuarios.Size = new System.Drawing.Size(180, 24);
            this.c_Usuarios.TabIndex = 27;
            this.c_Usuarios.SelectedIndexChanged += new System.EventHandler(this.c_Usuarios_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(173, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 34;
            this.label7.Text = "Perfil";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 16);
            this.label6.TabIndex = 35;
            this.label6.Text = "Nombre de EMPLEADO";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(242, 203);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(346, 22);
            this.c_Nombre.TabIndex = 28;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(186, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(235, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Seleccione un usuario / ETV";
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(344, 306);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 30;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Modificar
            // 
            this.c_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Modificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Modificar.Location = new System.Drawing.Point(113, 306);
            this.c_Modificar.Name = "c_Modificar";
            this.c_Modificar.Size = new System.Drawing.Size(122, 47);
            this.c_Modificar.TabIndex = 29;
            this.c_Modificar.Text = "Modificar";
            this.c_Modificar.UseVisualStyleBackColor = true;
            this.c_Modificar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "MODOFICAR OPERADOR";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 20);
            this.label1.TabIndex = 33;
            this.label1.Text = "Numero de EMPLEADO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(212, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 16);
            this.label3.TabIndex = 39;
            this.label3.Text = "Confirme Contraseña";
            this.label3.Visible = false;
            // 
            // c_confirmacionContraseña
            // 
            this.c_confirmacionContraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_confirmacionContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmacionContraseña.Location = new System.Drawing.Point(415, 73);
            this.c_confirmacionContraseña.Name = "c_confirmacionContraseña";
            this.c_confirmacionContraseña.PasswordChar = '*';
            this.c_confirmacionContraseña.Size = new System.Drawing.Size(180, 22);
            this.c_confirmacionContraseña.TabIndex = 37;
            this.c_confirmacionContraseña.Visible = false;
            this.c_confirmacionContraseña.Click += new System.EventHandler(this.c_confirmacionContraseña_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(275, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "Contraseña";
            this.label2.Visible = false;
            // 
            // c_Contraseña
            // 
            this.c_Contraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_Contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Contraseña.Location = new System.Drawing.Point(416, 49);
            this.c_Contraseña.Name = "c_Contraseña";
            this.c_Contraseña.PasswordChar = '*';
            this.c_Contraseña.Size = new System.Drawing.Size(180, 22);
            this.c_Contraseña.TabIndex = 36;
            this.c_Contraseña.Visible = false;
            this.c_Contraseña.Click += new System.EventHandler(this.c_Contraseña_Click);
            // 
            // c_perfiles
            // 
            this.c_perfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_perfiles.FormattingEnabled = true;
            this.c_perfiles.Location = new System.Drawing.Point(241, 165);
            this.c_perfiles.Name = "c_perfiles";
            this.c_perfiles.Size = new System.Drawing.Size(180, 24);
            this.c_perfiles.TabIndex = 40;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(496, 275);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(140, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 43;
            this.label8.Text = "Referencia";
            // 
            // c_referencia
            // 
            this.c_referencia.BackColor = System.Drawing.SystemColors.Info;
            this.c_referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_referencia.Location = new System.Drawing.Point(241, 233);
            this.c_referencia.Name = "c_referencia";
            this.c_referencia.Size = new System.Drawing.Size(249, 22);
            this.c_referencia.TabIndex = 42;
            this.c_referencia.Click += new System.EventHandler(this.c_referencia_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(150, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 16);
            this.label9.TabIndex = 45;
            this.label9.Text = "Convenio";
            // 
            // c_convenio
            // 
            this.c_convenio.BackColor = System.Drawing.SystemColors.Info;
            this.c_convenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_convenio.Location = new System.Drawing.Point(241, 262);
            this.c_convenio.Name = "c_convenio";
            this.c_convenio.Size = new System.Drawing.Size(249, 22);
            this.c_convenio.TabIndex = 44;
            this.c_convenio.Click += new System.EventHandler(this.c_convenio_Click);
            // 
            // FormModificarUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 370);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.c_convenio);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.c_referencia);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_perfiles);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_confirmacionContraseña);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Contraseña);
            this.Controls.Add(this.c_Usuarios);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Modificar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormModificarUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormModificarUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox c_Usuarios;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Modificar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_confirmacionContraseña;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Contraseña;
        private System.Windows.Forms.ComboBox c_perfiles;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox c_referencia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox c_convenio;
    }
}