﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormImportCliente : Form
    {
        public FormImportCliente()
        {
            InitializeComponent();
        }



        private void c_PathChoose_Click(object sender, EventArgs e)
        {
            int l_Count = 0;
            string line;

            if (Client_File.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader l_File = new StreamReader(Client_File.FileName);

                while ((line = l_File.ReadLine()) != null)
                {
                    string[] ClientData = line.Split('|');

                    string l_ClaveCliente, l_Nombre_Cliente, l_IdOperadro, l_Password, l_Cuenta, l_Banco;

                    l_ClaveCliente = ClientData[0];
                    l_Nombre_Cliente = ClientData[1];
                    l_IdOperadro = ClientData[2];
                    l_Password = ClientData[3];
                    l_Banco = ClientData[4];
                    l_Cuenta = ClientData[5];

                    // insert into client

                    BDCliente.Insertar(l_ClaveCliente, l_Nombre_Cliente, null);

                    // insert into Cuenta
                    BDCuentaOperador.Insertar2(l_Banco, l_Banco, l_Cuenta, true, true, l_ClaveCliente);

                    // insert into usario

                    BDOperador.Insertar1(l_IdOperadro, l_Nombre_Cliente, null, l_Password, true, l_ClaveCliente);


                    l_Count++;
                }




                MessageBox.Show(l_Count.ToString(), "Number of Client Added");
                this.Close();

            }
        }


        private void FormImportCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
