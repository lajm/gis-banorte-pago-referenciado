﻿namespace __DemoFID_B
{
    partial class FormTeclado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_keyboard = new KeyboardClassLibrary.Keyboardcontrol();
            this.c_Texto = new System.Windows.Forms.TextBox();
            this.c_salir = new System.Windows.Forms.Button();
            this.c_cambiar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c_keyboard
            // 
            this.c_keyboard.KeyboardType = KeyboardClassLibrary.BoW.Standard;
            this.c_keyboard.Location = new System.Drawing.Point(0, 33);
            this.c_keyboard.Name = "c_keyboard";
            this.c_keyboard.Size = new System.Drawing.Size(946, 367);
            this.c_keyboard.TabIndex = 0;
            this.c_keyboard.UserKeyPressed += new KeyboardClassLibrary.KeyboardDelegate(this.keyboardcontrol1_UserKeyPressed);
            // 
            // c_Texto
            // 
            this.c_Texto.BackColor = System.Drawing.SystemColors.Info;
            this.c_Texto.Dock = System.Windows.Forms.DockStyle.Top;
            this.c_Texto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Texto.Location = new System.Drawing.Point(0, 0);
            this.c_Texto.Name = "c_Texto";
            this.c_Texto.Size = new System.Drawing.Size(780, 31);
            this.c_Texto.TabIndex = 1;
            this.c_Texto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.c_Texto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.c_Texto_KeyDown);
            // 
            // c_salir
            // 
            this.c_salir.Location = new System.Drawing.Point(25, 345);
            this.c_salir.Name = "c_salir";
            this.c_salir.Size = new System.Drawing.Size(124, 38);
            this.c_salir.TabIndex = 2;
            this.c_salir.Text = "Salir, Cerrar";
            this.c_salir.UseVisualStyleBackColor = true;
            this.c_salir.Click += new System.EventHandler(this.c_salir_Click);
            // 
            // c_cambiar
            // 
            this.c_cambiar.Location = new System.Drawing.Point(626, 345);
            this.c_cambiar.Name = "c_cambiar";
            this.c_cambiar.Size = new System.Drawing.Size(124, 38);
            this.c_cambiar.TabIndex = 3;
            this.c_cambiar.Text = "Aceptar";
            this.c_cambiar.UseVisualStyleBackColor = true;
            this.c_cambiar.Click += new System.EventHandler(this.c_cambiar_Click);
            // 
            // FormTeclado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 400);
            this.Controls.Add(this.c_cambiar);
            this.Controls.Add(this.c_salir);
            this.Controls.Add(this.c_Texto);
            this.Controls.Add(this.c_keyboard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormTeclado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormTeclado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KeyboardClassLibrary.Keyboardcontrol c_keyboard;
        private System.Windows.Forms.TextBox c_Texto;
        private System.Windows.Forms.Button c_salir;
        private System.Windows.Forms.Button c_cambiar;
    }
}