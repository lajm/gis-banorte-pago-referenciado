﻿using SidApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormSesion : Form
    {
        String l_Password = "";
        public String l_IdUsuario;

        public FormSesion()
        {
            InitializeComponent();
           
        }

        private void OprimirTecla(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Password += "0";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Password += "1";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Password += "2";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Password += "3";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Password += "4";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Password += "5";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Password += "6";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Password += "7";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Password += "8";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Password += "9";
                c_EtiquetaPassword.Text += "*";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Password.Length != 0)
                {
                    l_Password = l_Password.Substring(0, l_Password.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Password.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {

                Globales.EmpresaCliente = null;
                Globales.AliasCuenta = null;

                Globales.Banco = null;
                Globales.CuentaBanco = null;
                Globales.NombreCliente = null;
                Globales.IdCliente = null;


             


                  


                if (l_Password.Length > 0)
                {
                    if (!Globales.AutenticarUsuario(l_IdUsuario,l_Password))
                    {
                        


                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Usuario/Contraseña Inválido";
                            f_Error.ShowDialog();
                        }
                        Close();
                    }
                    else
                    {
                        

                        Globales.EscribirBitacora("LOGIN", " NUMERO " + Globales.IdUsuario + " NOMBRE: " + Globales.NombreUsuario.ToUpper(), "EXITOSO",1);

                        if (Globales.IdTipoUsuario == 1)
                        {

                            Globales.EmpresaCliente = Globales.NombreCliente;
                            Globales.AliasCuenta = Globales.NombreCliente;
                            if (true)//l_IdMoneda == 1)
                            {
                                int lRet;
                              int   l_Reintentos = 0;
                                try
                                {
                                    do
                                    {
                                        if (l_Reintentos >= 1) //mayor que cero para cambiar BIN
                                            break;
                                        lRet = SidLib.ResetError();
                                        l_Reintentos++;
                                    } while (lRet != 0);
                                }
                                catch (Exception ex)
                                {
                                }

                                using (FormDeposito f_Deposito = new FormDeposito())
                                {

                                    f_Deposito.Equipo = "Equipo1";
                                    // Globales.CambiaEquipo(l_Equipo1);

                                    //UtilsComunicacion.MantenerVivo();
                                    f_Deposito.ShowDialog();

                                    Close();
                                }
                            }
                        }
                        else
                            using (FormOpciones f_Opciones = new FormOpciones())
                            {
                                f_Opciones.ShowDialog();
                            }
                        Close();
                    }
                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir una contraseña";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
            //for (int i = 0; i < l_Password.Length; i++)
        }

        private void FormIniciarSesion_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
        }
    }
}
