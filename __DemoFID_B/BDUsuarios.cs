﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace __DemoFID_B
{
    public class BDUsuarios
    {
        public static bool
            DesactivarUsuarios()
        {
            String l_Query
                = "UPDATE                   Usuario "
                + "SET                      Activo = 0 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Desactivar Usuarios", "Error Conexion: ", ex.Message , 1);
                    return false;
                }
            }
            return true;
        }

        public static bool
         DesactivarUsuario(String p_Usuario)
        {
            String l_Query
                = "UPDATE                   Usuario "
                + "SET                      Activo = 0 "
                + "WHERE IdUsuario=@IdUsuario";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_Usuario);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Desactivar Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }
            return true;
        }

        public static bool
        DeleteUsuario(String p_Usuario)
        {
            String l_Query
                = "DELETE                  Usuario "
                + "WHERE IdUsuario=@IdUsuario";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_Usuario);
                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Delete Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }
            return true;
        }

        public static bool
            ValidarUsuarioExistente(String p_UserId, int p_IdPerfil, String p_Nombre)
        {
            String l_Query
                = "SELECT                   Activo "
                + "FROM                     Usuario "
                + "WHERE                    IdUsuario = @IdUsuario ";
            
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                    return InsertarUsuario(p_UserId, p_IdPerfil, p_Nombre);
                else
                {
                    if (!(bool)l_Lector[0])
                        return ActualizarUsuario(p_UserId, p_IdPerfil, p_Nombre);
                    else
                        return true;
                }
            }
        }

        public static bool
            ValidarUsuarioExistente(String p_UserId, int p_IdPerfil)
        {
            String l_Query
                = "SELECT                   Activo "
                + "FROM                     Usuario "
                + "WHERE                    IdUsuario = @IdUsuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                    return false;
                else
                {
                    return (bool)l_Lector[0];
                }
            }
        }

        public static bool
            InsertarUsuario(String p_UserId, int p_IdPerfil, String p_Nombre)
        {
            String l_Query
                = "INSERT INTO              Usuario "
                + "                         (IdUsuario, IdPerfil, Password, Nombre, Activo) "
                + "VALUES                   (@IdUsuario, @IdPerfil, @Password, @Nombre, 1) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Password", p_UserId);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Insertar Usuario", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
          InsertarUsuario(String p_UserId, int p_IdPerfil, String p_Nombre, String p_Pass)
        {
            String l_Query
                = "INSERT INTO              Usuario"
                + "                         (IdUsuario, IdPerfil, Password, Nombre, Activo)"
                + "VALUES                   (@IdUsuario, @IdPerfil, @Password, @Nombre, 1)";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Password", p_Pass);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Insertar Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

       public static void CuentaAsociada(string p_IdUsuario, out string l_banco, out string l_cuentabanco, out string l_cliente,out string l_idcliente)
        {
            String l_Query
                 = "SELECT       Cliente.Nombre, Cliente.ClaveCliente,Cuenta.Banco, Cuenta.Cuenta "
                   + "FROM Cliente INNER JOIN "
                   + "Cuenta ON Cliente.ClaveCliente = Cuenta.ClaveCliente INNER JOIN "
                   + "Usuario ON Cliente.ClaveCliente = Usuario.ClaveCliente "
                   + "WHERE Usuario.IdUsuario = @IdUsuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
              
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                {
                    l_cliente = l_Lector[0].ToString();
                    l_idcliente = l_Lector[1].ToString() ;
                    l_banco = l_Lector[2].ToString();
                    l_cuentabanco = l_Lector[3].ToString();
                    return ;
                }
                else
                {
                    l_cliente = "";
                    l_idcliente = "";
                    l_banco = "";
                    l_cuentabanco = "";
                    return ;
                }


            }
        }

        public static bool
         InsertarUsuario(String p_UserId, int p_IdPerfil, String p_Nombre,String p_Referencia, String p_Convenio , String p_Pass)
        {
            String l_Query
                = "INSERT INTO              Usuario"
                + "                         (IdUsuario, IdPerfil, Password, Nombre, Referencia,ConvenioDEM, Activo)"
                + "VALUES                   (@IdUsuario, @IdPerfil, @Password, @Nombre, @Referencia,@ConvenioDEM,1)";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Password", p_Pass);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);
                l_Comando.Parameters.AddWithValue("@Referencia", p_Referencia);
                l_Comando.Parameters.AddWithValue("@ConvenioDEM", p_Convenio);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Insertar Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

      

        public static bool
            ActualizarUsuario(String p_UserId, int p_IdPerfil, String p_Nombre, String p_referencia, String p_convenio)
        {
            String l_Query
                = "UPDATE                   Usuario "
                + "SET                      Activo = 1, "
                + "                         IdPerfil = @IdPerfil, "
                + "                         Nombre = @Nombre, "
                + "                         Referencia = @Referencia, "
                + "                         ConvenioDEM = @Convenio "
                + "WHERE                    IdUsuario = @IdUsuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);
                l_Comando.Parameters.AddWithValue("@Referencia", p_referencia);
                l_Comando.Parameters.AddWithValue("@Convenio", p_convenio);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Actualizar  Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
           ActualizarUsuario(String p_UserId, int p_IdPerfil, String p_Nombre )
        {
            String l_Query
                = "UPDATE                   Usuario "
                + "SET                      Activo = 1, "
                + "                         IdPerfil = @IdPerfil, "
                + "                         Nombre = @Nombre "
                + "WHERE                    IdUsuario = @IdUsuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Nombre", p_Nombre);
              

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Actualizar  Usuarios", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static bool
            IniciarSesion(String p_IdUsuario, String p_Password, out String p_Nombre, out int p_IdPerfil)
        {
            String l_Query
                = "SELECT               IdPerfil, Nombre  "
                + "FROM                 Usuario "
                + "WHERE                IdUsuario = @IdUsuario "
                + "AND                  Password = @Password "
                + "AND                  Activo = 1 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
                l_Comando.Parameters.AddWithValue("@Password", p_Password);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                {
                    p_Nombre = l_Lector[1].ToString();
                    p_IdPerfil = (int)l_Lector[0];
                    return true;
                }
                else
                {
                    p_Nombre = "";
                    p_IdPerfil = 0;
                    return false;
                }


            }
        }

        public static bool
           IniciarSesion(String p_IdUsuario, String p_Password, out String p_Nombre, out int p_IdPerfil
                         ,out String p_Referencia,out String p_ConvenioDEM,out String p_ConvenioCIE
                        ,out String p_Zona, out String p_Agencia)
        {
            String l_Query
                = "SELECT               IdPerfil, Nombre, Referencia, "
                + "                     ConvenioDEM, ConvenioCIE, Zona, Agencia "
                + "FROM                 Usuario "
                + "WHERE                IdUsuario = @IdUsuario "
                + "AND                  Password = @Password "
                + "AND                  Activo = 1 ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
                l_Comando.Parameters.AddWithValue("@Password", p_Password);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                {
                    p_Nombre = l_Lector[1].ToString();
                    p_IdPerfil = (int)l_Lector[0];
                    p_Referencia = l_Lector[2].ToString();
                    p_ConvenioDEM = l_Lector[3].ToString();
                    p_ConvenioCIE = l_Lector[4].ToString();
                    p_Agencia = l_Lector[5].ToString();
                    p_Zona = l_Lector[6].ToString();
                    return true;
                }
                else
                {
                    p_Nombre = "";
                    p_IdPerfil = 0;
                    p_Referencia = "";
                    p_ConvenioDEM = "";
                    p_ConvenioCIE = "";
                    p_Agencia = "";
                    p_Zona = "";
                    return false;
                }


            }
        }


        public static bool
        ActualizarContraseña(String p_UserId, int p_IdPerfil, String p_contraseña)
        {
            String l_Query
                = "UPDATE                   Usuario "
                + "SET                      Activo = 1,"
                + "                         IdPerfil = @IdPerfil,"
                + "                         Password = @Password "
                + "WHERE                    IdUsuario = @IdUsuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);
                l_Comando.Parameters.AddWithValue("@IdPerfil", p_IdPerfil);
                l_Comando.Parameters.AddWithValue("@Password", p_contraseña);

                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Actualizar Contraseña", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }


       

        public static int
            ContarUsuarios()
        {
            String l_Query
                = "SELECT           COUNT(*) "
                + "FROM             Usuario ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        return (int)l_Lector[0];
                    else
                        return 0;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Contar Usuarios", "Error Consulta : ", ex.Message, 1);
                    return 0;
                }
            }
        }

        public static bool
   BorrarUsuario(String p_UserId)
        {
            String l_Query
                = "Delete  FROM               Usuario"
                + "WHERE                    IdUsuario = @IdUsuario";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_UserId);


                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Borrar Usuario", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
                return true;
            }
        }

        public static DataTable Traerusuarios()
        {
            String l_query
                = "SELECT   Usuario.IdUsuario, Usuario.Password, Usuario.Nombre, Usuario.IdPerfil, Usuario.Referencia, Usuario.ConvenioDEM, Perfil.Descripcion AS Perfil "
                + "FROM Usuario "
                + "INNER JOIN Perfil ON "
                + " Usuario.IdPerfil = Perfil.IdPerfil "
                + "WHERE Usuario.Activo=1 AND Usuario.idPerfil != 3 ";


            using (SqlConnection l_conex = UtilsBD.NuevaConexion())
            {
                SqlCommand l_comand = new SqlCommand(l_query, l_conex);
                SqlDataAdapter l_adap = new SqlDataAdapter(l_comand);
                DataTable l_tabla = new DataTable("Usuarios");

                try
                {
                    l_adap.Fill(l_tabla);
                    return l_tabla;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Traer Usuarios", "Error Consulta : ", ex.Message, 1);
                    return null;
                }


            }

        }

        public static DataTable TraerOperadores()
        {
            String l_query
                = "SELECT   Usuario.IdUsuario, Usuario.Password, Usuario.Nombre, Usuario.IdPerfil, Perfil.Descripcion AS Perfil "
                + "FROM Usuario "
                + "INNER JOIN Perfil ON "
                + " Usuario.IdPerfil = Perfil.IdPerfil "
                + "WHERE Usuario.Activo=1 AND Usuario.idPerfil=1";


            using (SqlConnection l_conex = UtilsBD.NuevaConexion())
            {
                SqlCommand l_comand = new SqlCommand(l_query, l_conex);
                SqlDataAdapter l_adap = new SqlDataAdapter(l_comand);
                DataTable l_tabla = new DataTable("Operadores");

                try
                {
                    l_adap.Fill(l_tabla);
                    return l_tabla;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Traer Usuarios Operadores", "Error Consulta : ", ex.Message, 1);
                    return null;
                }


            }

        }

    }
}
