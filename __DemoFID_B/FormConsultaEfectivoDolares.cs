﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormConsultaEfectivoDolares : Form
    {
        Double l_TotalCajon1 = 0;
        Double l_TotalCajon2 = 0;
        Double l_TotalCajon3 = 0;
        Double l_TotalCajon4 = 0;

        public FormConsultaEfectivoDolares()
        {
            InitializeComponent();
        }

        private void FormConsultaEfectivoDolares_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            DataTable l_Datos = BDConsulta.ObtenerConsultaDepositos(9);
            if (l_Datos.Rows.Count < 1)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "No hay depósitos en dólares";
                    f_Error.ShowDialog();
                    Close();
                }
            }

            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
                if (l_Detalle[1].ToString().Trim() == "100")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 100;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 100;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 100;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 100;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "50")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 50;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 50;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 50;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 50;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "20")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 20;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 20;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 20;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 20;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "10")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 10;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 10;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 10;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_10.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 10;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "5")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 5;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 5;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 5;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_5.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 5;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "2")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0] * 2;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0] * 2;
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0] * 2;
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_2.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0] * 2;
                    }
                }
                if (l_Detalle[1].ToString().Trim() == "1")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon1_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon1 += (int)l_Detalle[0];
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon2_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon2 += (int)l_Detalle[0];
                    }
                    else if (l_Detalle[2].ToString() == "3")
                    {
                        c_Cajon3_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon3 += (int)l_Detalle[0];
                    }
                    else if (l_Detalle[2].ToString() == "2")
                    {
                        c_Cajon4_1.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalCajon4 += (int)l_Detalle[0];
                    }
                }

            }
            c_TotalCajon1.Text = l_TotalCajon1.ToString("#,###,###,##0.00");
            c_TotalCajon2.Text = l_TotalCajon2.ToString("#,###,###,##0.00");
            c_TotalCajon3.Text = l_TotalCajon3.ToString("#,###,###,##0.00");
            c_TotalCajon4.Text = l_TotalCajon4.ToString("#,###,###,##0.00");
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();
        }

        private void c_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Imprimir_Click(object sender, EventArgs e)
        {
            if (l_TotalCajon1 > 0)
            {
                ModulePrint imprimir = new ModulePrint();
                imprimir.AddHeaderLine("Consulta de Efectivo");
                imprimir.AddHeaderLine("Moneda: Dólares");
                imprimir.AddHeaderLine("Cajón: 1");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
                imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());

                imprimir.AddItem(c_Cajon1_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon1_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon1_20.Text, "$20.00", "20");
                imprimir.AddItem(c_Cajon1_10.Text, "$10.00", "10");
                imprimir.AddItem(c_Cajon1_5.Text, "$5.00", "5");
                imprimir.AddItem(c_Cajon1_2.Text, "$2.00", "2");
                imprimir.AddItem(c_Cajon1_1.Text, "$1.00", "1");
                imprimir.AddFooterLine("Total: " + c_TotalCajon1.Text);
                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }

            if (l_TotalCajon2 > 0)
            {
                ModulePrint imprimir = new ModulePrint();
                imprimir.AddHeaderLine("Consulta de Efectivo");
                imprimir.AddHeaderLine("Moneda: Dólares");
                imprimir.AddHeaderLine("Cajón: 2");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
                imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());

                imprimir.AddItem(c_Cajon2_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon2_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon2_20.Text, "$20.00", "20");
                imprimir.AddItem(c_Cajon2_10.Text, "$10.00", "10");
                imprimir.AddItem(c_Cajon2_5.Text, "$5.00", "5");
                imprimir.AddItem(c_Cajon2_2.Text, "$2.00", "2");
                imprimir.AddItem(c_Cajon2_1.Text, "$1.00", "1");
                imprimir.AddFooterLine("Total: " + c_TotalCajon2.Text);
                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }

            if (l_TotalCajon3 > 0)
            {
                ModulePrint imprimir = new ModulePrint();
                imprimir.AddHeaderLine("Consulta de Efectivo");
                imprimir.AddHeaderLine("Moneda: Dólares");
                imprimir.AddHeaderLine("Cajón: 3");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
                imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());

                imprimir.AddItem(c_Cajon3_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon3_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon3_20.Text, "$20.00", "20");
                imprimir.AddItem(c_Cajon3_10.Text, "$10.00", "10");
                imprimir.AddItem(c_Cajon3_5.Text, "$5.00", "5");
                imprimir.AddItem(c_Cajon3_2.Text, "$2.00", "2");
                imprimir.AddItem(c_Cajon3_1.Text, "$1.00", "1");
                imprimir.AddFooterLine("Total: " + c_TotalCajon3.Text);
                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }

            if (l_TotalCajon4 > 0)
            {
                ModulePrint imprimir = new ModulePrint();
                imprimir.AddHeaderLine("Consulta de Efectivo");
                imprimir.AddHeaderLine("Moneda: Dólares");
                imprimir.AddHeaderLine("Cajón: 4");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy"));
                imprimir.AddSubHeaderLine("Hora: " + DateTime.Now.ToShortTimeString());

                imprimir.AddItem(c_Cajon4_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon4_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon4_20.Text, "$20.00", "20");
                imprimir.AddItem(c_Cajon4_10.Text, "$10.00", "10");
                imprimir.AddItem(c_Cajon4_5.Text, "$5.00", "5");
                imprimir.AddItem(c_Cajon4_2.Text, "$2.00", "2");
                imprimir.AddItem(c_Cajon4_1.Text, "$1.00", "1");
                imprimir.AddFooterLine("Total: " + c_TotalCajon4.Text);
                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }
            Close();
        }

    }
}
