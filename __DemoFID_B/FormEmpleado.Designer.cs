﻿namespace __DemoFID_B
{
    partial class FormEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmpleado));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BackSpace = new System.Windows.Forms.Button();
            this.c_EtiquetaPassword = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_Boton0 = new System.Windows.Forms.Button();
            this.c_Boton9 = new System.Windows.Forms.Button();
            this.c_Boton8 = new System.Windows.Forms.Button();
            this.c_Boton6 = new System.Windows.Forms.Button();
            this.c_Boton5 = new System.Windows.Forms.Button();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_Boton7 = new System.Windows.Forms.Button();
            this.c_Boton3 = new System.Windows.Forms.Button();
            this.c_Boton2 = new System.Windows.Forms.Button();
            this.c_Boton4 = new System.Windows.Forms.Button();
            this.c_Boton1 = new System.Windows.Forms.Button();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_BackSpace);
            this.panel1.Controls.Add(this.c_EtiquetaPassword);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Controls.Add(this.c_Boton0);
            this.panel1.Controls.Add(this.c_Boton9);
            this.panel1.Controls.Add(this.c_Boton8);
            this.panel1.Controls.Add(this.c_Boton6);
            this.panel1.Controls.Add(this.c_Boton5);
            this.panel1.Controls.Add(this.c_BotonCancelar);
            this.panel1.Controls.Add(this.c_Boton7);
            this.panel1.Controls.Add(this.c_Boton3);
            this.panel1.Controls.Add(this.c_Boton2);
            this.panel1.Controls.Add(this.c_Boton4);
            this.panel1.Controls.Add(this.c_Boton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(171, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(449, 29);
            this.label1.TabIndex = 26;
            this.label1.Text = "INGRESE SU NUMERO DE EMPLEADO";
            // 
            // c_BackSpace
            // 
            this.c_BackSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BackSpace.Image = global::@__DemoFID_B.Properties.Resources.back;
            this.c_BackSpace.Location = new System.Drawing.Point(156, 433);
            this.c_BackSpace.Name = "c_BackSpace";
            this.c_BackSpace.Size = new System.Drawing.Size(150, 63);
            this.c_BackSpace.TabIndex = 25;
            this.c_BackSpace.TabStop = false;
            this.c_BackSpace.Text = "Borrar";
            this.c_BackSpace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BackSpace.UseVisualStyleBackColor = true;
            this.c_BackSpace.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_EtiquetaPassword
            // 
            this.c_EtiquetaPassword.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaPassword.Location = new System.Drawing.Point(252, 498);
            this.c_EtiquetaPassword.Name = "c_EtiquetaPassword";
            this.c_EtiquetaPassword.Size = new System.Drawing.Size(297, 39);
            this.c_EtiquetaPassword.TabIndex = 24;
            this.c_EtiquetaPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_BotonAceptar.Location = new System.Drawing.Point(682, 258);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(115, 72);
            this.c_BotonAceptar.TabIndex = 23;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton0
            // 
            this.c_Boton0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton0.Location = new System.Drawing.Point(332, 432);
            this.c_Boton0.Name = "c_Boton0";
            this.c_Boton0.Size = new System.Drawing.Size(150, 63);
            this.c_Boton0.TabIndex = 22;
            this.c_Boton0.TabStop = false;
            this.c_Boton0.Text = "0";
            this.c_Boton0.UseVisualStyleBackColor = true;
            this.c_Boton0.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton9
            // 
            this.c_Boton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton9.Location = new System.Drawing.Point(507, 336);
            this.c_Boton9.Name = "c_Boton9";
            this.c_Boton9.Size = new System.Drawing.Size(150, 90);
            this.c_Boton9.TabIndex = 21;
            this.c_Boton9.TabStop = false;
            this.c_Boton9.Text = "9";
            this.c_Boton9.UseVisualStyleBackColor = true;
            this.c_Boton9.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton8
            // 
            this.c_Boton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton8.Location = new System.Drawing.Point(332, 336);
            this.c_Boton8.Name = "c_Boton8";
            this.c_Boton8.Size = new System.Drawing.Size(150, 90);
            this.c_Boton8.TabIndex = 20;
            this.c_Boton8.TabStop = false;
            this.c_Boton8.Text = "8";
            this.c_Boton8.UseVisualStyleBackColor = true;
            this.c_Boton8.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton6
            // 
            this.c_Boton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton6.Location = new System.Drawing.Point(507, 240);
            this.c_Boton6.Name = "c_Boton6";
            this.c_Boton6.Size = new System.Drawing.Size(150, 90);
            this.c_Boton6.TabIndex = 19;
            this.c_Boton6.TabStop = false;
            this.c_Boton6.Text = "6";
            this.c_Boton6.UseVisualStyleBackColor = true;
            this.c_Boton6.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton5
            // 
            this.c_Boton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton5.Location = new System.Drawing.Point(332, 240);
            this.c_Boton5.Name = "c_Boton5";
            this.c_Boton5.Size = new System.Drawing.Size(150, 90);
            this.c_Boton5.TabIndex = 18;
            this.c_Boton5.TabStop = false;
            this.c_Boton5.Text = "5";
            this.c_Boton5.UseVisualStyleBackColor = true;
            this.c_Boton5.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Image = global::@__DemoFID_B.Properties.Resources.block;
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.c_BotonCancelar.Location = new System.Drawing.Point(507, 432);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(150, 63);
            this.c_BotonCancelar.TabIndex = 17;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Salir";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton7
            // 
            this.c_Boton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton7.Location = new System.Drawing.Point(156, 336);
            this.c_Boton7.Name = "c_Boton7";
            this.c_Boton7.Size = new System.Drawing.Size(150, 90);
            this.c_Boton7.TabIndex = 16;
            this.c_Boton7.TabStop = false;
            this.c_Boton7.Text = "7";
            this.c_Boton7.UseVisualStyleBackColor = true;
            this.c_Boton7.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton3
            // 
            this.c_Boton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton3.Location = new System.Drawing.Point(507, 144);
            this.c_Boton3.Name = "c_Boton3";
            this.c_Boton3.Size = new System.Drawing.Size(150, 90);
            this.c_Boton3.TabIndex = 15;
            this.c_Boton3.TabStop = false;
            this.c_Boton3.Text = "3";
            this.c_Boton3.UseVisualStyleBackColor = true;
            this.c_Boton3.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton2
            // 
            this.c_Boton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton2.Location = new System.Drawing.Point(332, 144);
            this.c_Boton2.Name = "c_Boton2";
            this.c_Boton2.Size = new System.Drawing.Size(150, 90);
            this.c_Boton2.TabIndex = 14;
            this.c_Boton2.TabStop = false;
            this.c_Boton2.Text = "2";
            this.c_Boton2.UseVisualStyleBackColor = true;
            this.c_Boton2.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton4
            // 
            this.c_Boton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton4.Location = new System.Drawing.Point(156, 240);
            this.c_Boton4.Name = "c_Boton4";
            this.c_Boton4.Size = new System.Drawing.Size(150, 90);
            this.c_Boton4.TabIndex = 13;
            this.c_Boton4.TabStop = false;
            this.c_Boton4.Text = "4";
            this.c_Boton4.UseVisualStyleBackColor = true;
            this.c_Boton4.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // c_Boton1
            // 
            this.c_Boton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton1.Location = new System.Drawing.Point(156, 144);
            this.c_Boton1.Name = "c_Boton1";
            this.c_Boton1.Size = new System.Drawing.Size(150, 90);
            this.c_Boton1.TabIndex = 12;
            this.c_Boton1.TabStop = false;
            this.c_Boton1.Text = "1";
            this.c_Boton1.UseVisualStyleBackColor = true;
            this.c_Boton1.Click += new System.EventHandler(this.Oprimir_tecla);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 3;
            // 
            // FormEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEmpleado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormTameme";
            this.Load += new System.EventHandler(this.FormTameme_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_BackSpace;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Button c_Boton0;
        private System.Windows.Forms.Button c_Boton8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label c_EtiquetaPassword;
        private System.Windows.Forms.Button c_Boton9;
        private System.Windows.Forms.Button c_Boton6;
        private System.Windows.Forms.Button c_Boton5;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_Boton7;
        private System.Windows.Forms.Button c_Boton3;
        private System.Windows.Forms.Button c_Boton2;
        private System.Windows.Forms.Button c_Boton4;
        private System.Windows.Forms.Button c_Boton1;
        private uc_PanelInferior uc_PanelInferior1;
    }
}