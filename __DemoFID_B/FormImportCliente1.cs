﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormImportCliente1 : Form
    {
        public FormImportCliente1()
        {
            InitializeComponent();
        }



        private void c_PathChoose_Click(object sender, EventArgs e)
        {
            int l_Count = 0;
            string line;
                  

            if (Client_File.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try {
                    StreamReader l_File = new StreamReader(Client_File.FileName);

                    while ((line = l_File.ReadLine()) != null)
                    {
                        if (l_Count > 0)

                        {
                            string[] ClientData = line.Split(',');
                            // ClaveCliente,NombreCliente,BancoCuenta,Cuenta

                            string l_ClaveCliente, l_Nombre_Cliente, l_Cuenta, l_BancoCuenta;

                            l_ClaveCliente = ClientData[0];
                            l_Nombre_Cliente = ClientData[1];
                            l_BancoCuenta = ClientData[2];
                            l_Cuenta = ClientData[3];



                            // insert into usario

                            BDOperador.DelyInsert1(l_Cuenta, l_Nombre_Cliente, null, l_Cuenta, true, l_ClaveCliente);

                            // insert into Cuenta
                            BDCuentaOperador.DeleteyInsert(l_BancoCuenta, l_BancoCuenta, l_Cuenta, true, true, l_ClaveCliente);

                            // insert into client

                            BDCliente.ValidarClienteExistente(l_ClaveCliente, l_Nombre_Cliente, null);



                        }

                        l_Count++;
                    }
                }
                catch (Exception E)
                {
                    
                    throw;
                }

                int l_ClientAdd = l_Count - 1;
                String l_ClientMessage = "Número de clientes que se añadan : " + l_ClientAdd.ToString();
                MessageBox.Show(l_ClientMessage, "Importar Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
        }


        private void FormImportCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
