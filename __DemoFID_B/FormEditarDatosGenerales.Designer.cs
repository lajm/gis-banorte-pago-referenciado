﻿using System.Windows.Forms;

namespace __DemoFID_B
{
    partial class FormEditarDatosGenerales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Telephone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.c_Clave = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(363, 408);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 7;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_BAceptar
            // 
            this.c_BAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BAceptar.Location = new System.Drawing.Point(178, 408);
            this.c_BAceptar.Name = "c_BAceptar";
            this.c_BAceptar.Size = new System.Drawing.Size(122, 47);
            this.c_BAceptar.TabIndex = 6;
            this.c_BAceptar.Text = "Aceptar";
            this.c_BAceptar.UseVisualStyleBackColor = true;
            this.c_BAceptar.Click += new System.EventHandler(this.c_BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "EDITAR  DATOS DE CLIENTE";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Window;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(276, 202);
            this.c_Nombre.Margin = new System.Windows.Forms.Padding(2);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(349, 26);
            this.c_Nombre.TabIndex = 28;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(199, 202);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Nombre :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(177, 232);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Telephone :";
            // 
            // c_Telephone
            // 
            this.c_Telephone.BackColor = System.Drawing.SystemColors.Window;
            this.c_Telephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Telephone.Location = new System.Drawing.Point(276, 232);
            this.c_Telephone.Margin = new System.Windows.Forms.Padding(2);
            this.c_Telephone.Name = "c_Telephone";
            this.c_Telephone.Size = new System.Drawing.Size(165, 26);
            this.c_Telephone.TabIndex = 30;
            this.c_Telephone.Click += new System.EventHandler(this.c_Telephone_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(213, 167);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "Clave :";
            // 
            // c_Clave
            // 
            this.c_Clave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.c_Clave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Clave.Location = new System.Drawing.Point(273, 161);
            this.c_Clave.Margin = new System.Windows.Forms.Padding(2);
            this.c_Clave.Name = "c_Clave";
            this.c_Clave.ReadOnly = true;
            this.c_Clave.Size = new System.Drawing.Size(165, 26);
            this.c_Clave.TabIndex = 26;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(577, 408);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormEditarDatosGenerales
            // 
            this.AcceptButton = this.c_BAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.CancelButton = this.c_Cancelar;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_Telephone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Clave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_BAceptar);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditarDatosGenerales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormEditarDatosGeneralesCliente";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Telephone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_Clave;
        private string text1;
        private string text2;
        private TextBox c_Telefono;
        private PictureBox pictureBox1;
    }
}