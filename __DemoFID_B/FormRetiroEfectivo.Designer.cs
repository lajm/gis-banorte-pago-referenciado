﻿namespace __DemoFID_B
{
    partial class FormRetiroEfectivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRetiroEfectivo));
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_totalenDB = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.c_totalbilletesbolsa = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.c_20 = new System.Windows.Forms.Label();
            this.c_50 = new System.Windows.Forms.Label();
            this.c_100 = new System.Windows.Forms.Label();
            this.c_200 = new System.Windows.Forms.Label();
            this.c_500 = new System.Windows.Forms.Label();
            this.c_1000 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Cajon2_20 = new System.Windows.Forms.Label();
            this.c_Cajon2_50 = new System.Windows.Forms.Label();
            this.c_Cajon2_100 = new System.Windows.Forms.Label();
            this.c_Cajon2_200 = new System.Windows.Forms.Label();
            this.c_Cajon2_500 = new System.Windows.Forms.Label();
            this.c_Cajon2_1000 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_TotalRetiroEfectivo = new System.Windows.Forms.TextBox();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.c_totalenDB);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.c_totalbilletesbolsa);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.c_20);
            this.panel1.Controls.Add(this.c_50);
            this.panel1.Controls.Add(this.c_100);
            this.panel1.Controls.Add(this.c_200);
            this.panel1.Controls.Add(this.c_500);
            this.panel1.Controls.Add(this.c_1000);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.c_Cajon2_20);
            this.panel1.Controls.Add(this.c_Cajon2_50);
            this.panel1.Controls.Add(this.c_Cajon2_100);
            this.panel1.Controls.Add(this.c_Cajon2_200);
            this.panel1.Controls.Add(this.c_Cajon2_500);
            this.panel1.Controls.Add(this.c_Cajon2_1000);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_BotonCancelar);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Controls.Add(this.c_TotalRetiroEfectivo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 1;
            // 
            // c_totalenDB
            // 
            this.c_totalenDB.Enabled = false;
            this.c_totalenDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalenDB.ForeColor = System.Drawing.SystemColors.Info;
            this.c_totalenDB.Location = new System.Drawing.Point(219, 459);
            this.c_totalenDB.Name = "c_totalenDB";
            this.c_totalenDB.ReadOnly = true;
            this.c_totalenDB.Size = new System.Drawing.Size(128, 30);
            this.c_totalenDB.TabIndex = 77;
            this.c_totalenDB.TabStop = false;
            this.c_totalenDB.Text = "0";
            this.c_totalenDB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.c_totalenDB.TextChanged += new System.EventHandler(this.c_totalenDB_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 464);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 25);
            this.label12.TabIndex = 76;
            this.label12.Text = "Total Billetes :";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // c_totalbilletesbolsa
            // 
            this.c_totalbilletesbolsa.Enabled = false;
            this.c_totalbilletesbolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_totalbilletesbolsa.ForeColor = System.Drawing.SystemColors.Info;
            this.c_totalbilletesbolsa.Location = new System.Drawing.Point(643, 459);
            this.c_totalbilletesbolsa.Name = "c_totalbilletesbolsa";
            this.c_totalbilletesbolsa.ReadOnly = true;
            this.c_totalbilletesbolsa.Size = new System.Drawing.Size(128, 30);
            this.c_totalbilletesbolsa.TabIndex = 75;
            this.c_totalbilletesbolsa.TabStop = false;
            this.c_totalbilletesbolsa.Text = "0";
            this.c_totalbilletesbolsa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.c_totalbilletesbolsa.Visible = false;
            this.c_totalbilletesbolsa.TextChanged += new System.EventHandler(this.c_totalbilletesbolsa_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(436, 464);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 25);
            this.label11.TabIndex = 74;
            this.label11.Text = "Total Billetes en Bolsa:";
            this.label11.Visible = false;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(474, 169);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(218, 24);
            this.label17.TabIndex = 73;
            this.label17.Text = "Contenido Real en Bolsa";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // c_20
            // 
            this.c_20.BackColor = System.Drawing.Color.Transparent;
            this.c_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_20.Location = new System.Drawing.Point(507, 360);
            this.c_20.Name = "c_20";
            this.c_20.Size = new System.Drawing.Size(98, 24);
            this.c_20.TabIndex = 72;
            this.c_20.Text = "0";
            this.c_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_20.Click += new System.EventHandler(this.c_20_Click);
            // 
            // c_50
            // 
            this.c_50.BackColor = System.Drawing.Color.Transparent;
            this.c_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_50.Location = new System.Drawing.Point(507, 331);
            this.c_50.Name = "c_50";
            this.c_50.Size = new System.Drawing.Size(98, 24);
            this.c_50.TabIndex = 71;
            this.c_50.Text = "0";
            this.c_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_50.Click += new System.EventHandler(this.c_50_Click);
            // 
            // c_100
            // 
            this.c_100.BackColor = System.Drawing.Color.Transparent;
            this.c_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_100.Location = new System.Drawing.Point(507, 301);
            this.c_100.Name = "c_100";
            this.c_100.Size = new System.Drawing.Size(98, 24);
            this.c_100.TabIndex = 70;
            this.c_100.Text = "0";
            this.c_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_100.Click += new System.EventHandler(this.c_100_Click);
            // 
            // c_200
            // 
            this.c_200.BackColor = System.Drawing.Color.Transparent;
            this.c_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_200.Location = new System.Drawing.Point(507, 272);
            this.c_200.Name = "c_200";
            this.c_200.Size = new System.Drawing.Size(98, 24);
            this.c_200.TabIndex = 69;
            this.c_200.Text = "0";
            this.c_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_200.Click += new System.EventHandler(this.c_200_Click);
            // 
            // c_500
            // 
            this.c_500.BackColor = System.Drawing.Color.Transparent;
            this.c_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_500.Location = new System.Drawing.Point(507, 243);
            this.c_500.Name = "c_500";
            this.c_500.Size = new System.Drawing.Size(98, 24);
            this.c_500.TabIndex = 68;
            this.c_500.Text = "0";
            this.c_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_500.Click += new System.EventHandler(this.c_500_Click);
            // 
            // c_1000
            // 
            this.c_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_1000.Location = new System.Drawing.Point(507, 214);
            this.c_1000.Name = "c_1000";
            this.c_1000.Size = new System.Drawing.Size(98, 24);
            this.c_1000.TabIndex = 67;
            this.c_1000.Text = "0";
            this.c_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_1000.Click += new System.EventHandler(this.c_1000_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(395, 169);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 24);
            this.label10.TabIndex = 66;
            this.label10.Text = "Cantidad Esperada";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(260, 360);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 65;
            this.label8.Text = "$20.00";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(260, 331);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 24);
            this.label7.TabIndex = 64;
            this.label7.Text = "$50.00";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(250, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 63;
            this.label6.Text = "$100.00";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(250, 272);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 62;
            this.label5.Text = "$200.00";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(250, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 24);
            this.label4.TabIndex = 61;
            this.label4.Text = "$500.00";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(235, 214);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 60;
            this.label3.Text = "$1,000.00";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(235, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 24);
            this.label2.TabIndex = 59;
            this.label2.Text = "Denominación";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // c_Cajon2_20
            // 
            this.c_Cajon2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_20.Location = new System.Drawing.Point(415, 360);
            this.c_Cajon2_20.Name = "c_Cajon2_20";
            this.c_Cajon2_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_20.TabIndex = 57;
            this.c_Cajon2_20.Text = "0";
            this.c_Cajon2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_20.Click += new System.EventHandler(this.c_Cajon2_20_Click);
            // 
            // c_Cajon2_50
            // 
            this.c_Cajon2_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_50.Location = new System.Drawing.Point(415, 331);
            this.c_Cajon2_50.Name = "c_Cajon2_50";
            this.c_Cajon2_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_50.TabIndex = 56;
            this.c_Cajon2_50.Text = "0";
            this.c_Cajon2_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_50.Click += new System.EventHandler(this.c_Cajon2_50_Click);
            // 
            // c_Cajon2_100
            // 
            this.c_Cajon2_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_100.Location = new System.Drawing.Point(415, 301);
            this.c_Cajon2_100.Name = "c_Cajon2_100";
            this.c_Cajon2_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_100.TabIndex = 55;
            this.c_Cajon2_100.Text = "0";
            this.c_Cajon2_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_100.Click += new System.EventHandler(this.c_Cajon2_100_Click);
            // 
            // c_Cajon2_200
            // 
            this.c_Cajon2_200.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_200.Location = new System.Drawing.Point(415, 272);
            this.c_Cajon2_200.Name = "c_Cajon2_200";
            this.c_Cajon2_200.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_200.TabIndex = 54;
            this.c_Cajon2_200.Text = "0";
            this.c_Cajon2_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_200.Click += new System.EventHandler(this.c_Cajon2_200_Click);
            // 
            // c_Cajon2_500
            // 
            this.c_Cajon2_500.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_500.Location = new System.Drawing.Point(415, 243);
            this.c_Cajon2_500.Name = "c_Cajon2_500";
            this.c_Cajon2_500.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_500.TabIndex = 53;
            this.c_Cajon2_500.Text = "0";
            this.c_Cajon2_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_500.Click += new System.EventHandler(this.c_Cajon2_500_Click);
            // 
            // c_Cajon2_1000
            // 
            this.c_Cajon2_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_1000.Location = new System.Drawing.Point(415, 214);
            this.c_Cajon2_1000.Name = "c_Cajon2_1000";
            this.c_Cajon2_1000.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_1000.TabIndex = 52;
            this.c_Cajon2_1000.Text = "0";
            this.c_Cajon2_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.c_Cajon2_1000.Click += new System.EventHandler(this.c_Cajon2_1000_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(192, 402);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 25);
            this.label9.TabIndex = 46;
            this.label9.Text = "Total Retiro:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(266, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 29);
            this.label1.TabIndex = 38;
            this.label1.Text = "RETIRO DE EFECTIVO";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonCancelar.Location = new System.Drawing.Point(441, 495);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(205, 52);
            this.c_BotonCancelar.TabIndex = 32;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Cancelar";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(154, 495);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(208, 52);
            this.c_BotonAceptar.TabIndex = 31;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_TotalRetiroEfectivo
            // 
            this.c_TotalRetiroEfectivo.Enabled = false;
            this.c_TotalRetiroEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalRetiroEfectivo.ForeColor = System.Drawing.SystemColors.Info;
            this.c_TotalRetiroEfectivo.Location = new System.Drawing.Point(357, 402);
            this.c_TotalRetiroEfectivo.Name = "c_TotalRetiroEfectivo";
            this.c_TotalRetiroEfectivo.ReadOnly = true;
            this.c_TotalRetiroEfectivo.Size = new System.Drawing.Size(178, 30);
            this.c_TotalRetiroEfectivo.TabIndex = 30;
            this.c_TotalRetiroEfectivo.TabStop = false;
            this.c_TotalRetiroEfectivo.Text = "$ 0.00";
            this.c_TotalRetiroEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.c_TotalRetiroEfectivo.TextChanged += new System.EventHandler(this.c_TotalRetiroEfectivo_TextChanged);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 0;
            // 
            // FormRetiroEfectivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormRetiroEfectivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormRetiroEfectivo";
            this.Load += new System.EventHandler(this.FormRetiroEfectivo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_TotalRetiroEfectivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label c_Cajon2_20;
        private System.Windows.Forms.Label c_Cajon2_50;
        private System.Windows.Forms.Label c_Cajon2_100;
        private System.Windows.Forms.Label c_Cajon2_200;
        private System.Windows.Forms.Label c_Cajon2_500;
        private System.Windows.Forms.Label c_Cajon2_1000;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label c_20;
        private System.Windows.Forms.Label c_50;
        private System.Windows.Forms.Label c_100;
        private System.Windows.Forms.Label c_200;
        private System.Windows.Forms.Label c_500;
        private System.Windows.Forms.Label c_1000;
        private System.Windows.Forms.TextBox c_totalenDB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox c_totalbilletesbolsa;
        private System.Windows.Forms.Label label11;
    }
}