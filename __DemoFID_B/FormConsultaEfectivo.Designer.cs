﻿namespace __DemoFID_B
{
    partial class FormConsultaEfectivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsultaEfectivo));
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_numbilletes = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Imprimir = new System.Windows.Forms.Button();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_TotalCajon2 = new System.Windows.Forms.Label();
            this.c_Cajon2_20 = new System.Windows.Forms.Label();
            this.c_Cajon2_50 = new System.Windows.Forms.Label();
            this.c_Cajon2_100 = new System.Windows.Forms.Label();
            this.c_Cajon2_200 = new System.Windows.Forms.Label();
            this.c_Cajon2_500 = new System.Windows.Forms.Label();
            this.c_Cajon2_1000 = new System.Windows.Forms.Label();
            this.c_mov = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.c_mov);
            this.panel1.Controls.Add(this.c_numbilletes);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_Imprimir);
            this.panel1.Controls.Add(this.c_Cerrar);
            this.panel1.Controls.Add(this.c_TotalCajon2);
            this.panel1.Controls.Add(this.c_Cajon2_20);
            this.panel1.Controls.Add(this.c_Cajon2_50);
            this.panel1.Controls.Add(this.c_Cajon2_100);
            this.panel1.Controls.Add(this.c_Cajon2_200);
            this.panel1.Controls.Add(this.c_Cajon2_500);
            this.panel1.Controls.Add(this.c_Cajon2_1000);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // c_numbilletes
            // 
            this.c_numbilletes.BackColor = System.Drawing.Color.Transparent;
            this.c_numbilletes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numbilletes.Location = new System.Drawing.Point(402, 469);
            this.c_numbilletes.Name = "c_numbilletes";
            this.c_numbilletes.Size = new System.Drawing.Size(117, 24);
            this.c_numbilletes.TabIndex = 53;
            this.c_numbilletes.Text = "0";
            this.c_numbilletes.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(45, 469);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(325, 24);
            this.label9.TabIndex = 52;
            this.label9.Text = "NUMERO DE BILLETES EN TOTAL: ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(281, 426);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 24);
            this.label13.TabIndex = 51;
            this.label13.Text = "Totales:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(416, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(175, 24);
            this.label10.TabIndex = 48;
            this.label10.Text = "Contenido en Bolsa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(225, 393);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 46;
            this.label8.Text = "$20.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(225, 364);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 24);
            this.label7.TabIndex = 45;
            this.label7.Text = "$50.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(215, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 24);
            this.label6.TabIndex = 44;
            this.label6.Text = "$100.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(215, 305);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 24);
            this.label5.TabIndex = 43;
            this.label5.Text = "$200.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(215, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 24);
            this.label4.TabIndex = 42;
            this.label4.Text = "$500.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(200, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 41;
            this.label3.Text = "$1,000.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(200, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 24);
            this.label2.TabIndex = 40;
            this.label2.Text = "Denominación";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(246, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 29);
            this.label1.TabIndex = 39;
            this.label1.Text = "CONSULTA DE EFECTIVO";
            // 
            // c_Imprimir
            // 
            this.c_Imprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Imprimir.Location = new System.Drawing.Point(87, 506);
            this.c_Imprimir.Name = "c_Imprimir";
            this.c_Imprimir.Size = new System.Drawing.Size(143, 41);
            this.c_Imprimir.TabIndex = 38;
            this.c_Imprimir.TabStop = false;
            this.c_Imprimir.Text = "Imprimir";
            this.c_Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Imprimir.UseVisualStyleBackColor = true;
            this.c_Imprimir.Click += new System.EventHandler(this.c_Imprimir_Click);
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(571, 506);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(143, 41);
            this.c_Cerrar.TabIndex = 37;
            this.c_Cerrar.TabStop = false;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = true;
            this.c_Cerrar.Click += new System.EventHandler(this.c_Cerrar_Click);
            // 
            // c_TotalCajon2
            // 
            this.c_TotalCajon2.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon2.Location = new System.Drawing.Point(400, 426);
            this.c_TotalCajon2.Name = "c_TotalCajon2";
            this.c_TotalCajon2.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon2.TabIndex = 13;
            this.c_TotalCajon2.Text = "$0.00";
            this.c_TotalCajon2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_20
            // 
            this.c_Cajon2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_20.Location = new System.Drawing.Point(419, 393);
            this.c_Cajon2_20.Name = "c_Cajon2_20";
            this.c_Cajon2_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_20.TabIndex = 12;
            this.c_Cajon2_20.Text = "0";
            this.c_Cajon2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_50
            // 
            this.c_Cajon2_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_50.Location = new System.Drawing.Point(419, 364);
            this.c_Cajon2_50.Name = "c_Cajon2_50";
            this.c_Cajon2_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_50.TabIndex = 11;
            this.c_Cajon2_50.Text = "0";
            this.c_Cajon2_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_100
            // 
            this.c_Cajon2_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_100.Location = new System.Drawing.Point(419, 334);
            this.c_Cajon2_100.Name = "c_Cajon2_100";
            this.c_Cajon2_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_100.TabIndex = 10;
            this.c_Cajon2_100.Text = "0";
            this.c_Cajon2_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_200
            // 
            this.c_Cajon2_200.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_200.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_200.Location = new System.Drawing.Point(419, 305);
            this.c_Cajon2_200.Name = "c_Cajon2_200";
            this.c_Cajon2_200.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_200.TabIndex = 9;
            this.c_Cajon2_200.Text = "0";
            this.c_Cajon2_200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_500
            // 
            this.c_Cajon2_500.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_500.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_500.Location = new System.Drawing.Point(419, 276);
            this.c_Cajon2_500.Name = "c_Cajon2_500";
            this.c_Cajon2_500.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_500.TabIndex = 8;
            this.c_Cajon2_500.Text = "0";
            this.c_Cajon2_500.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_1000
            // 
            this.c_Cajon2_1000.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_1000.Location = new System.Drawing.Point(419, 247);
            this.c_Cajon2_1000.Name = "c_Cajon2_1000";
            this.c_Cajon2_1000.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_1000.TabIndex = 7;
            this.c_Cajon2_1000.Text = "0";
            this.c_Cajon2_1000.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_mov
            // 
            this.c_mov.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_mov.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_mov.Location = new System.Drawing.Point(307, 507);
            this.c_mov.Name = "c_mov";
            this.c_mov.Size = new System.Drawing.Size(187, 41);
            this.c_mov.TabIndex = 54;
            this.c_mov.TabStop = false;
            this.c_mov.Text = "Detalle por Movimientos";
            this.c_mov.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_mov.UseVisualStyleBackColor = true;
            this.c_mov.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormConsultaEfectivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormConsultaEfectivo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormConsultaEfectivo";
            this.Load += new System.EventHandler(this.FormConsultaEfectivo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label c_TotalCajon2;
        private System.Windows.Forms.Label c_Cajon2_20;
        private System.Windows.Forms.Label c_Cajon2_50;
        private System.Windows.Forms.Label c_Cajon2_100;
        private System.Windows.Forms.Label c_Cajon2_200;
        private System.Windows.Forms.Label c_Cajon2_500;
        private System.Windows.Forms.Label c_Cajon2_1000;
        private System.Windows.Forms.Button c_Imprimir;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label c_numbilletes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button c_mov;
    }
}