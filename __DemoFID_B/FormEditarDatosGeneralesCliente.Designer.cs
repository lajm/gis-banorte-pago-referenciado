﻿namespace __DemoFID_B
{
    partial class FormEditarDatosGeneralesCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Clave = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.c_Telefono = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(425, 319);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 7;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_BAceptar
            // 
            this.c_BAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BAceptar.Location = new System.Drawing.Point(217, 319);
            this.c_BAceptar.Name = "c_BAceptar";
            this.c_BAceptar.Size = new System.Drawing.Size(122, 47);
            this.c_BAceptar.TabIndex = 6;
            this.c_BAceptar.Text = "Aceptar";
            this.c_BAceptar.UseVisualStyleBackColor = true;
            this.c_BAceptar.Click += new System.EventHandler(this.c_BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(329, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "AGREGAR  DATOS GENERALES DE CLIENTE";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Window;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(273, 191);
            this.c_Nombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(349, 26);
            this.c_Nombre.TabIndex = 28;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(196, 194);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Nombre :";
            // 
            // c_Clave
            // 
            this.c_Clave.BackColor = System.Drawing.SystemColors.Window;
            this.c_Clave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Clave.Location = new System.Drawing.Point(273, 161);
            this.c_Clave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Clave.Name = "c_Clave";
            this.c_Clave.Size = new System.Drawing.Size(165, 26);
            this.c_Clave.TabIndex = 26;
            this.c_Clave.Click += new System.EventHandler(this.c_Clave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(206, 161);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "Clave  :";
            // 
            // c_Telefono
            // 
            this.c_Telefono.BackColor = System.Drawing.SystemColors.Window;
            this.c_Telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Telefono.Location = new System.Drawing.Point(273, 221);
            this.c_Telefono.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Telefono.Name = "c_Telefono";
            this.c_Telefono.Size = new System.Drawing.Size(349, 26);
            this.c_Telefono.TabIndex = 30;
            this.c_Telefono.Click += new System.EventHandler(this.c_Telefono_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(190, 221);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Telefono :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(646, 319);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormEditarDatosGeneralesCliente
            // 
            this.AcceptButton = this.c_BAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.CancelButton = this.c_Cancelar;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_Telefono);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Clave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_BAceptar);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditarDatosGeneralesCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormEditarDatosGeneralesCliente";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_Clave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_Telefono;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}