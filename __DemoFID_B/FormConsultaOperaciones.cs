﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace __DemoFID_B
{
    public partial class FormConsultaOperaciones : Form
    {
        public FormConsultaOperaciones()
        {
            InitializeComponent();
        }

        DataTable t_Operacoiones;

        private void c_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormConsultaOperaciones_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;

           
            foreach (String l_correo in Globales.MailDestino.Split(';')) 
            c_mails.Items.Add(l_correo);
            c_mails.SelectedIndex = c_mails.Items.Count - 1;


            t_Operacoiones = BDConsulta.ObtenerConsultaOperaciones(c_fechabox.Value);
            c_ConsultaOperaciones.DataSource = t_Operacoiones;
            c_ConsultaOperaciones.Columns[0].HeaderText = "Nombre";
         //   c_ConsultaOperaciones.Columns[0].MinimumWidth = 150  ;
            c_ConsultaOperaciones.Columns[1].HeaderText = "Clave";
            c_ConsultaOperaciones.Columns[2].HeaderText = "Operación";
            c_ConsultaOperaciones.Columns[3].HeaderText = "Monto";
            c_ConsultaOperaciones.Columns[4].HeaderText = "Moneda";
            c_ConsultaOperaciones.Columns[5].HeaderText = "Fecha/Hora";
            
            Cursor.Hide();

            if (c_ConsultaOperaciones.RowCount == 0)
            {
                c_ConsultaOperaciones.DataSource = null;
                c_numoperaciones.Text = "0";
            }
            else
                c_numoperaciones.Text = c_ConsultaOperaciones.RowCount.ToString();
        }

        private void c_fechabox_ValueChanged(object sender, EventArgs e)
        {
        
            t_Operacoiones = BDConsulta.ObtenerConsultaOperaciones(c_fechabox.Value);

            c_ConsultaOperaciones.DataSource = t_Operacoiones;
            c_ConsultaOperaciones.Columns[0].HeaderText = "Nombre";
            //   c_ConsultaOperaciones.Columns[0].MinimumWidth = 150  ;
            c_ConsultaOperaciones.Columns[1].HeaderText = "Clave";
            c_ConsultaOperaciones.Columns[2].HeaderText = "Operación";
            c_ConsultaOperaciones.Columns[3].HeaderText = "Monto";
            c_ConsultaOperaciones.Columns[4].HeaderText = "Moneda";
            c_ConsultaOperaciones.Columns[5].HeaderText = "Fecha/Hora";

            if (c_ConsultaOperaciones.RowCount == 0)
            {
                c_ConsultaOperaciones.DataSource = null;
                c_numoperaciones.Text = "0";
            }
            else
                c_numoperaciones.Text  = c_ConsultaOperaciones.RowCount.ToString ();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (t_Operacoiones .Rows.Count >0)
           if ( Procesar(t_Operacoiones ))
                using (FormError l_mensaje = new FormError(false))
            {
                l_mensaje.c_MensajeError.Text = "Envio de archivo Excel enviado con Exito";
                l_mensaje.ShowDialog();
            }

           else

            using (FormError l_mensaje = new FormError(false))
            {
                l_mensaje.c_MensajeError.Text = "No se pudo enviar correo al destinatario";
                l_mensaje.ShowDialog();
            }





        }


        public bool CrearExcel( string l_Archivo)
        {
            return true;
        }

        bool Procesar(DataTable l_Datos)
        {
            try
            {

                String l_NombreOrigen = "Templete_ALLA.xls";

                String l_NombreFinal = "Registro de Operaciones " + c_fechabox.Value.ToShortDateString().Replace("/", "_") + "_" + c_fechabox.Value.Second.ToString() + ".xls";

                File.Copy(l_NombreOrigen, l_NombreFinal, true);

                using (OleDbConnection l_ConexionFinal = UtilsExcel.AbrirArchivoExcelXLSX(l_NombreFinal, true))
                {
                    foreach (DataRow l_Fila in l_Datos.Rows)
                        InsertarFila(l_Fila, l_ConexionFinal);




                    l_ConexionFinal.Close();


                }


                if (Prosegur.Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, (string)c_mails.SelectedItem
                                   , "Envio de Operaciones en Excel " + Globales.Serial1, "Registro de Operaciones", l_NombreFinal))
                {

                    Globales.EscribirBitacora("Mail", "Enviodecorreo", "Exitoso",1);

                    GC.Collect();
                    return true;

                }
                else
                {

                    Globales.EscribirBitacora("Mail", "Enviodecorreo", "FRACASO",1);

                    GC.Collect();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Creacion de Excel", "Abrir archivo", ex.Message,1 );

                return false;
            }
            

        }


        void InsertarFila(DataRow p_Fila, OleDbConnection p_Conexion)
        {
            String l_QueryInsertar
                = "INSERT INTO [Hoja1$] "
                + " "
                + "VALUES "
                + "     ( ?, ?, ?, ?, ?, ? ) ";

            using (OleDbCommand l_Comando = new OleDbCommand(l_QueryInsertar, p_Conexion))
            {
                try
                {
                    for (int i = 0; i < p_Fila.ItemArray.Length; i++)
                    {
                        if (p_Fila[i] != DBNull.Value)
                            l_Comando.Parameters.AddWithValue("?", p_Fila[i]);
                        else
                            l_Comando.Parameters.AddWithValue("?", "");
                    }
                    l_Comando.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error de Conversion de datos:" + ex.Message);

                }

            }
        }

        private void c_ConsultaOperaciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
