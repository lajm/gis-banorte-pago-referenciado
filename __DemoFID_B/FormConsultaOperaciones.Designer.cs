﻿namespace __DemoFID_B
{
    partial class FormConsultaOperaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsultaOperaciones));
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_mails = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.c_numoperaciones = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_fechabox = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_ConsultaOperaciones = new System.Windows.Forms.DataGridView();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.c_mails);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.c_numoperaciones);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.c_fechabox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_Cerrar);
            this.panel1.Controls.Add(this.c_ConsultaOperaciones);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 523);
            this.panel1.TabIndex = 2;
            // 
            // c_mails
            // 
            this.c_mails.FormattingEnabled = true;
            this.c_mails.ItemHeight = 20;
            this.c_mails.Location = new System.Drawing.Point(480, 112);
            this.c_mails.Name = "c_mails";
            this.c_mails.Size = new System.Drawing.Size(285, 24);
            this.c_mails.TabIndex = 44;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(305, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 34);
            this.button1.TabIndex = 43;
            this.button1.Text = "excel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // c_numoperaciones
            // 
            this.c_numoperaciones.BackColor = System.Drawing.Color.Gainsboro;
            this.c_numoperaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c_numoperaciones.Location = new System.Drawing.Point(202, 476);
            this.c_numoperaciones.Name = "c_numoperaciones";
            this.c_numoperaciones.Size = new System.Drawing.Size(63, 19);
            this.c_numoperaciones.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(10, 476);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 20);
            this.label2.TabIndex = 41;
            this.label2.Text = "Numero de Operaciones : ";
            // 
            // c_fechabox
            // 
            this.c_fechabox.Location = new System.Drawing.Point(3, 110);
            this.c_fechabox.Name = "c_fechabox";
            this.c_fechabox.Size = new System.Drawing.Size(285, 26);
            this.c_fechabox.TabIndex = 40;
            this.c_fechabox.ValueChanged += new System.EventHandler(this.c_fechabox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(202, 82);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(364, 29);
            this.label1.TabIndex = 39;
            this.label1.Text = "CONSULTA DE OPERACIONES";
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(295, 476);
            this.c_Cerrar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(177, 34);
            this.c_Cerrar.TabIndex = 38;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = true;
            this.c_Cerrar.Click += new System.EventHandler(this.c_Cerrar_Click);
            // 
            // c_ConsultaOperaciones
            // 
            this.c_ConsultaOperaciones.AllowUserToAddRows = false;
            this.c_ConsultaOperaciones.AllowUserToDeleteRows = false;
            this.c_ConsultaOperaciones.AllowUserToOrderColumns = true;
            this.c_ConsultaOperaciones.AllowUserToResizeColumns = false;
            this.c_ConsultaOperaciones.AllowUserToResizeRows = false;
            this.c_ConsultaOperaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.c_ConsultaOperaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.c_ConsultaOperaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.c_ConsultaOperaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_ConsultaOperaciones.Location = new System.Drawing.Point(4, 144);
            this.c_ConsultaOperaciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.c_ConsultaOperaciones.Name = "c_ConsultaOperaciones";
            this.c_ConsultaOperaciones.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.c_ConsultaOperaciones.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.c_ConsultaOperaciones.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_ConsultaOperaciones.Size = new System.Drawing.Size(792, 325);
            this.c_ConsultaOperaciones.TabIndex = 0;
            this.c_ConsultaOperaciones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.c_ConsultaOperaciones_CellContentClick);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 523);
            this.uc_PanelInferior1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 77);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // FormConsultaOperaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormConsultaOperaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormConsultaOperaciones";
            this.Load += new System.EventHandler(this.FormConsultaOperaciones_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_ConsultaOperaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView c_ConsultaOperaciones;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker c_fechabox;
        private System.Windows.Forms.TextBox c_numoperaciones;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox c_mails;
        private System.Windows.Forms.Button button1;
    }
}