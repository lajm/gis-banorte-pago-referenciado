﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.OleDb;

namespace __DemoFID_B
{
    class UtilsExcel
    {
        public static OleDbConnection
            AbrirArchivoExcel(String p_NombreArchivo, bool p_UsarCabecera)
        {
            DbProviderFactory l_GeneraConexion = DbProviderFactories.GetFactory("System.Data.OleDb");
            DbConnection l_Conexion = l_GeneraConexion.CreateConnection();
            String l_Propiedades = "Excel 8.0;";
            if (p_UsarCabecera)
                l_Propiedades += "HDR=YES";
            else
                l_Propiedades += "HDR=NO";

            String l_CadenaConexion = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + p_NombreArchivo + ";Extended Properties='" + l_Propiedades + "'";

            l_Conexion = new System.Data.OleDb.OleDbConnection(l_CadenaConexion);

            l_Conexion.Open();

            return (OleDbConnection)l_Conexion;
        }

    
        public static OleDbConnection
            AbrirArchivoExcelXLSX(String p_NombreArchivo, bool p_UsarCabecera)
        {
            DbProviderFactory l_GeneraConexion = DbProviderFactories.GetFactory("System.Data.OleDb");
            DbConnection l_Conexion = l_GeneraConexion.CreateConnection();
            String l_Propiedades = "Excel 12.0; HTML Import;";
            if (p_UsarCabecera)
                l_Propiedades += "HDR=YES";
            else
                l_Propiedades += "HDR=NO";

            String l_CadenaConexion = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + p_NombreArchivo + ";Extended Properties='" + l_Propiedades + "'";


            l_Conexion = new System.Data.OleDb.OleDbConnection(l_CadenaConexion);

            l_Conexion.Open();

            return (OleDbConnection)l_Conexion;
        }
    }
}
