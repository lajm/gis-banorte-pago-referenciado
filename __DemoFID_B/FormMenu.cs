﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;
using System.IO;
using IrdsMensajes;
using System.Configuration;
using IrdsTools;

namespace __DemoFID_B
{
    public partial class FormMenu : Form
    {
        int lRet;
        String l_VersionDll, l_Version_Doc;
        public  bool l_BuscarTarjeta;
        public bool l_CerrarVandalDoor= true;
        String l_Error;
        byte tipo = 0x3;
        int l_Reintentos = 0;
        DataTable l_datos;
        
        bool Envio = true;
        bool Reset = true;

        DateTime l_espera_tiempoReenvio;

        public FormMenu()
        {
            InitializeComponent();
            Directory.CreateDirectory("Bitacoras");
            Directory.CreateDirectory("Transacciones");
        }

        private void ProbarFunciones()
        {
            
            if (Globales.DEBUG )
            {
                Globales.EscribirBitacora("Inicio", "Modo Debug?", Globales.DEBUG.ToString(),1);



                try
                {
                    SidLib.ResetError();

                    lRet = SidLib.SID_Open(Globales.SaveImagenes);

                    SidLib.Dll_Configuration l_configuration = new SidLib.Dll_Configuration();
                    lRet = SidLib.GetDLLVersion(ref l_configuration, ref l_datos);

                    foreach (DataRow linea in l_datos.Rows)
                        Globales.EscribirBitacora("DLLVersion", "Caracteristica :" + linea[0].ToString(), linea[1].ToString(),1);
                    l_Version_Doc = l_datos.Rows[7][1].ToString() + " FW Version:" + l_datos.Rows[0][1].ToString ();
                    if (l_configuration.Door_Sensor)
                        Globales.EscribirBitacora("DLLVersion", "\r\n Door Sensor", " Present ",1);
                    if (l_configuration.Lock_Sensor)
                        Globales.EscribirBitacora("DLLVersion", "\r\n lock Sensor", " Present",1);
                    if (l_configuration.Uv_Sensor)
                        Globales.EscribirBitacora("DLLVersion", "\r\n Uv SENSOr", " Present ",1);
                    if (l_configuration.Magnetic_18Channel)
                        Globales.EscribirBitacora("DLLVersion", "\r\n MICR", " Present ",1);

                    lRet = SidLib.SID_Initialize();
                    if (lRet != 0)
                    {
                        SidLib.Error_Sid(lRet,  out l_Error); 
                       
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = l_Error;
                            f_Error.ShowDialog();
                            //c_BotonCancelar_Click(this, null);
                        }
                    }

                    
                    SidLib.SID_Close();
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Inicio", "GetIdentyfy: ",ex.Message + lRet.ToString(),1);
                }
                try
                {
                    do
                    {
                        if (l_Reintentos >= 3)
                            break;
                        lRet = SidLib.ResetPath();
                        l_Reintentos++;
                    } while (lRet != 0);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Inicio", "RESET: ", ex.Message + lRet.ToString(), 1);
                }
                l_Reintentos = 0;
                try
                {
                    do
                    {
                        if (l_Reintentos >= 3)
                            break;
                        lRet = SidLib.ResetError();
                        l_Reintentos++;
                    } while (lRet != 0);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Inicio", "Reset Software Error: ", ex.Message +  lRet.ToString(), 1);
                }

                
                
                
                try
                {
                    lRet = SidLib.SID_Open(true);

                    StringBuilder l_cadena = new StringBuilder(64);

                    lRet = SidLib.SID_GetVersion(l_cadena, 64);
                    if (lRet == SidLib.SID_OKAY)
                        Globales.EscribirBitacora("ProbarFunciones", "GetVersion", "Llama función, return = " + l_cadena.ToString(),1);
                    else
                    {
                        SidLib.Error_Sid(lRet, out l_Error); 
                        Globales.EscribirBitacora("NS_GetIdentify", "Retorno = " + lRet, l_Error ,1);
                    }
                    l_VersionDll = l_cadena.ToString();
                    lRet = SidLib.SID_Close();
                   
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("ProbarFunciones", "NS_GetDLLVersion", "ERROR: " + ex.ToString(),1);
                }
                
                
            }
        }

        private void FormMenu_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            Banderas();
            Abrir_HCR360(tipo);
            ProbarFunciones();
            //Byte[] prueba = { 0x00, 0x12, 0x34, 0x56, 0xAA, 0x55, 0xFF };

            Log.s_Activo = Globales.LogServer;



            uc_Bienvenida c_PanelBienvenida = new uc_Bienvenida();
            c_PanelBienvenida.c_SimularTarjeta.Click += new EventHandler(this.c_DeslizarTarjeta);
            c_PanelBienvenida.c_Logueo.Click += new EventHandler(this.c_Logueo);
            c_PanelBienvenida.Click += new EventHandler(this.c_PanelCentral_Click);
                     

            c_PanelBienvenida.c_Dll.Text = " " + l_VersionDll + " TECHMAGUI IRDS OK ";
            c_PanelBienvenida.c_Doc.Text = " " + l_Version_Doc;
            c_PanelCentral.Controls.Add(c_PanelBienvenida);

            ValidarDirectorio();
            Globales.LeerBolsaPuesta();
          // UtilsComunicacion.MantenerVivo();
           //UtilsComunicacion.EnviarAlerta("Equipo En Funcionamiento",true);
            Prosegur.MantenerVivo(String.Empty);
            l_espera_tiempoReenvio = DateTime.Now.AddMinutes (Globales.TiempoVivo);
          //UtilsComunicacion.BuscarDepositosNoEnviados();
            timer1.Enabled = true;
            l_BuscarTarjeta = true;
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            this.Focus();

        }

        private bool Abrir_HCR360(byte l_Tipo)
        {
            if (Globales.DEBUG)
            {
                try
                {
                    hcr360_api.hcr360Api.CerrarPuerto(Globales.HCR360_port);
                    //Sleep(10000);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Menú", "Activated", "ERROR: CloseCom" + ex.Message,1);
                    return false;
                }
              //  Globales.EscribirBitacora("Menú", "Activated", "HCR360.CloseCom");
                try
                {
                    lRet = hcr360_api.hcr360Api.AbrirPuerto(Globales.HCR360_port);
                    //MessageBox.Show("Número de lRet al intentar abrir el puerto: " + lRet.ToString());
                //    Globales.EscribirBitacora("Menú", "Activated", "Abrir puerto HCR360 Respuesta: " + lRet.ToString());
                 //   Globales.EscribirBitacora("Menú", "HCR360", "Puerto: " + Globales.HCR360_port);
                    Cursor.Current = Cursors.WaitCursor;
                    //FidLib.Dormir(20000);
                    Cursor.Current = Cursors.Default;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Menú", "Activated", "ERROR: OpenCom " + ex.ToString(),1);
                    lRet = -1;
                }
                try
                {
                    lRet = hcr360_api.hcr360Api.CambiarTipoTarjeta(Globales.HCR360_port, tipo);
               //     Globales.EscribirBitacora("Menú", "Activated", "CambiarTipoTarjeta lRet " + lRet.ToString() + " Tipo " + l_Tipo);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Cambiar tipo tarjeta", "Error HCR360 : ", ex.Message, 1);
                    
                }
            }
            return true;
        }

        private void ValidarDirectorio()
        {
            if (DateTime.Now.Day == 1 && !Globales.EnviarCorreos)
            {
                try
                {
                    System.IO.File.Delete(Application.StartupPath + "\\SELLADO.TXT");
                    System.IO.File.Delete(Application.StartupPath + "\\Transacciones.TXT");
                    System.IO.File.Delete(Application.StartupPath + "\\TransaccionesAcumuladas.TXT");
                }



                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Error al borrar Archivos de Registro: SELLADO y Transaciones Acumuladas", "Error Borrando ", ex.Message, 1);
                    
                }
            }
        }

        private bool ValidarConfiguracion(String p_Nombre)
        {
            if (int.Parse(System.Configuration.ConfigurationSettings.AppSettings[p_Nombre]) == 1)
                return true;
            else
                return false;
        }

        private void Banderas()
        {
            //Module1.FLAG_CSD8_PRESENT = ValidarConfiguracion("FLAG_CSD8_PRESENT");
            //Module1.FLAG_BAG_PRESENT = ValidarConfiguracion("FLAG_BAG_PRESENT");
            //Module1.DEMO_FLAG_NO_PRINTER = ValidarConfiguracion("FLAG_NO_PRINTER");
            //Module1.FLAG_NO_CSD8 = ValidarConfiguracion("FLAG_NO_CSD8");
            //Module1.DEMO_FLAG_NO_BADGE = ValidarConfiguracion("FLAG_NO_BADGE");
            //Module1.DEMO_FLAG_NO_DRAWERS = ValidarConfiguracion("FLAG_NO_DRAWERS");
            //Module1.DEMO_FLAG_NO_SERVICE = ValidarConfiguracion("FLAG_NO_SERVICE");
            //Module1.DEMO_FLAG_NO_IBUTTON = ValidarConfiguracion("FLAG_NO_IBUTTON");
            //Module1.DEMO_FLAG_NO_UPS = ValidarConfiguracion("FLAG_NO_UPS");
            //Module1.FLAG_CHECK_OCR_PRESENT = ValidarConfiguracion("FLAG_CHECK_OCR_PRESENT");
            //Module1.FLAG_DIVIDE_BANKNOTE_CHECK = ValidarConfiguracion("FLAG_DIVIDE_BANKNOTE_CHECK");
            //Module1.g_handleOfDll = 0;
            //Module1.g_numero_transazione = 0;

            Globales.DEBUG = ValidarConfiguracion("DEBUG");
            Globales.CajonUSD = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["USD_DRAWER"]);
            Globales.Capacidad_Cajon = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["DRAWER_CAPACITY"]);
            Globales.HCR360_port = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["HCR360_COMPORT"]);
            Globales.HCR360_baud = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["HCR360_BAUD"]);// 7 default
            Globales.HCR360_Parity = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["HCR360_PARITY"]);// 4 default;

            //FidApi.g_flag_Control_CajaFuerte = true;
            //timer1.Interval = 60000;
           // timer2.Interval = 2000;

            Globales.Iniciar_moveelevator();

            

            Globales.EscribirBitacora("Menú", "Load", "Termina Carga banderas",1);

            
            
                    
            //    For iii = 1 To MAX_DRAWERS
            //        g_drawer_pos(iii) = iii
            //    Next iii

            //    Dim COMPORT_DRAWERtmp As String
            //    COMPORT_DRAWERtmp = FI2DDemoIni_GetKey("SECTION_CSD8", "COMPORT_DRAWER", "2")
            //    If (IsNumeric(COMPORT_DRAWERtmp)) Then
            //        COMPORT_DRAWER = CLng(COMPORT_DRAWERtmp)
            //    Else
            //        COMPORT_DRAWER = 2
            //    End If
                
            //    If ((FLAG_CHECK_OCR_PRESENT = True) And (FLAG_CHECK_OCR_USE_CTSDECOD_DLL = True)) Then
            //        If (FLAG_DEMO_DEBUG = True) Then
            //        Else
            //            Call CreateCTSEvent
            //        End If
            //    End If
            //if (Module1.FLAG_CHECK_OCR_PRESENT && Module1.FLAG_CHECK_OCR_USE_CTSDECOD_DLL)
                
            //End Sub
        }

        private void c_Logueo(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            l_BuscarTarjeta = false;
           
            /* using (FormLogin f_logueo = new FormLogin())
            {
             f_logueo.ShowDialog();
             }*/

            using (FormDaEmpresa f_Inicio = new FormDaEmpresa())
            {
                l_BuscarTarjeta = false;
                // f_Inicio.l_IdUsuario = "Supervisor_ZONA";//ETV_Traslado
                //f_Inicio.l_IdUsuario = "ETV_Traslado";//symet002
                //f_Inicio.l_IdUsuario = "symetry2";
                f_Inicio.ShowDialog();
            }

            timer1.Enabled = true;
            l_BuscarTarjeta = true;
        }

        private void c_DeslizarTarjeta(object sender, EventArgs e)
        {
            /*Sleep(300);
            flag_LOOP_running = false;
            if (flag_user_abort)
            {
                if (Module1.DEMO_FLAG_NO_BADGE && label_cliccata)
                {*/
                    //label_cliccata = false;
         if (Globales.DEBUG)   {
             Close();
             return;
         }else
           using (FormSesion f_Inicio = new FormSesion())
           {
               l_BuscarTarjeta = false;
              // f_Inicio.l_IdUsuario = "Supervisor_ZONA";//ETV_Traslado
              f_Inicio.l_IdUsuario = "ETV_Traslado";//symet002
              //f_Inicio.l_IdUsuario = "symetry2";
               f_Inicio.ShowDialog();
           }
                /*}
            }*/
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


            String l_Usuario;
            byte[] RcvData = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] SW = new byte[10];
            byte Status = 0;


            timer1.Enabled = false;
            if (Globales.DEBUG)
            {
                

                if (l_BuscarTarjeta)
                {
                    try
                    {
                        lRet = hcr360_api.hcr360Api.LeerEstatus(Globales.HCR360_port, ref RcvData[0]);
                        //Globales.EscribirBitacora("FormMenu", "LeerEstatus", lRet.ToString());
                        if (lRet > 0)
                        {
                            //Globales.EscribirBitacora("FormMenu", "RcvData", RcvData[0].ToString());
                            if (int.Parse(RcvData[0].ToString()) > 0)
                            {
                                l_BuscarTarjeta = false;
                                lRet = hcr360_api.hcr360Api.LeerDatos(Globales.HCR360_port, ref RcvData[0], ref SW[0]);
                                l_Usuario = hcr360_tools.Lector.BinAryToAsciiStr(RcvData, 6);
                               // MessageBox.Show(l_Usuario);
                                //Globales.EscribirBitacora("Menú", "HCR360", "Usuario: " + l_Usuario);
                                if (l_Usuario == "ADMIN1")
                                    l_Usuario = "Supervisor_ZONA";
                                else if (l_Usuario == "OPER07")
                                    l_Usuario = "symetry2";
                                else if (l_Usuario == "CIT__ ")
                                    l_Usuario = "ETV_Traslado";
                                else l_Usuario = "";
                                if (l_Usuario != "")
                                {
                                    l_CerrarVandalDoor = true;
                                    hcr360_api.hcr360Api.CerrarPuerto(Globales.HCR360_port);
                                    using (FormSesion f_Inicio = new FormSesion())
                                    {
                                        f_Inicio.l_IdUsuario = l_Usuario;
                                        f_Inicio.ShowDialog();
                                    }
                                }
                                else
                                    l_BuscarTarjeta = true;
                            }
                            
                        }
                        else if (lRet < 0)
                        {
                            Abrir_HCR360(tipo);
                        }
                    }
                    catch (Exception ex)
                    {
                        Globales.EscribirBitacora("LEER TARJETA", "Error HCR360 : ", ex.Message, 1);
                    }
                }
            }
            DateTime l_Comprobar = DateTime.Now;
            
            

            if (l_Comprobar.Hour == Globales.HoraAcumulado && Envio)
            {
                if (Prosegur.MandarBitacoraAyer() )
                {
                    Globales.EscribirBitacora("Enviar Correo", "Bitacora DIARIA", "Envio exitoso", 1);

                     Envio = false;
                }
                else
                {

                    Globales.EscribirBitacora("Enviar Correo", "Bitacora DIARIA", "Error en la creación, o en el envio de la bitacora ", 1);
                }

            }
            if (Reset && l_Comprobar.Hour == Globales.HoraAcumulado && l_Comprobar.Minute >= Globales.MinutosReset )
            {
               
                Globales.EscribirBitacora("Preparando Reset Automatico", DateTime.Now.ToLongTimeString(), "Intentando Shutdown", 1);
                //int l_result = SidApi.SidLib.SID_UPSShutDown(1);

               
                Globales.EscribirBitacora("Preparando Reset Automatico", DateTime.Now.ToLongTimeString(),"Resultado: "+ "virtual = 0", 1);
                Reset = false;
                
                

                
            }
            if (l_Comprobar.Hour != Globales.HoraAcumulado)
            {
                Envio = true;
                Reset = true;
            }
            if (l_Comprobar > l_espera_tiempoReenvio)
            {
                l_espera_tiempoReenvio = l_Comprobar.AddMinutes(Globales.TiempoVivo);
                try
                {
                    Double l_numbilletesenbolsa = BDDeposito.ObtenerTotalBilletes();


                    Double l_decimal = (l_numbilletesenbolsa / Globales.Capacidad_Bolsa1);
                    l_numbilletesenbolsa = (l_decimal * 100);
                    String l_porcentaje = l_numbilletesenbolsa.ToString("###.##") + " %";
                    Prosegur.MantenerVivo(l_porcentaje);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("LEER Porcentaje", "Calculo % ", ex.Message, 1);
                }
                
            }
            try
            {


               

                MensajePing2 l_ping = new MensajePing2(Globales.Estacion);

                try
                {
                    String l_Json = l_ping.CodificarJson();

                    String[] l_Servers = ConfigurationManager.AppSettings["Servers"].Split(',');
                    foreach (String l_Servidor in l_Servers)
                    {
                        int l_IdServidor = Convert.ToInt32(l_Servidor);
                        ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(l_IdServidor);
                        l_Cola.AgregarMensaje(MensajeIrds.CategoriaMensajeEnum.Servicio
                       , MensajeIrds.TipoMensajeEnum.Servicio_Ping
                       , 2
                       , l_Json);

                        l_Cola.MandarMensajesPendientes(Globales.Estacion);
                    }
                }
                catch (Exception p)
                {
                    Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", p.Message, 1);
                }

            }
            catch (Exception exxx)
            {
                Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", exxx.Message, 1);
                Globales.EscribirBitacora("Exception AL procesar Ping de Conexion", "Error", exxx.Message, 1);
            }

            timer1.Enabled = true;
        }

        private void FormMenu_Activated(object sender, EventArgs e)
        {   
            this.Refresh();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            Abrir_HCR360(tipo);
            l_BuscarTarjeta = true;
            //Cursor.Current = Cursors.Default;
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
        }

        private void c_PanelCentral_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        private void c_PanelCentral_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            using (FormSelect f_seleccion = new FormSelect())
            {
                f_seleccion.ShowDialog();


            }
            

            timer1.Enabled = true;
        }


    }
}
