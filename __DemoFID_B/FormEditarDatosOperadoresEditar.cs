﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarDatosOperadoresEditar : Form
    {
        public enum Modo { Agregar, Editar };

        Modo m_Modo;

        
        public FormEditarDatosOperadoresEditar( string text )
        {
            string ClientID;
            InitializeComponent();
            ClientID = text;
           

            string l_id, l_Nombre, l_Refer, l_status, l_Pass;

            using ( FormEditarDatosOperadoresEditar l_Forma = new FormEditarDatosOperadoresEditar() )
            {
                if ( BDOperador.SelcOperador( ClientID, out l_id, out l_Nombre ,out l_Refer ,out l_status, out l_Pass ) )
                {
                    l_Forma.c_IdUsario.Text = l_id;
                    l_Forma.c_Nombre.Text = l_Nombre;
                    l_Forma.c_Refer.Text =  l_Refer;
                    //l_Forma.c_Status.Text = l_status;
                    l_Forma.c_Pass.Text = l_Pass;
                    l_Forma.c_PassConf.Text = l_Pass;

                    if (l_status == "True")
                        l_Forma.c_Status.Checked = true;
                    else
                        l_Forma.c_Status.Checked = false;

                    l_Forma.ShowDialog();

                }
            }
        }

        public FormEditarDatosOperadoresEditar()
        {
            InitializeComponent();
            
        }

       /* public static DialogResult EjecutarAgregar( out String p_Clave )
        {
            using ( FormEditarDatosOperadoresAgregar l_Forma = new FormEditarDatosOperadoresAgregar() )
            {
                l_Forma.m_Modo = Modo.Agregar;
                DialogResult l_Resultado = l_Forma.ShowDialog();
                p_Clave = l_Forma.c_Clave.Text;

                return l_Resultado;
            }
        }


        public static DialogResult EjecutarEditar( String p_Clave )
        {
            using ( FormEditarDatosGeneralesCliente l_Forma = new FormEditarDatosGeneralesCliente() )
            {
                l_Forma.m_Modo = Modo.Editar;
                if ( l_Forma.CargaUsuario( p_Clave ) )
                {
                    l_Forma.c_Clave.ReadOnly = true;
                    return l_Forma.ShowDialog();
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }

        public bool CargaUsuario( String p_Clave )
        {
            String l_Nombre, l_Telefono;

            if ( BDCliente.TraerCliente( p_Clave, out l_Nombre, out l_Telefono ) )
            {
                c_Clave.Text = p_Clave;
                c_Nombre.Text = l_Nombre;
                c_Telefono.Text = l_Telefono;
                return true;
            }

            return false;
        }*/

        private void c_BAceptar_Click( object sender, EventArgs e )
        {
            if ( m_Modo == Modo.Agregar )
            {

                if (c_PassConf.Text == "" || c_Pass.Text == "")
                {
                    MessageBox.Show("Por favor, introduzca los valores");
                    return;
                }
                if (c_PassConf.Text != c_Pass.Text)
                {
                    MessageBox.Show("confirmar la contraseña no coincide con la contraseña nueva ");
                    c_PassConf.Focus();
                    return;
                }

                BDOperador.Update1( c_Nombre.Text, c_Refer.Text,c_Status.Checked, c_IdUsario.Text, c_Pass.Text);
                DialogResult = DialogResult.OK;
            }
            else
            {
                // TODO: Hacer
            }
        }

        int l_idTeclado = 0;
        private void pictureBox1_Click( object sender, EventArgs e )
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_Nombre_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Nombre.Text = l_teclado.Captura;

            }
        }

        private void c_Refer_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Refer.Text = l_teclado.Captura;

            }
        }

        private void c_Cancelar_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        private void c_Pass_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_Pass.Text = l_teclado.Captura;

            }
        }

        private void c_PassConf_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_PassConf.Text = l_teclado.Captura;

            }
        }
    }
}
