﻿namespace __DemoFID_B
{
    partial class FormDepositoDolares
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDepositoDolares));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Depositar = new System.Windows.Forms.Button();
            this.c_Suma100 = new System.Windows.Forms.TextBox();
            this.c_Cantidad100 = new System.Windows.Forms.TextBox();
            this.c_EtiquetaNumeroCuenta = new System.Windows.Forms.Label();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_TotalDeposito = new System.Windows.Forms.TextBox();
            this.c_BilletesRechazados = new System.Windows.Forms.TextBox();
            this.c_BilletesAceptados = new System.Windows.Forms.TextBox();
            this.c_Suma1 = new System.Windows.Forms.TextBox();
            this.c_Suma2 = new System.Windows.Forms.TextBox();
            this.c_Suma5 = new System.Windows.Forms.TextBox();
            this.c_Suma10 = new System.Windows.Forms.TextBox();
            this.c_Suma20 = new System.Windows.Forms.TextBox();
            this.c_Suma50 = new System.Windows.Forms.TextBox();
            this.c_Cantidad1 = new System.Windows.Forms.TextBox();
            this.c_Cantidad2 = new System.Windows.Forms.TextBox();
            this.c_Cantidad5 = new System.Windows.Forms.TextBox();
            this.c_Cantidad10 = new System.Windows.Forms.TextBox();
            this.c_Cantidad20 = new System.Windows.Forms.TextBox();
            this.c_Cantidad50 = new System.Windows.Forms.TextBox();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_Depositar);
            this.panel1.Controls.Add(this.c_Suma100);
            this.panel1.Controls.Add(this.c_Cantidad100);
            this.panel1.Controls.Add(this.c_EtiquetaNumeroCuenta);
            this.panel1.Controls.Add(this.c_BotonCancelar);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Controls.Add(this.c_TotalDeposito);
            this.panel1.Controls.Add(this.c_BilletesRechazados);
            this.panel1.Controls.Add(this.c_BilletesAceptados);
            this.panel1.Controls.Add(this.c_Suma1);
            this.panel1.Controls.Add(this.c_Suma2);
            this.panel1.Controls.Add(this.c_Suma5);
            this.panel1.Controls.Add(this.c_Suma10);
            this.panel1.Controls.Add(this.c_Suma20);
            this.panel1.Controls.Add(this.c_Suma50);
            this.panel1.Controls.Add(this.c_Cantidad1);
            this.panel1.Controls.Add(this.c_Cantidad2);
            this.panel1.Controls.Add(this.c_Cantidad5);
            this.panel1.Controls.Add(this.c_Cantidad10);
            this.panel1.Controls.Add(this.c_Cantidad20);
            this.panel1.Controls.Add(this.c_Cantidad50);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(114, 435);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 24);
            this.label15.TabIndex = 73;
            this.label15.Text = "$1.00";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(568, 167);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(132, 24);
            this.label14.TabIndex = 72;
            this.label14.Text = "No. de Cuenta";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(357, 466);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 26);
            this.label13.TabIndex = 71;
            this.label13.Text = "Total Depósito:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(616, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 24);
            this.label12.TabIndex = 70;
            this.label12.Text = "Suma";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(391, 240);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 24);
            this.label11.TabIndex = 69;
            this.label11.Text = "Cantidad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(114, 407);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 24);
            this.label8.TabIndex = 68;
            this.label8.Text = "$2.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(114, 379);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 24);
            this.label7.TabIndex = 67;
            this.label7.Text = "$5.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(104, 351);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 24);
            this.label6.TabIndex = 66;
            this.label6.Text = "$10.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(104, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 24);
            this.label5.TabIndex = 65;
            this.label5.Text = "$20.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(104, 295);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 24);
            this.label4.TabIndex = 64;
            this.label4.Text = "$50.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(94, 267);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 24);
            this.label9.TabIndex = 63;
            this.label9.Text = "$100.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(83, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 24);
            this.label10.TabIndex = 62;
            this.label10.Text = "Denominación";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(175, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(255, 25);
            this.label3.TabIndex = 61;
            this.label3.Text = "BILLETES RECHAZADOS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(158, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 25);
            this.label2.TabIndex = 60;
            this.label2.Text = "BILLETES ACEPTADOS:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(272, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 29);
            this.label1.TabIndex = 59;
            this.label1.Text = "DEPOSITO DÓLARES";
            // 
            // c_Depositar
            // 
            this.c_Depositar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Depositar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Depositar.Location = new System.Drawing.Point(329, 503);
            this.c_Depositar.Name = "c_Depositar";
            this.c_Depositar.Size = new System.Drawing.Size(143, 41);
            this.c_Depositar.TabIndex = 58;
            this.c_Depositar.TabStop = false;
            this.c_Depositar.Text = "Depositar";
            this.c_Depositar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Depositar.UseVisualStyleBackColor = true;
            this.c_Depositar.Click += new System.EventHandler(this.c_Depositar_Click);
            // 
            // c_Suma100
            // 
            this.c_Suma100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma100.Location = new System.Drawing.Point(575, 269);
            this.c_Suma100.Name = "c_Suma100";
            this.c_Suma100.ReadOnly = true;
            this.c_Suma100.Size = new System.Drawing.Size(100, 22);
            this.c_Suma100.TabIndex = 57;
            this.c_Suma100.TabStop = false;
            this.c_Suma100.Text = "$0.00";
            this.c_Suma100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad100
            // 
            this.c_Cantidad100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad100.Location = new System.Drawing.Point(391, 269);
            this.c_Cantidad100.Name = "c_Cantidad100";
            this.c_Cantidad100.ReadOnly = true;
            this.c_Cantidad100.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad100.TabIndex = 56;
            this.c_Cantidad100.TabStop = false;
            this.c_Cantidad100.Text = "0";
            this.c_Cantidad100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_EtiquetaNumeroCuenta
            // 
            this.c_EtiquetaNumeroCuenta.AutoSize = true;
            this.c_EtiquetaNumeroCuenta.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaNumeroCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaNumeroCuenta.Location = new System.Drawing.Point(568, 191);
            this.c_EtiquetaNumeroCuenta.Name = "c_EtiquetaNumeroCuenta";
            this.c_EtiquetaNumeroCuenta.Size = new System.Drawing.Size(135, 20);
            this.c_EtiquetaNumeroCuenta.TabIndex = 55;
            this.c_EtiquetaNumeroCuenta.Text = "00000000000001";
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonCancelar.Location = new System.Drawing.Point(478, 503);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(143, 41);
            this.c_BotonCancelar.TabIndex = 54;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "Cancelar";
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonAceptar.Location = new System.Drawing.Point(180, 503);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(143, 41);
            this.c_BotonAceptar.TabIndex = 53;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_TotalDeposito
            // 
            this.c_TotalDeposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalDeposito.Location = new System.Drawing.Point(550, 465);
            this.c_TotalDeposito.Name = "c_TotalDeposito";
            this.c_TotalDeposito.ReadOnly = true;
            this.c_TotalDeposito.Size = new System.Drawing.Size(125, 27);
            this.c_TotalDeposito.TabIndex = 52;
            this.c_TotalDeposito.TabStop = false;
            this.c_TotalDeposito.Text = "$0.00";
            this.c_TotalDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_BilletesRechazados
            // 
            this.c_BilletesRechazados.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesRechazados.Location = new System.Drawing.Point(428, 202);
            this.c_BilletesRechazados.Name = "c_BilletesRechazados";
            this.c_BilletesRechazados.ReadOnly = true;
            this.c_BilletesRechazados.Size = new System.Drawing.Size(86, 27);
            this.c_BilletesRechazados.TabIndex = 51;
            this.c_BilletesRechazados.TabStop = false;
            this.c_BilletesRechazados.Text = "No";
            // 
            // c_BilletesAceptados
            // 
            this.c_BilletesAceptados.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BilletesAceptados.Location = new System.Drawing.Point(428, 169);
            this.c_BilletesAceptados.Name = "c_BilletesAceptados";
            this.c_BilletesAceptados.ReadOnly = true;
            this.c_BilletesAceptados.Size = new System.Drawing.Size(86, 27);
            this.c_BilletesAceptados.TabIndex = 50;
            this.c_BilletesAceptados.TabStop = false;
            this.c_BilletesAceptados.Text = "0";
            this.c_BilletesAceptados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma1
            // 
            this.c_Suma1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma1.Location = new System.Drawing.Point(575, 437);
            this.c_Suma1.Name = "c_Suma1";
            this.c_Suma1.ReadOnly = true;
            this.c_Suma1.Size = new System.Drawing.Size(100, 22);
            this.c_Suma1.TabIndex = 49;
            this.c_Suma1.TabStop = false;
            this.c_Suma1.Text = "$0.00";
            this.c_Suma1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma2
            // 
            this.c_Suma2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma2.Location = new System.Drawing.Point(575, 409);
            this.c_Suma2.Name = "c_Suma2";
            this.c_Suma2.ReadOnly = true;
            this.c_Suma2.Size = new System.Drawing.Size(100, 22);
            this.c_Suma2.TabIndex = 48;
            this.c_Suma2.TabStop = false;
            this.c_Suma2.Text = "$0.00";
            this.c_Suma2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma5
            // 
            this.c_Suma5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma5.Location = new System.Drawing.Point(575, 381);
            this.c_Suma5.Name = "c_Suma5";
            this.c_Suma5.ReadOnly = true;
            this.c_Suma5.Size = new System.Drawing.Size(100, 22);
            this.c_Suma5.TabIndex = 47;
            this.c_Suma5.TabStop = false;
            this.c_Suma5.Text = "$0.00";
            this.c_Suma5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma10
            // 
            this.c_Suma10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma10.Location = new System.Drawing.Point(575, 353);
            this.c_Suma10.Name = "c_Suma10";
            this.c_Suma10.ReadOnly = true;
            this.c_Suma10.Size = new System.Drawing.Size(100, 22);
            this.c_Suma10.TabIndex = 46;
            this.c_Suma10.TabStop = false;
            this.c_Suma10.Text = "$0.00";
            this.c_Suma10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma20
            // 
            this.c_Suma20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma20.Location = new System.Drawing.Point(575, 325);
            this.c_Suma20.Name = "c_Suma20";
            this.c_Suma20.ReadOnly = true;
            this.c_Suma20.Size = new System.Drawing.Size(100, 22);
            this.c_Suma20.TabIndex = 45;
            this.c_Suma20.TabStop = false;
            this.c_Suma20.Text = "$0.00";
            this.c_Suma20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Suma50
            // 
            this.c_Suma50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Suma50.Location = new System.Drawing.Point(575, 297);
            this.c_Suma50.Name = "c_Suma50";
            this.c_Suma50.ReadOnly = true;
            this.c_Suma50.Size = new System.Drawing.Size(100, 22);
            this.c_Suma50.TabIndex = 44;
            this.c_Suma50.TabStop = false;
            this.c_Suma50.Text = "$0.00";
            this.c_Suma50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad1
            // 
            this.c_Cantidad1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad1.Location = new System.Drawing.Point(391, 437);
            this.c_Cantidad1.Name = "c_Cantidad1";
            this.c_Cantidad1.ReadOnly = true;
            this.c_Cantidad1.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad1.TabIndex = 43;
            this.c_Cantidad1.TabStop = false;
            this.c_Cantidad1.Text = "0";
            this.c_Cantidad1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad2
            // 
            this.c_Cantidad2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad2.Location = new System.Drawing.Point(391, 409);
            this.c_Cantidad2.Name = "c_Cantidad2";
            this.c_Cantidad2.ReadOnly = true;
            this.c_Cantidad2.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad2.TabIndex = 42;
            this.c_Cantidad2.TabStop = false;
            this.c_Cantidad2.Text = "0";
            this.c_Cantidad2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad5
            // 
            this.c_Cantidad5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad5.Location = new System.Drawing.Point(391, 381);
            this.c_Cantidad5.Name = "c_Cantidad5";
            this.c_Cantidad5.ReadOnly = true;
            this.c_Cantidad5.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad5.TabIndex = 41;
            this.c_Cantidad5.TabStop = false;
            this.c_Cantidad5.Text = "0";
            this.c_Cantidad5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad10
            // 
            this.c_Cantidad10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad10.Location = new System.Drawing.Point(391, 353);
            this.c_Cantidad10.Name = "c_Cantidad10";
            this.c_Cantidad10.ReadOnly = true;
            this.c_Cantidad10.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad10.TabIndex = 40;
            this.c_Cantidad10.TabStop = false;
            this.c_Cantidad10.Text = "0";
            this.c_Cantidad10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad20
            // 
            this.c_Cantidad20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad20.Location = new System.Drawing.Point(391, 325);
            this.c_Cantidad20.Name = "c_Cantidad20";
            this.c_Cantidad20.ReadOnly = true;
            this.c_Cantidad20.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad20.TabIndex = 39;
            this.c_Cantidad20.TabStop = false;
            this.c_Cantidad20.Text = "0";
            this.c_Cantidad20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // c_Cantidad50
            // 
            this.c_Cantidad50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cantidad50.Location = new System.Drawing.Point(391, 297);
            this.c_Cantidad50.Name = "c_Cantidad50";
            this.c_Cantidad50.ReadOnly = true;
            this.c_Cantidad50.Size = new System.Drawing.Size(71, 22);
            this.c_Cantidad50.TabIndex = 38;
            this.c_Cantidad50.TabStop = false;
            this.c_Cantidad50.Text = "0";
            this.c_Cantidad50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // FormDepositoDolares
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDepositoDolares";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDepositoDolares";
            this.Load += new System.EventHandler(this.FormDepositoDolares_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox c_Suma100;
        private System.Windows.Forms.TextBox c_Cantidad100;
        private System.Windows.Forms.Label c_EtiquetaNumeroCuenta;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.TextBox c_TotalDeposito;
        private System.Windows.Forms.TextBox c_BilletesRechazados;
        private System.Windows.Forms.TextBox c_BilletesAceptados;
        private System.Windows.Forms.TextBox c_Suma1;
        private System.Windows.Forms.TextBox c_Suma2;
        private System.Windows.Forms.TextBox c_Suma5;
        private System.Windows.Forms.TextBox c_Suma10;
        private System.Windows.Forms.TextBox c_Suma20;
        private System.Windows.Forms.TextBox c_Suma50;
        private System.Windows.Forms.TextBox c_Cantidad1;
        private System.Windows.Forms.TextBox c_Cantidad2;
        private System.Windows.Forms.TextBox c_Cantidad5;
        private System.Windows.Forms.TextBox c_Cantidad10;
        private System.Windows.Forms.TextBox c_Cantidad20;
        private System.Windows.Forms.TextBox c_Cantidad50;
        private System.Windows.Forms.Button c_Depositar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
    }
}