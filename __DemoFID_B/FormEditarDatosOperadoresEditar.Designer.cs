﻿namespace __DemoFID_B
{
    partial class FormEditarDatosOperadoresEditar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_IdUsario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Refer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.c_Status = new System.Windows.Forms.CheckBox();
            this.c_Pass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.c_PassConf = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(484, 502);
            this.c_Cancelar.Margin = new System.Windows.Forms.Padding(4);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(163, 58);
            this.c_Cancelar.TabIndex = 7;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_BAceptar
            // 
            this.c_BAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BAceptar.Location = new System.Drawing.Point(237, 502);
            this.c_BAceptar.Margin = new System.Windows.Forms.Padding(4);
            this.c_BAceptar.Name = "c_BAceptar";
            this.c_BAceptar.Size = new System.Drawing.Size(163, 58);
            this.c_BAceptar.TabIndex = 6;
            this.c_BAceptar.Text = "Aceptar";
            this.c_BAceptar.UseVisualStyleBackColor = true;
            this.c_BAceptar.Click += new System.EventHandler(this.c_BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(103, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(410, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "EDITAR  DATOS GENERALES DE OPERADOR";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Window;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(361, 325);
            this.c_Nombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(464, 30);
            this.c_Nombre.TabIndex = 28;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(258, 325);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 27;
            this.label1.Text = "Nombre :";
            // 
            // c_IdUsario
            // 
            this.c_IdUsario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.c_IdUsario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_IdUsario.Location = new System.Drawing.Point(364, 198);
            this.c_IdUsario.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c_IdUsario.Name = "c_IdUsario";
            this.c_IdUsario.ReadOnly = true;
            this.c_IdUsario.Size = new System.Drawing.Size(219, 30);
            this.c_IdUsario.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(284, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 25);
            this.label3.TabIndex = 25;
            this.label3.Text = "Clave :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(229, 362);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 25);
            this.label2.TabIndex = 29;
            this.label2.Text = "Referencia :";
            // 
            // c_Refer
            // 
            this.c_Refer.BackColor = System.Drawing.SystemColors.Window;
            this.c_Refer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Refer.Location = new System.Drawing.Point(361, 362);
            this.c_Refer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c_Refer.Name = "c_Refer";
            this.c_Refer.Size = new System.Drawing.Size(219, 30);
            this.c_Refer.TabIndex = 30;
            this.c_Refer.Click += new System.EventHandler(this.c_Refer_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(241, 411);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 25);
            this.label7.TabIndex = 35;
            this.label7.Text = "Active :";
            // 
            // c_Status
            // 
            this.c_Status.AutoSize = true;
            this.c_Status.Location = new System.Drawing.Point(361, 416);
            this.c_Status.Margin = new System.Windows.Forms.Padding(4);
            this.c_Status.Name = "c_Status";
            this.c_Status.Size = new System.Drawing.Size(18, 17);
            this.c_Status.TabIndex = 36;
            this.c_Status.UseVisualStyleBackColor = true;
            // 
            // c_Pass
            // 
            this.c_Pass.BackColor = System.Drawing.Color.White;
            this.c_Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Pass.Location = new System.Drawing.Point(365, 235);
            this.c_Pass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c_Pass.Name = "c_Pass";
            this.c_Pass.Size = new System.Drawing.Size(219, 30);
            this.c_Pass.TabIndex = 38;
            this.c_Pass.Click += new System.EventHandler(this.c_Pass_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(225, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 25);
            this.label5.TabIndex = 37;
            this.label5.Text = "Contraseña :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(789, 502);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // c_PassConf
            // 
            this.c_PassConf.BackColor = System.Drawing.Color.White;
            this.c_PassConf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_PassConf.Location = new System.Drawing.Point(365, 280);
            this.c_PassConf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.c_PassConf.Name = "c_PassConf";
            this.c_PassConf.Size = new System.Drawing.Size(219, 30);
            this.c_PassConf.TabIndex = 41;
            this.c_PassConf.Click += new System.EventHandler(this.c_PassConf_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(151, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(199, 25);
            this.label6.TabIndex = 40;
            this.label6.Text = "Confirma Contareña :";
            // 
            // FormEditarDatosOperadoresEditar
            // 
            this.AcceptButton = this.c_BAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.CancelButton = this.c_Cancelar;
            this.ClientSize = new System.Drawing.Size(1067, 738);
            this.Controls.Add(this.c_PassConf);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_Pass);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Status);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.c_Refer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_IdUsario);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_BAceptar);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormEditarDatosOperadoresEditar";
            this.Text = "FormEditarDatosGeneralesCliente";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_IdUsario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Refer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox c_Status;
        private System.Windows.Forms.TextBox c_Pass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox c_PassConf;
        private System.Windows.Forms.Label label6;
    }
}