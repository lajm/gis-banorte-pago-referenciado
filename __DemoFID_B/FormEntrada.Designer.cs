﻿namespace __DemoFID_B
{
    partial class FormEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Empezar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c_Empezar
            // 
            this.c_Empezar.Location = new System.Drawing.Point(164, 166);
            this.c_Empezar.Name = "c_Empezar";
            this.c_Empezar.Size = new System.Drawing.Size(228, 33);
            this.c_Empezar.TabIndex = 0;
            this.c_Empezar.Text = "Empezar DEMO Symetry";
            this.c_Empezar.UseVisualStyleBackColor = true;
            this.c_Empezar.Click += new System.EventHandler(this.c_Empezar_Click);
            // 
            // FormEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(528, 205);
            this.Controls.Add(this.c_Empezar);
            this.DoubleBuffered = true;
            this.Name = "FormEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BIENVENIDO";
            this.Load += new System.EventHandler(this.FormEntrada_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button c_Empezar;

    }
}