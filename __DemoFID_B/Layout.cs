﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Data;

using System.Net.Mail;

namespace __DemoFID_B
{
    class Santander
    {
        //Diario para Santander//

        private static String l_NombreArchivo;
        private static String l_Cabecera;
        private static String l_Transaccion;
        private static String l_separador = ",";


        private static int l_Provedor = 194;
        private static int l_Consecutivo = DateTime.Now.DayOfYear;
        private static String l_Fechaoperacion = DateTime.Now.ToString("yyyyMMdd");
        private static int l_TotalTransacciones = 0;
        private static Double l_ImporteTotal = 0;
        private static Double l_ImporteTotalUSD = 0;




        private static int l_numeroTransaccion = 0;


        private enum l_tipoTransaccion
        {
            Deposito = 0
                    ,
            SinFicha = 1275
                ,
            Sobrante = 142
                ,
            Faltante = 643
                , BilleteFalso = 1777
        };
        private enum l_Divisa { MXN = 0, USD };

        private static Double l_cuentaCheque = 65503000865;
        private static Double l_convenio = 0;
        private static Double l_Comprobante = 172324955;
        private static Double l_Folio = 7931;
        private static String l_referenciaCliente = "Mapvillahermosac208";
        private static int l_centroBoveda = 7077;
        private static Double l_ImporteTotalTransaccion = 25000;
        private static Double l_ImporteTotalDocumento = 25000;
        private static Double l_deshuso = 0;
        private static String l_Denominacion = "B0001000UMXN01";
        private static int l_Cantidad = 25;
        private static Double l_Importe = 25000;

        #region ENcapsulamientos
        public static String NombreArchivo
        {
            get { return Santander.l_NombreArchivo; }
            set { Santander.l_NombreArchivo = value; }
        }
        public static String Transaccion
        {
            get { return Santander.l_Transaccion; }
            set { Santander.l_Transaccion = value; }
        }

        public static String Cabecera
        {
            get { return Santander.l_Cabecera; }
            set { Santander.l_Cabecera = value; }
        }

        public static int Provedor
        {
            get { return Santander.l_Provedor; }
            set { Santander.l_Provedor = value; }
        }
        public static int Consecutivo
        {
            get { return Santander.l_Consecutivo; }
            set { Santander.l_Consecutivo = value; }
        }
        public static int TotalTransacciones
        {
            get { return Santander.l_TotalTransacciones; }
            set { Santander.l_TotalTransacciones = value; }
        }
        public static Double ImporteTotal
        {
            get { return Santander.l_ImporteTotal; }
            set { Santander.l_ImporteTotal = value; }
        }
        public static Double ImporteTotalUSD
        {
            get { return Santander.l_ImporteTotalUSD; }
            set { Santander.l_ImporteTotalUSD = value; }
        }
        public static int numeroTransaccion
        {
            get { return Santander.l_numeroTransaccion; }
            set { Santander.l_numeroTransaccion = value; }
        }
        #endregion

        private static void
           Escribir_Cabecera_Layout()
        {
            if (String.IsNullOrEmpty(NombreArchivo))
                NombreArchivo = "dm" + l_centroBoveda + l_Provedor.ToString("00000") + l_Fechaoperacion + l_Consecutivo.ToString("000") + ".txt";

            using (StreamWriter l_Archivo = File.AppendText(l_NombreArchivo))
            {
                l_Archivo.WriteLine(Cabecera);
            }
        }

        private static void
          Escribir_Cuerpo_Layout(String p_transaccion)
        {
            using (StreamWriter l_Archivo = File.AppendText(NombreArchivo))
            {
                l_Archivo.WriteLine(p_transaccion);
            }
        }


        public static void
          Escribir_Transaccion(String Transaccion)
        {
            using (StreamWriter l_Archivo = File.AppendText("Transacciones.txt"))
            {
                l_Archivo.WriteLine(Transaccion);

            }
        }

        private static String CrearCabecera()
        {
            return Cabecera = "D," + l_Fechaoperacion + "," + l_Provedor.ToString("00000") + "," + l_Consecutivo.ToString("000") + "," + l_TotalTransacciones.ToString("0000000000000000") + "," + l_ImporteTotal.ToString("0000000000000000.0000") + "," + l_ImporteTotalUSD.ToString("0000000000000000.0000");


        }


        private static void agregarcoma(String p_cadena)
        {


        }

        public static String CrearTransaccion(String p_Desglose, int p_moneda, String p_cuentaCheques, String p_Referencia, Double p_Total)
        {
            l_Divisa m_pesos = l_Divisa.MXN;
            l_Divisa m_Dolares = l_Divisa.MXN;

            numeroTransaccion = NumeroTransacciones();
            l_cuentaCheque = Double.Parse(p_cuentaCheques);
            l_referenciaCliente = p_Referencia;
            l_ImporteTotalTransaccion = l_ImporteTotalDocumento = p_Total;




            if (p_moneda == 1)
                return l_Transaccion = l_numeroTransaccion.ToString("00000") + "," + ((int)l_tipoTransaccion.Deposito).ToString("000000") + "," + l_cuentaCheque.ToString("000000000000")
                                    + "," + l_convenio.ToString("00000000000") + "," + l_Comprobante.ToString("0000000000") + "," + l_Folio.ToString("0000000000") + "," + l_referenciaCliente.PadRight(40, ' ')
                                    + "," + l_centroBoveda.ToString("0000") + "," + m_pesos + "," + l_ImporteTotalTransaccion.ToString("0000000000000000.0000")
                                    + "," + l_ImporteTotalDocumento.ToString("0000000000000000.0000") + "," + l_deshuso.ToString("000000000000000000000")
                                    + "," + "0" + "," + p_Desglose;
            else
                return l_Transaccion = l_numeroTransaccion.ToString("00000") + "," + ((int)l_tipoTransaccion.Deposito).ToString("000000") + "," + l_cuentaCheque.ToString("000000000000")
                                         + "," + l_convenio.ToString("00000000000") + "," + l_Comprobante.ToString("0000000000") + "," + l_Folio.ToString("0000000000") + "," + l_referenciaCliente.PadRight(40, ' ')
                                         + "," + l_centroBoveda.ToString("0000") + "," + m_Dolares + "," + l_ImporteTotalTransaccion.ToString("0000000000000000.0000")
                                         + "," + l_ImporteTotalDocumento.ToString("0000000000000000.0000") + "," + l_deshuso.ToString("000000000000000000000")
                                         + "," + "0" + "," + p_Desglose;

        }

        public static String CrearDesglose(DataTable p_DetalleTransaccion)
        {
            String p_Desglose = "";
            int l_denominacion, l_cantidad;
            foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
            {
                l_denominacion = int.Parse(l_Detalle[2].ToString());
                l_cantidad = Int32.Parse(l_Detalle[1].ToString());

                p_Desglose += "B" + l_denominacion.ToString("0000000") + "UMXN01" + ","
                    + l_cantidad.ToString("000000") + "," + (l_cantidad * l_denominacion).ToString("0000000000000000.0000") + ",";
            }
            //    l_Denominacion + "," + l_Cantidad.ToString("000000") + "," + l_Importe.ToString("0000000000000000.0000")
            return p_Desglose.Substring(0, p_Desglose.Length - 1);
        }


        public static bool LeerTransacciones()
        {
            Double l_Total = 0, l_TotalUSD = 0;

            if (File.Exists("Transacciones.txt"))
            {
                String[] lineas = File.ReadAllLines("Transacciones.txt");

                if (lineas.Length > 0)
                {
                    TotalTransacciones = lineas.Length;

                    foreach (String l_transaccion in lineas)
                    {
                        String[] l_deatalle = l_transaccion.Split(',');
                        if (l_deatalle[8] == "MXN")
                            l_Total += Double.Parse(l_deatalle[9]);
                        else
                            l_TotalUSD += Double.Parse(l_deatalle[9]);

                    }

                    ImporteTotal = l_Total;
                    ImporteTotalUSD = l_TotalUSD;

                    CrearCabecera();
                    Escribir_Cabecera_Layout();
                    foreach (String l_transaccion in lineas)
                    {
                        Escribir_Cuerpo_Layout(l_transaccion);
                    }

                    return LimpiarLayout();

                }
                else
                    return false;
            }
            else
                return false;
        }

        private static bool LimpiarLayout()
        {

            if (File.Exists("Transacciones.txt"))
            {
                File.Delete("Transacciones.txt");
                TotalTransacciones = 0;
                ImporteTotal = 0;
                ImporteTotalUSD = 0;
                return true;
            }

            return false;
        }
        private static int NumeroTransacciones()
        {
            if (File.Exists("Transacciones.txt"))
            {
                String[] lineas = File.ReadAllLines("Transacciones.txt");

                if (lineas.Length > 0)
                {
                    return lineas.Length;
                }
                else
                    return 1;
            }
            else
                return 1;
        }

    }

    class Prosegur
    {
        //Diario para Prosegur//

        private static String l_NombreArchivo;
        private static String l_Cabecera;
        private static String l_Transaccion;
        private static String l_separador = ",";


        private static int l_Consecutivo = DateTime.Now.DayOfYear;
        private static String l_FechaCreacion = DateTime.Now.ToString("dd/MM/yyyy/s");
        private static String l_Sucursal = Globales.SucursalProsegur;
        private static String l_referenciaCliente = Globales.ClienteProsegur ;




        private static int l_TotalTransacciones = 0;
        private static Double l_ImporteTotal = 0;
        // private static Double l_ImporteTotalUSD = 0;




        private static int l_numeroTransaccion = 0;


        public enum l_tipoTransaccion
        {
            Deposito = 0
                    ,
            Retiro = 1
        };
        private enum l_Divisa { MXN = 0, USD };





        #region Encapsulamientos
        public static String NombreArchivo
        {
            get { return Prosegur.l_NombreArchivo; }
            set { Prosegur.l_NombreArchivo = value; }
        }
        public static String Transaccion
        {
            get { return Prosegur.l_Transaccion; }
            set { Prosegur.l_Transaccion = value; }
        }

        public static String Cabecera
        {
            get { return Prosegur.l_Cabecera; }
            set { Prosegur.l_Cabecera = value; }
        }


        public static int Consecutivo
        {
            get { return Prosegur.l_Consecutivo; }
            set { Prosegur.l_Consecutivo = value; }
        }
        public static int TotalTransacciones
        {
            get { return Prosegur.l_TotalTransacciones; }
            set { Prosegur.l_TotalTransacciones = value; }
        }
        public static Double ImporteTotal
        {
            get { return Prosegur.l_ImporteTotal; }
            set { Prosegur.l_ImporteTotal = value; }
        }

        public static int numeroTransaccion
        {
            get { return Prosegur.l_numeroTransaccion; }
            set { Prosegur.l_numeroTransaccion = value; }
        }
        #endregion

        private static void
           Escribir_Cabecera_Layout(int p_opcion)
        {
            
            l_Consecutivo = DateTime.Now.DayOfYear;
            l_FechaCreacion = DateTime.Now.ToString("dd/MM/yyyy/HH/ss");

            if (String.IsNullOrEmpty(NombreArchivo))
                NombreArchivo =".\\Transacciones\\" + l_Sucursal + l_FechaCreacion.Replace("/", "") + l_Consecutivo.ToString("000") ;
            if (p_opcion == 1)
                NombreArchivo += "Acu.txt";
            else
            NombreArchivo += ".txt";

            try
            {

                using (StreamWriter l_Archivo = File.CreateText(NombreArchivo))
                {
                    l_Archivo.WriteLine(Cabecera);

                }
            }
            catch (Exception ex)
            {
                System.Threading.Thread.Sleep(1000);
                using (StreamWriter l_Archivo = File.CreateText(NombreArchivo.Replace(l_Consecutivo.ToString("000"), (l_Consecutivo +1).ToString("000"))))
                {
                    l_Archivo.WriteLine(Cabecera);

                }
            }

        }

        private static void
          Escribir_Cuerpo_Layout(String p_transaccion)
        {
            using (StreamWriter l_Archivo = File.AppendText(NombreArchivo))
            {
                l_Archivo.WriteLine(p_transaccion);
            }
        }


        public static void
          Escribir_Transaccion(String Transaccion)
        {
            using (StreamWriter l_Archivo = File.AppendText("Transacciones.txt"))
            {
                l_Archivo.WriteLine(Transaccion);

            }

            using (StreamWriter l_Archivo = File.AppendText("TransaccionesAcumuladas.txt"))
            {
                l_Archivo.WriteLine(Transaccion);

            }
        }

        private static String CrearCabecera()
        {
            return Cabecera = "Fecha,Cliente,Sucursal,Numero de Serie,Usuario,Folio OPERACION,Fecha/Hora,Operacion,Denominación,Tipo,Cantidad,Monto";


        }


        private static void agregarcoma(String p_cadena)
        {


        }

        public static bool CrearTransaccion(String p_Usuario, DataTable p_DetalleTransaccion, l_tipoTransaccion p_Operacion, int p_folio)
        {
                       
            String p_Desglose = "";
            int l_denominacion, l_cantidad;

            if (p_Operacion == l_tipoTransaccion.Deposito)
                l_Transaccion = l_FechaCreacion + "," + l_referenciaCliente + "," + l_Sucursal + "," + Globales.Serial1 + "," + p_Usuario + "," + p_folio  + ","
                                    + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "," + "Deposito" + "," + p_Desglose;
            else
                l_Transaccion = l_FechaCreacion + "," + l_referenciaCliente + "," + l_Sucursal + "," + Globales.Serial1 + "," + p_Usuario + "," + p_folio  + ","
                                    + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "," + "Retiro" + "," + p_Desglose;

            foreach (DataRow l_Detalle in p_DetalleTransaccion.Rows)
            {
                p_Desglose = "";
                l_denominacion = 0;
                l_cantidad = 0;

                l_denominacion = int.Parse(l_Detalle[2].ToString());
                l_cantidad = Int32.Parse(l_Detalle[1].ToString());

                p_Desglose = l_denominacion + "," + "Billete" + ","
                    + l_cantidad.ToString() + "," + (l_cantidad * l_denominacion);

                Escribir_Transaccion(l_Transaccion + p_Desglose);
            }


            return true;



        }

        public static String Leerarchivo(String l_path)
        { 
            String l_info ="";

         if (File.Exists(l_path ))
            {
                String[] lineas = File.ReadAllLines(l_path );

                if (lineas.Length > 0)
                {
                     foreach (String l_linea in lineas)
                    {
                       l_info += l_linea + "<br/>"; 
                     }
                }
         }
            return l_info ;

        }

        public static bool MandarBitacoraAyer()
        {

            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.AddDays (-1).ToString("MM_dd") + ".txt";

            if (File.Exists(l_log))
            {
                string l_logenviado = l_log + "_enviado.txt";
                File.Copy(l_log, l_logenviado, true);

                if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                , "Envio de BItacora " + Globales.Serial1, "Bitacora Diaria " + DateTime.Now.ToLongDateString(), l_logenviado))
                {
                    return true;

                }
                else
                {


                    return false;
                }



            }
            else
            {
                if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                       , "Envio de Bitacora " + Globales.Serial1, "NO existe Bitacora", ""))
                {
                    Globales.EscribirBitacora("Mail", "EnviodeBitacora", "Exitoso", 1);

                    return true;
                }
                else

                    return false;
            }
        }

        public static bool MandarBitacoraActual()
        {

            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.ToString("MM_dd") + ".txt";

            if (File.Exists(l_log))
            {
                string l_logenviado =l_log + "_enviado.txt";
                File.Copy (l_log,l_logenviado,true );

                    if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                    , "Envio de BItacora " + Globales.Serial1,"Bitacora del dia "+DateTime.Now.ToLongDateString (),l_logenviado))
                    {
                        return true;

                    }
                    else
                    {

                        
                        return false;
                    }
                    

               
            }
            else
            {
                if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                       , "Envio de Bitacora " + Globales.Serial1, "NO existe Bitacora",""))
                {
                    Globales.EscribirBitacora("Mail", "EnviodeBitacora", "Exitoso", 1);
                
                    return true;
                }else

                return false;
            }
        }

        public static bool LeerTransacciones()
        {
            Double l_Total = 0;
            Double l_TotalRetirado = 0;


            if (File.Exists("Transacciones.txt"))
            {
                String[] lineas = File.ReadAllLines("Transacciones.txt");

                if (lineas.Length > 0)
                {
                    TotalTransacciones = lineas.Length;

                    foreach (String l_transaccion in lineas)
                    {
                        String[] l_deatalle = l_transaccion.Split(',');
                        if (l_deatalle[7] == "Retiro")
                            l_TotalRetirado += Double.Parse(l_deatalle[11]);
                        else
                        l_Total += Double.Parse(l_deatalle[11]);

                    }

                    ImporteTotal = l_Total;


                    CrearCabecera();
                    Escribir_Cabecera_Layout(0);
                    foreach (String l_transaccion in lineas)
                    {
                        Escribir_Cuerpo_Layout(l_transaccion);
                    }

                    Escribir_Cuerpo_Layout(",,,,,,,,,,Total: " + l_Total.ToString ("$#,###,##0.00"));

                    if (l_TotalRetirado >0)
                    Escribir_Cuerpo_Layout(",,,,,,,,,,Total RETIRADO: " + l_TotalRetirado.ToString("$#,###,##0.00"));


                    if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                    , "Envio de Transaccion " + Globales.Serial1, Leerarchivo (NombreArchivo), ""))
                    {
                        LimpiarLayout();
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "Exitoso",1);

                    }
                    else
                    {
                        NombreArchivo = "";
                        //File.Copy(NombreArchivo, Globales.SucursalProsegur + "Atrasado.txt");
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "FRACASO",1);
                        return false;
                    }

                    return true;

                }
                else
                    return false;
            }
            else
            {
                if (File.Exists(Globales.SucursalProsegur + "Atrasado.txt"))
                    if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                       , "Envio de Lay Out " + Globales.Serial1, "Lay Out de transacciones", Globales.SucursalProsegur + "Atrasado.txt"))
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "Exitoso",1);
                {
                    try
                    {
                        File.Delete((Globales.SucursalProsegur + "Atrasado.txt"));
                    }
                    catch
                    {
                    }
                }

                return true;
            }
        }


        public static bool LeerAcumulado()
        {
            Double l_Total = 0;
            Double l_TotalRetirado = 0;

            if (File.Exists("TransaccionesAcumuladas.txt"))
            {
                String[] lineas = File.ReadAllLines("TransaccionesAcumuladas.txt");

                if (lineas.Length > 0)
                {
                    TotalTransacciones = lineas.Length;

                    foreach (String l_transaccion in lineas)
                    {
                        String[] l_deatalle = l_transaccion.Split(',');
                        if (l_deatalle[7] == "Retiro")
                            l_TotalRetirado += Double.Parse(l_deatalle[11]);
                        else
                            l_Total += Double.Parse(l_deatalle[11]);

                    }

                    ImporteTotal = l_Total;


                    CrearCabecera();
                    Escribir_Cabecera_Layout(1);
                    foreach (String l_transaccion in lineas)
                    {
                        Escribir_Cuerpo_Layout(l_transaccion);
                    }

                    Escribir_Cuerpo_Layout(",,,,,,,,,,Total: " + l_Total.ToString("$#,###,##0.00"));

                    if (l_TotalRetirado > 0)
                        Escribir_Cuerpo_Layout(",,,,,,,,,,Total RETIRADO: " + l_TotalRetirado.ToString("$#,###,##0.00"));

                    if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                    , "Envio Transacciones Acumuladas" + Globales.Serial1, Leerarchivo (NombreArchivo ), ""))
                    {
                        LimpiarAcumulado();
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "Exitoso",1);

                    }
                    else
                    {
                        NombreArchivo = "";
                        //File.Copy(NombreArchivo, Globales.SucursalProsegur + "Atrasado.txt");
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "FRACASO",1);
                        return false;
                    }

                    return true;

                }
                else
                    return false;
            }
            else
            {
                if (File.Exists(Globales.SucursalProsegur + "Atrasado.txt"))
                    if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                       , "Envio de Lay Out " + Globales.Serial1, "Lay Out de transacciones", Globales.SucursalProsegur + "Atrasado.txt"))
                        Globales.EscribirBitacora("Mail", "Enviodecorreo", "Exitoso",1);
                {
                    try
                    {
                        File.Delete((Globales.SucursalProsegur + "Atrasado.txt"));
                    }
                    catch
                    {
                    }
                }

                return true;
            }
        }

        private static bool LimpiarLayout()
        {
            NombreArchivo = "";

            if (File.Exists("Transacciones.txt"))
            {
                File.Delete("Transacciones.txt");
                TotalTransacciones = 0;
                ImporteTotal = 0;

                return true;
            }

            return false;
        }
        private static bool LimpiarAcumulado()
        {
            NombreArchivo = "";

            if (File.Exists("TransaccionesAcumuladas.txt"))
            {
                File.Delete("TransaccionesAcumuladas.txt");
                TotalTransacciones = 0;
                ImporteTotal = 0;

                return true;
            }

            return false;
        }

        private static int NumeroTransacciones()
        {
            if (File.Exists("Transacciones.txt"))
            {
                String[] lineas = File.ReadAllLines("Transacciones.txt");

                if (lineas.Length > 0)
                {
                    return lineas.Length;
                }
                else
                    return 0;
            }
            else
                return -1;
        }

        public static bool Enviodecorreo(String p_from, String p_nombre_from, String p_Destino
        , String p_subject, String p_mensaje, String p_path_adjunto)
        {

          if (Globales.EnviarCorreos)
            try
            {
                using (MailMessage oMsg = new MailMessage())
                {
                    // TODO: Replace with sender e-mail address.
                    oMsg.From = new MailAddress(p_from, p_nombre_from);
                    // TODO: Replace with recipient e-mail address.

                    foreach (String l_correo in p_Destino.Split(';'))
                        oMsg.To.Add(l_correo);


                    oMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                    oMsg.Subject = p_subject;
                    // SEND IN HTML FORMAT (comment this line to send plain text).
                    oMsg.IsBodyHtml = true;
                    // HTML Body (remove HTML tags for plain text).
                    oMsg.Body = "<HTML><BODY><B>" + "\n\n Archivo Creado el " + DateTime.Now.ToLongDateString()
                              + "<p>" + p_mensaje + "</p> <p>\n Archivo Enviado el " + DateTime.Now.ToLongDateString()
                              + "\n</p>\n\n\t<hr> <p> No Responda este mail automatizado  </B></BODY></HTML>";
                   // oMsg.Priority = MailPriority.High;
                    // ADD AN ATTACHMENT.
                    // TODO: Replace with path to attachment.
                    if (!String.IsNullOrEmpty(p_path_adjunto))
                    {
                        Attachment l_adjunto = new Attachment(p_path_adjunto);
                        oMsg.Attachments.Add(l_adjunto);
                    }
                    // TODO: Replace with the name of your remote SMTP server.
                    SmtpClient m_cliente = new SmtpClient();
                    m_cliente.Credentials = new System.Net.NetworkCredential(Globales.SMTPCredencial, Globales.SMTPPassw);
                    m_cliente.EnableSsl = false;
                 // m_cliente.UseDefaultCredentials = false;
                    m_cliente.Timeout = 20000;
                    m_cliente.Host = Globales.SMTPcliente;
                    m_cliente.Port = Globales.SMTPpuerto;
                    m_cliente.DeliveryMethod = SmtpDeliveryMethod.Network;
                    


                    m_cliente.Send(oMsg);


                    oMsg.Attachments.Clear();
                    oMsg.Dispose();

                    return true;

                }

            }
            catch (Exception ex)
            {
                using (StreamWriter l_Archivo = File.AppendText("MAIL ERROR.txt"))
                {
                    l_Archivo.WriteLine(DateTime.Now + " " + ex.Message  );

                }
            }

            return false;
        }

        public static void MantenerVivo(String p_Poscentaje)
        {
            if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                         , "STATUS " + Globales.Serial1, "EN LINEA :: " + p_Poscentaje, ""))
            {
                
                Globales.EscribirBitacora("Mail", "Mantener VIVO", "Exitoso",1);

            }
            else
            {

                //File.Copy(NombreArchivo, Globales.SucursalProsegur + "Atrasado.txt");
                Globales.EscribirBitacora("Mail", "Mantener VIVO", "FRACASO",1);

            }
        }

        public static void Alerta(String p_Mensaje )
        {
            if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                         , "ALERTA " + Globales.Serial1, p_Mensaje , ""))
            {
                
                Globales.EscribirBitacora("Mail", "ALERTA", "Exitoso",1);

            }
            else
            {

                //File.Copy(NombreArchivo, Globales.SucursalProsegur + "Atrasado.txt");
                Globales.EscribirBitacora("Mail", "ALERTA", "FRACASO",1);

            }
        }

        public static void AlertaSTOCK()
        {
            if (Enviodecorreo("Sid@cfe.gob.mx", Globales.Serial1, Globales.MailDestino
                                         , "STOCK " + Globales.Serial1, "Se ha llegado al Procentaje MAXIMO Establecido", ""))
            {
             
                Globales.EscribirBitacora("Mail", "STOCK", "Exitoso",1);

            }
            else
            {

                //File.Copy(NombreArchivo, Globales.SucursalProsegur + "Atrasado.txt");
                Globales.EscribirBitacora("Mail", "STOCK", "FRACASO",1);

            }
        }
    }

   
}








