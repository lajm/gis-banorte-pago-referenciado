﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;

namespace __DemoFID_B
{
    public partial class FormDepositoC : Form
    {        
        public int l_IdMoneda;
        int l_Reintentos = 0;
        String l_Equipo1 = "Equipo1";

        public FormDepositoC()
        {
            InitializeComponent();
            c_Empleado.Text = Globales.IdUsuario;
            c_nombreUsuario.Text = Globales.NombreUsuario.ToUpper ();
            c_nombreUsuario.Location = new System.Drawing.Point(400 - c_nombreUsuario .Size.Width/2,265);

            Globales.EscribirBitacora("LOGIN"," NUMERO " +Globales.IdUsuario + " NOMBRE: "+ Globales.NombreUsuario.ToUpper () , "EXITOSO",1 );
           
        }

        private void FormDepositoC_Load(object sender, EventArgs e)
        {
           
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            
            //try
            //{
            //    do
            //    {
            //        if (l_Reintentos >= 0)//Mayor que cero para cambiar BIN
            //            break;
            //        lRet = SidLib.ResetPath();
            //        l_Reintentos++;
            //    } while (lRet != 0);
            //}
            //catch (Exception ex)
            //{
            //}
          
            //Cursor.Current = Cursors.Default;
            Cursor.Hide();
        }


        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormDepositoC_Activated(object sender, EventArgs e)
        {
            
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            c_BotonAceptar.Enabled = false;
            
            if (l_IdMoneda == 1)
            {
                int lRet;
                l_Reintentos = 0;
                try
                {
                    do
                    {
                        if (l_Reintentos >= 1) //mayor que cero para cambiar BIN
                            break;
                        lRet = SidLib.ResetError();
                        l_Reintentos++;
                    } while (lRet != 0);
                }
                catch (Exception ex)
                {
                }

                using (FormDeposito f_Deposito = new FormDeposito())
                {
                   
                    f_Deposito.Equipo = l_Equipo1 ;
                   // Globales.CambiaEquipo(l_Equipo1);
                           
                    //UtilsComunicacion.MantenerVivo();
                    f_Deposito.ShowDialog();

                    Close();
                }
            }
            else
            {
                using (FormDepositoDolares f_Deposito = new FormDepositoDolares())
                {
                    f_Deposito.ShowDialog();
                    Close();
                }
            }
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();

            c_BotonAceptar.Enabled = true;
        }

        private void c_equipo1_CheckedChanged(object sender, EventArgs e)
        {
            if (c_equipo1.Checked)
              l_Equipo1 = "Equipo1";

            
        }

        private void c_equipo2_CheckedChanged(object sender, EventArgs e)
        {
            if (c_equipo2.Checked)
                l_Equipo1 = "Equipo2";
        }

       

    }
}

