﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormAbcClientes : Form
    {
        public FormAbcClientes()
        {
            InitializeComponent();
            ActualizarListado();
        }

        void ActualizarListado()
        {
            c_Datos.DataSource = BDCliente.TraerClientes();
            c_Datos.Update();
            Application.DoEvents();
        }

        private void c_BSalir_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }

        private void c_BAgregar_Click( object sender, EventArgs e )
        {
            String l_Clave;

            if ( FormEditarDatosGeneralesCliente.EjecutarAgregar( out l_Clave )
                == DialogResult.OK )
            {
                ActualizarListado();
                EditarCliente( l_Clave );
            }

        }

        void EditarCliente( String p_Clave )
        {
            FormEditarCliente.Ejecutar( p_Clave );
        }

        private void c_BEditar_Click( object sender, EventArgs e )
        {
            string MainID;

            MainID = c_Datos.Rows[ c_Datos.SelectedRows[ 0 ].Index ].Cells[ "ClaveCliente" ].Value.ToString();
            EditarCliente( MainID );
            ActualizarListado();


        }

        private void c_BEliminar_Click( object sender, EventArgs e )
        {
            string MainID;

            MainID = c_Datos.Rows[ c_Datos.SelectedRows[ 0 ].Index ].Cells[ "ClaveCliente" ].Value.ToString();

            BDCliente.EliminarClave( MainID );
            ActualizarListado();
        }
    }
}
