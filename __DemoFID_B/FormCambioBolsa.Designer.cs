﻿namespace __DemoFID_B
{
    partial class FormCambioBolsa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCambioBolsa));
            this.c_esperarsellado = new System.Windows.Forms.PictureBox();
            this.c_paso1 = new System.Windows.Forms.TextBox();
            this.c_paso2 = new System.Windows.Forms.TextBox();
            this.c_paso4 = new System.Windows.Forms.TextBox();
            this.c_paso5 = new System.Windows.Forms.TextBox();
            this.paso1 = new System.Windows.Forms.Label();
            this.paso5 = new System.Windows.Forms.Label();
            this.paso4 = new System.Windows.Forms.Label();
            this.paso2 = new System.Windows.Forms.Label();
            this.paso6 = new System.Windows.Forms.Label();
            this.c_paso6 = new System.Windows.Forms.TextBox();
            this.paso3 = new System.Windows.Forms.Label();
            this.c_paso3 = new System.Windows.Forms.TextBox();
            this.c_esperarInicio = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_empezar = new System.Windows.Forms.Button();
            this.c_Salir = new System.Windows.Forms.Button();
            this.panelSellado1 = new @__DemoFID_B.PanelSellado();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.c_barcodenuevo = new System.Windows.Forms.Label();
            this.c_barcode = new System.Windows.Forms.Label();
            this.c_ok = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarsellado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_ok)).BeginInit();
            this.SuspendLayout();
            // 
            // c_esperarsellado
            // 
            this.c_esperarsellado.Image = global::@__DemoFID_B.Properties.Resources.emblem_urgent;
            this.c_esperarsellado.Location = new System.Drawing.Point(644, 177);
            this.c_esperarsellado.Name = "c_esperarsellado";
            this.c_esperarsellado.Size = new System.Drawing.Size(55, 47);
            this.c_esperarsellado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_esperarsellado.TabIndex = 1;
            this.c_esperarsellado.TabStop = false;
            this.c_esperarsellado.Visible = false;
            // 
            // c_paso1
            // 
            this.c_paso1.BackColor = System.Drawing.Color.Yellow;
            this.c_paso1.Enabled = false;
            this.c_paso1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso1.ForeColor = System.Drawing.Color.Navy;
            this.c_paso1.Location = new System.Drawing.Point(196, 133);
            this.c_paso1.Name = "c_paso1";
            this.c_paso1.Size = new System.Drawing.Size(445, 31);
            this.c_paso1.TabIndex = 2;
            this.c_paso1.Text = "INTRODUZCA CODIGO";
            this.c_paso1.Visible = false;
            // 
            // c_paso2
            // 
            this.c_paso2.BackColor = System.Drawing.Color.Yellow;
            this.c_paso2.Enabled = false;
            this.c_paso2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso2.ForeColor = System.Drawing.Color.Navy;
            this.c_paso2.Location = new System.Drawing.Point(196, 193);
            this.c_paso2.Name = "c_paso2";
            this.c_paso2.Size = new System.Drawing.Size(445, 31);
            this.c_paso2.TabIndex = 3;
            this.c_paso2.Text = "ESPERE... REVISANDO EQUIPO";
            this.c_paso2.Visible = false;
            // 
            // c_paso4
            // 
            this.c_paso4.BackColor = System.Drawing.Color.Yellow;
            this.c_paso4.Enabled = false;
            this.c_paso4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso4.ForeColor = System.Drawing.Color.Navy;
            this.c_paso4.Location = new System.Drawing.Point(196, 315);
            this.c_paso4.Name = "c_paso4";
            this.c_paso4.Size = new System.Drawing.Size(445, 31);
            this.c_paso4.TabIndex = 4;
            this.c_paso4.Text = "RETIRE LA BOLSA";
            this.c_paso4.Visible = false;
            this.c_paso4.TextChanged += new System.EventHandler(this.c_paso4_TextChanged);
            // 
            // c_paso5
            // 
            this.c_paso5.BackColor = System.Drawing.Color.Yellow;
            this.c_paso5.Enabled = false;
            this.c_paso5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso5.ForeColor = System.Drawing.Color.Navy;
            this.c_paso5.Location = new System.Drawing.Point(196, 375);
            this.c_paso5.Name = "c_paso5";
            this.c_paso5.Size = new System.Drawing.Size(445, 31);
            this.c_paso5.TabIndex = 5;
            this.c_paso5.Text = "Leer Nuevo Codigo de Barras: ";
            this.c_paso5.Visible = false;
            // 
            // paso1
            // 
            this.paso1.AutoSize = true;
            this.paso1.BackColor = System.Drawing.Color.Transparent;
            this.paso1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso1.Location = new System.Drawing.Point(155, 117);
            this.paso1.Name = "paso1";
            this.paso1.Size = new System.Drawing.Size(65, 16);
            this.paso1.TabIndex = 6;
            this.paso1.Text = "PASO 1:";
            this.paso1.Visible = false;
            // 
            // paso5
            // 
            this.paso5.AutoSize = true;
            this.paso5.BackColor = System.Drawing.Color.Transparent;
            this.paso5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso5.Location = new System.Drawing.Point(155, 359);
            this.paso5.Name = "paso5";
            this.paso5.Size = new System.Drawing.Size(65, 16);
            this.paso5.TabIndex = 8;
            this.paso5.Text = "PASO 5:";
            this.paso5.Visible = false;
            // 
            // paso4
            // 
            this.paso4.AutoSize = true;
            this.paso4.BackColor = System.Drawing.Color.Transparent;
            this.paso4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso4.Location = new System.Drawing.Point(155, 299);
            this.paso4.Name = "paso4";
            this.paso4.Size = new System.Drawing.Size(65, 16);
            this.paso4.TabIndex = 9;
            this.paso4.Text = "PASO 4:";
            this.paso4.Visible = false;
            // 
            // paso2
            // 
            this.paso2.AutoSize = true;
            this.paso2.BackColor = System.Drawing.Color.Transparent;
            this.paso2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso2.Location = new System.Drawing.Point(155, 177);
            this.paso2.Name = "paso2";
            this.paso2.Size = new System.Drawing.Size(65, 16);
            this.paso2.TabIndex = 10;
            this.paso2.Text = "PASO 2:";
            this.paso2.Visible = false;
            // 
            // paso6
            // 
            this.paso6.AutoSize = true;
            this.paso6.BackColor = System.Drawing.Color.Transparent;
            this.paso6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso6.Location = new System.Drawing.Point(155, 415);
            this.paso6.Name = "paso6";
            this.paso6.Size = new System.Drawing.Size(65, 16);
            this.paso6.TabIndex = 13;
            this.paso6.Text = "PASO 6:";
            this.paso6.Visible = false;
            // 
            // c_paso6
            // 
            this.c_paso6.BackColor = System.Drawing.Color.Yellow;
            this.c_paso6.Enabled = false;
            this.c_paso6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso6.ForeColor = System.Drawing.Color.Navy;
            this.c_paso6.Location = new System.Drawing.Point(196, 431);
            this.c_paso6.Name = "c_paso6";
            this.c_paso6.Size = new System.Drawing.Size(445, 31);
            this.c_paso6.TabIndex = 12;
            this.c_paso6.Text = "CIERRE LA BOVEDA y ESPERE...";
            this.c_paso6.Visible = false;
            // 
            // paso3
            // 
            this.paso3.AutoSize = true;
            this.paso3.BackColor = System.Drawing.Color.Transparent;
            this.paso3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paso3.Location = new System.Drawing.Point(155, 238);
            this.paso3.Name = "paso3";
            this.paso3.Size = new System.Drawing.Size(65, 16);
            this.paso3.TabIndex = 15;
            this.paso3.Text = "PASO 3:";
            this.paso3.Visible = false;
            // 
            // c_paso3
            // 
            this.c_paso3.BackColor = System.Drawing.Color.Yellow;
            this.c_paso3.Enabled = false;
            this.c_paso3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_paso3.ForeColor = System.Drawing.Color.Navy;
            this.c_paso3.Location = new System.Drawing.Point(196, 254);
            this.c_paso3.Name = "c_paso3";
            this.c_paso3.Size = new System.Drawing.Size(445, 31);
            this.c_paso3.TabIndex = 14;
            this.c_paso3.Text = "ABRA LA BOVEDAD";
            this.c_paso3.Visible = false;
            // 
            // c_esperarInicio
            // 
            this.c_esperarInicio.Image = global::@__DemoFID_B.Properties.Resources.emblem_urgent;
            this.c_esperarInicio.Location = new System.Drawing.Point(644, 415);
            this.c_esperarInicio.Name = "c_esperarInicio";
            this.c_esperarInicio.Size = new System.Drawing.Size(55, 47);
            this.c_esperarInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_esperarInicio.TabIndex = 16;
            this.c_esperarInicio.TabStop = false;
            this.c_esperarInicio.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(386, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(402, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "INSTRUCCIONES PARA EL CAMBIO DE BOLSA";
            // 
            // c_empezar
            // 
            this.c_empezar.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_empezar.Location = new System.Drawing.Point(245, 8);
            this.c_empezar.Name = "c_empezar";
            this.c_empezar.Size = new System.Drawing.Size(310, 90);
            this.c_empezar.TabIndex = 18;
            this.c_empezar.Text = "EMPEZAR";
            this.c_empezar.UseVisualStyleBackColor = true;
            this.c_empezar.Click += new System.EventHandler(this.c_empezar_Click);
            // 
            // c_Salir
            // 
            this.c_Salir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Salir.Location = new System.Drawing.Point(288, 465);
            this.c_Salir.Name = "c_Salir";
            this.c_Salir.Size = new System.Drawing.Size(224, 45);
            this.c_Salir.TabIndex = 19;
            this.c_Salir.Text = "SALIR";
            this.c_Salir.UseVisualStyleBackColor = true;
            this.c_Salir.Click += new System.EventHandler(this.c_Salir_Click);
            // 
            // panelSellado1
            // 
            this.panelSellado1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSellado1.Location = new System.Drawing.Point(0, 500);
            this.panelSellado1.Name = "panelSellado1";
            this.panelSellado1.Size = new System.Drawing.Size(800, 50);
            this.panelSellado1.TabIndex = 11;
            this.panelSellado1.Visible = false;
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 0;
            // 
            // c_barcodenuevo
            // 
            this.c_barcodenuevo.AutoSize = true;
            this.c_barcodenuevo.BackColor = System.Drawing.Color.Transparent;
            this.c_barcodenuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_barcodenuevo.Location = new System.Drawing.Point(662, 382);
            this.c_barcodenuevo.Name = "c_barcodenuevo";
            this.c_barcodenuevo.Size = new System.Drawing.Size(76, 20);
            this.c_barcodenuevo.TabIndex = 20;
            this.c_barcodenuevo.Text = "Barcode";
            this.c_barcodenuevo.Visible = false;
            // 
            // c_barcode
            // 
            this.c_barcode.AutoSize = true;
            this.c_barcode.BackColor = System.Drawing.Color.Transparent;
            this.c_barcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_barcode.Location = new System.Drawing.Point(662, 261);
            this.c_barcode.Name = "c_barcode";
            this.c_barcode.Size = new System.Drawing.Size(76, 20);
            this.c_barcode.TabIndex = 21;
            this.c_barcode.Text = "Barcode";
            this.c_barcode.Visible = false;
            // 
            // c_ok
            // 
            this.c_ok.BackColor = System.Drawing.Color.Transparent;
            this.c_ok.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_ok.Location = new System.Drawing.Point(27, 396);
            this.c_ok.Name = "c_ok";
            this.c_ok.Size = new System.Drawing.Size(108, 47);
            this.c_ok.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.c_ok.TabIndex = 22;
            this.c_ok.TabStop = false;
            this.c_ok.Visible = false;
            // 
            // FormCambioBolsa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.c_ok);
            this.Controls.Add(this.c_barcode);
            this.Controls.Add(this.c_barcodenuevo);
            this.Controls.Add(this.c_Salir);
            this.Controls.Add(this.c_empezar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_esperarInicio);
            this.Controls.Add(this.paso3);
            this.Controls.Add(this.c_paso3);
            this.Controls.Add(this.paso6);
            this.Controls.Add(this.c_paso6);
            this.Controls.Add(this.panelSellado1);
            this.Controls.Add(this.paso2);
            this.Controls.Add(this.paso4);
            this.Controls.Add(this.paso5);
            this.Controls.Add(this.paso1);
            this.Controls.Add(this.c_paso5);
            this.Controls.Add(this.c_paso4);
            this.Controls.Add(this.c_paso2);
            this.Controls.Add(this.c_paso1);
            this.Controls.Add(this.c_esperarsellado);
            this.Controls.Add(this.uc_PanelInferior1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCambioBolsa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormCambioBolsa";
            this.Load += new System.EventHandler(this.FormCambioBolsa_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCambioBolsa_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarsellado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_esperarInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_ok)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.PictureBox c_esperarsellado;
        private System.Windows.Forms.TextBox c_paso1;
        private System.Windows.Forms.TextBox c_paso2;
        private System.Windows.Forms.TextBox c_paso4;
        private System.Windows.Forms.TextBox c_paso5;
        private System.Windows.Forms.Label paso1;
        private System.Windows.Forms.Label paso5;
        private System.Windows.Forms.Label paso4;
        private System.Windows.Forms.Label paso2;
        private PanelSellado panelSellado1;
        private System.Windows.Forms.Label paso6;
        private System.Windows.Forms.TextBox c_paso6;
        private System.Windows.Forms.Label paso3;
        private System.Windows.Forms.TextBox c_paso3;
        private System.Windows.Forms.PictureBox c_esperarInicio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_empezar;
        private System.Windows.Forms.Button c_Salir;
        private System.Windows.Forms.Label c_barcodenuevo;
        private System.Windows.Forms.Label c_barcode;
        private System.Windows.Forms.PictureBox c_ok;
    }
}