﻿using Irds;
using System;
using System.Data;
using System.Data.SqlClient;

namespace __DemoFID_B
{

    class BDCliente
    {
        public static int Insertar(String p_Clave, String p_Nombre, String p_Telefono)
        {
            String l_Query
                = "INSERT INTO Cliente "
                + "     ( ClaveCliente  "
                + "     , Nombre  "
                + "     , Telefono ) "
                + "VALUES "
                + "     ( '" + p_Clave + "'  "
                + "     , '" + p_Nombre + "'  "
                + "     , '" + p_Telefono + "' ) ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();

                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);

                if (l_Comando.ExecuteNonQuery() != 1)
                    return 0;
                return 1;
            }
            return 0;
        }

        public static void ValidarClienteExistente(string p_ClaveCliente, string p_Nombre_Cliente, string p_telephone)
        {
            String l_Query
                = "SELECT ClaveCliente "
                + " , Nombre "
                + ", Telefono "
                + "FROM Cliente "
                + "WHERE                    ClaveCliente = @IdCliente ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCliente", p_ClaveCliente);

                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (!l_Lector.Read())
                    Insertar(p_ClaveCliente, p_Nombre_Cliente, p_telephone);
                else
                {
                    ActualizarCliente(p_ClaveCliente, p_Nombre_Cliente, p_telephone);
                }
            }
        }

        public static int ActualizarCliente(string p_ClaveCliente, string p_Nombre_Cliente, string p_telephone)
        {
            p_telephone = "";

            String l_Query
        = "UPDATE           Cliente "
        + "SET           ClaveCliente    = @IdCliente "
        + ",          Nombre     = @NombreCliente "
        + ",           Telefono    = @Telephone "
        + "WHERE          ClaveCliente   = @IdCliente ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdCliente", p_ClaveCliente);
                l_Comando.Parameters.AddWithValue("@NombreCliente", p_Nombre_Cliente);
                l_Comando.Parameters.AddWithValue("@Telephone", p_telephone);

                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception E) { throw; }
            }


            return 0;
        }
        public static DataTable TraerClientes()
        {
            String l_Query
                = "SELECT ClaveCliente "
                + " , Nombre "
                + ", Telefono "
                + "FROM Cliente "
                + "ORDER BY Nombre ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    return UtilsDB.GenerarTabla(l_Query);
                }
                catch (Exception E)
                {

                    throw E;
                }
            }

        }

        public static void EliminarClave(string p_Clave)
        {
            String l_Query1
                = "DELETE "
                + "FROM Cliente "
                + "WHERE ClaveCliente = '" + p_Clave + "' ";
            String l_Query2
               = "DELETE "
               + "FROM Usuario "
               + "WHERE ClaveCliente = '" + p_Clave + "' ";


            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    SqlCommand l_Comando1 = new SqlCommand(l_Query1, l_Conexion);
                    SqlCommand l_Comando2 = new SqlCommand(l_Query2, l_Conexion);

                    l_Comando1.ExecuteNonQuery();
                    l_Comando2.ExecuteNonQuery();
                }
                catch (Exception E)
                {

                    throw E;
                }
            }

        }

        public static bool TraerCliente(String p_Clave
                                , out String p_Nombre, out String p_Telefono)
        {
            String l_Query
                = "SELECT ClaveCliente "
                + " , Nombre "
                + ", Telefono "
                + "FROM Cliente "
                + "WHERE ClaveCliente = '" + p_Clave + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    DataTable l_Datos = UtilsDB.GenerarTabla(l_Query);

                    if ((l_Datos != null)
                        || (l_Datos.Rows.Count != 0))
                    {

                        p_Nombre = l_Datos.Rows[0]["Nombre"].ToString();
                        p_Telefono = l_Datos.Rows[0]["Telefono"].ToString();
                        return true;
                    }
                }
                catch (Exception E)
                {
                    throw E;
                }
            }
            p_Nombre = p_Telefono = "";

            return false;
        }
    }
}
