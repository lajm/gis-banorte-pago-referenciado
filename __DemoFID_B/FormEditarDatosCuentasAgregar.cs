﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarDatosCuentasAgregar : Form
    {
        public enum Modo { Agregar, Editar };

        Modo m_Modo;

        string ClientID;
        public FormEditarDatosCuentasAgregar( string text )
        {
            InitializeComponent();
            ClientID = text;
   
        }

        public FormEditarDatosCuentasAgregar()
        {
            InitializeComponent();

        }

       /* public static DialogResult EjecutarAgregar( out String p_Clave )
        {
            using ( FormEditarDatosOperadoresAgregar l_Forma = new FormEditarDatosOperadoresAgregar() )
            {
                l_Forma.m_Modo = Modo.Agregar;
                DialogResult l_Resultado = l_Forma.ShowDialog();
                p_Clave = l_Forma.c_Clave.Text;

                return l_Resultado;
            }
        }


        public static DialogResult EjecutarEditar( String p_Clave )
        {
            using ( FormEditarDatosGeneralesCliente l_Forma = new FormEditarDatosGeneralesCliente() )
            {
                l_Forma.m_Modo = Modo.Editar;
                if ( l_Forma.CargaUsuario( p_Clave ) )
                {
                    l_Forma.c_Clave.ReadOnly = true;
                    return l_Forma.ShowDialog();
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }

        public bool CargaUsuario( String p_Clave )
        {
            String l_Nombre, l_Telefono;

            if ( BDCliente.TraerCliente( p_Clave, out l_Nombre, out l_Telefono ) )
            {
                c_Clave.Text = p_Clave;
                c_Nombre.Text = l_Nombre;
                c_Telefono.Text = l_Telefono;
                return true;
            }

            return false;
        }*/

        private void c_BAceptar_Click( object sender, EventArgs e )
        {
            if ( m_Modo == Modo.Agregar )
            {
               
                int l_IdCuenta =  BDCuentaOperador.Insertar2( c_Alias.Text, c_Banco.Text,c_Cuenta.Text,c_Status.Checked,c_AcpRef.Checked, ClientID );
                DialogResult = DialogResult.OK;
            }
            else
            {
                // TODO: Hacer
            }
        }

     
        int l_idTeclado = 0;
        private void pictureBox1_Click( object sender, EventArgs e )
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_IdCuenta_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_IdCuenta.Text = l_teclado.Captura;

            }
        }

        private void c_Alias_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Alias.Text = l_teclado.Captura;

            }
        }

        private void c_Banco_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Banco.Text = l_teclado.Captura;

            }
        }

        private void c_Cuenta_Click( object sender, EventArgs e )
        {
            using ( FormTeclado l_teclado = new FormTeclado() )
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if ( l_respuesta == DialogResult.OK )
                    c_Cuenta.Text = l_teclado.Captura;

            }
        }

        private void c_Cancelar_Click( object sender, EventArgs e )
        {
            this.Close();
        }
    }
}
