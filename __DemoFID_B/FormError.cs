﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormError : Form
    {
        bool l_cierreAutomatico = false;

        public FormError(bool l_opcioncerrar)
        {
            l_cierreAutomatico = l_opcioncerrar;
            InitializeComponent();
        }

        private void FormError_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
            if (l_cierreAutomatico)
                timer1.Start();
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
