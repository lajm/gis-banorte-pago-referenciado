﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace __DemoFID_B
{
    public partial class FormCambioNumeroBolsa : Form
    {
        public FormCambioNumeroBolsa()
        {
            InitializeComponent();
        }

        private void c_IdUsuario_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                   c_NumeroBolsa.Text = l_teclado.Captura;
                   
                        

            }
        }

        private bool EsNumero(string cadena)
        {
            //Sencillamente, si se logra hacer la conversión, entonces es número
            try
            {
                decimal resp = Convert.ToDecimal(cadena);
                return true;
            }
            catch //caso contrario, es falso.
            {
                return false;
            }

        }

        private void c_Aceptar_Click(object sender, EventArgs e)
        {

            if (EsNumero(c_NumeroBolsa.Text))
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Esta seguro de SOBRESCRIBIR EL NUMERO De BOlSA, Se registrara este Evento";
                    f_Error.c_Imagen.Visible = true;
                   DialogResult l_respuesta  =  f_Error.ShowDialog();

                   if (l_respuesta == DialogResult.OK)
                   {
                       EscribirNumeroDeBOLSA(c_NumeroBolsa.Text);

                       using (FormError f_aviso = new FormError(false))
                       {
                           f_aviso.c_MensajeError.Text = "Se ha cambiado el numero de Bolsa";
                           f_aviso.c_Imagen.Visible = false;
                           f_aviso.ShowDialog();
                       }
                   }

                }

                Close();

            }
            else
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Se esperan solo numeros, Escriba nuevamente el # de Bolsa";
                    f_Error.c_Imagen.Visible = false;
                    f_Error.ShowDialog();
                }
            }


        }

        public void
          EscribirNumeroDeBOLSA(String p_Mensaje)
        {
            using (StreamWriter l_Archivo = File.CreateText("NumeroBOLSA.txt"))
            {
                l_Archivo.WriteLine(p_Mensaje);
            }
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormCambioNumeroBolsa_Load(object sender, EventArgs e)
        {
            Globales.LeerBolsaPuesta();
            c_actualnumBolsa.Text  = Globales.NumeroSerieBOLSA;
        }
    }
}
