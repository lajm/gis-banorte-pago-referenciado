﻿namespace __DemoFID_B
{
    partial class FormConsultaEfectivoDolares
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsultaEfectivoDolares));
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1 = new System.Windows.Forms.Panel();
            this.c_Cajon1_50 = new System.Windows.Forms.Label();
            this.c_Cajon1_20 = new System.Windows.Forms.Label();
            this.c_Cajon1_10 = new System.Windows.Forms.Label();
            this.c_Cajon1_5 = new System.Windows.Forms.Label();
            this.c_Cajon1_2 = new System.Windows.Forms.Label();
            this.c_Cajon1_1 = new System.Windows.Forms.Label();
            this.c_TotalCajon1 = new System.Windows.Forms.Label();
            this.c_Cajon2_50 = new System.Windows.Forms.Label();
            this.c_Cajon2_20 = new System.Windows.Forms.Label();
            this.c_Cajon2_10 = new System.Windows.Forms.Label();
            this.c_Cajon2_5 = new System.Windows.Forms.Label();
            this.c_Cajon2_2 = new System.Windows.Forms.Label();
            this.c_Cajon2_1 = new System.Windows.Forms.Label();
            this.c_TotalCajon2 = new System.Windows.Forms.Label();
            this.c_Cajon3_50 = new System.Windows.Forms.Label();
            this.c_Cajon3_20 = new System.Windows.Forms.Label();
            this.c_Cajon3_10 = new System.Windows.Forms.Label();
            this.c_Cajon3_5 = new System.Windows.Forms.Label();
            this.c_Cajon3_2 = new System.Windows.Forms.Label();
            this.c_Cajon3_1 = new System.Windows.Forms.Label();
            this.c_TotalCajon3 = new System.Windows.Forms.Label();
            this.c_Cajon4_50 = new System.Windows.Forms.Label();
            this.c_Cajon4_20 = new System.Windows.Forms.Label();
            this.c_Cajon4_10 = new System.Windows.Forms.Label();
            this.c_Cajon4_5 = new System.Windows.Forms.Label();
            this.c_Cajon4_2 = new System.Windows.Forms.Label();
            this.c_Cajon4_1 = new System.Windows.Forms.Label();
            this.c_TotalCajon4 = new System.Windows.Forms.Label();
            this.c_Cerrar = new System.Windows.Forms.Button();
            this.c_Imprimir = new System.Windows.Forms.Button();
            this.c_Cajon1_100 = new System.Windows.Forms.Label();
            this.c_Cajon2_100 = new System.Windows.Forms.Label();
            this.c_Cajon3_100 = new System.Windows.Forms.Label();
            this.c_Cajon4_100 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_Cajon4_100);
            this.panel1.Controls.Add(this.c_Cajon3_100);
            this.panel1.Controls.Add(this.c_Cajon2_100);
            this.panel1.Controls.Add(this.c_Cajon1_100);
            this.panel1.Controls.Add(this.c_Imprimir);
            this.panel1.Controls.Add(this.c_Cerrar);
            this.panel1.Controls.Add(this.c_TotalCajon4);
            this.panel1.Controls.Add(this.c_Cajon4_1);
            this.panel1.Controls.Add(this.c_Cajon4_2);
            this.panel1.Controls.Add(this.c_Cajon4_5);
            this.panel1.Controls.Add(this.c_Cajon4_10);
            this.panel1.Controls.Add(this.c_Cajon4_20);
            this.panel1.Controls.Add(this.c_Cajon4_50);
            this.panel1.Controls.Add(this.c_TotalCajon3);
            this.panel1.Controls.Add(this.c_Cajon3_1);
            this.panel1.Controls.Add(this.c_Cajon3_2);
            this.panel1.Controls.Add(this.c_Cajon3_5);
            this.panel1.Controls.Add(this.c_Cajon3_10);
            this.panel1.Controls.Add(this.c_Cajon3_20);
            this.panel1.Controls.Add(this.c_Cajon3_50);
            this.panel1.Controls.Add(this.c_TotalCajon2);
            this.panel1.Controls.Add(this.c_Cajon2_1);
            this.panel1.Controls.Add(this.c_Cajon2_2);
            this.panel1.Controls.Add(this.c_Cajon2_5);
            this.panel1.Controls.Add(this.c_Cajon2_10);
            this.panel1.Controls.Add(this.c_Cajon2_20);
            this.panel1.Controls.Add(this.c_Cajon2_50);
            this.panel1.Controls.Add(this.c_TotalCajon1);
            this.panel1.Controls.Add(this.c_Cajon1_1);
            this.panel1.Controls.Add(this.c_Cajon1_2);
            this.panel1.Controls.Add(this.c_Cajon1_5);
            this.panel1.Controls.Add(this.c_Cajon1_10);
            this.panel1.Controls.Add(this.c_Cajon1_20);
            this.panel1.Controls.Add(this.c_Cajon1_50);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            // 
            // c_Cajon1_50
            // 
            this.c_Cajon1_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_50.Location = new System.Drawing.Point(230, 266);
            this.c_Cajon1_50.Name = "c_Cajon1_50";
            this.c_Cajon1_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_50.TabIndex = 39;
            this.c_Cajon1_50.Text = "0";
            this.c_Cajon1_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon1_20
            // 
            this.c_Cajon1_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_20.Location = new System.Drawing.Point(230, 295);
            this.c_Cajon1_20.Name = "c_Cajon1_20";
            this.c_Cajon1_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_20.TabIndex = 40;
            this.c_Cajon1_20.Text = "0";
            this.c_Cajon1_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon1_10
            // 
            this.c_Cajon1_10.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_10.Location = new System.Drawing.Point(230, 324);
            this.c_Cajon1_10.Name = "c_Cajon1_10";
            this.c_Cajon1_10.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_10.TabIndex = 41;
            this.c_Cajon1_10.Text = "0";
            this.c_Cajon1_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon1_5
            // 
            this.c_Cajon1_5.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_5.Location = new System.Drawing.Point(230, 353);
            this.c_Cajon1_5.Name = "c_Cajon1_5";
            this.c_Cajon1_5.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_5.TabIndex = 42;
            this.c_Cajon1_5.Text = "0";
            this.c_Cajon1_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon1_2
            // 
            this.c_Cajon1_2.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_2.Location = new System.Drawing.Point(230, 383);
            this.c_Cajon1_2.Name = "c_Cajon1_2";
            this.c_Cajon1_2.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_2.TabIndex = 43;
            this.c_Cajon1_2.Text = "0";
            this.c_Cajon1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon1_1
            // 
            this.c_Cajon1_1.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_1.Location = new System.Drawing.Point(230, 412);
            this.c_Cajon1_1.Name = "c_Cajon1_1";
            this.c_Cajon1_1.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon1_1.TabIndex = 44;
            this.c_Cajon1_1.Text = "0";
            this.c_Cajon1_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_TotalCajon1
            // 
            this.c_TotalCajon1.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon1.Location = new System.Drawing.Point(214, 446);
            this.c_TotalCajon1.Name = "c_TotalCajon1";
            this.c_TotalCajon1.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon1.TabIndex = 45;
            this.c_TotalCajon1.Text = "$0.00";
            this.c_TotalCajon1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_50
            // 
            this.c_Cajon2_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_50.Location = new System.Drawing.Point(374, 266);
            this.c_Cajon2_50.Name = "c_Cajon2_50";
            this.c_Cajon2_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_50.TabIndex = 46;
            this.c_Cajon2_50.Text = "0";
            this.c_Cajon2_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_20
            // 
            this.c_Cajon2_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_20.Location = new System.Drawing.Point(374, 295);
            this.c_Cajon2_20.Name = "c_Cajon2_20";
            this.c_Cajon2_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_20.TabIndex = 47;
            this.c_Cajon2_20.Text = "0";
            this.c_Cajon2_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_10
            // 
            this.c_Cajon2_10.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_10.Location = new System.Drawing.Point(374, 324);
            this.c_Cajon2_10.Name = "c_Cajon2_10";
            this.c_Cajon2_10.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_10.TabIndex = 48;
            this.c_Cajon2_10.Text = "0";
            this.c_Cajon2_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_5
            // 
            this.c_Cajon2_5.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_5.Location = new System.Drawing.Point(374, 353);
            this.c_Cajon2_5.Name = "c_Cajon2_5";
            this.c_Cajon2_5.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_5.TabIndex = 49;
            this.c_Cajon2_5.Text = "0";
            this.c_Cajon2_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_2
            // 
            this.c_Cajon2_2.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_2.Location = new System.Drawing.Point(374, 383);
            this.c_Cajon2_2.Name = "c_Cajon2_2";
            this.c_Cajon2_2.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_2.TabIndex = 50;
            this.c_Cajon2_2.Text = "0";
            this.c_Cajon2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_1
            // 
            this.c_Cajon2_1.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_1.Location = new System.Drawing.Point(374, 412);
            this.c_Cajon2_1.Name = "c_Cajon2_1";
            this.c_Cajon2_1.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_1.TabIndex = 51;
            this.c_Cajon2_1.Text = "0";
            this.c_Cajon2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_TotalCajon2
            // 
            this.c_TotalCajon2.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon2.Location = new System.Drawing.Point(358, 446);
            this.c_TotalCajon2.Name = "c_TotalCajon2";
            this.c_TotalCajon2.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon2.TabIndex = 52;
            this.c_TotalCajon2.Text = "$0.00";
            this.c_TotalCajon2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_50
            // 
            this.c_Cajon3_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_50.Location = new System.Drawing.Point(518, 266);
            this.c_Cajon3_50.Name = "c_Cajon3_50";
            this.c_Cajon3_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_50.TabIndex = 53;
            this.c_Cajon3_50.Text = "0";
            this.c_Cajon3_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_20
            // 
            this.c_Cajon3_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_20.Location = new System.Drawing.Point(518, 295);
            this.c_Cajon3_20.Name = "c_Cajon3_20";
            this.c_Cajon3_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_20.TabIndex = 54;
            this.c_Cajon3_20.Text = "0";
            this.c_Cajon3_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_10
            // 
            this.c_Cajon3_10.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_10.Location = new System.Drawing.Point(518, 324);
            this.c_Cajon3_10.Name = "c_Cajon3_10";
            this.c_Cajon3_10.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_10.TabIndex = 55;
            this.c_Cajon3_10.Text = "0";
            this.c_Cajon3_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_5
            // 
            this.c_Cajon3_5.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_5.Location = new System.Drawing.Point(518, 353);
            this.c_Cajon3_5.Name = "c_Cajon3_5";
            this.c_Cajon3_5.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_5.TabIndex = 56;
            this.c_Cajon3_5.Text = "0";
            this.c_Cajon3_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_2
            // 
            this.c_Cajon3_2.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_2.Location = new System.Drawing.Point(518, 383);
            this.c_Cajon3_2.Name = "c_Cajon3_2";
            this.c_Cajon3_2.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_2.TabIndex = 57;
            this.c_Cajon3_2.Text = "0";
            this.c_Cajon3_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_1
            // 
            this.c_Cajon3_1.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_1.Location = new System.Drawing.Point(518, 412);
            this.c_Cajon3_1.Name = "c_Cajon3_1";
            this.c_Cajon3_1.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_1.TabIndex = 58;
            this.c_Cajon3_1.Text = "0";
            this.c_Cajon3_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_TotalCajon3
            // 
            this.c_TotalCajon3.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon3.Location = new System.Drawing.Point(502, 446);
            this.c_TotalCajon3.Name = "c_TotalCajon3";
            this.c_TotalCajon3.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon3.TabIndex = 59;
            this.c_TotalCajon3.Text = "$0.00";
            this.c_TotalCajon3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_50
            // 
            this.c_Cajon4_50.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_50.Location = new System.Drawing.Point(659, 266);
            this.c_Cajon4_50.Name = "c_Cajon4_50";
            this.c_Cajon4_50.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_50.TabIndex = 60;
            this.c_Cajon4_50.Text = "0";
            this.c_Cajon4_50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_20
            // 
            this.c_Cajon4_20.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_20.Location = new System.Drawing.Point(659, 295);
            this.c_Cajon4_20.Name = "c_Cajon4_20";
            this.c_Cajon4_20.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_20.TabIndex = 61;
            this.c_Cajon4_20.Text = "0";
            this.c_Cajon4_20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_10
            // 
            this.c_Cajon4_10.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_10.Location = new System.Drawing.Point(659, 324);
            this.c_Cajon4_10.Name = "c_Cajon4_10";
            this.c_Cajon4_10.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_10.TabIndex = 62;
            this.c_Cajon4_10.Text = "0";
            this.c_Cajon4_10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_5
            // 
            this.c_Cajon4_5.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_5.Location = new System.Drawing.Point(659, 353);
            this.c_Cajon4_5.Name = "c_Cajon4_5";
            this.c_Cajon4_5.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_5.TabIndex = 63;
            this.c_Cajon4_5.Text = "0";
            this.c_Cajon4_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_2
            // 
            this.c_Cajon4_2.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_2.Location = new System.Drawing.Point(659, 383);
            this.c_Cajon4_2.Name = "c_Cajon4_2";
            this.c_Cajon4_2.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_2.TabIndex = 64;
            this.c_Cajon4_2.Text = "0";
            this.c_Cajon4_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_1
            // 
            this.c_Cajon4_1.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_1.Location = new System.Drawing.Point(659, 412);
            this.c_Cajon4_1.Name = "c_Cajon4_1";
            this.c_Cajon4_1.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_1.TabIndex = 65;
            this.c_Cajon4_1.Text = "0";
            this.c_Cajon4_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_TotalCajon4
            // 
            this.c_TotalCajon4.BackColor = System.Drawing.Color.Transparent;
            this.c_TotalCajon4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_TotalCajon4.Location = new System.Drawing.Point(643, 446);
            this.c_TotalCajon4.Name = "c_TotalCajon4";
            this.c_TotalCajon4.Size = new System.Drawing.Size(117, 24);
            this.c_TotalCajon4.TabIndex = 66;
            this.c_TotalCajon4.Text = "$0.00";
            this.c_TotalCajon4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cerrar
            // 
            this.c_Cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Cerrar.Location = new System.Drawing.Point(403, 503);
            this.c_Cerrar.Name = "c_Cerrar";
            this.c_Cerrar.Size = new System.Drawing.Size(143, 41);
            this.c_Cerrar.TabIndex = 67;
            this.c_Cerrar.Text = "Cerrar";
            this.c_Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Cerrar.UseVisualStyleBackColor = true;
            this.c_Cerrar.Click += new System.EventHandler(this.c_Cerrar_Click);
            // 
            // c_Imprimir
            // 
            this.c_Imprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Imprimir.Location = new System.Drawing.Point(254, 503);
            this.c_Imprimir.Name = "c_Imprimir";
            this.c_Imprimir.Size = new System.Drawing.Size(143, 41);
            this.c_Imprimir.TabIndex = 68;
            this.c_Imprimir.Text = "Imprimir";
            this.c_Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Imprimir.UseVisualStyleBackColor = true;
            this.c_Imprimir.Click += new System.EventHandler(this.c_Imprimir_Click);
            // 
            // c_Cajon1_100
            // 
            this.c_Cajon1_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon1_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon1_100.Location = new System.Drawing.Point(230, 240);
            this.c_Cajon1_100.Name = "c_Cajon1_100";
            this.c_Cajon1_100.Size = new System.Drawing.Size(98, 27);
            this.c_Cajon1_100.TabIndex = 69;
            this.c_Cajon1_100.Text = "0";
            this.c_Cajon1_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon2_100
            // 
            this.c_Cajon2_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon2_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon2_100.Location = new System.Drawing.Point(374, 240);
            this.c_Cajon2_100.Name = "c_Cajon2_100";
            this.c_Cajon2_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon2_100.TabIndex = 70;
            this.c_Cajon2_100.Text = "0";
            this.c_Cajon2_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon3_100
            // 
            this.c_Cajon3_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon3_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon3_100.Location = new System.Drawing.Point(518, 240);
            this.c_Cajon3_100.Name = "c_Cajon3_100";
            this.c_Cajon3_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon3_100.TabIndex = 71;
            this.c_Cajon3_100.Text = "0";
            this.c_Cajon3_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // c_Cajon4_100
            // 
            this.c_Cajon4_100.BackColor = System.Drawing.Color.Transparent;
            this.c_Cajon4_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cajon4_100.Location = new System.Drawing.Point(659, 240);
            this.c_Cajon4_100.Name = "c_Cajon4_100";
            this.c_Cajon4_100.Size = new System.Drawing.Size(98, 24);
            this.c_Cajon4_100.TabIndex = 72;
            this.c_Cajon4_100.Text = "0";
            this.c_Cajon4_100.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(246, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 29);
            this.label1.TabIndex = 73;
            this.label1.Text = "CONSULTA DE EFECTIVO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 24);
            this.label2.TabIndex = 74;
            this.label2.Text = "Denominación";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(254, 202);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 24);
            this.label9.TabIndex = 75;
            this.label9.Text = "Cajón 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(398, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 24);
            this.label10.TabIndex = 76;
            this.label10.Text = "Cajón 2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(542, 202);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 24);
            this.label11.TabIndex = 77;
            this.label11.Text = "Cajón 3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(683, 202);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 24);
            this.label12.TabIndex = 78;
            this.label12.Text = "Cajón 4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(54, 240);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 24);
            this.label3.TabIndex = 79;
            this.label3.Text = "$100.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(64, 266);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 24);
            this.label4.TabIndex = 80;
            this.label4.Text = "$50.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(64, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 24);
            this.label5.TabIndex = 81;
            this.label5.Text = "$20.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(64, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 24);
            this.label6.TabIndex = 82;
            this.label6.Text = "$10.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(74, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 24);
            this.label7.TabIndex = 83;
            this.label7.Text = "$5.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(74, 383);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 24);
            this.label8.TabIndex = 84;
            this.label8.Text = "$2.00";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(148, 446);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 24);
            this.label13.TabIndex = 85;
            this.label13.Text = "Totales:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(74, 412);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 24);
            this.label14.TabIndex = 86;
            this.label14.Text = "$1.00";
            // 
            // FormConsultaEfectivoDolares
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormConsultaEfectivoDolares";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormConsultaEfectivoDolares";
            this.Load += new System.EventHandler(this.FormConsultaEfectivoDolares_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label c_Cajon4_100;
        private System.Windows.Forms.Label c_Cajon3_100;
        private System.Windows.Forms.Label c_Cajon2_100;
        private System.Windows.Forms.Label c_Cajon1_100;
        private System.Windows.Forms.Button c_Imprimir;
        private System.Windows.Forms.Button c_Cerrar;
        private System.Windows.Forms.Label c_TotalCajon4;
        private System.Windows.Forms.Label c_Cajon4_1;
        private System.Windows.Forms.Label c_Cajon4_2;
        private System.Windows.Forms.Label c_Cajon4_5;
        private System.Windows.Forms.Label c_Cajon4_10;
        private System.Windows.Forms.Label c_Cajon4_20;
        private System.Windows.Forms.Label c_Cajon4_50;
        private System.Windows.Forms.Label c_TotalCajon3;
        private System.Windows.Forms.Label c_Cajon3_1;
        private System.Windows.Forms.Label c_Cajon3_2;
        private System.Windows.Forms.Label c_Cajon3_5;
        private System.Windows.Forms.Label c_Cajon3_10;
        private System.Windows.Forms.Label c_Cajon3_20;
        private System.Windows.Forms.Label c_Cajon3_50;
        private System.Windows.Forms.Label c_TotalCajon2;
        private System.Windows.Forms.Label c_Cajon2_1;
        private System.Windows.Forms.Label c_Cajon2_2;
        private System.Windows.Forms.Label c_Cajon2_5;
        private System.Windows.Forms.Label c_Cajon2_10;
        private System.Windows.Forms.Label c_Cajon2_20;
        private System.Windows.Forms.Label c_Cajon2_50;
        private System.Windows.Forms.Label c_TotalCajon1;
        private System.Windows.Forms.Label c_Cajon1_1;
        private System.Windows.Forms.Label c_Cajon1_2;
        private System.Windows.Forms.Label c_Cajon1_5;
        private System.Windows.Forms.Label c_Cajon1_10;
        private System.Windows.Forms.Label c_Cajon1_20;
        private System.Windows.Forms.Label c_Cajon1_50;
    }
}