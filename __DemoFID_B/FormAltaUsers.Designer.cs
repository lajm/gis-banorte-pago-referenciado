﻿namespace __DemoFID_B
{
    partial class FormAltaUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Alta = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_confirmacionContraseña = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Contraseña = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_IdUsuario = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.c_perfiles = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.c_referencia = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.c_convenio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(373, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Rellene todos los campos";
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(354, 299);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 6;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Alta
            // 
            this.c_Alta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Alta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Alta.Location = new System.Drawing.Point(123, 299);
            this.c_Alta.Name = "c_Alta";
            this.c_Alta.Size = new System.Drawing.Size(122, 47);
            this.c_Alta.TabIndex = 5;
            this.c_Alta.Text = "Dar de Alta";
            this.c_Alta.UseVisualStyleBackColor = true;
            this.c_Alta.Click += new System.EventHandler(this.c_Alta_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(42, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(217, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "ALTA DE NUEVO OPERADOR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(84, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Confirme Contraseña";
            this.label3.Visible = false;
            // 
            // c_confirmacionContraseña
            // 
            this.c_confirmacionContraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_confirmacionContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmacionContraseña.Location = new System.Drawing.Point(241, 270);
            this.c_confirmacionContraseña.Name = "c_confirmacionContraseña";
            this.c_confirmacionContraseña.PasswordChar = '*';
            this.c_confirmacionContraseña.Size = new System.Drawing.Size(180, 22);
            this.c_confirmacionContraseña.TabIndex = 4;
            this.c_confirmacionContraseña.Visible = false;
            this.c_confirmacionContraseña.Click += new System.EventHandler(this.c_confirmacionContraseña_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(148, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "Contraseña";
            this.label2.Visible = false;
            // 
            // c_Contraseña
            // 
            this.c_Contraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_Contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Contraseña.Location = new System.Drawing.Point(242, 245);
            this.c_Contraseña.Name = "c_Contraseña";
            this.c_Contraseña.PasswordChar = '*';
            this.c_Contraseña.Size = new System.Drawing.Size(180, 22);
            this.c_Contraseña.TabIndex = 3;
            this.c_Contraseña.Visible = false;
            this.c_Contraseña.Click += new System.EventHandler(this.c_Contraseña_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Numero de Empleado";
            // 
            // c_IdUsuario
            // 
            this.c_IdUsuario.BackColor = System.Drawing.SystemColors.Info;
            this.c_IdUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_IdUsuario.Location = new System.Drawing.Point(226, 102);
            this.c_IdUsuario.Name = "c_IdUsuario";
            this.c_IdUsuario.Size = new System.Drawing.Size(346, 22);
            this.c_IdUsuario.TabIndex = 0;
            this.c_IdUsuario.Click += new System.EventHandler(this.c_IdUsuario_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Nombre del EMPLEADO";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.SystemColors.Info;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(224, 157);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.Size = new System.Drawing.Size(346, 22);
            this.c_Nombre.TabIndex = 2;
            this.c_Nombre.Click += new System.EventHandler(this.c_Nombre_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(168, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Perfil";
            // 
            // c_perfiles
            // 
            this.c_perfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_perfiles.FormattingEnabled = true;
            this.c_perfiles.Location = new System.Drawing.Point(226, 130);
            this.c_perfiles.Name = "c_perfiles";
            this.c_perfiles.Size = new System.Drawing.Size(180, 24);
            this.c_perfiles.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(502, 277);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // c_referencia
            // 
            this.c_referencia.BackColor = System.Drawing.SystemColors.Info;
            this.c_referencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_referencia.Location = new System.Drawing.Point(223, 187);
            this.c_referencia.Name = "c_referencia";
            this.c_referencia.Size = new System.Drawing.Size(285, 22);
            this.c_referencia.TabIndex = 33;
            this.c_referencia.Click += new System.EventHandler(this.c_referencia_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(130, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 34;
            this.label8.Text = "Referencia";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(130, 227);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 16);
            this.label9.TabIndex = 36;
            this.label9.Text = "Convenio";
            // 
            // c_convenio
            // 
            this.c_convenio.BackColor = System.Drawing.SystemColors.Info;
            this.c_convenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_convenio.Location = new System.Drawing.Point(223, 221);
            this.c_convenio.Name = "c_convenio";
            this.c_convenio.Size = new System.Drawing.Size(285, 22);
            this.c_convenio.TabIndex = 35;
            this.c_convenio.Click += new System.EventHandler(this.c_convenio_TextChanged);
            // 
            // FormAltaUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 370);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.c_convenio);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.c_referencia);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_perfiles);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Nombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Alta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_confirmacionContraseña);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Contraseña);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_IdUsuario);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAltaUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAltaUsers";
            this.Load += new System.EventHandler(this.FormAltaUsers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Alta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_confirmacionContraseña;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_Contraseña;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_IdUsuario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox c_perfiles;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox c_referencia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox c_convenio;
    }
}