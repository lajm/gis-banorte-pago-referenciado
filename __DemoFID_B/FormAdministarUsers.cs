﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormAdministarUsers : Form
    {
        public FormAdministarUsers()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            switch (listView1.FocusedItem.Index)
            {
                case 0:
                    using (FormAltaUsers f_alta = new FormAltaUsers ())
                    {
                        f_alta.ShowDialog();
                    }
                    break;
                case 1:
                    using (FormModificarUsuario f_mod = new FormModificarUsuario ())
                    {
                        f_mod.ShowDialog();
                    }

                    break;
                case 2:
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text ="Administracion de ETVs No Necesaria por el momento";
                        f_Error.ShowDialog();

                        return;
                    }

                    break;
                case 3:
                    using (FormCambiarContraseña2 f_cambio = new FormCambiarContraseña2())
                    {
                        f_cambio.ShowDialog();
                    }

                    break;
                case 4:
                    using (FormBajaUsuarios f_baja = new FormBajaUsuarios())
                    {
                        f_baja.ShowDialog();
                    }
                    break;
                case 5:
                    Close();
                    break;
            }

        }
    }
}
