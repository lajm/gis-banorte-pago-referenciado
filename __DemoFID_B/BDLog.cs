﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace __DemoFID_B
{
    public class BDLog
    {
        public static void
            InsertarLog(int p_DialogId, int p_ReturnValue, String p_Mensaje)
        {
            String l_Query
                = "INSERT INTO              LogRespuestas "
                + "                         (FechaHora, DialogId, ReturnValue, Mensaje) "
                + "VALUES                   (@FechaHora, @DialogId, @ReturnValue, @Mensaje) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                    l_Comando.Parameters.AddWithValue("@DialogId", p_DialogId);
                    l_Comando.Parameters.AddWithValue("@ReturnValue", p_ReturnValue);
                    l_Comando.Parameters.AddWithValue("@Mensaje", p_Mensaje);
                    l_Comando.ExecuteNonQuery();

                    Globales.EscribirBitacora("Comunicaciones", "INSERTAR LOG", "Exito",1);
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Comunicaciones","INSERTAR LOG","Fallo Conexion: "+ ex.ToString(),1);
                }
            }
        }
    }
}
