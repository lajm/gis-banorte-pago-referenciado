﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEmpleado : Form
    {

        String l_Identificacion = "";
        String l_Empresa = "";

        public String Empresa
        {
            get { return l_Empresa; }
            set { l_Empresa = value; }
        }
    

        public FormEmpleado()
        {
            InitializeComponent();
        }

       

        private void FormTameme_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
        }

        private void Oprimir_tecla(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Identificacion += "0";
                c_EtiquetaPassword.Text += "0";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Identificacion += "1";
                c_EtiquetaPassword.Text += "1";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Identificacion += "2";
                c_EtiquetaPassword.Text += "2";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Identificacion += "3";
                c_EtiquetaPassword.Text += "3";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Identificacion += "4";
                c_EtiquetaPassword.Text += "4";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Identificacion += "5";
                c_EtiquetaPassword.Text += "5";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Identificacion += "6";
                c_EtiquetaPassword.Text += "6";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Identificacion += "7";
                c_EtiquetaPassword.Text += "7";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Identificacion += "8";
                c_EtiquetaPassword.Text += "8";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Identificacion += "9";
                c_EtiquetaPassword.Text += "9";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Identificacion.Length != 0)
                {
                    l_Identificacion = l_Identificacion.Substring(0, l_Identificacion.Length - 1);
                    c_EtiquetaPassword.Text = c_EtiquetaPassword.Text.Substring(0, l_Identificacion.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                if (l_Identificacion.Length > 0)
                {

                   if (!Globales.ExisteUsuario(l_Identificacion))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "El Usuario No Existe";
                            f_Error.ShowDialog();
                        }

                    }else

                        //Globales.IdUsuario = l_Identificacion;


                        using (FormSesion f_Inicio = new FormSesion())
                        {
                            
                            f_Inicio.l_IdUsuario = l_Identificacion;
                            f_Inicio.ShowDialog();
                        }



                    Close();
                    
                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir un numero de Empleado para continuar";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
        }

      
        
    }
}
