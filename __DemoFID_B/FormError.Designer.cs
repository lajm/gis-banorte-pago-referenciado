﻿namespace __DemoFID_B
{
    partial class FormError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.c_Imagen = new System.Windows.Forms.PictureBox();
            this.c_MensajeError = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.c_Imagen)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Imagen
            // 
            this.c_Imagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.c_Imagen.Image = global::@__DemoFID_B.Properties.Resources.warning;
            this.c_Imagen.Location = new System.Drawing.Point(12, 242);
            this.c_Imagen.Name = "c_Imagen";
            this.c_Imagen.Size = new System.Drawing.Size(32, 32);
            this.c_Imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.c_Imagen.TabIndex = 0;
            this.c_Imagen.TabStop = false;
            // 
            // c_MensajeError
            // 
            this.c_MensajeError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.c_MensajeError.BackColor = System.Drawing.Color.Transparent;
            this.c_MensajeError.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_MensajeError.Location = new System.Drawing.Point(12, 56);
            this.c_MensajeError.Name = "c_MensajeError";
            this.c_MensajeError.Size = new System.Drawing.Size(367, 159);
            this.c_MensajeError.TabIndex = 1;
            this.c_MensajeError.Text = "label1";
            this.c_MensajeError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_BotonAceptar.Location = new System.Drawing.Point(68, 208);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(120, 75);
            this.c_BotonAceptar.TabIndex = 24;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "ACEPTAR";
            this.c_BotonAceptar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.c_BotonAceptar_Click);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.c_BotonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Image = global::@__DemoFID_B.Properties.Resources.block;
            this.c_BotonCancelar.Location = new System.Drawing.Point(215, 208);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(120, 75);
            this.c_BotonCancelar.TabIndex = 25;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.Text = "CANCELAR";
            this.c_BotonCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_BotonCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.c_BotonCancelar_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormError
            // 
            this.AcceptButton = this.c_BotonAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.c_BotonCancelar;
            this.ClientSize = new System.Drawing.Size(391, 286);
            this.ControlBox = false;
            this.Controls.Add(this.c_BotonCancelar);
            this.Controls.Add(this.c_BotonAceptar);
            this.Controls.Add(this.c_Imagen);
            this.Controls.Add(this.c_MensajeError);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormError_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c_Imagen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label c_MensajeError;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Button c_BotonCancelar;
        public System.Windows.Forms.PictureBox c_Imagen;
        private System.Windows.Forms.Timer timer1;
    }
}