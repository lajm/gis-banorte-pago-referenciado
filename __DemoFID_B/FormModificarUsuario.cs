﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormModificarUsuario : Form
    {
        DataTable t_Usuarios;
        DataTable t_perfil;
        String l_usuario;
        int l_idTeclado=0;

        public FormModificarUsuario()
        {
            InitializeComponent();
        }
        private void CargarDatos()
        {
            
            t_perfil = BDPerfiles.TraerPerfiles();

            c_perfiles.DataSource = t_perfil;
            c_perfiles.DisplayMember ="Descripcion";
            c_perfiles.ValueMember ="IdPerfil";

            t_Usuarios = BDUsuarios.Traerusuarios();
            c_Usuarios.DataSource = t_Usuarios;
            c_Usuarios.DisplayMember ="IdUsuario";
            c_Usuarios.ValueMember ="IdUsuario";


         


           
        }

        private void FormModificarUsuario_Load(object sender, EventArgs e)
        {
            CargarDatos();
            
        }

        private void c_Usuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            c_perfiles.SelectedIndex =(int ) t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"] - 1;
            c_Nombre.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Nombre"].ToString ();
            c_referencia.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["Referencia"].ToString();
            c_convenio.Text = t_Usuarios.Rows[c_Usuarios.SelectedIndex]["ConvenioDEM"].ToString();
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {

            if (c_Contraseña.Text.Length < 1)
            {
                if (!BDUsuarios.ActualizarUsuario(t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdUsuario"].ToString()
                                                , (int)t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"], c_Nombre.Text.ToUpper(), c_referencia.Text, c_convenio.Text))
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "No se Pudo Actualizar este usuario";
                        f_Error.ShowDialog();
                    }

                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Datos ACTUALIZADOs";
                        f_Error.ShowDialog();
                    }
                }
            }
            else
            {
                if (c_Contraseña.Text == c_confirmacionContraseña.Text && c_Contraseña.Text.Length >= 5)
                {
                    if (!BDUsuarios.ActualizarContraseña(t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdUsuario"].ToString()
                                                        , (int)t_Usuarios.Rows[c_Usuarios.SelectedIndex]["IdPerfil"], c_Contraseña.Text))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "No se Pudo actualizar la contraseña";
                            f_Error.ShowDialog();
                        }
                    }
                    else
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Contraseña ACTUALIZADA";
                            f_Error.ShowDialog();
                        }
                    }

                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "La contraseña debe contener minimo 6 caracteres";
                        f_Error.ShowDialog();
                    }
                }
            }

               
            
            c_confirmacionContraseña.Text  ="";
            c_Contraseña.Text  ="";

            CargarDatos();
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            l_idTeclado= Globales.LlamarTeclado();
        }

        private void c_Nombre_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
               
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_Nombre.  Text = l_teclado.Captura;

            }
        }

        private void c_Contraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_Contraseña.Text = l_teclado.Captura;

            }
        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_confirmacionContraseña. Text = l_teclado.Captura;

            }
        }

        private void c_referencia_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_referencia. Text = l_teclado.Captura;

            }
        }

        private void c_convenio_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_convenio. Text = l_teclado.Captura;

            }
        }
    }
}
