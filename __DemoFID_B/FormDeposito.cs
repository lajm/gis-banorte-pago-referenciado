﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using CTS;
using SidApi;
using System.IO;

namespace __DemoFID_B
{
    public partial class FormDeposito : Form
    {



        int lRet;
        int l_detalle_reconocimiento;
        String l_Error;
        uint l_TotalAceptados = 0;
        uint l_TotalRechazados = 0;
        int l_Total20 = 0;
        int l_Total50 = 0;
        int l_Total100 = 0;
        int l_Total200 = 0;
        int l_Total500 = 0;
        int l_Total1000 = 0;
        byte drawer_id;
        Double l_TotalEfectivo;
        bool l_terminar = false;
        bool l_Noscanimages = false;
        ushort[] l_TotalBag = new ushort[10];

        ushort[] l_bag_inicio;
        ushort[] l_bag_final;
        ushort[] l_bag_deposito;

        String l_Equipo;
        String l_directorioimages = "C:\\DetalleDepositos";
        String l_Path = "";
        int l_numerobilleteF = 1;
        int l_numerobilleteR = 1;
        int l_topeDeposito = 0;
        int l_topefijo = Globales.Capacidad_Bolsa1;
        ushort Procesar_NUM_Documentos = 0;

        public String Equipo
        {
            get { return l_Equipo; }
            set { l_Equipo = value; }
        }

        public FormDeposito()
        {
            InitializeComponent();



        }


        private void FormDeposito_Load(object sender, EventArgs e)
        {
            byte Status = 0;
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            lRet = 0;
            GC.Collect();

            if (Globales.SaveImagenes)
            {
                l_Noscanimages = true;
                Creardirectoriodestino();

            }

            c_empleado.Text = Globales.NombreUsuario.ToUpper();
            c_EtiquetaNumeroCuenta.Text = Globales.ClienteProsegur;
            Cursor.Hide();

            //using (FormError f_Mensaje = new FormError(false))
            //{
            //    f_Mensaje.c_Imagen.Visible = false;
            //    f_Mensaje.c_MensajeError.Text =
            //        "Coloque los billetes en el alimentador y oprima depositar, si su depósito es muy grande repita la operación.";
            //    DialogResult l_resp = f_Mensaje.ShowDialog();

            //    if (l_resp == DialogResult.Cancel)
            //        l_terminar = true;


            //}

            if (Globales.DEBUG)
            {
                Globales.EscribirBitacora("Deposito", "DETALLE BOLSA ANTES DEL DEPOSITO", "CONTABILIDAD", 1);
                l_bag_inicio = Contenido_en_Bolsa();

                l_topefijo = l_topeDeposito = Globales.Capacidad_Bolsa1 - obtener_cantidad_total(l_bag_inicio);

                c_tope.Text = "Maximo Permitido: " + l_topeDeposito + " Billetes";

                if (l_topeDeposito < 300)
                {
                    Procesar_NUM_Documentos = (ushort)l_topeDeposito;
                    c_tope.Visible = true;
                }

                if (l_bag_inicio == null)
                    using (FormError f_Error = new FormError(false))
                    {
                        SidLib.Error_Sid(lRet, out l_Error);
                        Globales.EscribirBitacora("Deposito", "Problema al obtener Billetes de Bolsa", "Funcion 'Contenido_en_Bolsa'", 1);
                        Prosegur.Alerta("Problema al obtener Billetes de Bolsa, Sugerir reinicio de Sistema");
                        f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde, Reinicie el Equipo si persiste este Error ";
                        f_Error.ShowDialog();
                        c_Depositar.Enabled = false;
                        Close();

                    }


            }else
                l_topeDeposito = Globales.Capacidad_Bolsa1;

        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            Byte Status = 0;

            if (Globales.DEBUG)
            {


            }
            Close();
        }

        private void EscribirResultados()
        {
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            Double l_Efectivo20;
            Double l_Efectivo50;
            Double l_Efectivo100;
            Double l_Efectivo200;
            Double l_Efectivo500;
            Double l_Efectivo1000;

            c_BilletesAceptados.Text = l_TotalAceptados.ToString();
            if (l_TotalRechazados > 1)
                c_BilletesRechazados.Text = "Si";
            c_Cantidad100.Text = l_Total100.ToString();
            c_Cantidad1000.Text = l_Total1000.ToString();
            c_Cantidad20.Text = l_Total20.ToString();
            c_Cantidad200.Text = l_Total200.ToString();
            c_Cantidad50.Text = l_Total50.ToString();
            c_Cantidad500.Text = l_Total500.ToString();
            l_Efectivo100 = l_Total100 * 100;
            l_Efectivo1000 = l_Total1000 * 1000;
            l_Efectivo20 = l_Total20 * 20;
            l_Efectivo200 = l_Total200 * 200;
            l_Efectivo50 = l_Total50 * 50;
            l_Efectivo500 = l_Total500 * 500;
            c_Suma100.Text = l_Efectivo100.ToString("$#,###,##0.00");
            c_Suma1000.Text = l_Efectivo1000.ToString("$#,###,##0.00");
            c_Suma20.Text = l_Efectivo20.ToString("$#,###,##0.00");
            c_Suma200.Text = l_Efectivo200.ToString("$#,###,##0.00");
            c_Suma50.Text = l_Efectivo50.ToString("$#,###,##0.00");
            c_Suma500.Text = l_Efectivo500.ToString("$#,###,##0.00");
            l_TotalEfectivo = l_Efectivo500 + l_Efectivo50 + l_Efectivo200
                + l_Efectivo20 + l_Efectivo1000 + l_Efectivo100;
            c_TotalDeposito.Text = l_TotalEfectivo.ToString("$#,###,##0.00");


            l_topeDeposito = l_topefijo - (int)l_TotalAceptados;

            if (l_topeDeposito < 1)
            {
                c_tope.Text = "DEJE DE ALIMENTAR EL EQUIPO POR FAVOR";
                c_tope.BackColor = Color.Yellow;

                
            }
            else
            {
                if (l_topeDeposito < 300)
                {
                    Procesar_NUM_Documentos = (ushort)l_topeDeposito;
                    c_tope.Visible = true;
                }

                c_tope.Text = "Maximo Permitido:  " + l_topeDeposito + " Billetes";
            }

            Cursor.Hide();
        }

        private void c_Depositar_Click(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            c_BilletesRechazados.Text = "No";
            l_TotalRechazados = 0;

            Int32 Frente, Back, IRFrente, IRBack, MgFrente, Mgback;
            int Reserved = 0;
            int intentos = 0;




            Cursor.Current = Cursors.WaitCursor;

            Byte[] key = new Byte[1];
            Byte[] sensor = new Byte[64];
            ushort[] l_totalNotes = new ushort[10];

            if (l_terminar)
            {
                Dispose();
                Close();
            }

            c_Depositar.Enabled = false;
            c_BotonAceptar.Enabled = false;
            Refresh();

            if (l_topeDeposito < 50)
            {
                c_BotonAceptar.Enabled = true ;
                Refresh();
                return;
            }

            if (Globales.DEBUG)
            {
                // char[] l_username = { '1', '0', '0', '2', '9', '1' };
                char[] l_username = Globales.IdUsuario.ToCharArray();

                try
                {
                    try
                    {


                        try
                        {

                            lRet = SidLib.SID_Open(l_Noscanimages);
                            Globales.EscribirBitacora("Deposito de Efectivo", "OPEN", lRet.ToString(), 1);
                            if (lRet != 0)
                                lRet = SidLib.ResetError();

                            lRet = SidApi.SidLib.SID_Login(l_username);

                            Globales.EscribirBitacora("SELECT DEPOSITO", "SID_LOGIN", "login SIDserial: " + Globales.Serial1, 3);

                            // lRet = SidLib.SID_ConfigDoubleLeafingAndDocLength(SidLib.SID_DOUBLE_LEAFING_WARNING, 50,0, 0);

                            lRet = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                            Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", lRet.ToString(), 1);


                            if (-215 <= lRet && lRet <= -199)
                            {

                                SidLib.Error_Sid(lRet, out l_Error);
                                Globales.EscribirBitacora("EnableDeposit", "Atasco de Billetes::", l_Error, 1);

                                do
                                {
                                    Globales.EscribirBitacora("EnableDeposit", "Recuperacion intento: " + intentos, l_Error, 1);
                                    using (FormError f_Error = new FormError(false))
                                    {
                                        f_Error.c_MensajeError.Text = l_Error + "  Por favor Abra el Equipo y revise las Areas de facil acceso buscando un Atoramiento";
                                        if (intentos == 2)
                                            f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                        f_Error.ShowDialog();




                                        // c_BotonCancelar_Click(this, null);
                                    }
                                    try
                                    {
                                        lRet = SidLib.ResetPath();

                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        lRet = SidLib.SID_Open(l_Noscanimages);
                                    }
                                    lRet = SidLib.ResetError();
                                    lRet = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                    Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", lRet.ToString(), 1);
                                    intentos++;
                                } while (lRet != 0 && intentos < 3);
                                SidLib.SID_Close();
                                c_BotonAceptar.Enabled = true;
                                return;

                            }

                            if (-246 <= lRet && lRet <= -239)
                            {

                                SidLib.Error_Sid(lRet, out l_Error);
                                Globales.EscribirBitacora("EnableDeposit", "Problema DIP_SWITCH::", l_Error, 1);
                                intentos = 0;
                                do
                                {
                                    Globales.EscribirBitacora("EnableDeposit", "Areas Cerradas completamente: " + intentos, l_Error, 1);
                                    using (FormError f_Error = new FormError(false))
                                    {
                                        f_Error.c_MensajeError.Text = l_Error + "  Por favor  Revise que las Areas de facil acceso esten Correctamente CERRADAS";
                                        if (intentos == 2)
                                            f_Error.c_MensajeError.Text += "ULTIMO INTENTO DE RECUPERACION";
                                        f_Error.ShowDialog();
                                        lRet = SidLib.ResetError();


                                        // c_BotonCancelar_Click(this, null);
                                    }
                                    lRet = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                    Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", lRet.ToString(), 1);

                                } while (lRet != 0 && intentos < 3);

                                SidLib.SID_Close();
                                c_BotonAceptar.Enabled = true;
                                return;

                            }

                            if (lRet == 1)
                            {
                                SidLib.Error_Sid(lRet, out l_Error);
                                Globales.EscribirBitacora("EnableDepositWhileFeeding", "Feeder vacio", l_Error, 1);

                                using (FormError f_Error = new FormError(false))
                                {
                                    f_Error.c_MensajeError.Text = "Ponga billetes en el alimentador y presione Depositar";
                                    f_Error.ShowDialog();
                                    SidLib.ResetError();
                                    SidLib.SID_Close();
                                    c_BotonAceptar.Enabled = true;
                                    c_Depositar.Enabled = true;
                                    return;
                                    // c_BotonCancelar_Click(this, null);
                                }
                            }
                            else
                            {

                                if (sensor[7] == 148)
                                {
                                    Globales.EscribirBitacora("CashIn", "sensor[7]", sensor[7].ToString(), 1);
                                    //  lRet = SidLib.SID_GetCashTotalBag(l_TotalBag);
                                    // foreach (ushort billetes in l_totalNotes)
                                    // c_Respuesta.Text += "\r\n l_totalNotes[]= " + billetes;

                                    //lRet = SidLib.SID_ConfigDoubleLeafingAndDocLength(SidLib.DOUBLE_LEAFING_WARNING, (short)Globales.Doubleleafing, 0, 0);
                                    lRet = SidLib.SID_ConfigDoubleLeafing(SidLib.DOUBLE_LEAFING_WARNING, (short)Globales.Doubleleafing, (short)Globales.Polymero, 0);
                                    SidLib.Error_Sid(lRet, out l_Error);
                                    Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: ALGODON " + Globales.Doubleleafing, l_Error, 1);
                                    Globales.EscribirBitacora("CashIn", "SID_ConfigDoubleLeafingAndDocLength VALOR: POLYMERO " + Globales.Polymero, l_Error, 1);

                                    lRet = SidLib.SID_CashIn(Procesar_NUM_Documentos, 1, 1, l_Noscanimages, 0, 0);/*Capture the notes */

                                    if (lRet == 1)
                                    {

                                        SidLib.Error_Sid(lRet, out l_Error);
                                        Globales.EscribirBitacora("EnableDeposit", "Feeder vacio", l_Error, 1);

                                        using (FormError f_Error = new FormError(false))
                                        {
                                            f_Error.c_MensajeError.Text = "Ponga billetes en el alimentador y presione Depositar";
                                            f_Error.ShowDialog();
                                            Refresh();
                                            SidLib.ResetError();
                                            SidLib.SID_Close();
                                            c_Depositar.Enabled = true;
                                            c_BotonAceptar.Enabled = true;

                                            c_Depositar.Text = "ALGO MAS";
                                            return;
                                            // c_BotonCancelar_Click(this, null);
                                        }
                                    }
                                    else if (lRet == SidApi.SidLib.SID_OKAY)
                                    {
                                        short[] result = new short[1];
                                        lRet = SidLib.SID_WaitValidationResult(result);
                                        System.Threading.Thread.Sleep(30);

                                        while (lRet == SidLib.SID_PERIF_BUSY || lRet == SidLib.SID_DOUBLE_LEAFING_WARNING || lRet == SidLib.SID_UNIT_DOC_TOO_LONG)
                                        {

                                            if (lRet == SidLib.SID_DOUBLE_LEAFING_WARNING || lRet == SidLib.SID_UNIT_DOC_TOO_LONG)
                                            {
                                                l_TotalRechazados++;
                                                SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                                                if (lRet == SidLib.SID_DOUBLE_LEAFING_WARNING)
                                                {
                                                    l_TotalRechazados++;
                                                    //  l_detalle_reconocimiento = SidApi.SidLib.SID_RecognizeErrorDetail();

                                                    //  SidApi.SidLib.Error_Sid(l_detalle_reconocimiento, out l_Error);
                                                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "Billete alimentado DOBLE", 2);
                                                }
                                                else
                                                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "Billete muy largo o Doble Desfazado", 2);
                                            }
                                            else if (result[0] == 0xe0 || result[0] == 0xe2)
                                            {
                                                l_TotalRechazados++;
                                                l_detalle_reconocimiento = SidApi.SidLib.SID_RecognizeErrorDetail();
                                                SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/
                                                SidApi.SidLib.Error_Sid(l_detalle_reconocimiento, out l_Error);
                                                Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", l_Error, 2);
                                            }
                                            else
                                                // c_Respuesta.Text += "\r\n" + result[0].ToString("X2");
                                                // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");
                                                if (result[0] != 0 && lRet != SidLib.SID_DOUBLE_LEAFING_WARNING)
                                                {

                                                    switch (result[0].ToString())
                                                    {
                                                        case "16":
                                                            l_TotalAceptados++;
                                                            l_Total20++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "20 Pesos", 3);
                                                            break;
                                                        case "17":
                                                            l_TotalAceptados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "50 Pesos", 3);
                                                            l_Total50++;
                                                            break;
                                                        case "18":
                                                            l_TotalAceptados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "100 Pesos", 3);
                                                            l_Total100++;
                                                            break;
                                                        case "19":
                                                            l_TotalAceptados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "200 Pesos", 3);
                                                            l_Total200++;
                                                            break;
                                                        case "20":
                                                            l_TotalAceptados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "500 Pesos", 3);
                                                            l_Total500++;
                                                            break;
                                                        case "21":
                                                            l_TotalAceptados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "1000 Pesos", 3);
                                                            l_Total1000++;
                                                            break;
                                                        case "224":
                                                            l_TotalRechazados++;
                                                            break;
                                                        case "226":
                                                            l_TotalRechazados++;
                                                            break;
                                                        case "227":
                                                            l_TotalRechazados++;
                                                            break;
                                                        case "228":
                                                            l_TotalRechazados++;
                                                            break;
                                                        case "255":
                                                            l_TotalRechazados++;
                                                            break;
                                                        default:
                                                            l_TotalRechazados++;
                                                            Globales.EscribirBitacora("Deposito", "Billete NO RECONOCIDO: ", "Denominacion desconocida", 3);
                                                            break;

                                                    }
                                                    /* Wait the end of the previous command*/
                                                    System.Threading.Thread.Sleep(30);


                                                    EscribirResultados();
                                                    this.Refresh();

                                                    if (l_Noscanimages)
                                                    {
                                                        Frente = Back = IRFrente = IRBack = Mgback = MgFrente = 0;

                                                        Reserved = SidLib.SID_ReadImage((int)SidLib.NO_CLEAR_BLACK, ref Frente, ref Back, ref IRFrente, ref IRBack, ref MgFrente, ref Mgback, ref Reserved);
                                                        Globales.EscribirBitacora("Deposito", "SID_READIMAGE: ", Reserved.ToString(), 3);

                                                        Reserved = SidLib.SID_SaveDIB(Frente, CrearPathDestinoFrente("Frente.bmp"));
                                                        Globales.EscribirBitacora("Deposito", "SID_DIB FRENTE: ", Reserved.ToString(), 3);
                                                        Reserved = SidLib.SID_SaveDIB(Back, CrearPathDestinoReverso("Reverso.bmp"));
                                                        Globales.EscribirBitacora("Deposito", "SID_DIB BACK: ", Reserved.ToString(), 3);

                                                        Reserved = SidLib.SID_SaveDIB(IRFrente, CrearPathDestinoFrente("FrenteIR.bmp"));
                                                        Globales.EscribirBitacora("Deposito", "SID_DIB IRFRENTE: ", Reserved.ToString(), 3);
                                                        Reserved = SidLib.SID_SaveDIB(IRBack, CrearPathDestinoReverso("ReversoIR.bmp"));
                                                        Globales.EscribirBitacora("Deposito", "SID_DIB IRBACK: ", Reserved.ToString(), 3);

                                                        if (Frente != 0)
                                                            SidLib.SID_FreeImage(Frente);
                                                        if (Back != 0)
                                                            SidLib.SID_FreeImage(Back);
                                                        if (IRFrente != 0)
                                                            SidLib.SID_FreeImage(IRFrente);
                                                        if (IRBack != 0)
                                                            SidLib.SID_FreeImage(IRBack);
                                                        if (MgFrente != 0)
                                                            SidLib.SID_FreeImage(MgFrente);
                                                        if (Mgback != 0)
                                                            SidLib.SID_FreeImage(Mgback);


                                                    }


                                                    SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/

                                                }




                                            lRet = SidLib.SID_WaitValidationResult(result);

                                        }

                                        SidLib.SID_ClearDenominationNote();

                                        if (-211 > lRet && lRet <= -201)
                                        {
                                            intentos = 0;
                                            using (FormError f_Error = new FormError(false))
                                            {
                                                SidLib.Error_Sid(lRet, out l_Error);
                                                Globales.EscribirBitacora("Deposito", "Atasco en el Deposito", l_Error, 1);

                                                f_Error.c_MensajeError.Text = "Ocurrio un atasco de billetes. Causa : " + l_Error;
                                                f_Error.ShowDialog();
                                                c_Depositar.Enabled = false;
                                                //Close();
                                                // return;
                                            }
                                            do
                                            {
                                                try
                                                {
                                                    lRet = SidLib.ResetPath();
                                                }
                                                catch { }
                                                finally
                                                {
                                                    lRet = SidLib.SID_Open(l_Noscanimages);
                                                }
                                                lRet = SidLib.ResetError();

                                                lRet = SidLib.SID_UnitStatus(key, sensor, null, null, null);
                                                Globales.EscribirBitacora("Deposito de Efectivo", "UnitStatus", lRet.ToString(), 1);

                                            } while (lRet != 0 && intentos < 3);

                                            SidLib.SID_Close();
                                            c_BotonAceptar_Click(null, null);
                                        }

                                        if (lRet == -13)
                                        {
                                            SidLib.Error_Sid(lRet, out l_Error);
                                            Globales.EscribirBitacora("Deposito", "ERROR en el Deposito", l_Error, 1);

                                            SidLib.ResetPath();
                                            SidLib.ResetError();
                                            SidLib.SID_Close();

                                            Globales.EscribirBitacora("Deposito de Efectivo", "Error en Deposito", lRet.ToString(), 1);
                                            c_BotonAceptar_Click(null, null);

                                        }

                                        SidLib.ResetError();

                                    }


                                }
                                else
                                    using (FormError f_Error = new FormError(false))
                                    {
                                        SidLib.Error_Sid(lRet, out l_Error);
                                        Globales.EscribirBitacora("Deposito", "Problema en el Deposito", l_Error, 1);

                                        f_Error.c_MensajeError.Text = "Deposito No Posible intente mas tarde. Causa : " + l_Error;
                                        f_Error.ShowDialog();
                                        c_Depositar.Enabled = true;
                                        c_BotonAceptar.Enabled = true;
                                        //Close();
                                        return;
                                    }


                            }


                            lRet = SidApi.SidLib.SID_Logout();
                            Globales.EscribirBitacora("SELECT DEPOSITO", "SID_LOgout", "logout: " + lRet, 3);
                            // SidLib.ResetError();
                            SidLib.SID_Close();

                        }
                        catch (Exception ex)
                        {
                            Globales.EscribirBitacora("Deposito", "LOOP VALIDACION", "Execpcion  = " + ex.Message, 1);
                        }



                        Globales.EscribirBitacora("Deposito", "AutoDOCHANDLE", "Estatus = " + lRet.ToString(), 1);
                        /*lRet = FidLib.AutoDocHandle(FidLib.VALIDATE.VALIDATE_BANKNOTE, FidLib.SIDE.SIDE_ALL_IMAGE,
                            FidLib.SCANSPEED.SCAN_MODE_SPEED2_BOTH, FidLib.CLEARBLACK.NO_CLEAR_BLACK, 999,
                                FidLib.SAVEIMAGE.IMAGE_SAVE_NONE, "\\images", "Image_", FidLib.FILEFORMAT.SAVE_BMP,
                                    FidLib.DOCTYPE.DOCTYPE_SCAN_BANKNOTE, FidLib.SCANEXTRAMODE.SCAN_EXTRAMODE_UV,
                                        FidLib.SCANMAGNETICMODE.BIT_SCAN_MAGNETIC_YES);*/

                        /*  if (lRet != 1)
                          {
                             // FidApi.FI2D_GetErrorString(lRet, "NS_AutoDocHandle", out l_Error);
                              using (FormError f_Error = new FormError(false))
                              {
                                  f_Error.c_MensajeError.Text = lRet.ToString(); ;
                                  f_Error.ShowDialog();
                                 // c_BotonCancelar_Click(this, null);
                              }
                          }
                          else
                          {
                           
                          }
                          */



                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception ex)
                {
                }

            }
            else
            {
                //test();
                Llenar_Aleatorio();
                EscribirResultados();
            }
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();
            if (l_TotalRechazados > 0)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Por favor retire los billetes rechazados";
                    f_Error.ShowDialog();
                    l_TotalRechazados = 0;
                }
            }

            c_Depositar.Enabled = true;

            //  System.Threading.Thread.Sleep(5000);
            c_BotonAceptar.Enabled = true;

            Globales.EscribirBitacora("Deposito", "Total aceptados parcial", l_TotalAceptados.ToString(), 1);
            c_Depositar.Text = "ALGO MAS";

        }

        //private void VisualizaResultado()
        //{
        //    Globales.EscribirBitacora("Deposito", "VisualizarResultado", "DocInfo = "+st_DocData.DocInfo.ToString("X"));
        //    switch (st_DocData.DocInfo.ToString("X"))
        //    {
        //        case "20":
        //            l_TotalRechazados++;
        //            break;
        //        case "35":
        //            l_TotalAceptados++;
        //            l_Total20++;
        //            break;
        //        case "36":
        //            l_TotalAceptados++;
        //            l_Total50++;
        //            break;
        //        case "37":
        //            l_TotalAceptados++;
        //            l_Total100++;
        //            break;
        //        case "38":
        //            l_TotalAceptados++;
        //            l_Total200++;
        //            break;
        //        case "39":
        //            l_TotalAceptados++;
        //            l_Total500++;
        //            break;
        //        case "3A":
        //            l_TotalAceptados++;
        //            l_Total1000++;
        //            break;
        //        case "40":
        //            l_TotalRechazados++;
        //            break;
        //        case "41":
        //            l_TotalRechazados++;
        //            break;
        //        case "50":
        //            l_TotalRechazados++;
        //            break;
        //        case "51":
        //            l_TotalRechazados++;
        //            break;
        //        case "60":
        //            l_TotalRechazados++;
        //            break;
        //        case "70":
        //            l_TotalRechazados++;
        //            break;
        //        case "71":
        //            l_TotalRechazados++;
        //            break;
        //        case "80":
        //            l_TotalRechazados++;
        //            break;
        //        case "81":
        //            l_TotalRechazados++;
        //            break;
        //        case "82":
        //            l_TotalRechazados++;
        //            break;
        //        case "83":
        //            l_TotalRechazados++;
        //            break;
        //        case "84":
        //            l_TotalRechazados++;
        //            break;
        //        case "85":
        //            l_TotalRechazados++;
        //            break;
        //        case "90":
        //            l_TotalRechazados++;
        //            break;
        //        case "91":
        //            l_TotalRechazados++;
        //            break;
        //        default:
        //            l_TotalRechazados++;
        //            break;
        //    }
        //}

        private void Llenar_Aleatorio()
        {
            Random rd = new Random();
            int l_ValorR;
            for (int i = 0; i < 50; i++)
            {
                l_ValorR = rd.Next(0, 1001);
                if (l_ValorR <= 20)
                {
                    l_TotalAceptados++;
                    l_Total20++;
                }
                else if (l_ValorR <= 50)
                {
                    l_TotalAceptados++;
                    l_Total50++;
                }
                else if (l_ValorR <= 100)
                {
                    l_TotalAceptados++;
                    l_Total100++;
                }
                else if (l_ValorR <= 200)
                {
                    l_TotalAceptados++;
                    l_Total200++;
                }
                else if (l_ValorR <= 500)
                {
                    l_Total500++;
                    l_TotalAceptados++;
                }
                else if (l_ValorR <= 1000)
                {
                    l_TotalAceptados++;
                    l_Total1000++;
                }
                else
                    l_TotalRechazados++;
            }
        }

        private ushort[] total_deposito_en_bolsa(ushort[] l_inicio, ushort[] l_fin)
        {
            ushort[] l_resta = new ushort[6];

            if (l_inicio != null && l_fin != null)
                for (int i = 0; i < 6; i++)
                {
                    l_resta[i] = (ushort)(l_fin[i] - l_inicio[i]);
                }
            else if (l_fin == null)
            {
                l_resta = null;
                Prosegur.Alerta("Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario:  " + Globales.NombreUsuario + "Hora evento: " + DateTime.Now);
                Globales.EscribirBitacora("Comparar Bolsas", "total_deposito_en_bolsa", "Deposito para Revisión ya que no se Obtuvo Contenido en Bolsa para confrontar Usuario:  " + Globales.NombreUsuario + "Hora evento: " + DateTime.Now, 3);
            }


            return l_resta;

        }

        private ushort[] Contenido_en_Bolsa()
        {
            int contador = 0;
            int l_Reply;
            if (Globales.DEBUG)
            {
                ushort[] l_totalNotes = new ushort[64];

                do
                {
                    l_Reply = SidLib.SID_Open(true);
                    SidLib.Error_Sid(l_Reply, out l_Error);
                    Globales.EscribirBitacora("Contenido bolsa", "sid_open", l_Reply.ToString(), 1);
                    contador++;
                    if (contador == 100)
                        l_Reply = SidLib.SID_CURRENT_ERROR;
                } while (l_Reply == SidLib.SID_COMMAND_IN_EXECUTION_YET);


                if (l_Reply == SidLib.SID_OKAY || l_Reply == SidLib.SID_ALREADY_OPEN)
                {
                    contador = 0;
                    do
                    {

                        l_Reply = SidLib.SID_GetCashTotalBag(l_totalNotes);
                        if (contador == 100)
                            break;

                    } while (l_Reply != SidLib.SID_OKAY);

                    SidLib.Error_Sid(l_Reply, out l_Error);
                    Globales.EscribirBitacora("Contenido en bolsa", "Global total notes obtenido", l_Error, 1);

                    if (l_Reply == SidLib.SID_OKAY)
                    {
                        l_Reply = SidLib.SID_Close();
                        Escribir_ContenidoBolsa(l_totalNotes);
                        return l_totalNotes;
                    }
                    else
                    {

                        l_Reply = SidLib.SID_Close();
                        return null;
                    }
                }
                else
                {
                    Globales.EscribirBitacora("GetTotalCash", "Sid_open", " No se pudo obtener el total en Bolsa: " + l_Error, 1);
                    return null;
                }
            }
            else
                return null;

        }


        private void Escribir_Log_sobrante(int[] l_datos)
        {

            for (int i = 0; i < l_datos.Length; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Macheo de billetes", "Diferencia de Billetes Cantidad :  " + l_datos[i] + "  Denominacion 0..5 : " + i.ToString(), 1);

                }
            }


        }

        private void Escribir_Log_faltante(int[] l_datos)
        {

            for (int i = 0; i < l_datos.Length; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Macheo de billetes", "Diferencia de Billetes Cantidad :  " + l_datos[i] + "  Denominacion 0..5 : " + i.ToString(), 1);

                }
            }


        }

        private void Escribir_ContenidoBolsa(int[] l_datos)
        {

            for (int i = 0; i < 6; i++)
            {
                if (l_datos[i] != 0)
                {
                    Globales.EscribirBitacora("Deposito", "Contenido bolsa", "  Denominacion: " + i.ToString() + " Cantidad Actual: " + l_datos[i], 1);

                }
            }


        }

        private void Escribir_ContenidoBolsa(ushort[] l_datos)
        {

            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    if (l_datos[i] != 0)
                    {
                        Globales.EscribirBitacora("Deposito", " Registro SID ", "  Denominacion: " + i.ToString() + " Cantidad Esperada: " + l_datos[i], 1);

                    }
                }
            else
            {
                for (int i = 0; i < 6; i++)
                {

                    Globales.EscribirBitacora("Deposito", " Registro SID ", "  Denominacion: " + i.ToString() + " Cantidad Esperada: " + "NO DETERMINADO", 1);


                }

            }


        }

        private int obtener_cantidad_total(ushort[] l_datos)
        {
            int l_suma_billetes = 0;
            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    l_suma_billetes += l_datos[i];
                }

            Globales.EscribirBitacora("Deposito", "Contenido bolsa", " Cantidad total en bolsa # de billetes: " + l_suma_billetes, 1);
            return l_suma_billetes;
        }

        private int obtener_cantidad_total(int[] l_datos)
        {
            int l_suma_billetes = 0;
            if (l_datos != null)
                for (int i = 0; i < 6; i++)
                {
                    l_suma_billetes += l_datos[i];
                }

            // Globales.EscribirBitacora("Deposito", "Contenido bolsa", " Cantidad total en bolsa # de billetes: " + l_suma_billetes, 1);
            return l_suma_billetes;
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            int l_Cajon = 1;

            c_Depositar.Enabled = false;
            c_BotonAceptar.Enabled = false;
            Application.DoEvents();
            System.Threading.Thread.Sleep(300);  //Esperar que SID termine ejecucion de Cashin

            int[] l_sobrante = new int[6];
            int[] l_faltante = new int[6];

            if (l_TotalEfectivo > 0)
            {
                Globales.EscribirBitacora("Deposito", "Contabilidad en Pantalla", "BIlletes Validados\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad Actual: " + l_Total1000 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad Actual: " + l_Total500 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad Actual: " + l_Total200 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad Actual: " + l_Total100 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad Actual: " + l_Total50 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad Actual: " + l_Total20 + "\n\r", 1);
                Globales.EscribirBitacora("Deposito", "Contabilidad Deposito", " Cantidad total Actual: " + l_TotalEfectivo + "\n\r", 1);

                ///////////////////Correccion ante Atascos 
                if (Globales.DEBUG)
                {
                    Globales.EscribirBitacora("Deposito", "Contabilidad EN BOLSA", "BIlletes ACEPTADOS y CONTADOS\n\r", 1);
                    l_bag_deposito = total_deposito_en_bolsa(l_bag_inicio, Contenido_en_Bolsa());

                    Escribir_ContenidoBolsa(l_bag_deposito);

                    if (l_bag_deposito == null)
                    {
                        l_bag_deposito = new ushort[6];

                        Double l_TotalEfectivoFaltante = 0;

                        l_faltante[5] = l_Total1000 - l_bag_deposito[5];
                        l_faltante[4] = l_Total500 - l_bag_deposito[4];
                        l_faltante[3] = l_Total200 - l_bag_deposito[3];
                        l_faltante[2] = l_Total100 - l_bag_deposito[2];
                        l_faltante[1] = l_Total50 - l_bag_deposito[1];
                        l_faltante[0] = l_Total20 - l_bag_deposito[0];

                        l_TotalEfectivoFaltante = l_faltante[5] * 1000 + l_faltante[4] * 500 + l_faltante[3] * 200 + l_faltante[2] * 100 + l_faltante[1] * 50 + l_faltante[0] * 20;

                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[5] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[4] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[3] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[2] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[1] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad PRESUNTAMENTE en bolsa: " + l_faltante[0] + "\n\r", 1);
                        Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Cantidad total PRESUNTAMENTE en bolsa: " + l_TotalEfectivoFaltante + "\n\r", 1);
                        // EscribirResultados();
                        // Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA",1);
                        Escribir_Log_faltante(l_faltante);
                        Globales.EscribirBitacora("Registro Deposito", "Registro sin CONFRONTAR", "FIN Registro", 1);


                    }
                    else
                    {

                        Double l_TotalEfectivoFaltante = 0;
                        ///compara una a una las entradas
                        if (l_Total1000 >= l_bag_deposito[5])
                        {
                            l_sobrante[5] = l_Total1000 - l_bag_deposito[5];
                            l_Total1000 = l_bag_deposito[5];

                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[5] * 1000;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[5]);

                        }
                        else
                        {
                            l_faltante[5] = l_bag_deposito[5] - l_Total1000;
                            l_TotalEfectivoFaltante += l_faltante[5] * 1000;


                        }

                        if (l_Total500 >= l_bag_deposito[4])
                        {
                            l_sobrante[4] = l_Total500 - l_bag_deposito[4];
                            l_Total500 = l_bag_deposito[4];
                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[4] * 500;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[4]);
                        }
                        else
                        {
                            l_faltante[4] = l_bag_deposito[4] - l_Total500;
                            l_TotalEfectivoFaltante += l_faltante[4] * 500;

                        }

                        if (l_Total200 >= l_bag_deposito[3])
                        {
                            l_sobrante[3] = l_Total200 - l_bag_deposito[3];
                            l_Total200 = l_bag_deposito[3];
                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[3] * 200;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[3]);
                        }
                        else
                        {
                            l_faltante[3] = l_bag_deposito[3] - l_Total200;
                            l_TotalEfectivoFaltante += l_faltante[3] * 200;

                        }

                        if (l_Total100 >= l_bag_deposito[2])
                        {
                            l_sobrante[2] = l_Total100 - l_bag_deposito[2];
                            l_Total100 = l_bag_deposito[2];

                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[2] * 100;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[2]);
                        }
                        else
                        {

                            l_faltante[2] = l_bag_deposito[2] - l_Total100;
                            l_TotalEfectivoFaltante += l_faltante[2] * 100;

                        }

                        if (l_Total50 >= l_bag_deposito[1])
                        {
                            l_sobrante[1] = l_Total50 - l_bag_deposito[1];
                            l_Total50 = l_bag_deposito[1];
                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[1] * 50;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[1]);
                        }
                        else
                        {
                            l_faltante[1] = l_bag_deposito[1] - l_Total50;
                            l_TotalEfectivoFaltante += l_faltante[1] * 50;


                        }

                        if (l_Total20 >= l_bag_deposito[0])
                        {
                            l_sobrante[0] = l_Total20 - l_bag_deposito[0];
                            l_Total20 = l_bag_deposito[0];
                            l_TotalEfectivo = l_TotalEfectivo - l_sobrante[0] * 20;
                            l_TotalAceptados = (uint)(l_TotalAceptados - l_sobrante[0]);
                        }
                        else
                        {
                            l_faltante[0] = l_bag_deposito[0] - l_Total20;
                            l_TotalEfectivoFaltante += l_faltante[0] * 20;

                        }


                        if (obtener_cantidad_total(l_sobrante) > 0)
                        {
                            Globales.EscribirBitacora("Deposito", "DIFERENCIA DEPOSITO VS ACEPTADOS", "Empieza AJUSTE", 1);

                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad Actual: " + l_Total1000 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad Actual: " + l_Total500 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad Actual: " + l_Total200 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad Actual: " + l_Total100 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad Actual: " + l_Total50 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad Actual: " + l_Total20 + "\n\r", 1);
                            Globales.EscribirBitacora("Ajuste Deposito", "Contabilidad Deposito", "  Cantidad total Actual: " + l_TotalEfectivo + "\n\r", 1);
                            EscribirResultados();
                            Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA", 1);
                            Escribir_Log_sobrante(l_sobrante);
                            Globales.EscribirBitacora("Deposito", "AJUSTE REALIZADO CON SOBRANTE", "FIN de AJUSTE", 1);

                        }

                        if (obtener_cantidad_total(l_faltante) > 0)
                        {
                            Globales.EscribirBitacora("Deposito", "DIFERENCIA DEPOSITO VS ACEPTADOS", "Empieza Registro", 1);

                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 1000 Cantidad FALTANTE en Conteo: " + l_faltante[5] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 500 Cantidad FALTANTE en Conteo: " + l_faltante[4] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 200 Cantidad FALTANTE en Conteo: " + l_faltante[3] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 100 Cantidad FALTANTE en Conte: " + l_faltante[2] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 50 Cantidad FALTANTE en Conteo: " + l_faltante[1] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Denominacion: 20 Cantidad FALTANTE en Conteo: " + l_faltante[0] + "\n\r", 1);
                            Globales.EscribirBitacora("Registro Deposito", "Contabilidad Deposito", "  Cantidad total FALTANTE en Conteo: " + l_TotalEfectivoFaltante + "\n\r", 1);
                            // EscribirResultados();
                            // Globales.EscribirBitacora("Deposito", "AJUSTE", "Modificacndo PANTALLA",1);
                            Escribir_Log_faltante(l_faltante);
                            Globales.EscribirBitacora("Registro Deposito", "Registro de FALTANTE en CONTEO", "FIN Registro", 1);

                            l_TotalEfectivo = l_TotalEfectivo + l_TotalEfectivoFaltante;
                        }


                        if (obtener_cantidad_total(l_sobrante) == 0 && obtener_cantidad_total(l_faltante) == 0)
                            Globales.EscribirBitacora("Deposito", "Contabilidad", "Deposito Congruente, sin ajustes", 1);
                    }
                }
                //////////////FIN CORRECCION


                DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
                if (l_Total1000 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "1000");
                    l_Nueva[1] = l_Total1000 +l_faltante[5] ;
                    l_Nueva[2] = "1000";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total500 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "500");
                    l_Nueva[1] = l_Total500 + l_faltante[4];
                    l_Nueva[2] = "500";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total200 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "200");
                    l_Nueva[1] = l_Total200 + l_faltante[3];
                    l_Nueva[2] = "200";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total100 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "100");
                    l_Nueva[1] = l_Total100 +l_faltante[2];
                    l_Nueva[2] = "100";
                    l_Detalle.Rows.Add(l_Nueva);
                }
                if (l_Total50 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "50");
                    l_Nueva[1] = l_Total50 + l_faltante[1];
                    l_Nueva[2] = "50";
                    l_Detalle.Rows.Add(l_Nueva);
                }

                if (l_Total20 > 0)
                {
                    DataRow l_Nueva = l_Detalle.NewRow();
                    l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(1, "20");
                    l_Nueva[1] = l_Total20 + l_faltante[0];
                    l_Nueva[2] = "20";

                    l_Detalle.Rows.Add(l_Nueva);
                }


                Cursor.Current = Cursors.Default;


                if (!UtilsComunicacion.Enviar_Transaccion(Globales.EmpresaCliente, l_Cajon, 1, 1, l_TotalEfectivo,
                    l_Detalle, 0, true, Globales.IdUsuario))
                {

                    //  Prosegur.CrearTransaccion (Globales.IdUsuario,l_Detalle ,Prosegur.l_tipoTransaccion .Deposito );
                    // Santander.Escribir_Transaccion(Santander.CrearTransaccion(Santander.CrearDesglose(l_Detalle), 1, Globales.NoCuenta, Globales.IdUsuario, l_TotalEfectivo));

                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "No se pudo insertar el depósito, recoja el dinero del ESCROW";
                        f_Error.ShowDialog();
                        //   lRet = FidLib.AbrirVandalDoor();
                        Close();
                        return;
                    }
                }
                else
                {
                    // Santander.Escribir_Transaccion(Santander.CrearTransaccion(Santander.CrearDesglose(l_Detalle), 1, Globales.NoCuenta, Globales.IdUsuario, l_TotalEfectivo));

                    using (FormError f_Error = new FormError(true))
                    {
                        SidLib.Error_Sid(lRet, out l_Error);
                        Globales.EscribirBitacora("Deposito", "Terminar Deposito", l_Error, 1);
                        f_Error.c_MensajeError.Text = "Deposito Exitoso";
                        f_Error.ShowDialog();
                        Dispose();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                        Close();
                        return;
                    }


                }
            }
            else
            {
                using (FormError f_Error = new FormError(false))
                {


                    Globales.EscribirBitacora("Deposito", "SIN Deposito", l_Error, 1);
                    f_Error.c_MensajeError.Text = "No ha depositado nada";
                    f_Error.ShowDialog();
                    Dispose();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    Close();
                    return;
                }

            }
        }
        private void test()
        {
            short[] result = new short[1];
            int lRet = 16;
            result[0] = 0x16
                ;

            while (lRet == SidLib.SID_PERIF_BUSY || lRet == SidLib.SID_DOUBLE_LEAFING_WARNING || lRet == SidLib.SID_UNIT_DOC_TOO_LONG)
            {

                if (lRet == SidLib.SID_DOUBLE_LEAFING_WARNING || lRet == SidLib.SID_UNIT_DOC_TOO_LONG)
                {
                    l_TotalRechazados++;

                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "Double_leafing", 2);
                }
                else if (result[0] == 0xe0 || result[0] == 0xe2)
                {
                    l_TotalRechazados++;

                    Globales.EscribirBitacora("Deposito", " MOTIVO RECHAZO ", "rechazado por..", 2);
                }
                else
                    // c_Respuesta.Text += "\r\n" + result[0].ToString("X2");
                    // c_Respuesta.Text += "\r\n" + result[1].ToString("X2");
                    if (result[0] != 0)
                    {

                        switch (result[0].ToString())
                        {
                            case "16":
                                l_TotalAceptados++;
                                l_Total20++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "20 Pesos", 3);
                                break;
                            case "17":
                                l_TotalAceptados++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "50 Pesos", 3);
                                l_Total50++;
                                break;
                            case "18":
                                l_TotalAceptados++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "100 Pesos", 3);
                                l_Total100++;
                                break;
                            case "19":
                                l_TotalAceptados++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "200 Pesos", 3);
                                l_Total200++;
                                break;
                            case "20":
                                l_TotalAceptados++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "500 Pesos", 3);
                                l_Total500++;
                                break;
                            case "21":
                                l_TotalAceptados++;
                                Globales.EscribirBitacora("Deposito", "Billete RECONOCIDO: ", "1000 Pesos", 3);
                                l_Total1000++;
                                break;
                            case "224":
                                l_TotalRechazados++;
                                break;
                            case "226":
                                l_TotalRechazados++;
                                break;
                            case "227":
                                l_TotalRechazados++;
                                break;
                            case "228":
                                l_TotalRechazados++;
                                break;
                            case "255":
                                l_TotalRechazados++;
                                break;
                            default:
                                l_TotalRechazados++;
                                Globales.EscribirBitacora("Deposito", "Billete NO RECONOCIDO: ", "Denominacion desconocida", 3);
                                break;

                        }
                        /* Wait the end of the previous command*/



                        EscribirResultados();
                        this.Refresh();

                        SidLib.SID_ClearDenominationNote(); /* Clear the type of note recognized*/

                    }




                lRet = SidLib.SID_WaitValidationResult(result);

            }

            if (-211 > lRet && lRet <= -201)
            {
                using (FormError f_Error = new FormError(false))
                {
                    SidLib.Error_Sid(lRet, out l_Error);
                    Globales.EscribirBitacora("Deposito", "Atasco en el Deposito", l_Error, 1);

                    f_Error.c_MensajeError.Text = "Ocurrio un atasco de billetes. Causa : " + l_Error;
                    f_Error.ShowDialog();
                    c_Depositar.Enabled = false;
                    //Close();
                    // return;
                }

                SidLib.ResetPath();
                SidLib.ResetError();
                SidLib.SID_Close();
                c_BotonAceptar_Click(null, null);
            }

            if (lRet == -13)
            {
                SidLib.Error_Sid(lRet, out l_Error);
                Globales.EscribirBitacora("Deposito", "ERROR en el Deposito", l_Error, 1);

                SidLib.ResetPath();
                SidLib.ResetError();
                SidLib.SID_Close();

                Globales.EscribirBitacora("Deposito de Efectivo", "Error en Deposito", lRet.ToString(), 1);
                c_BotonAceptar_Click(null, null);

            }




        }

        private void Creardirectoriodestino()
        {
            try
            {
                String l_carpetaDia = DateTime.Today.ToString("d_dddd");
                String l_caprteaMes = DateTime.Today.ToString("MMMM");
                String l_horaTransaccion = DateTime.Now.ToString("HH_mm_ss");

                l_Path = l_directorioimages + "\\" + l_caprteaMes + "\\" + l_carpetaDia + "\\" + Globales.IdUsuario + "\\" + l_horaTransaccion;
                Directory.CreateDirectory(l_Path);
                Globales.EscribirBitacora("Deposito con Imagenes ", "Creardirectoriodestino", l_Path, 1);
            }
            catch (Exception ex)
            {
                Globales.EscribirBitacora("Deposito con Imagenes ", ex.Message, l_Path, 1);
            }

        }


        private string CrearPathDestinoFrente(string l_sufijo)
        {

            String l_fin = "\\" + l_numerobilleteF.ToString("00000") + l_sufijo;
            l_numerobilleteF++;
            return l_Path + l_fin;
        }

        private string CrearPathDestinoReverso(string l_sufijo)
        {

            String l_fin = "\\" + l_numerobilleteR.ToString("00000") + l_sufijo;
            l_numerobilleteR++;
            return l_Path + l_fin;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
