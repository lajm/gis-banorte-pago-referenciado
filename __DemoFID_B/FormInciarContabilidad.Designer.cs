﻿namespace __DemoFID_B
{
    partial class FormInciarContabilidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BackSpace = new System.Windows.Forms.Button();
            this.c_EtiquetaPassword = new System.Windows.Forms.Label();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_Boton0 = new System.Windows.Forms.Button();
            this.c_Boton9 = new System.Windows.Forms.Button();
            this.c_Boton8 = new System.Windows.Forms.Button();
            this.c_Boton6 = new System.Windows.Forms.Button();
            this.c_Boton5 = new System.Windows.Forms.Button();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_Boton7 = new System.Windows.Forms.Button();
            this.c_Boton3 = new System.Windows.Forms.Button();
            this.c_Boton2 = new System.Windows.Forms.Button();
            this.c_Boton4 = new System.Windows.Forms.Button();
            this.c_Boton1 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.c_BackSpace);
            this.panel2.Controls.Add(this.c_EtiquetaPassword);
            this.panel2.Controls.Add(this.c_BotonAceptar);
            this.panel2.Controls.Add(this.c_Boton0);
            this.panel2.Controls.Add(this.c_Boton9);
            this.panel2.Controls.Add(this.c_Boton8);
            this.panel2.Controls.Add(this.c_Boton6);
            this.panel2.Controls.Add(this.c_Boton5);
            this.panel2.Controls.Add(this.c_BotonCancelar);
            this.panel2.Controls.Add(this.c_Boton7);
            this.panel2.Controls.Add(this.c_Boton3);
            this.panel2.Controls.Add(this.c_Boton2);
            this.panel2.Controls.Add(this.c_Boton4);
            this.panel2.Controls.Add(this.c_Boton1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 600);
            this.panel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(207, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 29);
            this.label1.TabIndex = 26;
            this.label1.Text = "INGRESE SU CONTRASEÑA";
            // 
            // c_BackSpace
            // 
            this.c_BackSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BackSpace.Image = global::@__DemoFID_B.Properties.Resources.back;
            this.c_BackSpace.Location = new System.Drawing.Point(153, 442);
            this.c_BackSpace.Name = "c_BackSpace";
            this.c_BackSpace.Size = new System.Drawing.Size(150, 53);
            this.c_BackSpace.TabIndex = 25;
            this.c_BackSpace.TabStop = false;
            this.c_BackSpace.Text = "Borrar";
            this.c_BackSpace.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.c_BackSpace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BackSpace.UseVisualStyleBackColor = true;
            this.c_BackSpace.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_EtiquetaPassword
            // 
            this.c_EtiquetaPassword.BackColor = System.Drawing.Color.Transparent;
            this.c_EtiquetaPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_EtiquetaPassword.Location = new System.Drawing.Point(252, 498);
            this.c_EtiquetaPassword.Name = "c_EtiquetaPassword";
            this.c_EtiquetaPassword.Size = new System.Drawing.Size(297, 39);
            this.c_EtiquetaPassword.TabIndex = 24;
            this.c_EtiquetaPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_BotonAceptar.Location = new System.Drawing.Point(497, 442);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(150, 53);
            this.c_BotonAceptar.TabIndex = 23;
            this.c_BotonAceptar.TabStop = false;
            this.c_BotonAceptar.Text = "Aceptar";
            this.c_BotonAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton0
            // 
            this.c_Boton0.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton0.Location = new System.Drawing.Point(324, 441);
            this.c_Boton0.Name = "c_Boton0";
            this.c_Boton0.Size = new System.Drawing.Size(151, 53);
            this.c_Boton0.TabIndex = 22;
            this.c_Boton0.TabStop = false;
            this.c_Boton0.Text = "0";
            this.c_Boton0.UseVisualStyleBackColor = true;
            this.c_Boton0.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton9
            // 
            this.c_Boton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton9.Location = new System.Drawing.Point(497, 346);
            this.c_Boton9.Name = "c_Boton9";
            this.c_Boton9.Size = new System.Drawing.Size(150, 90);
            this.c_Boton9.TabIndex = 21;
            this.c_Boton9.TabStop = false;
            this.c_Boton9.Text = "9";
            this.c_Boton9.UseVisualStyleBackColor = true;
            this.c_Boton9.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton8
            // 
            this.c_Boton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton8.Location = new System.Drawing.Point(325, 346);
            this.c_Boton8.Name = "c_Boton8";
            this.c_Boton8.Size = new System.Drawing.Size(150, 90);
            this.c_Boton8.TabIndex = 20;
            this.c_Boton8.TabStop = false;
            this.c_Boton8.Text = "8";
            this.c_Boton8.UseVisualStyleBackColor = true;
            this.c_Boton8.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton6
            // 
            this.c_Boton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton6.Location = new System.Drawing.Point(497, 248);
            this.c_Boton6.Name = "c_Boton6";
            this.c_Boton6.Size = new System.Drawing.Size(150, 90);
            this.c_Boton6.TabIndex = 19;
            this.c_Boton6.TabStop = false;
            this.c_Boton6.Text = "6";
            this.c_Boton6.UseVisualStyleBackColor = true;
            this.c_Boton6.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton5
            // 
            this.c_Boton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton5.Location = new System.Drawing.Point(325, 248);
            this.c_Boton5.Name = "c_Boton5";
            this.c_Boton5.Size = new System.Drawing.Size(150, 90);
            this.c_Boton5.TabIndex = 18;
            this.c_Boton5.TabStop = false;
            this.c_Boton5.Text = "5";
            this.c_Boton5.UseVisualStyleBackColor = true;
            this.c_Boton5.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Image = global::@__DemoFID_B.Properties.Resources.block;
            this.c_BotonCancelar.Location = new System.Drawing.Point(713, 497);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(75, 53);
            this.c_BotonCancelar.TabIndex = 17;
            this.c_BotonCancelar.TabStop = false;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton7
            // 
            this.c_Boton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton7.Location = new System.Drawing.Point(153, 346);
            this.c_Boton7.Name = "c_Boton7";
            this.c_Boton7.Size = new System.Drawing.Size(150, 90);
            this.c_Boton7.TabIndex = 16;
            this.c_Boton7.TabStop = false;
            this.c_Boton7.Text = "7";
            this.c_Boton7.UseVisualStyleBackColor = true;
            this.c_Boton7.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton3
            // 
            this.c_Boton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton3.Location = new System.Drawing.Point(497, 150);
            this.c_Boton3.Name = "c_Boton3";
            this.c_Boton3.Size = new System.Drawing.Size(150, 90);
            this.c_Boton3.TabIndex = 15;
            this.c_Boton3.TabStop = false;
            this.c_Boton3.Text = "3";
            this.c_Boton3.UseVisualStyleBackColor = true;
            this.c_Boton3.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton2
            // 
            this.c_Boton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton2.Location = new System.Drawing.Point(325, 150);
            this.c_Boton2.Name = "c_Boton2";
            this.c_Boton2.Size = new System.Drawing.Size(150, 90);
            this.c_Boton2.TabIndex = 14;
            this.c_Boton2.TabStop = false;
            this.c_Boton2.Text = "2";
            this.c_Boton2.UseVisualStyleBackColor = true;
            this.c_Boton2.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton4
            // 
            this.c_Boton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton4.Location = new System.Drawing.Point(153, 248);
            this.c_Boton4.Name = "c_Boton4";
            this.c_Boton4.Size = new System.Drawing.Size(150, 90);
            this.c_Boton4.TabIndex = 13;
            this.c_Boton4.TabStop = false;
            this.c_Boton4.Text = "4";
            this.c_Boton4.UseVisualStyleBackColor = true;
            this.c_Boton4.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton1
            // 
            this.c_Boton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton1.Location = new System.Drawing.Point(153, 150);
            this.c_Boton1.Name = "c_Boton1";
            this.c_Boton1.Size = new System.Drawing.Size(150, 90);
            this.c_Boton1.TabIndex = 12;
            this.c_Boton1.TabStop = false;
            this.c_Boton1.Text = "1";
            this.c_Boton1.UseVisualStyleBackColor = true;
            this.c_Boton1.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // FormInciarContabilidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormInciarContabilidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormIniciarSesion";
            this.Load += new System.EventHandler(this.FormIniciarSesion_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label c_EtiquetaPassword;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Button c_Boton0;
        private System.Windows.Forms.Button c_Boton9;
        private System.Windows.Forms.Button c_Boton8;
        private System.Windows.Forms.Button c_Boton6;
        private System.Windows.Forms.Button c_Boton5;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_Boton7;
        private System.Windows.Forms.Button c_Boton3;
        private System.Windows.Forms.Button c_Boton2;
        private System.Windows.Forms.Button c_Boton4;
        private System.Windows.Forms.Button c_Boton1;
        private System.Windows.Forms.Button c_BackSpace;
        private System.Windows.Forms.Label label1;
    }
}