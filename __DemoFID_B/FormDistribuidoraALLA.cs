﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormDistribuidoraALLA : Form
    {
        public FormDistribuidoraALLA()
        {
            InitializeComponent();
        }

        private void FormDistribuidoraALLA_Load(object sender, EventArgs e)
        {
            try
            {
                c_Empresa.Image = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenEmpresa"]);
                c_Empresa.SizeMode = PictureBoxSizeMode.Zoom;
            }
            catch (Exception ex)
            {

                
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            using (FormEmpleado f_Inicio = new FormEmpleado())
            {
                f_Inicio.Empresa = Globales.ClaveClienteProsegur;
                f_Inicio.ShowDialog();
            }

            Close();

            GC.Collect();
        }
    }
}
