﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormAltaUsers : Form
    {
        DataTable t_perfil;
        int l_idTeclado = 0;
        public FormAltaUsers()
        {
            InitializeComponent();
        }

        private void c_Cancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Alta_Click(object sender, EventArgs e)
        {
            
            if (!String.IsNullOrEmpty(c_IdUsuario.Text))
            {
                c_confirmacionContraseña.Text = c_IdUsuario.Text;
                c_Contraseña.Text = c_IdUsuario.Text;    

                if (c_confirmacionContraseña.Text == c_Contraseña.Text)
                {
                    if (!BDUsuarios.InsertarUsuario(c_IdUsuario.Text, int.Parse(c_perfiles.SelectedValue.ToString()), c_Nombre.Text.ToUpper (), c_referencia.Text, c_convenio.Text,  c_Contraseña.Text))
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text ="No se pudo Dar de Alta, revise los campos o verifique que el EMPLEADO no exista";
                            f_Error.ShowDialog();
                            return;
                        }
                    }
                    else
                    {
                       
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text ="EMPLEADO dado de alta exitosamente";
                            f_Error.ShowDialog();
                            Close();
                        }
                    }


                     }else{
                          using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text ="Las Contraseñas no Coinciden";
                        f_Error.ShowDialog();
                        return;
                    }

                     }
            }
            else {
                     using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text ="Proporciona un numero de EMPLEADO";
                        f_Error.ShowDialog();
                        return;
                    }
                     }
        }

        private void FormAltaUsers_Load(object sender, EventArgs e)
        {
            t_perfil = BDPerfiles.TraerPerfiles();


            c_perfiles.DataSource = t_perfil;
            c_perfiles.DisplayMember ="Descripcion";
            c_perfiles.ValueMember ="IdPerfil";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            l_idTeclado = Globales.LlamarTeclado();
        }

        private void c_IdUsuario_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
               
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_IdUsuario. Text = l_teclado.Captura;

            }
        }

        private void c_Nombre_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_Nombre . Text = l_teclado.Captura.ToUpper();

            }
        }

        private void c_Contraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_Contraseña.Text = l_teclado.Captura;

            }
        }

        private void c_confirmacionContraseña_Click(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {
                l_teclado.Password(true);
                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                    c_confirmacionContraseña . Text = l_teclado.Captura;

            }
        }

        private void c_referencia_TextChanged(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                   c_referencia. Text = l_teclado.Captura;

            }
        }

        private void c_convenio_TextChanged(object sender, EventArgs e)
        {
            using (FormTeclado l_teclado = new FormTeclado())
            {

                DialogResult l_respuesta = l_teclado.ShowDialog();

                if (l_respuesta == DialogResult.OK)
                   c_convenio. Text = l_teclado.Captura;

            }
        }

       
    }
}
