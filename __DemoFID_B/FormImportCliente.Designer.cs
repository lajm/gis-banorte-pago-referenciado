﻿namespace __DemoFID_B
{
    partial class FormImportCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_PathChoose = new System.Windows.Forms.Button();
            this.Client_File = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c_PathChoose
            // 
            this.c_PathChoose.Location = new System.Drawing.Point(33, 29);
            this.c_PathChoose.Margin = new System.Windows.Forms.Padding(2);
            this.c_PathChoose.Name = "c_PathChoose";
            this.c_PathChoose.Size = new System.Drawing.Size(150, 55);
            this.c_PathChoose.TabIndex = 1;
            this.c_PathChoose.Text = "Busqueda de Archivo";
            this.c_PathChoose.UseVisualStyleBackColor = true;
            this.c_PathChoose.Click += new System.EventHandler(this.c_PathChoose_Click);
            // 
            // Client_File
            // 
            this.Client_File.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(245, 29);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 55);
            this.button1.TabIndex = 2;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormImportCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 104);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.c_PathChoose);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormImportCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Actualización de la información del cliente";
            this.Load += new System.EventHandler(this.FormImportCliente_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button c_PathChoose;
        private System.Windows.Forms.OpenFileDialog Client_File;
        private System.Windows.Forms.Button button1;
    }
}