﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class uc_PanelInferior : UserControl
    {
        public uc_PanelInferior()
        {
            InitializeComponent();
            c_usuario.Text = Globales.IdUsuario;

            
        }

        private void c_TimerFechaHora_Tick(object sender, EventArgs e)
        {
            c_FechaHora.Text = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
        }

        private void uc_PanelInferior_Load(object sender, EventArgs e)
        {
            c_FechaHora.Text = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
            c_TimerFechaHora.Enabled = true;
        }
    }
}
