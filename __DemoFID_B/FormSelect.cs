﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormSelect : Form
    {
        

        public FormSelect()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void c_Deposito_Click(object sender, EventArgs e)
        {
            timer1.Stop();

            using (FormEmpleado f_Inicio = new FormEmpleado())
            {
                f_Inicio.Empresa = Globales.ClaveClienteProsegur;
                f_Inicio.ShowDialog();
            }
           
            Close();
            
        }

        private void c_Administrar_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            using (FormSesion f_Inicio = new FormSesion())
            {
               
                f_Inicio.l_IdUsuario = "Supervisor_ZONA";
                f_Inicio.ShowDialog();
            }
            Close();
        }

        private void c_Etv_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            using (FormSesion f_Inicio = new FormSesion())
            {
                
                f_Inicio.l_IdUsuario = "ETV_Traslado";
                f_Inicio.ShowDialog();
            }
          
            
            Close();
        }

        private void FormSelect_Load(object sender, EventArgs e)
        {
            
            Globales.IdUsuario = "";
            Double l_numbilletesenbolsa =BDDeposito.ObtenerTotalBilletes();
            if (l_numbilletesenbolsa == 0)
            {
               Globales.Alertastock_enviada = false;
                label2.Text = "0.00 %";
                Globales.Iniciar_moveelevator();
            }



            else
            {
                Double l_decimal = (l_numbilletesenbolsa / Globales.Capacidad_Bolsa1);
                l_numbilletesenbolsa = (l_decimal * 100);
                label2.Text = l_numbilletesenbolsa.ToString("###.##") + " %";

             /*   if (Globales.DEBUG)
                {
                    int l_posición = (int)(Math.Round(l_decimal, 1) * 10);

                   if (Globales.g_movelevator[l_posición] == false)
                    {
                        try
                        {
                            int l_result;

                            using (FormError f_Error = new FormError(false))
                            {
                                f_Error.c_MensajeError.Text = "Espere un momento ajustando Bolsa";
                                f_Error.Show();

                                // c_BotonCancelar_Click(this, null);




                                l_result = SidApi.SidLib.SID_Open(true);
                                l_result = SidApi.SidLib.SID_MoveElevatorBag();

                                if (l_result == SidApi.SidLib.SID_OKAY )
                                    Globales.g_movelevator[l_posición] = true;

                                l_result = SidApi.SidLib.SID_Close();
                                Globales.g_movelevator[l_posición] = true;

                                f_Error.Close();
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }*/

                if (l_numbilletesenbolsa > 79 && !Globales.Alertastock_enviada  )
                {
                    Prosegur.AlertaSTOCK();
                    Globales.Alertastock_enviada = true;
                }

                if (l_numbilletesenbolsa > 99)
                {
                    c_Deposito.Enabled = false;
                    pictureBox3.Enabled = false;
                }
                else
                    c_Deposito.Enabled = true;
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void c_contabilidad_Click(object sender, EventArgs e)
        {
            c_contabilidad.Enabled = false;
            using (FormConsultaEfectivo f_ConsultaPesos = new FormConsultaEfectivo())
            {
                f_ConsultaPesos.ShowDialog();
            }
            c_contabilidad.Enabled = true;
        }

       
    }
}
