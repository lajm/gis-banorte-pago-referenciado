﻿/******************************************************************************
 * 
 * COMPATIBLE CHECK READER PROJECT
 * 
 * PROGRAMMED BY: LEONARDO PAGÉS TUÑÓN
 * 
 * COPYRIGHT(c) 2011: SYMETRY S.A. DE C.V
 * DERECHOS DE AUTOR 2011: SYMETRY S.A. DE C.V.
 * 
 * PROHIBIDO QUITAR LOS COMENTARIOS DE ESTA SECCIÓN
 * PROHIBIDO MODIFICAR LOS NOMBRES DE COMPONENTES
 * PROHIBIDA SU COMERCIALIZACIÓN
 * 
 *****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public class FidApi
    {
        #region CONSTANTES 

        public static bool g_flag_Control_CajaFuerte;
        public static bool g_flag_Apertura_CajaFuerte;

        public static long g_LastError;
        public String g_szLastCommand;
        public String g_szLastErrorDescription;

        public String g_Message1;
        public String g_Message2;
        public String g_Risposta;

        public static byte g_vdoor_status;

        public const byte BITNONE = 0x00;
        public const byte BIT0 = 0x01;
        public const byte BIT1 = 0x02;
        public const byte BIT2 = 0x03;
        public const byte BIT3 = 0x04;
        public const byte BIT4 = 0x10;
        public const byte BIT5 = 0x20;
        public const byte BIT6 = 0x40;
        public const byte BIT7 = 0x80;

        // rs232_photo[0]
        public const byte BIT_STATUS_FEEDER = BIT2;
        public const byte BIT_STATUS_SUSPECT = BIT3;
        public const byte BIT_STATUS_REJECT = BIT4;
        public const byte BIT_STATUS_ESCROW = BIT6;

        // rs232_photo[3]
        public const byte BIT_STATUS_FEED_RUNNING = BIT1;

        public const byte FI2D_CONNECTION_USB = 0x55;
        public const byte FI2D_CONNECTION_LAN = 0x4c;


        public const byte SCAN_EXTRAMODE_NONE = 0x30;//'0';
        public const byte SCAN_EXTRAMODE_UV = 0x31;//'1';
        public const byte SCAN_EXTRAMODE_NOSCAN = 0x32;//'2';

        // Magnetic scan mode (ScanMagneticMode) 
        public const byte BIT_SCAN_MAGNETIC_NO = BIT7;
        public const byte BIT_SCAN_MAGNETIC_YES = BIT0;
        public const byte BIT_SCAN_MAGNETIC_NOCOMP = BIT1;	// reserved
        public const byte BIT_SCAN_MAGNETIC_COUNTERS = BIT2;	// reserved
        public const byte BIT_SCAN_MAGNETIC_SINGLE_CHAN = BIT3;	// reserved

        // BIT_SCAN_MAGNETIC_SINGLE_CHAN		// reserved
        public const byte MAGNETIC_SINGLE_CHAN_0 = 0x30;//'0';
        public const byte MAGNETIC_SINGLE_CHAN_1 = 0x31;//'1';
        public const byte MAGNETIC_SINGLE_CHAN_2 = 0x32;//'2';
        public const byte MAGNETIC_SINGLE_CHAN_3 = 0x33;//'3';
        public const byte MAGNETIC_SINGLE_CHAN_4 = 0x34;//'4';
        public const byte MAGNETIC_SINGLE_CHAN_5 = 0x35;//'5';
        public const byte MAGNETIC_SINGLE_CHAN_6 = 0x36;//'6';
        public const byte MAGNETIC_SINGLE_CHAN_7 = 0x37;//'7';
        public const byte MAGNETIC_SINGLE_CHAN_8 = 0x38;//'8';
        public const byte MAGNETIC_SINGLE_CHAN_9 = 0x39;//'9';
        public const byte MAGNETIC_SINGLE_CHAN_10 = 0x41;//'A';


        // Validate
        public const byte BIT_VALIDATE_NONE = BIT7;
        public const byte BIT_VALIDATE_BANKNOTE = BIT0;
        public const byte BIT_VALIDATE_CHECK = BIT1;
        public const byte BIT_VALIDATE_LOCALIZATION = BIT2; // reserved
        public const byte BIT_VALIDATE_THICKNESS = BIT3; // reserved
        public const byte BIT_VALIDATE_COUNTERS = BIT4; // reserved


        // Codeline
        public const byte NO_READ_CODELINE = 0x30;//'0';
        public const byte READ_CODELINE_MICR = 0x31;//'1';
        public const byte READ_BARCODE_PDF417 = 0x32;//'2';
        public const byte READ_BARCODE_2_OF_5 = 0x33;//'3';
        public const byte READ_BARCODE_CODE39 = 0x34;//'4';
        public const byte READ_BARCODE_CODE128 = 0x35;//'5';
        public const byte READ_BARCODE_EAN13 = 0x36;//'6';
        public const byte READ_CODELINE_SW_OCRA = 0x41;//'A';
        public const byte READ_CODELINE_SW_OCRB_NUM = 0x42;//'B';
        public const byte READ_CODELINE_SW_OCRB_ALFANUM = 0x43;//'C';
        public const byte READ_CODELINE_SW_OCRB_ITALY = 0x46;//'F';
        public const byte READ_CODELINE_SW_E13B = 0x45;//'E';
        public const byte READ_ONE_CODELINE_TYPE = 0x4e;//'N';
        public const byte READ_CODELINE_SW_MULTI_READ = 0x4d;//'M';
        public const byte READ_CODELINE_SW_E13B_X_OCRB = 0x58;//'X';
        public const byte READ_CODELINE_CMC7 = 0x5a;//'Z';

        // ResetType
        public const byte RESET_SW_ERROR = 0x30;//'0';
        public const byte RESET_HW_FREE_PATH = 0x31;//'1';

        // Codeline position
        public const byte CODELINE_NOT_PRESENT = 0x30;//'0';
        public const byte CODELINE_ON_FRONT_DOWN = 0x31;//'1';
        public const byte CODELINE_ON_FRONT_UP = 0x32;//'2';
        public const byte CODELINE_ON_REAR_DOWN = 0x33;//'3';
        public const byte CODELINE_ON_REAR_UP = 0x34;//'4';

        /*	ScanMode:
	        SPEED0: 2822 mm/s not_scanned_notes (for counting)  (~1500 doc/min)
	        SPEED1: 1411 mm/s not_interlaced_checks 200x200 dpi (~ 400 doc/min)
	interlaced_checks     200x100 dpi (~ 400 doc/min)
	        SPEED2: 2285 mm/s not_interlaced_notes  200x120 dpi (~1000 doc/min)
	interlaced_notes      200x60  dpi (~1000 doc/min)
	        SPEED3:  705 mm/s interlaced_checks     200x200 dpi (~ 200 doc/min)
        */
        // Parameter ScanMode
        public const byte SCAN_MODE_SPEED0 = 0x30;//'0'; // no-scan
        public const byte SCAN_MODE_SPEED1_Y = 0x31;//'1'; // 200x200 (default for checks)
        public const byte SCAN_MODE_SPEED1_YNC = 0x32;//'2'; // 200x200
        public const byte SCAN_MODE_SPEED1_IR = 0x33;//'3'; // 200x200
        public const byte SCAN_MODE_SPEED1_IRNC = 0x34;//'4'; // 200x200
        public const byte SCAN_MODE_SPEED1_BOTH = 0x35;//'5'; // 200x100
        public const byte SCAN_MODE_SPEED1_BOTHNC = 0x36;//'6'; // 200x100
        public const byte SCAN_MODE_SPEED2_Y = 0x37;//'7'; // 200x120
        public const byte SCAN_MODE_SPEED2_YNC = 0x38;//'8'; // 200x120
        public const byte SCAN_MODE_SPEED2_IR = 0x39;//'9'; // 200x120
        public const byte SCAN_MODE_SPEED2_IRNC = 0x41;//'A'; // 200x120
        public const byte SCAN_MODE_SPEED2_BOTH = 0x42;//'B'; // 200x60 (default for banknotes)
        public const byte SCAN_MODE_SPEED2_BOTHNC = 0x43;//'C'; // 200x60 
        public const byte SCAN_MODE_SPEED3_BOTH = 0x44;//'D'; // 200x200
        public const byte SCAN_MODE_SPEED3_BOTHNC = 0x45;//'E'; // 200x200

        public const byte SCAN_DOC_SPEED_200 = 0x30;//'0';
        public const byte SCAN_DOC_SPEED_120 = 0x32;//'2';

        public const byte SCAN_SOURCE_COUNTERS = 0x30;//'0';	// reserved
        public const byte SCAN_SOURCE_SCANNER = 0x31;//'1';

        public const byte SCAN_SOURCE_ENCODER_YES = 0x30;//'0';
        public const byte SCAN_SOURCE_ENCODER_NO = 0x31;//'1';	// reserved

        // Parameter ClearBlack
        public const byte NO_CLEAR_BLACK = 0x30;//'0';
        public const byte CLEAR_ALL_BLACK = 0x31;//'1';
        public const byte CLEAR_AND_ALIGN_IMAGE = 0x32;//'2';
        public const byte ALIGN_IMAGE = 0x33;//'3';

        // Parameter Side
        public const byte SIDE_FRONT_IMAGE = 0x46;//'F';
        public const byte SIDE_BACK_IMAGE = 0x52;//'R';
        public const byte SIDE_ALL_IMAGE = 0x42;//'B';
        public const byte SIDE_NONE_IMAGE = 0x4e;//'N';

        // Parameter SaveMode
        public const byte IMAGE_SAVE_NONE = 0x30;//'0'; // not used
        public const byte IMAGE_SAVETO_FILE = 0x31;//'1';
        public const byte IMAGE_SAVETO_MEMORY = 0x32;//'2';
        public const byte IMAGE_SAVE_BOTH = 0x33;//'3';

        // Parameter FileFormat
        public const byte SAVE_JPEG = 0x30;//'0';
        public const byte SAVE_BMP = 0x31;//'1';

        // Parameter: DocType 
        public const byte DOCTYPE_SCAN_CHECK = 0x41;//'A';
        public const byte DOCTYPE_SCAN_BANKNOTE = 0x42;//'B';

        // Parameter: drawer_id
        public const byte SAFE_DRAWER_NOVALUE = 0x3F;//           '?'
        public const byte SAFE_DRAWER_ALL = 0x30;//'0';
        public const byte SAFE_DRAWER_1 = 0x31;//'1';
        public const byte SAFE_DRAWER_2 = 0x32;//'2';
        public const byte SAFE_DRAWER_3 = 0x33;//'3';
        public const byte SAFE_DRAWER_4 = 0x34;//'4';
        public const byte SAFE_DRAWER_BAG = 0x80;

        public const int MAX_CODELINE_LEN = 256;	// max length of returned codeline

        // max number of documents handled by the DLL
        //public const int MAXDOC_TOTAL = 400;
        public const int MAXDOC_TOTAL_400 = 400;

        // Parameter: NumDocument (to scan all docs present in the feeder)
        public const int SCAN_ALL_DOCS = 999;


        //
        // document types (by validation process) 
        //
        //#define DOCTYPE_NOTYPE					(byte)0xFF
	        // 0x2x: extra errors (not from validation)
        public const byte DOCTYPE_NOTYPE = 0xFF;
        public const byte DOCTYPE_VALIDATE_TIMEOUT = 0x20;        // (es.: validation time too long)
        public const byte DOCTYPE_VALIDATE_SEQUENCE = 0x24;	    // validation sequence error
        public const byte DOCTYPE_VALIDATE_FEED_ANOMALY = 0x2D;	// feed anomaly
	        // doc_thickness[]
        public const byte DOCTYPE_THICKNESS_DOUBLE = 0x21;        // thickness: Double feed
        public const byte DOCTYPE_THICKNESS_NODOC = 0x22;         // thickness: No doc
        public const byte DOCTYPE_THICKNESS_OVERFLOW = 0x23;      // thickness: Signal Overflow
        public const byte DOCTYPE_THICKNESS_GENERIC_ERR = 0x26;   // thickness: 
        public const byte DOCTYPE_THICKNESS_GLITCH_ERR = 0x25;    // thickness: glitch
        public const byte DOCTYPE_THICKNESS_NO_ANW_ERR = 0x2E;	// thickness: no response
        public const byte DOCTYPE_THICKNESS_SIGNERR = 0x2F;       // thickness: Unknown signal error
	        // 0x3x: accepted
        public const byte DOCTYPE_CHECK = 0x30;           // checks
        public const byte DOCTYPE_1 = 0x31;  // 1
        public const byte DOCTYPE_2 = 0x32;  // 2
        public const byte DOCTYPE_5 = 0x33;  // 5
        public const byte DOCTYPE_10 = 0x34; // 10
        public const byte DOCTYPE_20 = 0x35; // 20
        public const byte DOCTYPE_50 = 0x36; // 50
        public const byte DOCTYPE_100 = 0x37;// 100
        public const byte DOCTYPE_200 = 0x38;// 200
        public const byte DOCTYPE_500 = 0x39;// 500
        public const byte DOCTYPE_1000 = 0x3A;            // 1000
	        // 0x4x: errors during visible (gray) recognition
        public const byte VIS_SUSPECT = 0x40;         // neither near nor far from a template
        public const byte VIS_REJECTED = 0x41;        // unknown
	        // 0x5x: errors during IR recognition
        public const byte IR_SUSPECT = 0x50;          // neither near nor far from a template
        public const byte IR_REJECTED = 0x51;         // unknown
	        // 0x6x: magnetic check
        public const byte MG_CHECK_FAILED = 0x60;
	        // 0x7x: UV check
        public const byte UV1_CHECK_FAILED = 0x70;
        public const byte UV2_CHECK_FAILED = 0x71;
	        // 0x8x: pre-fitness errors
        public const byte DOC_BAD_LOCALIZATION = 0x80;
        public const byte DOC_BAD_DIMENSIONS = 0x81;      // before classification
        public const byte DOC_UNEXPECTED_DIM = 0x82;      // after classification
        public const byte DOC_BAD_POSITION = 0x83;
        public const byte DOC_BAD_SHAPE = 0x84;
        public const byte DOC_BLOBS_PRESENT = 0x85;
	        // 0x9x: software errors
        public const byte INTERNAL_ERROR = 0x90;      // internal dll error
        public const byte PARAMETERS_ERROR = 0x91;    // invalid function arguments
        public const byte DOCTYPE_COUNTERDATA_FRONT_ERR = 0xDA; // reserved
        public const byte DOCTYPE_COUNTERDATA_REAR_ERR = 0xDB;  // reserved
        public const byte DOCTYPE_COUNTERDATA_MAG_ERR = 0xDC;   // reserved
        public const byte DOCTYPE_COUNTERDATA_UV_ERR = 0xDD;    // reserved
        public const byte DOCTYPE_COUNTERDATA_OK = 0xDE;	    // reserved
        public const byte DOCTYPE_OK = 0xDF;	                // reserved


        // 
        // status values (Statusbyte[0]) 
        // 
        public const byte SK_NOERROR = 0x00;
        public const byte SK_BUSY = 0x02;
        public const byte SK_BADVALUE = 0xFF;


        // 
        // reply codes
        //

        public const short NS_OKAY = 0;

        // warnings
        public const short NS_SCANNER_F_NOT_CONNECTED = 8;
        public const short NS_SCANNER_R_NOT_CONNECTED = 9;

        //// errors
        public const byte NS_SUSPECT_BOX_FULL = 0xF4;
        public const byte NS_REJECT_BOX_FULL = 0xF5;
        public const byte NS_REJECT_BOX_NOTEMPTY = 0xF6;
        public const byte NS_ESCROW_BOX_NOTEMPTY = 0xF7;
        public const byte NS_ESCROW_BOX_FULL = 0xF8;

        public const byte OPTIC_RET_FPGA_ERR = 0x20;
        public const byte OPTIC_RET_AD_ERR = 0x21;
        public const byte OPTIC_RET_SRAM_ERR = 0x22;
        public const byte THICKNESS_RET_TIMEOUT = 0x23;
        public const byte UV_MAGNETIC_RET_TIMEOUT = 0x24;

        public const short RS232_OKAY = NS_OKAY;
        public const byte RS232_SEND_ERR = 0x31;
        public const byte RS232_RECEIVE_ERR = 0x32;
        public const byte RS232_COMM_ERR = 0x33;
        public const byte RS232_OPEN_ERR = 0x34;
        public const byte RS232_CLOSE_ERR = 0x35;
        public const byte RS232_GENERIC_ERR = 0x36;
        public const byte RS232_NACK_ERR = 0x37;
        //public const byte RS232_DMA_TIMEOUT_ERR = 0x38;

        // MOTORS retcodes
        public const byte MOT_TX_CAN_ERROR = 0x40;
        public const byte MOT_RX_CAN_TRANSP_ERROR = 0x41;
        public const byte MOT_RX_CAN_DRAWER_ERROR = 0x42;
        public const byte MOT_FRONTDOOR_OPEN_WRN = 0x43;
        public const byte MOT_REARDOOR_OPEN_WRN = 0x44;
        public const byte MOT_NO_ESCROWBOX_WRN = 0x45;
        public const byte MOT_PHOTO_REJ_SX_WRN = 0x46;
        public const byte MOT_PHOTO_REJ_DX_WRN = 0x47;
        public const byte MOT_PHOTO_START_SX_WRN = 0x48;
        public const byte MOT_PHOTO_START_DX_WRN = 0x49;
        public const byte MOT_FEED_ANOMALY = 0x4A;

        public const byte MOT_SYNC_ERR = 0x6D;
        public const byte MOT_PRESSA_ERR = 0x6E;
        public const byte MOT_SEQ_VALIDAZIONE_ERR = 0x6F;
        public const byte MOT_FEEDER_EMPTY = 0x70;
        public const byte NS_WRN_FEEDER_EMPTY = MOT_FEEDER_EMPTY;
        public const byte MOT_REJECT_TIMEOUT = 0x71;
        public const byte MOT_BOURRAGE_PHOTO_START = 0x72;
        public const byte MOT_BOURRAGE_PHOTO_DISCARD = 0x73;
        public const byte MOT_PHOTO_CALIB_ERROR = 0x74;
        public const byte MOT_DMA_COMMAND_ERR = 0x75;
        public const byte MOT_SKEW_ERROR = 0x76;
        public const byte MOT_DISPENSE_ERROR = 0x77;
        public const byte MOT_DEPOSIT_ERROR = 0x78;
        public const byte MOT_DEPOSIT_ANOMALY = 0x79;
        public const byte MOT_DEPOSIT_NO_DOC = 0x7A;
        public const byte MOT_FEED_ERR = 0x7B;
        public const byte MOT_ESCROW_ERROR = 0x7C;
        public const byte MOT_TRANSPORT_ERR = 0x7D;
        public const byte MOT_STACKER_ERR = 0x7E;
        public const byte MOT_TRANSPORT_V_ERR = 0x7F;

        // trasporto cassetti
        public const byte CASS_CAN_TX = 0x80;
        public const byte CASS_CAN_RX = 0x81;
        public const byte CASS_PHOTO_CALIB_ERR = 0x82;
        public const byte CASS_NOT_PRESENT = 0x83;
        public const byte CASS_JAM_SYNC = 0x84;
        public const byte CASS_JAM_SKEW1 = 0x85;
        public const byte CASS_JAM_SKEW2 = 0x86;
        public const byte CASS_JAM_IN_1 = 0x87;
        public const byte CASS_JAM_IN_2 = 0x88;
        public const byte CASS_JAM_IN_3 = 0x89;
        public const byte CASS_JAM_IN_4 = 0x8A;
        public const byte CASS_MICRO_SAFE_OPEN = 0x8B;
        public const byte CASS_DEPOSIT_ERROR = 0x8C;
        public const byte CASS_EEPROM_ERROR = 0x8D;
        public const byte CASS_MOT_TRANSPORT_ERR = 0x8E;

        public const byte NS_BAD_COMMAND = 0x90;
        public const byte NS_BAD_PARAMETER = 0x91;
        public const byte NS_COMMAND_ERROR = 0x93;

        // cassetti
        public const byte CASS_MOTOR1_ERR = 0xA0;
        public const byte CASS_MOTOR2_ERR = 0xA1;
        public const byte CASS_MOTOR3_ERR = 0xA2;
        public const byte CASS_MOTOR4_ERR = 0xA3;
        public const byte CASS_1_NO = 0xA5;
        public const byte CASS_2_NO = 0xA6;
        public const byte CASS_3_NO = 0xA7;
        public const byte CASS_4_NO = 0xA8;
        public const byte CASS_PHOTO1_ERR = 0xA9;
        public const byte CASS_PHOTO2_ERR = 0xAA;
        public const byte CASS_PHOTO3_ERR = 0xAB;
        public const byte CASS_PHOTO4_ERR = 0xAC;
        public const byte CASS_1_DOWN = 0xAD;
        public const byte CASS_2_DOWN = 0xAE;
        public const byte CASS_3_DOWN = 0xAF;
        public const byte CASS_4_DOWN = 0xB0;
        public const byte CASS_1_UP = 0xB1;
        public const byte CASS_2_UP = 0xB2;
        public const byte CASS_3_UP = 0xB3;
        public const byte CASS_4_UP = 0xB4;
        public const byte CASS_NO_PWM_C = 0xB5;
        public const byte CASS_NO_PWM_C1 = 0xB6;
        public const byte CASS_NO_PWM_C2 = 0xB7;
        public const byte CASS_NO_PWM_C3 = 0xB8;
        public const byte CASS_NO_PWM_C4 = 0xB9;
        public const byte CASS_PWM_CALIB_ERR = 0xBA;
        public const byte CASS_NO_RESET = 0xBB;
        public const byte CASS_NO_RESET1 = 0xBC;
        public const byte CASS_NO_RESET2 = 0xBD;
        public const byte CASS_NO_RESET3 = 0xBE;
        public const byte CASS_NO_RESET4 = 0xBF;
        public const byte CASS_NO_ENAB1 = 0xC0;
        public const byte CASS_NO_ENAB2 = 0xC1;
        public const byte CASS_NO_ENAB3 = 0xC2;
        public const byte CASS_NO_ENAB4 = 0xC3;
        public const byte CASS_NO_SPACE = 0xC4;
        public const byte CASS_1_BUSY = 0xC5;
        public const byte CASS_2_BUSY = 0xC6;
        public const byte CASS_3_BUSY = 0xC7;
        public const byte CASS_4_BUSY = 0xC8;

        //sacco
        public const byte BAG_JAM_SYNC_ERR = 0x94;
        public const byte BAG_JAM_ING1_ERR = 0x95;
        public const byte BAG_JAM_ING2_ERR = 0x96;
        public const byte BAG_JAM_ING3_ERR = 0x97;
        public const byte BAG_JAM_ING4_ERR = 0x98;
        public const byte BAG_JAM_ING0_ERR = 0x99;
        public const byte BAG_OPEN_ERR = 0x9A;
        public const byte BAG_TRANSPORT_MOTOR_ERR = 0x9B;
        public const byte BAG_NOT_INSERTED = 0x9C;
        public const byte BAG_CLOSED_ERR = 0x9D;


        //thickness
        public const byte TICKNESS_TARATURA_ERR = 0x10;
        public const byte TICKNESS_TIMEOUT_SPI_ERR = 0x11;
        public const byte TICKNESS_FLASH_ERR = 0x12;
        public const byte TICKNESS_NOT_PRESENT = 0x1F;

        // USBLOW return codes 

        public const int USBLOW_RET_BASE = 200;
        public const int USBLOW_RET_OK = 0;				       //	0 (zero is by default) 
        public const int USBLOW_RET_NODEVICE = (USBLOW_RET_BASE + 1);       //  201
        public const int USBLOW_RET_NOOPEN = (USBLOW_RET_BASE + 2);         //	202
        public const int USBLOW_RET_FILEOPEN_ERR = (USBLOW_RET_BASE + 3);   //	203
        public const int USBLOW_RET_FILEREAD_ERR = (USBLOW_RET_BASE + 4);   //	204
        public const int USBLOW_RET_FILEWRITE_ERR = (USBLOW_RET_BASE + 5);  //	205
        public const int USBLOW_RET_COMMAND_FAILED = (USBLOW_RET_BASE + 6); //	206
        public const int USBLOW_RET_FILE_HEX_ERR = (USBLOW_RET_BASE + 7);   //	207
        public const int USBLOW_RET_PARAM_ERR = (USBLOW_RET_BASE + 8);      //	208
        public const int USBLOW_RET_CMD_ERR = (USBLOW_RET_BASE + 1);        //	209
        //  ... add here new codes ...
        public const int USBLOW_RET_LAST = (USBLOW_RET_BASE + 1);
        public const int USBLOW_RET_RX_TIMEOUT = -116;


// USB2USB return codes (reserved)
//#ifndef USB2USB_RET_BASE
//#define USB2USB_RET_BASE				300
//enum {	USB2USB_RET_OK ,			//	0 (zero is by default) 
//        USB2USB_RET_NODEVICE = (USB2USB_RET_BASE+1) ,	// 301
//        USB2USB_RET_NOOPEN ,		//	302
//        USB2USB_RET_FILEOPEN_ERR ,	//	303
//        USB2USB_RET_FILEREAD_ERR ,	//	304
//        USB2USB_RET_FILEWRITE_ERR ,	//	305
//        USB2USB_RET_COMMAND_FAILED ,//	306
//        USB2USB_RET_FILE_HEX_ERR ,	//	307
//        USB2USB_RET_PARAM_ERR ,		//	308
//        USB2USB_RET_CMD_ERR ,		//	309
//  ... add here new codes ...
        //USB2USB_RET_LAST 
//}; 
//#endif//USB2USB_RET_BASE


        public const int LAN_RET_OPEN_ERR = 400;
        public const int LAN_RET_CLOSE_ERR = 401;
        public const int LAN_RET_WRITE_ERR = 402;
        public const int LAN_RET_READ_ERR = 403;
        public const int LAN_RET_READ_NODATA = 404;

        public const int NS_SYSTEM_ERROR = -501;
        public const int NS_USB_ERROR = -502;
        public const int NS_PERIFNOTFOUND = -503;
        public const int NS_MEMORY_ERROR = -504;
        public const int NS_NO_DOC = -505;
        //public const int NS_CLIENT_CONNECT_FAIL = -506;
        public const int NS_SDRAM_OVERFLOW = -506;
        public const int NS_READ_DMA_ERR = -507;
        public const int NS_SYNC_FILM_ERROR = -508;
        //public const int NS_TARGET_BUSY = -508;
        //public const int NS_INVALIDCMD = -509;
        //public const int NS_INVALIDPARAM = -510;
        //public const int NS_EXECCMD = -511;
        public const int NS_JPEG_ERROR = -512;
        public const int NS_CMDSEQUENCEERROR = -513;
        public const int NS_USER_ABORT_REQ = -514;
        public const int NS_NO_SERIAL_NUMBER = -516;
        //public const int NS_INVALID_HANDLE = -516;
        public const int NS_NO_LIBRARY_LOAD = -517;
        public const int NS_BMP_ERROR = -518;
        public const int NS_TIFF_ERROR = -519;
        //public const int NS_IMAGE_NO_FILMED = -521;
        public const int NS_IMAGE_TOO_SHORT = -522;
        //public const int NS_BARCODE_ERROR = -525;
        //public const int NS_INVALID_BARCODE_TYPE = -529;
        public const int NS_OPEN_NOT_DONE = -530;
        public const int NS_INVALID_CLEARBLACK = -532;
        //public const int NS_INVALID_SIDE = -533;
        public const int NS_MISSING_IMAGE = -534;
        //public const int NS_INVALID_TYPE = -535;
        public const int NS_INVALID_SAVEMODE = -536;
        //public const int NS_INVALID_PAGE_NUMBER = -537;
        public const int NS_INVALID_VALIDATE = -541;
        public const int NS_INVALID_CODELINE_TYPE = -542;
        public const int NS_INVALID_SCANMODE = -544;
        //public const int NS_DOCTYPE_VALIDATE_TIMEOUT = -545;
        //public const int NS_INVALID_FEEDER =	-546;
        //public const int NS_INVALID_SORTER = -547;
        public const int NS_MISSING_FILENAME = -549;
        //public const int NS_INVALID_QUALITY = -550;
        public const int NS_INVALID_FILEFORMAT = -551;
        //public const int NS_INVALID_COORDINATE = -552;
        //public const int NS_MISSING_HANDLE_VARIABLE = -553;
        //public const int NS_INVALID_POLO_FILTER = -554;
        //public const int NS_INVALID_SIZEH_VALUE = -556;
        //public const int NS_INVALID_FORMAT = -557;
        //public const int NS_INVALID_CMD_HISTORY = -560;
        //public const int NS_MISSING_BUFFER_HISTORY = -561;
        public const int NS_OPEN_FILE_ERROR = -563;
        //public const int NS_INVALID_METHOD = -565;
        //public const int NS_CALIBRATION_FAILED = -566;
        //public const int NS_CALIBRATION_DATA_ERR	= -567;
        public const int NS_INVALID_UNIT = -568;
        public const int NS_NO_MAGNETIC_CALIB = -569;
        public const int NS_MAGNETIC_HEAD_KO = -570;
        public const int NS_INVALID_NUMDOC = -572;
        //public const int NS_ILLEGAL_REQUEST = -573;
        public const int NS_NO_MAGNETIC_N_CHANNEL = -574;
        //public const int NS_INVALID_DEGREE = -577;
        public const int NS_ROTATE_ERROR = -578;
        //public const int NS_INVALID_SIDETOPRINT = -584;
        //public const int NS_DOUBLE_LEAF_ERROR = -585;
        public const int NS_INVALID_RESET_TYPE = -587;
        public const int NS_INVALID_CALLBACK_SET = -588;
        //public const int NS_IMAGE_NOT_200_DPI = -589;
        //public const int NS_DOWNLOAD_ERROR = -590;
        //public const int NS_INVALID_SORT_ON_CHOICE = -591;
        //public const int NS_HOPPER_COUNTER_ERR = -592;

        public const int NS_CALIB_DLLPRM_ERR = -600;
        public const int NS_CALIB_FILECOMP_ERR = -601;
        public const int NS_CALIB_DATACOMP_ERR = -602;
        public const int NS_CALIB_FILEPARAM_ERR = -603;
        public const int NS_CALIB_SYSTEM_ERR = -604;
        public const int NS_CALIB_OFFSET_ERR = -605;
        public const int NS_CALIB_PWM_ERR = -606;
        public const int NS_CALIB_GAIN_ERR = -607;
        public const int NS_CALIB_COEFF_ERR = -608;
        public const int NS_CALIB_PWM_R_ERR = -609;
        public const int NS_CALIB_PWM_G_ERR = -610;
        public const int NS_CALIB_PWM_IR_ERR = -611;

        public const int NS_DECODE_FONT_NOT_PRESENT = -1101;
        public const int NS_DECODE_INVALID_COORDINATE = -1102;
        public const int NS_DECODE_INVALID_OPTION = -1103;
        public const int NS_DECODE_INVALID_CODELINE_TYPE = -1104;
        public const int NS_DECODE_SYSTEM_ERROR = -1105;
        public const int NS_DECODE_DATA_TRUNC = -1106;
        public const int NS_DECODE_INVALID_BITMAP = -1107;
        public const int NS_DECODE_ILLEGAL_USE = -1108;

        public const int NS_BARCODE_GENERIC_ERROR = -1201;
        public const int NS_BARCODE_NOT_DECODABLE = -1202;
        public const int NS_BARCODE_OPENFILE_ERROR = -1203;
        public const int NS_BARCODE_READBMP_ERROR = -1204;
        public const int NS_BARCODE_MEMORY_ERROR = -1205;
        public const int NS_BARCODE_START_NOTFOUND = -1206;
        public const int NS_BARCODE_STOP_NOTFOUND = -1207;

//'------------------------------------------------------------------------------------------------
//' RETURN CODES
//'------------------------------------------------------------------------------------------------

        public const byte RC_OK = 0x0;
        public const byte RC_W_CMDEXE = 0x10;
        public const byte RC_E_CMDEXE = 0x11;
        public const byte RC_W_DIAGEXE = 0x12;
        public const byte RC_W_INSUFFICIENT_COIN = 0x13;
        public const byte RC_W_BOX_NEARFULL = 0x14;
        public const byte RC_W_BOX_FULL = 0x15;
        public const byte RC_W_INCOMPLETE_PAY = 0x16;
        public const byte RC_W_COVER_OPEN = 0x18;
        public const byte RC_W_NO_NEW_COIN = 0x19;
        public const byte RC_E_CMDABORT = 0x30;
        public const byte RC_E_TOOMANY_COIN = 0x50;
        public const byte RC_E_PHOTO_COVERED = 0x51;
        public const byte RC_E_PRECOIN_COVERED = 0x52;
        public const byte RC_E_CASH_STATUS = 0x55;
        public const byte RC_E_CLEANING = 0x56;
        public const byte RC_E_REJECT_FULL = 0x57;
        public const byte RC_E_DISPENSE_1C = 0x60;
        public const byte RC_E_DISPENSE_2C = 0x61;
        public const byte RC_E_DISPENSE_5C = 0x62;
        public const byte RC_E_DISPENSE_10C = 0x63;
        public const byte RC_E_DISPENSE_20C = 0x64;
        public const byte RC_E_DISPENSE_50C = 0x65;
        public const byte RC_E_DISPENSE_1E = 0x66;
        public const byte RC_E_DISPENSE_2E = 0x67;
        public const byte RC_E_PHOTO_EJECT = 0x6A;
        public const byte RC_E_VALIDATOR_TIMEOUT = 0x70;
        public const byte RC_E_FLASH_WRITE = 0x80;
        public const byte RC_E_EEPROM_INIT = 0x83;
        public const byte RC_E_NOCMD = 0x90;
        public const byte RC_E_PARAM = 0x91;

        public const long SW_BIAS = 0x100;
        public const long RC_E_BAD_COM_PARAM = (SW_BIAS + 0);
        public const long RC_E_COM_CLOSE_ERR = (SW_BIAS + 1);
        public const long RC_E_COM_OPEN_ERR = (SW_BIAS + 2);
        public const long RC_E_COM_WRITE_ERR = (SW_BIAS + 3);
        public const long RC_E_COM_READ_ERR = (SW_BIAS + 4);
        public const long RC_E_COM_NOTOPEN_ERR = (SW_BIAS + 5);
        public const long RC_E_SYSTEM_ERR = (SW_BIAS + 6);

        
        #endregion


        # region ESTRUCTURAS

//        public const int HWVERSION_LEN = 30;
//        public const int FWVERSION_LEN = 30;
//        public const int TRANSPORTVERSION_LEN = 30;
//        public const int THICKNESSVERSION_LEN = 30;
//        public const int UVMAGVERSION_LEN = 30;

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_INDENTIFY
//        {
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = HWVERSION_LEN + 1)]
//            public string HwVersion;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
//            public string  FwVersion;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = TRANSPORTVERSION_LEN + 1)]
//            public string TrVersion;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = THICKNESSVERSION_LEN + 1)]
//            public string ThicknessVersion;
//        };

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_INDENTIFY_USB2USB
//        {
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
//            public string usb2usb_FwExtVersion;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
//            public string usb2usb_FwIntVersion;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = HWVERSION_LEN + 1)]
//            public string usb3usb_HwVersion;
//        };
        

//        public const int DLLVERSION_LEN = 64;

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_DLLVERSION
//        {
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DLLVERSION_LEN)]
//            public string dllversion;// = new Char[DLLVERSION_LEN];
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DLLVERSION_LEN)]
//            public string doc_recognition;// = new Char[DLLVERSION_LEN];
//        };

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_STATUS
//        {
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
//            public byte[] Statusbyte;
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
//            public byte[] rs232_photo;// = new byte[4];      // BIT_STATUS_FEEDER, BIT_STATUS_ESCROW ...
//            public string rs232_error;      // motorboard error status 
//            public int rs232_totaldoc;     // number of documents scanned last time 
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXDOC_TOTAL_400)]
//            public byte[] DocsInfo;         // last scanned documents sequence (DOCTYPE_CHECK, DOCTYPE_1, ...)
//            public string flag_boxfull;
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAXDOC_TOTAL_400)]
//            public double[] DocAngleHor;// = new double[MAXDOC_TOTAL_400];      // return the horizontal angle
//        };

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_STATUS_USB2USB
//        {
//            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
//            public byte[] Statusbyte;
//        };
                
//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_AUTODOCHANDLE
//        {
//            public byte Validate;					// require the document validation (BIT_VALIDATE_BANKNOTE...)
//            public byte	Side;						// SIDE_FRONT_IMAGE  (...)
//            public short ScanSpeed;					// SCAN_MODE_SPEED0  (...)
//            public short ClearBlack;  				// require border cleaning (NO_CLEAR_BLACK ...)
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
//            public string NumDocument;				// number of required documents in ASCII format (SCAN_ALL_DOCS=999 for all)
//            public short SaveImage;					// require save to file (IMAGE_SAVETO_MEMORY ...)
//            public string SaveImageFolder;			// string format ( ex: "c:\myapp\images" )
//            public string SaveImageBasename;		// string format ( ex: "Image_" )
//            public short Unit;						// unused (0)
//            public float pos_x;						// unused (0)
//            public float pos_y;						// unused (0)
//            public float sizeW;						// unused (0)
//            public float sizeH;						// unused (0)
//            public short FileFormat;					// '1' = SAVE_BMP
//            public int Quality;					// used only for JPEG format
//            public int   SaveMode;					// used only for TIFF format (SAVE_OVERWRITE ...)
//            public int PageNumber;					// fixed = 1 (used only for TIFF)
//            public byte DocType;					// type of document to scan (DOCTYPE_SCAN_BANKNOTE ...)
//            public byte ScanSource;					// data source: '0'=counters (DEBUG) - '1'=scanner (SCAN_SOURCE_SCANNER ...)
//            public byte ScanExtraMode;				// extra scan mode (SCAN_EXTRAMODE_NONE - SCAN_EXTRAMODE_UV ...) 
//            public byte ScanMagneticMode;			// magnetic scan request (BIT_SCAN_MAGNETIC_NO ...)
//            public byte ScanSourceEncoder;			// tmp: '1'=timer (DEBUG) - '0'=encoder (SCAN_SOURCE_ENCODER_YES ...)
//            public byte ScanSingleChannel;			// MAGNETIC_SINGLE_CHAN_0...
//        };

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_GETDOCDATA
//        {
//            public int FrontImage;						// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//            public int BackImage; 						// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//            public int FrontImage_IR; 					// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//            public int BackImage_IR;					// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//            public int MagneticImage;					// (max. 15KB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_CODELINE_LEN)]
//            public string CodelineSW;
//            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_CODELINE_LEN)]
//            public string CodelineHW;
//            public byte DocInfo;						// return the document type (DOCTYPE_CHECK, DOCTYPE_1, DOCTYPE_CHECK_NO_MAGNETIC...)
//            public int NrDoc; 							// progressive document number (from 1)
//            public short CodelineType;					// select the type of codeline to read (READ_CODELINE_SW_E13B...)
//            public byte CodelineSide;					// return the codeline position read on document (CODELINE_ON_FRONT_DOWN...)
//            public double DocAngleHor;					// return the horizontal angle
//            public int pBufferUV1;						// return the pointer to the UV1 data
//            public int pBufferUV2;						// return the pointer to the UV2 data
//        };

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct READOPTIONS
//        {
//            public bool PutBlanks;
//            public String TypeRead;
//        };
////typedef struct _ReadOption
////{
////    IN	BOOL	PutBlanks;
////    IN	char	TypeRead;
////}READOPTIONS, *LPREADOPTIONS;



////// deposit module----------------------------

//        public static int MAXDOCTYPE_IN_DRAWER = 15;
//        public static int MAXDOC_IN_DRAWER_DEF = 2500;
//        public static byte DOCTYPE_FINELOG = 0x3F;
//        public static int TRANSPORT_PHOTO_LEN = 5;
//        public static int DRAWER_PHOTO_LEN = 5;

//        public static byte SAFE_DRAWER_0 = 0x30;//'0';
//        //public static char SAFE_DRAWER_1 = '1';
//        //public static char SAFE_DRAWER_2 = '2';
//        //public static char SAFE_DRAWER_3 = '3';
//        //public static char SAFE_DRAWER_4 = '4';
//        public static byte SAFE_BAG = 0x80;

////typedef struct _st_docdeposit
//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_DOCDEPOSIT
//        {
//            public byte drawer_id;					// box identify (SAFE_DRAWER_1...)
//            public uint doccount_todo;				// documents to be deposited
//            public uint doccount_done;				// documents deposited
//        };
////}ST_DOCDEPOSIT, *LPST_DOCDEPOSIT;


//////typedef struct _st_docdispense			// reserved
//////{
//////	IN	DWORD	doccount_todo;				// documents to be dispensed
//////	OUT	DWORD	doccount_done;				// documents dispensed
//////
//////}ST_DOCDISPENSE, *LPST_DOCDISPENSE;


//        public static char SAFE_MODULE_DRAWER = 'D';
//        public static char SAFE_MODULE_TRANSPORT = 'T';
//        public static int DRAWER_VERSION_LEN = 40;
////typedef struct _st_drawer_identify

//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_DRAWER_INDETIFY
//        {
//            public byte module;						// 'D'=Drawers 'T'=transport
//            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 41)]
//            public string version_D;
//            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 41)]
//            public string version_T;
//            //    OUT	char	version_D[DRAWER_VERSION_LEN+1];
//            //    OUT	char	version_T[DRAWER_VERSION_LEN+1];
//        };

////}ST_DRAWER_INDENTIFY, *LPST_DRAWER_INDENTIFY;


////typedef struct _st_drawer_reset
//        [StructLayoutAttribute(LayoutKind.Sequential)]
//        public struct ST_DRAWER_RESET
//        {
//            public byte drawer_id;					// box identify (SAFE_DRAWER_1...)
//        };

////}ST_DRAWER_RESET, *LPST_DRAWER_RESET;


////typedef struct _st_drawer_status
//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_DRAWER_STATUS
//        {
//            public byte stat_drawer1;
//            public byte stat_drawer2;
//            public byte stat_drawer3;
//            public byte stat_drawer4;
//            public long ndoc_drawer1;				// number of documents present in the drawer #1
//            public long ndoc_drawer2;				// number of documents present in the drawer #2
//            public long ndoc_drawer3;				// number of documents present in the drawer #3
//            public long ndoc_drawer4;				// number of documents present in the drawer #4
//        }
//        public static ST_DRAWER_STATUS g_ST_DRAWER_STATUS;
////}ST_DRAWER_STATUS, *LPST_DRAWER_STATUS;
////// deposit module----------------------------




////typedef struct _st_thickness_status
//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_THICKNESS_STATUS
//        {
//            public int	stat_thickness;
//        }
//        public static ST_THICKNESS_STATUS g_ST_THICKNESS_STATUS;
////}ST_THICKNESS_STATUS, *LPST_THICKNESS_STATUS;


////typedef struct _st_current_counters
//        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
//        public struct ST_CURRENT_COUNTERES
//        {
//            public int doc_inserted_total;
//            public int doc_inserted_last;
//            public int doc_in_escrow_total;
//            public int doc_in_escrow_last;
//            public int doc_in_reject_last;
//            public int doc_in_suspect_last;
//        };

        #endregion

        // SERVICE BOARD
        public static byte SERVICE_VERSION_LEN = 40;
        public static byte SERVICE_CMD_GETID = 0x49;           //'I'
        public static byte SERVICE_CMD_GETSTATUS = 0x53;       //'S'
        public static byte SERVICE_CMD_RESET = 0x52;           //'R'
        // VANDAL DOOR
        public static byte SERVICE_VDOOR_MODULE = 0x56;        //'V'
        public static byte SERVICE_CMD_VDOOR_UNLOCK = 0x41;    //'A'
        public static byte SERVICE_CMD_VDOOR_LOCK = 0x43;      //'C'
        public static byte SERVICE_CMD_VDOOR_STATUS = 0x53;    //'S'
        public static byte SERVICE_CMD_VDOOR_RESET = 0x52;     //'R'
//
        public static byte SERVICE_VDOOR_STAT_OPEN = 0x41;     //'A'
        public static byte SERVICE_VDOOR_STAT_CLOSE = 0x43;    //'C'
        public static byte SERVICE_VDOOR_STAT_NONE = 0x3F;     //'?'
        // ALARM SENSORS
        public static byte SERVICE_ALARM_SENSOR_LEN = 3;
        public static byte SERVICE_ALARM_BIT_SAFEOPEN = BIT0;
        public static byte SERVICE_ALARM_BIT_VDOOROPEN = BIT1;
        public static byte SERVICE_ALARM_BIT_VDOORANOMALY = BIT2; //'// viene resettato ad ogni comando "LOCK"
        public static byte SERVICE_ALARM_BIT_RESERVED3 = BIT3;
        public static byte SERVICE_ALARM_BIT_RESERVED4 = BIT4;
        public static byte SERVICE_ALARM_BIT_RESERVED5 = BIT5;
        public static byte SERVICE_ALARM_BIT_RESERVED6 = BIT6;
        public static byte SERVICE_ALARM_BIT_RESERVED7 = BIT7;
        public static byte SERVICE_ALARM_MODULE = 0x41;        //'A'
        public static byte SERVICE_CMD_ALARM_GET = 0x47;       //'G'

        
        public static void FI2D_GetErrorString(long lERR, String sLastCommand, out String g_szLastErrorDescr)
        {        
            g_szLastErrorDescr = "";

            switch (lERR)
            {
                case NS_OKAY:
                    g_szLastErrorDescr = "OK!";
                    break;
                case NS_BAD_COMMAND:
                    g_szLastErrorDescr = "Comando Inválido";
                    break;
                case NS_BAD_PARAMETER:
                    g_szLastErrorDescr = "Parámetro Inválido";
                    break;
                case NS_COMMAND_ERROR:
                    g_szLastErrorDescr = "Error de Comando";
                    break;
                case NS_SCANNER_F_NOT_CONNECTED:
                    g_szLastErrorDescr = "Escaner frontal desconectado";
                    break;
                case NS_SCANNER_R_NOT_CONNECTED:
                    g_szLastErrorDescr = "Escaner trasero desconectado";
                    break;
                //    '// --- WARNINGS ----------------------------------------------------------------------
                /*case NS_WRN_FEEDER_EMPTY:
                    g_szLastErrorDescr = "OK - (Feeder Empty)";
                    break;*/
                //'// case NS_WRN_ALREADY_OPEN:           g_szLastErrorDescr = "Peripheral already connected"
                //'// case NS_WRN_PERIF_BUSY:g_szLastErrorDescr = "Peripheral busy"
                //    '// --- ERRORS ------------------------------------------------------------------------
                case NS_SYSTEM_ERROR:
                    g_szLastErrorDescr = "Error de sistema";
                    break;
                case NS_MEMORY_ERROR:
                    g_szLastErrorDescr = "Error de memoria";
                    break;
                case NS_USB_ERROR:
                    g_szLastErrorDescr = "Error USB";
                    break;
                case NS_PERIFNOTFOUND:
                    g_szLastErrorDescr = "No se encontró el dispositivo";
                    break;
                //'// case NS_HARDERR:       g_szLastErrorDescr = "Device error (scan failed)"
                case NS_NO_DOC:
                    g_szLastErrorDescr = "Ningún documento";
                    break;
                case NS_READ_DMA_ERR:
                    g_szLastErrorDescr = "Error leyendo el DMA: ¡reinicie el dispositivo!";
                    break;
                case NS_SDRAM_OVERFLOW:
                    g_szLastErrorDescr = "SDRAM Overflow: ¡reinicie el dispositivo!";
                    break;
                //'// case NS_SYNC_FILM_ERROR:            g_szLastErrorDescr = "Sync Film Error: reset device!"
                //'// case NS_CLIENT_CONNECT_FAIL:        g_szLastErrorDescr = "Connection to server failed"
                //'// case NS_PAPER_JAM:     g_szLastErrorDescr = "Document jammed"
                //'// case NS_INVALIDCMD:    g_szLastErrorDescr = "Invalid command"
                //'// case NS_INVALIDPARAM:  g_szLastErrorDescr = "Invalid parameter"
                //'// case NS_EXECCMD:       g_szLastErrorDescr = "Overlapping commands"
                case NS_JPEG_ERROR:
                    g_szLastErrorDescr = "No se creó el JPEG";
                    break;
                case NS_CMDSEQUENCEERROR:
                    g_szLastErrorDescr = "Secuencia de comandos inválida";
                    break;
                case NS_USER_ABORT_REQ:
                    g_szLastErrorDescr = "Comando abortado por el usuario";
                    break;
                case NS_NO_LIBRARY_LOAD:
                    g_szLastErrorDescr = "No se cargó la librería de decodificación";
                    break;
                case NS_BMP_ERROR:
                    g_szLastErrorDescr = "No se creó el BMP";
                    break;
                case NS_TIFF_ERROR:
                    g_szLastErrorDescr = "No se creó el TIFF";
                    break;
                //'// case NS_IMAGE_NO_FILMED:            g_szLastErrorDescr = "Document not scanned"
                case NS_IMAGE_TOO_SHORT:
                    g_szLastErrorDescr = "La imagen es muy pequeña";
                    break;
                case NS_OPEN_NOT_DONE:
                    g_szLastErrorDescr = "Falló al abrir el dispositivo periférico";
                    break;
                case NS_INVALID_CLEARBLACK:
                    g_szLastErrorDescr = "Parámetro de clearblack inválido";
                    break;
                case NS_MISSING_IMAGE:
                    g_szLastErrorDescr = "No se encontró la imagen";
                    break;
                case NS_INVALID_SAVEMODE:
                    g_szLastErrorDescr = "Parámetro de guardado inválido";
                    break;
                //case NS_SAVE_BMP_ERROR: g_szLastErrorDescr = "Save BMP Error"; break;
                case NS_INVALID_VALIDATE:
                    g_szLastErrorDescr = "Parámetro de validación inválido";
                    break;
                case NS_INVALID_CODELINE_TYPE:
                    g_szLastErrorDescr = "Parámetro de codeline inválido";
                    break;
                case NS_INVALID_SCANMODE:
                    g_szLastErrorDescr = "Parámetro de scanmode inválido";
                    break;
                //'// case NS_DOCTYPE_VALIDATE_TIMEOUT:   g_szLastErrorDescr = "Document validate timeout"
                case NS_MISSING_FILENAME:
                    g_szLastErrorDescr = "Parámetro de nombre de archivo inválido";
                    break;
                case NS_INVALID_FILEFORMAT:
                    g_szLastErrorDescr = "Parámetro de formato de archivo inválido";
                    break;
                case NS_OPEN_FILE_ERROR:
                    g_szLastErrorDescr = "Error de archvio o archivo no encontrado";
                    break;
                case NS_INVALID_UNIT:
                    g_szLastErrorDescr = "Parámetro de unidad de medida inválido";
                    break;
                //'// case NS_CALIBRATION_FAILED:         g_szLastErrorDescr = "Calibration failed"
                //'// case NS_CALIBRATION_DATA_ERR:       g_szLastErrorDescr = "Calibration data Error"
                //    '//
                case NS_CALIB_DLLPRM_ERR:
                    g_szLastErrorDescr = "Error de parámetro de SW";
                    break;
                case NS_CALIB_FILECOMP_ERR:
                    g_szLastErrorDescr = "Falta archivo de calibración o es inválido";
                    break;
                case NS_CALIB_DATACOMP_ERR:
                    g_szLastErrorDescr = "Información de calibración inválido";
                    break;
                case NS_CALIB_FILEPARAM_ERR:
                    g_szLastErrorDescr = "Parámetro de calibración inválido";
                    break;
                case NS_CALIB_SYSTEM_ERR:
                    g_szLastErrorDescr = "Error de memoria en calibración";
                    break;
                case NS_CALIB_OFFSET_ERR:
                    g_szLastErrorDescr = "Error OFFSET calibración";
                    break;
                case NS_CALIB_PWM_ERR:
                    g_szLastErrorDescr = "Error PWM calibración";
                    break;
                case NS_CALIB_PWM_R_ERR:
                    g_szLastErrorDescr = "PWM RED Error";
                    break;
                case NS_CALIB_PWM_G_ERR:
                    g_szLastErrorDescr = "PWM GREEN Error";
                    break;
                case NS_CALIB_PWM_IR_ERR:
                    g_szLastErrorDescr = "PWM IR Error";
                    break;
                case NS_CALIB_GAIN_ERR:
                    g_szLastErrorDescr = "Calib GAIN Error";
                    break;
                case NS_CALIB_COEFF_ERR:
                    g_szLastErrorDescr = "Calib COEFF Error";
                    break;
                //    '//
                case NS_NO_SERIAL_NUMBER:
                    g_szLastErrorDescr = "Falta el número de serie";
                    break;
                case NS_NO_MAGNETIC_CALIB:
                    g_szLastErrorDescr = "Error en el archivo de calibración (Magnetic)";
                    break;
                case NS_MAGNETIC_HEAD_KO:
                    g_szLastErrorDescr = "Cabeza magnética desconectada";
                    break;
                //case NS_UV_SENSOR_KO:  g_szLastErrorDescr = "UV sensor KO or disconnected"
                case NS_INVALID_NUMDOC:
                    g_szLastErrorDescr = "Número de documento requerido inválido";
                    break;
                //case NS_NUMOFMAGNCHANNEL_NOSET:     g_szLastErrorDescr = "Number of magnetic channels not set"
                case NS_ROTATE_ERROR:
                    g_szLastErrorDescr = "Error al rotar la imagen";
                    break;
                case NS_INVALID_RESET_TYPE:
                    g_szLastErrorDescr = "Parametro de reinicio inválido";
                    break;
                case NS_INVALID_CALLBACK_SET:
                    g_szLastErrorDescr = "Función de validación inválida";
                    break;
                //'// case NS_HOPPER_COUNTER_ERR:         g_szLastErrorDescr = "Hopper Counters Error: empty drawers and reset!"
                //case NS_CLEAR_AND_VALIDATE_ERR:     g_szLastErrorDescr = "Clear+Validate are NOT Compatible"
                case NS_DECODE_INVALID_BITMAP:
                    g_szLastErrorDescr = "Decodificación inválida BMP";
                    break;
                //    '//
                case NS_SUSPECT_BOX_FULL:
                    g_szLastErrorDescr = "Caja de billetes falsos llena: ¡Vaciela y reinicie!";
                    break;
                case NS_REJECT_BOX_FULL:
                    g_szLastErrorDescr = "Caja de rechazos llena: ¡Vaciela y reinicie!";
                    break;
                case NS_REJECT_BOX_NOTEMPTY:
                    g_szLastErrorDescr = "La caja de rechazos no esta vacía: ¡Vaciela y reinicie!";
                    break;
                case NS_ESCROW_BOX_NOTEMPTY:
                    g_szLastErrorDescr = "El escrow no esta vacio: ¡Vacielo y reinicie!";
                    break;
                case NS_ESCROW_BOX_FULL:
                    g_szLastErrorDescr = "El escrow esta lleno: ¡Vacielo y reinicie!";
                    break;
                //'// case NS_PATH_NOT_FREE: g_szLastErrorDescr = "Path NOT FREE: clean the path and reset!"
                //    '// RS232 COMMUNICATION
                case RS232_SEND_ERR:
                    g_szLastErrorDescr = "RS232 Error de escritura (TX KO)";
                    break;
                case RS232_RECEIVE_ERR:
                    g_szLastErrorDescr = "RS232 Timeout (RX KO)";
                    break;
                case RS232_COMM_ERR:
                    g_szLastErrorDescr = "RS232 Error de comunicación";
                    break;
                case RS232_OPEN_ERR:
                    g_szLastErrorDescr = "RS232 Error al abrir";
                    break;
                case RS232_CLOSE_ERR:
                    g_szLastErrorDescr = "RS232 Error al cerrar";
                    break;
                case RS232_GENERIC_ERR:
                    g_szLastErrorDescr = "RS232 Error genérico";
                    break;
                case RS232_NACK_ERR:
                    g_szLastErrorDescr = "RS232 NACK Error";
                    break;
                //'// case RS232_DMA_TIMEOUT_ERR:         g_szLastErrorDescr = "RS232 DMA Timeout / Image too Big"
                //    '// MOTORS retcodes
                case MOT_FEEDER_EMPTY:
                    g_szLastErrorDescr = "OK - (Feeder Vacio)";
                    break;
                case MOT_REJECT_TIMEOUT:
                    g_szLastErrorDescr = "Reject Timeout";
                    break;
                case MOT_BOURRAGE_PHOTO_START:
                    g_szLastErrorDescr = "Bourrage Photo Start: reset device!";
                    break;
                //case MOT_BOURRAGE_PHOTO_REJ:        g_szLastErrorDescr = "Bourrage Photo Reject: reset device!"
                case MOT_PHOTO_CALIB_ERROR:
                    g_szLastErrorDescr = "Photo Calib Error";
                    break;
                case MOT_DMA_COMMAND_ERR:
                    g_szLastErrorDescr = "DMA Command Error";
                    break;
                case MOT_SKEW_ERROR:
                    g_szLastErrorDescr = "Skew Error";
                    break;
                case MOT_TX_CAN_ERROR:
                    g_szLastErrorDescr = "TX CAN Error";
                    break;
                case MOT_RX_CAN_TRANSP_ERROR:
                    g_szLastErrorDescr = "RX CAN Transport Error";
                    break;
                case MOT_RX_CAN_DRAWER_ERROR:
                    g_szLastErrorDescr = "RX CAN Drawer Error";
                    break;
                case MOT_FRONTDOOR_OPEN_WRN:
                    g_szLastErrorDescr = "Front door is open";
                    break;
                case MOT_REARDOOR_OPEN_WRN:
                    g_szLastErrorDescr = "Rear door is open";
                    break;
                case MOT_NO_ESCROWBOX_WRN:
                    g_szLastErrorDescr = "Escrow box not present";
                    break;
                case MOT_PHOTO_REJ_SX_WRN:
                    g_szLastErrorDescr = "Reject photo sx failure";
                    break;
                case MOT_PHOTO_REJ_DX_WRN:
                    g_szLastErrorDescr = "Reject photo dx failure";
                    break;
                case MOT_PHOTO_START_SX_WRN:
                    g_szLastErrorDescr = "Start photo sx failure";
                    break;
                case MOT_PHOTO_START_DX_WRN:
                    g_szLastErrorDescr = "Start photo dx failure";
                    break;
                case MOT_FEED_ANOMALY:
                    g_szLastErrorDescr = "Feeding Anomaly";
                    break;
                //case MOT_SYNC_ERROR:   g_szLastErrorDescr = "Sync Error"
                //case MOT_PRESSURE_PLATE_ERROR:      g_szLastErrorDescr = "Doc Compressor Plate Error"
                case MOT_DISPENSE_ERROR:
                    g_szLastErrorDescr = "Dispense Error";
                    break;
                case MOT_DEPOSIT_ERROR:
                    g_szLastErrorDescr = "Deposit Error";
                    break;
                case MOT_DEPOSIT_ANOMALY:
                    g_szLastErrorDescr = "Deposit Anomaly";
                    break;
                case MOT_DEPOSIT_NO_DOC:
                    g_szLastErrorDescr = "Deposit No Doc";
                    break;
                //case MOT_FEEDER_BLOCKED_ERR:        g_szLastErrorDescr = "Feeder Blocked"
                case MOT_ESCROW_ERROR:
                    g_szLastErrorDescr = "Escrow Error";
                    break;
                //case MOT_TRANSPORT_BLOCKED_ERR:     g_szLastErrorDescr = "Trasport Blocked"
                case MOT_STACKER_ERR:
                    g_szLastErrorDescr = "Stacker Blocked";
                    break;
                //case MOT_TRANSPORT_SPEED_ERR:       g_szLastErrorDescr = "Transport Speed Error"
                //    '//
                //    '// DRAWERS
                //    '//
                //'// trasporto cassetti
                case CASS_CAN_TX:
                    g_szLastErrorDescr = "TX CAN error";
                    break;
                case CASS_CAN_RX:
                    g_szLastErrorDescr = "RX CAN Error; Module FI2D_TRAS";
                    break;
                case CASS_PHOTO_CALIB_ERR:
                    g_szLastErrorDescr = "Photo calib error";
                    break;
                case CASS_NOT_PRESENT:
                    g_szLastErrorDescr = "Drawer not present";
                    break;
                case CASS_JAM_SYNC:
                    g_szLastErrorDescr = "Jam input sync";
                    break;
                case CASS_JAM_SKEW1:
                    g_szLastErrorDescr = "Jam photo skew 1";
                    break;
                case CASS_JAM_SKEW2:
                    g_szLastErrorDescr = "Jam photo skew 2";
                    break;
                case CASS_JAM_IN_1:
                    g_szLastErrorDescr = "Jam photo drawer 1";
                    break;
                case CASS_JAM_IN_2:
                    g_szLastErrorDescr = "Jam photo drawer 2";
                    break;
                case CASS_JAM_IN_3:
                    g_szLastErrorDescr = "Jam photo drawer 3";
                    break;
                case CASS_JAM_IN_4:
                    g_szLastErrorDescr = "Jam photo drawer 4";
                    break;
                case CASS_MICRO_SAFE_OPEN:
                    g_szLastErrorDescr = "Micro safe open";
                    break;
                case CASS_DEPOSIT_ERROR:
                    g_szLastErrorDescr = "Deposit error";
                    break;
                case CASS_EEPROM_ERROR:
                    g_szLastErrorDescr = "Eeprom error";
                    break;
                case CASS_MOT_TRANSPORT_ERR:
                    g_szLastErrorDescr = "Transport Error";
                    break;
                //'// cassetti
                case CASS_MOTOR1_ERR:
                    g_szLastErrorDescr = "Drawer 1 motor error";
                    break;
                case CASS_MOTOR2_ERR:
                    g_szLastErrorDescr = "Drawer 2 motor error";
                    break;
                case CASS_MOTOR3_ERR:
                    g_szLastErrorDescr = "Drawer 3 motor error";
                    break;
                case CASS_MOTOR4_ERR:
                    g_szLastErrorDescr = "Drawer 4 motor error";
                    break;
                case CASS_1_NO:
                    g_szLastErrorDescr = "Drawer 1 unplaced";
                    break;
                case CASS_2_NO:
                    g_szLastErrorDescr = "Drawer 2 unplaced";
                    break;
                case CASS_3_NO:
                    g_szLastErrorDescr = "Drawer 3 unplaced";
                    break;
                case CASS_4_NO:
                    g_szLastErrorDescr = "Drawer 4 unplaced";
                    break;
                case CASS_PHOTO1_ERR:
                    g_szLastErrorDescr = "Drawer 1 photo error";
                    break;
                case CASS_PHOTO2_ERR:
                    g_szLastErrorDescr = "Drawer 2 photo error";
                    break;
                case CASS_PHOTO3_ERR:
                    g_szLastErrorDescr = "Drawer 3 photo error";
                    break;
                case CASS_PHOTO4_ERR:
                    g_szLastErrorDescr = "Drawer 4 photo error";
                    break;
                case CASS_1_DOWN:
                    g_szLastErrorDescr = "Drawer 1 FULL";
                    break;
                case CASS_2_DOWN:
                    g_szLastErrorDescr = "Drawer 2 FULL";
                    break;
                case CASS_3_DOWN:
                    g_szLastErrorDescr = "Drawer 3 FULL";
                    break;
                case CASS_4_DOWN:
                    g_szLastErrorDescr = "Drawer 4 FULL";
                    break;
                case CASS_1_UP:
                    g_szLastErrorDescr = "Drawer 1 up";
                    break;
                case CASS_2_UP:
                    g_szLastErrorDescr = "Drawer 2 up";
                    break;
                case CASS_3_UP:
                    g_szLastErrorDescr = "Drawer 3 up";
                    break;
                case CASS_4_UP:
                    g_szLastErrorDescr = "Drawer 4 up";
                    break;
                case CASS_NO_PWM_C:
                    g_szLastErrorDescr = "Drawer pwm calib missing";
                    break;
                case CASS_NO_PWM_C1:
                    g_szLastErrorDescr = "Drawer 1 pwm calib error";
                    break;
                case CASS_NO_PWM_C2:
                    g_szLastErrorDescr = "Drawer 2 pwm calib error";
                    break;
                case CASS_NO_PWM_C3:
                    g_szLastErrorDescr = "Drawer 3 pwm calib error";
                    break;
                case CASS_NO_PWM_C4:
                    g_szLastErrorDescr = "Drawer 4 pwm calib error";
                    break;
                case CASS_PWM_CALIB_ERR:
                    g_szLastErrorDescr = "Drawer pwm calib error";
                    break;
                case CASS_NO_RESET:
                    g_szLastErrorDescr = "Drawer no reset";
                    break;
                case CASS_NO_RESET1:
                    g_szLastErrorDescr = "Drawer 1 no reset";
                    break;
                case CASS_NO_RESET2:
                    g_szLastErrorDescr = "Drawer 2 no reset";
                    break;
                case CASS_NO_RESET3:
                    g_szLastErrorDescr = "Drawer 3 no reset";
                    break;
                case CASS_NO_RESET4:
                    g_szLastErrorDescr = "Drawer 4 no reset";
                    break;
                case CASS_NO_ENAB1:
                    g_szLastErrorDescr = "Drawer 1 disabled";
                    break;
                case CASS_NO_ENAB2:
                    g_szLastErrorDescr = "Drawer 2 disabled";
                    break;
                case CASS_NO_ENAB3:
                    g_szLastErrorDescr = "Drawer 3 disabled";
                    break;
                case CASS_NO_ENAB4:
                    g_szLastErrorDescr = "Drawer 4 disabled";
                    break;
                case CASS_NO_SPACE:
                    g_szLastErrorDescr = "Drawer no space";
                    break;
                case CASS_1_BUSY:
                    g_szLastErrorDescr = "Drawer 1 busy";
                    break;
                case CASS_2_BUSY:
                    g_szLastErrorDescr = "Drawer 2 busy";
                    break;
                case CASS_3_BUSY:
                    g_szLastErrorDescr = "Drawer 3 busy";
                    break;
                case CASS_4_BUSY:
                    g_szLastErrorDescr = "Drawer 4 busy";
                    break;
                //    ' BAG - SACCO
                //case CPS_JAM_SU_FOTO_SYNC_INGRESSO_ERR: g_szLastErrorDescr = "JAM_PHOTO_SYNC_IN_ERR"
                //case CPS_JAM_SU_FOTO_ING1_SACCO_ERR:    g_szLastErrorDescr = "JAM_PHOTO_IN1_BAG_ERR"
                //case CPS_JAM_SU_FOTO_ING2_SACCO_ERR:    g_szLastErrorDescr = "JAM_PHOTO_IN2_BAG_ERR"
                //case CPS_JAM_SU_FOTO_ING3_SACCO_ERR:    g_szLastErrorDescr = "JAM_PHOTO_IN3_BAG_ERR"
                //case CPS_JAM_SU_FOTO_ING4_SACCO_ERR:    g_szLastErrorDescr = "JAM_PHOTO_IN4_BAG_ERR"
                //case CPS_JAM_SU_FOTO_ING0_SACCO_ERR:    g_szLastErrorDescr = "JAM_PHOTO_IN0_BAG_ERR"
                //case CPS_GRUPPO_SACCO_NON_CHIUSO:       g_szLastErrorDescr = "GROUP_BAG_NOT_CLOSED"
                //case CPS_MOTORE_TRASPORTO_ERR:          g_szLastErrorDescr = "TRANSPORT_MOTOR_ERR"
                //case CPS_SACCO_NON_INSERITO:            g_szLastErrorDescr = "BAG_NOT_PRESENT"
                //case CPS_SACCO_NON_APERTO: g_szLastErrorDescr = "BAG_NOT_OPEN"
                //    '
                case OPTIC_RET_FPGA_ERR:
                    g_szLastErrorDescr = "FPGA Loading Error";
                    break;
                case OPTIC_RET_AD_ERR:
                    g_szLastErrorDescr = "A/D Converter Error";
                    break;
                case OPTIC_RET_SRAM_ERR:
                    g_szLastErrorDescr = "SRAM Error";
                    break;
                case THICKNESS_RET_TIMEOUT:
                    g_szLastErrorDescr = "THICKNESS Board Timeout";
                    break;
                case UV_MAGNETIC_RET_TIMEOUT:
                    g_szLastErrorDescr = "UV/MAGNETIC Board Timeout";
                    break;
                case TICKNESS_TARATURA_ERR:
                    g_szLastErrorDescr = "THICKNESS Calibration Error";
                    break;
                //    '// LAN COMMUNICATION
                case LAN_RET_OPEN_ERR:
                    g_szLastErrorDescr = "LAN Open Error";
                    break;
                case LAN_RET_CLOSE_ERR:
                    g_szLastErrorDescr = "LAN Close Error";
                    break;
                case LAN_RET_WRITE_ERR:
                    g_szLastErrorDescr = "LAN Write Error";
                    break;
                case LAN_RET_READ_ERR:
                    g_szLastErrorDescr = "LAN Read Error";
                    break;
                case LAN_RET_READ_NODATA:
                    g_szLastErrorDescr = "LAN Read No Data";
                    break;
                //    '// USB COMMUNICATION
                case USBLOW_RET_NODEVICE:
                    g_szLastErrorDescr = "USB device not found";
                    break;
                case USBLOW_RET_NOOPEN:
                    g_szLastErrorDescr = "USB device not connected";
                    break;
                case USBLOW_RET_FILEOPEN_ERR:
                    g_szLastErrorDescr = "USB file open error";
                    break;
                case USBLOW_RET_FILEREAD_ERR:
                    g_szLastErrorDescr = "USB file read error";
                    break;
                case USBLOW_RET_FILEWRITE_ERR:
                    g_szLastErrorDescr = "USB file write error";
                    break;
                case USBLOW_RET_COMMAND_FAILED:
                    g_szLastErrorDescr = "USB command failed";
                    break;
                case USBLOW_RET_FILE_HEX_ERR:
                    g_szLastErrorDescr = "USB hex file format error";
                    break;
                case USBLOW_RET_PARAM_ERR:
                    g_szLastErrorDescr = "USB parameter error";
                    break;
                /*case USBLOW_RET_CMD_ERR:
                    g_szLastErrorDescr = "USB command error";
                    break;*/
                case USBLOW_RET_RX_TIMEOUT:
                    g_szLastErrorDescr = "USB rx timeout";
                    break;
                //case USBLOW_RET_EIO:   g_szLastErrorDescr = "USB I/O Error"
                //    '
                default:
                    g_szLastErrorDescr = "Error desconocido";
                    break;
                //    End Select

                //    g_szLastErrorDescr = g_szLastErrorDescr & vbCrLf & "Code: " & CStr(lERR) & "  (0x" & Hex(lERR) & ")"
            }
            g_szLastErrorDescr += "Codigo de Error: " + lERR  + sLastCommand;
            //End Sub
        }

        //Public Sub exec_Controllo_Cassaforte()

    }
}
