﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidApi;
//using CTS;

namespace __DemoFID_B
{
    public partial class FormDepositoDolares : Form
    {
        int lRet;
        String l_Error;
        bool l_Deposito_ESCROW;
        bool l_scanimages = false;
        int l_TotalAceptados = 0;
        int l_TotalRechazados = 0;
        int l_Total1 = 0;
        int l_Total2 = 0;
        int l_Total5 = 0;
        int l_Total10 = 0;
        int l_Total20 = 0;
        int l_Total50 = 0;
        int l_Total100 = 0;
        Double l_TotalEfectivo;
        String l_Equipo = "Equipo1";

        Byte[] key = new Byte[1];
        Byte[] sensor = new Byte[64];
        ushort[] l_totalNotes = new ushort[10];

        public FormDepositoDolares()
        {
            InitializeComponent();
        }

        private void EscribirResultados()
        {
            Double l_Efectivo1;
            Double l_Efectivo2;
            Double l_Efectivo5;
            Double l_Efectivo10;
            Double l_Efectivo20;
            Double l_Efectivo50;
            Double l_Efectivo100;

            c_BilletesAceptados.Text = l_TotalAceptados.ToString();
            if (l_TotalRechazados > 1)
                c_BilletesRechazados.Text = "No";
            c_Cantidad1.Text = l_Total1.ToString();
            c_Cantidad2.Text = l_Total2.ToString();
            c_Cantidad5.Text = l_Total5.ToString();
            c_Cantidad10.Text = l_Total10.ToString();
            c_Cantidad20.Text = l_Total20.ToString();
            c_Cantidad50.Text = l_Total50.ToString();
            c_Cantidad100.Text = l_Total100.ToString();
            l_Efectivo100 = l_Total100 * 100;
            l_Efectivo10 = l_Total10 * 10;
            l_Efectivo1 = l_Total1;
            l_Efectivo20 = l_Total20 * 20;
            l_Efectivo2 = l_Total2 * 2;
            l_Efectivo50 = l_Total50 * 50;
            l_Efectivo5 = l_Total5 * 5;
            c_Suma5.Text = l_Efectivo5.ToString("$#,###,##0.00");
            c_Suma50.Text = l_Efectivo50.ToString("$#,###,##0.00");
            c_Suma1.Text = l_Efectivo1.ToString("$#,###,##0.00");
            c_Suma10.Text = l_Efectivo10.ToString("$#,###,##0.00");
            c_Suma100.Text = l_Efectivo100.ToString("$#,###,##0.00");
            c_Suma2.Text = l_Efectivo2.ToString("$#,###,##0.00");
            c_Suma20.Text = l_Efectivo20.ToString("$#,###,##0.00");
            l_TotalEfectivo = l_Efectivo50 + l_Efectivo5 + l_Efectivo20
                + l_Efectivo2 + l_Efectivo10 + l_Efectivo100 + l_Efectivo1;
            c_TotalDeposito.Text = l_TotalEfectivo.ToString("$#,###,##0.00");
        }

        private void c_Depositar_Click(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (Globales.DEBUG)
            {
                try
                {
                    lRet = SidLib.SID_Open(l_scanimages );
                    lRet =  SidLib.SID_Initialize();
                    if (lRet != 0)
                    {
                        SidLib.Error_Sid(lRet, out l_Error);
                        Globales.EscribirBitacora("Deposito", "NS_DocSessionStart", l_Error,1);
                       
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = l_Error  ;
                            f_Error.ShowDialog();
                            c_BotonCancelar_Click(this, null);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    try
                    {
                       /* FidLib.AUTODOCHANDLE st_aut = new FidLib.AUTODOCHANDLE();

                        st_aut.Validate = FidLib.VALIDATE.VALIDATE_BANKNOTE;
                        st_aut.Side = FidLib.SIDE.SIDE_ALL_IMAGE;
                        st_aut.ScanSpeed = FidLib.SCANSPEED.SCAN_MODE_SPEED2_BOTH; // (default for banknotes)
                        st_aut.ClearBlack = FidLib.CLEARBLACK.NO_CLEAR_BLACK;
                        st_aut.SaveImage = FidLib.SAVEIMAGE.IMAGE_SAVE_NONE;
                        st_aut.DocType = FidLib.DOCTYPE.DOCTYPE_SCAN_BANKNOTE;
                        st_aut.NumDocument = 999;
                        st_aut.UVMode = FidLib.UVMODE.BIT_UV_YES;
                        st_aut.MagneticMode = FidLib.MAGNETIC_MODE.BIT_MAGNETIC_YES;
                        st_aut.ThicknessMode = FidLib.THICKNESS_MODE.THICKNESS_MODE_NONE;
                        st_aut.CodelineType = FidLib.CODELINE_TYPE.NO_READ_CODELINE;

                        lRet = FidLib.AutoDocHandle(st_aut);

                        /* lRet = FidLib.AutoDocHandle(FidLib.VALIDATE.VALIDATE_BANKNOTE, FidLib.SIDE.SIDE_ALL_IMAGE,
                            FidLib.SCANSPEED.SCAN_MODE_SPEED2_BOTH, FidLib.CLEARBLACK.NO_CLEAR_BLACK, 999,
                                FidLib.SAVEIMAGE.IMAGE_SAVE_NONE, "\\images", "Image_", FidLib.FILEFORMAT.SAVE_BMP,
                                    FidLib.DOCTYPE.DOCTYPE_SCAN_BANKNOTE, FidLib.SCANEXTRAMODE.SCAN_EXTRAMODE_UV,
                                        FidLib.SCANMAGNETICMODE.BIT_SCAN_MAGNETIC_YES);*/

                        if (lRet != 0)
                        {
                         
                            SidLib.Error_Sid(lRet, out l_Error);
                            Globales.EscribirBitacora("Deposito", "NS_AutoDocHandle", l_Error,1);
                            using (FormError f_Error = new FormError(false))
                            {
                                f_Error.c_MensajeError.Text = l_Error;
                                f_Error.ShowDialog();
                                c_BotonCancelar_Click(this, null);
                            }
                        }
                        else
                        {
                            /*  do
                              {
                                  FidLib.GETDOCDATA st_DocData = new FidLib.GETDOCDATA();
                                 // st_DocData.CodelineSide = FidLib.CODELINETYPE.NO_READ_CODELINE;
                                  lRet = FidLib.GetDocData(ref st_DocData);
                                  switch (st_DocData.DocInfo.ToString("X"))
                                  {
                          case "20":
                              l_TotalRechazados++;
                              break;
                          case "31":
                              l_TotalAceptados++;
                              l_Total1++;
                              break;
                          case "32":
                              l_TotalAceptados++;
                              l_Total2++;
                              break;
                          case "33":
                              l_TotalAceptados++;
                              l_Total5++;
                              break;
                          case "34":
                              l_TotalAceptados++;
                              l_Total10++;
                              break;
                          case "35":
                              l_TotalAceptados++;
                              l_Total20++;
                              break;
                          case "36":
                              l_TotalAceptados++;
                              l_Total50++;
                              break;
                          case "37":
                              l_TotalAceptados++;
                              l_Total100++;
                              break;
                          default:
                              l_TotalRechazados++;
                              break;
                      }
                              }
                              while (lRet == 0);
                          }
                          lRet = FidLib.DocSessionEnd();*/

                            EscribirResultados();
                        }
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }
                catch (Exception ex)
                {
                }
            }
            else
                Llenar_Aleatorio();
            EscribirResultados();
            Cursor.Current = Cursors.Default;
            Cursor.Hide();
            Cursor.Hide();
            if (l_TotalRechazados > 1)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "Porfavor retire los billetes rechazados";
                    f_Error.ShowDialog();
                    l_TotalRechazados = 0;
                }
            }
        }

        private void Llenar_Aleatorio()
        {
            Random rd = new Random();
            int l_ValorR;
            for (int i = 0; i < 50; i++)
            {
                l_ValorR = rd.Next(1, 101);
                if (l_ValorR == 2)
                {
                    l_TotalAceptados++;
                    l_Total2++;
                }
                else if (l_ValorR == 5)
                {
                    l_TotalAceptados++;
                    l_Total5++;
                }
                else if (l_ValorR == 10)
                {
                    l_TotalAceptados++;
                    l_Total10++;
                }
                else if (l_ValorR == 20)
                {
                    l_TotalAceptados++;
                    l_Total20++;
                }
                else if (l_ValorR == 50)
                {
                    l_Total50++;
                    l_TotalAceptados++;
                }
                else if (l_ValorR == 100)
                {
                    l_TotalAceptados++;
                    l_Total100++;
                }
                else if (l_ValorR == 1)
                {
                    l_TotalAceptados++;
                    l_Total1++;
                }
                else
                    l_TotalRechazados++;
            }
        }

        private void c_BotonAceptar_Click(object sender, EventArgs e)
        {
            DataTable l_Detalle = BDDeposito.ObtenerDetalleDeposito(0);
            if (l_Total100 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "100");
                l_Nueva[1] = l_Total100;
                l_Nueva[2] = "100";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total10 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "10");
                l_Nueva[1] = l_Total10;
                l_Nueva[2] = "10";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total1 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "1");
                l_Nueva[1] = l_Total1;
                l_Nueva[2] = "1";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total20 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "20");
                l_Nueva[1] = l_Total20;
                l_Nueva[2] = "20";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total2 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "2");
                l_Nueva[1] = l_Total2;
                l_Nueva[2] = "2";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total50 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "50");
                l_Nueva[1] = l_Total50;
                l_Nueva[2] = "50";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (l_Total5 > 0)
            {
                DataRow l_Nueva = l_Detalle.NewRow();
                l_Nueva[0] = BDDenominaciones.ObtenerIdDenominacion(9, "5");
                l_Nueva[1] = l_Total5;
                l_Nueva[2] = "5";
                l_Detalle.Rows.Add(l_Nueva);
            }
            if (!UtilsComunicacion.Enviar_Transaccion(Globales.EmpresaCliente, 1, 9, 1, l_TotalEfectivo,
                l_Detalle, 0, true, Globales.IdUsuario))
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "No se pudo insertar el depósito, recoja el dinero del ESCROW";
                    f_Error.ShowDialog();
                    // Liberar ESCROW
                    Close();
                }
            }
            else
            {
                Santander.Escribir_Transaccion(Santander.CrearTransaccion(Santander.CrearDesglose(l_Detalle), 1,Globales.EmpresaCliente,Globales.IdUsuario,l_TotalEfectivo));
            }
        }

        private void c_BotonCancelar_Click(object sender, EventArgs e)
        {
            if (Globales.DEBUG)
            {
               
                if (l_TotalAceptados > 0)
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Retire el dinero del Escrow";
                        f_Error.ShowDialog();
                    }
                }
            }
            //Globales.EscribirBitacora("Depósito USD", "FI2DSERV_VandalDoor Abrir", "Estatus = " + lRet.ToString());
            Close();
        }

        private void FormDepositoDolares_Load(object sender, EventArgs e)
        {
            if (Globales.DEBUG)
            {
                //Globales.EscribirBitacora("Depósito USD", "NS_GetStatus", "Estatus = " + lRet.ToString());
                //Asegurar la puerta del ESCROW
                try
                {
                   // lRet = FidLib.CerrarVandalDoor();
                }
                catch (Exception ex)
                {
                    //Globales.EscribirBitacora("Depósito MX", "FI2DSERV_VandalDoor Cerrar", "Error = " + ex.ToString());
                }
                //Globales.EscribirBitacora("Depósito MX", "FI2DSERV_VandalDoor Cerrar", "Estatus = " + lRet.ToString());
                if (lRet != 0)
                {
                    do
                    {
                        using (FormError f_Error = new FormError(false))
                        {
                            f_Error.c_MensajeError.Text = "Asegurese de que la puerta del escrow este cerrada";
                            f_Error.ShowDialog();
                        }
                        try
                        {
                           // lRet = FidLib.CerrarVandalDoor();
                        }
                        catch (Exception ex)
                        {
                            //Globales.EscribirBitacora("Depósito MX", "FI2DSERV_VandalDoor Cerrar", "Error = " + ex.ToString());
                        }
                    } while (lRet != 0);
                }
            }
            c_EtiquetaNumeroCuenta.Text = Globales.AliasCuenta;

            using (FormError f_Mensaje = new FormError(false))
            {
                f_Mensaje.c_Imagen.Visible = false;
                f_Mensaje.c_MensajeError.Text =
                    "Coloque los billetes en el alimentador y oprima depositar, si su depósito es muy grande repita la operación.";
                f_Mensaje.ShowDialog();
            }
        }
    }
}
