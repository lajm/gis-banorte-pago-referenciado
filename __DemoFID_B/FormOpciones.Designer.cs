﻿namespace __DemoFID_B
{
    partial class FormOpciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOpciones));
            this.panel1 = new System.Windows.Forms.Panel();
            this.OPERACIONES = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.c_importar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.c_contabilidad = new System.Windows.Forms.Button();
            this.c_administarusuarios = new System.Windows.Forms.Button();
            this.c_OpcionConsultaEfectivo = new System.Windows.Forms.Button();
            this.c_OpcionConsultaOperacion = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.c_numeroBolsa = new System.Windows.Forms.Button();
            this.c_Reset = new System.Windows.Forms.Button();
            this.c_BotonHerramientas = new System.Windows.Forms.Button();
            this.c_OpcionCreaLayout = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.c_reset_retiro = new System.Windows.Forms.Button();
            this.c_OpcionDeposito = new System.Windows.Forms.Button();
            this.c_OpcionRetiroETV = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.c_BotonSalir = new System.Windows.Forms.Button();
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.OPERACIONES.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.OPERACIONES);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.c_BotonSalir);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            // 
            // OPERACIONES
            // 
            this.OPERACIONES.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.OPERACIONES.Controls.Add(this.tabPage1);
            this.OPERACIONES.Controls.Add(this.tabPage2);
            this.OPERACIONES.Controls.Add(this.tabPage3);
            this.OPERACIONES.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OPERACIONES.Location = new System.Drawing.Point(74, 112);
            this.OPERACIONES.Name = "OPERACIONES";
            this.OPERACIONES.SelectedIndex = 0;
            this.OPERACIONES.ShowToolTips = true;
            this.OPERACIONES.Size = new System.Drawing.Size(653, 374);
            this.OPERACIONES.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Info;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.c_importar);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.c_contabilidad);
            this.tabPage1.Controls.Add(this.c_administarusuarios);
            this.tabPage1.Controls.Add(this.c_OpcionConsultaEfectivo);
            this.tabPage1.Controls.Add(this.c_OpcionConsultaOperacion);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(645, 338);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ADMINISTRACIÓN";
            this.tabPage1.ToolTipText = "Opciones administrativas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // c_importar
            // 
            this.c_importar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_importar.Location = new System.Drawing.Point(15, 260);
            this.c_importar.Name = "c_importar";
            this.c_importar.Size = new System.Drawing.Size(126, 57);
            this.c_importar.TabIndex = 19;
            this.c_importar.Text = "Importar Clientes";
            this.c_importar.UseVisualStyleBackColor = true;
            this.c_importar.Click += new System.EventHandler(this.c_importar_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(325, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 150);
            this.button1.TabIndex = 18;
            this.button1.Text = "Administrar Usuarios";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // c_contabilidad
            // 
            this.c_contabilidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_contabilidad.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_contabilidad.Location = new System.Drawing.Point(489, 94);
            this.c_contabilidad.Name = "c_contabilidad";
            this.c_contabilidad.Size = new System.Drawing.Size(150, 150);
            this.c_contabilidad.TabIndex = 17;
            this.c_contabilidad.Text = "Contabilidad 0";
            this.c_contabilidad.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_contabilidad.UseVisualStyleBackColor = true;
            this.c_contabilidad.Click += new System.EventHandler(this.c_contabilidad_Click);
            // 
            // c_administarusuarios
            // 
            this.c_administarusuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_administarusuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_administarusuarios.Location = new System.Drawing.Point(325, 167);
            this.c_administarusuarios.Name = "c_administarusuarios";
            this.c_administarusuarios.Size = new System.Drawing.Size(150, 150);
            this.c_administarusuarios.TabIndex = 16;
            this.c_administarusuarios.Text = "Administrar Clientes";
            this.c_administarusuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_administarusuarios.UseVisualStyleBackColor = true;
            this.c_administarusuarios.Click += new System.EventHandler(this.c_administarusuarios_Click);
            // 
            // c_OpcionConsultaEfectivo
            // 
            this.c_OpcionConsultaEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionConsultaEfectivo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionConsultaEfectivo.Location = new System.Drawing.Point(1, 94);
            this.c_OpcionConsultaEfectivo.Name = "c_OpcionConsultaEfectivo";
            this.c_OpcionConsultaEfectivo.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionConsultaEfectivo.TabIndex = 9;
            this.c_OpcionConsultaEfectivo.Text = "Consulta de Efectivo";
            this.c_OpcionConsultaEfectivo.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionConsultaEfectivo.UseVisualStyleBackColor = true;
            this.c_OpcionConsultaEfectivo.Click += new System.EventHandler(this.c_OpcionConsultaEfectivo_Click);
            // 
            // c_OpcionConsultaOperacion
            // 
            this.c_OpcionConsultaOperacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionConsultaOperacion.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionConsultaOperacion.Location = new System.Drawing.Point(160, 94);
            this.c_OpcionConsultaOperacion.Name = "c_OpcionConsultaOperacion";
            this.c_OpcionConsultaOperacion.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionConsultaOperacion.TabIndex = 10;
            this.c_OpcionConsultaOperacion.Text = "Consulta de Operaciones";
            this.c_OpcionConsultaOperacion.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionConsultaOperacion.UseVisualStyleBackColor = true;
            this.c_OpcionConsultaOperacion.Click += new System.EventHandler(this.c_OpcionConsultaOperacion_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.IndianRed;
            this.tabPage2.Controls.Add(this.c_numeroBolsa);
            this.tabPage2.Controls.Add(this.c_Reset);
            this.tabPage2.Controls.Add(this.c_BotonHerramientas);
            this.tabPage2.Controls.Add(this.c_OpcionCreaLayout);
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(645, 338);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "SISTEMA";
            this.tabPage2.ToolTipText = "Opciones de Sistema";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // c_numeroBolsa
            // 
            this.c_numeroBolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_numeroBolsa.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_numeroBolsa.Location = new System.Drawing.Point(171, 85);
            this.c_numeroBolsa.Name = "c_numeroBolsa";
            this.c_numeroBolsa.Size = new System.Drawing.Size(150, 150);
            this.c_numeroBolsa.TabIndex = 17;
            this.c_numeroBolsa.Text = "Ingresar # Bolsa manualmente";
            this.c_numeroBolsa.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_numeroBolsa.UseVisualStyleBackColor = true;
            this.c_numeroBolsa.Click += new System.EventHandler(this.c_numeroBolsa_Click);
            // 
            // c_Reset
            // 
            this.c_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Reset.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_Reset.Location = new System.Drawing.Point(13, 85);
            this.c_Reset.Name = "c_Reset";
            this.c_Reset.Size = new System.Drawing.Size(150, 150);
            this.c_Reset.TabIndex = 15;
            this.c_Reset.Text = "RESET AL SISTEMA";
            this.c_Reset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_Reset.UseVisualStyleBackColor = true;
            this.c_Reset.Click += new System.EventHandler(this.c_Reset_Click);
            // 
            // c_BotonHerramientas
            // 
            this.c_BotonHerramientas.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonHerramientas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonHerramientas.Location = new System.Drawing.Point(487, 85);
            this.c_BotonHerramientas.Name = "c_BotonHerramientas";
            this.c_BotonHerramientas.Size = new System.Drawing.Size(150, 150);
            this.c_BotonHerramientas.TabIndex = 13;
            this.c_BotonHerramientas.Text = "CERRAR SISTEMA";
            this.c_BotonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonHerramientas.UseVisualStyleBackColor = true;
            this.c_BotonHerramientas.Click += new System.EventHandler(this.c_BotonHerramientas_Click);
            // 
            // c_OpcionCreaLayout
            // 
            this.c_OpcionCreaLayout.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionCreaLayout.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionCreaLayout.Location = new System.Drawing.Point(329, 85);
            this.c_OpcionCreaLayout.Name = "c_OpcionCreaLayout";
            this.c_OpcionCreaLayout.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionCreaLayout.TabIndex = 11;
            this.c_OpcionCreaLayout.Text = "Enviar Bitacora Actual";
            this.c_OpcionCreaLayout.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionCreaLayout.UseVisualStyleBackColor = true;
            this.c_OpcionCreaLayout.Click += new System.EventHandler(this.c_OpcionConsultaRechazos_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.c_reset_retiro);
            this.tabPage3.Controls.Add(this.c_OpcionDeposito);
            this.tabPage3.Controls.Add(this.c_OpcionRetiroETV);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(645, 338);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "RETIRO ";
            this.tabPage3.ToolTipText = "Retiro Bolsa corte";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // c_reset_retiro
            // 
            this.c_reset_retiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_reset_retiro.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_reset_retiro.Location = new System.Drawing.Point(247, 94);
            this.c_reset_retiro.Name = "c_reset_retiro";
            this.c_reset_retiro.Size = new System.Drawing.Size(150, 150);
            this.c_reset_retiro.TabIndex = 16;
            this.c_reset_retiro.Text = "RESET AL SISTEMA";
            this.c_reset_retiro.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_reset_retiro.UseVisualStyleBackColor = true;
            this.c_reset_retiro.Click += new System.EventHandler(this.c_Reset_Click);
            // 
            // c_OpcionDeposito
            // 
            this.c_OpcionDeposito.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionDeposito.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionDeposito.Location = new System.Drawing.Point(466, 99);
            this.c_OpcionDeposito.Name = "c_OpcionDeposito";
            this.c_OpcionDeposito.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionDeposito.TabIndex = 9;
            this.c_OpcionDeposito.Text = "APERTURA FORZADA DE BOVEDA";
            this.c_OpcionDeposito.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionDeposito.UseVisualStyleBackColor = true;
            this.c_OpcionDeposito.Click += new System.EventHandler(this.c_OpcionDeposito_Click);
            // 
            // c_OpcionRetiroETV
            // 
            this.c_OpcionRetiroETV.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_OpcionRetiroETV.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_OpcionRetiroETV.Location = new System.Drawing.Point(30, 99);
            this.c_OpcionRetiroETV.Name = "c_OpcionRetiroETV";
            this.c_OpcionRetiroETV.Size = new System.Drawing.Size(150, 150);
            this.c_OpcionRetiroETV.TabIndex = 8;
            this.c_OpcionRetiroETV.Text = "RETIRO ETV ";
            this.c_OpcionRetiroETV.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_OpcionRetiroETV.UseVisualStyleBackColor = true;
            this.c_OpcionRetiroETV.Click += new System.EventHandler(this.c_OpcionRetiroETV_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(238, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 29);
            this.label1.TabIndex = 14;
            this.label1.Text = "SELECCIONE UNA OPCIÓN";
            // 
            // c_BotonSalir
            // 
            this.c_BotonSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.c_BotonSalir.Location = new System.Drawing.Point(658, 503);
            this.c_BotonSalir.Name = "c_BotonSalir";
            this.c_BotonSalir.Size = new System.Drawing.Size(100, 41);
            this.c_BotonSalir.TabIndex = 12;
            this.c_BotonSalir.Text = "Salir";
            this.c_BotonSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.c_BotonSalir.UseVisualStyleBackColor = true;
            this.c_BotonSalir.Click += new System.EventHandler(this.c_BotonSalir_Click);
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(165, 260);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 57);
            this.button2.TabIndex = 20;
            this.button2.Text = "Importar Clientes op2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormOpciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOpciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormOpciones";
            this.Load += new System.EventHandler(this.FormOpciones_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.OPERACIONES.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button c_BotonHerramientas;
        private System.Windows.Forms.Button c_BotonSalir;
        private System.Windows.Forms.Button c_OpcionCreaLayout;
        private System.Windows.Forms.Button c_OpcionRetiroETV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button c_Reset;
        private System.Windows.Forms.TabControl OPERACIONES;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button c_OpcionDeposito;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button c_administarusuarios;
        private System.Windows.Forms.Button c_OpcionConsultaEfectivo;
        private System.Windows.Forms.Button c_OpcionConsultaOperacion;
        private System.Windows.Forms.Button c_reset_retiro;
        private System.Windows.Forms.Button c_numeroBolsa;
        private System.Windows.Forms.Button c_contabilidad;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button c_importar;
        private System.Windows.Forms.Button button2;
    }
}