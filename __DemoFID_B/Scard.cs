﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Printing;

namespace __DemoFID_B
{
    public class Scard
    {
        //' *******************************************************
        //' Common SCard functions
        //' *******************************************************

        //'
        //' Establish a context to resource manager
        //' Parameters:
        //'       dwScope         = Scope (see Scopes)
        //'       pvReserved1     = Reserved for further use
        //'       pvReserved2     = Reserved for further use
        //'       g_hContext       = Pointer to Context
        //'
        //Public Declare Function SCardEstablishContext Lib "WINSCARD" _
        //    (ByVal dwScope As Long, _
        //     ByVal pvReserved1 As Long, _
        //     ByVal pvReserved2 As Long, _
        //     ByRef g_hContext As Long) As Long
        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardEstablishContext", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardEstablishContext(int dwScope, int pvReserved1, int pvReserved2, ref int g_hContext);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardReleaseContext", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardReleaseContext(int hContext);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardListReaders", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardListReaders(int hContext, Byte mszGroups, ref Byte mszReaders, ref int pcchReaders);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardConnect", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardConnect(int hContext, String szReader, int dwShareMode, int dwPreferredProtocols,
            ref int hCard, ref int dwActiveProtocol);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardDisconnect", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardDisconnect(int hCard, int dwDisposition);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardStatus(int hCard, ref Byte szReaderLen, ref int pcchReaders, ref int pdwState,
            ref int pdwProtocol, ref Byte pbAtr, ref int pcbAtrLen);

        [DllImportAttribute("WINSCARD.dll", EntryPoint = "SCardControl", CallingConvention = CallingConvention.StdCall)]
        public static extern int SCardControl(int hCard, int dwControlCode, ref Byte lpInBuffer, int nInBufferSize,
            ref Byte lpOutBuffer, int nOutBufferSize, ref int lpBytesReturned);

        public const int SCARD_SCOPE_USER = 0;         //'The context is a user context, and any
        //                                                ' database operations are performed within the
        //                                                ' domain of the user.
        public const int SCARD_SCOPE_TERMINAL = 1;     //'The context is that of the current terminal,
        //                                                ' and any database operations are performed
        //                                                ' within the domain of that terminal.  (The
        //                                                ' calling application must have appropriate
        //                                                ' access permissions for any database actions.)
        public const int SCARD_SCOPE_SYSTEM = 2;       //'The context is the system context, and any
        //                                                ' database operations are performed within the
        //                                                ' domain of the system.  (The calling
        //                                                ' application must have appropriate access
        //                                                ' permissions for any database actions.)

        //'
        //' Share Modes
        //'
        public const int SCARD_SHARE_EXCLUSIVE = 1;    //' This application is not willing to share this
        //                                                ' card with other applications.
        public const int SCARD_SHARE_SHARED = 2;       //' This application is willing to share this
        //                                                ' card with other applications.
        public const int SCARD_SHARE_DIRECT = 3;       //' This application demands direct control of
        //                                                ' the reader, so it is not available to other
        //                                                ' applications.

        //'
        //' Protocols
        //'
        public const int SCARD_PROTOCOL_UNDEFINED = 0x0;   //' There is no active protocol.
        public const int SCARD_PROTOCOL_T0 = 0x1;          //' T=0 is the active protocol.
        public const int SCARD_PROTOCOL_T1 = 0x2;          //' T=1 is the active protocol.
        public const int SCARD_PROTOCOL_RAW = 0x10000;     //' Raw is the active protocol.
        //'
        //' Dispositions (after disconnecting)
        //'
        public const int SCARD_LEAVE_CARD = 0;     //' Don't do anything special on close
        public const int SCARD_RESET_CARD = 1;     //' Reset the card on close
        public const int SCARD_UNPOWER_CARD = 2;   //' Power down the card on close
        public const int SCARD_EJECT_CARD = 3;     //' Eject the card on close
        //'
        //'  Reader states
        //'
        public const int SCARD_UNKNOWN = 0;    //' This value implies the driver is unaware of the current state of the reader.
        public const int SCARD_ABSENT = 1;     //' This value implies there is no card in the reader.
        public const int SCARD_PRESENT = 2;    //' This value implies there is a card is present in the reader, but that it has not been moved into position for use.
        public const int SCARD_SWALLOWED = 3;  //' This value implies there is a card in the reader in position for use.  The card is not powered.
        public const int SCARD_POWERED = 4;    //' This value implies there is power is being provided to the card, but the Reader Driver is unaware of the mode of the card.
        public const int SCARD_NEGOTIABLE = 5; //' This value implies the card has been reset and is awaiting PTS negotiation.
        public const int SCARD_SPECIFIC = 6;   //' This value implies the card has been reset and specific communication protocols have been established.
        //'
        //' Reader Action IOCTLs
        //' winioctl.h
        //'

        //' Define the method codes for how buffers are passed for I/O and FS controls
        public const int METHOD_BUFFERED = 0;
        public const int METHOD_IN_DIRECT = 1;
        public const int METHOD_OUT_DIRECT = 2;
        public const int METHOD_NEITHER = 3;

        public const int FILE_DEVICE_SMARTCARD = 0x31;

        public const int FILE_ANY_ACCESS = 0;
        public const int FILE_READ_ACCESS = (0x1);
        public const int FILE_WRITE_ACCESS = (0x2);

        //'
        //' SCardControl()
        //' winsmcrd.h
        //'
        //'Public Const SCARD_CTL_CODE(code) = CTL_CODE(FILE_DEVICE_SMARTCARD, (code), METHOD_BUFFERED, FILE_ANY_ACCESS)
        //'Public Const IOCTL_SMARTCARD_POWER = SCARD_CTL_CODE(1)
        //'Public Const IOCTL_SMARTCARD_GET_ATTRIBUTE = SCARD_CTL_CODE(2)
        //'Public Const IOCTL_SMARTCARD_SET_ATTRIBUTE = SCARD_CTL_CODE(3)
        //'Public Const IOCTL_SMARTCARD_CONFISCATE = SCARD_CTL_CODE(4)

        //' The IOCTL_SMARTCARD_TRANSMIT DeviceIoControl operation
        //' transmits data to and receives data from the currently inserted smart card.
        //'Public Const IOCTL_SMARTCARD_TRANSMIT = SCARD_CTL_CODE(5)  //' 0x310000 + 5*2^2
        public const int IOCTL_SMARTCARD_TRANSMIT = 0x310014;

        //'Public Const IOCTL_SMARTCARD_EJECT = SCARD_CTL_CODE(6)
        //'Public Const IOCTL_SMARTCARD_SWALLOW = SCARD_CTL_CODE(7)
        //'Public Const IOCTL_SMARTCARD_READ = SCARD_CTL_CODE(8) //' obsolete
        //'Public Const IOCTL_SMARTCARD_WRITE = SCARD_CTL_CODE(9)//' obsolete
        //'Public Const IOCTL_SMARTCARD_IS_PRESENT = SCARD_CTL_CODE(10)
        //'Public Const IOCTL_SMARTCARD_IS_ABSENT = SCARD_CTL_CODE(11)
        //'Public Const IOCTL_SMARTCARD_SET_PROTOCOL = SCARD_CTL_CODE(12)
        //'Public Const IOCTL_SMARTCARD_GET_STATE = SCARD_CTL_CODE(14)
        //'Public Const IOCTL_SMARTCARD_GET_LAST_ERROR = SCARD_CTL_CODE(15)
        //'Public Const IOCTL_SMARTCARD_GET_PERF_CNTR = SCARD_CTL_CODE(16)

        //'
        //' Smart Card Error Codes
        //' All for SCARD error codes of the resource manager , a OK error code exists.
        //'
        public const long OKERR_SCARD__E_CANCELLED = 0x80100002;                //' @cnst  The action was cancelled by an SCardCancel request
        public const long OKERR_SCARD__E_INVALID_HANDLE = 0x80100003;           //' @cnst  The supplied handle was invalid
        public const long OKERR_SCARD__E_INVALID_PARAMETER = 0x80100004;        //' @cnst  One or more of the supplied parameters could not be properly longerpreted
        public const long OKERR_SCARD__E_INVALID_TARGET = 0x80100005;           //' @cnst  Registry startup information is missing or invalid
        public const long OKERR_SCARD__E_NO_MEMORY = 0x80100006;                //' @cnst  Not enough memory available to complete this command
        public const long OKERR_SCARD__F_WAITED_TOO_LONG = 0x80100007;          //' @cnst  An longernal consistency timer has expired
        public const long OKERR_SCARD__E_INSUFFICIENT_BUFFER = 0x80100008;      //' @cnst  The data buffer to receive returned data is too small for the returned data
        public const long OKERR_SCARD__E_UNKNOWN_READER = 0x80100009;           //' @cnst  The specified reader name is not recognized
        public const long OKERR_SCARD__E_TIMEOUT = 0x8010000A;                  //' @cnst  The user-specified timeout value has expired
        public const long OKERR_SCARD__E_SHARING_VIOLATION = 0x8010000B;        //' @cnst  The smart card cannot be accessed because of other connections outstanding
        public const long OKERR_SCARD__E_NO_SMARTCARD = 0x8010000C;             //' @cnst  The operation requires a Smart Card, but no Smart Card is currently in the device
        public const long OKERR_SCARD__E_UNKNOWN_CARD = 0x8010000D;             //' @cnst  The specified smart card name is not recognized
        public const long OKERR_SCARD__E_CANT_DISPOSE = 0x8010000E;             //' @cnst  The system could not dispose of the media in the requested manner
        public const long OKERR_SCARD__E_PROTO_MISMATCH = 0x8010000F;           //' @cnst  The requested protocols are incompatible with the protocol currently in use with the smart card
        public const long OKERR_SCARD__E_NOT_READY = 0x80100010;                //' @cnst  The reader or smart card is not ready to accept commands
        public const long OKERR_SCARD__E_INVALID_VALUE = 0x80100011;            //' @cnst  One or more of the supplied parameters values could not be properly longerpreted
        public const long OKERR_SCARD__E_SYSTEM_CANCELLED = 0x80100012;         //' @cnst  The action was cancelled by the system, presumably to log off or shut down
        public const long OKERR_SCARD__F_COMM_ERROR = 0x80100013;               //' @cnst  An longernal communications error has been detected
        public const long OKERR_SCARD__F_UNKNOWN_ERROR = 0x80100014;            //' @cnst  An longernal error has been detected, but the source is unknown
        public const long OKERR_SCARD__E_INVALID_ATR = 0x80100015;              //' @cnst  An ATR obtained from the registry is not a valid ATR string
        public const long OKERR_SCARD__E_NOT_TRANSACTED = 0x80100016;           //' @cnst  An attempt was made to end a non-existent transaction
        public const long OKERR_SCARD__E_READER_UNAVAILABLE = 0x80100017;       //' @cnst  The specified reader is not currently available for use
        public const long OKERR_SCARD__P_SHUTDOWN = 0x80100018;                 //' @cnst  The operation has been aborted to allow the server application to exit
        public const long OKERR_SCARD__E_PCI_TOO_SMALL = 0x80100019;            //' @cnst  The PCI Receive buffer was too small
        public const long OKERR_SCARD__E_READER_UNSUPPORTED = 0x8010001A;       //' @cnst  The reader driver does not meet minimal requirements for support
        public const long OKERR_SCARD__E_DUPLICATE_READER = 0x8010001B;         //' @cnst  The reader driver did not produce a unique reader name
        public const long OKERR_SCARD__E_CARD_UNSUPPORTED = 0x8010001C;         //' @cnst  The smart card does not meet minimal requirements for support
        public const long OKERR_SCARD__E_NO_SERVICE = 0x8010001D;               //' @cnst  The Smart card resource manager is not running
        public const long OKERR_SCARD__E_SERVICE_STOPPED = 0x8010001E;          //' @cnst  The Smart card resource manager has shut down
        public const long OKERR_SCARD__E_UNEXPECTED = 0x8010001F;               //' @cnst  An unexpected card error has occurred
        public const long OKERR_SCARD__E_ICC_INSTALLATION = 0x80100020;         //' @cnst  No Primary Provider can be found for the smart card
        public const long OKERR_SCARD__E_ICC_CREATEORDER = 0x80100021;          //' @cnst  The requested order of object creation is not supported
        public const long OKERR_SCARD__E_UNSUPPORTED_FEATURE = 0x80100022;      //' @cnst  This smart card does not support the requested feature
        public const long OKERR_SCARD__E_DIR_NOT_FOUND = 0x80100023;            //' @cnst  The identified directory does not exist in the smart card
        public const long OKERR_SCARD__E_FILE_NOT_FOUND = 0x80100024;           //' @cnst  The identified file does not exist in the smart card
        public const long OKERR_SCARD__E_NO_DIR = 0x80100025;                   //' @cnst  The supplied path does not represent a smart card directory
        public const long OKERR_SCARD__E_NO_FILE = 0x80100026;                  //' @cnst  The supplied path does not represent a smart card file
        public const long OKERR_SCARD__E_NO_ACCESS = 0x80100027;                //' @cnst  Access is denied to this file
        public const long OKERR_SCARD__E_WRITE_TOO_MANY = 0x80100028;           //' @cnst  An attempt was made to write more data than would fit in the target object
        public const long OKERR_SCARD__E_BAD_SEEK = 0x80100029;                 //' @cnst  There was an error trying to set the smart card file object polonger
        public const long OKERR_SCARD__E_INVALID_CHV = 0x8010002A;              //' @cnst  The supplied PIN is incorrect
        public const long OKERR_SCARD__E_UNKNOWN_RES_MNG = 0x8010002B;          //' @cnst  An unrecognized error code was returned from a layered component
        public const long OKERR_SCARD__E_NO_SUCH_CERTIFICATE = 0x8010002C;      //' @cnst  The requested certificate does not exist
        public const long OKERR_SCARD__E_CERTIFICATE_UNAVAILABLE = 0x8010002D;  //' @cnst  The requested certificate could not be obtained
        public const long OKERR_SCARD__E_NO_READERS_AVAILABLE = 0x8010002E;     //' @cnst  Cannot find a smart card reader
        public const long OKERR_SCARD__E_COMM_DATA_LOST = 0x8010002F;           //' @cnst  A communications error with the smart card has been detected
        public const long OKERR_SCARD__W_UNSUPPORTED_CARD = 0x80100065;         //' @cnst  The reader cannot communicate with the smart card, due to ATR configuration conflicts
        public const long OKERR_SCARD__W_UNRESPONSIVE_CARD = 0x80100066;        //' @cnst  The smart card is not responding to a reset
        public const long OKERR_SCARD__W_UNPOWERED_CARD = 0x80100067;           //' @cnst  Power has been removed from the smart card, so that further communication is not possible
        public const long OKERR_SCARD__W_RESET_CARD = 0x80100068;               //' @cnst  The smart card has been reset, so any shared state information is invalid
        public const long OKERR_SCARD__W_REMOVED_CARD = 0x80100069;             //' @cnst  The smart card has been removed, so that further communication is not possible
        public const long OKERR_SCARD__W_SECURITY_VIOLATION = 0x8010006A;       //' @cnst  Access was denied because of a security violation
        public const long OKERR_SCARD__W_WRONG_CHV = 0x8010006B;                //' @cnst  The card cannot be accessed because the wrong PIN was presented
        public const long OKERR_SCARD__W_CHV_BLOCKED = 0x8010006C;              //' @cnst  The card cannot be accessed because the maximum number of PIN entry attempts has been reached
        public const long OKERR_SCARD__W_EOF = 0x8010006D;                      //' @cnst  The end of the smart card file has been reached
        public const long OKERR_SCARD__W_CANCELLED_BY_USER = 0x8010006E;        //' @cnst  The action was cancelled by the user

        public const long OKERR_PARM1 = 0x81000000;                 //' Error in parameter 1
        public const long OKERR_PARM2 = 0x81000001;                 //' Error in parameter 2
        public const long OKERR_PARM3 = 0x81000002;                 //' Error in parameter 3
        public const long OKERR_PARM4 = 0x81000003;                 //' Error in parameter 4
        public const long OKERR_PARM5 = 0x81000004;                 //' Error in parameter 5
        public const long OKERR_PARM6 = 0x81000005;                 //' Error in parameter 6
        public const long OKERR_PARM7 = 0x81000006;                 //' Error in parameter 7
        public const long OKERR_PARM8 = 0x81000007;                 //' Error in parameter 8
        public const long OKERR_PARM9 = 0x81000008;                 //' Error in parameter 9
        public const long OKERR_PARM10 = 0x81000009;                //' Error in parameter 10
        public const long OKERR_PARM11 = 0x8100000A;                //' Error in parameter 11
        public const long OKERR_PARM12 = 0x8100000B;                //' Error in parameter 12
        public const long OKERR_PARM13 = 0x8100000C;                //' Error in parameter 13
        public const long OKERR_PARM14 = 0x8100000D;                //' Error in parameter 14
        public const long OKERR_PARM15 = 0x8100000E;                //' Error in parameter 15
        public const long OKERR_PARM16 = 0x8100000F;                //' Error in parameter 16
        public const long OKERR_PARM17 = 0x81000010;                //' Error in parameter 17
        public const long OKERR_PARM18 = 0x81000011;                //' Error in parameter 18
        public const long OKERR_PARM19 = 0x81000012;                //' Error in parameter 19
        public const long OKERR_INSUFFICIENT_PRIV = 0x81100000;     //' You currently do not have the rights to execute the requested action. Usually a password has to be presented in advance.
        public const long OKERR_PW_WRONG = 0x81100001;              //' The presented password is wrong
        public const long OKERR_PW_LOCKED = 0x81100002;             //' The password has been presented several times wrong and is therefore locked. Usually use some administrator tool to unblock it.
        public const long OKERR_PW_TOO_SHORT = 0x81100003;          //' The lenght of the password was too short.
        public const long OKERR_PW_TOO_LONG = 0x81100004;           //' The length of the password was too long.
        public const long OKERR_PW_NOT_LOCKED = 0x81100005;         //' The password is not locked
        public const long OKERR_ITEM_NOT_FOUND = 0x81200000;        //' An item (e.g. a key of a specific name) could not be found
        public const long OKERR_ITEMS_LEFT = 0x81200001;            //' There are still items left, therefore e.g. the directory / structure etc. can't be deleted.
        public const long OKERR_INVALID_CFG_FILE = 0x81200002;      //' Invalid configuration file
        public const long OKERR_SECTION_NOT_FOUND = 0x81200003;     //' Section not found
        public const long OKERR_ENTRY_NOT_FOUND = 0x81200004;       //' Entry not found
        public const long OKERR_NO_MORE_SECTIONS = 0x81200005;      //' No more sections
        public const long OKERR_ITEM_ALREADY_EXISTS = 0x81200006;   //' The specified item alread exists.
        public const long OKERR_ITEM_EXPIRED = 0x81200007;          //' Some item (e.g. a certificate) has expired.
        public const long OKERR_UNEXPECTED_RET_VALUE = 0x81300000;  //' Unexpected return value
        public const long OKERR_COMMUNICATE = 0x81300001;           //' General communication error
        public const long OKERR_NOT_ENOUGH_MEMORY = 0x81300002;     //' Not enough memory
        public const long OKERR_BUFFER_OVERFLOW = 0x81300003;       //' Buffer overflow
        public const long OKERR_TIMEOUT = 0x81300004;               //' A timeout has occurred
        public const long OKERR_NOT_SUPPORTED = 0x81300005;         //' The requested functionality is not supported at this time / under this OS / in this situation etc.
        public const long OKERR_ILLEGAL_ARGUMENT = 0x81300006;      //' Illegal argument
        public const long OKERR_READ_FIO = 0x81300007;              //' File IO read error
        public const long OKERR_WRITE_FIO = 0x81300008;             //' File IO write error
        public const long OKERR_INVALID_HANDLE = 0x81300009;        //' Invalid handle
        public const long OKERR_GENERAL_FAILURE = 0x8130000A;       //' General failure. Use this error code in cases where no other errors match and it is not worth to define a new error code.
        public const long OKERR_FILE_NOT_FOUND = 0x8130000B;        //' File not found
        public const long OKERR_OPEN_FILE = 0x8130000C;             //' File opening failed
        public const long OKERR_SEM_USED = 0x8130000D;              //' The semaphore is currently use by an other process
        public const long OKERR_NOP = 0x81F00001;                   //' No operation done
        public const long OKERR_NOK = 0x81F00002;                   //' Function not executed
        public const long OKERR_FWBUG = 0x81F00003;                 //' Internal error detected
        public const long OKERR_INIT = 0x81F00004;                  //' Module not initialized
        public const long OKERR_FIO = 0x81F00005;                   //' File IO error detected
        public const long OKERR_ALLOC = 0x81F00006;                 //' Cannot allocate memory
        public const long OKERR_SESSION_ERR = 0x81F00007;           //' General error
        public const long OKERR_ACCESS_ERR = 0x81F00008;            //' Access not allowed
        public const long OKERR_OPEN_FAILURE = 0x81F00009;          //' An open command was not successful
        public const long OKERR_CARD_NOT_POWERED = 0x81F0000A;      //' Card is not powered
        public const long OKERR_ILLEGAL_CARDTYPE = 0x81F0000B;      //' Illegal cardtype
        public const long OKERR_CARD_NOT_INSERTED = 0x81F0000C;     //' Card not inserted
        public const long OKERR_NO_DRIVER = 0x81F0000D;             //' No device driver installed
        public const long OKERR_OUT_OF_SERVICE = 0x81F0000E;        //' The service is currently not available
        public const long OKERR_EOF_REACHED = 0x81F0000F;           //' End of file reached
        public const long OKERR_ON_BLACKLIST = 0x81F00010;          //' The ID is on a blacklist, the requested action is therefore not allowed.
        public const long OKERR_CONSISTENCY_CHECK = 0x81F00011;     //' Error during consistency check
        public const long OKERR_IDENTITY_MISMATCH = 0x81F00012;     //' The identity does not match a defined cross-check identity
        public const long OKERR_MULTIPLE_ERRORS = 0x81F00013;       //' Multiple errors have occurred. Use this if there is only the possibility to return one error code, but there happened different errors before (e.g. each thread returned a different error and the controlling thread may only report one).
        public const long OKERR_ILLEGAL_DRIVER = 0x81F00014;        //' Illegal driver
        public const long OKERR_ILLEGAL_FW_RELEASE = 0x81F00015;    //' The connected hardware whose firmware is not useable by this software
        public const long OKERR_NO_CARDREADER = 0x81F00016;         //' No cardreader attached
        public const long OKERR_IPC_FAULT = 0x81F00017;             //' General failure of longer process communication
        public const long OKERR_WAIT_AND_RETRY = 0x81F00018;        //' The service currently does not take calls. The task has to go back to the message loop and try again at a later time (Windows 3.1 only). The code may also be used, in every situation where a ‘  wait and retry ’  action is requested.

        //'
        //' winioctl.h
        //'
        //' Macro definition for defining IOCTL and FSCTL function control codes.
        //Public Function CTL_CODE(param_DeviceType As Long, param_Code As Long, param_Method As Long, param_Access As Long) As Long
        //    CTL_CODE = ((param_DeviceType * (2 ^ 16)) Or _
        //                (param_Access * (2 ^ 14)) Or _
        //                (param_Code * (2 ^ 2)) Or _
        //                (param_Method))
        //End Function
        /*public static long CTL_CODE(long param_DeviceType, long param_Code, long param_Method, long param_Access)
        {
            long l_Ret = {(param_DeviceType * (2 ^ 16)) ||
                (param_Access * (2 ^ 14)) ||
                (param_Code * (2 ^ 2)) ||
                (param_Method)};
        }*/
    }

    public class SCard2WBP
    {
        //' ********************************************
        //' OMNIKEY DLL Functions
        //' ********************************************
        //'
        //' Read Bytes to Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulBytesToRead   = Number of Bytes to Read
        //'       pbData          = Array of Bytes containing data
        //'       ulAddress       = Offset where read starts
        //'
        //Public Declare Function SCard2WBPReadData Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulBytesToRead As Long, _
        //     ByRef pbData As Byte, _
        //     ByVal ulAddress As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPReadData(long hCard, long ulBytesToRead, ref Byte pbData, long ulAddress);
        //'
        //' Read Protection Bits of first 32 Bytes
        //'       hCard           = Handle to current Card
        //'       ulDataLen       = Length of pbData (have to be 4)
        //'       pbData          = Returned Data
        //'
        //Public Declare Function SCard2WBPReadProtectionMemory Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulDataLen As Long, _
        //     ByRef pbData As Byte) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPReadProtectionMemory(long hCard, long ulDataLen, ref Byte pbData);
        //'
        //' Write Bytes to Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulDataLen       = Size of pbData
        //'       pbData          = Array of Bytes storing data
        //'       ulAddress       = Offset where write starts
        //'
        //Public Declare Function SCard2WBPWriteData Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulDataLen As Long, _
        //     ByRef pbData As Byte, _
        //     ByVal ulAddress As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPWriteData(long hCard, long ulDataLen, ref Byte pbData, long ulAddress);
        //'
        //' Compare bytes and protect them if they are equal
        //' Parameters:
        //'       hCard       = Handle to current Card
        //'       bData       = Bytes to compare with
        //'       ulAddress   = Starting to compare at this Address
        //'
        //Public Declare Function SCard2WBPCompareAndProtect Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByRef bData As Byte, _
        //     ByVal ulAddress As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPCompareAndProtect(long hCard, ref Byte pbData, long ulAddress);
        //'
        //' Presents a PIN
        //' Parameters:
        //'       hCard        = Handle to current Card
        //'       ulPINLen     = Length of PIN (have to be 3)
        //'       pbPIN        = Byte-array containing PIN
        //'
        //Public Declare Function SCard2WBPPresentPIN Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulPINLen As Long, _
        //     ByRef pbPIN As Byte) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPPresentPIN(long hCard, long ulPINLen, ref Byte pbPIN);
        //'
        //' Change PIN of current Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulOldPINLen     = Length of old PIN (have to be 3)
        //'       pbOldPIN        = Byte-array containing old PIN
        //'       ulNewPINLen     = Length of new PIN (have to be 3)
        //'       pbNewPIN        = Byte-array containing new PIN
        //'
        //Public Declare Function SCard2WBPChangePIN Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulOldPINLen As Long, _
        //     ByRef pbOldPIN As Byte, _
        //     ByVal ulNewPINLen As Long, _
        //     ByRef pbNewPIN As Byte) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPChangePIN(long hCard, long ulOldPINLen, ref Byte pbOldPIN, long ulNewPINLen, ref Byte pbNewPIN);
        //'
        //' Check if PIN is presented
        //' Parameters
        //'       hCard           = Handle to current Card
        //'       pfPinPresented  = TRUE  (1) if PIN is already presented
        //'                         FALSE (0) if PIN is not presented
        //'       ATTENTION:  In VB TRUE is -1 in C/C++ TRUE is 1
        //'                   FALSE is 0 in VB and C/C++
        //'
        //Public Declare Function SCard2WBPIsPinPresented Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByRef pfPinPresented As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard2WBPIsPinPresented(long hCard, ref long pfPinPresented);
    }

    public class SCard3WBP
    {
        //' ********************************************
        //' OMNIKEY DLL Functions
        //' ********************************************

        //'
        //' Read Bytes to Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulBytesToRead   = Number of Bytes to Read
        //'       pbData          = Array of Bytes containing data
        //'       ulAddress       = Offset where read starts
        //'
        //Public Declare Function SCard3WBPReadData Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulBytesToRead As Long, _
        //     ByRef bData As Byte, _
        //     ByVal ulAddress As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPReadData(long hCard, long ulBytesToRead, ref Byte bData, long ulAddress);
        //'
        //' Checks if Byte at ulAddress is Protected
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulAddress       = Address of Byte to verify
        //'       pfProtected     = Returns TRUE  (1) if Byte is protected
        //'                         Returns FALSE (0) if Byte is not protected
        //'
        //'   ATTENTION:  In VB TRUE is -1 in C/C++ TRUE is 1
        //'               FALSE is 0 in VB and C/C++
        //Public Declare Function SCard3WBPVerifyProtectBit Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulAddress As Long, _
        //     ByRef pfProtected As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPVerifyProtectBit(long hCard, long ulAddress, ref long pfProtected);
        //'
        //' Writes Data to Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulDataLen       = Length of Data Buffer (pbData)
        //'       pbData          = Data Buffer
        //'       ulAddress       = Address where to start writing
        //'       fProtect        = Set the protectbit if this is TRUE (1)
        //'
        //'   ATTENTION:  In VB TRUE is -1 in C/C++ TRUE is 1
        //'               FALSE is 0 in VB and C/C++
        //Public Declare Function SCard3WBPWriteData Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulDataLen As Long, _
        //     ByRef pbData As Byte, _
        //     ByVal ulAddress As Long, _
        //     ByVal fProtect As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPWriteData(long hCard, long ulDataLen, ref Byte pbData, long ulAddress, long fProtect);
        //'
        //' Compare bytes and protect them if they are equal
        //' Parameters:
        //'       hCard       = Handle to current Card
        //'       bData       = Bytes to compare with
        //'       ulAddress   = Starting to compare at this Address
        //'
        //Public Declare Function SCard3WBPCompareAndProtect Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByRef bData As Byte, _
        //     ByVal ulAddress As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPCompareAndProtect(long hCard, ref Byte bData, long ulAddress);
        //'
        //' Presents a PIN
        //' Parameters:
        //'       hCard        = Handle to current Card
        //'       ulPINLen     = Length of PIN (have to be 2)
        //'       pbPIN        = Byte-array containing PIN
        //'
        //Public Declare Function SCard3WBPPresentPIN Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulPINLen As Long, _
        //     ByRef pbPIN As Byte) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPPresentPIN(long hCard, long ulPINLen, ref Byte pbPIN);
        //'
        //' Change PIN of current Card
        //' Parameters:
        //'       hCard           = Handle to current Card
        //'       ulOldPINLen     = Length of old PIN (have to be 2)
        //'       pbOldPIN        = Byte-array containing old PIN
        //'       ulNewPINLen     = Length of new PIN (have to be 2)
        //'       pbNewPIN        = Byte-array containing new PIN
        //'
        //Public Declare Function SCard3WBPChangePIN Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByVal ulOldPINLen As Long, _
        //     ByRef pbOldPIN As Byte, _
        //     ByVal ulNewPINLen As Long, _
        //     ByRef pbNewPIN As Byte) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPChangePIN(long hCard, long ulOldPINLen, ref Byte pbOldPIN, long ulNewPINLen, ref Byte pbNewPIN);
        //'
        //' Check if PIN is presented
        //' Parameters
        //'       hCard           = Handle to current Card
        //'       pfPinPresented  = TRUE  (1) if PIN is already presented
        //'                         FALSE (0) if PIN is not presented
        //'
        //'   ATTENTION:  In VB TRUE is -1 in C/C++ TRUE is 1
        //'               FALSE is 0 in VB and C/C++
        //Public Declare Function SCard3WBPIsPinPresented Lib "scardsyn" _
        //    (ByVal hCard As Long, _
        //     ByRef pfPinPresented As Long) As Long
        [DllImport("scardsyn")]
        public static extern long SCard3WBPChangePIN(long hCard, ref long pfPinPresented);
    }

    public class CSD8
    {

        /*public static long COMPORT_CSD8;
        public static long COMPORT_DRAWER;

        //'------------------------------------------------------------------------------------------------
        //' RETURN CODES
        //'------------------------------------------------------------------------------------------------

        public const Byte RC_OK = 0x0;
        public const Byte RC_W_CMDEXE = 0x10;
        public const Byte _E_CMDEXE = 0x11;
        public const Byte RC_W_DIAGEXE = 0x12;
        public const Byte RC_W_INSUFFICIENT_COIN = 0x13;
        public const Byte RC_W_BOX_NEARFULL = 0x14;
        public const Byte RC_W_BOX_FULL = 0x15;
        public const Byte RC_W_INCOMPLETE_PAY = 0x16;
        public const Byte RC_W_COVER_OPEN = 0x18;
        public const Byte RC_W_NO_NEW_COIN = 0x19;
        public const Byte RC_E_CMDABORT = 0x30;
        public const Byte RC_E_TOOMANY_COIN = 0x50;
        public const Byte RC_E_PHOTO_COVERED = 0x51;
        public const Byte RC_E_PRECOIN_COVERED = 0x52;
        public const Byte RC_E_CASH_STATUS = 0x55;
        public const Byte RC_E_CLEANING = 0x56;
        public const Byte RC_E_REJECT_FULL = 0x57;
        public const Byte RC_E_DISPENSE_1C = 0x60;
        public const Byte RC_E_DISPENSE_2C = 0x61;
        public const Byte RC_E_DISPENSE_5C = 0x62;
        public const Byte RC_E_DISPENSE_10C = 0x63;
        public const Byte RC_E_DISPENSE_20C = 0x64;
        public const Byte RC_E_DISPENSE_50C = 0x65;
        public const Byte RC_E_DISPENSE_1E = 0x66;
        public const Byte RC_E_DISPENSE_2E = 0x67;
        public const Byte RC_E_PHOTO_EJECT = 0x6A;
        public const Byte RC_E_VALIDATOR_TIMEOUT = 0x70;
        public const Byte RC_E_FLASH_WRITE = 0x80;
        public const Byte RC_E_EEPROM_INIT = 0x83;
        public const Byte RC_E_NOCMD = 0x90;
        public const Byte RC_E_PARAM = 0x91;
        //'
        public const long SW_BIAS = 0x100;
        public const long RC_E_BAD_COM_PARAM = (SW_BIAS + 0);
        public const long RC_E_COM_CLOSE_ERR = (SW_BIAS + 1);
        public const long RC_E_COM_OPEN_ERR = (SW_BIAS + 2);
        public const long RC_E_COM_WRITE_ERR = (SW_BIAS + 3);
        public const long RC_E_COM_READ_ERR = (SW_BIAS + 4);
        public const long RC_E_COM_NOTOPEN_ERR = (SW_BIAS + 5);
        public const long RC_E_SYSTEM_ERR = (SW_BIAS + 6);

        //'------------------------------------------------------------------------------------------------

        public struct ST_INDENTIFY
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FidApi.HWVERSION_LEN + 1)]
            public String HwVersion;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FidApi.FWVERSION_LEN + 1)]
            public String FwVersion;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FidApi.TRANSPORTVERSION_LEN + 1)]
            public String ThicknessVersion;
        }
        public static ST_INDENTIFY g_ST_INDENTIFY;

        public enum E_CHANNEL
        {
            CSD8_CHANNEL = 1,
            DRAWER_CHANNEL
        }


        public static bool CSD8_CHANNEL_open;
        public static bool DRAWER_CHANNEL_open;

        public enum E_COIN_INDEX
        {
            COIN_1C = 1,
            COIN_2C,
            COIN_5C,
            COIN_10C,
            COIN_20C,
            COIN_50C,
            COIN_1E,
            COIN_2E,
            COIN_REJ
        }


        //' l'indice 9 contiene i rejected
        //'    1   2   3   4   5   6   7   8   9
        //'    1C  2C  5C 10C 20C 50C  1E  2E REJ
        public const long MAX_COIN_TYPES = (8 + 1);
        //public static long g_cash_array(1, MAX_COIN_TYPES) ;
        //Public g_depo_array(1 To MAX_COIN_TYPES) As Long
        //Public g_disp_array(1 To MAX_COIN_TYPES) As Long
        //Public g_disp_array_todo(1 To MAX_COIN_TYPES) As Long

        //' posizione cassetti: 1 2 3 4 5 6 7 8
        public const long MAX_DRAWERS = 8;
        public const long g_drawer_pos = (1 - 8);// = { 1, 2, 3, 4, 5, 6, 7, 8 };


        //' max capacita' monete di ogni cassetto
        public const long MAX_IN_DRAWER_1C = 2600;
        public const long MAX_IN_DRAWER_2C = 1900;
        public const long MAX_IN_DRAWER_5C = 1500;
        public const long MAX_IN_DRAWER_10C = 1500;
        public const long MAX_IN_DRAWER_20C = 1000;
        public const long MAX_IN_DRAWER_50C = 800;
        public const long MAX_IN_DRAWER_1E = 850;
        public const long MAX_IN_DRAWER_2E = 800;


        //' status_flag[0]
        public const Byte BIT_POWERON = FidApi.BIT0;
        public const Byte BIT_DIAGON = FidApi.BIT6;
        //' status_flag[1]
        public const Byte BIT_CMDSEQ = FidApi.BIT0;
        //' status_flag[2]
        public const Byte BIT_DRAWER_NEAR_EMPTY = FidApi.BIT1;
        public const Byte BIT_DRAWER_EMPTY = FidApi.BIT2;
        public const Byte BIT_DRAWER_FULL = FidApi.BIT3;
        public const Byte BIT_DRAWER_NEAR_FULL = FidApi.BIT4;
        //' status_flag[3]
        public const Byte BIT_CASHSTATUS_ERROR = FidApi.BIT0;
        public const Byte BIT_CASHSTATUS_ANOMALY = FidApi.BIT1;

        //' ST_STATUS
        public struct ST_STATUS_CSD8
        {
            public static Byte[] status_flag = { 0, 1, 2, 3 };       // status, s1, s2
        }
        public static ST_STATUS_CSD8 g_ST_STATUS_CSD8;

        
        //' ST_INDENTIFY

        public const long VERSION_LEN_CSD8 = (30 + 1);

        public struct ST_INDENTIFY_CSD8
        {
            public static String csd8_id = VERSION_LEN_CSD8.ToString();
            public static String val_id = VERSION_LEN_CSD8.ToString();
            public static String drawer_id = VERSION_LEN_CSD8.ToString();
        }
        public static ST_INDENTIFY_CSD8 g_ST_INDENTIFY_CSD8;

        //' ST_DLLVERSION
        public const long DLLVERSION_LEN_CSD8 = 64;
        public struct ST_DLLVERSION_CSD8
        {
            public static String dllversion = DLLVERSION_LEN_CSD8.ToString();
        }
        public static ST_DLLVERSION_CSD8 g_ST_DLLVERSION_CSD8;


        //'------------------------------------------------------------------------------------------------
        //' CSD8.DLL - Public Prototypes
        //'------------------------------------------------------------------------------------------------
        */
        //'int __stdcall CSD8_Connect(DWORD channel, DWORD port/*COM1-COM10*/);
        //Public Declare Function CSD8_Connect Lib "CSD8.dll" (ByVal channel As Long, ByVal port As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Connect(long channel, long port);
        ////'int __stdcall CSD8_Disconnect(DWORD channel);
        ////Public Declare Function CSD8_Disconnect Lib "CSD8.dll" (ByVal channel As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Disconnect(long channel);
        ////'int __stdcall CSD8_Status(struct csd8_st *st, int *sums);
        ////Public Declare Function CSD8_Status Lib "CSD8.dll" (ByRef p_st_sta As ST_STATUS_CSD8, ByRef p_coin_array As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Connect(ref ST_STATUS_CSD8 p_st_sta, ref long p_coin_array);
        ////'int __stdcall CSD8_Identify(struct csd8_id *id);
        ////Public Declare Function CSD8_Identify Lib "CSD8.dll" (ByRef p_st_id As ST_INDENTIFY_CSD8) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Identify(ref ST_INDENTIFY_CSD8 p_st_id);
        ////'int __stdcall CSD8_Reset(DWORD channel);
        ////Public Declare Function CSD8_Reset Lib "CSD8.dll" (ByVal channel As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Reset(long channel);
        ////'int __stdcall CSD8_DepositStart(CSD8_CALLBACK cbk, int *sums);
        ////Public Declare Function CSD8_DepositStart Lib "CSD8.dll" (ByVal lpFunc As Long, ByRef p_coin_array As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_DepositStart(long lpFunc, long p_coin_array);
        ////'int __stdcall CSD8_DepositStop(void);
        ////Public Declare Function CSD8_DepositStop Lib "CSD8.dll" () As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_DepositStop();
        //'int __stdcall CSD8_Dispense(CSD8_CALLBACK cbk, int *sums, int *positions);
        //Public Declare Function CSD8_Dispense Lib "CSD8.dll" (ByVal lpFunc As Long, ByRef p_coin_array As Long, ByRef p_coin_drawer As Long) As Long
        //[DllImport("CSD8.dll")]
        //public static extern long CSD8_Connect(long channel, long port);
        //'int __stdcall CSD8_CashSet(int *sums);
        //Public Declare Function CSD8_CashSet Lib "CSD8.dll" (ByRef p_cash_array As Long) As Long
        //'TODO...
        //'Public Declare Function CSD8_GetDLLVersion Lib "CSD8.dll" (ByRef p_st_ver As ST_DLLVERSION_CSD8) As Long


        //Public Sub CSD8_GetErrorString(ByVal lERR As Long, ByVal sLastCommand As String)

        //    g_LastError = lERR
        //    g_szLastCommand = sLastCommand
        //    g_szLastErrorDescr = ""

        //    Select Case (lERR)
        //    Case RC_OK:                     g_szLastErrorDescr = "OK!"
        //    Case RC_W_CMDEXE:               g_szLastErrorDescr = "Command Already Running: Continue"
        //    Case RC_E_CMDEXE:               g_szLastErrorDescr = "Command Already Running: Fail"
        //    Case RC_W_DIAGEXE:              g_szLastErrorDescr = "Diagnostic Running"
        //    Case RC_W_INSUFFICIENT_COIN:    g_szLastErrorDescr = "Coins Insufficient For Dispense"
        //    Case RC_W_BOX_NEARFULL:         g_szLastErrorDescr = "At Least 1 Drawer is Near Full"
        //    Case RC_W_BOX_FULL:             g_szLastErrorDescr = "At Least 1 Drawer is Full"
        //    Case RC_W_INCOMPLETE_PAY:       g_szLastErrorDescr = "Payment Not Completed"
        //    Case RC_W_COVER_OPEN:           g_szLastErrorDescr = "Cover Is Open"
        //    Case RC_W_NO_NEW_COIN:          g_szLastErrorDescr = "No New Coin"
        //    Case RC_E_CMDABORT:             g_szLastErrorDescr = "Command Aborted"
        //    Case RC_E_TOOMANY_COIN:         g_szLastErrorDescr = "Too Many Coins Present"
        //    Case RC_E_PHOTO_COVERED:        g_szLastErrorDescr = "Photo Covered Error"
        //    Case RC_E_PRECOIN_COVERED:      g_szLastErrorDescr = "Precoin Covered Error"
        //    Case RC_E_CASH_STATUS:          g_szLastErrorDescr = "Cash Status Error"
        //    Case RC_E_CLEANING:             g_szLastErrorDescr = "Cleaning Error"
        //    Case RC_E_REJECT_FULL:          g_szLastErrorDescr = "Reject Box Full"
        //    Case RC_E_DISPENSE_1C:          g_szLastErrorDescr = "Dispense 1C Error"
        //    Case RC_E_DISPENSE_2C:          g_szLastErrorDescr = "Dispense 2C Error"
        //    Case RC_E_DISPENSE_5C:          g_szLastErrorDescr = "Dispense 5C Error"
        //    Case RC_E_DISPENSE_10C:         g_szLastErrorDescr = "Dispense 10C Error"
        //    Case RC_E_DISPENSE_20C:         g_szLastErrorDescr = "Dispense 20C Error"
        //    Case RC_E_DISPENSE_50C:         g_szLastErrorDescr = "Dispense 50C Error"
        //    Case RC_E_DISPENSE_1E:          g_szLastErrorDescr = "Dispense 1E Error"
        //    Case RC_E_DISPENSE_2E:          g_szLastErrorDescr = "Dispense 2E Error"
        //    Case RC_E_PHOTO_EJECT:          g_szLastErrorDescr = "Eject Photo Calib Error"
        //    Case RC_E_VALIDATOR_TIMEOUT:    g_szLastErrorDescr = "Validator Timeout"
        //    Case RC_E_FLASH_WRITE:          g_szLastErrorDescr = "Flash/Eeprom Write Error"
        //    Case RC_E_EEPROM_INIT:          g_szLastErrorDescr = "Eeprom Init Error"
        //    Case RC_E_NOCMD:                g_szLastErrorDescr = "Command Error"
        //    Case RC_E_PARAM:                g_szLastErrorDescr = "Parameter Error"
        //    '
        //    Case RC_E_BAD_COM_PARAM:        g_szLastErrorDescr = "COM Bad Param"
        //    Case RC_E_COM_CLOSE_ERR:        g_szLastErrorDescr = "COM Close Error"
        //    Case RC_E_COM_OPEN_ERR:         g_szLastErrorDescr = "COM Open Error"
        //    Case RC_E_COM_WRITE_ERR:        g_szLastErrorDescr = "COM Write Error"
        //    Case RC_E_COM_READ_ERR:         g_szLastErrorDescr = "COM Read Error"
        //    Case RC_E_COM_NOTOPEN_ERR:      g_szLastErrorDescr = "COM Notopen Error"
        //    Case RC_E_SYSTEM_ERR:           g_szLastErrorDescr = "System Error"
        //    '
        //    Case Else:                      g_szLastErrorDescr = "Unknown Error"
        //    End Select

        //    g_szLastErrorDescr = g_szLastErrorDescr & " (code: " & CStr(lERR) & " 0x" & Hex(lERR) & ")"

        //End Sub


        //Public Sub CallbackDeposit()

        //    ' NOTA: i contatori vengono INCREMENTATI (++) delle monete depositate.

        //#If False Then
        //'    FormCoinDeposit.LabelConta_1C.Caption = Format(g_depo_array(COIN_1C), "0")
        //'    FormCoinDeposit.LabelConta_2C.Caption = Format(g_depo_array(COIN_2C), "0")
        //'    FormCoinDeposit.LabelConta_5C.Caption = Format(g_depo_array(COIN_5C), "0")
        //'    FormCoinDeposit.LabelConta_10C.Caption = Format(g_depo_array(COIN_10C), "0")
        //'    FormCoinDeposit.LabelConta_20C.Caption = Format(g_depo_array(COIN_20C), "0")
        //'    FormCoinDeposit.LabelConta_50C.Caption = Format(g_depo_array(COIN_50C), "0")
        //'    FormCoinDeposit.LabelConta_1E.Caption = Format(g_depo_array(COIN_1E), "0")
        //'    FormCoinDeposit.LabelConta_2E.Caption = Format(g_depo_array(COIN_2E), "0")
        //'    FormCoinDeposit.LabelConta_REJ.Caption = Format(g_depo_array(COIN_REJ), "0")
        //'    FormCoinDeposit.LabelConta_TOT.Caption = Format(g_depo_array(COIN_1C) + g_depo_array(COIN_2C) + g_depo_array(COIN_5C) + g_depo_array(COIN_10C) + _
        //'                                                    g_depo_array(COIN_20C) + g_depo_array(COIN_50C) + g_depo_array(COIN_1E) + g_depo_array(COIN_2E), "0")
        //'    '
        //'    FormCoinDeposit.LabelAmount_1C.Caption = Format(g_depo_array(COIN_1C) * 1 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_2C.Caption = Format(g_depo_array(COIN_2C) * 2 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_5C.Caption = Format(g_depo_array(COIN_5C) * 5 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_10C.Caption = Format(g_depo_array(COIN_10C) * 10 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_20C.Caption = Format(g_depo_array(COIN_20C) * 20 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_50C.Caption = Format(g_depo_array(COIN_50C) * 50 / 100, "0.00")
        //'    FormCoinDeposit.LabelAmount_1E.Caption = Format(g_depo_array(COIN_1E) * 1, "0.00")
        //'    FormCoinDeposit.LabelAmount_2E.Caption = Format(g_depo_array(COIN_2E) * 2, "0.00")
        //'    FormCoinDeposit.LabelAmount_TOT.Caption = Format((g_depo_array(COIN_1C) * 1 / 100) + (g_depo_array(COIN_2C) * 2 / 100) + (g_depo_array(COIN_5C) * 5 / 100) + (g_depo_array(COIN_10C) * 10 / 100) + _
        //'                                                     (g_depo_array(COIN_20C) * 20 / 100) + (g_depo_array(COIN_50C) * 50 / 100) + (g_depo_array(COIN_1E) * 1) + (g_depo_array(COIN_2E) * 2), "0.00")
        //'
        //'    DoEvents
        //#Else
        //    Call FormCoinDeposit.visualizza_risultati_depo
        //#End If

        //End Sub

        //Public Sub CallbackDispense()

        //    ' E' importante fare in modo che gli errori generati all'interno di una routine callback
        //    ' non vengano propagati alla routine esterna che ha richiamato la routine callback.
        //    ' Per ottenere questo risultato, inserire l'istruzione On Error Resume Next
        //    ' all' inizio della routine callback.
        //'''''NO!!!!--->    On Error Resume Next

        //    ' NOTA: i contatori vengono DECREMENTATI (--) delle monete dispensate.

        //    FormCoinDispense.LabelConta_1C.Caption = Format(g_disp_array(COIN_1C), "0")
        //    FormCoinDispense.LabelConta_2C.Caption = Format(g_disp_array(COIN_2C), "0")
        //    FormCoinDispense.LabelConta_5C.Caption = Format(g_disp_array(COIN_5C), "0")
        //    FormCoinDispense.LabelConta_10C.Caption = Format(g_disp_array(COIN_10C), "0")
        //    FormCoinDispense.LabelConta_20C.Caption = Format(g_disp_array(COIN_20C), "0")
        //    FormCoinDispense.LabelConta_50C.Caption = Format(g_disp_array(COIN_50C), "0")
        //    FormCoinDispense.LabelConta_1E.Caption = Format(g_disp_array(COIN_1E), "0")
        //    FormCoinDispense.LabelConta_2E.Caption = Format(g_disp_array(COIN_2E), "0")
        //''''FormCoinDispense.LabelConta_REJ.Caption = Format(g_disp_array(COIN_REJ), "0")
        //    FormCoinDispense.LabelConta_TOT.Caption = Format(g_disp_array(COIN_1C) + g_disp_array(COIN_2C) + g_disp_array(COIN_5C) + g_disp_array(COIN_10C) + _
        //                                                     g_disp_array(COIN_20C) + g_disp_array(COIN_50C) + g_disp_array(COIN_1E) + g_disp_array(COIN_2E), "0")
        //    '
        //    FormCoinDispense.LabelAmount_1C.Caption = Format(g_disp_array(COIN_1C) * 1 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_2C.Caption = Format(g_disp_array(COIN_2C) * 2 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_5C.Caption = Format(g_disp_array(COIN_5C) * 5 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_10C.Caption = Format(g_disp_array(COIN_10C) * 10 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_20C.Caption = Format(g_disp_array(COIN_20C) * 20 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_50C.Caption = Format(g_disp_array(COIN_50C) * 50 / 100, "0.00")
        //    FormCoinDispense.LabelAmount_1E.Caption = Format(g_disp_array(COIN_1E) * 1, "0.00")
        //    FormCoinDispense.LabelAmount_2E.Caption = Format(g_disp_array(COIN_2E) * 2, "0.00")
        //    FormCoinDispense.LabelAmount_TOT.Caption = Format((g_disp_array(COIN_1C) * 1 / 100) + (g_disp_array(COIN_2C) * 2 / 100) + (g_disp_array(COIN_5C) * 5 / 100) + (g_disp_array(COIN_10C) * 10 / 100) + _
        //                                                      (g_disp_array(COIN_20C) * 20 / 100) + (g_disp_array(COIN_50C) * 50 / 100) + (g_disp_array(COIN_1E) * 1) + (g_disp_array(COIN_2E) * 2), "0.00")
        //    DoEvents

        //End Sub


        //Public Sub CallbackEmptyAll()

        //    ' NON FACCIO NULLA !!!

        //End Sub


        //Public Function myCSD8_Reconnect(last_error As Long, do_always As Boolean) As Long

        //    myCSD8_Reconnect = RC_OK

        //    If ((do_always = False) And (last_error <> RC_E_COM_OPEN_ERR) And (last_error <> RC_E_COM_WRITE_ERR) And (last_error <> RC_E_COM_NOTOPEN_ERR)) Then
        //       Exit Function
        //    End If

        //    Dim rettmp As Long

        //    ' OPEN CSD8_CHANNEL
        //    If (FLAG_NO_CSD8 = True) Then
        //        rettmp = RC_OK: Call Sleep(100)
        //    Else
        //        Call CSD8_Disconnect(CSD8_CHANNEL)
        //        CSD8_CHANNEL_open = False
        //        rettmp = CSD8_Connect(CSD8_CHANNEL, COMPORT_CSD8)
        //    End If
        //    If (rettmp = RC_OK) Then
        //        CSD8_CHANNEL_open = True
        //    End If

        //    ' OPEN DRAWER_CHANNEL
        //    If (FLAG_NO_CSD8 = True) Then
        //        rettmp = RC_OK: Call Sleep(100)
        //    Else
        //        Call CSD8_Disconnect(DRAWER_CHANNEL)
        //        DRAWER_CHANNEL_open = False
        //        rettmp = CSD8_Connect(DRAWER_CHANNEL, COMPORT_DRAWER)
        //    End If
        //    If (rettmp = RC_OK) Then
        //        DRAWER_CHANNEL_open = True
        //    End If

        //    myCSD8_Reconnect = rettmp

        //End Function

    }

    public class ModulePrint
    {
        /*public struct ST_DOLLARS_AMOUNT
        {
            public long one;
            public long two;
            public long five;
            public long ten;
            public long twenty;
            public long fifty;
            public long hundred;
            public long rejected;
            public long suspect;
        };

        public struct ST_DENOM_AMOUNT
        {
            public long denom_1;
            public long denom_2;
            public long denom_5;
            public long denom_10;
            public long denom_20;
            public long denom_50;
            public long denom_100;
            public long denom_200;
            public long denom_500;
            public long denom_1000;
        };

        public struct REPORT
        {
            public long print_cheque_summary_section; // TRUE: PRINT CHEQUES SUMMARY               FALSE: DO NOT PRINT
            public long print_valids_cheques_section; // TRUE: PRINT VALID CHEQUES SECTION         FALSE: DO NOT PRINT
            public long print_rejected_cheques_section;  // TRUE: PRINT REJECTED CHEQUES SECTION      FALSE: DO NOT PRINT
            public long print_dollars_section;           // TRUE: PRINT DOLLARS SECTION               FALSE: DO NOT PRINT
            public long print_denom_section;             // TRUE: PRINT DENOM SECTION                 FALSE: DO NOT PRINT
            //REPORT STRUCTS
            //cheque_report As ST_CHEQUE_REPORT       // Total processed cheques struct
            public ST_DOLLARS_AMOUNT dollars_amount;      // The dollars_amount struct
            public ST_DENOM_AMOUNT denom_amount;         // The denom_amount struct
        };

        public struct ST_DRAWER_REMOVE
        {
            public long current_drawer;
            public long ndoc_drawer_1;
            public long ndoc_drawer_2;
            public long ndoc_drawer_3;
            public long ndoc_drawer_4;
            public String serial_number_1;
            public String serial_number_2;
            public String serial_number_3;
            public String serial_number_4;
            public String operator_name;
            public long Conta1000;
            public long Conta500;
            public long Conta200;
            public long Conta100;
            public long Conta50;
            public long Conta20;
            public long Conta10;
            public long Conta5;
            public long Conta2;
            public long Conta1;
            public long total_checks;
        };

        //int printReceipt ( REPORT * Printing_Report );
        [DllImport("CTSReceipt.dll", SetLastError = true)]
        public static extern long printReceipt (ref REPORT Printing_Report);
        //int printSafeCounter ( ST_SAFE_COUNTER p_safe_counter[4] );
        //[DllImport("CTSReceipt.dll", SetLastError = true)]
        //public static extern long printSafeCounter(ref ST_SAFE_COUNTER p_safe_counter);
        //int printDrawerRemove ( ST_DRAWER_REMOVE * p_drawer_remove );
        [DllImport("CTSReceipt.dll", SetLastError = true)]
        public static extern long printDrawerRemove(ref ST_DRAWER_REMOVE drawer_remove);
        //int printSelectLanguage  ( IN BYTE _language, IN LPSTR _denom_name )
        //public static const LANG_INGLESE = &H30    // default
        //Public Const LANG_ITALIANO = &H31
        //Public Const LANG_SPAGNOLO = &H32
        [DllImport("CTSReceipt.dll", SetLastError = true)]
        public static extern long printSelectLanguage(Byte language_type, String denom_name);*/
        ArrayList headerLines = new ArrayList();
        ArrayList subHeaderLines = new ArrayList();
        public ArrayList items = new ArrayList();
        ArrayList totales = new ArrayList();
        ArrayList footerLines = new ArrayList();
        private Image headerImage = null;

        int count = 0;

        int maxChar = 35;
        int maxCharDescription = 9;

        int imageHeight = 0;

        float leftMargin = 0;
        float topMargin = 3;

        string fontName = "Lucida Console";
        int fontSize = 9;

        Font printFont = null;
        SolidBrush myBrush = new SolidBrush(Color.Black);

        Graphics gfx = null;

        string line = null;

        public ModulePrint()
        {

        }

        public Image HeaderImage
        {
            get { return headerImage; }
            set { if (headerImage != value) headerImage = value; }
        }

        public int MaxChar
        {
            get { return maxChar; }
            set { if (value != maxChar) maxChar = value; }
        }

        public int MaxCharDescription
        {
            get { return maxCharDescription; }
            set { if (value != maxCharDescription) maxCharDescription = value; }
        }

        public int FontSize
        {
            get { return fontSize; }
            set { if (value != fontSize) fontSize = value; }
        }

        public string FontName
        {
            get { return fontName; }
            set { if (value != fontName) fontName = value; }
        }

        public void AddHeaderLine(string line)
        {
            //if (line.Length <= 35)\r\n
            while (line.Contains("\r\n"))
            {
                int l_Inicio = line.IndexOf("\r");
                String l_Imprimir = line.Substring(0, l_Inicio);
                headerLines.Add(l_Imprimir);
                line = line.Substring(l_Inicio + 2);
            }
            if (line != "")
                headerLines.Add(line);
        }

        public void AddSubHeaderLine(string line)
        {
            subHeaderLines.Add(line);
        }

        public void AddItem(string cantidad, string item, string price)
        {
            OrderItem newItem = new OrderItem('?');
            if (item.Length > 18)
                items.Add(newItem.GenerateItem(cantidad, item.Substring(0, 18), price));
            else
                items.Add(newItem.GenerateItem(cantidad, item, price));
        }

        public void AddTotal(string name, string price)
        {
            OrderTotal newTotal = new OrderTotal('?');
            totales.Add(newTotal.GenerateTotal(name, price));
        }

        public void AddFooterLine(string line)
        {
            footerLines.Add(line);
        }

        private string AlignRightText(int lenght)
        {
            string espacios = "";
            int spaces = maxChar - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string AllingPrices(int lenght)
        {
            string espacios = "";
            int spaces = maxChar + 6 - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string DottedLine()
        {
            string dotted = "";
            for (int x = 0; x < maxChar; x++)
                dotted += "=";
            return dotted;
        }

        public bool PrinterExists(string impresora)
        {
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                if (impresora == strPrinter)
                    return true;
            }
            return false;
        }

        public void PrintTicket(string impresora)
        {
            printFont = new Font(fontName, fontSize, FontStyle.Regular);
            PrintDocument pr = new PrintDocument();
            pr.PrinterSettings.PrinterName = impresora;
            pr.PrintPage += new PrintPageEventHandler(pr_PrintPage);
            pr.Print();
        }

        private void pr_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            gfx = e.Graphics;

            DrawImage();

            DrawHeader();
            DrawSubHeader();
            DrawItems();
            DrawTotales();
            DrawFooter();

            if (headerImage != null)
            {
                HeaderImage.Dispose();
                headerImage.Dispose();
            }
        }

        private float YPosition()
        {
            return topMargin + (count * printFont.GetHeight(gfx) + imageHeight);
        }

        private void DrawImage()
        {
            //HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);
            headerImage = null;

            if (headerImage != null)
            {
                try
                {
                    gfx.DrawImage(headerImage, new Point((int)leftMargin, (int)YPosition()));
                    double height = ((double)headerImage.Height);
                    imageHeight = (int)Math.Round(height) - 60;
                }
                catch (Exception)
                {
                }
            }
        }

        private void DrawHeader()
        {
            foreach (string header in headerLines)
            {
                if (header.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = header.Length;

                    while (headerLenght > maxChar)
                    {
                        line = header.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = header;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = header;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawSubHeader()
        {
            foreach (string subHeader in subHeaderLines)
            {
                if (subHeader.Length > maxChar)
                {
                    int currentChar = 0;
                    int subHeaderLenght = subHeader.Length;

                    while (subHeaderLenght > maxChar)
                    {
                        line = subHeader;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        subHeaderLenght -= maxChar;
                    }
                    line = subHeader;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = subHeader;

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;

                    line = DottedLine();

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawItems()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("CANT     DENOMINACION      TOTAL   ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            foreach (string item in items)
            {
                int l_Cantidad = int.Parse(ordIt.GetItemCantidad(item));
                Double l_Precio = Double.Parse(ordIt.GetItemPrice(item));
                String l_Total = (l_Precio * l_Cantidad).ToString("$#,###,##0.00");
                line = ordIt.GetItemCantidad(item);

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                //line = ordIt.GetItemPrice(item);
                line = l_Total;
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);
                while (name.Length < maxCharDescription)
                    name = " " + name;
                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = " " + l_PrecioUnitario;
                leftMargin = 0;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("         " + name.Substring(0, maxCharDescription), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    while (name.Length < maxCharDescription)
                        name = " " + name;
                    gfx.DrawString("          " + name, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = 0;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();
        }

        private void DrawTotales()
        {
            OrderTotal ordTot = new OrderTotal('?');

            foreach (string total in totales)
            {
                line = ordTot.GetTotalCantidad(total);
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                leftMargin = 0;

                line = "          " + ordTot.GetTotalName(total);
                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                count++;
            }
            leftMargin = 0;
            DrawEspacio();
            DrawEspacio();
        }

        private void DrawFooter()
        {
            foreach (string footer in footerLines)
            {
                if (footer.Length > maxChar)
                {
                    int currentChar = 0;
                    int footerLenght = footer.Length;

                    while (footerLenght > maxChar)
                    {
                        line = footer;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        footerLenght -= maxChar;
                    }
                    line = footer;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = footer;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            leftMargin = 0;
            DrawEspacio();
        }

        private void DrawEspacio()
        {
            line = "";

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
        }

    }

    public class OrderItem
    {
        char[] delimitador = new char[] { '?' };

        public OrderItem(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetItemCantidad(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetItemName(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[1];
        }

        public string GetItemPrice(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[2];
        }

        public string GenerateItem(string cantidad, string itemName, string price)
        {
            return cantidad + delimitador[0] + itemName + delimitador[0] + price;
        }
    }

    public class OrderTotal
    {
        char[] delimitador = new char[] { '?' };

        public OrderTotal(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetTotalName(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetTotalCantidad(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[1];
        }

        public string GenerateTotal(string totalName, string price)
        {
            return totalName + delimitador[0] + price;
        }
    }

    public class iButton
    {
        // session handle
        public static long iButton_SHandle;
        // data state space for DLL
        public static byte[] state_buffer;//(15360)
        // port selection
        public static int PortNum;
        public static int PortType;
        // debounce array
        public static int[] Debounce;//(100)
        // save selected values
        public static int[] SelectROM;//(8) As Integer
        public static byte[] selectFile;//(6) As Byte
        // directory of the current opened device
        public static byte[] DirBuf;//(256) As Byte
        // data to save
        public static string iButton_SaveData;// As String
        // flag to indicate if TMSetup has been run
        public static bool iButton_SetupDone; 
        // max debounce value
        public static int MaxDbnc; //As Integer

        public const byte FAMILY_CODE_DS1996 = 0xC;   // 65536-Bit Memory iButton  ROM: "0C ED 31 21 00 00 00 D0"
        public const byte FAMILY_CODE_DS1972 = 0x2D;  // 1024-Bit EEPROM iButton   ROM: "2D 80 63 E2 01 00 00 87"
        public const byte FAMILY_CODE_DS2415 = 0x24;  // realtime clock

        static bool fIButtonInitialized;

        public static bool IButton_ExecRead()
        {
            int iRet;
            string Buf;//* 150
            string NBuf;
            int device_found;
            int flag;
            int[] ROM;//ReDim ROM(8) As Integer
            string RS;// As String
            bool key_found = false;

            if (!fIButtonInitialized)
            {
                MaxDbnc = 3;
                //iRet=
                
            }return key_found;
        }
    }

}
