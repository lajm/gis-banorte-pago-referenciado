﻿using Irds;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace __DemoFID_B
{
    class BDCuentaOperador
    {
        public static int Insertar2(String p_Alias, string p_Banco, string p_Cuenta, bool p_status, bool p_AceptaReferencias, string p_ClientId)
        {

            int p_moneda = 1;

            String l_Query
                = "INSERT INTO Cuenta "
                + "     (  ClaveCliente  "
                + "     , Alias "
                + "     , Banco  "
                + "     , Cuenta "
                + "     , IdMoneda  "
                + "     , Activa "
                + "     , AceptaReferencias ) "
                + "VALUES "
                + "     ('" + p_ClientId + "'  "
                + "     , '" + p_Alias + "'  "
                + "     , '" + p_Banco + "'  "
                + "     , '" + p_Cuenta + "'  "
                + "     , '" + p_moneda + "'  "
                + "     , '" + p_status + "'  "
                + "     , '" + p_AceptaReferencias + "' ) "
                + "; SELECT @@IDENTITY ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();

                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                try
                {
                    String l_IdStr = l_Comando.ExecuteScalar().ToString();

                    int l_Id = Convert.ToInt32(l_IdStr);

                    return l_Id;
                }
                catch (Exception E)

                {
                    MessageBox.Show("Por favor, introduzca un valor único para ID de Cuenta, Esta ID ya existen para otro cliente , Por favor inténtelo de nuevo", "Important Note",
    MessageBoxButtons.OK,
    MessageBoxIcon.Error);

                    return 0;
                }

                return 1;
            }

        }

        public static void DeleteyInsert(String p_Alias, string p_Banco, string p_Cuenta, bool p_status, bool p_AceptaReferencias, string p_ClientId)
        {

            String l_Query1
                         = "DELETE "
                        + "FROM Cuenta "
                        + "WHERE           Cuenta = @Cuenta "
                        + "AND             Banco = @Banco  ";


            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query1, l_Conexion);
                    l_Comando.Parameters.AddWithValue("@Cuenta", p_Cuenta);
                    l_Comando.Parameters.AddWithValue("@Banco", p_Banco);
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception E)
                {
                    throw E;
                }
            }

            // p_status = true;
            // p_AceptaReferencias = true;
            Insertar2(p_Alias, p_Banco, p_Cuenta, p_status, p_AceptaReferencias, p_ClientId);
        }
        public static void Update1(String p_Alias, string p_Banco, string p_Cuenta, bool p_AceptaReferencias, bool p_Status, string p_IdCuenta)
        {

            String l_Query
                    = "UPDATE           Cuenta "
                    + "SET           Alias    = @Alias "
                    + ",          Banco     = @Banco "
                    + ",           Cuenta    = @Cuenta"
                    + ",          Activa     = @Activa "
                    + ",           AceptaReferencias    = @AceptaReferencias "
                    + "WHERE          IdCuenta   = @clientid ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@Alias", p_Alias);
                l_Comando.Parameters.AddWithValue("@Banco", p_Banco);
                l_Comando.Parameters.AddWithValue("@Cuenta", p_Cuenta);
                l_Comando.Parameters.AddWithValue("@Activa", p_Status);
                l_Comando.Parameters.AddWithValue("@AceptaReferencias", p_AceptaReferencias);
                l_Comando.Parameters.AddWithValue("@clientid", p_IdCuenta);

                try
                {
                    l_Comando.ExecuteNonQuery();
                }
                catch (Exception E) { throw; }
            }


        }

        public static DataTable TraerOperador(string p_Clave)
        {
            String l_Query
                = "SELECT Alias "
                + " , Banco "
                + " , Cuenta "
                + " , IdCuenta "
                + " , AceptaReferencias "
                + ",  Activa "
                + "FROM Cuenta "
                + " WHERE ClaveCliente = '" + p_Clave + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    return UtilsDB.GenerarTabla(l_Query);
                }
                catch (Exception E)
                {

                    throw E;
                }
            }

        }


        public static bool SelcOperador(string ClientID, out string p_Alias, out string p_Cuenta, out string p_AceptaReferencias, out string p_status, out string p_Banco, out string p_IdCuenta)
        {

            String l_Query
                = "SELECT Alias "
                + " , Banco "
                + " , Cuenta "
                + ",  Activa "
                 + ",  IdCuenta "
                 + ",  AceptaReferencias "
                + "FROM Cuenta "
                + " WHERE IdCuenta = '" + ClientID + "' ";

            using (SqlConnection l_Conexion = UtilsDB.NuevaConexion())
            {
                l_Conexion.Open();
                try
                {
                    DataTable l_Datos = UtilsDB.GenerarTabla(l_Query);

                    if ((l_Datos != null)
                        || (l_Datos.Rows.Count != 0))
                    {

                        p_Alias = l_Datos.Rows[0]["Alias"].ToString();
                        p_Cuenta = l_Datos.Rows[0]["Cuenta"].ToString();
                        p_AceptaReferencias = l_Datos.Rows[0]["AceptaReferencias"].ToString();
                        p_status = l_Datos.Rows[0]["Activa"].ToString();
                        p_Banco = l_Datos.Rows[0]["Banco"].ToString();
                        p_IdCuenta = l_Datos.Rows[0]["IdCuenta"].ToString();
                        return true;
                    }
                }
                catch (Exception E)
                {
                    throw E;
                }
            }
            p_Alias = p_Cuenta = p_AceptaReferencias = p_status = p_Banco = p_IdCuenta = "";

            return false;
        }
    }
}
