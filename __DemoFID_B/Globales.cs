﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace __DemoFID_B
{
    class Globales
    {
        //' is_SMARTCARD_HCR360
        public static int HCR360_port;
        public static int HCR360_baud;
        public static int HCR360_Parity;
        public static bool DEBUG;
        public static int CajonUSD;
        public static int Capacidad_Cajon;
        private static int Capacidad_Bolsa;
        private static int Operacion_Bolsa;

        public static int OperacionBolsa
        {
            get { return Globales.Operacion_Bolsa; }
            set { Globales.Operacion_Bolsa = value; }
        }

  

        public static long g_hContext;
        public static long g_hCard;
        public static bool g_isContext;
        public static bool g_isCardConnected;
        public static String g_curReader;

       

        public static bool [] g_movelevator = new bool [10];


        public static void Iniciar_moveelevator()
        {

            for (int i = 0; i < 10; i++)
            {
                if (i<5)
                g_movelevator[i] = true;

                if (i > 5)
                    g_movelevator[i] = false;


            }

        }


        public static String Equipo
        {
            get { return Globales.m_Equipo; }
            set { Globales.m_Equipo = value; }
        }

        public static String
            IdUsuario
        {
            get { return Globales.m_IdUsuario; }
            set { Globales.m_IdUsuario = value; }
        }

        public static String ALiasIdUsuario
        {
            get { return Globales.m_ALiasIdUsuario; }
            set { Globales.m_ALiasIdUsuario = value; }
        }

        public static int
            IdTipoUsuario
        {
            get { return Globales.m_IdTipoUsuario; }
            set { Globales.m_IdTipoUsuario = value; }
        }

        public static int
            TiempoInactividad
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["TiempoInactividad"]); }
        }


        public static int Capacidad_Bolsa1
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["CAPACIDAD_BOLSA"]); }

        }

        public static String
            tmp_HCR360_COMPORT
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["HCR360_COMPORT"]; }
        }

        public static String
            NombreUsuario
        {
            get { return Globales.m_NombreUsuario; }
            set { Globales.m_NombreUsuario = value; }
        }

        public static String ReferenciaUsuario
        {
            get { return Globales.m_ReferenciaUsuario; }
            set { Globales.m_ReferenciaUsuario = value; }
        }
        public static String ConevenioDEMUsuario
        {
            get { return Globales.m_ConevenioDEMUsuario; }
            set { Globales.m_ConevenioDEMUsuario = value; }
        }
        public static String ConvenioCIEUsuario
        {
            get { return Globales.m_ConvenioCIEUsuario; }
            set { Globales.m_ConvenioCIEUsuario = value; }
        }

      

        public static String ZonaUsuario
        {
            get { return Globales.m_ZonaUsuario; }
            set { Globales.m_ZonaUsuario = value; }
        }

        public static String AgenciaUsuario
        {
            get { return Globales.m_AgenciaUsuario; }
            set { Globales.m_AgenciaUsuario = value; }
        }

        public static DateTime
            FechaHoraSesion
        {
            get { return Globales.m_FechaHoraSesion; }
            set { Globales.m_FechaHoraSesion = value; }
        }

        public static String
            EmpresaCliente
        {
            get { return Globales.m_IdCuenta; }
            set { Globales.m_IdCuenta = value; }
        }

        public static String
            AliasCuenta
        {
            get { return Globales.m_AliasCuenta; }
            set { Globales.m_AliasCuenta = value; }
        }

        public static bool
            AutenticarUsuario(String p_IdUsuario, String  p_Password)
        {
            String l_NombreUsuario;
            String l_Referencia;
            String l_ConvenioDEM;
            String l_ConvenioCIE;
            String l_Zona;
            String l_Agencia;

            String l_banco;
            String l_cuentabanco;
            String l_cliente;
            String l_idcliente;
            int l_IdPerfil;

            if (BDUsuarios.IniciarSesion(p_IdUsuario, p_Password, out l_NombreUsuario, out l_IdPerfil,out l_Referencia
                ,out l_ConvenioDEM ,out l_ConvenioCIE ,out l_Zona ,out l_Agencia ))
            {
                NombreUsuario = l_NombreUsuario;
                IdTipoUsuario = l_IdPerfil;
                IdUsuario = p_IdUsuario;
                ReferenciaUsuario = l_Referencia;
                ConevenioDEMUsuario = l_ConvenioDEM;
                ConvenioCIEUsuario = l_ConvenioCIE;
                ZonaUsuario = l_Zona;
                AgenciaUsuario = l_Agencia;

                FechaHoraSesion = DateTime.Now;


                BDUsuarios.CuentaAsociada(p_IdUsuario, out l_banco, out l_cuentabanco, out l_cliente, out l_idcliente);

                Banco = l_banco;
                CuentaBanco = l_cuentabanco;
                NombreCliente = l_cliente;
                IdCliente = l_idcliente;

                return true;
            }
            else
            {
                NombreUsuario = null;
                IdTipoUsuario = 0;
                IdUsuario = null;
                ReferenciaUsuario = null;
                ConevenioDEMUsuario = null;
                ConvenioCIEUsuario = null;
                ZonaUsuario = null;
                AgenciaUsuario = null;

                return false;
            }
        }

        public static bool ExisteUsuario(string l_Identificacion)
        {
            return BDUsuarios.ValidarUsuarioExistente(l_Identificacion, 1);
        }
        public static int LlamarTeclado()
        {
            string windir = Environment.GetEnvironmentVariable("WINDIR");
            string osk = null;

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "sysnative"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = Path.Combine(Path.Combine(windir, "system32"), "osk.exe");
                if (!File.Exists(osk))
                {
                    osk = null;
                }
            }

            if (osk == null)
            {
                osk = "osk.exe";
            }


            return System.Diagnostics.Process.Start(osk).Id;
        }

        public static void CambiaEquipo(String p_Equipo)
        {
            if (p_Equipo != Equipo)
            {
    
            if (p_Equipo == "Equipo1" )
            {
                BDEquipo.CambioEquipo(23213);
                Serial = 23213;
                Equipo = "Equipo1";
            }
            else
            {
             
                BDEquipo.CambioEquipo(25647);
                Serial = 25647;
                Equipo = "Equipo2";
            }
            }
        }

        public static bool
            ValidarTiempoLog()
        {
            if ((DateTime.Now.Minute - FechaHoraSesion.Minute) > TiempoInactividad)
                return false;
            else
                return true;
        }

        public static void
            CerrarSesion(String p_IdUsuario)
        {
            NombreUsuario = null;
            IdTipoUsuario = 0;
            IdUsuario = null;
        }

        public static void
            EscribirBitacora(String p_Modulo, String p_Funcion, String p_Mensaje, int nivel_detalle)
        {
            string l_log = ".\\Bitacoras\\Bitacora_" + DateTime.Now.ToString("MM_dd") + ".txt";
           if (nivel_detalle <= Globales.DetalleLOG  )
            {
                using (StreamWriter l_Archivo = File.AppendText(l_log))
                    {
                        l_Archivo.WriteLine(DateTime.Now.ToString ("dd/MM/yyyy HH:mm:ss:fff") + ":::," + p_Modulo + ", " + p_Funcion + ", " + p_Mensaje);
                    }
               
                   
            }
            
           
        }

      

        public static void LeerBolsaPuesta()
        {
            if (File.Exists("NumeroBOLSA.txt"))
            {
                String[] lineas = File.ReadAllLines("NumeroBolsa.txt");

                if (lineas.Length > 0)
                {
                   Globales.NumeroSerieBOLSA = lineas [0];
                }
                else
                    Globales.NumeroSerieBOLSA = "00000";
               
            }
            else
                Globales.NumeroSerieBOLSA = "00000";
               
        }

        private static String m_ALiasIdUsuario;
        private static String m_IdUsuario;
        private static int m_IdTipoUsuario;
        private static String m_IdCuenta;
        private static String m_AliasCuenta;
        private static String m_NombreUsuario;
        private static String m_ReferenciaUsuario;
        private static String m_ConevenioDEMUsuario;
        private static String m_ConvenioCIEUsuario;
        private static String m_ZonaUsuario;
        private static String m_AgenciaUsuario;

        

        private static DateTime m_FechaHoraSesion;
        private static String m_Equipo="Equipo1";
        private static int m_PruebaBanamex;
        public static bool IniciarSesion=true;
        public static bool Alertastock_enviada = false;
        public static bool BolsaCorrecta = true;
        public static String NumeroSerieBOLSA = "";
        public static String NumeroSerieBOLSAanterior = "";
        private static int Serial = 23213;
        private static String m_SMTPcliente;
        private static int m_SMTPpuerto;
        private static String m_SMTPCredencial;
        private static String m_SMTPPassw;
        private static String m_MailDestino;

        public static String MailDestino
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["MailDestino"]; }
        }

        public static String SMTPcliente
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["SMTPCliente"]; }
        }
      

        public static int SMTPpuerto
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["SMTPPuerto"]); }
        }
        

        public static String SMTPCredencial
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["SMTPCredencial"]; }
        }
     

        public static String SMTPPassw
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["SMTPPassw"]; }
        }



        public static String Serial1
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["SerialEquipo"]; }
            
        }

        public static int PruebaBanamex
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["PruebaBANAMEX"]); }
            
        }

        public static string ClienteProsegur
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["ClienteProsegur"]; }

        }

        public static string SucursalProsegur
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["SucursalProsegur"]; }

        }

        public static string ClaveClienteProsegur
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["ClaveClienteProsegur"]; }

        }

        public static int Estacion
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["Estacion"]); }

        }

        public static bool SaveImagenes
        {
            
            get {  int opcion=0;
                opcion= int.Parse (System.Configuration.ConfigurationSettings.AppSettings["SALVARIMAGENES"]);
                if (opcion == 1)
                    return true;
                else
                    return false;
                
            
            }

        }

        public static bool LogServer
        {

            get
            {
                int opcion = 0;
                opcion = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["Log_Comunicacion"]);
                if (opcion == 1)
                    return true;
                else
                    return false;


            }

        }

        public static int SensorRemoverBolsa
        {

            get
            {
                int opcion = 0;
                opcion = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["SELENOIDE"]);
                if (opcion == 1)
                    return 63;
                else
                    return 127;


            }

        }

        public static bool EnviarCorreos
        {
            get
            {
                int opcion = 0;
                opcion = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["EnviarCorreos"]);
                if (opcion == 1)
                    return true;
                else
                    return false;
            }
        }


        public static int TiempoVivo
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["TiempoVivo"]); }

        }

        public static int HoraAcumulado
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["HoraAcumulado"]); }

        }

        public static int MinutosReset
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["Reset"]); }

        }

        public static int DetalleLOG
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["NivelDetalleBitacora"]); }

        }

        public static int TicketsDeposito
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["NumeroTicketsDeposito"]); }

        }

        public static int Doubleleafing
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["ValorDobleLeafing"]); }

        }

        public static int Polymero
        {
            get { return int.Parse(System.Configuration.ConfigurationSettings.AppSettings["ValorPolimero"]); }

        }

        public static string IdCliente { get; internal set; }
        public static string NombreCliente { get; internal set; }
        public static string Banco { get; internal set; }
        public static string CuentaBanco { get; internal set; }
    }
}
