﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormEditarCliente : Form
    {
        public FormEditarCliente( String p_Clave )
        {
            InitializeComponent();
            m_ClaveCliente = p_Clave;
            CargarDatosGenerales();
        }


        String m_ClaveCliente;

        void CargarDatosGenerales( )
        {
            string l_Nombre, l_Telefono;

            if ( BDCliente.TraerCliente(m_ClaveCliente, out l_Nombre, out l_Telefono ) )
            {
                c_Clave.Text = m_ClaveCliente;
                c_Nombre.Text = l_Nombre;
                c_Telefono.Text = l_Telefono;
            }

        }

        public static void Ejecutar( String p_Clave )
        {
            using ( FormEditarCliente l_Forma = new FormEditarCliente( p_Clave) )
            {
                l_Forma.c_Usuarios.DataSource = BDOperador.TraerOperador( p_Clave );
                l_Forma.c_Usuarios.Update();
                Application.DoEvents();


                l_Forma.c_Cuentas.DataSource = BDCuentaOperador.TraerOperador( p_Clave );
                l_Forma.c_Cuentas.Update();
                Application.DoEvents();

                l_Forma.ShowDialog();
            }
        }

        private void c_BAgregarUsuario_Click( object sender, EventArgs e )
        {
            string ClientID1;

            ClientID1 = c_Usuarios.RowCount.ToString();
            if ( ClientID1 == "1" )
            {
                MessageBox.Show( "No Puedo Agregar mas de Uno Usario", "Important Note",
MessageBoxButtons.OK,
MessageBoxIcon.Error );
                return;
            }

            using ( FormEditarDatosOperadoresAgregar l_Forma = new FormEditarDatosOperadoresAgregar( c_Clave.Text ) )
            {
                l_Forma.ShowDialog();

                c_Usuarios.DataSource = BDOperador.TraerOperador(  c_Clave.Text );
                c_Usuarios.Update();
                Application.DoEvents();
            }
        }

        private void c_BEditarUsuario_Click( object sender, EventArgs e )
        {
            string ClientID1;

            ClientID1 = c_Usuarios.Rows[ c_Usuarios.SelectedRows[ 0 ].Index ].Cells["IdUsuario"].Value.ToString();



            using ( FormEditarDatosOperadoresEditar l_Forma = new FormEditarDatosOperadoresEditar( ClientID1 ) )
            {
                

                c_Usuarios.DataSource = BDOperador.TraerOperador( c_Clave.Text );
                c_Usuarios.Update();
                Application.DoEvents();
            }
        }

        private void button2_Click( object sender, EventArgs e )
        {
            this.Close();
            
            
        }

        private void c_BAgregarCuenta_Click( object sender, EventArgs e )
        {
            string ClientID1;

            ClientID1 = c_Cuentas.RowCount.ToString();
            if ( ClientID1 == "1" )
            {
                MessageBox.Show( "No Puedo Agregar mas de Uno Cuenta", "Important Note",
     MessageBoxButtons.OK,
     MessageBoxIcon.Error );
                return;
            }

            using ( FormEditarDatosCuentasAgregar l_Forma = new FormEditarDatosCuentasAgregar( c_Clave.Text ) )
            {
                l_Forma.ShowDialog();

                c_Cuentas.DataSource = BDCuentaOperador.TraerOperador( c_Clave.Text );
                c_Cuentas.Update();
                Application.DoEvents();
            }
        }

        private void c_BEditarCuenta_Click( object sender, EventArgs e )
        {
            string ClientID2;

            ClientID2 = c_Cuentas.Rows[ c_Cuentas.SelectedRows[ 0 ].Index ].Cells[ "IdCuenta" ].Value.ToString();



            using ( FormEditarDatosCuentaEditar l_Forma = new FormEditarDatosCuentaEditar( ClientID2 ) )
            {


                c_Cuentas.DataSource = BDCuentaOperador.TraerOperador( c_Clave.Text );
                c_Cuentas.Update();
                Application.DoEvents();
            }
        }

        private void button1_Click( object sender, EventArgs e )
        {
            using ( FormEditarDatosGenerales form_l = new FormEditarDatosGenerales( c_Clave.Text,c_Nombre.Text,c_Telefono.Text ) )
            {
                CargarDatosGenerales();   
               // form_l.ShowDialog();
            }

           

        }
    }
}
