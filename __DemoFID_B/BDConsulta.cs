﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace __DemoFID_B
{
    public class BDConsulta
    {
        public static DataTable
            ObtenerConsultaDepositos(int p_IdMoneda)
        {
            String l_Query
                = "SELECT               SUM(b.Cantidad), c.Codigo, a.IdCajon "
                + "FROM                 Denominacion c, Deposito a, DetalleDeposito b "
                + "WHERE                c.IdDenominacion = b.IdDenominacion "
                + "AND                  a.IdDeposito = b.IdDeposito "
                + "AND                  a.Retirado = 0 "
                + "AND                  a.IdMoneda = @IdMoneda "
                + "GROUP BY             c.Codigo, a.IdCajon ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("ConsultaDeposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
           ObtenerConsulta_Depositos(int p_IdMoneda,String p_Equipo)
        {
            String l_Query
                = "SELECT               SUM(b.Cantidad), c.Codigo, a.IdCajon "
                + "FROM                 Denominacion c, Deposito a, DetalleDeposito b "
                + "WHERE                a.Equipo = @Equipo "
                + "AND                  c.IdDenominacion = b.IdDenominacion "
                + "AND                  a.IdDeposito = b.IdDeposito "
                + "AND                  a.Retirado = 0 "
                + "AND                  a.IdMoneda = @IdMoneda "
                + "GROUP BY             c.Codigo, a.IdCajon ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                 l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("ConsultaDeposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }


        public static DataTable
            ObtenerConsultaOperaciones()
        {
            String l_Query
                = "SELECT               a.IdUsuario, 'DEPÓSITO', '$' + CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1), "
                + "                     b.Descripcion, a.FechaHora "
                + "FROM                 Moneda b, Deposito a " 
                + "WHERE                b.IdMoneda = a.IdMoneda "
                + "AND					DATEPART(DAY, a.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, a.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, a.FechaHora) = @anio "
                + "UNION "
                + "SELECT				c.IdUsuario, ' RETIRO ', '$' + CONVERT(VARCHAR, CONVERT(MONEY, c.Total), 1), " 
                + "                     b.Descripcion, c.FechaHora "
                + "FROM				    Moneda b, Retiro c "
                + "WHERE				b.IdMoneda = c.IdMoneda "
                + "AND					DATEPART(DAY, c.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, c.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, c.FechaHora) = @anio "
                + "ORDER BY			    5 DESC ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@dia", DateTime.Today.Day.ToString());
                l_Comando.Parameters.AddWithValue("@mes", DateTime.Today.Month.ToString());
                l_Comando.Parameters.AddWithValue("@anio", DateTime.Today.Year.ToString());
                DataTable l_Datos = new DataTable("Operaciones");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
         ObtenerConsultaOperaciones(DateTime p_Fecha)
        {
            String l_Query
                = "SELECT               u.Nombre, a.IdUsuario, 'DEPÓSITO', '$' + CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1), "
                + "                     b.Descripcion, a.FechaHora "
                + "FROM                 Moneda b, Deposito a, Usuario u "
                + "WHERE                b.IdMoneda = a.IdMoneda "
                + "AND					DATEPART(DAY, a.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, a.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, a.FechaHora) = @anio "
                    + "AND				u.IdUsuario = a.IdUsuario "
                + "UNION "
                + "SELECT				v.Nombre, c.IdUsuario, ' RETIRO ', '$' + CONVERT(VARCHAR, CONVERT(MONEY, c.Total), 1), "
                + "                     b.Descripcion, c.FechaHora "
                + "FROM				    Moneda b, Retiro c, Usuario v "
                + "WHERE				b.IdMoneda = c.IdMoneda "
                + "AND					DATEPART(DAY, c.FechaHora) = @dia "
                + "AND					DATEPART(MONTH, c.FechaHora) = @mes "
                + "AND					DATEPART(YEAR, c.FechaHora) = @anio "
                 + "AND					v.IdUsuario = c.IdUsuario "
                + "ORDER BY			    6 DESC ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@dia", p_Fecha.Day.ToString());
                l_Comando.Parameters.AddWithValue("@mes", p_Fecha.Month.ToString());
                l_Comando.Parameters.AddWithValue("@anio", p_Fecha.Year.ToString());
                DataTable l_Datos = new DataTable("Operaciones");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
        ObtenerConsultaOperaciones_mov()
        {
            String l_Query
                = "SELECT              a.FechaHora, a.IdUsuario,  CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1) "
                + "FROM                 Deposito a, Usuario u "
                + "WHERE               u.IdUsuario = a.IdUsuario "
                    + "AND             a.Retirado =0 ";


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Movimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable ObtenerConsultaOperaciones_movTotales()
        {
            String l_Query
                = "SELECT               a.IdUsuario, SUM( CONVERT(VARCHAR, CONVERT(MONEY, a.TotalDeposito), 1)) "
                + "FROM                 Deposito a, Usuario u "
                + "WHERE               u.IdUsuario = a.IdUsuario "
                    + "AND             a.Retirado =0 "
                    + "GROUP BY a.IdUsuario" ;


            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                DataTable l_Datos = new DataTable("Movimientos");
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }
    }
}
