﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace __DemoFID_B
{
    public class BDEquipo
    {
        public static bool
            ObtenerEquipo(out String p_Serie, out String p_ContractNumber, out String p_ProviderId, 
                out String p_InstallationId, out String p_SystemUserId, out String p_Key1, out String p_Key2,int  p_numeroInstalacion)
        {
            // Función para extraer los datos de la base de datos;
            String l_Query
                = "SELECT                   NumeroSerie, ContractNumber, ProviderId, InstallationId, "
                + "                         SystemUserId, CryptKey1, CryptKey2 "
                + "FROM                     Equipo "
                + "WHERE                    IdEquipo = @id_Equipo ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@id_Equipo",p_numeroInstalacion);
                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                {
                    p_Serie = l_Lector[0].ToString();
                    p_ContractNumber = l_Lector[1].ToString();
                    p_ProviderId = l_Lector[2].ToString();
                    p_InstallationId = l_Lector[3].ToString();
                    p_SystemUserId = l_Lector[4].ToString();
                    p_Key1 = l_Lector[5].ToString();
                    p_Key2 = l_Lector[6].ToString();
                    return true;
                }
                else
                {
                    p_Serie = "";   
                    p_ContractNumber = "";
                    p_ProviderId = "";
                    p_InstallationId = "";
                    p_SystemUserId = "";
                    p_Key1 = "";
                    p_Key2 = "";
                    Globales.EscribirBitacora("Inicio","ObtenerEquipo","Error En coonsulta Sql Confirmar conexion ",1);
                    return false;
                }

            }
        }

        public static bool
            ActualizarSystemUserId(string p_Serie, String p_SystemUserId, int p_IdEquipo)
        {
            String l_Query
                = "UPDATE           Equipo "
                + "SET              SystemUserId = @SystemUserId "
                + "WHERE            IdEquipo = @IdEquipo ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@SystemUserId", p_SystemUserId);
               l_Comando.Parameters.AddWithValue("@IdEquipo", p_IdEquipo);

                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    Globales.EscribirBitacora("Inicio", " ActualizarSystemUserId", "Error En coonsulta Sql Confirmar conexion ",1);
                    return false;
                }

                else
                {
                    return true;
                }
            }
        }

        public static bool
            ActualizarCryptKeys(string p_Serie, String p_Key1, String p_Key2, int p_IdEquipo)
        {
            String l_Query
                = "UPDATE           Equipo "
                + "SET              CryptKey1 = @CryptKey1, "
                + "                 CryptKey2 = @CryptKey2 "
                + "WHERE            IdEquipo = @IdEquipo ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@CryptKey1", p_Key1);
                l_Comando.Parameters.AddWithValue("@CryptKey2", p_Key2);
                l_Comando.Parameters.AddWithValue("@IdEquipo", p_IdEquipo);

                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            BorrarVariables(string p_Serie,int p_IdEquipo)
        {
            String l_Query
                = "UPDATE           Equipo "
                + "SET              SystemUserId = null, "
                + "                 CryptKey1 = null, "
                + "                 CryptKey2 = null "
                + "WHERE            IdEquipo = @IdEquipo  ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdEquipo", p_IdEquipo);

                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
          CambioEquipo(int p_Equipo)
        {
            String l_Query
                = "UPDATE           Equipo "
                + "SET              NumeroSerie =@NumeroSerie ";
               

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@NumeroSerie", p_Equipo);
               if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }
    }
}
