﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace __DemoFID_B
{
    class BDSide
    {

        public static bool
          InsertarTransaccion( int p_idtransaccion , string p_dispositivo , string  p_usuario , int p_operacion , DateTime  p_fechaHoraDispositivo
                            , DateTime  p_fechahoraServicio , string  p_codigoBolsa , int p_cuentaBanco , int p_referencia 
                            , int p_estatusMaquina , int p_codigoRetiro , string  p_descripcion , int p_totalValidado , int p_billetesValidados 
                            , int p_totalManual , int p_billetesManual , float p_totalDocumentosExternos , float p_totalSobre , string  p_claveSobre 
                            , string  p_divisa ,DataTable p_detalleDeposito , int p_denominacion7 , int p_denominacion8 , int p_denominacion1Manual , int p_denominacion2Manual 
                            , int p_denominacion3Manual , int p_denominacion4Manual , int p_denominacion5Manual , int p_denominacion6Manual 
                            , int p_denominacion7Manual , int p_denominacion8Manual , int p_estatusTransaccion )
        {
           int p_IdSIDE = FolioSIDE ();
           int p_transaccionBolsa = FolioDepositoBoilsa(p_codigoBolsa );

           if (p_operacion == 44)
               p_transaccionBolsa++;

           p_detalleDeposito = completarTabla(p_detalleDeposito);

            String l_Query
                = "INSERT INTO          Transacciones "
                + "                     (idtransaccion ,dispositivo ,usuario ,operacion "
                + "                     ,fechaHoraDispositivo ,fechahoraServicio ,transaccionBolsa  "
                + "                     ,codigoBolsa ,cuentaBanco ,referencia ,estatusMaquina ,codigoRetiro  "
                + "                     ,descripcion ,totalValidado ,billetesValidados ,totalManual "
                + "                     ,billetesManual ,totalDocumentosExternos ,totalSobre ,claveSobre  "
                + "                     ,divisa ,denominacion1 ,denominacion2 ,denominacion4 ,denominacion3  "
                + "                     ,denominacion5 ,denominacion6 ,denominacion7 ,denominacion8 "
                + "                     ,denominacion1Manual ,denominacion2Manual ,denominacion3Manual ,denominacion4Manual  "
                + "                     ,denominacion5Manual ,denominacion6Manual ,denominacion7Manual ,denominacion8Manual ,estatusTransaccion) "
                + "VALUES               (@idtransaccion ,@dispositivo ,@usuario ,@operacion "
                + "                     ,@fechaHoraDispositivo ,@fechahoraServicio ,@transaccionBolsa  "
                + "                     ,@codigoBolsa ,@cuentaBanco ,@referencia ,@estatusMaquina ,@codigoRetiro  "
                + "                     ,@descripcion ,@totalValidado ,@billetesValidados ,@totalManual "
                + "                     ,@billetesManual ,@totalDocumentosExternos ,@totalSobre ,@claveSobre  "
                + "                     ,@divisa ,@denominacion1 ,@denominacion2 ,@denominacion4 ,@denominacion3  "
                + "                     ,@denominacion5 ,@denominacion6 ,@denominacion7 ,@denominacion8 "
                + "                     ,@denominacion1Manual ,@denominacion2Manual ,@denominacion3Manual ,@denominacion4Manual  "
                + "                     ,@denominacion5Manual ,@denominacion6Manual ,@denominacion7Manual ,@denominacion8Manual ,@estatusTransaccion) "
     ;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexionSIDE())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@idtransaccion", p_idtransaccion);
                l_Comando.Parameters.AddWithValue("@dispositivo", p_dispositivo);
                l_Comando.Parameters.AddWithValue("@usuario", p_usuario);
                l_Comando.Parameters.AddWithValue("@operacion", p_operacion);
                l_Comando.Parameters.AddWithValue("@fechaHoraDispositivo", p_fechaHoraDispositivo);
                l_Comando.Parameters.AddWithValue("@fechahoraServicio", p_fechahoraServicio);
                l_Comando.Parameters.AddWithValue("@transaccionBolsa", p_transaccionBolsa);
                l_Comando.Parameters.AddWithValue("@codigoBolsa", p_codigoBolsa);
                l_Comando.Parameters.AddWithValue("@cuentaBanco", p_cuentaBanco);
                l_Comando.Parameters.AddWithValue("@referencia", p_referencia);
                l_Comando.Parameters.AddWithValue("@estatusMaquina", p_estatusMaquina);
                l_Comando.Parameters.AddWithValue("@codigoRetiro", p_codigoRetiro);
                l_Comando.Parameters.AddWithValue("@descripcion", p_descripcion);
                l_Comando.Parameters.AddWithValue("@totalValidado", p_totalValidado);
                l_Comando.Parameters.AddWithValue("@billetesValidados", p_billetesValidados);
                l_Comando.Parameters.AddWithValue("@totalManual", p_totalManual);
                l_Comando.Parameters.AddWithValue("@billetesManual", p_billetesManual);
                l_Comando.Parameters.AddWithValue("@totalDocumentosExternos", p_totalDocumentosExternos);
                l_Comando.Parameters.AddWithValue("@totalSobre", p_totalSobre);
                l_Comando.Parameters.AddWithValue("@claveSobre", p_claveSobre);
                l_Comando.Parameters.AddWithValue("@divisa", p_divisa);
                l_Comando.Parameters.AddWithValue("@denominacion7", p_denominacion7 );
                l_Comando.Parameters.AddWithValue("@denominacion8", p_denominacion8 );
                l_Comando.Parameters.AddWithValue("@denominacion1Manual", p_denominacion1Manual);
                l_Comando.Parameters.AddWithValue("@denominacion2Manual", p_denominacion2Manual);
                l_Comando.Parameters.AddWithValue("@denominacion3Manual", p_denominacion3Manual);
                l_Comando.Parameters.AddWithValue("@denominacion4Manual", p_denominacion4Manual);
                l_Comando.Parameters.AddWithValue("@denominacion5Manual", p_denominacion5Manual);
                l_Comando.Parameters.AddWithValue("@denominacion6Manual", p_denominacion6Manual);
                l_Comando.Parameters.AddWithValue("@denominacion7Manual", p_denominacion7Manual);
                l_Comando.Parameters.AddWithValue("@denominacion8Manual", p_denominacion8Manual);
                l_Comando.Parameters.AddWithValue("@estatusTransaccion", p_estatusTransaccion);
                //Denominaciones


                
                    l_Comando.Parameters.AddWithValue("@denominacion1", (int)p_detalleDeposito.Rows[5][1]);
                    l_Comando.Parameters.AddWithValue("@denominacion2", (int)p_detalleDeposito.Rows[4][1]);
                    l_Comando.Parameters.AddWithValue("@denominacion4", (int)p_detalleDeposito.Rows[3][1]);
                    l_Comando.Parameters.AddWithValue("@denominacion3", (int)p_detalleDeposito.Rows[2][1]);
                    l_Comando.Parameters.AddWithValue("@denominacion5", (int)p_detalleDeposito.Rows[1][1]);
                    l_Comando.Parameters.AddWithValue("@denominacion6", (int)p_detalleDeposito.Rows[0][1]);
                
                


                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();
                    return true;
                }
            }
        }


        private static int
            FolioSIDE()
        {
            String l_Query
                = "SELECT MAX           (idtransaccion) "
                + "FROM                 Transacciones ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexionSIDE())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                }
            }
            l_Folio++;
            return l_Folio;
        }

        private static int
            FolioDepositoBoilsa(string p_codigoBolsa)
        {
            String l_Query
                = "SELECT COUNT           (Bolsa) "
                + "FROM                 Deposito "
                + "WHERE          Bolsa ="+p_codigoBolsa +" " ;
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                }
            }
            //l_Folio++;
            return l_Folio;
        }

        private static DataTable completarTabla (DataTable p_Valores)
        {
        DataTable l_tablacompleta= BDDeposito.ObtenerDetalleDeposito(0);

        DataRow l_1000 = l_tablacompleta.NewRow ();
                    l_1000[0] = BDDenominaciones.ObtenerIdDenominacion(1, "1000");
                    l_1000[1] = 0;
                    l_1000[2] = "1000";
                  

                    DataRow l_500 = l_tablacompleta.NewRow();
                    l_500[0] = BDDenominaciones.ObtenerIdDenominacion(1, "500");
                    l_500[1] = 0;
                    l_500[2] = "500";
                    
                    DataRow l_200 = l_tablacompleta.NewRow();
                    l_200[0] = BDDenominaciones.ObtenerIdDenominacion(1, "200");
                    l_200[1] = 0;
                    l_200[2] = "200";
                   
                    DataRow l_100 = l_tablacompleta.NewRow();
                    l_100[0] = BDDenominaciones.ObtenerIdDenominacion(1, "100");
                    l_100[1] = 0;
                    l_100[2] = "100";
            
                    DataRow l_50 = l_tablacompleta.NewRow();
                    l_50[0] = BDDenominaciones.ObtenerIdDenominacion(1, "50");
                    l_50[1] = 0;
                    l_50[2] = "50";
                   
                    DataRow l_20 = l_tablacompleta.NewRow();
                    l_20[0] = BDDenominaciones.ObtenerIdDenominacion(1, "20");
                    l_20[1] = 0;
                    l_20[2] = "20";
                  


          foreach (DataRow l_Detalle in p_Valores . Rows)
                    {
                        switch (l_Detalle["Codigo"].ToString ())
                        {
                            case "1000":
                                l_1000[1]=  l_Detalle [1];
                                break;
                            case "500":
                               l_500[1] = l_Detalle[1];
                                break;
                            case "200":
                                l_200[1] = l_Detalle[1];
                                break;
                            case "100":
                                l_100[1] = l_Detalle[1];
                                break;
                            case "50":
                                l_50[1] = l_Detalle[1];
                                break;
                            case "20":
                                l_20[1] = l_Detalle[1];
                                break;
                            default:
                                break;
                        }
                    }

          l_tablacompleta.Rows.Add(l_1000);
          l_tablacompleta.Rows.Add(l_500);
          l_tablacompleta.Rows.Add(l_200);
          l_tablacompleta.Rows.Add(l_100);
          l_tablacompleta.Rows.Add(l_50);
          l_tablacompleta.Rows.Add(l_20);

          return l_tablacompleta;
        }
    }
}
