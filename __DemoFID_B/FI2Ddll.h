#ifndef _FI2DDLL_H_
#define _FI2DDLL_H_

#define BITNONE							(BYTE)0x00
#define BIT0							(BYTE)0x01
#define BIT1							(BYTE)0x02
#define BIT2							(BYTE)0x04
#define BIT3							(BYTE)0x08
#define BIT4							(BYTE)0x10
#define BIT5							(BYTE)0x20
#define BIT6							(BYTE)0x40
#define BIT7							(BYTE)0x80

// rs232_photo[0]
#define BIT_STATUS_FEEDER				BIT2
#define BIT_STATUS_SUSPECT				BIT3
#define BIT_STATUS_REJECT				BIT4
#define BIT_STATUS_ESCROW				BIT6
// rs232_photo[3]
#define BIT_STATUS_FEED_RUNNING			BIT1


#define FI2D_CONNECTION_USB				'U'
#define FI2D_CONNECTION_LAN				'L'

#define NS_CONNECT_LOADDLL_YES			TRUE
#define NS_CONNECT_LOADDLL_NO			FALSE

#define SCAN_EXTRAMODE_NONE				'0'
#define SCAN_EXTRAMODE_UV				'1'
#define SCAN_EXTRAMODE_NOSCAN			'2'

// Magnetic scan mode (ScanMagneticMode) 
#define BIT_SCAN_MAGNETIC_NO			BIT7
#define BIT_SCAN_MAGNETIC_YES			BIT0
#define BIT_SCAN_MAGNETIC_NOCOMP		BIT1	// reserved
#define BIT_SCAN_MAGNETIC_COUNTERS		BIT2	// reserved
#define BIT_SCAN_MAGNETIC_SINGLE_CHAN	BIT3	// reserved

// BIT_SCAN_MAGNETIC_SINGLE_CHAN		// reserved
#define MAGNETIC_SINGLE_CHAN_0			'0'
#define MAGNETIC_SINGLE_CHAN_1			'1'
#define MAGNETIC_SINGLE_CHAN_2			'2'
#define MAGNETIC_SINGLE_CHAN_3			'3'
#define MAGNETIC_SINGLE_CHAN_4			'4'
#define MAGNETIC_SINGLE_CHAN_5			'5'
#define MAGNETIC_SINGLE_CHAN_6			'6'
#define MAGNETIC_SINGLE_CHAN_7			'7'
#define MAGNETIC_SINGLE_CHAN_8			'8'
#define MAGNETIC_SINGLE_CHAN_9			'9'
#define MAGNETIC_SINGLE_CHAN_10			'A'


// Validate
#define BIT_VALIDATE_NONE				BIT7
#define BIT_VALIDATE_BANKNOTE			BIT0
#define BIT_VALIDATE_CHECK				BIT1
#define BIT_VALIDATE_LOCALIZATION		BIT2 // reserved
#define BIT_VALIDATE_THICKNESS			BIT3 // reserved
#define BIT_VALIDATE_COUNTERS			BIT4 // reserved


// Codeline
#define NO_READ_CODELINE				'0'
#define READ_CODELINE_MICR				'1'
#define READ_BARCODE_PDF417				'2'
#define READ_BARCODE_2_OF_5				'3'
#define READ_BARCODE_CODE39				'4'
#define READ_BARCODE_CODE128			'5'
#define READ_BARCODE_EAN13				'6'
#define READ_CODELINE_SW_OCRA			'A'
#define READ_CODELINE_SW_OCRB_NUM		'B'
#define READ_CODELINE_SW_OCRB_ALFANUM	'C'
#define READ_CODELINE_SW_OCRB_ITALY		'F'
#define READ_CODELINE_SW_E13B			'E'
#define READ_ONE_CODELINE_TYPE			'N'
#define READ_CODELINE_SW_MULTI_READ		'M'
#define READ_CODELINE_SW_E13B_X_OCRB	'X'
#define READ_CODELINE_CMC7				'Z'

// ResetType
#define RESET_SW_ERROR					'0'
#define RESET_HW_FREE_PATH				'1'

// Codeline position
#define CODELINE_NOT_PRESENT			'0'
#define CODELINE_ON_FRONT_DOWN			'1'
#define CODELINE_ON_FRONT_UP			'2'
#define CODELINE_ON_REAR_DOWN			'3'
#define CODELINE_ON_REAR_UP				'4'

/*	ScanMode:
	SPEED0: 2822 mm/s not_scanned_notes (for counting)  (~1500 doc/min)
	SPEED1: 1411 mm/s not_interlaced_checks 200x200 dpi (~ 400 doc/min)
	                  interlaced_checks     200x100 dpi (~ 400 doc/min)
	SPEED2: 2285 mm/s not_interlaced_notes  200x120 dpi (~1000 doc/min)
	                  interlaced_notes      200x60  dpi (~1000 doc/min)
	SPEED3:  705 mm/s interlaced_checks     200x200 dpi (~ 200 doc/min)
*/
// Parameter ScanMode
#define SCAN_MODE_SPEED0				'0' // no-scan
#define SCAN_MODE_SPEED1_Y				'1' // 200x200 (default for checks)
#define SCAN_MODE_SPEED1_YNC			'2' // 200x200
#define SCAN_MODE_SPEED1_IR 			'3' // 200x200
#define SCAN_MODE_SPEED1_IRNC			'4' // 200x200
#define SCAN_MODE_SPEED1_BOTH			'5' // 200x100
#define SCAN_MODE_SPEED1_BOTHNC			'6' // 200x100
#define SCAN_MODE_SPEED2_Y				'7' // 200x120
#define SCAN_MODE_SPEED2_YNC			'8' // 200x120
#define SCAN_MODE_SPEED2_IR				'9' // 200x120
#define SCAN_MODE_SPEED2_IRNC			'A' // 200x120
#define SCAN_MODE_SPEED2_BOTH			'B' // 200x60 (default for banknotes)
#define SCAN_MODE_SPEED2_BOTHNC			'C' // 200x60 
#define SCAN_MODE_SPEED3_BOTH			'D' // 200x200
#define SCAN_MODE_SPEED3_BOTHNC			'E' // 200x200

#define SCAN_DOC_SPEED_200				'0'
#define SCAN_DOC_SPEED_120				'2'

#define SCAN_SOURCE_COUNTERS			'0'	// reserved
#define SCAN_SOURCE_SCANNER				'1'

#define SCAN_SOURCE_ENCODER_YES			'0'
#define SCAN_SOURCE_ENCODER_NO			'1'	// reserved

// Parameter ClearBlack
#define NO_CLEAR_BLACK					'0'
#define CLEAR_ALL_BLACK					'1'
#define CLEAR_AND_ALIGN_IMAGE			'2'
#define ALIGN_IMAGE						'3'

// Parameter Side
#define SIDE_FRONT_IMAGE				'F'
#define SIDE_BACK_IMAGE					'R'
#define SIDE_ALL_IMAGE					'B'
#define SIDE_NONE_IMAGE					'N'

// Parameter SaveMode
#define	IMAGE_SAVE_NONE					'0'	// not used
#define	IMAGE_SAVETO_FILE				'1'
#define	IMAGE_SAVETO_MEMORY				'2'
#define	IMAGE_SAVE_BOTH					'3'

// Parameter FileFormat
#define	SAVE_JPEG						'0'
#define	SAVE_BMP						'1'

// Parameter: DocType 
#define DOCTYPE_SCAN_CHECK				'A'
#define DOCTYPE_SCAN_BANKNOTE			'B'

// Parameter: drawer_id
#define SAFE_DRAWER_ALL					'0'
#define SAFE_DRAWER_1					'1'
#define SAFE_DRAWER_2					'2'
#define SAFE_DRAWER_3					'3'
#define SAFE_DRAWER_4					'4'
#define SAFE_DRAWER_BAG					0x80

#define	MAX_CODELINE_LEN				256	// max length of returned codeline

// max number of documents handled by the DLL
#define MAXDOC_TOTAL_400				400

// Parameter: NumDocument (to scan all docs present in the feeder)
#define SCAN_ALL_DOCS					999


//
// document types (by validation process) 
//
#define DOCTYPE_NOTYPE					(BYTE)0xFF
	// 0x2x: extra errors (not from validation)
#define DOCTYPE_VALIDATE_TIMEOUT		(BYTE)0x20  // (es.: validation time too long)
#define DOCTYPE_VALIDATE_SEQUENCE		(BYTE)0x24	// validation sequence error
#define DOCTYPE_VALIDATE_FEED_ANOMALY	(BYTE)0x2D	// feed anomaly
	// doc_thickness[]
#define DOCTYPE_THICKNESS_DOUBLE		(BYTE)0x21  // thickness: Double feed
#define DOCTYPE_THICKNESS_NODOC			(BYTE)0x22  // thickness: No doc
#define DOCTYPE_THICKNESS_OVERFLOW		(BYTE)0x23  // thickness: Signal Overflow
#define DOCTYPE_THICKNESS_GENERIC_ERR	(BYTE)0x26  // thickness: 
#define DOCTYPE_THICKNESS_GLITCH_ERR	(BYTE)0x25	// thickness: glitch
#define DOCTYPE_THICKNESS_NO_ANW_ERR	(BYTE)0x2E	// thickness: no response
#define DOCTYPE_THICKNESS_SIGNERR		(BYTE)0x2F  // thickness: Unknown signal error
	// 0x3x: accepted
#define DOCTYPE_CHECK					(BYTE)0x30  // checks
#define DOCTYPE_1						(BYTE)0x31  // 1
#define DOCTYPE_2						(BYTE)0x32  // 2
#define DOCTYPE_5						(BYTE)0x33  // 5
#define DOCTYPE_10						(BYTE)0x34  // 10
#define DOCTYPE_20						(BYTE)0x35  // 20
#define DOCTYPE_50						(BYTE)0x36  // 50
#define DOCTYPE_100 					(BYTE)0x37  // 100
#define DOCTYPE_200 					(BYTE)0x38  // 200
#define DOCTYPE_500 					(BYTE)0x39  // 500
#define DOCTYPE_1000					(BYTE)0x3A  // 1000
	// 0x4x: errors during visible (gray) recognition
#define VIS_SUSPECT 					(BYTE)0x40  // neither near nor far from a template
#define VIS_REJECTED					(BYTE)0x41  // unknown
	// 0x5x: errors during IR recognition
#define IR_SUSPECT						(BYTE)0x50  // neither near nor far from a template
#define IR_REJECTED 					(BYTE)0x51  // unknown
	// 0x6x: magnetic check
#define MG_CHECK_FAILED 				(BYTE)0x60
	// 0x7x: UV check
#define UV1_CHECK_FAILED				(BYTE)0x70
#define UV2_CHECK_FAILED				(BYTE)0x71
	// 0x8x: pre-fitness errors
#define DOC_BAD_LOCALIZATION			(BYTE)0x80
#define DOC_BAD_DIMENSIONS				(BYTE)0x81  // before classification
#define DOC_UNEXPECTED_DIM				(BYTE)0x82  // after classification
#define DOC_BAD_POSITION				(BYTE)0x83
#define DOC_BAD_SHAPE					(BYTE)0x84
#define DOC_BLOBS_PRESENT				(BYTE)0x85
	// 0x9x: software errors
#define INTERNAL_ERROR					(BYTE)0x90  // internal dll error
#define PARAMETERS_ERROR				(BYTE)0x91  // invalid function arguments
#define DOCTYPE_COUNTERDATA_FRONT_ERR	(BYTE)0xDA  // reserved
#define DOCTYPE_COUNTERDATA_REAR_ERR	(BYTE)0xDB  // reserved
#define DOCTYPE_COUNTERDATA_MAG_ERR		(BYTE)0xDC  // reserved
#define DOCTYPE_COUNTERDATA_UV_ERR		(BYTE)0xDD  // reserved
#define DOCTYPE_COUNTERDATA_OK			(BYTE)0xDE	// reserved
#define DOCTYPE_OK						(BYTE)0xDF	// reserved


// 
// status values (StatusByte[0])
// 
#define SK_NOERROR						0x00
#define SK_BUSY							0x02
#define SK_BADVALUE						0xFF


// 
// reply codes
//
 
#define NS_OKAY							0

// warnings
//#define NS_WRN_FEEDER_EMPTY			1
//#define NS_DOC_THICKNESS				5
#define NS_SCANNER_F_NOT_CONNECTED		8
#define NS_SCANNER_R_NOT_CONNECTED		9

// errors
#define NS_SUSPECT_BOX_FULL				(BYTE)0xF4
#define NS_REJECT_BOX_FULL				(BYTE)0xF5
#define NS_REJECT_BOX_NOTEMPTY			(BYTE)0xF6
#define NS_ESCROW_BOX_NOTEMPTY			(BYTE)0xF7
#define NS_ESCROW_BOX_FULL				(BYTE)0xF8

#define OPTIC_RET_FPGA_ERR				0x20
#define OPTIC_RET_AD_ERR				0x21
#define OPTIC_RET_SRAM_ERR				0x22
#define THICKNESS_RET_TIMEOUT			0x23
#define UV_MAGNETIC_RET_TIMEOUT			0x24

#define RS232_OKAY						NS_OKAY
#define RS232_SEND_ERR					0x31
#define RS232_RECEIVE_ERR				0x32 
#define RS232_COMM_ERR					0x33
#define RS232_OPEN_ERR					0x34
#define RS232_CLOSE_ERR					0x35
#define RS232_GENERIC_ERR 				0x36
#define RS232_NACK_ERR					0x37
//#define RS232_DMA_TIMEOUT_ERR			0x38

// MOTORS retcodes
#define MOT_TX_CAN_ERROR				0x40
#define MOT_RX_CAN_TRANSP_ERROR			0x41
#define MOT_RX_CAN_DRAWER_ERROR			0x42
#define MOT_FRONTDOOR_OPEN_WRN			0x43
#define MOT_REARDOOR_OPEN_WRN			0x44
#define MOT_NO_ESCROWBOX_WRN			0x45
#define MOT_PHOTO_REJ_SX_WRN			0x46
#define MOT_PHOTO_REJ_DX_WRN			0x47
#define MOT_PHOTO_START_SX_WRN			0x48
#define MOT_PHOTO_START_DX_WRN			0x49
#define MOT_FEED_ANOMALY				0x4A

#define MOT_SYNC_ERR					0x6D
#define MOT_PRESSA_ERR					0x6E
#define MOT_SEQ_VALIDAZIONE_ERR			0x6F
#define MOT_FEEDER_EMPTY				0x70
#define NS_WRN_FEEDER_EMPTY				MOT_FEEDER_EMPTY
#define MOT_REJECT_TIMEOUT				0x71
#define MOT_BOURRAGE_PHOTO_START		0x72
#define MOT_BOURRAGE_PHOTO_DISCARD		0x73
#define MOT_PHOTO_CALIB_ERROR			0x74
#define MOT_DMA_COMMAND_ERR				0x75
#define MOT_SKEW_ERROR					0x76
#define MOT_DISPENSE_ERROR				0x77
#define MOT_DEPOSIT_ERROR				0x78
#define MOT_DEPOSIT_ANOMALY				0x79
#define MOT_DEPOSIT_NO_DOC				0x7A
#define MOT_FEED_ERR					0x7B
#define MOT_ESCROW_ERROR				0x7C
#define MOT_TRANSPORT_ERR				0x7D
#define MOT_STACKER_ERR					0x7E
#define MOT_TRANSPORT_V_ERR				0x7F

// trasporto cassetti
#define CASS_CAN_TX						0x80
#define CASS_CAN_RX						0x81
#define CASS_PHOTO_CALIB_ERR			0x82
#define CASS_NOT_PRESENT				0x83
#define CASS_JAM_SYNC					0x84
#define CASS_JAM_SKEW1					0x85
#define CASS_JAM_SKEW2					0x86
#define CASS_JAM_IN_1					0x87
#define CASS_JAM_IN_2					0x88
#define CASS_JAM_IN_3					0x89
#define CASS_JAM_IN_4					0x8A
#define CASS_MICRO_SAFE_OPEN			0x8B
#define CASS_DEPOSIT_ERROR				0x8C
#define CASS_EEPROM_ERROR				0x8D
#define CASS_MOT_TRANSPORT_ERR			0x8E

#define NS_BAD_COMMAND					0x90
#define NS_BAD_PARAMETER				0x91
#define NS_COMMAND_ERROR				0x93

// cassetti
#define CASS_MOTOR1_ERR					0xA0
#define CASS_MOTOR2_ERR					0xA1
#define CASS_MOTOR3_ERR					0xA2
#define CASS_MOTOR4_ERR					0xA3
#define CASS_1_NO						0xA5
#define CASS_2_NO						0xA6
#define CASS_3_NO						0xA7
#define CASS_4_NO						0xA8
#define CASS_PHOTO1_ERR					0xA9
#define CASS_PHOTO2_ERR					0xAA
#define CASS_PHOTO3_ERR					0xAB
#define CASS_PHOTO4_ERR					0xAC
#define CASS_1_DOWN						0xAD
#define CASS_2_DOWN						0xAE
#define CASS_3_DOWN						0xAF
#define CASS_4_DOWN						0xB0
#define CASS_1_UP						0xB1
#define CASS_2_UP						0xB2
#define CASS_3_UP						0xB3
#define CASS_4_UP						0xB4
#define CASS_NO_PWM_C					0xB5
#define CASS_NO_PWM_C1					0xB6
#define CASS_NO_PWM_C2					0xB7
#define CASS_NO_PWM_C3					0xB8
#define CASS_NO_PWM_C4					0xB9
#define CASS_PWM_CALIB_ERR				0xBA
#define CASS_NO_RESET					0xBB
#define CASS_NO_RESET1					0xBC
#define CASS_NO_RESET2					0xBD
#define CASS_NO_RESET3					0xBE
#define CASS_NO_RESET4					0xBF
#define CASS_NO_ENAB1					0xC0
#define CASS_NO_ENAB2					0xC1
#define CASS_NO_ENAB3					0xC2
#define CASS_NO_ENAB4					0xC3
#define CASS_NO_SPACE					0xC4
#define CASS_1_BUSY						0xC5			
#define CASS_2_BUSY						0xC6			
#define CASS_3_BUSY						0xC7
#define CASS_4_BUSY						0xC8

//sacco
#define BAG_JAM_SYNC_ERR				0x94
#define BAG_JAM_ING1_ERR				0x95
#define BAG_JAM_ING2_ERR				0x96
#define BAG_JAM_ING3_ERR				0x97
#define BAG_JAM_ING4_ERR				0x98
#define BAG_JAM_ING0_ERR				0x99
#define BAG_OPEN_ERR					0x9A
#define BAG_TRANSPORT_MOTOR_ERR			0x9B
#define BAG_NOT_INSERTED				0x9C
#define BAG_CLOSED_ERR					0x9D





//thickness
#define TICKNESS_TARATURA_ERR			0x10
#define TICKNESS_TIMEOUT_SPI_ERR		0x11
#define TICKNESS_FLASH_ERR				0x12
#define TICKNESS_NOT_PRESENT			0x1F

// USBLOW return codes 
#ifndef USBLOW_RET_BASE
#define USBLOW_RET_BASE					200
enum {	USBLOW_RET_OK ,				//	0 (zero is by default) 
		USBLOW_RET_NODEVICE = (USBLOW_RET_BASE+1) ,	// 201
		USBLOW_RET_NOOPEN ,			//	202
		USBLOW_RET_FILEOPEN_ERR ,	//	203
		USBLOW_RET_FILEREAD_ERR ,	//	204
		USBLOW_RET_FILEWRITE_ERR ,	//	205
		USBLOW_RET_COMMAND_FAILED ,	//	206
		USBLOW_RET_FILE_HEX_ERR ,	//	207
		USBLOW_RET_PARAM_ERR ,		//	208
		USBLOW_RET_CMD_ERR ,		//	209
//  ... add here new codes ...
		USBLOW_RET_LAST 
}; 
#ifndef USBLOW_RET_RX_TIMEOUT
#define USBLOW_RET_RX_TIMEOUT			-116
#endif
#endif//USBLOW_RET_BASE


// USB2USB return codes (reserved)
#ifndef USB2USB_RET_BASE
#define USB2USB_RET_BASE				300
enum {	USB2USB_RET_OK ,			//	0 (zero is by default) 
		USB2USB_RET_NODEVICE = (USB2USB_RET_BASE+1) ,	// 301
		USB2USB_RET_NOOPEN ,		//	302
		USB2USB_RET_FILEOPEN_ERR ,	//	303
		USB2USB_RET_FILEREAD_ERR ,	//	304
		USB2USB_RET_FILEWRITE_ERR ,	//	305
		USB2USB_RET_COMMAND_FAILED ,//	306
		USB2USB_RET_FILE_HEX_ERR ,	//	307
		USB2USB_RET_PARAM_ERR ,		//	308
		USB2USB_RET_CMD_ERR ,		//	309
//  ... add here new codes ...
		USB2USB_RET_LAST 
}; 
#endif//USB2USB_RET_BASE


#ifndef LAN_RET_OPEN_ERR
#define LAN_RET_OPEN_ERR				400
#define LAN_RET_CLOSE_ERR				401
#define LAN_RET_WRITE_ERR				402
#define LAN_RET_READ_ERR				403
#define LAN_RET_READ_NODATA				404
#endif

#define NS_SYSTEM_ERROR 				-501
#define NS_USB_ERROR					-502
#define NS_PERIFNOTFOUND				-503
#define NS_MEMORY_ERROR					-504
#define NS_NO_DOC						-505
//#define NS_CLIENT_CONNECT_FAIL		-506
#define NS_SDRAM_OVERFLOW				-506
#define NS_READ_DMA_ERR					-507
//#define NS_SYNC_FILM_ERROR			-508
//#define NS_TARGET_BUSY				-508
//#define NS_INVALIDCMD					-509
//#define NS_INVALIDPARAM				-510
//#define NS_EXECCMD					-511
#define NS_JPEG_ERROR					-512
#define NS_CMDSEQUENCEERROR 			-513
#define NS_USER_ABORT_REQ				-514
#define NS_NO_SERIAL_NUMBER				-516
//#define NS_INVALID_HANDLE				-516
#define NS_NO_LIBRARY_LOAD				-517
#define NS_BMP_ERROR					-518
#define NS_TIFF_ERROR					-519
//#define NS_IMAGE_NO_FILMED			-521
#define NS_IMAGE_TOO_SHORT				-522
//#define NS_BARCODE_ERROR				-525
//#define NS_INVALID_BARCODE_TYPE 		-529
#define NS_OPEN_NOT_DONE				-530
#define NS_INVALID_CLEARBLACK			-532
//#define NS_INVALID_SIDE 				-533
#define NS_MISSING_IMAGE				-534
//#define NS_INVALID_TYPE 				-535
#define NS_INVALID_SAVEMODE 			-536
//#define NS_INVALID_PAGE_NUMBER		-537
#define NS_INVALID_VALIDATE 			-541
#define NS_INVALID_CODELINE_TYPE		-542
#define NS_INVALID_SCANMODE 			-544
//#define NS_DOCTYPE_VALIDATE_TIMEOUT	-545
//#define NS_INVALID_FEEDER				-546
//#define NS_INVALID_SORTER				-547
#define NS_MISSING_FILENAME 			-549
//#define NS_INVALID_QUALITY			-550
#define NS_INVALID_FILEFORMAT			-551
//#define NS_INVALID_COORDINATE			-552
//#define NS_MISSING_HANDLE_VARIABLE	-553
//#define NS_INVALID_POLO_FILTER		-554
//#define NS_INVALID_SIZEH_VALUE		-556
//#define NS_INVALID_FORMAT				-557
//#define NS_INVALID_CMD_HISTORY		-560
//#define NS_MISSING_BUFFER_HISTORY		-561
#define NS_OPEN_FILE_ERROR				-563
//#define NS_INVALID_METHOD				-565
//#define NS_CALIBRATION_FAILED			-566
//#define NS_CALIBRATION_DATA_ERR		-567
#define NS_INVALID_UNIT 				-568
#define NS_NO_MAGNETIC_CALIB			-569
#define NS_MAGNETIC_HEAD_KO				-570
#define NS_INVALID_NUMDOC				-572
//#define NS_ILLEGAL_REQUEST			-573
#define NS_NO_MAGNETIC_N_CHANNEL		-574
//#define NS_INVALID_DEGREE				-577
#define NS_ROTATE_ERROR 				-578
//#define NS_INVALID_SIDETOPRINT		-584
//#define NS_DOUBLE_LEAF_ERROR			-585
#define NS_INVALID_RESET_TYPE			-587
#define NS_INVALID_CALLBACK_SET 		-588
//#define NS_IMAGE_NOT_200_DPI			-589
//#define NS_DOWNLOAD_ERROR				-590
//#define NS_INVALID_SORT_ON_CHOICE		-591
//#define NS_HOPPER_COUNTER_ERR			-592

#define NS_CALIB_DLLPRM_ERR				-600
#define NS_CALIB_FILECOMP_ERR			-601
#define NS_CALIB_DATACOMP_ERR			-602
#define NS_CALIB_FILEPARAM_ERR			-603
#define NS_CALIB_SYSTEM_ERR				-604
#define NS_CALIB_OFFSET_ERR				-605
#define NS_CALIB_PWM_ERR				-606
#define NS_CALIB_GAIN_ERR				-607
#define NS_CALIB_COEFF_ERR				-608
#define NS_CALIB_PWM_R_ERR				-609
#define NS_CALIB_PWM_G_ERR				-610
#define NS_CALIB_PWM_IR_ERR				-611

#define NS_DECODE_FONT_NOT_PRESENT		-1101
#define NS_DECODE_INVALID_COORDINATE	-1102
#define NS_DECODE_INVALID_OPTION		-1103
#define NS_DECODE_INVALID_CODELINE_TYPE	-1104
#define NS_DECODE_SYSTEM_ERROR			-1105
#define NS_DECODE_DATA_TRUNC			-1106
#define NS_DECODE_INVALID_BITMAP		-1107
#define NS_DECODE_ILLEGAL_USE			-1108

#define NS_BARCODE_GENERIC_ERROR		-1201
#define NS_BARCODE_NOT_DECODABLE		-1202
#define NS_BARCODE_OPENFILE_ERROR		-1203
#define NS_BARCODE_READBMP_ERROR		-1204
#define NS_BARCODE_MEMORY_ERROR			-1205
#define NS_BARCODE_START_NOTFOUND		-1206
#define NS_BARCODE_STOP_NOTFOUND		-1207



// 
// structures
// 

#define HWVERSION_LEN					30 
#define FWVERSION_LEN					30 
#define TRANSPORTVERSION_LEN			30
#define THICKNESSVERSION_LEN			30
#define UVMAGVERSION_LEN				30

typedef struct _st_identify
{
	OUT	char	HwVersion[HWVERSION_LEN+1];
	OUT	char	FwVersion[FWVERSION_LEN+1];
	OUT	char	TrVersion[TRANSPORTVERSION_LEN+1];
	OUT	char	ThicknessVersion[THICKNESSVERSION_LEN+1];
//Mir...	OUT	char	UV_MagVersion[UVMAGVERSION_LEN+1];

}ST_INDENTIFY, *LPST_INDENTIFY;


typedef struct _st_identify_usb2usb
{
	OUT	char	usb2usb_FwExtVersion[FWVERSION_LEN+1];
	OUT	char	usb2usb_FwIntVersion[FWVERSION_LEN+1];
	OUT	char	usb2usb_HwVersion[HWVERSION_LEN+1];

}ST_INDENTIFY_USB2USB, *LPST_INDENTIFY_USB2USB;


#define DLLVERSION_LEN					64
typedef struct _st_dllversion
{
	OUT	char	dllversion[DLLVERSION_LEN];
	OUT	char	doc_recognition[DLLVERSION_LEN];

}ST_DLLVERSION, *LPST_DLLVERSION;


typedef struct _st_status
{
	OUT	BYTE	StatusByte[3];				// status, s1, s2
	OUT	BYTE	rs232_photo[4];				// BIT_STATUS_FEEDER, BIT_STATUS_ESCROW ...
	OUT	BYTE	rs232_error;				// motorboard error status 
	OUT	DWORD	rs232_totaldoc;				// number of documents scanned last time 
	OUT	BYTE	DocsInfo[MAXDOC_TOTAL_400];		// last scanned documents sequence (DOCTYPE_CHECK, DOCTYPE_1, ...)
	OUT	BYTE	flag_boxfull;
	OUT	double	DocAngleHor[MAXDOC_TOTAL_400];	// return the horizontal angle

}ST_STATUS, *LPST_STATUS;


#if defined(__FI2D_PC_INTERNO__) || defined(__FI2D_PC_ESTERNO__)
typedef struct _st_sta_usb2usb
{
	OUT	BYTE	StatusByte[2];

}ST_STATUS_USB2USB, *LPST_STATUS_USB2USB;
#endif

typedef struct _st_autodochandle
{
	IN	BYTE	Validate;					// require the document validation (BIT_VALIDATE_BANKNOTE...)
	IN	char	Side;						// SIDE_FRONT_IMAGE  (...)
	IN	short	ScanSpeed;					// SCAN_MODE_SPEED0  (...)
	IN	short	ClearBlack;					// require border cleaning (NO_CLEAR_BLACK ...)
	IN	char	NumDocument[3];				// number of required documents in ASCII format (SCAN_ALL_DOCS=999 for all)
	IN	short	SaveImage;					// require save to file (IMAGE_SAVETO_MEMORY ...)
	IN	char	*SaveImageFolder;			// string format ( ex: "c:\myapp\images" )
	IN	char	*SaveImageBasename;			// string format ( ex: "Image_" )
	IN	short	Unit;						// unused (0)
	IN	float	pos_x;						// unused (0)
	IN	float	pos_y;						// unused (0)
	IN	float	sizeW;						// unused (0)
	IN	float	sizeH;						// unused (0)
	IN	short	FileFormat;					// '1' = SAVE_BMP
	IN	int		Quality;					// used only for JPEG format
	IN	int		SaveMode;					// used only for TIFF format (SAVE_OVERWRITE ...)
	IN	int		PageNumber;					// fixed = 1 (used only for TIFF)
	IN	char	DocType;					// type of document to scan (DOCTYPE_SCAN_BANKNOTE ...)
	IN	BYTE	ScanSource;					// data source: '0'=counters (DEBUG) - '1'=scanner (SCAN_SOURCE_SCANNER ...)
	IN	BYTE	ScanExtraMode;				// extra scan mode (SCAN_EXTRAMODE_NONE - SCAN_EXTRAMODE_UV ...) 
	IN	BYTE	ScanMagneticMode;			// magnetic scan request (BIT_SCAN_MAGNETIC_NO ...)
	IN	BYTE	ScanSourceEncoder;			// tmp: '1'=timer (DEBUG) - '0'=encoder (SCAN_SOURCE_ENCODER_YES ...)
	IN	BYTE	ScanSingleChannel;			// MAGNETIC_SINGLE_CHAN_0...

}ST_AUTODOCHANDLE, *LPST_AUTODOCHANDLE;


typedef struct _st_getdocdata
{
	OUT	HANDLE	FrontImage;						// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
	OUT	HANDLE	BackImage; 						// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
	OUT	HANDLE	FrontImage_IR; 					// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
	OUT	HANDLE	BackImage_IR;					// (max. 2MB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
//	OUT	HANDLE	MagneticImage;					// (max. 15KB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
	OUT	LPBYTE	MagneticImage;					// (max. 15KB) image dimensions are returned in the bitmap header (BITMAPINFOHEADER)
	OUT	char	CodelineSW[MAX_CODELINE_LEN];	// codeline read on check document
	OUT	char	CodelineHW[MAX_CODELINE_LEN];	// TODO...
	OUT	BYTE	DocInfo;						// return the document type (DOCTYPE_CHECK, DOCTYPE_1, DOCTYPE_CHECK_NO_MAGNETIC...)
	OUT	ULONG	NrDoc; 							// progressive document number (from 1)
	IN	short	CodelineType;					// select the type of codeline to read (READ_CODELINE_SW_E13B...)
	OUT	char	CodelineSide;					// return the codeline position read on document (CODELINE_ON_FRONT_DOWN...)
	OUT	double	DocAngleHor;					// return the horizontal angle
	OUT	LPBYTE	pBufferUV1;						// return the pointer to the UV1 data
	OUT	LPBYTE	pBufferUV2;						// return the pointer to the UV2 data

}ST_GETDOCDATA, *LPST_GETDOCDATA;



typedef struct _ReadOption
{
	IN	BOOL	PutBlanks;
	IN	char	TypeRead;
}READOPTIONS, *LPREADOPTIONS;



// deposit module----------------------------
#define MAXDOCTYPE_IN_DRAWER		15
#define MAXDOC_IN_DRAWER_DEF		2500
#define DOCTYPE_FINELOG				0x3F
#define TRANSPORT_PHOTO_LEN			5
#define DRAWER_PHOTO_LEN			5

#define SAFE_DRAWER_NOVALUE			'?'
#define SAFE_DRAWER_0				'0'
#define SAFE_DRAWER_1				'1'
#define SAFE_DRAWER_2				'2'
#define SAFE_DRAWER_3				'3'
#define SAFE_DRAWER_4				'4'
#define SAFE_BAG					0x80

typedef struct _st_docdeposit
{
	IN	BYTE	drawer_id;					// box identify (SAFE_DRAWER_1...)
	IN	DWORD	doccount_todo;				// documents to be deposited
	OUT	DWORD	doccount_done;				// documents deposited

}ST_DOCDEPOSIT, *LPST_DOCDEPOSIT;


//typedef struct _st_docdispense			// reserved
//{
//	IN	DWORD	doccount_todo;				// documents to be dispensed
//	OUT	DWORD	doccount_done;				// documents dispensed
//
//}ST_DOCDISPENSE, *LPST_DOCDISPENSE;


#define SAFE_MODULE_DRAWER			'D'
#define SAFE_MODULE_TRANSPORT		'T'
#define DRAWER_VERSION_LEN			40
typedef struct _st_drawer_identify
{
	IN	BYTE	module;						// 'D'=Drawers 'T'=transport
	OUT	char	version_D[DRAWER_VERSION_LEN+1];
	OUT	char	version_T[DRAWER_VERSION_LEN+1];

}ST_DRAWER_INDENTIFY, *LPST_DRAWER_INDENTIFY;


typedef struct _st_drawer_reset
{
	IN	BYTE	drawer_id;					// box identify (SAFE_DRAWER_1...)

}ST_DRAWER_RESET, *LPST_DRAWER_RESET;


typedef struct _st_drawer_status
{
	OUT	BYTE	stat_drawer1;				
	OUT	BYTE	stat_drawer2;				
	OUT	BYTE	stat_drawer3;				
	OUT	BYTE	stat_drawer4;				
	OUT	DWORD	ndoc_drawer1;				// number of documents present in the drawer #1
	OUT	DWORD	ndoc_drawer2;				// number of documents present in the drawer #2
	OUT	DWORD	ndoc_drawer3;				// number of documents present in the drawer #3
	OUT	DWORD	ndoc_drawer4;				// number of documents present in the drawer #4

}ST_DRAWER_STATUS, *LPST_DRAWER_STATUS;
// deposit module----------------------------




typedef struct _st_thickness_status
{
	OUT	int	stat_thickness;

}ST_THICKNESS_STATUS, *LPST_THICKNESS_STATUS;


typedef struct _st_current_counters
{
	OUT	int	doc_inserted_total;
	OUT	int	doc_inserted_last;
	OUT	int	doc_in_escrow_total;
	OUT	int	doc_in_escrow_last;
	OUT	int	doc_in_reject_last;
	OUT	int	doc_in_suspect_last;

}ST_CURRENT_COUNTERS, *LPST_CURRENT_COUNTERS;





#ifdef __cplusplus
extern "C" {
#endif

// public functions
extern int __stdcall NS_GetIdentify(OUT LPST_INDENTIFY p_st_id, OUT LPST_INDENTIFY_USB2USB p_st_id_usb2usb);
extern int __stdcall NS_GetDLLVersion(OUT LPST_DLLVERSION p_st_ver);
extern int __stdcall NS_GetStatus(OUT LPST_STATUS p_st_sta);
extern int __stdcall NS_GetSensorLevel(OUT LPWORD v_fotoStartDX, OUT LPWORD v_fotoStartSX, OUT LPWORD v_fotoScartoDX, OUT LPWORD v_fotoScartoSX);
extern int __stdcall NS_Reset(IN char _ResetType);
extern int __stdcall NS_DocSessionStart(void);
extern int __stdcall NS_DocSessionEnd(void);
extern int __stdcall NS_AutoDocHandle(IN LPST_AUTODOCHANDLE p_st_aut);
extern int __stdcall NS_GetDocData(LPST_GETDOCDATA p_st_gdd);
extern int __stdcall NS_SerialNumber(IN BYTE _setget, char _sernum[6], char _date[6], IN LPSTR _pwd);
extern int __stdcall NS_GetUSBErrorMsg(IN int _errcode, OUT LPSTR _errdesc);
//
extern int __stdcall NS_DepositExec(IN LPST_DOCDEPOSIT p_st_dep);
extern int __stdcall NS_ReleaseBag(void);
extern int __stdcall NS_DrawerGetId(OUT LPST_DRAWER_INDENTIFY p_st_drawer_identify);
extern int __stdcall NS_DrawerReset(IN LPST_DRAWER_RESET p_st_drawer_reset);
extern int __stdcall NS_DrawerGetStatus(OUT LPST_DRAWER_STATUS p_st_drawer_status);
extern int __stdcall NS_DrawerGetStatusEx(IN BYTE _drawer_id, OUT DWORD p_numdoc[MAXDOCTYPE_IN_DRAWER]);
extern int __stdcall NS_DrawerGetStatusLog(IN BYTE _drawer_id, OUT LPBYTE p_log);
extern int __stdcall NS_DrawerGetNumof(IN BYTE _setget, LPBYTE p_numof_drawers, IN LPSTR _pwd);
extern int __stdcall NS_DrawerGetMaxdoc(IN BYTE _setget, LPDWORD p_maxdoc, IN LPSTR _pwd);
extern int __stdcall NS_EnableDepositWhileFeeding(IN BOOL enable);
extern int __stdcall NS_DrawerGetPhoto(OUT BYTE p_transport_photos[TRANSPORT_PHOTO_LEN], OUT BYTE p_drawer_photos[DRAWER_PHOTO_LEN]);
//
extern int __stdcall NS_SaveTrace(IN LPSTR _trace_filename, IN BOOL from_sot);
extern int __stdcall NS_EraseImagesFolder(IN LPSTR _pwd);
extern int __stdcall NS_ThicknessGetStatus(OUT LPST_THICKNESS_STATUS p_st_thickness_status);
extern int __stdcall NS_GetCurrentCounters(OUT LPST_CURRENT_COUNTERS p_st_current_counters);

// reserved functions
#if defined(__FI2D_PC_INTERNO__) || defined(__FI2D_PC_ESTERNO__)
extern int __stdcall NS_Connect(IN BOOL _flag_load_dll, IN char _connection_mode, IN char *p_lan_ipaddress);
extern int __stdcall NS_Disconnect(void);
extern int __stdcall NS_GetStatus_usb2usb(OUT LPST_STATUS_USB2USB p_st_sta_usb2usb);
#endif

#ifdef __cplusplus
}
#endif

#endif//_FI2DDLL_H_
