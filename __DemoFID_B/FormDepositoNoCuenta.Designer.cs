﻿namespace __DemoFID_B
{
    partial class FormDepositoNoCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDepositoNoCuenta));
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c_BackSpace = new System.Windows.Forms.Button();
            this.c_NoCuenta = new System.Windows.Forms.TextBox();
            this.c_BotonAceptar = new System.Windows.Forms.Button();
            this.c_Boton0 = new System.Windows.Forms.Button();
            this.c_Boton9 = new System.Windows.Forms.Button();
            this.c_Boton8 = new System.Windows.Forms.Button();
            this.c_Boton6 = new System.Windows.Forms.Button();
            this.c_Boton5 = new System.Windows.Forms.Button();
            this.c_BotonCancelar = new System.Windows.Forms.Button();
            this.c_Boton7 = new System.Windows.Forms.Button();
            this.c_Boton3 = new System.Windows.Forms.Button();
            this.c_Boton2 = new System.Windows.Forms.Button();
            this.c_Boton4 = new System.Windows.Forms.Button();
            this.c_Boton1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.c_BackSpace);
            this.panel1.Controls.Add(this.c_NoCuenta);
            this.panel1.Controls.Add(this.c_BotonAceptar);
            this.panel1.Controls.Add(this.c_Boton0);
            this.panel1.Controls.Add(this.c_Boton9);
            this.panel1.Controls.Add(this.c_Boton8);
            this.panel1.Controls.Add(this.c_Boton6);
            this.panel1.Controls.Add(this.c_Boton5);
            this.panel1.Controls.Add(this.c_BotonCancelar);
            this.panel1.Controls.Add(this.c_Boton7);
            this.panel1.Controls.Add(this.c_Boton3);
            this.panel1.Controls.Add(this.c_Boton2);
            this.panel1.Controls.Add(this.c_Boton4);
            this.panel1.Controls.Add(this.c_Boton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 550);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 25);
            this.label1.TabIndex = 53;
            this.label1.Text = "Escriba el número de Cuenta:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(247, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(303, 29);
            this.label3.TabIndex = 52;
            this.label3.Text = "DEPÓSITO DE EFECTIVO";
            // 
            // c_BackSpace
            // 
            this.c_BackSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BackSpace.Image = global::@__DemoFID_B.Properties.Resources.back;
            this.c_BackSpace.Location = new System.Drawing.Point(282, 423);
            this.c_BackSpace.Name = "c_BackSpace";
            this.c_BackSpace.Size = new System.Drawing.Size(75, 53);
            this.c_BackSpace.TabIndex = 37;
            this.c_BackSpace.UseVisualStyleBackColor = true;
            this.c_BackSpace.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_NoCuenta
            // 
            this.c_NoCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_NoCuenta.Location = new System.Drawing.Point(338, 204);
            this.c_NoCuenta.Name = "c_NoCuenta";
            this.c_NoCuenta.ReadOnly = true;
            this.c_NoCuenta.Size = new System.Drawing.Size(181, 26);
            this.c_NoCuenta.TabIndex = 36;
            // 
            // c_BotonAceptar
            // 
            this.c_BotonAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonAceptar.Image = global::@__DemoFID_B.Properties.Resources.tick;
            this.c_BotonAceptar.Location = new System.Drawing.Point(444, 423);
            this.c_BotonAceptar.Name = "c_BotonAceptar";
            this.c_BotonAceptar.Size = new System.Drawing.Size(75, 53);
            this.c_BotonAceptar.TabIndex = 35;
            this.c_BotonAceptar.UseVisualStyleBackColor = true;
            this.c_BotonAceptar.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton0
            // 
            this.c_Boton0.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton0.Location = new System.Drawing.Point(363, 423);
            this.c_Boton0.Name = "c_Boton0";
            this.c_Boton0.Size = new System.Drawing.Size(75, 53);
            this.c_Boton0.TabIndex = 34;
            this.c_Boton0.Text = "0";
            this.c_Boton0.UseVisualStyleBackColor = true;
            this.c_Boton0.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton9
            // 
            this.c_Boton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton9.Location = new System.Drawing.Point(444, 364);
            this.c_Boton9.Name = "c_Boton9";
            this.c_Boton9.Size = new System.Drawing.Size(75, 53);
            this.c_Boton9.TabIndex = 33;
            this.c_Boton9.Text = "9";
            this.c_Boton9.UseVisualStyleBackColor = true;
            this.c_Boton9.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton8
            // 
            this.c_Boton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton8.Location = new System.Drawing.Point(363, 364);
            this.c_Boton8.Name = "c_Boton8";
            this.c_Boton8.Size = new System.Drawing.Size(75, 53);
            this.c_Boton8.TabIndex = 32;
            this.c_Boton8.Text = "8";
            this.c_Boton8.UseVisualStyleBackColor = true;
            this.c_Boton8.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton6
            // 
            this.c_Boton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton6.Location = new System.Drawing.Point(444, 305);
            this.c_Boton6.Name = "c_Boton6";
            this.c_Boton6.Size = new System.Drawing.Size(75, 53);
            this.c_Boton6.TabIndex = 30;
            this.c_Boton6.Text = "6";
            this.c_Boton6.UseVisualStyleBackColor = true;
            this.c_Boton6.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton5
            // 
            this.c_Boton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton5.Location = new System.Drawing.Point(363, 305);
            this.c_Boton5.Name = "c_Boton5";
            this.c_Boton5.Size = new System.Drawing.Size(75, 53);
            this.c_Boton5.TabIndex = 31;
            this.c_Boton5.Text = "5";
            this.c_Boton5.UseVisualStyleBackColor = true;
            this.c_Boton5.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_BotonCancelar
            // 
            this.c_BotonCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BotonCancelar.Image = global::@__DemoFID_B.Properties.Resources.block;
            this.c_BotonCancelar.Location = new System.Drawing.Point(713, 497);
            this.c_BotonCancelar.Name = "c_BotonCancelar";
            this.c_BotonCancelar.Size = new System.Drawing.Size(75, 53);
            this.c_BotonCancelar.TabIndex = 29;
            this.c_BotonCancelar.UseVisualStyleBackColor = true;
            this.c_BotonCancelar.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton7
            // 
            this.c_Boton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton7.Location = new System.Drawing.Point(282, 364);
            this.c_Boton7.Name = "c_Boton7";
            this.c_Boton7.Size = new System.Drawing.Size(75, 53);
            this.c_Boton7.TabIndex = 28;
            this.c_Boton7.Text = "7";
            this.c_Boton7.UseVisualStyleBackColor = true;
            this.c_Boton7.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton3
            // 
            this.c_Boton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton3.Location = new System.Drawing.Point(444, 246);
            this.c_Boton3.Name = "c_Boton3";
            this.c_Boton3.Size = new System.Drawing.Size(75, 53);
            this.c_Boton3.TabIndex = 27;
            this.c_Boton3.Text = "3";
            this.c_Boton3.UseVisualStyleBackColor = true;
            this.c_Boton3.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton2
            // 
            this.c_Boton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton2.Location = new System.Drawing.Point(363, 246);
            this.c_Boton2.Name = "c_Boton2";
            this.c_Boton2.Size = new System.Drawing.Size(75, 53);
            this.c_Boton2.TabIndex = 26;
            this.c_Boton2.Text = "2";
            this.c_Boton2.UseVisualStyleBackColor = true;
            this.c_Boton2.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton4
            // 
            this.c_Boton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton4.Location = new System.Drawing.Point(282, 305);
            this.c_Boton4.Name = "c_Boton4";
            this.c_Boton4.Size = new System.Drawing.Size(75, 53);
            this.c_Boton4.TabIndex = 25;
            this.c_Boton4.Text = "4";
            this.c_Boton4.UseVisualStyleBackColor = true;
            this.c_Boton4.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // c_Boton1
            // 
            this.c_Boton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Boton1.Location = new System.Drawing.Point(282, 246);
            this.c_Boton1.Name = "c_Boton1";
            this.c_Boton1.Size = new System.Drawing.Size(75, 53);
            this.c_Boton1.TabIndex = 24;
            this.c_Boton1.Text = "1";
            this.c_Boton1.UseVisualStyleBackColor = true;
            this.c_Boton1.Click += new System.EventHandler(this.OprimirTecla);
            // 
            // FormDepositoNoCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uc_PanelInferior1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDepositoNoCuenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDepositoNoCuenta";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private uc_PanelInferior uc_PanelInferior1=null;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox c_NoCuenta;
        private System.Windows.Forms.Button c_BotonAceptar;
        private System.Windows.Forms.Button c_Boton0;
        private System.Windows.Forms.Button c_Boton9;
        private System.Windows.Forms.Button c_Boton8;
        private System.Windows.Forms.Button c_Boton6;
        private System.Windows.Forms.Button c_Boton5;
        private System.Windows.Forms.Button c_BotonCancelar;
        private System.Windows.Forms.Button c_Boton7;
        private System.Windows.Forms.Button c_Boton3;
        private System.Windows.Forms.Button c_Boton2;
        private System.Windows.Forms.Button c_Boton4;
        private System.Windows.Forms.Button c_Boton1;
        private System.Windows.Forms.Button c_BackSpace;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}