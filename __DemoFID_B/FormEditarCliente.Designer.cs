﻿namespace __DemoFID_B
{
    partial class FormEditarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.c_TabUsuarios = new System.Windows.Forms.TabPage();
            this.c_Usuarios = new System.Windows.Forms.DataGridView();
            this.IdUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Activo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.c_BAgregarUsuario = new System.Windows.Forms.ToolStripButton();
            this.c_BEditarUsuario = new System.Windows.Forms.ToolStripButton();
            this.c_TabCuentas = new System.Windows.Forms.TabPage();
            this.c_Cuentas = new System.Windows.Forms.DataGridView();
            this.Alias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdCuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Banco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Activa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AceptaReferencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.c_BAgregarCuenta = new System.Windows.Forms.ToolStripButton();
            this.c_BEditarCuenta = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.c_Telefono = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_Nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Clave = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.c_TabUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Usuarios)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.c_TabCuentas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Cuentas)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.c_TabUsuarios);
            this.tabControl1.Controls.Add(this.c_TabCuentas);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 221);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(789, 327);
            this.tabControl1.TabIndex = 18;
            // 
            // c_TabUsuarios
            // 
            this.c_TabUsuarios.Controls.Add(this.c_Usuarios);
            this.c_TabUsuarios.Controls.Add(this.toolStrip1);
            this.c_TabUsuarios.Location = new System.Drawing.Point(4, 29);
            this.c_TabUsuarios.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_TabUsuarios.Name = "c_TabUsuarios";
            this.c_TabUsuarios.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_TabUsuarios.Size = new System.Drawing.Size(781, 294);
            this.c_TabUsuarios.TabIndex = 0;
            this.c_TabUsuarios.Text = "Operadores";
            this.c_TabUsuarios.UseVisualStyleBackColor = true;
            // 
            // c_Usuarios
            // 
            this.c_Usuarios.AllowUserToAddRows = false;
            this.c_Usuarios.AllowUserToDeleteRows = false;
            this.c_Usuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.c_Usuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_Usuarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdUsuario,
            this.Nombre,
            this.Referencia,
            this.Activo});
            this.c_Usuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c_Usuarios.Location = new System.Drawing.Point(2, 27);
            this.c_Usuarios.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Usuarios.Name = "c_Usuarios";
            this.c_Usuarios.ReadOnly = true;
            this.c_Usuarios.RowTemplate.Height = 31;
            this.c_Usuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.c_Usuarios.Size = new System.Drawing.Size(777, 265);
            this.c_Usuarios.TabIndex = 3;
            // 
            // IdUsuario
            // 
            this.IdUsuario.DataPropertyName = "IdUsuario";
            this.IdUsuario.HeaderText = "Clave";
            this.IdUsuario.Name = "IdUsuario";
            this.IdUsuario.ReadOnly = true;
            this.IdUsuario.Width = 73;
            // 
            // Nombre
            // 
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 90;
            // 
            // Referencia
            // 
            this.Referencia.DataPropertyName = "Referencia";
            this.Referencia.HeaderText = "Referencia";
            this.Referencia.Name = "Referencia";
            this.Referencia.ReadOnly = true;
            this.Referencia.Width = 112;
            // 
            // Activo
            // 
            this.Activo.DataPropertyName = "Activo";
            this.Activo.HeaderText = "Activo";
            this.Activo.Name = "Activo";
            this.Activo.ReadOnly = true;
            this.Activo.Width = 77;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.c_BAgregarUsuario,
            this.c_BEditarUsuario});
            this.toolStrip1.Location = new System.Drawing.Point(2, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(777, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // c_BAgregarUsuario
            // 
            this.c_BAgregarUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.c_BAgregarUsuario.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.c_BAgregarUsuario.Name = "c_BAgregarUsuario";
            this.c_BAgregarUsuario.Size = new System.Drawing.Size(106, 22);
            this.c_BAgregarUsuario.Text = "Agregar Operador";
            this.c_BAgregarUsuario.Click += new System.EventHandler(this.c_BAgregarUsuario_Click);
            // 
            // c_BEditarUsuario
            // 
            this.c_BEditarUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.c_BEditarUsuario.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.c_BEditarUsuario.Name = "c_BEditarUsuario";
            this.c_BEditarUsuario.Size = new System.Drawing.Size(94, 22);
            this.c_BEditarUsuario.Text = "Editar Operador";
            this.c_BEditarUsuario.Click += new System.EventHandler(this.c_BEditarUsuario_Click);
            // 
            // c_TabCuentas
            // 
            this.c_TabCuentas.Controls.Add(this.c_Cuentas);
            this.c_TabCuentas.Controls.Add(this.toolStrip2);
            this.c_TabCuentas.Location = new System.Drawing.Point(4, 29);
            this.c_TabCuentas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_TabCuentas.Name = "c_TabCuentas";
            this.c_TabCuentas.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_TabCuentas.Size = new System.Drawing.Size(781, 294);
            this.c_TabCuentas.TabIndex = 1;
            this.c_TabCuentas.Text = "Cuentas";
            this.c_TabCuentas.UseVisualStyleBackColor = true;
            // 
            // c_Cuentas
            // 
            this.c_Cuentas.AllowUserToAddRows = false;
            this.c_Cuentas.AllowUserToDeleteRows = false;
            this.c_Cuentas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.c_Cuentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.c_Cuentas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Alias,
            this.IdCuenta,
            this.Banco,
            this.Cuenta,
            this.Activa,
            this.AceptaReferencia});
            this.c_Cuentas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c_Cuentas.Location = new System.Drawing.Point(2, 27);
            this.c_Cuentas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Cuentas.Name = "c_Cuentas";
            this.c_Cuentas.ReadOnly = true;
            this.c_Cuentas.RowTemplate.Height = 31;
            this.c_Cuentas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.c_Cuentas.Size = new System.Drawing.Size(777, 265);
            this.c_Cuentas.TabIndex = 5;
            // 
            // Alias
            // 
            this.Alias.DataPropertyName = "Alias";
            this.Alias.HeaderText = "Alias";
            this.Alias.Name = "Alias";
            this.Alias.ReadOnly = true;
            this.Alias.Width = 68;
            // 
            // IdCuenta
            // 
            this.IdCuenta.DataPropertyName = "IdCuenta";
            this.IdCuenta.HeaderText = "Column1";
            this.IdCuenta.Name = "IdCuenta";
            this.IdCuenta.ReadOnly = true;
            this.IdCuenta.Visible = false;
            this.IdCuenta.Width = 120;
            // 
            // Banco
            // 
            this.Banco.DataPropertyName = "Banco";
            this.Banco.HeaderText = "Banco";
            this.Banco.Name = "Banco";
            this.Banco.ReadOnly = true;
            this.Banco.Width = 80;
            // 
            // Cuenta
            // 
            this.Cuenta.DataPropertyName = "Cuenta";
            this.Cuenta.HeaderText = "Cuenta";
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.ReadOnly = true;
            this.Cuenta.Width = 86;
            // 
            // Activa
            // 
            this.Activa.DataPropertyName = "Activa";
            this.Activa.HeaderText = "Activa";
            this.Activa.Name = "Activa";
            this.Activa.ReadOnly = true;
            this.Activa.Width = 77;
            // 
            // AceptaReferencia
            // 
            this.AceptaReferencia.DataPropertyName = "AceptaReferencias";
            this.AceptaReferencia.HeaderText = "Acepta Referencia";
            this.AceptaReferencia.Name = "AceptaReferencia";
            this.AceptaReferencia.ReadOnly = true;
            this.AceptaReferencia.Width = 152;
            // 
            // toolStrip2
            // 
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.c_BAgregarCuenta,
            this.c_BEditarCuenta});
            this.toolStrip2.Location = new System.Drawing.Point(2, 2);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(777, 25);
            this.toolStrip2.TabIndex = 6;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // c_BAgregarCuenta
            // 
            this.c_BAgregarCuenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.c_BAgregarCuenta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.c_BAgregarCuenta.Name = "c_BAgregarCuenta";
            this.c_BAgregarCuenta.Size = new System.Drawing.Size(94, 22);
            this.c_BAgregarCuenta.Text = "Agregar Cuenta";
            this.c_BAgregarCuenta.Click += new System.EventHandler(this.c_BAgregarCuenta_Click);
            // 
            // c_BEditarCuenta
            // 
            this.c_BEditarCuenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.c_BEditarCuenta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.c_BEditarCuenta.Name = "c_BEditarCuenta";
            this.c_BEditarCuenta.Size = new System.Drawing.Size(82, 22);
            this.c_BEditarCuenta.Text = "Editar Cuenta";
            this.c_BEditarCuenta.Click += new System.EventHandler(this.c_BEditarCuenta_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.c_Telefono);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.c_Nombre);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.c_Clave);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(0, 107);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(789, 118);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales Cliente";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(559, 68);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 26);
            this.button2.TabIndex = 24;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(559, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 27);
            this.button1.TabIndex = 23;
            this.button1.Text = "Editar Datos Generales";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // c_Telefono
            // 
            this.c_Telefono.BackColor = System.Drawing.Color.Gainsboro;
            this.c_Telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Telefono.Location = new System.Drawing.Point(92, 82);
            this.c_Telefono.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Telefono.Name = "c_Telefono";
            this.c_Telefono.ReadOnly = true;
            this.c_Telefono.Size = new System.Drawing.Size(349, 26);
            this.c_Telefono.TabIndex = 22;
            this.c_Telefono.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 83);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "Teléfono :";
            // 
            // c_Nombre
            // 
            this.c_Nombre.BackColor = System.Drawing.Color.Gainsboro;
            this.c_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Nombre.Location = new System.Drawing.Point(92, 52);
            this.c_Nombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.ReadOnly = true;
            this.c_Nombre.Size = new System.Drawing.Size(349, 26);
            this.c_Nombre.TabIndex = 20;
            this.c_Nombre.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Nombre :";
            // 
            // c_Clave
            // 
            this.c_Clave.BackColor = System.Drawing.Color.Gainsboro;
            this.c_Clave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Clave.Location = new System.Drawing.Point(92, 22);
            this.c_Clave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Clave.Name = "c_Clave";
            this.c_Clave.ReadOnly = true;
            this.c_Clave.Size = new System.Drawing.Size(165, 26);
            this.c_Clave.TabIndex = 18;
            this.c_Clave.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Clave :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "EDITAR CLIENTE";
            // 
            // FormEditarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditarCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormEditarCliente";
            this.tabControl1.ResumeLayout(false);
            this.c_TabUsuarios.ResumeLayout(false);
            this.c_TabUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Usuarios)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.c_TabCuentas.ResumeLayout(false);
            this.c_TabCuentas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_Cuentas)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage c_TabUsuarios;
        private System.Windows.Forms.DataGridView c_Usuarios;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton c_BAgregarUsuario;
        private System.Windows.Forms.ToolStripButton c_BEditarUsuario;
        private System.Windows.Forms.TabPage c_TabCuentas;
        private System.Windows.Forms.DataGridView c_Cuentas;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton c_BAgregarCuenta;
        private System.Windows.Forms.ToolStripButton c_BEditarCuenta;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox c_Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_Clave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox c_Telefono;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Activo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Alias;
        private System.Windows.Forms.DataGridViewTextBoxColumn Banco;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Activa;
        private System.Windows.Forms.DataGridViewTextBoxColumn AceptaReferencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCuenta;
    }
}