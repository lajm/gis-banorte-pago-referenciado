﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormConsultaEfectivo : Form
    {
        Double l_totalSaco = 0;
       
        Double l_TotalBolsa = 0;
        

        public FormConsultaEfectivo()
        {
            InitializeComponent();
        }

        private void FormConsultaEfectivo_Load(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            DataTable l_Datos = BDConsulta.ObtenerConsultaDepositos(1);
            int l_totalbilletes = 0;

            if (l_Datos.Rows.Count < 1)
            {
                using (FormError f_Error = new FormError(false))
                {
                    f_Error.c_MensajeError.Text = "No hay depósitos en pesos";
                    f_Error.ShowDialog();
                    Close();
                }
            }
            foreach (DataRow l_Detalle in l_Datos.Rows)
            {
                if (l_Detalle[1].ToString().Trim() == "1000")
                {
                    
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_1000.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 1000;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                  
                }
                if (l_Detalle[1].ToString().Trim() == "500")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_500.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 500;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                   
                }
                if (l_Detalle[1].ToString().Trim() == "200")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_200.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 200;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                   
                }
                if (l_Detalle[1].ToString().Trim() == "100")
                {
                   if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_100.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 100;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                   
                }
                if (l_Detalle[1].ToString().Trim() == "50")
                {
                   
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_50.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 50;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                   
                }
                if (l_Detalle[1].ToString().Trim() == "20")
                {
                    if (l_Detalle[2].ToString() == "1")
                    {
                        c_Cajon2_20.Text = Int32.Parse(l_Detalle[0].ToString()).ToString("#,##0");
                        l_TotalBolsa += (int)l_Detalle[0] * 20;
                        l_totalbilletes += (int)l_Detalle[0];
                    }
                    
                }

            }

            c_numbilletes.Text = l_totalbilletes.ToString ();
            c_TotalCajon2.Text = l_TotalBolsa.ToString("#,###,###,##0.00");
            Cursor.Hide();
        }

        private void c_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void c_Imprimir_Click(object sender, EventArgs e)
        {
            c_Imprimir.Enabled = false;
            
            if (l_TotalBolsa > 0)
            {
                ModulePrint imprimir = new ModulePrint();

                imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);

                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine("Consulta de Efectivo");
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("Contenido en BOVEDAD hasta el momento");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy")+ " Hora: " + DateTime.Now.ToLongTimeString());
                imprimir.AddSubHeaderLine(Globales.ClienteProsegur );

                imprimir.AddItem(c_Cajon2_1000.Text, "$1,000.00", "1000");
                imprimir.AddItem(c_Cajon2_500.Text, "$500.00", "500");
                imprimir.AddItem(c_Cajon2_200.Text, "$200.00", "200");
                imprimir.AddItem(c_Cajon2_100.Text, "$100.00", "100");
                imprimir.AddItem(c_Cajon2_50.Text, "$50.00", "50");
                imprimir.AddItem(c_Cajon2_20.Text, "$20.00", "20");

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Total: " + c_TotalCajon2.Text);

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Numero de Billetes: " + c_numbilletes. Text);



                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }

            c_Imprimir.Enabled = true;
            Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            c_mov.Enabled = false;
            DataTable l_Movimientos;
            Double l_total = 0;

            l_Movimientos = BDConsulta.ObtenerConsultaOperaciones_mov();

          
            if (l_Movimientos.Rows.Count > 0)
            {
               
                ModulePrint imprimir = new ModulePrint();

                imprimir.HeaderImage = Image.FromFile(System.Configuration.ConfigurationSettings.AppSettings["ImagenTicket"]);

                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine(" ");
                imprimir.AddHeaderLine("Consulta de MOVIMIENTOS");
                imprimir.AddHeaderLine("Moneda: Pesos");
                imprimir.AddHeaderLine("Operaciones registradas hasta el momento");
                imprimir.AddHeaderLine("");
                imprimir.AddSubHeaderLine("Usuario: " + Globales.IdUsuario);
                imprimir.AddSubHeaderLine("Fecha: " + DateTime.Now.ToString("dd/M/yyyy") + " Hora: " + DateTime.Now.ToLongTimeString());
                imprimir.AddSubHeaderLine(Globales.ClienteProsegur);

                foreach (DataRow l_Detalle in l_Movimientos.Rows)
                {
                    Double l_monto = Double.Parse(l_Detalle[2].ToString());
                    imprimir.AddItemUsuario(l_Detalle[1].ToString(), l_monto.ToString("$#,###,###,##0.00"), l_Detalle[0].ToString());
                    l_total += l_monto;

                }

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Numero de Depositantes: " + l_Movimientos.Rows.Count.ToString());

                imprimir.AddFooterLine("");
                imprimir.AddFooterLine("Total: " + l_total.ToString("$#,###,###,##0.00"));

                imprimir.AddFooterLine("");

                imprimir.PrintTicket(System.Configuration.ConfigurationSettings.AppSettings["PRINTER"], DateTime.Now.ToShortTimeString());
            }
            c_mov.Enabled = true;
            Close();
        }

    }
}
