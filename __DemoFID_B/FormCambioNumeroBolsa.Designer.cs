﻿namespace __DemoFID_B
{
    partial class FormCambioNumeroBolsa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_Aceptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.c_NumeroBolsa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_actualnumBolsa = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(355, 311);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 8;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_Aceptar
            // 
            this.c_Aceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Aceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Aceptar.Location = new System.Drawing.Point(124, 311);
            this.c_Aceptar.Name = "c_Aceptar";
            this.c_Aceptar.Size = new System.Drawing.Size(122, 47);
            this.c_Aceptar.TabIndex = 7;
            this.c_Aceptar.Text = "Aceptar";
            this.c_Aceptar.UseVisualStyleBackColor = true;
            this.c_Aceptar.Click += new System.EventHandler(this.c_Aceptar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Numero de BOLSA ACTUAL";
            // 
            // c_NumeroBolsa
            // 
            this.c_NumeroBolsa.BackColor = System.Drawing.SystemColors.Info;
            this.c_NumeroBolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_NumeroBolsa.Location = new System.Drawing.Point(264, 215);
            this.c_NumeroBolsa.Name = "c_NumeroBolsa";
            this.c_NumeroBolsa.Size = new System.Drawing.Size(260, 38);
            this.c_NumeroBolsa.TabIndex = 10;
            this.c_NumeroBolsa.Click += new System.EventHandler(this.c_IdUsuario_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nuevo Numero de BOLSA";
            // 
            // c_actualnumBolsa
            // 
            this.c_actualnumBolsa.BackColor = System.Drawing.SystemColors.Info;
            this.c_actualnumBolsa.Enabled = false;
            this.c_actualnumBolsa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_actualnumBolsa.Location = new System.Drawing.Point(264, 84);
            this.c_actualnumBolsa.Name = "c_actualnumBolsa";
            this.c_actualnumBolsa.Size = new System.Drawing.Size(260, 38);
            this.c_actualnumBolsa.TabIndex = 12;
            // 
            // FormCambioNumeroBolsa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 370);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_actualnumBolsa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_NumeroBolsa);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Aceptar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCambioNumeroBolsa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormCambioNumeroBolsa";
            this.Load += new System.EventHandler(this.FormCambioNumeroBolsa_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_Aceptar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_NumeroBolsa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_actualnumBolsa;
    }
}