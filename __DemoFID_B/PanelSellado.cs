﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class PanelSellado : UserControl
    {
        public PanelSellado()
        {
            InitializeComponent();
        }
        int contador = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (contador > 60)
                c_time.Text = "1:0"+ (contador++ - 60) + "s" ;
            else if (contador > 9)
                c_time.Text = "0:"+ contador++ + "s";
            else 
            c_time.Text = "0:0"+ contador++ + "s";

            Invalidate();
            Update();
            Refresh();
            Application.DoEvents();
            
        }

        public void Empezar()
        {
            timer1.Enabled = true;
        }

        public void Detener()
        {
            timer1.Enabled = false;
            contador = 0;
            c_time.Text = "0:00 s";
        }


        private void PanelSellado_Load(object sender, EventArgs e)
        {
          timer1.Enabled = false;
        }
    }
}
