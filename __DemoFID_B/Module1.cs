﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace __DemoFID_B
{
    public class Module1
    {
# region CONSTANTES

        //Public Const CENTER_HORI = 0
        //Public Const CENTER_VERT = 1
        //Public Const CENTER_BOTH = 2
        public const int CENTER_HORI = 0;
        public const int CENTER_VERT = 1;
        public const int CENTER_BOTH = 2;

        //public const long ChequeSave_MAX = (FidApi.MAXDOC_TOTAL_400 * 2);

//Public ChequeSave_ImageFilename(1 To ChequeSave_MAX) As String
//Public ChequeSave_ImageCodeline(1 To ChequeSave_MAX) As String
//Public ChequeSave_OCRAmount(1 To ChequeSave_MAX) As String
//Public ChequeSave_Idx As Long
//Public g_check_totalcounter As Long
//Public g_check_lastcounter As Long '12/Apr/2011
        public static bool FLAG_CHECK_OCR_PRESENT;
        public static bool FLAG_CHECK_OCR_USE_CTSDECOD_DLL;

        public static long MAX_DRAWERS = 4;
        public static long COMPORT_CSD8;

//Public Type BITMAPINFOHEADER  '40 bytes
//    biSize As Long
//    biWidth As Long
//    biHeight As Long
//    biPlanes As Integer
//    biBitCount As Integer
//    biCompression As Long
//    biSizeImage As Long
//    biXPelsPerMeter As Long
//    biYPelsPerMeter As Long
//    biClrUsed As Long
//    biClrImportant As Long
//End Type
//Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
        [DllImport("kernel32", EntryPoint = "RtlMoveMemoryA")]
        public static extern void RtlMoveMemory(Object Destination, Object Source, long Length);

//' Reader OMNIKEY : scardsyn.dll
        public const bool is_SMARTCARD_OMNYKEY = false;

//' Reader HCR360 : HCR360.dll
        public const bool is_SMARTCARD_HCR360 = true;

        public static bool FLAG_DEMO_DEBUG;
        public static bool FLAG_CSD8_PRESENT;
        public static bool FLAG_BAG_PRESENT;
        public static bool FLAG_NO_CSD8;
        public static bool FLAG_NO_HIDECURSOR;
        public static bool DEMO_FLAG_VIDEORESIZE;
        public static bool DEMO_FLAG_NO_PRINTER;
        public static bool DEMO_FLAG_NO_BADGE;
        public static bool DEMO_FLAG_NO_DRAWERS;
        public static bool DEMO_FLAG_NO_SERVICE;
        public static bool DEMO_FLAG_NO_IBUTTON;
        public static bool DEMO_FLAG_NO_UPS;
        public static bool FLAG_DIVIDE_BANKNOTE_CHECK;
        public static long DEMO_DRAWER_FOR_CHECKS; //' 0=none 1=drawer1 2=drawer2

        public static long DEMO_WIDTH;
        public static long DEMO_HEIGHT;

//Public Const DEMO_BACKCOLOR = &HFBFBFB
//Public Const DEMO_FORECOLOR = &HFFFFFF

//Public Const CRT_A_MAIU = "Á" ' 0193
//Public Const CRT_A_minu = "á" ' 0225
//Public Const CRT_E_MAIU = "É" ' 0201
//Public Const CRT_E_minu = "é" ' 0233
//Public Const CRT_I_minu = "í" ' 0237
//Public Const CRT_I_MAIU = "Í" ' 0205
//Public Const CRT_O_MAIU = "Ó" ' 0211
//Public Const CRT_O_minu = "ó" ' 0243
//Public Const CRT_U_MAIU = "Ú" ' 0218
//Public Const CRT_U_minu = "ú" ' 0250
//Public Const CRT_N_MAIU = "Ñ" ' 0209
//Public Const CRT_N_minu = "ñ" ' 0241


        public static bool g_FI2DSERV_opened;
        public static bool g_FormIntro_Loaded;
        public static bool g_flag_DocSessionStarted;
        public static bool g_flag_DocSessionAborted;

//Public g_demo_language As String '0=ENGLISH 1=ESPAÑOL 2=ITALIANO 3=selectable
//'Public g_bank_name As String ' NOTUSED!
//Public g_denom_name As String
//Public g_nome_operatore As String
//Public g_oper_nomi(0 To 9) As String
        public static String g_password_operatore;
        public static String g_password_inserita;
        public static long g_handleOfDll;
        public static String g_PathOfDll;
        public static bool g_flag_prima_mazzetta;
        public static bool g_flag_inizio_transazione;
        public static long g_numero_transazione;
        public static bool g_flag_administrator;
        public static bool g_flag_CIT_person;
        public static bool g_admin_from_sottomenu;
        public static bool g_from_insert_card;

        public static long g_TotaleRejected;
        public static long g_TotaleAccepted;
        public static long g_Conta1000;
        public static long g_Conta500;
        public static long g_Conta200;
        public static long g_Conta100;
        public static long g_Conta50;
        public static long g_Conta20;
        public static long g_Conta10;
        public static long g_Conta5;
        public static long g_Conta2;
        public static long g_Conta1;
        public static long g_ContaDepoTotale;
        public static long g_AmountDepoTotale;

//'// numero di documenti per ogni doctype
//Public g_numdoc_in_box1(0 To MAXDOCTYPE_IN_DRAWER - 1) As Long
//Public g_numdoc_in_box2(0 To MAXDOCTYPE_IN_DRAWER - 1) As Long
//Public g_numdoc_in_box3(0 To MAXDOCTYPE_IN_DRAWER - 1) As Long
//Public g_numdoc_in_box4(0 To MAXDOCTYPE_IN_DRAWER - 1) As Long

//'// log: sequenza dei documenti inseriti
//'BYTE buf_drawer1[MAXDOC_IN_DRAWER_DEF];
//'BYTE buf_drawer2[MAXDOC_IN_DRAWER_DEF];
//'BYTE buf_drawer3[MAXDOC_IN_DRAWER_DEF];
//'BYTE buf_drawer4[MAXDOC_IN_DRAWER_DEF];

//' totali banconote di una transazione
//Public Type ST_TRANSACTION_COUNTER
//    Conta1000 As Long
//    Conta500 As Long
//    Conta200 As Long
//    Conta100 As Long
//    Conta50 As Long
//    Conta20 As Long
//    Conta10 As Long
//    Conta5 As Long
//    Conta2 As Long
//    Conta1 As Long
//    ContaDepoTotale As Long
//    AmountDepoTotale As Long
//    ContaAssegni As Long
//    AmountAssegniTotale As Single
//End Type
//Public g_trans_counter As ST_TRANSACTION_COUNTER

//' totali banconote depositate nei cassetti
//Public Type ST_SAFE_COUNTER
//    Conta1000 As Long
//    Conta500 As Long
//    Conta200 As Long
//    Conta100 As Long
//    Conta50 As Long
//    Conta20 As Long
//    Conta10 As Long
//    Conta5 As Long
//    Conta2 As Long
//    Conta1 As Long
//    ContaTotale As Long
//    total_checks As Long    '05/Apr/2011
//End Type
//Public g_safe_counter(0 To 3) As ST_SAFE_COUNTER

//Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
        [DllImport("kernel32")]
        public static extern void CreateEvent(long dwMilliseconds);

//'Registry
//Public Const ERROR_SUCCESS = 0
//Public Const HKEY_CLASSES_ROOT = &H80000000
//Public Const HKEY_CURRENT_CONFIG = &H80000005
//Public Const HKEY_CURRENT_USER = &H80000001
//Public Const HKEY_LOCAL_MACHINE = &H80000002
//Public Const HKEY_USERS = &H80000003
//Public Const STANDARD_RIGHTS_ALL = &H1F0000
//Public Const KEY_QUERY_VALUE = &H1
//Public Const KEY_SET_VALUE = &H2
//Public Const KEY_CREATE_SUB_KEY = &H4
//Public Const KEY_ENUMERATE_SUB_KEYS = &H8
//Public Const KEY_NOTIFY = &H10
//Public Const KEY_CREATE_LINK = &H20
//Public Const SYNCHRONIZE = &H100000 'da WINNT.H
//Public Const READ_CONTROL = &H20000
//Public Const STANDARD_RIGHTS_READ = (READ_CONTROL)
//Public Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
//Public Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))
//Public Const REG_SZ = 1 ' unicode Stringa null terminated
//Public Const REG_BINARY = 3 ' Binary in forma libera
//Public Const REG_DWORD = 4 ' 32 bit number
//Public Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As Any, phkResult As Long, lpdwDisposition As Long) As Long
//Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
//Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long ' Note that if you declare the lpData parameter as String, you must pass it By Value.
//Public Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long ' Note that if you declare the lpData parameter as String, you must pass it By Value.
//Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
//Public Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, lpData As Byte, lpcbData As Long) As Long
//Public Declare Function RegEnumValueStr Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, ByRef lpcbValueName As Long, ByVal lReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
//Public Const REG_MAX_SIZE = 200 'arbitrary value
//Public Const ERROR_NO_MORE_ITEMS = 259&
//Public Const ERROR_MORE_DATA = 234
//Public Const REG_OPTION_NON_VOLATILE = 0

        public const long MAX_PATH = 260;


//Public Type FILETIME
//    dwLowDateTime As Long
//    dwHighDateTime As Long
//End Type
        public struct FILETIME
        {
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long dwLowDateTime;
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long dwHighDateTime;
        }

//Public Const INVALID_HANDLE_VALUE = -1

        public struct WIN32_FIND_DATA
        {
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long dwFileAttributes;
            [MarshalAs(UnmanagedType.ByValArray)]
            public FILETIME ftCreationTime;
            [MarshalAs(UnmanagedType.ByValArray)]
            public FILETIME ftLastAccessTime;
            [MarshalAs(UnmanagedType.ByValArray)]
            public FILETIME ftLastWriteTime;
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long nFileSizeHigh;
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long nFileSizeLow;
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long dwReserved0;
            [MarshalAs(UnmanagedType.ByValArray)]
            public static long dwReserved1;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst= (int)MAX_PATH)]
            public static String cFileName;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
            public static String cAlternate;
        }
        public static WIN32_FIND_DATA g_WIN32_FIND_DATA;
//Public Type WIN32_FIND_DATA
//    dwFileAttributes As Long
//    ftCreationTime As FILETIME
//    ftLastAccessTime As FILETIME
//    ftLastWriteTime As FILETIME
//    nFileSizeHigh As Long
//    nFileSizeLow As Long
//    dwReserved0 As Long
//    dwReserved1 As Long
//    cFileName As String * MAX_PATH ' A null-terminated string that is the name of the file.
//    cAlternate As String * 14
//End Type
//Public Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
        [DllImport("kernel32", EntryPoint = "FindFirstFileA")]
        public static extern long FindFirstFile(String lpFileName, WIN32_FIND_DATA lpFindFileData);
//Public Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
        [DllImport("kernel32", EntryPoint = "FindNextFileA")]
        public static extern long FindNextFile(long hFindFile, WIN32_FIND_DATA lpFindFileData);
//Public Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
        [DllImport("kernel32")]
        public static extern long FindClose(long hFindFile);
//Public Declare Function GetModuleHandle Lib "kernel32" Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Long
        [DllImport("kernel32", EntryPoint = "GetModuleHandleA")]
        public static extern long GetModuleHandle(String lpModuleName);
//Public Declare Function GetModuleFileName Lib "kernel32" Alias "GetModuleFileNameA" (ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
        [DllImport("kernel32", EntryPoint = "GetModuleFileNameA")]
        public static extern long GetModuleFileName(long hModule, String lpFileName, long nSize);
//Public Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
        [DllImport("kernel32", EntryPoint = "LoadLibraryA")]
        public static extern long LoadLibrary(String lpLibFileName);
//Public Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long
        [DllImport("kernel32")]
        public static extern long FreeLibrary(long hLibModule);
//Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
        [DllImport("kernel32", EntryPoint = "GetPrivateProfileStringA")]
        public static extern long GetPrivateProfileString(String lpApplicationName, Object lpKeyName,
            String lpDefault, String lpReturnedString, long nSize, String lpFileName);
//Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
        [DllImport("kernel32", EntryPoint = "WritePrivateProfileStringA")]
        public static extern long WritePrivateProfileString(String lpApplicationName, Object lpKeyName, Object lpString);


//' ASSEGNI --
//Public Type T_OCRReadOption
//    PutBlanks As Long
//    TypeRead As Byte
//End Type
//'Public Const MAX_PIXEL_ROW = 42 '// fixed height in pixels of the OCR window
//Public Const MAX_PIXEL_ROW = 84 '// fixed height in pixels of the OCR window

//'unsigned long WINAPI DecodeChar(
//'    long bmpBuffer[],           // IN  full document DIB handle
//'    unsigned char font[],       // IN  'A'->OCRA, 'B'->OCRB SET RIDOTTO, 'C'->OCRB SET COMPLETO, 'E'->E13B
//'    short start_x,              // IN  (left to right) in pixels
//'    short start_y,              // IN  (bottom to top) in pixels
//'    short size_x,               // IN  width of the codeline window, in pixels
//'    short size_y,               // IN  height of the codeline window, in pixels (MAX_PIXEL_ROW)
//'    PDECODEOPTIONS pOptions,    // IN  above-defined structure
//'    char szText[],              // OUT <FONT><codeline>'\0'
//'    char szPattFileName[] );    // IN  (not used, set to NULL)
//Public Declare Function DecodeChar Lib "Ctsdecod.dll" (ByVal bmpBuffer As Long, ByRef myfont As Byte, _
//            ByVal start_x As Integer, ByVal start_y As Integer, ByVal size_x As Integer, ByVal size_y As Integer, _
//            ByRef lpOption As T_OCRReadOption, ByVal sCodeline As String, ByVal res As Long) As Long


//' CTS DECODE EVENT --
//Public heCtsdecod As Long
//Public Const PAGE_READWRITE As Long = &H4
//Public Const FILE_MAP_WRITE As Long = &H2
//Public Declare Function CreateFileMapping Lib "kernel32.dll" Alias "CreateFileMappingA" ( _
//            ByVal hFile As Long, ByVal lpFileMappigAttributes As Long, ByVal flProtect As Long, _
//            ByVal dwMaximumSizeHigh As Long, ByVal dwMaximumSizeLow As Long, ByVal lpname As String) As Long
//Private Declare Function MapViewOfFile Lib "kernel32.dll" (ByVal hFileMappingObject As Long, _
//            ByVal dwDesiredAccess As Long, ByVal dwFileOffsetHigh As Long, _
//            ByVal dwFileOffsetLow As Long, ByVal dwNumberOfBytesToMap As Long) As Long
//Public Declare Function CreateEvent& Lib "kernel32" Alias "CreateEventA" (ByVal lpEventAttributes As Long, ByVal bManualReset As Long, ByVal bInitialState As Long, ByVal lpname As String)
        [DllImport("kernel32", EntryPoint = "CreateEventA")]
        public static extern void CreateEvent(long lpEventAttributes, long bManualReset, long bInitialState, String lpname);
//Public Declare Function SetEvent Lib "kernel32.dll" (ByVal hEvent As Long) As Long
        [DllImport("kernel32")]
        public static extern long SetEvent(long hEvent);
//Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
        [DllImport("kernel32")]
        public static extern long CloseHandle(long hObject);
//'


//Public Function FI2DDemoIni_GetKey(SectionName As String, keyname As String, DefaultString As String) As String
    
//    Dim num_of_crt As Long
//    Dim lpReturnString As String
//    Dim nSize As Long
    
//    lpReturnString = Space$(128)
//    nSize = Len(lpReturnString)
    
//    num_of_crt = GetPrivateProfileString(SectionName, keyname, DefaultString, lpReturnString, nSize, _
//        App.path & "\FI2DDemoVB.ini")
    
//    If ((num_of_crt = 0) Or (num_of_crt > 80)) Then
//        FI2DDemoIni_GetKey = ""
//        Exit Function
//    End If
    
//    FI2DDemoIni_GetKey = Left(lpReturnString, num_of_crt)

//End Function

//Public Function FI2DDemoIni_SetKey(SectionName As String, keyname As String, lpString As String) As Boolean
    
//    Dim success As Long
    
//    success = WritePrivateProfileString(SectionName, keyname, lpString, _
//        App.path & "\FI2DDemoVB.ini")
    
//    FI2DDemoIni_SetKey = success
        
//End Function


//Public Function FI2DNoteRecIni_GetKey(SectionName As String, keyname As String, DefaultString As String) As String
    
//    Dim num_of_crt As Long
//    Dim lpReturnString As String
//    Dim nSize As Long
    
//    lpReturnString = Space$(128)
//    nSize = Len(lpReturnString)
    
//    num_of_crt = GetPrivateProfileString(SectionName, keyname, DefaultString, lpReturnString, nSize, _
//        g_PathOfDll & "\NoteRec.ini")
    
//    If ((num_of_crt = 0) Or (num_of_crt > 80)) Then
//        FI2DNoteRecIni_GetKey = ""
//        Exit Function
//    End If
    
//    FI2DNoteRecIni_GetKey = Left(lpReturnString, num_of_crt)

//End Function

//Public Sub myCenterControl(frm As Form, ctr As Control, Mode As Long)

//    If ((Mode = CENTER_HORI) Or (Mode = CENTER_BOTH)) Then
//        ctr.Left = frm.Left + (frm.Width / 2) - (ctr.Width / 2)
//    End If

//    If ((Mode = CENTER_VERT) Or (Mode = CENTER_BOTH)) Then
//        ctr.Top = frm.Top + (frm.Height / 2) - (ctr.Height / 2)
//    End If
    
//End Sub

//Public Sub exitprogram()

//    Screen.MousePointer = vbHourglass: DoEvents

//    If (heCtsdecod <> 0) Then
//        Call CloseHandle(heCtsdecod)
//    End If
    
//    ' UNLOCK vandal door
//    If (DEMO_FLAG_NO_SERVICE = True) Then
//        'lRet = NS_OKAY: Call Sleep(100)
//    Else
//        If (g_FI2DSERV_opened = True) Then
//            Call FI2DSERV_VandalDoor(SERVICE_CMD_VDOOR_UNLOCK, g_vdoor_status)
//        End If
//    End If

//    If (g_handleOfDll <> 0) Then
//        Call FreeLibrary(g_handleOfDll)
//        g_handleOfDll = 0
//    End If

//    If (DEMO_FLAG_VIDEORESIZE = True) Then
//        Call videores_Restore
//    End If
    
//    If (DEMO_FLAG_NO_BADGE = False) Then
//        If (is_SMARTCARD_HCR360 = True) Then
//            Call CloseCom(HCR360_port)
//        End If
//    End If
    
//''''PROVA
//'    Dim ii As Integer
//'    Dim numform As Integer
//'    numform = Forms.count - 1
//'    For ii = numform To 0 Step -1
//'        Unload Forms(ii)
//'    Next ii
//''''PROVA
    
//    End 'terminate demo program!
    
//End Sub


//' 11/Apr/2011
//Public Function deleteFileInFolder(path As String, filefilter As String) As Boolean
    
//    ' esempio: deleteFileInFolder(App.path & "\images\checks", "*.bmp")
    
//    Dim hFiles As Long
//    Dim fd As WIN32_FIND_DATA
//    Dim fRet As Boolean
    
//    ' Find first file (get handle to find)
//    hFiles = FindFirstFile(path & "\" & filefilter, fd)
//    fRet = (hFiles <> INVALID_HANDLE_VALUE)
    
//    Do While (fRet = True)
//        ' delete file
//        Call Kill(path & "\" & fd.cFileName)
//        ' Find next file
//        fRet = FindNextFile(hFiles, fd)
//    Loop
    
//    ' Close the specified search handle
//    fRet = FindClose(hFiles)

//    deleteFileInFolder = True

//End Function


//Public Sub resetDatiAssegni()
    
//    Dim iii As Long
    
//    For iii = 1 To ChequeSave_MAX
//        'ChequeSave_ImageHandle(iii) = 0
//        ChequeSave_ImageFilename(iii) = ""
//        ChequeSave_ImageCodeline(iii) = ""
//        ChequeSave_OCRAmount(iii) = ""
//    Next iii

//    ChequeSave_Idx = 0
    
//    ' 11/Apr/2011
//    Call deleteFileInFolder(App.path & "\images\checks", "*.bmp")
    
//End Sub


//Public Function CreateCTSEvent() As Long
    
//'Exit Function
    
//    Dim hfmCtsDecod As Long
//    Dim pMemFile As Long
   
//    hfmCtsDecod = CreateFileMapping(-1, 0, PAGE_READWRITE, 0, 1024, "SYSTEM_FILE_MAPPING")

//    If (hfmCtsDecod = 0) Then
//        CreateCTSEvent = -1
//        Exit Function
//    End If

//    pMemFile = MapViewOfFile(hfmCtsDecod, FILE_MAP_WRITE, 0, 0, 0)
    
//    If (pMemFile = 0) Then
//        CreateCTSEvent = -2
//        Exit Function
//    End If

//    heCtsdecod = CreateEvent(ByVal 0&, False, False, "CTS_EVENT")
//    If (heCtsdecod = 0) Then
//        CreateCTSEvent = -3
//        Exit Function
//    End If

//    CreateCTSEvent = 0 ' OK

//End Function

//Public Function getstringa(ss As String) As String

//    getstringa = ""
    
//    If (InStr(1, ss, Chr$(0)) > 0) Then
//        getstringa = Left$(ss, InStr(1, ss, Chr$(0)) - 1)
//    Else
//        getstringa = Trim$(ss) ' & Chr$(0)
//    End If
    
//End Function


//Public Sub visualizza_per_valuta(denom As String, frm As Form)

//    Dim iii As Long

//    ' trovo le top position per le label (il riferimento e' frm.LabelNomeDenom)
//    Dim myTop(0 To 6) As Single
//    For iii = 0 To 6
//        myTop(iii) = frm.LabelNomeDenom(iii).Top
//    Next iii
    

# endregion
    }
}
