﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormDaEmpresa : Form
    {
      String l_Empresa = "";

        public FormDaEmpresa()
        {
            InitializeComponent();
        }
        private void FormTameme_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
        }

        private void Oprimir_tecla(object sender, EventArgs e)
        {
            Cursor.Show();
            Cursor.Show();
            Cursor.Current = Cursors.WaitCursor;
            if (sender.Equals(c_Boton0))
            {
                l_Empresa += "0";
                c_RutaTameme.Text += "0";
            }
            else if (sender.Equals(c_Boton1))
            {
                l_Empresa += "1";
                c_RutaTameme.Text += "1";
            }
            else if (sender.Equals(c_Boton2))
            {
                l_Empresa += "2";
                c_RutaTameme.Text += "2";
            }
            else if (sender.Equals(c_Boton3))
            {
                l_Empresa += "3";
                c_RutaTameme.Text += "3";
            }
            else if (sender.Equals(c_Boton4))
            {
                l_Empresa += "4";
                c_RutaTameme.Text += "4";
            }
            else if (sender.Equals(c_Boton5))
            {
                l_Empresa += "5";
                c_RutaTameme.Text += "5";
            }
            else if (sender.Equals(c_Boton6))
            {
                l_Empresa += "6";
                c_RutaTameme.Text += "6";
            }
            else if (sender.Equals(c_Boton7))
            {
                l_Empresa += "7";
                c_RutaTameme.Text += "7";
            }
            else if (sender.Equals(c_Boton8))
            {
                l_Empresa += "8";
                c_RutaTameme.Text += "8";
            }
            else if (sender.Equals(c_Boton9))
            {
                l_Empresa += "9";
                c_RutaTameme.Text += "9";
            }
            else if (sender.Equals(c_BotonCancelar))
            {
                Close();
                return;
            }
            else if (sender.Equals(c_BackSpace))
            {
                if (l_Empresa.Length != 0)
                {
                    l_Empresa = l_Empresa.Substring(0, l_Empresa.Length - 1);
                    c_RutaTameme.Text = c_RutaTameme.Text.Substring(0, l_Empresa.Length);
                }
            }
            else if (sender.Equals(c_BotonAceptar))
            {
                if (l_Empresa.Length > 2)
                {
                   
                    using (FormEmpleado f_Inicio = new FormEmpleado())
                    {
                        f_Inicio.Empresa = l_Empresa;
                        f_Inicio.ShowDialog();
                    }
                

                    Close();
                    return;

                }
                else
                {
                    using (FormError f_Error = new FormError(false))
                    {
                        f_Error.c_MensajeError.Text = "Debe escribir un numero de Empresa para continuar";
                        f_Error.ShowDialog();
                    }
                }
            }
            Cursor.Hide();
        }

        private void FormRutaTAMEME_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
        }

      


    }
}
