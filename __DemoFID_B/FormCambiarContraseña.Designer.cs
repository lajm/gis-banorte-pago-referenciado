﻿namespace __DemoFID_B
{
    partial class FormCambiarContraseña
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_contraseña = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.c_nuevacontraseña = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.c_confirmacionContraseña = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.c_Cambiar = new System.Windows.Forms.Button();
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_contraseña
            // 
            this.c_contraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_contraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_contraseña.Location = new System.Drawing.Point(283, 144);
            this.c_contraseña.Name = "c_contraseña";
            this.c_contraseña.PasswordChar = '*';
            this.c_contraseña.Size = new System.Drawing.Size(180, 22);
            this.c_contraseña.TabIndex = 0;
            this.c_contraseña.Click += new System.EventHandler(this.c_contraseña_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(143, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Contraseña Actual";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(143, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nueva contraseña";
            // 
            // c_nuevacontraseña
            // 
            this.c_nuevacontraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_nuevacontraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_nuevacontraseña.Location = new System.Drawing.Point(283, 184);
            this.c_nuevacontraseña.Name = "c_nuevacontraseña";
            this.c_nuevacontraseña.PasswordChar = '*';
            this.c_nuevacontraseña.Size = new System.Drawing.Size(180, 22);
            this.c_nuevacontraseña.TabIndex = 2;
            this.c_nuevacontraseña.Click += new System.EventHandler(this.c_nuevacontraseña_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(79, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Confirme nueva Contraseña";
            // 
            // c_confirmacionContraseña
            // 
            this.c_confirmacionContraseña.BackColor = System.Drawing.SystemColors.Info;
            this.c_confirmacionContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_confirmacionContraseña.Location = new System.Drawing.Point(283, 224);
            this.c_confirmacionContraseña.Name = "c_confirmacionContraseña";
            this.c_confirmacionContraseña.PasswordChar = '*';
            this.c_confirmacionContraseña.Size = new System.Drawing.Size(180, 22);
            this.c_confirmacionContraseña.TabIndex = 4;
            this.c_confirmacionContraseña.Click += new System.EventHandler(this.c_confirmacionContraseña_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cambio de Contraseña";
            // 
            // c_Cambiar
            // 
            this.c_Cambiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_Cambiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cambiar.Location = new System.Drawing.Point(118, 300);
            this.c_Cambiar.Name = "c_Cambiar";
            this.c_Cambiar.Size = new System.Drawing.Size(122, 47);
            this.c_Cambiar.TabIndex = 7;
            this.c_Cambiar.Text = "Cambiar";
            this.c_Cambiar.UseVisualStyleBackColor = true;
            this.c_Cambiar.Click += new System.EventHandler(this.c_Cambiar_Click);
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(349, 300);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 8;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(189, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(217, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Contraseña ADMINISTRADOR";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(496, 279);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormCambiarContraseña
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(600, 370);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_Cambiar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_confirmacionContraseña);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_nuevacontraseña);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_contraseña);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCambiarContraseña";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cambiar Contraseña Administrador";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.FormCambiarContraseña_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox c_contraseña;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox c_nuevacontraseña;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox c_confirmacionContraseña;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button c_Cambiar;
        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}