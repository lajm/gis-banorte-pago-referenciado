﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public partial class FormAvisoEsperar : Form
    {
        public FormAvisoEsperar()
        {
            InitializeComponent();
            c_redibujado.Enabled = true;
        }

        private void c_redibujado_Tick(object sender, EventArgs e)
        {
            c_redibujado.Stop ();


            Forzardibujado();
            c_redibujado.Start(); ;


        }

        public void Mensaje(String p_mensaje)
        {
            c_mensaje.Text = p_mensaje;
            Forzardibujado();
            
        }

        private void FormAvisoEsperar_Load(object sender, EventArgs e)
        {
            c_redibujado.Start() ;
        }

        public void Forzardibujado()
        {  
            this.Invalidate();
            this.Update();
            this.Refresh();
            Application.DoEvents();
        }

        private void FormAvisoEsperar_FormClosing(object sender, FormClosingEventArgs e)
        {
            c_redibujado.Enabled = false;
        }
    }
}
