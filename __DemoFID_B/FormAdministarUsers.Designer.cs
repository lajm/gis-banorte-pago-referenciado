﻿namespace __DemoFID_B
{
    partial class FormAdministarUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("ALTA DE EMPLEADO", 2);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("MODIFICAR EMPLEADO", 0);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(" ETV", 1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "CONTRASEÑA  ADMINISTRADOR"}, 5, System.Drawing.Color.Empty, System.Drawing.SystemColors.Window, null);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("BAJA DE EMPLEADO", 3);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("REGRESAR", 10);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdministarUsers));
            this.listView1 = new System.Windows.Forms.ListView();
            this.c_imagenes = new System.Windows.Forms.ImageList(this.components);
            this.uc_PanelInferior1 = new @__DemoFID_B.uc_PanelInferior();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.White;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            listViewItem2.StateImageIndex = 0;
            listViewItem4.IndentCount = 1;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6});
            this.listView1.LargeImageList = this.c_imagenes;
            this.listView1.Location = new System.Drawing.Point(94, 116);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(612, 368);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // c_imagenes
            // 
            this.c_imagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("c_imagenes.ImageStream")));
            this.c_imagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.c_imagenes.Images.SetKeyName(0, "ManageUsers.png");
            this.c_imagenes.Images.SetKeyName(1, "Security.png");
            this.c_imagenes.Images.SetKeyName(2, "Personnel.png");
            this.c_imagenes.Images.SetKeyName(3, "Users.png");
            this.c_imagenes.Images.SetKeyName(4, "user.ico");
            this.c_imagenes.Images.SetKeyName(5, "Key.png");
            this.c_imagenes.Images.SetKeyName(6, "Lock.png");
            this.c_imagenes.Images.SetKeyName(7, "dialog-password.png");
            this.c_imagenes.Images.SetKeyName(8, "dialog-password-2.png");
            this.c_imagenes.Images.SetKeyName(9, "okteta.png");
            this.c_imagenes.Images.SetKeyName(10, "utilities-session_properties.png");
            // 
            // uc_PanelInferior1
            // 
            this.uc_PanelInferior1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uc_PanelInferior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uc_PanelInferior1.BackgroundImage")));
            this.uc_PanelInferior1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uc_PanelInferior1.Enabled = false;
            this.uc_PanelInferior1.Location = new System.Drawing.Point(0, 550);
            this.uc_PanelInferior1.Name = "uc_PanelInferior1";
            this.uc_PanelInferior1.Size = new System.Drawing.Size(800, 50);
            this.uc_PanelInferior1.TabIndex = 1;
            // 
            // FormAdministarUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.uc_PanelInferior1);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAdministarUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modulo de Administración de  Usuarios";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ImageList c_imagenes;
        private uc_PanelInferior uc_PanelInferior1;
    }
}