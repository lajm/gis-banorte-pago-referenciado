﻿namespace __DemoFID_B
{
    partial class FormEditarDatosCuentaEditar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c_Cancelar = new System.Windows.Forms.Button();
            this.c_BAceptar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.c_Cuenta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_Alias = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.c_Status = new System.Windows.Forms.CheckBox();
            this.c_Banco = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.c_IdCuenta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.c_AcptRef = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c_Cancelar
            // 
            this.c_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.c_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cancelar.Location = new System.Drawing.Point(363, 408);
            this.c_Cancelar.Name = "c_Cancelar";
            this.c_Cancelar.Size = new System.Drawing.Size(122, 47);
            this.c_Cancelar.TabIndex = 7;
            this.c_Cancelar.Text = "Cancelar";
            this.c_Cancelar.UseVisualStyleBackColor = true;
            this.c_Cancelar.Click += new System.EventHandler(this.c_Cancelar_Click);
            // 
            // c_BAceptar
            // 
            this.c_BAceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.c_BAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_BAceptar.Location = new System.Drawing.Point(178, 408);
            this.c_BAceptar.Name = "c_BAceptar";
            this.c_BAceptar.Size = new System.Drawing.Size(122, 47);
            this.c_BAceptar.TabIndex = 6;
            this.c_BAceptar.Text = "Aceptar";
            this.c_BAceptar.UseVisualStyleBackColor = true;
            this.c_BAceptar.Click += new System.EventHandler(this.c_BAceptar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(223, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "EDITAR  CUENTA OPERADOR";
            // 
            // c_Cuenta
            // 
            this.c_Cuenta.BackColor = System.Drawing.SystemColors.Window;
            this.c_Cuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Cuenta.Location = new System.Drawing.Point(273, 254);
            this.c_Cuenta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Cuenta.Name = "c_Cuenta";
            this.c_Cuenta.Size = new System.Drawing.Size(166, 26);
            this.c_Cuenta.TabIndex = 28;
            this.c_Cuenta.Click += new System.EventHandler(this.c_Cuenta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(196, 254);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Cuenta :";
            // 
            // c_Alias
            // 
            this.c_Alias.BackColor = System.Drawing.Color.White;
            this.c_Alias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Alias.Location = new System.Drawing.Point(273, 189);
            this.c_Alias.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Alias.Name = "c_Alias";
            this.c_Alias.Size = new System.Drawing.Size(165, 26);
            this.c_Alias.TabIndex = 26;
            this.c_Alias.Click += new System.EventHandler(this.c_Alias_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(213, 195);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "Alias  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(107, 290);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Acepta Referencias :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(183, 324);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 35;
            this.label7.Text = "Active :";
            // 
            // c_Status
            // 
            this.c_Status.AutoSize = true;
            this.c_Status.Location = new System.Drawing.Point(273, 328);
            this.c_Status.Name = "c_Status";
            this.c_Status.Size = new System.Drawing.Size(15, 14);
            this.c_Status.TabIndex = 36;
            this.c_Status.UseVisualStyleBackColor = true;
            // 
            // c_Banco
            // 
            this.c_Banco.BackColor = System.Drawing.Color.White;
            this.c_Banco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_Banco.Location = new System.Drawing.Point(274, 219);
            this.c_Banco.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_Banco.Name = "c_Banco";
            this.c_Banco.Size = new System.Drawing.Size(165, 26);
            this.c_Banco.TabIndex = 38;
            this.c_Banco.Click += new System.EventHandler(this.c_Banco_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(196, 222);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 37;
            this.label5.Text = "Banco :";
            // 
            // c_IdCuenta
            // 
            this.c_IdCuenta.BackColor = System.Drawing.Color.White;
            this.c_IdCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c_IdCuenta.Location = new System.Drawing.Point(274, 159);
            this.c_IdCuenta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c_IdCuenta.Name = "c_IdCuenta";
            this.c_IdCuenta.ReadOnly = true;
            this.c_IdCuenta.Size = new System.Drawing.Size(165, 26);
            this.c_IdCuenta.TabIndex = 40;
            this.c_IdCuenta.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(169, 159);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 20);
            this.label6.TabIndex = 39;
            this.label6.Text = "ID Cuenta :";
            this.label6.Visible = false;
            // 
            // c_AcptRef
            // 
            this.c_AcptRef.AutoSize = true;
            this.c_AcptRef.Location = new System.Drawing.Point(274, 293);
            this.c_AcptRef.Name = "c_AcptRef";
            this.c_AcptRef.Size = new System.Drawing.Size(15, 14);
            this.c_AcptRef.TabIndex = 41;
            this.c_AcptRef.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::@__DemoFID_B.Properties.Resources.Preh_configuracion;
            this.pictureBox1.Location = new System.Drawing.Point(614, 408);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 42;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // FormEditarDatosCuentaEditar
            // 
            this.AcceptButton = this.c_BAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::@__DemoFID_B.Properties.Resources.FID_fondo_pantallas_ok;
            this.CancelButton = this.c_Cancelar;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.c_AcptRef);
            this.Controls.Add(this.c_IdCuenta);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.c_Banco);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.c_Status);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.c_Cuenta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_Alias);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_Cancelar);
            this.Controls.Add(this.c_BAceptar);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditarDatosCuentaEditar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormEditarDatosGeneralesCliente";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c_Cancelar;
        private System.Windows.Forms.Button c_BAceptar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox c_Cuenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox c_Alias;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox c_Status;
        private System.Windows.Forms.TextBox c_Banco;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox c_IdCuenta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox c_AcptRef;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}