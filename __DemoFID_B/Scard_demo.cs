﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace __DemoFID_B
{
    public class Scard_demo
    {
        /*
        //'
        //' is_SMARTCARD_HCR360
        //'
        public static int HCR360_port;
        public static int HCR360_baud;
        public static int HCR360_Parity;
        //'

        public static int g_hContext;
        public static long g_hCard;
        public static bool g_isContext;
        public static bool g_isCardConnected;
        public static String g_curReader;

        public static long OKERR_OK = 0x0;

        //'
        //' Handles errorcodes defined in Scard.bas and return an errormessage as String
        //'
        //Public Function ScardHandleError(ByVal lRet As Long) As String
        public static String ScardHandleError(long lRet)
        {
            //    ScardHandleError = ""
            if (lRet == OKERR_OK)
                return "No Error";
            else if (lRet == Scard.OKERR_SCARD__E_CANCELLED) return "The action was cancelled by an SCardCancel request";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_HANDLE) return "The supplied handle was invalid";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_PARAMETER) return "One or more of the supplied parameters could not be properly interpreted";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_TARGET) return "Registry startup information is missing or invalid";
            else if (lRet == Scard.OKERR_SCARD__E_NO_MEMORY) return "Not enough memory available to complete this command";
            else if (lRet == Scard.OKERR_SCARD__F_WAITED_TOO_LONG) return "An internal consistency timer has expired";
            else if (lRet == Scard.OKERR_SCARD__E_INSUFFICIENT_BUFFER) return "The data buffer to receive returned data is too small for the returned data";
            else if (lRet == Scard.OKERR_SCARD__E_UNKNOWN_READER) return "The specified reader name is not recognized";
            else if (lRet == Scard.OKERR_SCARD__E_TIMEOUT) return "The user-specified timeout value has expired";
            else if (lRet == Scard.OKERR_SCARD__E_SHARING_VIOLATION) return "The smart card cannot be accessed because of other connections outstanding";
            else if (lRet == Scard.OKERR_SCARD__E_NO_SMARTCARD) return "The operation requires a Smart Card, but no Smart Card is currently in the device";
            else if (lRet == Scard.OKERR_SCARD__E_UNKNOWN_CARD) return "The specified smart card name is not recognized";
            else if (lRet == Scard.OKERR_SCARD__E_CANT_DISPOSE) return "The system could not dispose of the media in the requested manner";
            else if (lRet == Scard.OKERR_SCARD__E_PROTO_MISMATCH) return "The requested protocols are incompatible with the protocol currently in use with the smart card";
            else if (lRet == Scard.OKERR_SCARD__E_NOT_READY) return "The reader or smart card is not ready to accept commands";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_VALUE) return "One or more of the supplied parameters values could not be properly interpreted";
            else if (lRet == Scard.OKERR_SCARD__E_SYSTEM_CANCELLED) return "The action was cancelled by the system, presumably to log off or shut down";
            else if (lRet == Scard.OKERR_SCARD__F_COMM_ERROR) return "An internal communications error has been detected";
            else if (lRet == Scard.OKERR_SCARD__F_UNKNOWN_ERROR) return "An internal error has been detected, but the source is unknown";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_ATR) return "An ATR obtained from the registry is not a valid ATR string";
            else if (lRet == Scard.OKERR_SCARD__E_NOT_TRANSACTED) return "An attempt was made to end a non-existent transaction";
            else if (lRet == Scard.OKERR_SCARD__E_READER_UNAVAILABLE) return "The specified reader is not currently available for use";
            else if (lRet == Scard.OKERR_SCARD__P_SHUTDOWN) return "The operation has been aborted to allow the server application to exit";
            else if (lRet == Scard.OKERR_SCARD__E_PCI_TOO_SMALL) return "The PCI Receive buffer was too small";
            else if (lRet == Scard.OKERR_SCARD__E_READER_UNSUPPORTED) return "The reader driver does not meet minimal requirements for support";
            else if (lRet == Scard.OKERR_SCARD__E_DUPLICATE_READER) return "The reader driver did not produce a unique reader name";
            else if (lRet == Scard.OKERR_SCARD__E_CARD_UNSUPPORTED) return "The smart card does not meet minimal requirements for support";
            else if (lRet == Scard.OKERR_SCARD__E_NO_SERVICE) return "The Smart card resource manager is not running";
            else if (lRet == Scard.OKERR_SCARD__E_SERVICE_STOPPED) return "The Smart card resource manager has shut down";
            else if (lRet == Scard.OKERR_SCARD__E_UNEXPECTED) return "An unexpected card error has occurred";
            else if (lRet == Scard.OKERR_SCARD__E_ICC_INSTALLATION) return "No Primary Provider can be found for the smart card";
            else if (lRet == Scard.OKERR_SCARD__E_ICC_CREATEORDER) return "The requested order of object creation is not supported";
            else if (lRet == Scard.OKERR_SCARD__E_UNSUPPORTED_FEATURE) return "This smart card does not support the requested feature";
            else if (lRet == Scard.OKERR_SCARD__E_DIR_NOT_FOUND) return "The identified directory does not exist in the smart card";
            else if (lRet == Scard.OKERR_SCARD__E_FILE_NOT_FOUND) return "The identified file does not exist in the smart card";
            else if (lRet == Scard.OKERR_SCARD__E_NO_DIR) return "The supplied path does not represent a smart card directory";
            else if (lRet == Scard.OKERR_SCARD__E_NO_FILE) return "The supplied path does not represent a smart card file";
            else if (lRet == Scard.OKERR_SCARD__E_NO_ACCESS) return "Access is denied to this file";
            else if (lRet == Scard.OKERR_SCARD__E_WRITE_TOO_MANY) return "An attempt was made to write more data than would fit in the target object";
            else if (lRet == Scard.OKERR_SCARD__E_BAD_SEEK) return "There was an error trying to set the smart card file object pointer";
            else if (lRet == Scard.OKERR_SCARD__E_INVALID_CHV) return "The supplied PIN is incorrect";
            else if (lRet == Scard.OKERR_SCARD__E_UNKNOWN_RES_MNG) return "An unrecognized error code was returned from a layered component";
            else if (lRet == Scard.OKERR_SCARD__E_NO_SUCH_CERTIFICATE) return "The requested certificate does not exist";
            else if (lRet == Scard.OKERR_SCARD__E_CERTIFICATE_UNAVAILABLE) return "The requested certificate could not be obtained";
            else if (lRet == Scard.OKERR_SCARD__E_NO_READERS_AVAILABLE) return "Cannot find a smart card reader";
            else if (lRet == Scard.OKERR_SCARD__E_COMM_DATA_LOST) return "A communications error with the smart card has been detected";
            else if (lRet == Scard.OKERR_SCARD__W_UNSUPPORTED_CARD) return "The reader cannot communicate with the smart card, due to ATR configuration conflicts";
            else if (lRet == Scard.OKERR_SCARD__W_UNRESPONSIVE_CARD) return "The smart card is not responding to a reset";
            else if (lRet == Scard.OKERR_SCARD__W_UNPOWERED_CARD) return "Power has been removed from the smart card, so that further communication is not possible";
            else if (lRet == Scard.OKERR_SCARD__W_RESET_CARD) return "The smart card has been reset, so any shared state information is invalid";
            else if (lRet == Scard.OKERR_SCARD__W_REMOVED_CARD) return "The smart card has been removed, so that further communication is not possible";
            else if (lRet == Scard.OKERR_SCARD__W_SECURITY_VIOLATION) return "Access was denied because of a security violation";
            else if (lRet == Scard.OKERR_SCARD__W_WRONG_CHV) return "The card cannot be accessed because the wrong PIN was presented";
            else if (lRet == Scard.OKERR_SCARD__W_CHV_BLOCKED) return "The card cannot be accessed because the maximum number of PIN entry attempts has been reached";
            else if (lRet == Scard.OKERR_SCARD__W_EOF) return "The end of the smart card file has been reached";
            else if (lRet == Scard.OKERR_SCARD__W_CANCELLED_BY_USER) return "The action was cancelled by the user";

            else if (lRet == Scard.OKERR_PARM1) return "Error in parameter 1";
            else if (lRet == Scard.OKERR_PARM2) return "Error in parameter 2";
            else if (lRet == Scard.OKERR_PARM3) return "Error in parameter 3";
            else if (lRet == Scard.OKERR_PARM4) return "Error in parameter 4";
            else if (lRet == Scard.OKERR_PARM5) return "Error in parameter 5";
            else if (lRet == Scard.OKERR_PARM6) return "Error in parameter 6";
            else if (lRet == Scard.OKERR_PARM7) return "Error in parameter 7";
            else if (lRet == Scard.OKERR_PARM8) return "Error in parameter 8";
            else if (lRet == Scard.OKERR_PARM9) return "Error in parameter 9";
            else if (lRet == Scard.OKERR_PARM10) return "Error in parameter 10";
            else if (lRet == Scard.OKERR_PARM11) return "Error in parameter 11";
            else if (lRet == Scard.OKERR_PARM12) return "Error in parameter 12";
            else if (lRet == Scard.OKERR_PARM13) return "Error in parameter 13";
            else if (lRet == Scard.OKERR_PARM14) return "Error in parameter 14";
            else if (lRet == Scard.OKERR_PARM15) return "Error in parameter 15";
            else if (lRet == Scard.OKERR_PARM16) return "Error in parameter 16";
            else if (lRet == Scard.OKERR_PARM17) return "Error in parameter 17";
            else if (lRet == Scard.OKERR_PARM18) return "Error in parameter 18";
            else if (lRet == Scard.OKERR_PARM19) return "Error in parameter 19";
            else if (lRet == Scard.OKERR_INSUFFICIENT_PRIV) return "You currently do not have the rights to execute the requested action. Usually a password has to be presented in advance.";
            else if (lRet == Scard.OKERR_PW_WRONG) return "The presented password is wrong";
            else if (lRet == Scard.OKERR_PW_LOCKED) return "The password has been presented several times wrong and is therefore locked. Usually use some administrator tool to unblock it.";
            else if (lRet == Scard.OKERR_PW_TOO_SHORT) return "The lenght of the password was too short.";
            else if (lRet == Scard.OKERR_PW_TOO_LONG) return "The length of the password was too long.";
            else if (lRet == Scard.OKERR_PW_NOT_LOCKED) return "The password is not locked";
            else if (lRet == Scard.OKERR_ITEM_NOT_FOUND) return "An item (e.g. a key of a specific name) could not be found";
            else if (lRet == Scard.OKERR_ITEMS_LEFT) return "There are still items left, therefore e.g. the directory / structure etc. can't be deleted.";
            else if (lRet == Scard.OKERR_INVALID_CFG_FILE) return "Invalid configuration file";
            else if (lRet == Scard.OKERR_SECTION_NOT_FOUND) return "Section not found";
            else if (lRet == Scard.OKERR_ENTRY_NOT_FOUND) return "Entry not found";
            else if (lRet == Scard.OKERR_NO_MORE_SECTIONS) return "No more sections";
            else if (lRet == Scard.OKERR_ITEM_ALREADY_EXISTS) return "The specified item alread exists.";
            else if (lRet == Scard.OKERR_ITEM_EXPIRED) return "Some item (e.g. a certificate) has expired.";
            else if (lRet == Scard.OKERR_UNEXPECTED_RET_VALUE) return "Unexpected return value";
            else if (lRet == Scard.OKERR_COMMUNICATE) return "General communication error";
            else if (lRet == Scard.OKERR_NOT_ENOUGH_MEMORY) return "Not enough memory";
            else if (lRet == Scard.OKERR_BUFFER_OVERFLOW) return "Buffer overflow";
            else if (lRet == Scard.OKERR_TIMEOUT) return "A timeout has occurred";
            else if (lRet == Scard.OKERR_NOT_SUPPORTED) return "The requested functionality is not supported at this time / under this OS / in this situation etc.";
            else if (lRet == Scard.OKERR_ILLEGAL_ARGUMENT) return "Illegal argument";
            else if (lRet == Scard.OKERR_READ_FIO) return "File IO read error";
            else if (lRet == Scard.OKERR_WRITE_FIO) return "File IO write error";
            else if (lRet == Scard.OKERR_INVALID_HANDLE) return "Invalid handle";
            else if (lRet == Scard.OKERR_GENERAL_FAILURE) return "General failure.";
            else if (lRet == Scard.OKERR_FILE_NOT_FOUND) return "File not found";
            else if (lRet == Scard.OKERR_OPEN_FILE) return "File opening failed";
            else if (lRet == Scard.OKERR_SEM_USED) return "The semaphore is currently use by an other process";
            else if (lRet == Scard.OKERR_NOP) return "No operation done";
            else if (lRet == Scard.OKERR_NOK) return "Function not executed";
            else if (lRet == Scard.OKERR_FWBUG) return "Internal error detected";
            else if (lRet == Scard.OKERR_INIT) return "Module not initialized";
            else if (lRet == Scard.OKERR_FIO) return "File IO error detected";
            else if (lRet == Scard.OKERR_ALLOC) return "Cannot allocate memory";
            else if (lRet == Scard.OKERR_SESSION_ERR) return "General error";
            else if (lRet == Scard.OKERR_ACCESS_ERR) return "Access not allowed";
            else if (lRet == Scard.OKERR_OPEN_FAILURE) return "An open command was not successful";
            else if (lRet == Scard.OKERR_CARD_NOT_POWERED) return "Card is not powered";
            else if (lRet == Scard.OKERR_ILLEGAL_CARDTYPE) return "Illegal cardtype";
            else if (lRet == Scard.OKERR_CARD_NOT_INSERTED) return "Card not inserted";
            else if (lRet == Scard.OKERR_NO_DRIVER) return "No device driver installed";
            else if (lRet == Scard.OKERR_OUT_OF_SERVICE) return "The service is currently not available";
            else if (lRet == Scard.OKERR_EOF_REACHED) return "End of file reached";
            else if (lRet == Scard.OKERR_ON_BLACKLIST) return "The ID is on a blacklist, the requested action is therefore not allowed.";
            else if (lRet == Scard.OKERR_CONSISTENCY_CHECK) return "Error during consistency check";
            else if (lRet == Scard.OKERR_IDENTITY_MISMATCH) return "The identity does not match a defined cross-check identity";
            else if (lRet == Scard.OKERR_MULTIPLE_ERRORS) return "Multiple errors have occurred. Use this if there is only the possibility to return one error code, but there happened different errors before (e.g. each thread returned a different error and the controlling thread may only report one).";
            else if (lRet == Scard.OKERR_ILLEGAL_DRIVER) return "Illegal driver";
            else if (lRet == Scard.OKERR_ILLEGAL_FW_RELEASE) return "The connected hardware whose firmware is not useable by this software";
            else if (lRet == Scard.OKERR_NO_CARDREADER) return "No cardreader attached";
            else if (lRet == Scard.OKERR_IPC_FAULT) return "General failure of inter process communication";
            else if (lRet == Scard.OKERR_WAIT_AND_RETRY) return "The service currently does not take calls. The task has to go back to the message loop and try again at a later time (Windows 3.1 only). The code may also be used, in every situation where a ‘  wait and retry ’  action is requested.";
            else if (lRet == 6)
                return "Missing context to resource manager: not established.";
            else
                return "Unknown Error: 0x";//& CStr(Hex(lRet))
            
        }
        //'
        //' Convert 1 Byte to String
        //'
        //Public Function GetString(ByVal curByte As Byte) As String
        public static String GetString(Byte curByte)
        {
            return curByte.ToString().Substring(0, 1);
            //    GetString = String(1, curByte)
        }
        //End Function

        //'
        //' Gets an array of bytes and its length and return a viewable
        //' dump of this byte-array
        //' For Example: a bytearray with values
        //'   array(0) = &H3B
        //'   array(1) = &HA2
        //' becomes the String "3B A2"
        //'
        //Public Function HexDump(response() As Byte, ByVal lenr As Integer) As String
        public static String HexDump(Byte[] response, int lenr)
        {
            //    Dim OUT As String
            //    Dim count As Integer
            //    Dim Value As Integer
            //    Dim ifvalue
            String OUT = "";
            int count;
            int Value = 0;
            double ifvalue;
            double otro = 0;
            
            //    For count = 0 To lenr - 1 Step 1
            //        Value = response(count)
            for (count = 0; count < lenr; count++)
            {
                Value = response[count];
                //        ifvalue = Fix(Value / 16)
                //        If ifvalue > -1 And ifvalue < 10 Then
                //            OUT = OUT & String(1, ifvalue + 48)
                //        Else
                //            If ifvalue > 9 And ifvalue < 16 Then
                //                OUT = OUT & String(1, ifvalue + 55)
                //            End If
                //        End If
                try
                {
                    ifvalue = Math.Truncate(double.Parse(Value.ToString()) / 16);
                    if (ifvalue > -1 && ifvalue < 10)
                        otro = ifvalue + 48;
                    else if (ifvalue > 9 && ifvalue < 16)
                        otro = ifvalue + 55;
                    //OUT += (int.Parse(otro.ToString())).ToString("X");
                    OUT += (int.Parse(otro.ToString())).ToString("X").Substring(0, 1);

                    //        Value = Fix(Value Mod 16)
                    double prueba = Math.Truncate(double.Parse(Value.ToString()) % 16);
                    Value = int.Parse(prueba.ToString());
                    //        If Value > -1 And Value < 10 Then
                    //            OUT = OUT & String(1, Value + 48)
                    //        Else
                    //            If Value > 9 And Value < 16 Then
                    //                OUT = OUT & String(1, Value + 55)
                    //            End If
                    //        End If
                    if (Value > -1 && Value < 10)
                        otro = Value + 48;
                    else if (Value > 9 && Value < 16)
                        otro = Value + 55;
                    //OUT += (int.Parse(otro.ToString())).ToString("X");//.Substring(0, 1);
                    OUT += (int.Parse(otro.ToString())).ToString("X").Substring(0, 1);

                    OUT += " ";
                    //        OUT = OUT & " "
                    //    Next count
                }
                catch (Exception ex)
                {
                }
            }
            //    HexDump = OUT
            return OUT;
            //End Function
        }
        //'
        //'
        //'
        //Public Function HexStrToInt(ByVal str As String) As Integer
        public static int HexStrToInt(String str)
        {
            //    Dim iii As Integer
            //    Dim iStrLen As Integer
            //    Dim iTmp As Integer
            //    Dim sTmp As String
            int iStrLen;
            int iTmp = 0;
            String sTmp;

            //    iStrLen = Len(str)
            iStrLen = str.Length;
            //    iTmp = 0
            //    For iii = 1 To iStrLen
            //        iTmp = iTmp * 16
            //        sTmp = Mid$(str, iii, 1)
            //        If (sTmp >= "0" And sTmp <= "9") Then
            //            iTmp = iTmp + Switch(1 = 1, sTmp)
            //        Else
            //            iTmp = iTmp + Switch(sTmp = "A", 10, _
            //                                 sTmp = "a", 10, _
            //                                 sTmp = "B", 11, _
            //                                 sTmp = "b", 11, _
            //                                 sTmp = "C", 12, _
            //                                 sTmp = "c", 12, _
            //                                 sTmp = "D", 13, _
            //                                 sTmp = "d", 13, _
            //                                 sTmp = "E", 14, _
            //                                 sTmp = "e", 14, _
            //                                 sTmp = "F", 15, _
            //                                 sTmp = "f", 15, _
            //                                 1 = 1, 0)
            //        End If
            //    Next iii
            iTmp = 0;
            for (int i = 0; i < iStrLen; i++)
            {
                iTmp = iTmp * 16;
                sTmp = str.Substring(i, 1);
                if (int.Parse(sTmp) >= 0 && int.Parse(sTmp) <= 9)
                    iTmp += int.Parse(sTmp);
                else
                {
                    switch (sTmp)
                    {
                        case "A":
                            iTmp += 10;
                            break;
                        case "a":
                            iTmp += 10;
                            break;
                        case "B":
                            iTmp += 11;
                            break;
                        case "b":
                            iTmp += 11;
                            break;
                        case "C":
                            iTmp += 12;
                            break;
                        case "c":
                            iTmp += 12;
                            break;
                        case "D":
                            iTmp += 13;
                            break;
                        case "d":
                            iTmp += 13;
                            break;
                        case "E":
                            iTmp += 14;
                            break;
                        case "e":
                            iTmp += 14;
                            break;
                        case "F":
                            iTmp += 15;
                            break;
                        case "f":
                            iTmp += 15;
                            break;
                    }
                }
                
            }
            //    HexStrToInt = iTmp
            return iTmp;
        }
        //End Function

        //'
        //' Convert query string to byte array
        //' (returns the size)
        //'
        //Public Function StrToArray(ByRef bArr() As Byte, ByVal str As String) As Integer
        public static int StrToArray(ref Byte[] bArr, String str)
        {
            //    Dim iii As Integer
            //    Dim iStrLen As Integer
            //    Dim iTmp As Integer
            //    Dim sTmp As String
            int iStrLen;
            //int iTmp = 0;
            //String sTmp;
            //    iStrLen = Len(str)
            iStrLen = str.Length;
            //    If (iStrLen = 0) Then
            //        StrToArray = 0
            //        Exit Function
            //    End If
            if (iStrLen == 0)
                return 0;
            //    If (iStrLen Mod 2) <> 0 Then
            //        StrToArray = 0
            //        Exit Function
            //    End If
            else if ((iStrLen % 2) != 0)
                return 0;
            //    ReDim bArr(1 To iStrLen / 2) As Byte

            //    iTmp = 0
            //    For iii = 1 To iStrLen
            //        sTmp = Mid$(str, iii, 1)
            //        If (sTmp >= "0" And sTmp <= "9") Or (sTmp >= "a" And sTmp <= "f") Or (sTmp >= "A" And sTmp <= "F") Then
            //            iTmp = iTmp + Switch(sTmp = "A", 10, _
            //                                 sTmp = "a", 10, _
            //                                 sTmp = "B", 11, _
            //                                 sTmp = "b", 11, _
            //                                 sTmp = "C", 12, _
            //                                 sTmp = "c", 12, _
            //                                 sTmp = "D", 13, _
            //                                 sTmp = "d", 13, _
            //                                 sTmp = "E", 14, _
            //                                 sTmp = "e", 14, _
            //                                 sTmp = "F", 15, _
            //                                 sTmp = "f", 15, _
            //                                 1 = 1, sTmp)
            //        Else
            //            MsgBox "Invalid Value ! Please enter hexadecimal values"
            //            Exit Function
            //        End If

            //        If (iii Mod 2) = 0 Then
            //            bArr((iii / 2)) = iTmp
            //            iTmp = 0
            //        End If
            //        iTmp = iTmp * 16
            //    Next iii

            //    StrToArray = iStrLen / 2
            return 0;
            //End Function
        }

        //'
        //' Establish Context and List Readers
        //'
        //Public Function ESTCONTEXTBUTTON_Click() As Long
        public static long ESTCONTEXTBUTTON_Click()
        {
            //    Dim lRet As Long
            //    Dim dwScope As Long
            long lRet = 0;
            int dwScope = 0;
            //    Dim mszReaders(2048) As Byte
            //    Dim mszGroup(1024) As Byte
            //    Dim pcchReaders As Long
            Byte[] mszReaders = new Byte[2048];
            Byte[] mszGroup = new Byte[1024];
            long pcchReaders;
            //    Dim iii As Integer

            //    ' if a Context is established, release it first
            //    If (g_isContext = True) Then
            //        Call RELCONTEXTBUTTON_Click
            //    End If
            if (g_isContext)
                RELCONTEXTBUTTON_Click();
            //    ' Set Scope (see Scard.bas "Scopes")
            //    dwScope = SCARD_SCOPE_USER
            dwScope = Scard.SCARD_SCOPE_USER;
            //    ' Establish Context
            //    lRet = SCardEstablishContext(dwScope, 0, 0, g_hContext)
            lRet = Scard.SCardEstablishContext(dwScope, 0, 0, ref g_hContext);
            //    If (lRet <> OKERR_OK) Then
            //        Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //        ESTCONTEXTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            if (lRet != OKERR_OK)
            {
                MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                return lRet;
            }
            //    ' Set maximum Length of mszReaders
            //    pcchReaders = 2048
            pcchReaders = 2048;
            //    ' Create a Multistring (terminated with two '\0's)
            //    mszGroup(0) = &H0
            //    mszGroup(1) = &H0
            mszGroup[0] = 0x0;
            mszGroup[1] = 0x0;
            //    lRet = SCardListReaders(g_hContext, mszGroup(LBound(mszGroup)), mszReaders(LBound(mszReaders)), pcchReaders)
            //    If (lRet <> OKERR_OK) Then
            //        Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //        ESTCONTEXTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            lRet = Scard.SCardListReaders(g_hContext, mszGroup[mszGroup.GetLowerBound(0)], 
                ref mszReaders[mszReaders.GetLowerBound(0)], ref pcchReaders);
            //'   ReaderList.Clear
            //    ' Split multistring and add single Readers to list
            //    For iii = 0 To pcchReaders - 2 Step 1
            //        If mszReaders(iii) = &H0 And iii <> 0 Then
            //'            ReaderList.AddItem (g_curReader)
            //'            ReaderList.ListIndex = 0
            //'            g_curReader = ""
            //'            iii = iii + 1
            //            Exit For
            //        End If

            //        g_curReader = g_curReader & GetString(mszReaders(iii))
            //    Next iii
            for (int i = 0; i < (pcchReaders - 1); i++)
            {
                if (mszReaders[i] == 0x0 && i != 0)
                    break;
                g_curReader += mszReaders[i].ToString();
            }
            //'    ' get selected Readername from ReaderList
            //'    If ReaderList.Text = "" Then
            //'        MESSAGETEXT.Text = ("No Reader Selected")
            //'        Exit Sub
            //'    Else
            //'        curReader = ReaderList.Text
            //'    End If
            //    g_isContext = True
            g_isContext = true;
            //    ESTCONTEXTBUTTON_Click = lRet
            return lRet;
            //End Function
        }
        //'
        //' Release context
        //'
        //Public Function RELCONTEXTBUTTON_Click() As Long
        public static long RELCONTEXTBUTTON_Click()
        {
            //    Dim lRet As Long
            long lRet = 0;
            //    ' if a Card is connected, disconnect first
            //    If (g_isCardConnected = True) Then
            //        Call DISCONNECTBUTTON_Click
            //    End If
            if (g_isCardConnected)
                DISCONNECTBUTTON_Click();
            //    ' check if a context is established
            //    If g_isContext = False Then
            //'        Call MsgBox("No context established", vbExclamation, "SCard Warning")
            //'        RELCONTEXTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            else
            {
                MessageBox.Show("No context established", "SCard Warning");
                return lRet;
            }
            //    ' release
            //    lRet = SCardReleaseContext(g_hContext)
            //    If (lRet <> OKERR_OK) Then
            //        Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //        RELCONTEXTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            lRet = Scard.SCardReleaseContext(g_hContext);
            if (lRet != OKERR_OK)
            {
                MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                return lRet;
            }
            //    g_isContext = False
            //    g_curReader = ""
            g_isContext = false;
            g_curReader = "";
            //    RELCONTEXTBUTTON_Click = lRet
            return lRet;
            //End Function
        }
        //'
        //' Connect to Card
        //'
        //Public Function CONNECTBUTTON_Click() As Long
        public static long CONNECTBUTTON_Click()
        {
            //    Dim lRet As Long
            long lRet = 0;
            //    Dim myByte As Byte
            //    Dim dwShareMode As Long
            //    Dim dwPreferredProtocols As Long
            //    Dim dwActiveProtocol As Long
            //Byte myByte;
            long dwShareMode;
            long dwPreferredProtocols;
            long dwActiveProtocol = 0;
            //    ' if a Card is connected, disconnect first
            //    If (g_isCardConnected = True) Then
            //        Call DISCONNECTBUTTON_Click
            //    End If
            if (g_isCardConnected)
                DISCONNECTBUTTON_Click();
            //    ' get selected Readername
            //    If (g_curReader = "") Then
            //'        Call MsgBox("No Reader Selected", vbExclamation, "SCard Warning")
            //'        CONNECTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            if (g_curReader == "")
            {
                MessageBox.Show("No Reader Selected", "SCard Warning");
                return lRet;
            } 
            //    ' Set Mode (see Scard.bas "Modes")
            //    dwShareMode = SCARD_SHARE_SHARED
            dwShareMode = Scard.SCARD_SHARE_SHARED;
            //    ' Set preferred Protocol (see Scard.bas "Protocols")
            //'   dwPreferredProtocols = SCARD_PROTOCOL_T0
            //    dwPreferredProtocols = SCARD_PROTOCOL_T0 Or SCARD_PROTOCOL_T1
            dwPreferredProtocols = Scard.SCARD_PROTOCOL_T0 ^ Scard.SCARD_PROTOCOL_T1;

            //    ' Connect
            //'    If (is_SMARTCARD_HCR360 = True) Then
            //'        ' 1. Connect to Reader Directly
            //'        ' 2. Send vendor IOCTLs (use: ScardControl)
            //'        dwShareMode = SCARD_SHARE_DIRECT
            //'        dwPreferredProtocols = SCARD_PROTOCOL_UNDEFINED
            //'        lRet = SCardConnect(g_hContext, g_curReader, dwShareMode, dwPreferredProtocols, g_hCard, dwActiveProtocol)
            //'    Else
            //        lRet = SCardConnect(g_hContext, g_curReader, dwShareMode, dwPreferredProtocols, g_hCard, dwActiveProtocol)
            //'    End If
            lRet = Scard.SCardConnect(g_hContext, g_curReader, dwShareMode, dwPreferredProtocols, ref g_hCard, ref dwActiveProtocol);
            //    If (lRet <> OKERR_OK And lRet <> OKERR_SCARD__W_REMOVED_CARD) Then
            //        Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //        CONNECTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            if (lRet != OKERR_OK && lRet != Scard.OKERR_SCARD__W_REMOVED_CARD)
            {
                MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                return lRet;
            }
            //    If (lRet <> OKERR_SCARD__W_REMOVED_CARD) Then
            //        g_isCardConnected = True
            //    End If
            if (lRet != Scard.OKERR_SCARD__W_REMOVED_CARD)
                g_isCardConnected = true;
                //    CONNECTBUTTON_Click = lRet
            return lRet;
            //End Function
        }
        //'
        //' Disconnects from Card
        //'
        //Public Function DISCONNECTBUTTON_Click() As Long
        public static long DISCONNECTBUTTON_Click()
        {
            //    Dim lRet As Long
            //    Dim dwDisposition As Long
            long lRet = 0;
            long dwDisposition = 0;
            //    ' check if a card is connected
            //    If (g_isCardConnected = False) Then
            //'        Call MsgBox("Not connected", vbExclamation, "SCard Warning")
            //'        DISCONNECTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            if (!g_isCardConnected)
                return 0;
            //    ' Set Disposition (see Scard.bas "Dispositions")
            //    dwDisposition = SCARD_LEAVE_CARD
            dwDisposition = Scard.SCARD_LEAVE_CARD;
            //    'Disconnect from Card
            //    lRet = SCardDisconnect(g_hCard, dwDisposition)
            //    If (lRet <> OKERR_OK) Then
            //        Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //        DISCONNECTBUTTON_Click = lRet
            //        Exit Function
            //    End If
            lRet = Scard.SCardDisconnect(g_hCard, dwDisposition);
            if (lRet != OKERR_OK)
            {
                MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                return lRet;
            }
            //    g_isCardConnected = False
            g_isCardConnected = false;
            //    DISCONNECTBUTTON_Click = lRet
            return lRet;
            //End Function
        }

        //'
        //' Read from 2WCard/3WCard
        //'
        //' Parameters:
        //'       ulBytesToRead   = Number of Bytes to Read
        //'       pbData          = Array of Bytes containing data
        //'       ulAddress       = Offset where read starts
        //Public Function READBUTTON_Click(strCardType As String, ByVal ulBytesToRead As Long, pbData As String, ByVal ulAddress As Long) As Long
        public static long READBUTTON_Click(String strCardType, long ulBytesToRead, String pbData, long ulAddress)
        {
            //    Dim lRet As Long
            //    Dim bData() As Byte
            //    Dim iii As Integer
            long lRet = 0;
            Byte[] bData = new Byte[ulBytesToRead];
            //    ReDim bData(ulBytesToRead)
            //    If (strCardType = "2W") Then
            //        ' Read
            //        lRet = SCard2WBPReadData(g_hCard, ulBytesToRead, bData(LBound(bData)), ulAddress)
            //        If (lRet <> OKERR_OK) Then
            //            Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //            READBUTTON_Click = lRet
            //            Exit Function
            //        End If
            //    End If
            if (strCardType == "2W")
            {
                lRet = SCard2WBP.SCard2WBPReadData(g_hCard, ulBytesToRead, ref bData[bData.GetLowerBound(0)], ulAddress);
                if (lRet != OKERR_OK)
                {
                    MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                    return lRet;
                }
            }
            //    If (strCardType = "3W") Then
            //        ' Read
            //        lRet = SCard3WBPReadData(g_hCard, ulBytesToRead, bData(LBound(bData)), ulAddress)
            //        If (lRet <> OKERR_OK) Then
            //            Call MsgBox(ScardHandleError(lRet), vbCritical, "SCard Error")
            //            READBUTTON_Click = lRet
            //            Exit Function
            //        End If
            //    End If
            else if (strCardType == "3W")
            {
                lRet = SCard3WBP.SCard3WBPReadData(g_hCard, ulBytesToRead, ref bData[bData.GetLowerBound(0)], ulAddress);
                if (lRet != OKERR_OK)
                {
                    MessageBox.Show(ScardHandleError(lRet), "SCard Error");
                    return lRet;
                }
            }
            //    ' Create a Hexdump from returned data
            //'    pbData = pbData & HexDump(bData, ulBytesToRead)

            //    pbData = ""
            //    For iii = 0 To ulBytesToRead - 1
            //        pbData = pbData & GetString(bData(iii))
            //    Next iii
            pbData = "";
            for (int i = 0; i < ulBytesToRead; i++)
                pbData += bData[i].ToString();
            //    READBUTTON_Click = lRet
            return lRet;
            //End Function
        }*/

    }
}
