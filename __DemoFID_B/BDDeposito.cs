﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using IrdsMensajes;
using System.Configuration;

namespace __DemoFID_B
{
    public class BDDeposito
    {
        public static DataTable
            ObtenerDeposito(int p_IdDeposito)
        {
            String l_Query
                = "SELECT               IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado "
                + "FROM                 Deposito "
                + "WHERE                IdDeposito = @IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Deposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
            ObtenerDetalleDeposito(int p_IdDeposito)
        {
            String l_Query
                = "SELECT           a.IdDenominacion, a.Cantidad, b.Codigo "
                + "FROM             Denominacion b, DetalleDeposito a "
                + "WHERE            b.IdDenominacion = a.IdDenominacion "
                + "AND              IdDeposito = @IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("DetalleDeposito");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static DataTable
            BuscarDepositosNoEnviados()
        {
            String l_Query
                = "SELECT               ProcessId, IdDeposito, IdCuenta, IdMoneda, TotalDeposito, IdUsuario, Equipo "
                + "FROM                 Deposito "
                + "WHERE                Enviado = 0 "
                + "ORDER BY             IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                SqlDataAdapter l_Adaptador = new SqlDataAdapter(l_Comando);
                DataTable l_Datos = new DataTable("Depositos");
                l_Adaptador.Fill(l_Datos);
                return l_Datos;
            }
        }

        public static bool
            ConfirmarDeposito(int p_IdDeposito)
        {
            String l_Query
                = "UPDATE               Deposito "
                + "SET                  Enviado = 1 "
                + "WHERE                IdDeposito = @IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            ActualizarProcessId(int p_IdDeposito, Int64 p_IdProceso)
        {
            String l_Query
                = "UPDATE           Deposito "
                + "SET              ProcessId = @ProcessId "
                + "WHERE            IdDeposito = @IdDeposito ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                l_Comando.Parameters.AddWithValue("@ProcessId", p_IdProceso);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                if (l_Comando.ExecuteNonQuery() != 1)
                    return false;
                else
                    return true;
            }
        }

        public static bool
            InsertarDeposito(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, Double p_TotalDeposito
                , DataTable p_DatosDetalle, String p_IdUsuario, out int p_IdDeposito)
        {
            
            p_IdDeposito = FolioDeposito();

            String l_Query
                = "INSERT INTO          Deposito "
                + "                     (IdDeposito, IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado, Retirado,Equipo,Bolsa) "
                + "VALUES               (@IdDeposito, @IdUsuario, @IdCuenta, @IdCajon, @IdMoneda, @FechaHora, @TotalDeposito, 0, 0,@Equipo,@Bolsa) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_IdCuenta);
                l_Comando.Parameters.AddWithValue("@IdCajon", p_IdCajon);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                l_Comando.Parameters.AddWithValue("@TotalDeposito", p_TotalDeposito);
                l_Comando.Parameters.AddWithValue("@Equipo", Globales.Equipo);
                l_Comando.Parameters.AddWithValue("@Bolsa", Globales.NumeroSerieBOLSA );
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                if (!InsertarDetalleDeposito(p_DatosDetalle, p_IdDeposito, l_Conexion, l_Transaccion))
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();


                    InsertarCiclo(p_IdDeposito);

                

                    //MensajeDeposito1 l_Irds = new MensajeDeposito1(
                    //Globales.Estacion     // id estacion
                    //, p_IdDeposito   // folio transacción
                    //, FolioCiclo()    // ciclo operacional
                    //, DateTime.Now.AddMinutes(-3) // fecha hora inicio
                    //, DateTime.Now // fecha hora fin
                    //, Globales.IdUsuario // string p_IdOperadorLocal
                    //, Globales.NombreUsuario   // String nombre completo del operador local
                    //, "SuperUser"               // Usuario global del sistema que realiza la operación o que creo el operador local
                    //, Moneda.MonedaPesos // Moneda p_IdMoneda
                    //, 1 // IdCuenta  ID Global cuenta del sistema
                    //, Globales.ReferenciaUsuario //string p_Referencia
                    //, l_saldoActual // decimal p_SaldoAnterior
                    //, (decimal)p_TotalDeposito // decimal p_MontoTransaccion
                    //, 0
                    //, 0         // int m_TotalIncidentes
                    //);

                    //foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
                    //{

                    //    l_Irds.AgregarDenominacion(1, Int32.Parse (l_Detalle[2].ToString()), (Int32)l_Detalle[1]); // 10 de 100$ en contenedor 1
                    //}

                    //ColaMensajesIrdsHttp l_Cola = new ColaMensajesIrdsHttp(1);
                    //l_Cola.AgregarMensaje(l_Irds);


                    //try
                    //{
                    //    ColaMensajesIrdsHttp l_Cola_envios = new ColaMensajesIrdsHttp(2);

                    //    l_Cola_envios.MandarMensajesPendientes(Globales.Estacion);
                    //    Globales.EscribirBitacora("Mensajes Pendientes ", "Lanzados ", "Sin Problemas", 1);

                    //}
                    //catch (Exception exxx)
                    //{
                    //    Globales.EscribirBitacora("Exception AL procesar Menasajes Pendientes", "Error", exxx.Message, 1);
                    //}

                    return true;

                    
                }
            }
        }

        public static bool
          Insertar_Deposito(String p_IdCuenta, int p_IdCajon, int p_IdMoneda, Double p_TotalDeposito
              , DataTable p_DatosDetalle, String p_IdUsuario, String p_Equipo, out int p_IdDeposito)
        {
            p_IdDeposito = FolioDeposito();
            String l_Query
                = "INSERT INTO          Deposito "
                + "                     (IdDeposito, IdUsuario, IdCuenta, IdCajon, IdMoneda, FechaHora, TotalDeposito, Enviado, Retirado, Equipo) "
                + "VALUES               (@IdDeposito, @IdUsuario, @IdCuenta, @IdCajon, @IdMoneda, @FechaHora, @TotalDeposito, 0, 0,@Equipo) ";

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                SqlTransaction l_Transaccion = l_Conexion.BeginTransaction();
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion, l_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                l_Comando.Parameters.AddWithValue("@IdUsuario", p_IdUsuario);
                l_Comando.Parameters.AddWithValue("@IdCuenta", p_IdCuenta);
                l_Comando.Parameters.AddWithValue("@IdCajon", p_IdCajon);
                l_Comando.Parameters.AddWithValue("@IdMoneda", p_IdMoneda);
                l_Comando.Parameters.AddWithValue("@FechaHora", DateTime.Now);
                l_Comando.Parameters.AddWithValue("@TotalDeposito", p_TotalDeposito);
                l_Comando.Parameters.AddWithValue("@Equipo", p_Equipo);
                if (l_Comando.ExecuteNonQuery() != 1)
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                if (!InsertarDetalleDeposito(p_DatosDetalle, p_IdDeposito, l_Conexion, l_Transaccion))
                {
                    l_Transaccion.Rollback();
                    return false;
                }
                else
                {
                    l_Transaccion.Commit();

                   
                    return true;
                }
            }
        }

        private static bool
            InsertarDetalleDeposito(DataTable p_DatosDetalle, int p_IdDeposito, SqlConnection p_Conexion
                , SqlTransaction p_Transaccion)
        {
            String l_Query
                = "INSERT INTO              DetalleDeposito "
                + "                         (IdDeposito, IdDenominacion, Cantidad) "
                + "VALUES                   (@IdDeposito, @IdDenominacion, @Cantidad) ";

            foreach (DataRow l_Detalle in p_DatosDetalle.Rows)
            {
                SqlCommand l_Comando = new SqlCommand(l_Query, p_Conexion, p_Transaccion);
                l_Comando.Parameters.AddWithValue("@IdDeposito", p_IdDeposito);
                l_Comando.Parameters.AddWithValue("@IdDenominacion", (int)l_Detalle[0]);
                l_Comando.Parameters.AddWithValue("@Cantidad", (Int32)l_Detalle[1]);
                try
                {
                    if (l_Comando.ExecuteNonQuery() != 1)
                        return false;
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Insertar Detalle Deposito", "Error Consulta : ", ex.Message, 1);
                    return false;
                }
            }

            return true;
        }

        private static int
            FolioDeposito()
        {
            String l_Query
                = "SELECT MAX           (IdDeposito) "
                + "FROM                 Deposito ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Folio Deposito", "Error Consulta : ", ex.Message, 1);
                }
            }
            l_Folio++;
            return l_Folio;
        }

        public static int ObtenerTotalBilletes()
        {
           int  l_Folio=0;
            String l_Query
              = "SELECT           SUM(a.Cantidad) as Totalbilletes "
              + "FROM             Deposito b "
              + "INNER JOIN       DetalleDeposito a "       
              + "ON               b.IdDeposito = a.IdDeposito "
              + "WHERE            b.Retirado = 0 ";


                         

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {

               
                SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);

                SqlDataReader l_Lector = l_Comando.ExecuteReader();
                if (l_Lector.Read())
                    l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Obtener Total billetes", "Error Consulta : ", ex.Message, 1);
                    l_Folio = 0;                  
                }

                return l_Folio;
            }
        }

        public static Double ObtenerMontoActual()
        {
            Double l_Monto = 0;
            String l_Query
              = "SELECT            SUM(TotalDeposito) AS MontoActual "
              + "FROM             Deposito "
              + "WHERE            Retirado = 0 ";





            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {


                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);

                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Monto = Double.Parse ( l_Lector[0].ToString());
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Obtener Monto Actual", "Error Consulta : ", ex.Message, 1);
                    l_Monto = 0;
                }

                return l_Monto;
            }
        }

        public static int
         FolioCiclo()
        {
            String l_Query
                = "SELECT MAX           (IdCicloOperacion) "
                + "FROM                 CicloOperacion ";
            int l_Folio = 0;

            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try
                {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    SqlDataReader l_Lector = l_Comando.ExecuteReader();
                    if (l_Lector.Read())
                        l_Folio = (int)l_Lector[0];
                }
                catch (Exception ex)
                {
                    Globales.EscribirBitacora("Folo CicloOperacion", "Error Consulta : ", ex.Message, 1);
                }
            }

            return l_Folio;
        }

        public static void
          InsertarCiclo(int p_folioDeposito)
        {
            int p_IdCiclo = FolioCiclo();
            String l_Query
                = "Insert INTO   CicloOperacion "
                + " (IdCicloOperacion, IdDeposito, IdRetiro) "
                + "VALUES (@IdCicloOperacion,@IdDeposito,0)";
            using (SqlConnection l_Conexion = UtilsBD.NuevaConexion())
            {
                try {
                    SqlCommand l_Comando = new SqlCommand(l_Query, l_Conexion);
                    l_Comando.Parameters.AddWithValue("@IdCicloOperacion", p_IdCiclo);
                    l_Comando.Parameters.AddWithValue("@IdDeposito", p_folioDeposito);
                    l_Comando.ExecuteNonQuery();
                }catch(Exception ex)
                {
                    Globales.EscribirBitacora("Folo CicloOperacion", "Error al Escribir el Ciclo de Operación : ", ex.Message, 1);
                }

                }

        }

    }
}
