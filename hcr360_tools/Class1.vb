﻿Public Class Lector

    Public Shared Function BinToHexStr(ByVal baInPut() As Byte, ByVal nLen As Integer) As String
        Dim Buf As String
        Dim i As Integer
        Dim Value As Byte

        For i = 0 To (nLen - 1)
            Value = baInPut(i)
            If Value <= 15 Then
                Buf += "0" + Hex$(Value)
            Else
                Buf += Hex$(Value)
            End If
        Next i
        BinToHexStr$ = Buf
    End Function

    Public Shared Function HexStrToBin(ByVal strInput As String, ByVal baOutPut() As Byte) As Integer
        Dim i As Integer, j As Integer
        Dim nStrLen As Integer
        Dim Buf As String

        nStrLen = Len(strInput)

        j = 0
        For i = 1 To nStrLen Step 2
            Buf$ = "&H" + Mid$(strInput$, i, 2)
            baOutPut(j) = Val(Buf$)
            j = j + 1
        Next i
        HexStrToBin% = nStrLen / 2
    End Function

    Public Shared Function BinAryToAsciiStr(ByVal baInPut() As Byte, ByVal nLen As Integer) As String
        Dim Buf As String
        Dim i As Integer
        Dim Value As Byte

        For i = 0 To (nLen - 1)
            Value = baInPut(i)
            If Value <= 15 Then
                Buf += "<0" + Hex$(Value) + ">"
            ElseIf Value <= 31 Or Value >= 127 Then
                Buf += "<" + Hex$(Value) + ">"
            Else
                Buf += Chr(Value)
            End If
        Next i
        BinAryToAsciiStr = Buf
    End Function

    Public Shared Function HexStrToAsciiStr(ByVal HexStr As String) As String
        Dim Buf As String
        Dim i As Integer
        Dim Value As Byte

        For i = 1 To Len(HexStr) Step 2
            Value = Val("&H" + Mid(HexStr, i, 2))
            If Value <= 15 Then
                Buf = Buf & "<0" & Hex(Value) & ">"
            ElseIf Value <= 31 Or Value >= 127 Then
                Buf += "<" + Hex(Value) + ">"
            Else
                Buf += Chr(Value)
            End If
        Next i
        HexStrToAsciiStr = Buf
    End Function
    Public Shared Function CharToCharCode(ByVal CharOrignial As Char) As Integer
        Dim Retorno As Integer
        Retorno = Asc(CharOrignial)
        CharToCharCode = Retorno
    End Function

End Class
