//
// CTS Electronics
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// July 2011

//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.ctsgroup.it		techsupp@ctsgroup.it
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   SID.H
//
//  PURPOSE:  SID Include Interface


#ifndef SID_H
#define SID_H     1


// ------------------------------------------------------------------------
//                     DEFINES
// ------------------------------------------------------------------------

// Parameter ResetType
#define RESET_ERROR						0x30
#define RESET_FREE_PATH					0x31

// Parameter History
#define CMD_READ_HISTORY				1
#define CMD_ERASE_HISTORY				2


// Parameter Type DoubleLeafing
#define DOUBLE_LEAFING_WARNING			0
#define DOUBLE_LEAFING_DISABLE			7


 // Parameter ClearBlack
#define NO_CLEAR_BLACK					0
#define CLEAR_AND_ALIGN_IMAGE			2

// Parameter CashType
#define CURRENCY_1						1
#define CURRENCY_2						2
#define CURRENCY_3						4
#define CURRENCY_4						8

#define CARD_SLE4442					0

enum
{
	VALIDATION_0 = 0 ,  //accept all documents and store in the bag
	VALIDATION_1 = 1,   //certified notes stored in the bag, other documents are ejected.
	VALIDATION_2 = 2,   //all documents are ejected
	VALIDATION_3 = 3,   //certified notes are ejected, documents not certified are stored in the bag.
	VALIDATION_4 = 4    //the suspect notes are captured
};

// ------------------------------------------------------------------------
//					DEFINES STRUCTURES
// ------------------------------------------------------------------------
typedef struct _UNITSTATUS
{
	int		Size ;
	int		UnitStatus;	
	BOOL	EntranceSensor1;
	BOOL	EntranceSensor2 ;
	BOOL	ShutterSensor1;
	BOOL	ShutterSensor2 ;
	BOOL	FeederFlapSensor;
	BOOL	RejectEntranceSensor1 ;
	BOOL	RejectEntranceSensor2 ;
	BOOL	BagEntranceSensor1 ;
	BOOL	BagEntranceSensor2 ;
	BOOL	FeederSensor ;
	BOOL	FeederEmptySensor ;
	BOOL	DoubleLeafingSensor1 ;
	BOOL	DoubleLeafingSensor2 ;
	BOOL	DoubleLeafingSensor3 ;	
	BOOL	MicroSwitchtensionerBag1 ;
	BOOL	MicroSwitchtensionerBag2 ;			
	BOOL	RejectBaySensor ;
	BOOL	bagsensor1 ;
	BOOL	bagsensor2 ;
	BOOL	bagsensor3 ;
	BOOL	bagsensor4 ;
	BOOL	bagsensor5 ;
	BOOL	bagsensor6 ;	
	BOOL	CounterMotorStepBagsensor ;		
	BOOL	InterlockDoorStrongboxClosed ;
	BOOL	MicroSwitchDoorOpen ;
	BOOL	BagPresent ;	
	BOOL	BagClosed ;
	BOOL	BagOpen ;
	BOOL	Codelockcorrect ;
	BOOL	CodelockcorrectwithAlarm ;
	BOOL	MicroSwitchelevatorpositionUP ;	
	BOOL	MicroSwitchelevatorpositionDOWN ;		
	BOOL	SensorDipSwitchLeaferOpen ;
	BOOL	SensorDipSwitchMagneticDoor1Open ;
	BOOL	SensorDipSwitchMagneticDoor2Open ;
	BOOL	MicroSwitchUnitOpen ;	
	BOOL	InterlockCoverHeadOpen ;
	BOOL	Reserved ;
	BOOL	Reserved1;
	BOOL	Reserved2 ;
	BOOL	Reserved3 ;
	BOOL	Reserved4 ;
	BOOL	Reserved5 ;
	BOOL	Reserved6 ;
	BOOL	Reserved7 ;
	BOOL	Reserved8 ;
	BOOL	Reserved9 ;
	

} UNITSTATUS, *PUNITSTATUS;






// ------------------------------------------------------------------------
//                          REPLY-CODE
// ------------------------------------------------------------------------

#define		SID_OKAY								0

// ------------------------------------------------------------------------
//                  ERRORS
// ------------------------------------------------------------------------
#define		SID_SYSTEM_ERROR						-1
#define		SID_USB_ERROR							-2
#define		SID_UNIT_NOT_FOUND						-3
#define		SID_HARDWARE_ERROR						-4
#define		SID_INVALID_COMMAND						-9
#define		SID_COMMAND_IN_EXECUTION_YET			-10
#define		SID_COMMAND_SEQUENCE_ERROR				-11
#define		SID_PC_HW_ERROR							-12
#define		SID_INVALID_HANDLE						-13
#define		SID_IMAGE_NO_FILMED						-14	
#define		SID_BMP_ERROR							-15
#define     SID_MISSING_IMAGE						-16
#define		SID_NO_LIBRARY_LOAD						-17
#define		SID_IMAGE_NOT_PRESENT					-18
#define		SID_OPEN_NOT_DONE						-19
#define		SID_INVALID_CLEARBLACK					-20
#define		SID_INVALID_SIDE						-21
#define		SID_INVALID_SCANMODE					-22
#define		SID_INVALID_CMD_HISTORY					-23
#define		SID_MISSING_BUFFER_HISTORY				-24
#define		SID_CALIBRATION_FAILED					-25
#define		SID_INVALID_DOC_LENGTH					-26
#define		SID_INVALID_RESET_TYPE					-27
#define		SID_INVALID_CASHTYPE					-28
#define		SID_INVALID_TYPE						-29
#define		SID_INVALID_VALUE						-30
#define		SID_MISSING_FILENAME					-32
#define     SID_OPEN_FILE_ERROR_OR_NOT_FOUND		-33
#define     SID_POWER_OFF							-34
#define     SID_UPS_NOT_ANSWER						-35
#define		SID_OPERATION_ERROR						-36
#define		SID_UPS_BATTERY_KO						-37
#define		SID_UPS_ERROR_COMUNICATION				-38
#define		SID_INVALID_NRDOC						-39
#define		SID_INVALID_QUALITY						-40
#define		SID_JPEG_ERROR							-41
#define     SID_INVALID_VALIDATION					-42
#define		SID_PERIPHERAL_RESERVED					-43

#define     SID_ILLEGAL_REQUEST						-73



//Section Web Cam
#define		SID_CALIBRATION_WEBCAM_ERROR			-99
#define		SID_INVALID_WEBCAM_TYPE					-100
#define		SID_INVALID_CAPTURE						-101
#define		SID_ERROR_FRAME							-102
#define		SID_NOTES_CHECKED						-103
#define		SID_INVALID_SHAPE_FOUND					-104
#define		SID_LIGHT_LOW							-105
#define		SID_ERROR_WEB							-106
#define		SID_TEMPLATE_FILE_NOT_FOUND				-107

//Section Jam
#define		SID_JAM_IN_FEEDER_IN					-200
#define		SID_JAM_SWITCH_PATH_IN					-201
#define		SID_JAM_BOX_REJECT_IN					-202
#define		SID_JAM_ENTER_BAG_IN					-203
#define		SID_JAM_IN_FEEDER_OUT					-204
#define		SID_JAM_SWITCH_PATH_OUT					-205
#define		SID_JAM_BOX_REJECT_OUT					-206
#define		SID_JAM_ENTER_BAG_OUT					-207
#define		SID_REJECT_BAY_FULL						-208
#define		SID_JAM_PHOTO_SCANNER					-209
#define		SID_REJECT_BAY_NOTEMPTY					-210
#define		SID_UNIT_DOC_TOO_LONG					-213
#define		SID_JAM_PRESS_NOTES						-214



#define		SID_WELD_NOT_POSSIBLE					-215
#define		SID_WELD_NOT_PERMITTED_NO_DOC_INSERTED	-216
#define		SID_ERROR_ON_CLOSE_BAG					-217
#define		SID_CURRENT_ERROR						-218
#define		SID_ERROR_WELD_PROTECTION				-219
#define		SID_TIME_OUT_EXPIRED					-220
#define		SID_SENSOR_DOOR_OPEN					-221
#define		SID_BAG_NOT_PRESENT						-222
#define		SID_BAG_CLOSED							-223
#define		SID_BAG_NOT_OPEN						-224
#define		SID_LOCK_ERROR							-225
#define		SID_BAG_PRESENT							-226
#define		SID_DIP_SWITCH_HIGHT					-227
#define		SID_DIP_SWITCH_NOT_LOW					-228
#define		SID_CODELOCCK_NOT_INSERTED				-229
#define		SID_ERROR_ELEVATOR						-230
#define		SID_ERROR_TENSIONER_1					-231
#define		SID_BAG_NOT_REMOVED						-232
#define		SID_ERROR_TENSIONER_2					-233

#define     SID_DIP_SWITCH_DOOR_LEAFER_OPEN			-240
#define     SID_DIP_SWITCH_SP_MAGN1_OPEN			-241
#define     SID_DIP_SWITCH_SP_MAGN2_OPEN			-242
#define		SID_DIP_SWITCH_SID_OPEN					-243
#define		SID_INTERLOCK_OPEN						-245

#define		SID_REJECT_DOCUMENT_NOT_RECOGNIZED		-345
#define		SID_REJECT_STOP_DOCUBLE_LEAFING			-346
#define		SID_REJECT_SUSPECT						-347

#define		SID_BARCODE_TIME_OUT_EXPIRED			-800
#define		SID_BARCODE_COM_ERROR					-801
#define		SID_BARCODE_STRING_TRUNCATED		    -802



#define		SID_ERROR_RECOGNIZE_SKEWED				-1101
#define		SID_ERROR_RECOGNIZE_VALIDATION_SIZE		-1102
#define		SID_ERROR_RECOGNIZE_CLASSIFICATION		-1110
#define		SID_ERROR_RECOGNIZE_VALIDATION			-1111
#define		SID_ERROR_RECOGNIZE_IR					-1112
#define		SID_ERROR_RECOGNIZE_MGT					-1113
#define		SID_ERROR_RECOGNIZE_UV					-1114

#define		SID_BADGE_NOT_PRESENT					-1200 
#define		SID_BADGE_CHECK_CARD_PRESENCE_ERROR		-1201
#define		SID_BADGE_SELECT_MEMCARDTYPE_ERROR		-1202
#define		SID_BADGE_MEMORY_READ_DATA				-1203
#define		SID_BADGE_NO_VALID_DATA					-1204
#define		SID_BADGE_OPEN_ERROR					-1205
#define		SID_BADGE_STATUS_ERROR					-1206

#define		SID_PRINTER_ERROR						-1230
#define		SID_PRINTER_STATUS_ERROR				-1231
#define		SID_PRINTER_PAPER_NOT_PRESENT			-1232

#define 	SID_RESTART_WELD						-1999


// ------------------------------------------------------------------------
//                  WARNINGS
// ------------------------------------------------------------------------
#define		SID_FEEDER_EMPTY						1
#define		SID_ALREADY_OPEN						5
#define		SID_PERIF_BUSY							6
#define     SID_REPORT_NOT_DEPOSTITED				7

#define		SID_COMMAND_NOT_SUPPORTED				13
#define		SID_TRY_TO_RESET						14
#define		SID_STRING_TRUNCATED					15
#define     SID_DOUBLE_LEAFING_WARNING				16

#define		SID_WARNING_ALARM_CODE					30




// ------------------------------------------------------------------------
//					DEFINES STRUTTURES
// ------------------------------------------------------------------------

// structure for read usefull information about the configuration peripheral 
typedef struct _SidConfiguration
{
	long            Size;                        // Size of the struct
	BOOL            Magnetic_18Channel;          // If FALSE MICR head with 9 channel
	BOOL            Door_Sensor;                 // Presence of the door
	BOOL            Lock_Sensor;                 // Presence of the Lock
	BOOL			Uv_Sensor ;					 // Presence of the UvSensor
	BOOL			CashControlLicense;
} SID_CONFIGURATION, *LP_SID_CONFIGURATION;




// structure for read usefull information about the peripheral life
typedef struct _History
{
	long			Size;					// Size of the struct
	unsigned long	time_peripheral_on;		// Seconds peripheral time life
	unsigned long	nr_power_on;			// Nr. of time power ON
	unsigned long	num_doc_handled;		// Nr. documents handled
	unsigned long	nr_double_leafing;		// Nr. double leafing occurs
	unsigned long	jam_feeder;				// Nr. jam in the feeder
    unsigned long   jam_switch_path_sensor ;// Nr. jam in switch path sensor
	unsigned long   jam_box_reject ;        // Nr. jam in the box reject
	unsigned long   jam_bag ;				// Nr. jam enter in bag
	unsigned long	nr_weld;	            // Nr Weld
	unsigned long	nr_banknote_inclined_clockwise;
	unsigned long	nr_banknote_inclined_anticlockwise;

} S_HISTORY, *LPS_HISTORY;




// ------------------------------------------------------------------------
//                  EXPORT FUNCTIONS
// ------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif


extern int APIENTRY SID_Open(BOOL 	fReadImages);

extern int APIENTRY SID_Initialize();

extern int APIENTRY SID_Close();

extern int APIENTRY SID_GetVersion(char *String,short Length);

extern int APIENTRY SID_Identify( SID_CONFIGURATION *pCfg,
								   LPSTR	FwBaseVersion,
								   LPSTR	DateFw,
								   LPSTR	SerialNrID,
								   LPSTR	FwPocket,
								   LPSTR	FPGABaseVersion,
								   LPSTR	ValidateVersion,
								   LPSTR	DateValidate,
								   LPSTR	FwOpticExpansion,
								   LPSTR	FPGAExpansionVersion,
								   LPSTR	Id_kiosk  );


extern int APIENTRY SID_UnitIdentify( SID_CONFIGURATION *SidBytesCfg,
								   LPSTR	FwBaseVersion,
								   LPSTR	DateFw,
								   LPSTR	SerialNrID,
								   LPSTR	FwBag,
								   LPSTR	FPGABaseVersion,
								   LPSTR	SecondFwVersion,
								   LPSTR	SecondDate,
								   LPSTR	FwValidatorVersion,
								   LPSTR	FPGAExpansionVersion,
								   LPSTR	Id_kiosk  );


extern int APIENTRY SID_ConfigDoubleLeafingAndDocLength(long Type,short	Value,long ValuePolymer,long Reserved);

extern int APIENTRY SID_ConfigDoubleLeafing(long Type,short ValuePaper,short ValuePolymer ,short Reserved);

extern int APIENTRY SID_CashIn(unsigned short	NrDocProcess,
							    long		 	CashType,
								long			fValidation,
								BOOL	 		fGetImages,
								long			Reserved ,
								long			Reserved1);

extern int APIENTRY SID_UnitStatus(  unsigned char	*SenseKey,
									 unsigned char	*StatusByte,
									 unsigned short *Reserved1,
									 unsigned short *Reserved2,
									 unsigned short *Reserved3);

extern int APIENTRY SID_DeviceStatus(UNITSTATUS *lpStatus);

extern  int APIENTRY SID_GetCashTotalBag(unsigned short *TotalNotesStored);

extern int APIENTRY SID_GetCashTotalSuspect(unsigned short *TotalNotesStoredSuspect);

extern int APIENTRY SID_ClearDenominationNote(); 

extern int APIENTRY SID_ReadImage(short		ClearBlack,
								  LPHANDLE	FrontImage,
								  LPHANDLE	BackImage,
								  LPHANDLE	FrontImageIr,
								  LPHANDLE	BackImageIr,
								  LPHANDLE	Reserved1,
								  LPHANDLE	Reserved2,
								  long      *Reserved3);

extern int APIENTRY SID_SaveDIB(HANDLE hImage, LPSTR filename);

extern int APIENTRY SID_Reset(char ResetType);

extern  int APIENTRY SID_JoinBag(long Operation);

extern int APIENTRY SID_JoinBagResult( long *WeldPercentageExecuted );

extern int APIENTRY SID_EnableToOpenDoor();

extern int APIENTRY SID_HistoryCommand(short Cmd,S_HISTORY	*sHistory);

extern int APIENTRY SID_FreeImage(LPHANDLE hImage);

extern int APIENTRY SID_UPSStatus(BOOL *fPowerOn , BOOL *fLowBattery ,BOOL *Reserved2);

extern int APIENTRY SID_UPSShutDown(long TimeOut );

extern int APIENTRY SID_ReadBarcode(LPSTR BarcodeRead,short	*len_Buff,long TimeOut);

extern int APIENTRY SID_ReadBarcodeEx(LPSTR BuffBarcode,short *len_Buff ,long TimeOut);

extern int APIENTRY SID_OpenBag(long fOperation);

extern int  APIENTRY SID_ForceOpenDoor();

extern int APIENTRY SID_WaitReplaceBag(long timeOut);

extern int APIENTRY SID_WaitRemoveBag(long timeOut);

extern int APIENTRY SID_WaitDoorOpen();

extern int APIENTRY SID_CheckCodeLockCorrect(long timeOut);

extern  int APIENTRY SID_WaitDoorClosed(long timeOut);

extern int APIENTRY SID_UnlockCover();

extern int APIENTRY SID_EnableTestNote(long Operation);

extern int APIENTRY SID_WaitValidationResult(short *NoteType);

extern int APIENTRY SID_RecognizeErrorDetail();

extern int APIENTRY SID_Login(unsigned char *username);

extern int APIENTRY SID_Logout();

extern int APIENTRY SID_DownloadFirmware(char *FileFw, int (*userfunc1)(char *Item));

extern int APIENTRY SID_WeldWithoutWarming(long CmdCode);

extern int APIENTRY SID_UnitRelease();

extern int APIENTRY SID_UnitReserve(long Timeout);

extern int APIENTRY SID_MoveElevatorBag();




//PRINTER SECTION
extern int APIENTRY SID_PrinterRelease();

extern int APIENTRY SID_PrinterReserve(long Timeout);

extern int APIENTRY SID_PrinterGetStatus();

//BADGE SECTION

extern int APIENTRY SID_BadgeSetPort(int port_A);

extern int APIENTRY SID_BadgeReadData(LPSTR strRcvData,int llstrRcvData ,short  MemCardType,int NumByteToRead);

extern int APIENTRY SID_ChechkBadgePresent();

extern int APIENTRY SID_BadgeRelease();

extern int APIENTRY SID_BadgeReserve(long Timeout);

extern int APIENTRY SID_BadgeStatus();


#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////

#endif