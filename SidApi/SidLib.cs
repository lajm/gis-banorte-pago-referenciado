﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;

namespace SidApi
{
    public class SidLib
    {
        #region Constantes


        // Parameter ResetType
        public static int RESET_ERROR = 0x30;
        public static int RESET_FREE_PATH = 0x31;
        public int RESET_IMAGES_NOTE = 0x32;
        public int RESET_TOTAL_NOTES = 0x33;


        // Parameter History
        public const int CMD_READ_HISTORY = 1;
        public const int CMD_ERASE_HISTORY = 2;


        // Parameter Type DoubleLeafing
        public const int DOUBLE_LEAFING_WARNING = 0;
        public const int DOUBLE_LEAFING_DISABLE = 7;


        // Parameter ClearBlack
        public const int NO_CLEAR_BLACK = 0;
        public const int CLEAR_AND_ALIGN_IMAGE = 2;




        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -
        //                          REPLY -CODE
        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -

        public const int SID_OKAY = 0;

        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -
        //                  ERRORS
        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -
        public const int SID_SYSTEM_ERROR = -1;
        public const int SID_USB_ERROR = -2;
        public const int SID_UNIT_NOT_FOUND = -3;
        public const int SID_HARDWARE_ERROR = -4;
        public const int SID_INVALID_COMMAND = -9;
        public const int SID_COMMAND_IN_EXECUTION_YET = -10;
        public const int SID_COMMAND_SEQUENCE_ERROR = -11;
        public const int SID_PC_HW_ERROR = -12;
        public const int SID_INVALID_HANDLE = -13;
        public const int SID_IMAGE_NO_FILMED = -14;
        public const int SID_BMP_ERROR = -15;
        public const int SID_MISSING_IMAGE = -16;
        public const int SID_NO_LIBRARY_LOAD = -17;
        public const int SID_IMAGE_NOT_PRESENT = -18;
        public const int SID_OPEN_NOT_DONE = -19;
        public const int SID_INVALID_CLEARBLACK = -20;
        public const int SID_INVALID_SIDE = -21;
        public const int SID_INVALID_SCANMODE = -22;
        public const int SID_INVALID_CMD_HISTORY = -23;
        public const int SID_MISSING_BUFFER_HISTORY = -24;
        public const int SID_CALIBRATION_FAILED = -25;
        public const int SID_INVALID_DOC_LENGTH = -26;
        public const int SID_INVALID_RESET_TYPE = -27;
        public const int SID_INVALID_CASHTYPE = -28;
        public const int SID_INVALID_TYPE = -29;
        public const int SID_INVALID_VALUE = -30;
        public const int SID_MISSING_FILENAME = -32;
        public const int SID_OPEN_FILE_ERROR_OR_NOT_FOUND = -33;
        public const int SID_POWER_OFF = -34;
        public const int SID_UPS_NOT_ANSWER = -35;
        public const int SID_OPERATION_ERROR = -36;
        public const int SID_UPS_BATTERY_KO = -37;
        public const int SID_UPS_ERROR_COMUNICATION = -38;
        public const int SID_INVALID_NRDOC = -39;
        public const int SID_INVALID_QUALITY = -40;
        public const int SID_JPEG_ERROR = -41;
        public const int SID_INVALID_VALIDATION = -42;
        public const int SID_PERIPHERAL_RESERVED = -43;



        //Section Web Cam
        public const int SID_CALIBRATION_WEBCAM_ERROR = -99;
        public const int SID_INVALID_WEBCAM_TYPE = -100;
        public const int SID_INVALID_CAPTURE = -101;
        public const int SID_ERROR_FRAME = -102;
        public const int SID_NOTES_CHECKED = -103;
        public const int SID_INVALID_SHAPE_FOUND = -104;
        public const int SID_LIGHT_LOW = -105;
        public const int SID_ERROR_WEB = -106;
        public const int SID_TEMPLATE_FILE_NOT_FOUND = -107;

        //Section Jam
        public const int SID_JAM_IN_FEEDER_IN = -200;
        public const int SID_JAM_SWITCH_PATH_IN = -201;
        public const int SID_JAM_BOX_REJECT_IN = -202;
        public const int SID_JAM_ENTER_BAG_IN = -203;
        public const int SID_JAM_IN_FEEDER_OUT = -204;
        public const int SID_JAM_SWITCH_PATH_OUT = -205;
        public const int SID_JAM_BOX_REJECT_OUT = -206;
        public const int SID_JAM_ENTER_BAG_OUT = -207;
        public const int SID_REJECT_BAY_FULL = -208;
        public const int SID_JAM_PHOTO_SCANNER = -209;
        public const int SID_UNIT_DOC_TOO_LONG = -213;
        public const int SID_JAM_PRESS_NOTES = -214;


        public const int SID_WELD_NOT_POSSIBLE = -215;
        public const int SID_WELD_NOT_PERMITTED_NO_DOC_INSERTED = -216;
        public const int SID_ERROR_ON_CLOSE_BAG = -217;
        public const int SID_CURRENT_ERROR = -218;
        public const int SID_ERROR_WELD_PROTECTION = -219;
        public const int SID_TIME_OUT_EXPIRED = -220;
        public const int SID_SENSOR_DOOR_OPEN = -221;
        public const int SID_BAG_NOT_PRESENT = -222;
        public const int SID_BAG_CLOSED = -223;
        public const int SID_BAG_NOT_OPEN = -224;
        public const int SID_LOCK_ERROR = -225;
        public const int SID_BAG_PRESENT = -226;
        public const int SID_DIP_SWITCH_HIGHT = -227;
        public const int SID_DIP_SWITCH_NOT_LOW = -228;
        public const int SID_CODELOCCK_NOT_INSERTED = -229;
        public const int SID_ERROR_ELEVATOR = -230;
        public const int SID_ERROR_TENSIONER = -231;
        public const int SID_BAG_NOT_REMOVED = -232;

        public const int SID_DIP_SWITCH_DOOR_LEAFER_OPEN = -240;
        public const int SID_DIP_SWITCH_SP_MAGN1_OPEN = -241;
        public const int SID_DIP_SWITCH_SP_MAGN2_OPEN = -242;
        public const int SID_DIP_SWITCH_SID_OPEN = -243;
        public const int SID_INTERLOCK_DOOR_OPEN = -244;
        public const int SID_INTERLOCK_OPEN = -245;

        public const int SID_REJECT_DOCUMENT_NOT_RECOGNIZED = -345;
        public const int SID_REJECT_STOP_DOCUBLE_LEAFING = -346;
        public const int SID_REJECT_SUSPECT = -347;

        public const int SID_BARCODE_TIME_OUT_EXPIRED = -800;
        public const int SID_BARCODE_COM_ERROR = -801;
        public const int SID_BARCODE_STRING_TRUNCATED = -802;

        public const int SID_ERROR_RECOGNIZE_SKEWED = -1101;
        public const int SID_ERROR_RECOGNIZE_VALIDATION_SIZE = -1102;
        public const int SID_ERROR_RECOGNIZE_CLASSIFICATION = -1110;
        public const int SID_ERROR_RECOGNIZE_VALIDATION = -1111;
        public const int SID_ERROR_RECOGNIZE_IR = -1112;
        public const int SID_ERROR_RECOGNIZE_MGT = -1113;
        public const int SID_ERROR_RECOGNIZE_UV = -1114;

        public const int SID_BADGE_NOT_PRESENT = -1200;
        public const int SID_BADGE_CHECK_CARD_PRESENCE_ERROR = -1201;
        public const int SID_BADGE_SELECT_MEMCARDTYPE_ERROR = -1202;
        public const int SID_BADGE_MEMORY_READ_DATA = -1203;
        public const int SID_BADGE_NO_VALID_DATA = -1204;
        public const int SID_BADGE_OPEN_ERROR = -1205;
        public const int SID_BADGE_STATUS_ERROR = -1206;

        public const int SID_PRINTER_ERROR = -1230;
        public const int SID_PRINTER_STATUS_ERROR = -1231;
        public const int SID_PRINTER_PAPER_NOT_PRESENT = -1232;

        public const int SID_RESTART_WELD = -1999;



        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -
        //                  WARNINGS
        // =	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	 -=	 -=	 -
        public const int SID_FEEDER_EMPTY = 1;
        public const int SID_ALREADY_OPEN = 5;
        public const int SID_PERIF_BUSY = 6;
        public const int SID_REPORT_NOT_DEPOSTITED = 7;

        public const int SID_COMMAND_NOT_SUPPORTED = 13;
        public const int SID_TRY_TO_RESET = 14;
        public const int SID_STRING_TRUNCATED = 15;
        public const int SID_DOUBLE_LEAFING_WARNING = 16;
        public const int SID_DATATRUNC = 17;

        //public const int		SID_WARNING_POWER_OFF					=20;
        //public const int		SID_PERIF_BUSY_WARNING_POWER_OFF		=21;
        //public const int		SID_PAPER_JAM_WARNING_POWER_OFF			=22;

        public const int SID_WARNING_ALARM_CODE = 30;

        #endregion

        #region Structuras
        // ------------------------------------------------------------------------
        //					DEFINES STRUTTURES
        // ------------------------------------------------------------------------

        // structure for read usefull information about the configuration peripheral 


        struct _Sid_Configuration
        {
            public Int32 Size;                        // Size of the struct
            public bool Magnetic_18Channel;          // If FALSE MICR head with 9 channel
            public bool Door_Sensor;                 // Presence of the door
            public bool Lock_Sensor;                 // 
            public bool Uv_Sensor;
        }




        // structure for read usefull information about the peripheral life
        struct _Sid_History
        {
            public Int32 Size;					// Size of the struct
            public UInt32 time_peripheral_on;		// Seconds peripheral time life
            public UInt32 nr_power_on;			// Nr. of time power ON
            public UInt32 num_doc_handled;		// Nr. documents handled
            public UInt32 nr_double_leafing;		// Nr. double leafing occurs
            public UInt32 jam_feeder;				// Nr. jam in the feeder
            public UInt32 jam_feeder_out;        // Nr. jam in the feeder out
            public UInt32 jam_box_reject;        // Nr. jam in the box reject
            public UInt32 jam_bag_in;			// Nr. jam enter in bag
            public UInt32 nr_weld;	            // Nr Weld
            public UInt32 nr_banknote_inclined_clockwise;
            public UInt32 nr_banknote_inclined_anticlockwise;

        }
        #endregion


        #region Funciones
        // ------------------------------------------------------------------------
        //                  EXPORT FUNCTIONS
        // ------------------------------------------------------------------------



        [DllImportAttribute("SID.dll", EntryPoint = "SID_Open", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Open(bool fReadImages);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Initialize", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Initialize();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Close", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Close();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_GetVersion", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_GetVersion([MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder String, short Length);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Identify", CallingConvention = CallingConvention.StdCall)]
        static extern int SID_Identify(ref _Sid_Configuration Configuracion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder FwBaseVersion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder DateFw,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder SerialNrID,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder FwPocket,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder FPGABaseVersion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder ValidateVersion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder DateValidate,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder FwOpticExpansion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder FPGAExpansionVersion,
                                          [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder Id_kiosk);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ConfigDoubleLeafing", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ConfigDoubleLeafing(Int32 Type, short ValuePaper, short ValuePolymer, short Reserved);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ConfigDoubleLeafingAndDocLength", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ConfigDoubleLeafingAndDocLength(Int32 Type, short Value, Int32 DocMin, Int32 DocMax);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_CashIn", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_CashIn(ushort NrDocProcess,
                                       Int32 CashType,
                                       Int32 fValidation,
                                       bool fGetImages,
                                       Int32 Reserved,
                                       Int32 Reserved1);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_UnitStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_UnitStatus([MarshalAsAttribute(UnmanagedType.LPArray)] Byte[] SenseKey,
                                         [MarshalAsAttribute(UnmanagedType.LPArray)] Byte[] StatusByte,
                                        char[] Reserved1,
                                        char[] Reserved2,
                                        char[] Reserved3);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_GetCashTotalBag", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_GetCashTotalBag([MarshalAsAttribute(UnmanagedType.LPArray)] ushort[] TotalNotesStored);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ClearDenominationNote", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ClearDenominationNote();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ReadImage", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ReadImage(short ClearBlack,
                                         ref Int32 FrontImage,
                                        ref Int32 BackImage,
                                        ref Int32 FrontImageIr,
                                        ref Int32 BackImageIr,
                                        ref Int32 Reserved1,
                                        ref Int32 Reserved2,
                                         ref Int32 Reserved3);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_SaveDIB", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_SaveDIB(Int32 handle ,[MarshalAsAttribute(UnmanagedType.LPStr)] String filename);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Reset", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Reset(char ResetType);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_JoinBag", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_JoinBag(Int32 Operation);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_JoinBagResult", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_JoinBagResult(ref Int32 WeldPercentageExecuted);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_EnableToOpenDoor", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_EnableToOpenDoor();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_HistoryCommand", CallingConvention = CallingConvention.StdCall)]
        static extern int SID_HistoryCommand(short Cmd, ref _Sid_History sHistory);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_FreeImage", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_FreeImage(Int32 hImage);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_UPSStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_UPSStatus(ref bool fPowerOn, ref  bool flowBatery, ref bool Reserved2);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_UPSShutDown", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_UPSShutDown(Int32 TimeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ReadBarcode", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ReadBarcode([MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder BarcodeRead, ref short len_Buff, Int32 TimeOut);


        [DllImportAttribute("SID.dll", EntryPoint = "SID_ReadBarcodeEx", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ReadBarcodeEx([MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder BarcodeRead, ref short len_Buff, Int32 TimeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_OpenBag", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_OpenBag(Int32 fOperation);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_ForceOpenDoor", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_ForceOpenDoor();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_WaitReplaceBag", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WaitReplaceBag(Int32 timeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_WaitRemoveBag", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WaitRemoveBag(Int32 timeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_WaitDoorOpen", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WaitDoorOpen();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_CheckCodeLockCorrect", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_CheckCodeLockCorrect(Int32 timeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_WaitDoorClosed", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WaitDoorClosed(Int32 timeOut);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_UnlockCover", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_UnlockCover();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_RecognizeErrorDetail", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_RecognizeErrorDetail();

        [DllImportAttribute("SID.dll", EntryPoint = "SID_EnableTestNote", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_EnableTestNote(Int32 Operation);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_WaitValidationResult", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WaitValidationResult([MarshalAsAttribute(UnmanagedType.LPArray)] short[] NoteType);


        [DllImportAttribute("SID.dll", EntryPoint = "SID_WebCamParameter", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_WebCamParameter(short Operation, char WebCamSide, short nrBytes, [MarshalAsAttribute(UnmanagedType.LPArray)] short pData);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Login", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Login([MarshalAsAttribute(UnmanagedType.LPArray)] char[] username);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Logout", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Logout();

        [DllImportAttribute( "SID.dll" , EntryPoint = "SID_PrinterGetStatus" , CallingConvention = CallingConvention.StdCall )]
        public static extern int SID_PrinterGetStatus( );

        // [DllImportAttribute("SID.dll", EntryPoint = "SID_DownloadFirmware", CallingConvention = CallingConvention.StdCall)]
        // public static extern int SID_DownloadFirmware(??,??);

        [DllImportAttribute("SID.dll", EntryPoint = "SID_Weldwithoutwarming", CallingConvention = CallingConvention.StdCall)]
        public static extern int SID_Weldwhitoutwarming(Int32 CmdCode);

        //Siddll 
        /* [DllImportAttribute("SID.dll", EntryPoint = "SID_MoveElevatorBag", CallingConvention = CallingConvention.StdCall)]
         public static extern int SID_MoveElevatorBag();*/




        #endregion


        #region Subversionamiento

        # region dllversion
        public struct Dll_Configuration
        {
            public Int32 Size;                        // Size of the struct
            public bool Magnetic_18Channel;          // If FALSE MICR head with 9 channel
            public bool Door_Sensor;                 // Presence of the door
            public bool Lock_Sensor;                 // 
            public bool Uv_Sensor;
        }

        public static int GetDLLVersion(ref Dll_Configuration dll_version, ref DataTable l_DatosEquipo)
        {
            _Sid_Configuration st_dll_version = new _Sid_Configuration();

            StringBuilder l_0cadena = new StringBuilder(64);
            StringBuilder l_1cadena = new StringBuilder(64);
            StringBuilder l_2cadena = new StringBuilder(64);
            StringBuilder l_3cadena = new StringBuilder(64);
            StringBuilder l_4cadena = new StringBuilder(64);

            StringBuilder l_5cadena = new StringBuilder(64);
            StringBuilder l_6cadena = new StringBuilder(64);
            StringBuilder l_7cadena = new StringBuilder(64);
            StringBuilder l_8cadena = new StringBuilder(64);
            StringBuilder l_9cadena = new StringBuilder(64);

            l_DatosEquipo = new DataTable("Datos_equipos");
            l_DatosEquipo.Columns.Add("Caracteristica");
            l_DatosEquipo.Columns.Add("Valor");


            int replyCode = SidLib.SID_Identify(ref st_dll_version, l_0cadena, l_1cadena, l_2cadena, l_3cadena
                                               , l_4cadena, l_5cadena, l_6cadena, l_7cadena, l_8cadena, l_9cadena);

            l_DatosEquipo.Rows.Add("FwBaseVersion", l_0cadena);
            l_DatosEquipo.Rows.Add("DateFw", l_1cadena);
            l_DatosEquipo.Rows.Add("SerialNrID", l_2cadena);
            l_DatosEquipo.Rows.Add("FwPocket", l_3cadena);
            l_DatosEquipo.Rows.Add("FwGABaseVersion", l_4cadena);
            l_DatosEquipo.Rows.Add("ValidateVersion", l_5cadena);
            l_DatosEquipo.Rows.Add("DateValidate", l_6cadena);
            l_DatosEquipo.Rows.Add("FwOpticExpansion", l_7cadena);
            l_DatosEquipo.Rows.Add("FPGAExpansionVersion", l_8cadena);
            l_DatosEquipo.Rows.Add("Id_kiosk", l_9cadena);


            dll_version.Size = st_dll_version.Size;
            dll_version.Magnetic_18Channel = st_dll_version.Magnetic_18Channel;
            dll_version.Door_Sensor = st_dll_version.Door_Sensor;
            dll_version.Lock_Sensor = st_dll_version.Lock_Sensor;
            dll_version.Uv_Sensor = st_dll_version.Uv_Sensor;






            return replyCode;
        }
        #endregion

        # region History
        public struct History
        {
            public Int32 Size;					// Size of the struct
            public UInt32 time_peripheral_on;		// Seconds peripheral time life
            public UInt32 nr_power_on;			// Nr. of time power ON
            public UInt32 num_doc_handled;		// Nr. documents handled
            public UInt32 nr_double_leafing;		// Nr. double leafing occurs
            public UInt32 jam_feeder;				// Nr. jam in the feeder
            public UInt32 jam_feeder_out;        // Nr. jam in the feeder out
            public UInt32 jam_box_reject;        // Nr. jam in the box reject
            public UInt32 jam_bag_in;			// Nr. jam enter in bag
            public UInt32 nr_weld;	            // Nr Weld
            public UInt32 nr_banknote_inclined_clockwise;
            public UInt32 nr_banknote_inclined_anticlockwise;

        }

        public static int GetHistory(short cmd, ref History history)
        {
            _Sid_History l_history = new _Sid_History();

            int replyCode = SidLib.SID_HistoryCommand(cmd, ref l_history);

            history.Size = l_history.Size;					// Size of the struct
            history.time_peripheral_on = l_history.time_peripheral_on; ;		// Seconds peripheral time life
            history.nr_power_on = l_history.nr_power_on;			// Nr. of time power ON
            history.num_doc_handled = l_history.num_doc_handled;		// Nr. documents handled
            history.nr_double_leafing = l_history.nr_double_leafing; ;		// Nr. double leafing occurs
            history.jam_feeder = l_history.jam_feeder;				// Nr. jam in the feeder
            history.jam_feeder_out = l_history.jam_feeder_out;        // Nr. jam in the feeder out
            history.jam_box_reject = l_history.jam_box_reject;        // Nr. jam in the box reject
            history.jam_bag_in = l_history.jam_bag_in;			// Nr. jam enter in bag
            history.nr_weld = l_history.nr_weld;	            // Nr Weld
            history.nr_banknote_inclined_clockwise = l_history.nr_banknote_inclined_clockwise;
            history.nr_banknote_inclined_anticlockwise = l_history.nr_banknote_inclined_anticlockwise;






            return replyCode;

        }



        #endregion

        public static int ResetPath()
        {
            int result;


            result = SID_Open(true);

            if (result == SID_OKAY || result == SID_ALREADY_OPEN)
            {
                result = SID_Reset((char)RESET_FREE_PATH);

                result = SID_Close();
            }

            return result;
        }
        public static int ResetError()
        {
            int result;


            result = SID_Open(true);

            if (result == SID_OKAY )
            {
               result = SID_Close();
            }
            else if (result == SID_ALREADY_OPEN)
            {
                result = SID_Reset((char)RESET_ERROR);
            }

            return result;
        }

        # endregion


        public static void Error_Sid(int l_codigo_Error, out String o_Descripcion)
        {
            o_Descripcion = "";

            switch (l_codigo_Error)
            {
                case SID_FEEDER_EMPTY:
                    o_Descripcion = "Alimentador Vacio";
                    break;
                case SID_ALREADY_OPEN:
                    o_Descripcion = " Está Abierto";
                    break;
                case SID_PERIF_BUSY:
                    o_Descripcion = "Periferico Ocupado ";
                    break;
                case SID_REPORT_NOT_DEPOSTITED:
                    o_Descripcion = "Reporta que no han  Depósitado";
                    break;
                case SID_COMMAND_NOT_SUPPORTED:
                    o_Descripcion = "Comando no Soportado";
                    break;
                case SID_TRY_TO_RESET:
                    o_Descripcion = "Trata de Resetearse";
                    break;
                case SID_STRING_TRUNCATED:
                    o_Descripcion = "Datos Truncados";
                    break;


                case SID_DOUBLE_LEAFING_WARNING:
                    o_Descripcion = "Doble Leafing Cuidado";
                    break;
                case SID_DATATRUNC:
                    o_Descripcion = "Datos Truncados";
                    break;
                case SID_WARNING_ALARM_CODE:
                    o_Descripcion = "Cuidado codigo de Alarma";
                    break;

                case SID_SYSTEM_ERROR:
                    o_Descripcion = "Error de Sistema";
                    break;
                case SID_USB_ERROR:
                    o_Descripcion = "Error USB";
                    break;
                case SID_UNIT_NOT_FOUND:
                    o_Descripcion = "Unidad no Encontrada";
                    break;
                case SID_HARDWARE_ERROR:
                    o_Descripcion = "Hardware Error";
                    break;
                case SID_INVALID_COMMAND:
                    o_Descripcion = "Comando Inválido";
                    break;
                case SID_COMMAND_IN_EXECUTION_YET:
                    o_Descripcion = "Comando aún en Ejecución";
                    break;
                case SID_COMMAND_SEQUENCE_ERROR:
                    o_Descripcion = "Error en Secuencia de comandos";
                    break;
                case SID_PC_HW_ERROR:
                    o_Descripcion = "PC hardware Error";
                    break;
                case SID_INVALID_HANDLE:
                    o_Descripcion = "HANDLE Inválido";
                    break;
                case SID_IMAGE_NO_FILMED:
                    o_Descripcion = "Imagen no Filmada";
                    break;
                case SID_BMP_ERROR:
                    o_Descripcion = "BMP Error";
                    break;
                case SID_MISSING_IMAGE:
                    o_Descripcion = "Imagen Perdida";
                    break;
                case SID_NO_LIBRARY_LOAD:
                    o_Descripcion = "No carga Librerías";
                    break;
                case SID_IMAGE_NOT_PRESENT:
                    o_Descripcion = "Imagen no Presente";
                    break;
                case SID_OPEN_NOT_DONE:
                    o_Descripcion = "DLL OPEN no Realizado";
                    break;

                case SID_INVALID_CLEARBLACK:
                    o_Descripcion = " Inválido al limpiar colores Obscuros";
                    break;
                case SID_INVALID_SIDE:
                    o_Descripcion = "Lado Inválido";
                    break;
                case SID_INVALID_SCANMODE:
                    o_Descripcion = "Modo de Scan Inválido";
                    break;
                case SID_INVALID_CMD_HISTORY:
                    o_Descripcion = "Histórico inválido de CMD";
                    break;
                case SID_MISSING_BUFFER_HISTORY:
                    o_Descripcion = "Histórico de Buffer Perdido";
                    break;
                case SID_CALIBRATION_FAILED:
                    o_Descripcion = "Falla en Calibración ";
                    break;
                case SID_INVALID_DOC_LENGTH:
                    o_Descripcion = "Longitud de documento Inválido";
                    break;
                case SID_INVALID_RESET_TYPE:
                    o_Descripcion = "Tipo de Reset Inválido";
                    break;
                case SID_INVALID_CASHTYPE:
                    o_Descripcion = "Tipo de billete Inválido";
                    break;
                case SID_INVALID_TYPE:
                    o_Descripcion = "Type Inválido";
                    break;
                case SID_INVALID_VALUE:
                    o_Descripcion = "Valor Inválido";
                    break;

                case SID_MISSING_FILENAME:
                    o_Descripcion = "Nombre del archivo Perdido";
                    break;
                case SID_OPEN_FILE_ERROR_OR_NOT_FOUND:
                    o_Descripcion = "Error al abrir archivo o no se Encontró";
                    break;
                case SID_POWER_OFF:
                    o_Descripcion = "Apagado";
                    break;
                case SID_UPS_NOT_ANSWER:
                    o_Descripcion = "UPS no Responde";
                    break;
                case SID_OPERATION_ERROR:
                    o_Descripcion = "Error en Operación";
                    break;

                //Section Web Cam
                case SID_INVALID_WEBCAM_TYPE:
                    o_Descripcion = "Tipo de Webcam Inválido";
                    break;
                case SID_INVALID_CAPTURE:
                    o_Descripcion = "Captura Inválida";
                    break;

                case SID_ERROR_FRAME:
                    o_Descripcion = "Error Frame";
                    break;
                case SID_NOTES_CHECKED:
                    o_Descripcion = "Billetes AFUERA de la BOLSA por favor intente de nuevo, si persiste este error llame al Ingeniero de Soporte";
                    break;
                case SID_INVALID_SHAPE_FOUND:
                    o_Descripcion = "Forma encontrada Inválida";
                    break;
                case SID_LIGHT_LOW:
                    o_Descripcion = "Iluminación Baja";
                    break;
                case SID_ERROR_WEB:
                    o_Descripcion = "WebCam Error";
                    break;

                //Section Jam
                case SID_JAM_IN_FEEDER_IN:
                    o_Descripcion = "Atoramiento dentro del Alimentador ";
                    break;
                case SID_JAM_SWITCH_PATH_IN:
                    o_Descripcion = "Atoramiento dentro de la trayectoria del SWITCH";
                    break;
                case SID_JAM_BOX_REJECT_IN:
                    o_Descripcion = "Atoramiento dentro de la Caja de Rechazo";
                    break;
                case SID_JAM_ENTER_BAG_IN:
                    o_Descripcion = "Atoramiento Entrada a la bolsa";
                    break;
                case SID_JAM_IN_FEEDER_OUT:
                    o_Descripcion = "Atoramiento fuera del alimentador";
                    break;
                case SID_JAM_SWITCH_PATH_OUT:
                    o_Descripcion = "Atoramiento fuera de la trayectoria del SWITCH";
                    break;
                case SID_JAM_BOX_REJECT_OUT:
                    o_Descripcion = "Atoramiento fuera de la Caja de Rechazo";
                    break;
                case SID_JAM_ENTER_BAG_OUT:
                    o_Descripcion = "Atoramiento fuera de la bolsa";
                    break;
                case SID_REJECT_BAY_FULL:
                    o_Descripcion = "Bahía de rechazo LLena";
                    break;
                case SID_JAM_PHOTO_SCANNER:
                    o_Descripcion = "Atoramiento en el Photo Escaner";
                    break;
                case SID_UNIT_DOC_TOO_LONG:
                    o_Descripcion = "Unidad con documento muy Largo";
                    break;
                case SID_JAM_PRESS_NOTES:
                    o_Descripcion = "Atoramiento al presionar los Billetes";
                    break;

                case SID_WELD_NOT_POSSIBLE:
                    o_Descripcion = "No es posible el Sellado";
                    break;
                case SID_WELD_NOT_PERMITTED_NO_DOC_INSERTED:
                    o_Descripcion = "Sellado no permitido, no se han depositado billetes";
                    break;
                case SID_ERROR_ON_CLOSE_BAG:
                    o_Descripcion = "Error al cerrar la bolsa, Revise por favor el Cupo y vuelva a intentarlo, si falla 2 veces Llame al Ingeniero de SOPORTE ";
                    break;
                case SID_CURRENT_ERROR:
                    o_Descripcion = "Error Persistente Reinicie por favor";
                    break;
                case SID_ERROR_WELD_PROTECTION:
                    o_Descripcion = "Error de proteccional al sellar";
                    break;
                case SID_TIME_OUT_EXPIRED:
                    o_Descripcion = "Tiempo fuera expirado";
                    break;
                case SID_SENSOR_DOOR_OPEN:
                    o_Descripcion = "Sensor de puerta abierto";
                    break;
                case SID_BAG_NOT_PRESENT:
                    o_Descripcion = "Bolsa no Presente";
                    break;
                case SID_BAG_CLOSED:
                    o_Descripcion = "Bolsa Cerrada";
                    break;
                case SID_BAG_NOT_OPEN:
                    o_Descripcion = "Bolsa no Abierta";
                    break;
                case SID_LOCK_ERROR:
                    o_Descripcion = "Error en Cerradura";
                    break;
                case SID_BAG_PRESENT:
                    o_Descripcion = "Bolsa Presente";
                    break;
                case SID_DIP_SWITCH_HIGHT:
                    o_Descripcion = "Dip switch Arriba";
                    break;
                case SID_DIP_SWITCH_NOT_LOW:
                    o_Descripcion = "Dip switch no Baja";
                    break;
                case SID_CODELOCCK_NOT_INSERTED:
                    o_Descripcion = "Código de cerradura no Insertado";
                    break;
                case SID_ERROR_ELEVATOR:
                    o_Descripcion = "Error de Elevador";
                    break;
                case SID_ERROR_TENSIONER:
                    o_Descripcion = "Error de Tensioner";
                    break;
                case SID_BAG_NOT_REMOVED:
                    o_Descripcion = "Bolsa no Removida";
                    break;

                case SID_DIP_SWITCH_DOOR_LEAFER_OPEN:
                    o_Descripcion = "Dip Switch del leafter Abierta";
                    break;
                case SID_DIP_SWITCH_SP_MAGN1_OPEN:
                    o_Descripcion = "Dip switch del SP MAGNI Abierto";
                    break;
                case SID_DIP_SWITCH_SP_MAGN2_OPEN:
                    o_Descripcion = "Dip switch SP MAGN2 Abierto";
                    break;
                case SID_DIP_SWITCH_SID_OPEN:
                    o_Descripcion = "Dip switch del SID abierto";
                    break;
                case SID_INTERLOCK_DOOR_OPEN:
                    o_Descripcion = "Switch interlock de puerta abierto";
                    break;
                case SID_INTERLOCK_OPEN:
                    o_Descripcion = "Switch interlock Abierto";
                    break;
                case SID_REJECT_DOCUMENT_NOT_RECOGNIZED :
                    o_Descripcion = " Rechazo Documento No Reconozido";
                    break;
                case SID_REJECT_STOP_DOCUBLE_LEAFING :
                    o_Descripcion = " Rechazo Double Leafing ha Ocurrido ";
                    break;
                case SID_REJECT_SUSPECT :
                    o_Descripcion = " Rechazo Billete Sospechoso ";
                    break;

                case SID_BARCODE_TIME_OUT_EXPIRED :
                    o_Descripcion = " Tiempo Agotado de Lectura de Barcode";
                    break;
                case SID_BARCODE_COM_ERROR :
                    o_Descripcion = " Error En Puerto COM del Barcode ";
                    break;
                case SID_BARCODE_STRING_TRUNCATED :
                    o_Descripcion = " Datos Truncados en la Lectura del Barcode  ";
                    break;

                case SID_ERROR_RECOGNIZE_SKEWED :
                    o_Descripcion = " Billete asimetrico o torcido ";
                    break;
                case SID_ERROR_RECOGNIZE_VALIDATION_SIZE :
                    o_Descripcion = " Tamaño de billete invalido  ";
                    break;
                case SID_ERROR_RECOGNIZE_CLASSIFICATION :
                    o_Descripcion = " No se reconocio el Billete como valido";
                    break;
                case SID_ERROR_RECOGNIZE_VALIDATION :
                    o_Descripcion = "  Billete Sospechoso";
                    break;
                case SID_ERROR_RECOGNIZE_IR :
                    o_Descripcion = "  Valor Infrarojo no valido o no presente ";
                    break;
                case SID_ERROR_RECOGNIZE_MGT :
                    o_Descripcion = " Valor Magnetico no valido o no presente ";
                    break;
                case SID_ERROR_RECOGNIZE_UV :
                    o_Descripcion = " Valor UV no valido o no presente ";
                    break;

                case SID_BADGE_NOT_PRESENT :
                    o_Descripcion = " NO HAY LECTOR De TARJETAS ";
                    break;
                case SID_BADGE_CHECK_CARD_PRESENCE_ERROR :
                    o_Descripcion = " ERROR EN PRECENCIA DE TARJETAS DEL LECTOR DE TARJETAS";
                    break;
                case SID_BADGE_SELECT_MEMCARDTYPE_ERROR :
                    o_Descripcion = " ERROR AL ESCOGER MEMCARDTYPE DEL LECTOR DE TARJETAS ";
                    break;
                case SID_BADGE_MEMORY_READ_DATA :
                    o_Descripcion = " DATOS EN MEMORIA DEL LECTOR TARJETAS";
                    break;
                case SID_BADGE_NO_VALID_DATA :
                    o_Descripcion = " DATOS INVALIDOS DEL LECTOR TARJETAS ";
                    break;
                case SID_BADGE_OPEN_ERROR :
                    o_Descripcion = " SIN COMUNICACION DEL LECTOR TARJETAS ";
                    break;
                case SID_BADGE_STATUS_ERROR :
                    o_Descripcion = " ERROR EN STATUS DEL LECTOR TARJETAS ";
                    break;

                case SID_PRINTER_ERROR :
                    o_Descripcion = " ERROR EN LA IMPRESORA ";
                    break;
                case SID_PRINTER_STATUS_ERROR :
                    o_Descripcion = " ERROR EN STATUS IMPRESORA";
                    break;
                case SID_PRINTER_PAPER_NOT_PRESENT :
                    o_Descripcion = " NO HAY ROLLO DE PAPEL ";
                    break;

                case SID_RESTART_WELD :
                    o_Descripcion = " RESTAR WELD";
                    break;
                default:
                    o_Descripcion = " STATUS SID";
                    break;

            }
            o_Descripcion += "  Codigo Error_Sid NUmerico = " + l_codigo_Error;

        }

        public class IniFile
        {
            public string path;

            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section,
                string key, string val, string filePath);
            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section,
                     string key, string def, StringBuilder retVal,
                int size, string filePath);

            public IniFile(string INIPath)
            {
                path = INIPath;
            }

            public void IniWriteValue(string Section, string Key, string Value)
            {
                WritePrivateProfileString(Section, Key, Value, this.path);
            }

            public string IniReadValue(string Section, string Key)
            {
                StringBuilder temp = new StringBuilder(255);
                int i = GetPrivateProfileString(Section, Key, "", temp,
                                                255, this.path);
                return temp.ToString();

            }
        }


    }
}
