﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace FidApi
{
    public static class FidLib
    {

        #region SDK

        #region CONSTANTS
        public const int NS_OKAY = 0;

        const int HWVERSION_LEN = 30;
        const int FWVERSION_LEN = 30;
        const int TRANSPORTVERSION_LEN = 30;
        const int THICKNESSVERSION_LEN = 30;
        const int UVMAGVERSION_LEN = 30;
        const int DLLVERSION_LEN = 64;
        const int MAX_CODELINE_LEN = 50;
        #endregion

        #region STRUCTURES

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct ST_INDENTIFY
        {
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = HWVERSION_LEN + 1)]
            public string HwVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
            public string FwVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = TRANSPORTVERSION_LEN + 1)]
            public string TrVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = THICKNESSVERSION_LEN + 1)]
            public string ThicknessVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = UVMAGVERSION_LEN + 1)]
            public string MagnVersion;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct ST_INDENTIFY_USB2USB
        {
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
            public string usb2usb_FwExtVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = FWVERSION_LEN + 1)]
            public string usb2usb_FwIntVersion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = HWVERSION_LEN + 1)]
            public string usb2usb_HwVersion;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct ST_DLLVERSION
        {
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = DLLVERSION_LEN)]
            public string dllversion;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = DLLVERSION_LEN)]
            public string doc_recognition;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        struct ST_STATUS
        {
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.I1)]
            public byte[] StatusByte;
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.I1)]
            public byte[] up_photo;
            public byte up_error;
            public uint up_totaldoc;
            public byte flag_boxfull;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct ST_AUTODOCHANDLE
        {
            public byte Validate;
            public byte Side;
            public short ScanSpeed;
            public short ClearBlack;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string NumDocument;
            public short SaveImage;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string SaveImageFolder;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 40)]
            public string SaveImageBasename;
           /* public short Unit;
            public float pos_x;
            public float pos_y;
            public float sizeW;
            public float sizeH;*/
            public short FileFormat;
            public int Quality;
            public byte DocType;
            public byte UVMode;
            public byte MagneticMode;
            public byte ThicknessMode;
            public uint OtherParamMode;
            public short CodelineType;
            public uint DebugParam;
            public byte MagneticSingleChannel;
            /*
            public byte ScanSource;
            public byte ScanExtraMode;
            public byte ScanMagneticMode;
            public int PageNumber;
            public byte ScanSourceEncoder;
            public byte DocType;
             * */
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        private struct ST_GETDOCDATA
        {
            public IntPtr FrontImage;
            public IntPtr BackImage;
            public IntPtr FrontImage_IR;
            public IntPtr BackImage_IR;
            public IntPtr MagneticImage;
            [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = MAX_CODELINE_LEN)]
            public string CodelineSW;
            public byte DocInfo;
            public uint NrDoc;
           // public short CodelineType;
            public byte CodelineSide;
            public double DocAngleHor;
            public IntPtr pBufferUV1;
            public IntPtr pBufferUV2;
            public IntPtr pBufferThickness;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        private struct ST_GETDOCDATA_FROM_MEM
        {
            public IntPtr FrontImage;
            public IntPtr BackImage;
            public IntPtr FrontImage_IR;
            public IntPtr BackImage_IR;
            public IntPtr MagneticImage;
            public IntPtr pBufferUV1;
            public IntPtr pBufferUV2;
            public IntPtr pBufferThickness;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        struct ST_DOCDEPOSIT
        {
            public byte drawer_id;
            public uint doccount_todo;
            public uint doccount_done;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        struct ST_DRAWER_RESET
        {
            public byte drawer_id;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        struct ST_DRAWER_STATUS
        {
            public byte stat_drawer1;
            public byte stat_drawer2;
            public byte stat_drawer3;
            public byte stat_drawer4;
            public uint ndoc_drawer1;
            public uint ndoc_drawer2;
            public uint ndoc_drawer3;
            public uint ndoc_drawer4;
        }

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct ST_DRAWER_INDETIFY
        {
            public byte module;		
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 41)]
            public string version_D;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 41)]
            public string version_T;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        struct ST_CURRENT_COUNTERS
        {
            public int doc_inserted_total;
            public int doc_inserted_last;
            public int doc_in_escrow_total;
            public int doc_in_escrow_last;
            public int doc_in_reject_last;
            public int doc_in_suspect_last;
        }

        #endregion

        #region DLL Functions

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetIdentify", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_GetIdentify(ref ST_INDENTIFY p_st_id);
        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetDLLVersion", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_GetDLLVersion(ref ST_DLLVERSION p_st_ver);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetStatus", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_GetStatus(ref ST_STATUS p_st_sta);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_Reset", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_Reset(byte _ResetType);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_CleanPath", CallingConvention = CallingConvention.StdCall)]
        private static extern int NS_CleanPath();

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DocSessionStart", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DocSessionStart();

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DocSessionEnd", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DocSessionEnd();

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_AutoDocHandle", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_AutoDocHandle(ref ST_AUTODOCHANDLE p_st_aut);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetDocData", CallingConvention = CallingConvention.StdCall)]
        private static extern int NS_GetDocData(ref ST_GETDOCDATA p_st_gdd);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetDocDataFromMem", CallingConvention = CallingConvention.StdCall)]
        private static extern int NS_GetDocDataFromMem(int offsetLastdoc, ref ST_GETDOCDATA_FROM_MEM p_st_gddmem);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DepositExec", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DepositExec(ref ST_DOCDEPOSIT p_st_dep);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DrawerReset", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DrawerReset(ref ST_DRAWER_RESET p_st_drawer_reset);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DrawerGetStatus", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DrawerGetStatus(ref ST_DRAWER_STATUS p_st_drawer_status);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_ReleaseBag", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_ReleaseBag();

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_SerialNumber", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_SerialNumber(byte _setget, System.IntPtr _sernum, System.IntPtr _date, [MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder _pwd);

        /*
        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetCurrentCounters", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_GetCurrentCounters(ref ST_CURRENT_COUNTERS p_st_current_counters);
        */
        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetMaxDocInMemory", CallingConvention = CallingConvention.StdCall)]
        private static extern int NS_GetMaxDocInMemory(ref uint p_numDoc);


        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_SaveTrace", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_SaveTrace([MarshalAsAttribute(UnmanagedType.LPStr)] String _trace_filename, [MarshalAsAttribute(UnmanagedType.Bool)] bool from_sot);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_EnableDepositWhileFeeding", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_EnableDepositWhileFeeding([MarshalAsAttribute(UnmanagedType.Bool)] bool enable);
        #endregion

        #region VandalDoor
        public static byte g_vdoor_status;

        public static byte SERVICE_VDOOR_MODULE = 0x56;        //'V'
        public static byte SERVICE_CMD_VDOOR_UNLOCK = 0x41;    //'A'
        public static byte SERVICE_CMD_VDOOR_LOCK = 0x43;      //'C'
        public static byte SERVICE_CMD_VDOOR_STATUS = 0x53;    //'S'
        public static byte SERVICE_CMD_VDOOR_RESET = 0x52;     //'R'
        //
        public static byte SERVICE_VDOOR_STAT_OPEN = 0x41;     //'A'
        public static byte SERVICE_VDOOR_STAT_CLOSE = 0x43;    //'C'
        public static byte SERVICE_VDOOR_STAT_NONE = 0x3F;     //'?'

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "FI2DSERV_VandalDoor", CallingConvention = CallingConvention.StdCall)]
        public static extern int FI2DSERV_VandalDoor(byte command, ref byte p_status);
        #endregion

        #region Drawers

        public static int MAXDOCTYPE_IN_DRAWER = 15;
        public static int MAXDOC_IN_DRAWER_DEF = 2500;
        public static byte DOCTYPE_FINELOG = 0x3F;
        public static int TRANSPORT_PHOTO_LEN = 5;
        public static int DRAWER_PHOTO_LEN = 5;

        public static byte SAFE_DRAWER_0 = 0x30;//'0';
        public static byte SAFE_DRAWER_1 = 0x31;
        public static byte SAFE_DRAWER_2 = 0x32;
        public static byte SAFE_DRAWER_3 = 0x33;
        public static byte SAFE_DRAWER_4 = 0x34;
        public static byte SAFE_BAG = 0x80;

       

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_DrawerGetId", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_DrawerGetId(ref ST_DRAWER_INDETIFY p_st_drawer_identify);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_EnableDepositWhileFeedint", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_EnableDepositWhileFeedint([MarshalAsAttribute(UnmanagedType.Bool)] bool enable);

        [DllImportAttribute("FI2Ddll.dll", EntryPoint = "NS_GetCurrentCounters", CallingConvention = CallingConvention.StdCall)]
        static extern int NS_GetCurrentCounters(ref ST_CURRENT_COUNTERS p_st_current_counters);

        #endregion

      
        #region Identy
        public struct IDENTIFY
        {
            public string HwVersion;
            public string FwVersion;
            public string TrVersion;
            public string ThicknessVersion;
            public string MagnVersion;
           // public string usb2usb_FwExtVersion;
           // public string usb2usb_FwIntVersion;
           // public string usb2usb_HwVersion;
        }

        public static int GetIdentify(ref IDENTIFY identify)
        {
            ST_INDENTIFY st_id = new ST_INDENTIFY();
            //ST_INDENTIFY_USB2USB st_id_usb = new ST_INDENTIFY_USB2USB();

            int replyCode = NS_GetIdentify(ref st_id);

            identify.HwVersion = st_id.HwVersion;
            identify.FwVersion = st_id.FwVersion;
            identify.TrVersion = st_id.TrVersion;
            identify.ThicknessVersion = st_id.ThicknessVersion;
            identify.MagnVersion = st_id.MagnVersion;
           // identify.usb2usb_FwExtVersion = st_id_usb.usb2usb_FwExtVersion;
            //identify.usb2usb_FwIntVersion = st_id_usb.usb2usb_FwIntVersion;
            //identify.usb2usb_HwVersion = st_id_usb.usb2usb_HwVersion;

            return replyCode;
        }
        #endregion

        # region dllversion
        public struct DLL_VERSION
        {
            public string dllversion;
            public string doc_recognition;
            public string fid_net_library;
        }

        public static int GetDLLVersion(ref DLL_VERSION dll_version)
        {
            ST_DLLVERSION st_dll_version = new ST_DLLVERSION();
             
            int replyCode = NS_GetDLLVersion(ref st_dll_version);

            dll_version.dllversion = st_dll_version.dllversion;
            dll_version.doc_recognition = st_dll_version.doc_recognition;
            dll_version.fid_net_library =System.Reflection.Assembly.GetExecutingAssembly().FullName;

            return replyCode;
        }
        #endregion

        #region GetStatus
        public struct STATUS
        {
            public byte[] StatusByte;
            public byte[] up_photo;
            public byte up_error;
            public uint up_totaldoc;
            public byte flag_boxfull;
        }

        public static int GetStatus(ref STATUS status)
        {
            ST_STATUS p_st_sta = new ST_STATUS();
            int reply_code = NS_GetStatus(ref p_st_sta);

            status.StatusByte = p_st_sta.StatusByte;
            status.up_photo = p_st_sta.up_photo;
            status.up_error = p_st_sta.up_error;
            status.up_totaldoc = p_st_sta.up_totaldoc;
            status.flag_boxfull = p_st_sta.flag_boxfull;
            return reply_code;
        }
        #endregion

        public enum RESET_TYPE : byte
        {
            RESET_SW_ERROR = 0x30,
            RESET_HW_FREE_PATH
        };

        public static int Reset(RESET_TYPE reset_type)
        {
            return NS_Reset((byte)reset_type);
        }

        #region CleanPath
        public static int CleanPath()
        {
            return NS_CleanPath();
        }
        #endregion

        public static int DocSessionStart()
        {
            return NS_DocSessionStart();
            
        }

        public static int DocSessionEnd()
        {
            return NS_DocSessionEnd();
        }

        #region AutoDocHandle
        public enum VALIDATE : byte
        {
            VALIDATE_NONE,
            VALIDATE_BANKNOTE,
            VALIDATE_CHECK
          /*  VALIDATE_LOCALIZATION = 0x04,
            VALIDATE_THICKNESS = 0x08,
            VALIDATE_COUNTERS = 0x10*/
        };

        public enum SIDE : byte
        {
            SIDE_FRONT_IMAGE = 0x46,
            SIDE_BACK_IMAGE = 0x52,
            SIDE_ALL_IMAGE = 0x42,
            SIDE_NONE_IMAGE = 0x4E
        };

        public enum SCANSPEED : short
        {
            SCAN_MODE_SPEED0 = 0x30,
            SCAN_MODE_SPEED1_Y,
            //SCAN_MODE_SPEED1_YNC,
            SCAN_MODE_SPEED1_IR,
           // SCAN_MODE_SPEED1_IRNC,
            SCAN_MODE_SPEED1_BOTH,
          //  SCAN_MODE_SPEED1_BOTHNC,
            SCAN_MODE_SPEED2_Y,
           // SCAN_MODE_SPEED2_YNC,
            SCAN_MODE_SPEED2_IR,
           // SCAN_MODE_SPEED2_IRNC = 0x41,
            SCAN_MODE_SPEED2_BOTH,
          //  SCAN_MODE_SPEED2_BOTHNC,
            SCAN_MODE_SPEED3_BOTH,
          //  SCAN_MODE_SPEED3_BOTHNC,
            SCAN_DOC_SPEED_200 = 0x30,
            SCAN_DOC_SPEED_120 = 0x32
        };

        public enum CLEARBLACK : short
        {
            NO_CLEAR_BLACK = 0x30,
            CLEAR_ALL_BLACK,
            CLEAR_AND_ALIGN_IMAGE
           // ALIGN_IMAGE
        };

        public enum SAVEIMAGE : short
        {
            IMAGE_SAVE_NONE = 0x30,
            IMAGE_SAVETO_FILE,
          //  IMAGE_SAVETO_MEMORY,
          //  IMAGE_SAVE_BOTH
        };

        public enum DOCTYPE : byte
        {
            DOCTYPE_SCAN_CHECK = 0x41,
            DOCTYPE_SCAN_BANKNOTE
        };

        public enum UVMODE : byte
        {
            UVMODE_NONE,
            BIT_UV_YES
        };

        public enum MAGNETIC_MODE : byte
        {
            MAGNETIC_MODE_NONE,
            BIT_MAGNETIC_YES
        };

        public enum THICKNESS_MODE : byte
        {
            THICKNESS_MODE_NONE,
            BIT_THICKNESS_YES
        };

        public enum CODELINE_TYPE : short
        {
            NO_READ_CODELINE = 0x30,
            READ_CODELINE_MICR,
            READ_BARCODE_PDF417,
            READ_BARCODE_2_OF_5,
            READ_BARCODE_CODE39,
            READ_BARCODE_CODE128,
            READ_BARCODE_EAN13,
            READ_CODELINE_SW_OCRA = 0x41,
            READ_CODELINE_SW_OCRB_NUM,
            READ_CODELINE_SW_OCRB_ALFANUM,
            READ_CODELINE_SW_E13B = 0x45,
            READ_CODELINE_SW_OCRB_ITALY,
            READ_CODELINE_SW_MULTI_READ = 0x4d,
            READ_ONE_CODELINE_TYPE,
            READ_CODELINE_CMC7 = 0x5a
        };
        /*
        public enum FILEFORMAT : short
        {
            SAVE_JPEG = 0x30,
            SAVE_BMP
        };
        public enum SCANEXTRAMODE : byte
        {
            SCAN_EXTRAMODE_NONE = 0x30,
            SCAN_EXTRAMODE_UV = 0x31,
            SCAN_EXTRAMODE_NOSCAN = 0x32
        };

        public enum SCANMAGNETICMODE : byte
        {
            BIT_SCAN_MAGNETIC_NO = 0x80,
            BIT_SCAN_MAGNETIC_YES = 0x01,
            BIT_SCAN_MAGNETIC_NOCOMP = 0x02,
            BIT_SCAN_MAGNETIC_COUNTERS = 0x04,
            BIT_SCAN_MAGNETIC_SINGLE_CHAN = 0x08
        };
        */

         public struct AUTODOCHANDLE
        {
            public VALIDATE Validate;
            public SIDE Side;
            public SCANSPEED ScanSpeed;
            public CLEARBLACK ClearBlack;
            public int NumDocument;
            public SAVEIMAGE SaveImage;
            public string SaveImageFolder;
            public DOCTYPE DocType;
            public UVMODE UVMode;
            public MAGNETIC_MODE MagneticMode;
            public THICKNESS_MODE ThicknessMode;
            public CODELINE_TYPE CodelineType;
        }

         public static int AutoDocHandle(AUTODOCHANDLE autodochandle)
         {
             ST_AUTODOCHANDLE p_st_aut = new ST_AUTODOCHANDLE();

             p_st_aut.Validate =(byte)autodochandle.Validate;
             p_st_aut.Side = (byte)autodochandle.Side;
             p_st_aut.ScanSpeed = (short)autodochandle.ScanSpeed;
             p_st_aut.ClearBlack = (short)autodochandle.ClearBlack;
             p_st_aut.NumDocument = autodochandle.NumDocument.ToString();
             p_st_aut.SaveImage = (short)autodochandle.SaveImage;
             p_st_aut.SaveImageFolder = autodochandle.SaveImageFolder;
             p_st_aut.FileFormat = 0x31;
             p_st_aut.DocType = (byte)autodochandle.DocType;
             p_st_aut.UVMode = (byte)autodochandle.UVMode;
             p_st_aut.MagneticMode = (byte)autodochandle.MagneticMode;
             p_st_aut.ThicknessMode = (byte)autodochandle.ThicknessMode;
             p_st_aut.OtherParamMode = 0;
             p_st_aut.CodelineType = (short)autodochandle.CodelineType;
             p_st_aut.DebugParam = 0;
             p_st_aut.MagneticSingleChannel = 0x30;

             return NS_AutoDocHandle(ref p_st_aut);
         }
      

        #endregion AutoDocHandle

      
        #region Get_DocData
        public struct GETDOCDATA
        {
            public IntPtr FrontImage;
            public IntPtr BackImage;
            public IntPtr FrontImage_IR;
            public IntPtr BackImage_IR;
            public IntPtr MagneticImage;
            public string CodelineSW;
           // public string CodelineHW;
            public byte DocInfo;
            public uint NrDoc;
           // public short CodelineType;
            public byte CodelineSide;
            public double DocAngleHor;
            public IntPtr pBufferUV1;
            public IntPtr pBufferUV2;
            public IntPtr pBufferThickness;
        }

        public static int GetDocData(ref GETDOCDATA getdocdata)
        {
            ST_GETDOCDATA p_st_gdd = new ST_GETDOCDATA();
            int reply_code = NS_GetDocData(ref p_st_gdd);

            getdocdata.FrontImage = p_st_gdd.FrontImage;
            getdocdata.BackImage = p_st_gdd.BackImage;
            getdocdata.FrontImage_IR = p_st_gdd.FrontImage_IR;
            getdocdata.BackImage_IR = p_st_gdd.BackImage_IR;
            getdocdata.MagneticImage = p_st_gdd.MagneticImage;
            getdocdata.CodelineSW = p_st_gdd.CodelineSW;
            getdocdata.DocInfo = p_st_gdd.DocInfo;
            getdocdata.NrDoc = p_st_gdd.NrDoc;
            getdocdata.CodelineSide = p_st_gdd.CodelineSide;
            getdocdata.DocAngleHor = p_st_gdd.DocAngleHor;
            getdocdata.pBufferUV1 = p_st_gdd.pBufferUV1;
            getdocdata.pBufferUV2 = p_st_gdd.pBufferUV2;
            getdocdata.pBufferThickness = p_st_gdd.pBufferThickness;

            return reply_code;
        }
        #endregion

        #region GetDocDataFromMem
        public struct GETDOCDATA_FROM_MEM
        {
            public IntPtr FrontImage;
            public IntPtr BackImage;
            public IntPtr FrontImage_IR;
            public IntPtr BackImage_IR;
            public IntPtr MagneticImage;
            public IntPtr pBufferUV1;
            public IntPtr pBufferUV2;
            public IntPtr pBufferThickness;
        }

        public static int GetDocDataFromMem(int offsetLastdoc, ref GETDOCDATA_FROM_MEM getdocdata)
        {
            ST_GETDOCDATA_FROM_MEM p_st_gddmem = new ST_GETDOCDATA_FROM_MEM();
            int reply_code = NS_GetDocDataFromMem(offsetLastdoc, ref p_st_gddmem);

            getdocdata.FrontImage = p_st_gddmem.FrontImage;
            getdocdata.BackImage = p_st_gddmem.BackImage;
            getdocdata.FrontImage_IR = p_st_gddmem.FrontImage_IR;
            getdocdata.BackImage_IR = p_st_gddmem.BackImage_IR;
            getdocdata.MagneticImage = p_st_gddmem.MagneticImage;
            getdocdata.pBufferUV1 = p_st_gddmem.pBufferUV1;
            getdocdata.pBufferUV2 = p_st_gddmem.pBufferUV2;
            getdocdata.pBufferThickness = p_st_gddmem.pBufferThickness;

            return reply_code;
        }
        #endregion

        #region DepositExec
        public struct DOCDEPOSIT
        {
            public SAFE_DRAWER drawer_id;
            public uint doccount_todo;
            public uint doccount_done;
        }

        public static int DepositExec(ref DOCDEPOSIT docdeposit)
        {
            ST_DOCDEPOSIT p_st_dep = new ST_DOCDEPOSIT();
            p_st_dep.doccount_todo = docdeposit.doccount_todo;
            p_st_dep.drawer_id = (byte)docdeposit.drawer_id;

            int reply_code = NS_DepositExec(ref p_st_dep);
            docdeposit.doccount_done = p_st_dep.doccount_done;

            return reply_code;
        }
        #endregion

        #region DrawerReset
        public enum SAFE_DRAWER : byte
        {
            SAFE_DRAWER_NOVALUE = 0x3f,
            SAFE_DRAWER_0 = 0x30,
            SAFE_DRAWER_1,
            SAFE_DRAWER_2,
            SAFE_DRAWER_3,
            SAFE_DRAWER_4,
            SAFE_BAG = 0x80
        };

        public static int DrawerReset(SAFE_DRAWER drawer)
        {
            ST_DRAWER_RESET p_st_drawer_reset = new ST_DRAWER_RESET();
            p_st_drawer_reset.drawer_id = (byte)drawer;

            return NS_DrawerReset(ref p_st_drawer_reset);
        }
        #endregion

        #region DrawerGetStatus
        public struct DRAWER_STATUS
        {
            public byte stat_drawer1;
            public byte stat_drawer2;
            public byte stat_drawer3;
            public byte stat_drawer4;
            public uint ndoc_drawer1;
            public uint ndoc_drawer2;
            public uint ndoc_drawer3;
            public uint ndoc_drawer4;
        }

        public static int DrawerGetStatus(ref DRAWER_STATUS drawer_status)
        {
            ST_DRAWER_STATUS p_st_drawer_status = new ST_DRAWER_STATUS();
            int reply_code = NS_DrawerGetStatus(ref p_st_drawer_status);

            drawer_status.stat_drawer1 = p_st_drawer_status.stat_drawer1;
            drawer_status.stat_drawer2 = p_st_drawer_status.stat_drawer2;
            drawer_status.stat_drawer3 = p_st_drawer_status.stat_drawer3;
            drawer_status.stat_drawer4 = p_st_drawer_status.stat_drawer4;
            drawer_status.ndoc_drawer1 = p_st_drawer_status.ndoc_drawer1;
            drawer_status.ndoc_drawer2 = p_st_drawer_status.ndoc_drawer2;
            drawer_status.ndoc_drawer3 = p_st_drawer_status.ndoc_drawer3;
            drawer_status.ndoc_drawer4 = p_st_drawer_status.ndoc_drawer4;
            return reply_code;
        }
        #endregion

        #region ReleaseBag
        public static int ReleaseBag()
        {
            return NS_ReleaseBag();
        }
        #endregion

        #region SerialNumber
        public static int SerialNumber(ref string Serial_Number, ref string Production_Date)
        {
            IntPtr sn = Marshal.AllocHGlobal(6);
            IntPtr date = Marshal.AllocHGlobal(6);

            int reply_code = NS_SerialNumber(0x47, sn, date, null);

            Serial_Number = Marshal.PtrToStringAnsi(sn, 6);
            Production_Date = Marshal.PtrToStringAnsi(date, 6);

            Marshal.FreeHGlobal(sn);
            Marshal.FreeHGlobal(date);

            return reply_code;
        }
        #endregion

        #region GetCurrentCounters
        public struct CURRENT_COUNTERS
        {
            public int doc_inserted_total;
            public int doc_inserted_last;
            public int doc_in_escrow_total;
            public int doc_in_escrow_last;
            public int doc_in_reject_last;
            public int doc_in_suspect_last;
        }

        public static int GetCurrentCounters(ref CURRENT_COUNTERS current_counters)
        {
            ST_CURRENT_COUNTERS p_st_current_counters = new ST_CURRENT_COUNTERS();
            int reply_code = NS_GetCurrentCounters(ref p_st_current_counters);

            current_counters.doc_inserted_total = p_st_current_counters.doc_inserted_total;
            current_counters.doc_inserted_last = p_st_current_counters.doc_inserted_last;
            current_counters.doc_in_escrow_total = p_st_current_counters.doc_in_escrow_total;
            current_counters.doc_in_escrow_last = p_st_current_counters.doc_in_escrow_last;
            current_counters.doc_in_reject_last = p_st_current_counters.doc_in_reject_last;
            current_counters.doc_in_suspect_last = p_st_current_counters.doc_in_suspect_last;

            return reply_code;
        }
        #endregion

        #region GetMaxDocInMemory
        public static int GetMaxDocInMemory(ref uint p_numDoc)
        {
            return NS_GetMaxDocInMemory(ref p_numDoc);
        }
        #endregion

        #region SaveTrace
        public static int SaveTrace(string filename, bool from_sot)
        {
            return NS_SaveTrace(filename, from_sot);
        }
        #endregion

        #region EnableDepositWhileFeeding
        public static int EnableDepositWhileFeeding(bool enable)
        {
            return NS_EnableDepositWhileFeeding(enable);
        }
        #endregion

        public enum DRAWERID : byte
        {
            SAFE_DRAWER_ALL = 0x30,
            SAFE_DRAWER_1,
            SAFE_DRAWER_2,
            SAFE_DRAWER_3,
            SAFE_DRAWER_4,
            SAFE_DRAWER_BAG = 0x80
        };

        public static int DepositExec(DRAWERID drawerid, uint doccount_todo, ref uint doccount_done)
        {
            ST_DOCDEPOSIT st_docdeposit = new ST_DOCDEPOSIT();
            st_docdeposit.drawer_id = (byte)drawerid;

            st_docdeposit.doccount_todo = doccount_todo;
            int ReplyCode = NS_DepositExec(ref st_docdeposit);

            doccount_done = st_docdeposit.doccount_done;

            return ReplyCode;
        }

        public static int AbrirVandalDoor()
        {
            int ReplyCode = FI2DSERV_VandalDoor(SERVICE_CMD_VDOOR_UNLOCK, ref g_vdoor_status);

            return ReplyCode;
        }

        public static int CerrarVandalDoor()
        {
            int ReplyCode = FI2DSERV_VandalDoor(SERVICE_CMD_VDOOR_LOCK, ref g_vdoor_status);

            return ReplyCode;
        }

       

        

        public static void Dormir(int p_Milisegundos)
        {
            int l_Milisegundos = 0;
            DateTime l_HoraContinuar = DateTime.Now.AddMilliseconds(p_Milisegundos);
            while (DateTime.Now <= l_HoraContinuar)
                l_Milisegundos++;
        }

        #endregion
    }

    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }

        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            255, this.path);
            return temp.ToString();

        }
    }
 
}
